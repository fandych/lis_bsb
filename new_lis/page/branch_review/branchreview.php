<?
require_once ("../../lib/formatError.php");
require_once ("../../lib/open_con.php");
require_once ("../../requirepage/parameter.php");

$cr_notes = "";
$cr_recomend = "";

$tsql = "SELECT * FROM Tbl_CustomerMasterBRA WHERE custnomid = '$custnomid'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);

$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
  $rowCount = sqlsrv_num_rows($sqlConn);
  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
  {
	  $cr_notes = $row['notes'];
	  $cr_recomend = $row['recomend'];
  }
}
sqlsrv_free_stmt( $sqlConn );


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js" ></script>
<script type="text/javascript" src="../../js/accounting.js" ></script>
<link href="../../css/d.css" rel="stylesheet" type="text/css" />

<title>Branch Review</title>
</head>

<body>
<div align="center">
<span style="font-weight:bold">Branch Review</span>

<div align="center" style="border:1px solid black;width:960px">
<form name="frm" id="frm" method="post" action="do_branchreview.php">
<table width="960px" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?
  $param = "custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction&userbranch=$userbranch&userregion=$userregion&userid=&userpwd=$userpwd";
  ?>
  <tr>
    <th scope="col">&nbsp;</th>
    <th scope="col"><a href="../analisa5c/analisa5c.php?<?=$param;?>">Hasil Analisa Calon Debitur</a></th>
    <th scope="col"><a href="../neraca/neraca.php?<?=$param;?>">Neraca Rugi Laba</a></th>
    <th scope="col"><a href="../mkk/view_mkk_n.php?<?=$param;?>">Memo Keputusan Kredit</a></th>
    <th scope="col">&nbsp;</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
    <p>Notes :</p>
    <textarea id="cr_notes" name="cr_notes" rows="7" style="width:100%;background-color:#FF0"><?=$cr_notes;?></textarea>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <?
    if($cr_recomend == "Y")
	{
		//echo "DIREKOMENDASIKAN";
		$Yselect = "selected";
		$Nselect = "";
	}
	else
	{
		//echo "TIDAK DIREKOMENDASIKAN";
		$Yselect = "";
		$Nselect = "selected";
	}
	?>
    
    
    <select name="cr_recomend">
      <option <?=$Yselect;?> value="Y">DIREKOMENDASIKAN</option>
      <option <?=$Nselect;?> value="N">TIDAK DIREKOMENDASIKAN</option>
    </select>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center"><input type="submit" class="buttonsaveflow" name="btnSubmit" id="btnSubmit" value="Save">&nbsp;</td>
    <td>
	<?
    require("../../requirepage/hiddenfield.php");
	?>
    &nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</div>
</div>
</body>
</html>