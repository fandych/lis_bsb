<?php
include("../../../requirepage/session.php");
//$userwfid=$_REQUEST['userwfid'];
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");
require_once("../../../lib/class.sqlserver.php");
$sql = new SQLSRV();
$sql->connect();


$paramSyntax = "SELECT * FROM tbl_CreditNeed";
$sql->executeQuery($paramSyntax);
$paramResult = $sql->lastResults;
$paramCount = count($paramResult);

if($sql->debug == true){
/*echo $paramSyntax;
echo "<pre>";
print_r($paramResult);
echo "</pre>";*/
}

$arrayTemp = array();

for($x=0;$x<$paramCount;$x++){
	$code = $paramResult[$x]['credit_need_code'];
	$type = $paramResult[$x]['credit_need_name'];
	$color = randomColorOne();
	$dataSyntax = "SELECT '$type' as type,COUNT(*) AS data,'$color' AS color FROM tbl_CustomerFacility WHERE custcreditneed = $code";
	$sql->executeQuery($dataSyntax);
	$dataResult = $sql->lastResults;
	$dataCount = count($dataResult);
	
	array_push($arrayTemp,$dataResult[0]);
	
	if($sql->debug == true){
	/*echo $dataSyntax;
	echo "<pre>";
	print_r($dataResult);
	echo "</pre>";*/
	}
}

//echo "<pre>";
//print_r($arrayTemp);
//echo "</pre>";

$totalData = count($arrayTemp);

$json =  json_encode($arrayTemp);



function randomColorOne() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 


function randomColorTwo() { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
    shuffle($possibilities);
    $color = "#";
    for($i=1;$i<=6;$i++){
        $color .= $possibilities[rand(0,14)];
    }
    return $color;
} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
<script src="../../../jquery/js/jquery-1.11.3.js"></script>
<script src="../../../bootstrap/dist/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Credit Need Report</title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>
<script src="../amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
var chart;

var chartData = <?=$json;?>;

AmCharts.ready(function () {
	// SERIAL CHART
	chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "type";
	// the following two lines makes chart 3D
	chart.depth3D = 20;
	chart.angle = 30;

	// AXES
	// category
	var categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 90;
	categoryAxis.dashLength = 5;
	categoryAxis.gridPosition = "start";

	// value
	var valueAxis = new AmCharts.ValueAxis();
	valueAxis.title = "Credit Need";
	valueAxis.dashLength = 5;
	chart.addValueAxis(valueAxis);

	// GRAPH
	var graph = new AmCharts.AmGraph();
	graph.valueField = "data";
	graph.colorField = "color";
	graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	graph.type = "column";
	graph.lineAlpha = 0;
	graph.fillAlphas = 1;
	chart.addGraph(graph);

	// CURSOR
	var chartCursor = new AmCharts.ChartCursor();
	chartCursor.cursorAlpha = 0;
	chartCursor.zoomable = false;
	chartCursor.categoryBalloonEnabled = false;
	chart.addChartCursor(chartCursor);

	chart.creditsPosition = "top-right";

	// WRITE
	chart.write("chartdiv");
});
</script>
</head>
<body style="padding:0px;margin:0px;">
<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
<div id="chartdiv" style="width: 300px; height: 400px;"></div>
<div id="parameter">
<h1 style="margin:0; margin-top:10px; padding:0; padding-left:25px; padding-bottom:10px; font-family:sans-serif;"></h1>
<div style="background:#444; color:#fafafa; padding:10px;"><!--
<form name="formreport" id="formreport">
<table>
<tr>
<?php
for($x=0;$x<$totalData;$x++){
$crcode = $arrayTemp[$x]['type'];
?>
<td><input type="checkbox" name="cropt" checked="checked" id="<?=$crcode;?>" class="css-checkbox" /><label for="<?=$crcode;?>" class="css-label">&nbsp;<?=strtoupper($crcode);?>&nbsp;</label></td>
<? }?>
</tr>
</table>
</form>!-->
</div>
<p style="padding-left:25px;">Property of <a href="http://spin-international.com">SPIN-International</a></p>
</div>
</body>
</html>