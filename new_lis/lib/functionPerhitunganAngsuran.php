<?php
	
	function perhitunganAngsuran($plafond, $jaWa, $suBu, $jeBu)
	{
		$plafond = str_replace(",","",$plafond);
		$thnJaWa = intval($jaWa) / 12;
		
		if($jeBu=="1" || $jeBu=="2")
		{
			$hasil = (intval($plafond) / intval($jaWa)) + ((intval($plafond)*(intval($suBu) / 100)*$thnJaWa) / intval($jaWa));
			
			return $hasil;
			
		}else if($jeBu=="3")
		{
			$perthn = pow((1+((intval($suBu)/100)/12)),intval($jaWa));
			$hasil = (intval($plafond) * $perthn * ((intval($suBu)/100)/12)/($perthn-1));
			
			return $hasil;
			
		}else if($jeBu=="4")
		{
			$interest = intval($suBu)/100;
			$months = intval($jaWa)/12;
			$loan = intval($plafond);

			$perthn = $interest * -$loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
			//perthn = (parseInt(subu)/100)*parseInt(plaNK)*Math.pow((1+(parseInt(subu)/100)),parseInt(jawa))/(1–Math.pow((1+(parseInt(subu)/100)),parseInt(jawa)));
			$hasil = $perthn/12;
			
			return $hasil;
		}
	}
	
	function perhitunganPlafond($jeBu, $angsuran, $suBu, $jaWa)
	{
		$angsuran = str_replace(",","",$angsuran);
		$thnJaWa = intval($jaWa) / 12;
		
		if($jeBu=="1" || $jeBu=="2")
		{
			$hasil = intval($angsuran) *  intval($jaWa) / ( ( ( intval($suBu) / 100 ) * $thnJaWa ) + 1 );
			
			return $hasil;
		
		}else if($jeBu=="3")
		{	
			$perthn = pow((1+((intval($suBu)/100)/12)),intval($jaWa));
			$hasil = (intval($angsuran) * ($perthn - 1)) / ($perthn * ((intval($suBu) / 100) / 12));
			
			return $hasil;
		
		}else if($jeBu=="4")
		{
			$hasil = (($angsuran * 12) / ( ($suBu/100) * pow(( 1 + ($suBu/100)),$thnJaWa) / (1 - pow((1+($suBu/100)),$thnJaWa)))) * -1;
			
			return $hasil;
		}
	}
	
?>