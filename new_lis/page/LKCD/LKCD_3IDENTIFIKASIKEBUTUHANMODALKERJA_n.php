<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$tujuanpenggunaanmodalkerja = "";
	$jumlahpersediaan = 0;
	$lamapersediaan = "";
	$carapenjualankepembeli = "";
	$jumlahpiutang = 0;
	$lamapiutang = "";
	$carapenjualankepemasok = "";
	$jumlahhutang = 0;
	$lamahutang = "";
	
	$tsql = "select * from Tbl_LKCDModalKerja where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tujuanpenggunaanmodalkerja = $row["tujuanpenggunaanmodalkerja"];
			$jumlahpersediaan = $row["jumlahpersediaan"];
			$lamapersediaan = $row["lamapersediaan"];
			$carapenjualankepembeli = $row["carapenjualankepembeli"];
			$jumlahpiutang = $row["jumlahpiutang"];
			$lamapiutang = $row["lamapiutang"];
			$carapenjualankepemasok = $row["carapenjualankepemasok"];
			$jumlahhutang = $row["jumlahhutang"];
			$lamahutang = $row["lamahutang"];
		}
	}
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<form id="formentry" name="formentry" method="post">
<table width="100%" align="center" style="border:1px solid black;">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">IDENTIFIKASI KEBUTUHAN MODAL KERJA</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<?
	$tsql = "SELECT * FROM Tbl_TujuanModalKerja";
	$b = sqlsrv_query($conn, $tsql);
	if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($b))
	{ 
?>

<tr>
	<td width="35%">Tujuan Penggunaan Modal Kerja</td>
	<td width="65%" style="font-size:10px;">
		<select id="tujuanpenggunaanmodalkerja" name="tujuanpenggunaanmodalkerja"  nai="Tujuan Penggunaan Modal Kerja " class="harus" style="">
			<option value="-" selected="selected">- Pilih -</option>
			<?
				While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
				{
					$varselected = "";
					if($tujuanpenggunaanmodalkerja == $rowType["tujuanmodalkerja_code"])
					{
						$varselected = "selected";
					}
			?>	
					<option value="<? echo $rowType["tujuanmodalkerja_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["tujuanmodalkerja_nama"];?></option>
			<?
				}
			?>
		</select>
	<i>* Kolom disamping diisi jika permohonan debitur adalah kredit modal kerja</i></td>
</tr>
<?
	}
?>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Persediaan: <span style="color:red">(Wajib Diisi)</span></strong></td>
</tr>
<tr>
	<td>Jumlah Persediaan, atau</td>
	<td><input type="text" id="jumlahpersediaan" style="width:200px;background:#FF0;" nai="Jumlah Persediaan " value="<? echo numberFormat($jumlahpersediaan);?>"  maxlength="10"   onKeyUp="outputMoney('jumlahpersediaan')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Lama Persediaan sampai dengan terjual</td>
	<td><input type="text" id="lamapersediaan" style="width:200px;background:#FF0" nai="Lama Persediaan sampai dengan terjual "value="<? echo $lamapersediaan;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> HARI</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Piutang Usaha: <span style="color:red">(Wajib Diisi)</span></strong></td>
</tr>
<?
	$tsql = "SELECT * FROM TblTermPayment";
	$b = sqlsrv_query($conn, $tsql);
	if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($b))
	{ 
?>

<tr>
	<td width="35%">Cara Penjualan ke pembeli/buyer</td>
	<td width="65%" style="font-size:10px;">
		<select id="carapenjualankepembeli" name="carapenjualankepembeli"  nai="Cara Penjualan ke pembeli/buyer " class="harus" style="background:#FF0;">
			<option value="" selected="selected">- Pilih -</option>
			<?
				While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
				{
					$varselected = "";
					if($carapenjualankepembeli == $rowType["term_code"])
					{
						$varselected = "selected";
					}
			?>	
					<option value="<? echo $rowType["term_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["term_name"];?></option>
			<?
				}
			?>
		</select>
	</td>
</tr>
<?
	}
?>
<tr>
	<td>Jumlah Piutang Usaha, atau</td>
	<td><input type="text" id="jumlahpiutang" style="width:200px;background:#FF0;" nai="Jumlah Piutang Usaha " value="<? echo numberFormat($jumlahpiutang);?>"  maxlength="10" onKeyUp="outputMoney('jumlahpiutang')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Lama Piutang Usaha </td>
	<td><input type="text" id="lamapiutang" style="width:200px;background:#FF0" nai="Lama Piutang Usaha "value="<? echo $lamapiutang;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> HARI</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Hutang Usaha: <span style="color:red">(Wajib Diisi)</span></strong></td>
</tr>
<?
	$tsql = "SELECT * FROM TblTermPayment";
	$b = sqlsrv_query($conn, $tsql);
	if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($b))
	{ 
?>

<tr>
	<td width="40%">Cara Penjualan ke pemasok/supplier</td>
	<td width="60%" style="font-size:10px;">
		<select id="carapenjualankepemasok" name="carapenjualankepemasok"  nai="Cara Penjualan ke pemasok/supplier " class="harus" style="background:#FF0;">
			<option value="" selected="selected">- Pilih -</option>
			<?
				While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
				{
					$varselected = "";
					if($carapenjualankepemasok == $rowType["term_code"])
					{
						$varselected = "selected";
					}
			?>	
					<option value="<? echo $rowType["term_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["term_name"];?></option>
			<?
				}
			?>
		</select>
	</td>
</tr>
<?
	}
?>
<tr>
	<td>Jumlah Piutang Usaha, atau</td>
	<td><input type="text" id="jumlahhutang" style="width:200px;background:#FF0;" nai="Jumlah Hutang Usaha " value="<? echo numberFormat($jumlahhutang);?>"  maxlength="10" onKeyUp="outputMoney('jumlahhutang')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Lama Piutang Usaha </td>
	<td><input type="text" id="lamahutang" style="width:200px;background:#FF0" nai="Lama Hutang Usaha "value="<? echo $lamahutang;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> HARI</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" /></td>
</tr>
	<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
	<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
	<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
	<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
	<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
	<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
	<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
	<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>

</table>
</form>
<script type="text/javascript">
	function kumi() {
		var nan="SAVE3";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var tujuanpenggunaanmodalkerja = $("#tujuanpenggunaanmodalkerja").val();
		var jumlahpersediaan = $("#jumlahpersediaan").val();
		var lamapersediaan = $("#lamapersediaan").val();
		var carapenjualankepembeli = $("#carapenjualankepembeli").val();
		var jumlahpiutang = $("#jumlahpiutang").val();
		var lamapiutang = $("#lamapiutang").val();
		var carapenjualankepemasok = $("#carapenjualankepemasok").val();
		var jumlahhutang = $("#jumlahhutang").val();
		var lamahutang = $("#lamahutang").val();
			
		var FormName="formentry";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_ajax_n.php",
				data: "nan="+nan+"&tujuanpenggunaanmodalkerja="+tujuanpenggunaanmodalkerja+"&jumlahpersediaan="+jumlahpersediaan+"&lamapersediaan="+lamapersediaan+"&carapenjualankepembeli="+carapenjualankepembeli+"&jumlahpiutang="+jumlahpiutang+"&lamapiutang="+lamapiutang+"&carapenjualankepemasok="+carapenjualankepemasok+"&jumlahhutang="+jumlahhutang+"&lamahutang="+lamahutang+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
					alert('Data berhasil disimpan');
				}
			});
		}
	
				
	}
</script>
</body>
</html> 