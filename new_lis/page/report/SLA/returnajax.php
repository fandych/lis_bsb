<?
include("../../../requirepage/session.php");
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");

/*
$userid=$_REQUEST['userid'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];*/

require("../../../requirepage/level.php");


//echo $querylevelcode;

$querylevelcode = str_replace("TXN_ID","b.TXN_ID",$querylevelcode);
$querylevelcode = str_replace("TXN_REGION_CODE","b.TXN_REGION_CODE",$querylevelcode);
$querylevelcode = str_replace("TXN_BRANCH_CODE","b.TXN_BRANCH_CODE",$querylevelcode);

$contentperpage=10;

$clickpage=1;
if(isset($_REQUEST['page']))
{
$clickpage=$_REQUEST['page'];
}

if($querylevelcode!="")
{
	$querylevelcode = "where ".$querylevelcode;
}

$totalpage=0;
$strsql="
select  cast((COUNT(*)/".$contentperpage.") as int)+ (case when (COUNT(*)%".$contentperpage.")<>0 then 1 else 0 end) as 'totalpage', count(*) as 'ttlapp'
from Tbl_CustomerMasterPerson2 a
join Tbl_FSTART b on a.custnomid = b.txn_id
left join tbl_flow_status fs on a.custnomid = fs.txn_id
".substr($querylevelcode,0,-4)."";
//echo $strsql;
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$totalpage=$rows['totalpage'];
		$ttlapp=$rows['ttlapp'];
	}
}



$startrow=($clickpage-1)*10;
if ($clickpage!="1")
{
$startrow+=1;
}
$endrow=$clickpage*10;




?>
<div style="text-align:center;">
	<div style="width:600px;border:0px solid black;margin:0 auto;">
		<div>
			<div style="border:0px solid black; text-align:left; font-weight:bold;">Total Aplikasi adalah <?=$ttlapp?></div>
		</div>
	</div>
</div>
<div>&nbsp;</div>
<table align="center" border="1" style="width:600px; font-family:Tahoma, Arial, Verdana, sans-serif;" cellpadding="0">
	<tr  style="width:30%; background-color:#CCCCCC;">
		<td style="width:50px;">No.</td>
		<td style="width:100px;">Aplicatin ID</td>
		<td>Nama Debitur</td>
		<td style="width:30%">Status</td>
		<td style="width:5%;">View</td>
	</tr>
	<?	
		$strsql="select * from (
				select ROW_NUMBER() OVER(ORDER BY custnomid) 'idx',custnomid,isnull(fs.txn_flow,'&nbsp;') as 'status',
				'name'= case when custsex='0' then custbusname else custfullname end,
				'link'= '<A style=\"text-decoration:none;\" target=\"baru\" HREF=\"reportsla_detail.php?custnomid='+custnomid+'\">View</a>'
				from Tbl_CustomerMasterPerson a
				join Tbl_FSTART b on a.custnomid = b.txn_id
				left join tbl_flow_status fs on a.custnomid = fs.txn_id
				".substr($querylevelcode,0,-4)."
				)tblx WHERE idx >=".$startrow." AND idx <=".$endrow."
				
				";
				//echo $strsql;
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				echo "
				<tr>
					<td>".$rows['idx']."</td>
					<td>".$rows['custnomid']."</td>
					<td>".$rows['name']."</td>
					<td>".$rows['status']."</td>
					<td>".$rows['link']."</td>
				</tr>
				";
			}
		}
	?>
</table>
<div style="text-align:center;">
	<div style="width:600px;border:0px solid black;margin:0 auto;">
		<div>&nbsp;</div>
		<div>
		<?
			for($i=0; $i<$totalpage; $i++)
			{
				$pagebutton =$i+1;
				echo ' <input class="button" id="'.$pagebutton.'" name="'.$pagebutton.'" type="button" value="'.$pagebutton.'" onclick="btnpage(this.id)">';
			}
		?>
		</div>
	</div>
</div>
