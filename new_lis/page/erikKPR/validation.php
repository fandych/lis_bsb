<?
echo'
if	(duty=="")
{
	alert("Silahkan isi tanggungan.");
	$("#duty").focus();
}
else if (address=="")
{
	alert("Silahkan isi alamat.");
	$("#address").focus();
}
else if(rt=="")
{
	alert("Silahkan isi RT.");
	$("#rt").focus();
}
else if(rw=="")
{
	alert("Silahkan isi Rw.");
	$("#rw").focus();
}
else if(custprop=="")
{
	alert("Silahkan isi Propinsi.");
	$("#custprop").focus();
}
else if(kec=="")
{
	alert("Silahkan isi kecamatan.");
	$("#kec").focus();
}
else if(kel=="")
{
	alert("Silahkan isi kelurahan.");
	$("#kel").focus();
}
else if(city=="")
{
	alert("Silahkan isi kota.");
	$("#city").focus();
}
else if(zip=="")
{
	alert("Silahkan isi kode pos.");
	$("#zip").focus();
}
else if (fixphone=="")
{
	alert("Silahkan isi telepon rumah.");
	$("#fixphone").focus();
}
else if (hp=="")
{
	alert("Silahkan isi nomor hp.");
	$("#hp").focus();
}
else if	(homestatus=="")
{
	alert("Silahkan pilih status tempat tinggal saat ini.");
	$("#homestatus").focus();
}
else if	(homeyearlong=="")
{
	alert("Silahkan isi lama tahun menempati rumah.");
	$("#homeyearlong").focus();
}
else if	(homemonthlong=="")
{
	alert("Silahkan isi lama bulan menempati rumah.");
	$("#homemonthlong").focus();
}
else if(getflag=="2")
{	
	if (ktpaddress=="")
	{
		alert("Silahkan isi alamat sesuai KTP.");
		$("#ktpaddress").focus();
	}
	else if(ktprt=="")
	{
		alert("Silahkan isi RT sesuai KTP.");
		$("#ktprt").focus();
	}
	else if(ktprw=="")
	{
		alert("Silahkan isi Rw sesuai KTP.");
		$("#ktprw").focus();
	}
	else if(custpropktp=="")
	{
		alert("Silahkan isi propinsi sesuai KTP.");
		$("#custpropktp").focus();
	}
	else if(ktpkec=="")
	{
		alert("Silahkan isi kecamatan sesuai KTP.");
		$("#ktpkec").focus();
	}
	else if(ktpkel=="")
	{
		alert("Silahkan isi kelurahan sesuai KTP.");
		$("#ktpkel").focus();
	}
	else if(ktpcity=="")
	{
		alert("Silahkan isi kota sesuai KTP.");
		$("#ktpcity").focus();
	}
	else if(ktpzip=="")
	{
		alert("Silahkan isi kode pos sesuai KTP.");
		$("#ktpzip").focus();
	}
    
    
                    
                    else if(_pekerjaan=="")
                    {
                        alert("Silahkan isi Pekerjaan");
                        $("#_pekerjaan").focus();
                    }
                    else if(_jenispekerjaan=="")
                    {
                        alert("Silahkan isi Jenis Pekerjaan");
                        $("#_jenispekerjaan").focus();
                    }
                    else if(_jabatan=="")
                    {
                        alert("Silahkan isi Jabatan");
                        $("#_jabatan").focus();
                    }
                    else if(_alamat=="")
                    {
                        alert("Silahkan isi Alamat");
                        $("#_alamat").focus();
                    }
                    else if(_lamabekerja=="")
                    {
                        alert("Silahkan isi Lama bekerja");
                        $("#_lamabekerja").focus();
                    }
                    else if(_penghasilan=="")
                    {
                        alert("Silahkan isi Penghasilan");
                        $("#_penghasilan").focus();
                    }
                    else if(_jenisperushaan=="")
                    {
                        alert("Silahkan isi gaji Jenis Perusahaan");
                        $("#_jenisperushaan").focus();
                    }
                    else if(_linebisnis=="")
                    {
                        alert("Silahkan isi Line bisnis");
                        $("#_linebisnis").focus();
                    }
                    else if(_noteleponperusahaan=="")
                    {
                        alert("Silahkan isi no telepon tempat bekerja");
                        $("#_noteleponperusahaan").focus();
                    }
                    
                    else if(_fax=="")
                    {
                        alert("Silahkan isi Fax");
                        $("#_fax").focus();
                    }
                    else if(_lamaberdiriperusahaan=="")
                    {
                        alert("Silahkan isi No Lama berdiri");
                        $("#_lamaberdiriperusahaan").focus();
                    }
                    
                    
                    
	else if(custcreditstatus=="")
	{
		alert("Silahkan pilih Permohonan Kredit.");
		$("#custcreditstatus").focus();
	}
	else if(statuscredit=="")
	{
		alert("Silahkan pilih jenis pemohonan kredit.");
		$("#statuscredit").focus();
	}
	else if(rowcount=="10")
	{
		alert("Silahkan Tambah fasilitas.");
		$("#creditneed").focus();
	}
	else if(totalplafond<parseFloat(min_plafond))
	{
		alert("Silahkan Tambah fasilitas karena total plafond belum sampai "+money_min_plafond+".");
		$("#creditneed").focus();
	}
	else if(totalplafond > max_plafond)
	{
		alert("Silahkan Menguragi jumlah fasilitas karena telah melebihi "+money_max_plafond+".");
		$("#creditneed").focus();
	}
	else if(lifeinsurance=="" && lostinsurance=="" && kreditinsurance=="")
	{
		alert("Silahkan pilih salah satu dari asuransi jiwa, kerugian, kredit.");
		$("#lifeinsurance").focus();
	}
	else if(lifeinsurance!="")
	{
		if(lifeinsurance=="JK99")
		{
			if(otherlifeinsurance=="")
			{				
				alert("Silahkan isi asuransi jiwa lainnya.");
				$("#otherlifeinsurance").focus();
			}
			else
			{
				if(lostinsurance=="JK99" && otherlostinsurance=="")
				{			
					alert("Silahkan isi asuransi kerugian lainnya.");
					$("#otherlostinsurance").focus();
				}
				else if(kreditinsurance=="JK99" && otherkreditinsurance=="")
				{			
					alert("Silahkan isi asuransi kredit lainnya.");
					$("#otherkreditinsurance").focus();
				}
				else if(lkcddate=="")
				{				
					alert("Silahkan pilih tanggal kunjungan.");
					$("#lkcddate").focus();
				}
				else if (introduce=="")
				{
					alert("Silahkan pilih perkenalan.");
					$("#introduce").focus();
				}
				else if(introduce=="I01")
				{
					if(nameref=="")
					{
						alert("Silahkan isi nama referensi.");
						$("#txtnama").focus();
					}
					else if(alamatref=="")
					{
						alert("Silahkan isi alamat referensi.");
						$("#txtalamat").focus();
					}
					else if(kodeposref=="")
					{
						alert("Silahkan isi kodepos referensi.");
						$("#txtkodepos").focus();
					}
					else if(tlpref=="")
					{
						alert("Silahkan isi telephon referensi.");
						$("#txttlp").focus();
					}
					else if(hpref=="")
					{
						alert("Silahkan isi no handphone referensi.");
						$("#txthp").focus();
					}
					else if(pekerjaanref=="")
					{
						alert("Silahkan isi pekerjaan referensi.");
						$("#txtpekerjaanref").focus();
					}
					else if (recomen=="")
					{
						alert("Silahkan pilih rekomendasi.");
						$("#recomen").focus();
					}
					else
					{
						document.getElementById("frm").action = "fak_ajax.php";
						submitform = window.confirm("'.$confmsg.'")
						if (submitform == true)
						{
							document.getElementById("frm").submit();
							return true;
						}
						else
						{
							return false;
						}
					}
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		else
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	else if(lostinsurance!="")
	{
		if(lostinsurance=="JK99")
		{
			if(otherlostinsurance=="")
			{				
				alert("Silahkan isi asuransi kerugian lainnya.");
				$("#otherlostinsurance").focus();
			}
			else
			{
				if(kreditinsurance=="JK99" && otherkreditinsurance=="")
				{			
					alert("Silahkan isi asuransi kredit lainnya.");
					$("#otherkreditinsurance").focus();
				}
				else if(lkcddate=="")
				{				
					alert("Silahkan pilih tanggal kunjungan.");
					$("#lkcddate").focus();
				}
				else if (introduce=="")
				{
					alert("Silahkan pilih perkenalan.");
					$("#introduce").focus();
				}
				else if(introduce=="I01")
				{
					if(nameref=="")
					{
						alert("Silahkan isi nama referensi.");
						$("#txtnama").focus();
					}
					else if(alamatref=="")
					{
						alert("Silahkan isi alamat referensi.");
						$("#txtalamat").focus();
					}
					else if(kodeposref=="")
					{
						alert("Silahkan isi kodepos referensi.");
						$("#txtkodepos").focus();
					}
					else if(tlpref=="")
					{
						alert("Silahkan isi telephon referensi.");
						$("#txttlp").focus();
					}
					else if(hpref=="")
					{
						alert("Silahkan isi no handphone referensi.");
						$("#txthp").focus();
					}
					else if(pekerjaanref=="")
					{
						alert("Silahkan isi pekerjaan referensi.");
						$("#txtpekerjaanref").focus();
					}
					else if (recomen=="")
					{
						alert("Silahkan pilih rekomendasi.");
						$("#recomen").focus();
					}
					else
					{
						document.getElementById("frm").action = "fak_ajax.php";
						submitform = window.confirm("'.$confmsg.'")
						if (submitform == true)
						{
							document.getElementById("frm").submit();
							return true;
						}
						else
						{
							return false;
						}
					}
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		else
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}

			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	else if(kreditinsurance!="")
	{
		if(kreditinsurance=="JK99")
		{
			if(otherkreditinsurance=="")
			{				
				alert("Silahkan isi asuransi kredit lainnya.");
				$("#otherkreditinsurance").focus();
			}
			else
			{
				if(lkcddate=="")
				{				
					alert("Silahkan pilih tanggal kunjungan.");
					$("#lkcddate").focus();
				}
				else if (introduce=="")
				{
					alert("Silahkan pilih perkenalan.");
					$("#introduce").focus();
				}
				else if(introduce=="I01")
				{
					if(nameref=="")
					{
						alert("Silahkan isi nama referensi.");
						$("#txtnama").focus();
					}
					else if(alamatref=="")
					{
						alert("Silahkan isi alamat referensi.");
						$("#txtalamat").focus();
					}
					else if(kodeposref=="")
					{
						alert("Silahkan isi kodepos referensi.");
						$("#txtkodepos").focus();
					}
					else if(tlpref=="")
					{
						alert("Silahkan isi telephon referensi.");
						$("#txttlp").focus();
					}
					else if(hpref=="")
					{
						alert("Silahkan isi no handphone referensi.");
						$("#txthp").focus();
					}
					else if(pekerjaanref=="")
					{
						alert("Silahkan isi pekerjaan referensi.");
						$("#txtpekerjaanref").focus();
					}
					else if (recomen=="")
					{
						alert("Silahkan pilih rekomendasi.");
						$("#recomen").focus();
					}
					else
					{
						document.getElementById("frm").action = "fak_ajax.php";
						submitform = window.confirm("'.$confmsg.'")
						if (submitform == true)
						{
							document.getElementById("frm").submit();
							return true;
						}
						else
						{
							return false;
						}
					}
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		else
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}

			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
}

//--- start new validation

else if(_pekerjaan=="")
                    {
                        alert("Silahkan isi Pekerjaan");
                        $("#_pekerjaan").focus();
                    }
                    else if(_jenispekerjaan=="")
                    {
                        alert("Silahkan isi Jenis Pekerjaan");
                        $("#_jenispekerjaan").focus();
                    }
                    else if(_jabatan=="")
                    {
                        alert("Silahkan isi Jabatan");
                        $("#_jabatan").focus();
                    }
                    else if(_alamat=="")
                    {
                        alert("Silahkan isi Alamat");
                        $("#_alamat").focus();
                    }
                    else if(_lamabekerja=="")
                    {
                        alert("Silahkan isi Lama bekerja");
                        $("#_lamabekerja").focus();
                    }
                    else if(_penghasilan=="")
                    {
                        alert("Silahkan isi Penghasilan");
                        $("#_penghasilan").focus();
                    }
                    else if(_jenisperushaan=="")
                    {
                        alert("Silahkan isi gaji Jenis Perusahaan");
                        $("#_jenisperushaan").focus();
                    }
                    else if(_linebisnis=="")
                    {
                        alert("Silahkan isi Line bisnis");
                        $("#_linebisnis").focus();
                    }
                    else if(_noteleponperusahaan=="")
                    {
                        alert("Silahkan isi no telepon tempat bekerja");
                        $("#_noteleponperusahaan").focus();
                    }
                    else if(_fax=="")
                    {
                        alert("Silahkan isi Fax");
                        $("#_fax").focus();
                    }
                    else if(_lamaberdiriperusahaan=="")
                    {
                        alert("Silahkan isi No Lama berdiri");
                        $("#_lamaberdiriperusahaan").focus();
                    }
                    
//--- ending new validation
else if(custcreditstatus=="")
{
	alert("Silahkan pilih Permohonan Kredit.");
	$("#custcreditstatus").focus();
}
else if(statuscredit=="")
{
	alert("Silahkan pilih jenis pemohonan kredit.");
	$("#statuscredit").focus();
}
else if(rowcount==10)
{
	alert("Silahkan Tambah fasilitas.");
	$("#creditneed").focus();
}
else if(totalplafond<parseFloat(min_plafond))
{
	alert("Silahkan Tambah fasilitas karena total plafond belum sampai "+money_min_plafond+".");
	$("#creditneed").focus();
}
else if(totalplafond > max_plafond)
{
	alert("Silahkan Menguragi jumlah fasilitas karena telah melebihi "+money_max_plafond+".");
	$("#creditneed").focus();
}
else if(lifeinsurance=="" && lostinsurance=="" && kreditinsurance=="")
{
	alert("Silahkan pilih salah satu dari asuransi jiwa, kerugian, kredit.");
	$("#lifeinsurance").focus();
}
else if(lifeinsurance!="")
{
	if(lifeinsurance=="JK99")
	{
		if(otherlifeinsurance=="")
		{				
			alert("Silahkan isi asuransi jiwa lainnya.");
			$("#otherlifeinsurance").focus();
		}
		else
		{
			if(lostinsurance=="JK99" && otherlostinsurance=="")
			{			
				alert("Silahkan isi asuransi kerugian lainnya.");
				$("#otherlostinsurance").focus();
			}
			else if(kreditinsurance=="JK99" && otherkreditinsurance=="")
			{			
				alert("Silahkan isi asuransi kredit lainnya.");
				$("#otherkreditinsurance").focus();
			}
			else if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	else
	{
		if(lkcddate=="")
		{				
			alert("Silahkan pilih tanggal kunjungan.");
			$("#lkcddate").focus();
		}
		else if (introduce=="")
		{
			alert("Silahkan pilih perkenalan.");
			$("#introduce").focus();
		}
		else if(introduce=="I01")
		{
			if(nameref=="")
			{
				alert("Silahkan isi nama referensi.");
				$("#txtnama").focus();
			}
			else if(alamatref=="")
			{
				alert("Silahkan isi alamat referensi.");
				$("#txtalamat").focus();
			}
			else if(kodeposref=="")
			{
				alert("Silahkan isi kodepos referensi.");
				$("#txtkodepos").focus();
			}
			else if(tlpref=="")
			{
				alert("Silahkan isi telephon referensi.");
				$("#txttlp").focus();
			}
			else if(hpref=="")
			{
				alert("Silahkan isi no handphone referensi.");
				$("#txthp").focus();
			}
			else if(pekerjaanref=="")
			{
				alert("Silahkan isi pekerjaan referensi.");
				$("#txtpekerjaanref").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if (recomen=="")
		{
			alert("Silahkan pilih rekomendasi.");
			$("#recomen").focus();
		}
		else
		{
			document.getElementById("frm").action = "fak_ajax.php";
			submitform = window.confirm("'.$confmsg.'")
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
else if(lostinsurance!="")
{
	if(lostinsurance=="JK99")
	{
		if(otherlostinsurance=="")
		{				
			alert("Silahkan isi asuransi kerugian lainnya.");
			$("#otherlostinsurance").focus();
		}
		else
		{
			if(kreditinsurance=="JK99" && otherkreditinsurance=="")
			{			
				alert("Silahkan isi asuransi kredit lainnya.");
				$("#otherkreditinsurance").focus();
			}
			else if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	else
	{
		if(lkcddate=="")
		{				
			alert("Silahkan pilih tanggal kunjungan.");
			$("#lkcddate").focus();
		}
		else if (introduce=="")
		{
			alert("Silahkan pilih perkenalan.");
			$("#introduce").focus();
		}
		else if(introduce=="I01")
		{
			if(nameref=="")
			{
				alert("Silahkan isi nama referensi.");
				$("#txtnama").focus();
			}
			else if(alamatref=="")
			{
				alert("Silahkan isi alamat referensi.");
				$("#txtalamat").focus();
			}
			else if(kodeposref=="")
			{
				alert("Silahkan isi kodepos referensi.");
				$("#txtkodepos").focus();
			}
			else if(tlpref=="")
			{
				alert("Silahkan isi telephon referensi.");
				$("#txttlp").focus();
			}
			else if(hpref=="")
			{
				alert("Silahkan isi no handphone referensi.");
				$("#txthp").focus();
			}
			else if(pekerjaanref=="")
			{
				alert("Silahkan isi pekerjaan referensi.");
				$("#txtpekerjaanref").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if (recomen=="")
		{
			alert("Silahkan pilih rekomendasi.");
			$("#recomen").focus();
		}
		else
		{
			document.getElementById("frm").action = "fak_ajax.php";
			submitform = window.confirm("'.$confmsg.'")
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
else if(kreditinsurance!="")
{
	if(kreditinsurance=="JK99")
	{
		if(otherkreditinsurance=="")
		{				
			alert("Silahkan isi asuransi kredit lainnya.");
			$("#otherkreditinsurance").focus();
		}
		else
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if(introduce=="I01")
			{
				if(nameref=="")
				{
					alert("Silahkan isi nama referensi.");
					$("#txtnama").focus();
				}
				else if(alamatref=="")
				{
					alert("Silahkan isi alamat referensi.");
					$("#txtalamat").focus();
				}
				else if(kodeposref=="")
				{
					alert("Silahkan isi kodepos referensi.");
					$("#txtkodepos").focus();
				}
				else if(tlpref=="")
				{
					alert("Silahkan isi telephon referensi.");
					$("#txttlp").focus();
				}
				else if(hpref=="")
				{
					alert("Silahkan isi no handphone referensi.");
					$("#txthp").focus();
				}
				else if(pekerjaanref=="")
				{
					alert("Silahkan isi pekerjaan referensi.");
					$("#txtpekerjaanref").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
	else
	{
		if(lkcddate=="")
		{				
			alert("Silahkan pilih tanggal kunjungan.");
			$("#lkcddate").focus();
		}
		else if (introduce=="")
		{
			alert("Silahkan pilih perkenalan.");
			$("#introduce").focus();
		}
		else if(introduce=="I01")
		{
			if(nameref=="")
			{
				alert("Silahkan isi nama referensi.");
				$("#txtnama").focus();
			}
			else if(alamatref=="")
			{
				alert("Silahkan isi alamat referensi.");
				$("#txtalamat").focus();
			}
			else if(kodeposref=="")
			{
				alert("Silahkan isi kodepos referensi.");
				$("#txtkodepos").focus();
			}
			else if(tlpref=="")
			{
				alert("Silahkan isi telephon referensi.");
				$("#txttlp").focus();
			}
			else if(hpref=="")
			{
				alert("Silahkan isi no handphone referensi.");
				$("#txthp").focus();
			}
			else if(pekerjaanref=="")
			{
				alert("Silahkan isi pekerjaan referensi.");
				$("#txtpekerjaanref").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if (recomen=="")
		{
			alert("Silahkan pilih rekomendasi.");
			$("#recomen").focus();
		}
		else
		{
			document.getElementById("frm").action = "fak_ajax.php";
			submitform = window.confirm("'.$confmsg.'")
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}

';

?>