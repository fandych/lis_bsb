<?
	
	require ("../../../lib/open_con.php");
	require ("../../../lib/formatError.php");
	require ("../../../requirepage/parameter.php");
	
	//echo $_POST['theid'];
	
	
    $strsqlv01="SELECT *, convert(varchar, _pasangan_tgllahir, 111) as _tgllahir, convert(varchar, _pasangan_expiredktp, 111) as _expiredktp, convert(varchar, _pasangan_mulaibekerjan, 111) as _mulaikerja FROM pl_custpasangan WHERE _custnomid = '$custnomid'";
    //echo $strsqlv01;
    $sqlconv01 = sqlsrv_query($conn, $strsqlv01);
    if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
    if(sqlsrv_has_rows($sqlconv01))
    {
        if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
        {
            $v_namadepan = $rowsv01['_pasangan_namadepan'];
            $v_namatengah = $rowsv01['_pasangan_namatengah'];
            $v_namabelakang = $rowsv01['_pasangan_namabelakang'];
            $v_tmptlahir = $rowsv01['_pasangan_tempatlahir'];
            $v_tgllahir = $rowsv01['_tgllahir'];
            $v_noktp = $rowsv01['_pasangan_nomorktp'];
            $v_expiredktp = $rowsv01['_expiredktp'];
            $v_pekerjaan = $rowsv01['_pasangan_pekerjaan_id'];
            $v_mulaikerja = $rowsv01['_mulaikerja'];
            $v_nmperusahaan = $rowsv01['_pasangan_namaperusahaan'];
            $v_tlpperusahaan = $rowsv01['_pasangan_telpperusahaan'];
            $v_penghasilan = number_format($rowsv01['_pasangan_penghasilanbersih']);
        }
    }
	
    
    $strsqlv01="SELECT *, convert(varchar, _pasangan_tgllahir, 111) as _tgllahir, convert(varchar, _pasangan_expiredktp, 111) as _expiredktp, convert(varchar, _pasangan_mulaibekerjan, 111) as _mulaikerja FROM pl_custpasangan2 WHERE _custnomid = '$custnomid'";
    //echo $strsqlv01;
    $sqlconv01 = sqlsrv_query($conn, $strsqlv01);
    if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
    if(sqlsrv_has_rows($sqlconv01))
    {
        if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
        {
            $new_vnamadepan = $rowsv01['_pasangan_namadepan'];
            $new_vnamatengah = $rowsv01['_pasangan_namatengah'];
            $new_vnamabelakang = $rowsv01['_pasangan_namabelakang'];
            $new_vtmptlahir = $rowsv01['_pasangan_tempatlahir'];
            $new_vtgllahir = $rowsv01['_tgllahir'];
            $new_vnoktp = $rowsv01['_pasangan_nomorktp'];
            $new_vexpiredktp = $rowsv01['_expiredktp'];
            $new_vpekerjaan = $rowsv01['_pasangan_pekerjaan_id'];
            $new_vmulaikerja = $rowsv01['_mulaikerja'];
            $new_vnmperusahaan = $rowsv01['_pasangan_namaperusahaan'];
            $new_vtlpperusahaan = $rowsv01['_pasangan_telpperusahaan'];
            $new_vpenghasilan = number_format($rowsv01['_pasangan_penghasilanbersih']);
        }
    }

?>
<html>
	<head>
		<title>Entry Data Pasangan</title>
		<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../../js/accounting.js" ></script>
		<script type="text/javascript" src="../../../js/my.js" ></script>
		<link href="../../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			function fnHitung() {
				var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('txtpenghasilan').value))));		 
			}
			function save()
			{
				var anmdepan=$("#txtnamadepan").val();
				var atmplahir=$("#txttmptlahir").val();
				var atgllahir=$("#txttgllahir").val();
				var anoktp=$("#txtnoktp").val();
				var amasaberlaku=$("#txtmasaberlaku").val();
				var apekerjaan=$("#txtpekerjaan").val();
				/*
				if (anmdepan=="")
				{
					alert("Silahkan isi nama depan pasangan.");
					$("#txtnamadepan").focus();
				}
				else if (atmplahir=="")
				{
					alert("Silahkan isi tempat lahir.");
					$("#txttmptlahir").focus();
				}
				else if (atgllahir=="")
				{
					alert("Silahkan isi tanggal lahir.");
					$("#txttgllahir").focus();
				}
				else if (anoktp=="")
				{
					alert("Silahkan isi nomer KTP.");
					$("#txtnoktp").focus();
				}
				else if (amasaberlaku=="")
				{
					alert("Silahkan isi masa berlaku KTP.");
					$("#txtmasaberlaku").focus();
				}
				else if (apekerjaan=="")
				{
					alert("Silahkan isi pekerjaan.");
					$("#txtpekerjaan").focus();
				}else{
                    
					document.frm.action = "./pasangan_pvid.php";
					document.frm.submit();
				}
                */
                
                document.frm.action = "./pasangan_pvid.php";
					document.frm.submit();
			}
		</script>
		<style>
        .betulinheight tr td
        {
            height:25px;
            line-height:25px;
        }
		</style>
	</head>
	<body style="margin:0;">
		<br><br>
		<form id="frm" name="frm" method="post">
			<div class="divcenter">
				<table border="1" style ="width:900px; border-color:black;" align="center" id="tblform">
					<tr>
						<td align="center" colspan="2" style="font-size:14pt;height:70px;">ENTRY DATA PASANGAN</td>
					</tr>
					<tr>
						<td width="50%">
							<table class="betulinheight" border="0" style="margin:10px 5px;">
								<tr>
									<td style="width:30%;">Nama Depan</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_namadepan?>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Nama Tengah</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_namatengah?>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Nama Belakang</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_namabelakang?>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Tempat Lahir</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_tmptlahir?>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Tanggal lahir</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_tgllahir?>
									</td>
								</tr>
								<tr>
									<td style="width:20%;">Nomer KTP</td>
									<td>&nbsp;</td>
									<td style="width:80%;">
										: <?php echo $v_noktp?>
									</td>
								</tr>
                                <tr>
									<td style="width:30%;">Masa Berlaku KTP</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										: <?php echo $v_expiredktp?>
									</td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>&nbsp;</td>
									<td>
											<?        
											$tsql = "select * from param_pekerjaan where code = '$v_pekerjaan'";
											$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
											$params = array(&$_POST['query']);
											$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

											if ( $sqlConn === false)
											die( FormatErrors( sqlsrv_errors() ) );

											if(sqlsrv_has_rows($sqlConn))
											{
												while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
												{
													$idker =$row['code'];
													$nameker = $row['attribute'];
													echo ": ".$nameker;
												}
											}
												sqlsrv_free_stmt( $sqlConn );
											?>
									</td>
								</tr>
								<tr>
									<td>Mulai Bekerja</td>
									<td>&nbsp;</td>
									<td>
										: <?php echo $v_mulaikerja?>
									</td>
								</tr>
								<tr>
									<td>Nama Perusahaan</td>
									<td>&nbsp;</td>
									<td>
										: <?php echo $v_nmperusahaan?>
									</td>
								</tr>
								<tr>
									<td>Telepon Perusahaan</td>
									<td>&nbsp;</td>
									<td>
										: <?php echo $v_tlpperusahaan?>
									</td>
								</tr>
								<tr>
									<td>Penghasilan Bersih</td>
									<td>&nbsp;</td>
									<td>
										: <?php echo $v_penghasilan?>
									</td>
								</tr>
							</table>
						</td>
						<td width="50%;">
							<table class="betulinheight" border="0" style="margin:10px 5px;">
								<tr>
									<td style="width:30%;">Nama Depan</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txtnamadepan" name="txtnamadepan" alt="Nama Depan" maxlength="50" value="<?php echo $new_vnamadepan?>" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Nama Tengah</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txtnamatengah" name="txtnamatengah" alt="Nama Tengah" maxlength="50" value="<?php echo $new_vnamatengah?>" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Nama Belakang</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txtnamabelakang" name="txtnamabelakang" alt="Nama Belakang" maxlength="50" value="<?php echo $new_vnamabelakang?>" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Tempat Lahir</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txttmptlahir" name="txttmptlahir" alt="Tempat Lahir" value="<?php echo $new_vtmptlahir?>" maxlength="50" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td style="width:30%;">Tanggal lahir</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txttgllahir" name="txttgllahir" alt="Tanggal Lahir" value="<?php echo $new_vtgllahir?>" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td style="width:20%;">Nomer KTP</td>
									<td>&nbsp;</td>
									<td style="width:80%;">
										<input type="text" id="txtnoktp" name="txtnoktp" alt="Nomor KTP" maxlength="50" value="<?php echo $new_vnoktp?>" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
                                <tr>
									<td style="width:30%;">Masa Berlaku KTP</td>
									<td>&nbsp;</td>
									<td style="width:70%;">
										<input type="text" id="txtmasaberlaku" name="txtmasaberlaku" alt="Masa Berlaku KTP" value="<?php echo $new_vexpiredktp?>" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');" style="width:100%; background-color:#ffffff;"/>
									</td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>&nbsp;</td>
									<td>
										<select name="txtpekerjaan" id="txtpekerjaan" alt="Pekerjaan" style="width:100%; background-color:#ffffff;">
											<option selected="selected" value="">- Pekerjaan -</option>
											<?        
											$tsql = "select * from param_pekerjaan";
											$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
											$params = array(&$_POST['query']);
											$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

											if ( $sqlConn === false)
											die( FormatErrors( sqlsrv_errors() ) );

											if(sqlsrv_has_rows($sqlConn))
											{
												while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
												{
													$idker =$row['code'];
													$nameker = $row['attribute'];
													if($new_vpekerjaan==$idker)
													{
														echo'<option value="'.$idker.'" selected>'.$nameker.'</option>';
													}else{
														echo'<option value="'.$idker.'">'.$nameker.'</option>';
													}
												}
											}
												sqlsrv_free_stmt( $sqlConn );
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td>Mulai Bekerja</td>
									<td>&nbsp;</td>
									<td>
										<input id="txtmulaikerja" name="txtmulaikerja" alt="Mulai Kerja" type="text" value="<?php echo $new_vmulaikerja?>" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');" style="width:100%;background-color:#ffffff;" />
									</td>
								</tr>
								<tr>
									<td>Nama Perusahaan</td>
									<td>&nbsp;</td>
									<td>
										<input id="txtnmperusahaan" name="txtnmperusahaan" alt="Nama Perusahaan" type="text" maxlength="30" value="<?php echo $new_vnmperusahaan?>" style="width:100%;background-color:#ffffff;" />
									</td>
								</tr>
								<tr>
									<td>Telepon Perusahaan</td>
									<td>&nbsp;</td>
									<td>
										<input id="txttlpperusahaan" name="txttlpperusahaan" alt="Telepon Perusahaan" type="text" maxlength="14" value="<?php echo $new_vtlpperusahaan?>" onkeypress="return isNumberKey(event)" style="width:100%; background-color:#ffffff;" />
									</td>
								</tr>
								<tr>
									<td>Penghasilan Bersih</td>
									<td>&nbsp;</td>
									<td>
										<input id="txtpenghasilan" name="txtpenghasilan" alt="Penghasilan Bersih" type="text" maxlength="30" value="<?php echo $new_vpenghasilan?>" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" onkeypress="return isNumberKey(event)" style="width:100%;background-color:#ffffff;" />
									</td>
								</tr>
							</table>
						</td>
					</td>
					<tr>
						<td colspan="2" align="center" height="50px">
							<input type="button" id="btnadd" name="btnadd" value="Save" class="buttonsaveflow" onclick="save();"/>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div>
				<?
					require ("../../../requirepage/hiddenfield.php");
				?>
		</form>
	</body>
</html>