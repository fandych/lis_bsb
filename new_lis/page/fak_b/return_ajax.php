<?


$strsql="SELECT 'custcurcode'= case when ISNULL(custcurcode,0)='0' then 'IDR' else custcurcode end,custbusnpwp from tbl_customermasterperson where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$custcurcode=$rows['custcurcode'];
	}
}


$strsql = "select count(*) as countrow from tbl_customerfacility where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$chcekrow=$rows['countrow'];
	}
}
$condition="";
if ($chcekrow!="0")
{
	$tmpcondition="";
	$strsql = "select custcreditneed from tbl_customerfacility where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$tmpcondition.="'".$rows['custcreditneed']."',";
		}
	}
	$condition="where credit_need_code not in (".substr($tmpcondition,0,-1).")";
}
?>
<table class="tbl100">
	<tr>
		<td>Tujuan Pengajuan Kredit</td>
		<td>
			<select id="creditneed" name="creditneed" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
				<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
				<?
					$strsql = "select * from tbl_Creditneed ".$condition;
					$sqlcon = sqlsrv_query($conn, $strsql);
					if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlcon))
					{
						while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
						{
							echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
						}
					}
				?>
			</select>
			
		</td>
	</tr>
	<tr>
		<td>Jenis Fasilitas</td>
		<td style="width:250px;" id="dtl_creditneed">
			<select id="codeproduct" name="codeproduct" style="width:100%;" class="nonmandatory">
				<option value="">-- Pilih Jenis Fasilitas --</option>
				<?
					$strsql = "select * from tbl_kodeproduk";
					$sqlcon = sqlsrv_query($conn, $strsql);
					if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlcon))
					{
						while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
						{
							echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Plafond (<? echo $custcurcode;?>)</td>
		<td>
			<input type="text" id="plafond" name="plafond" class="nonmandatory" onkeyup="currency(this.id);" maxlength="15"/>
		</td>
	</tr>
	<tr>
		<td>Jangka Waktu</td>
		<td>
			<input type="text" style="width:50px" id="period" name="period" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);"/> &nbsp;&nbsp; Bulan
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right">
			<input type="button" id="btnadd" name="btnadd" value="save" class="button" onclick="adddtl();"/>
		</td>
	</tr>
</table>
<?
	
$totalpalfond=0;
$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,
			c.produk_type_description,a.custcreditplafond,a.custcreditlong 
			from tbl_CustomerFacility a 
			left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
			left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
			where a.custnomid ='".$custnomid."'";
		
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	echo '
		<table class="tbl100">
		';
	while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$totalpalfond+=$rows['custcreditplafond'];
		echo '
			<tr>
				<td style="width:180px;">Tujuan Pengajuan Kredit</td>
				<td style="width:180px;">'.$rows['credit_need_name'].'</td>
				<td rowspan="2" style="text-align:right;">&nbsp;
					<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />
				</td>
			</tr>
			<tr>
				<td>Jenis Fasilitas</td>
				<td>'.$rows['produk_type_description'].'</td>
			</tr>
			<tr>
				<td>Plafond ('.$custcurcode.')</td>
				<td>'.numberFormat($rows['custcreditplafond']).'</td>
				<td rowspan="2" style="text-align:right;">
					<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />
				</td>
			</tr>
			<tr>
				<td>Jangka Waktu</td>
				<td>'.$rows['custcreditlong'].'</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
		';
	}	
	echo '
		</table>
		<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
	';
}
?>