<?
require("../../lib/sqlsrv.lis.php");
require("../../requirepage/parameter.php");

$db_lis = new DB_LIS();
$db_lis->connect();

$custnomid = $_REQUEST['custnomid'];

$sort_by = "
ORDER BY
CASE
  WHEN ISNUMERIC(code)=1
  THEN CAST(code AS int)
  WHEN PATINDEX('%[^0-9]%',code) > 1
  THEN CAST(LEFT(code,PATINDEX('%[^0-9]%',code) - 1) AS int)
  ELSE 999999999999999
END,
CASE
  WHEN ISNUMERIC(code) = 1
  THEN NULL
  WHEN PATINDEX('%[^0-9]%',code) > 1
  THEN SUBSTRING(code,PATINDEX('%[^0-9]%',code),50)
  ELSE code
END
";

$cust_info = "select * from Tbl_CustomerMasterPerson2 where custnomid = '$custnomid'";
$db_lis->executeQuery($cust_info);
$cust_info = $db_lis->lastResults;
foreach($cust_info[0] as $key=>$value)
{
    $$key = $value;
}

////////// PK INFO ///////////////////////////////

if($custproccode == "KKB")
{
    $pk = "PK_KKB";
}
else if($custproccode == "KGS")
{
    $pk = "PK_KGS";
}
else {
    $pk = "TABLE_NYA_PASTI_TIDAK_TERIDENTIFIKASI_SAYA_BERANI_JAMIN";
}

$pk_info = "SELECT * FROM $pk WHERE CUSTNOMID = '$custnomid'";
$db_lis->executeQuery($pk_info);
$pk_info = $db_lis->lastResults;

foreach($pk_info[0] as $key=>$value)
{
    $$key = $value;
}

$datenow = getdate();
$no_pk = $NOMORPK;
$cif = $custaplno;

$cust_info = "SELECT * FROM DRAWDOWN WHERE custnomid = '$custnomid'";
$db_lis->executeQuery($cust_info);
$cust_info = $db_lis->lastResults;

if(count($cust_info)!=0)
{
	foreach($cust_info[0] as $key=>$value)
	{
		$$key = $value;
	}
}

$dinas_sel = isset($dinas) ? $dinas : "";
$sub_dinas_sel = isset($sub_dinas) ? $sub_dinas : "";
$sub_sub_dinas_sel = isset($sub_sub_dinas) ? $sub_sub_dinas : "";

$cust_dinas = "select distinct(L3COD1), L3DES1 from param_branch_dinas";
$db_lis->executeQuery($cust_dinas);
$cust_dinas = $db_lis->lastResults;

$marketing_kolektif = "SELECT * FROM param_marketing_kolektif ORDER BY LONOBR";
$db_lis->executeQuery($marketing_kolektif);
$marketing_kolektif = $db_lis->lastResults;

$get_golongan_penjamin = "SELECT * FROM param_golongan_penjamin $sort_by";
$db_lis->executeQuery($get_golongan_penjamin);
$get_golongan_penjamin = $db_lis->lastResults;

$get_kode_plan = "SELECT * FROM param_kode_plan $sort_by";
$db_lis->executeQuery($get_kode_plan);
$get_kode_plan = $db_lis->lastResults;

$get_sandi_realisasi = "SELECT * FROM param_sandi_realisasi $sort_by";
$db_lis->executeQuery($get_sandi_realisasi);
$get_sandi_realisasi = $db_lis->lastResults;

$get_sifat_kredit = "SELECT * FROM param_sifat_kredit $sort_by";
$db_lis->executeQuery($get_sifat_kredit);
$get_sifat_kredit = $db_lis->lastResults;

$get_kode_lokasi = "SELECT * FROM param_kode_lokasi $sort_by";
$db_lis->executeQuery($get_kode_lokasi);
$get_kode_lokasi = $db_lis->lastResults;

$get_orientasi_penggunaan = "SELECT * FROM param_orientasi_penggunaan $sort_by";
$db_lis->executeQuery($get_orientasi_penggunaan);
$get_orientasi_penggunaan = $db_lis->lastResults;

$get_kategori_pengukuran = "SELECT * FROM param_kategori_pengukuran $sort_by";
$db_lis->executeQuery($get_kategori_pengukuran);
$get_kategori_pengukuran = $db_lis->lastResults;

$get_kategori_debitur = "SELECT * FROM param_kategori_debitur $sort_by";
$db_lis->executeQuery($get_kategori_debitur);
$get_kategori_debitur = $db_lis->lastResults;

$get_kategori_portofolio = "SELECT * FROM param_kategori_portofolio $sort_by";
$db_lis->executeQuery($get_kategori_portofolio);
$get_kategori_portofolio = $db_lis->lastResults;

$get_jenis_kredit = "SELECT * FROM param_jenis_kredit $sort_by";
$db_lis->executeQuery($get_jenis_kredit);
$get_jenis_kredit = $db_lis->lastResults;

$get_jenis_suku_bunga = "SELECT * FROM param_jenis_suku_bunga $sort_by";
$db_lis->executeQuery($get_jenis_suku_bunga);
$get_jenis_suku_bunga = $db_lis->lastResults;

$get_jenis_garansi = "SELECT * FROM param_jenis_garansi $sort_by";
$db_lis->executeQuery($get_jenis_garansi);
$get_jenis_garansi = $db_lis->lastResults;

$get_jaminan = "SELECT * FROM param_jaminan $sort_by";
$db_lis->executeQuery($get_jaminan);
$get_jaminan = $db_lis->lastResults;

$get_sektor_ekonomi = "SELECT * FROM param_sektor_ekonomi $sort_by";
$db_lis->executeQuery($get_sektor_ekonomi);
$get_sektor_ekonomi = $db_lis->lastResults;

$get_pupn = "SELECT * FROM param_pupn $sort_by";
$db_lis->executeQuery($get_pupn);
$get_pupn = $db_lis->lastResults;

$get_jenis_pinjaman = "SELECT * FROM param_jenis_pinjaman $sort_by";
$db_lis->executeQuery($get_jenis_pinjaman);
$get_jenis_pinjaman = $db_lis->lastResults;

$get_golongan_debitur = "SELECT * FROM param_golongan_debitur $sort_by";
$db_lis->executeQuery($get_golongan_debitur);
$get_golongan_debitur = $db_lis->lastResults;

$get_hubungan_dengan_bank = "SELECT * FROM param_hubungan_dengan_bank $sort_by";
$db_lis->executeQuery($get_hubungan_dengan_bank);
$get_hubungan_dengan_bank = $db_lis->lastResults;

$get_status_debitur = "SELECT * FROM param_status_debitur $sort_by";
$db_lis->executeQuery($get_status_debitur);
$get_status_debitur = $db_lis->lastResults;

$get_lembaga_pemeringkat = "SELECT * FROM param_lembaga_pemeringkat $sort_by";
$db_lis->executeQuery($get_lembaga_pemeringkat);
$get_lembaga_pemeringkat = $db_lis->lastResults;

$get_tipe_kredit_khusus = "SELECT * FROM param_tipe_kredit_khusus $sort_by";
$db_lis->executeQuery($get_tipe_kredit_khusus);
$get_tipe_kredit_khusus = $db_lis->lastResults;

$get_jenis_asuransi = "SELECT * FROM param_jenis_asuransi $sort_by";
$db_lis->executeQuery($get_jenis_asuransi);
$get_jenis_asuransi = $db_lis->lastResults;

$get_jenis_pengikat = "SELECT * FROM param_jenis_pengikat $sort_by";
$db_lis->executeQuery($get_jenis_pengikat);
$get_jenis_pengikat = $db_lis->lastResults;

$get_negara_pihak_pemohon = "SELECT * FROM param_negara_pihak_pemohon $sort_by";
$db_lis->executeQuery($get_negara_pihak_pemohon);
$get_negara_pihak_pemohon = $db_lis->lastResults;

?>
<div align="right" style="padding-top: 10px; padding-right: 217px; padding-bottom: 20px">
<?
require ("../../requirepage/btnbacktoflow.php");
?>
</div>
<?

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FORM</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.6.4.min.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

	function changecabang() {
		var a = "CABANG";
		var cabang=$("#cabang_dinas").val();

		$.ajax({
			type: "GET",
			url: "ajax_input.php",
			data: "a="+a+"&cabang="+cabang+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{
				//alert(response);
				$("#tddinas").html(response);
			}
		});
	}

	function otf(param, param2, param3) {
		var a = "CABANG";
		var cabang=$("#cabang_dinas").val();

		$.ajax({
			type: "GET",
			url: "ajax_input.php",
			data: "a="+a+"&cabang="+cabang+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{
				//alert(response);
				$("#tddinas").html(response);

				$("#dinas").val(param);

				var a = "DINAS";
				var cabang=$("#cabang_dinas").val();
				var dinas=$("#dinas").val();
				$.ajax({
					type: "GET",
					url: "ajax_input.php",
					data: "a="+a+"&cabang="+cabang+"&dinas="+dinas+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{
						$("#tdsubdinas").html(response);

						$("#sub_dinas").val(param2);

						var a = "SUBDINAS";
						var cabang=$("#cabang_dinas").val();
						var dinas=$("#dinas").val();
						var subdinas=$("#sub_dinas").val();
						$.ajax({
							type: "GET",
							url: "ajax_input.php",
							data: "a="+a+"&cabang="+cabang+"&dinas="+dinas+"&subdinas="+subdinas+"&random="+ <?php echo time(); ?> +"",
							success: function(response)
							{
								$("#tdsubsubdinas").html(response);

								$("#sub_sub_dinas").val(param3);
							}
						});
					}
				});

			}
		});
	}

	function changedinas() {
		var a = "DINAS";
		var cabang=$("#cabang_dinas").val();
		var dinas=$("#dinas").val();
		$.ajax({
			type: "GET",
			url: "ajax_input.php",
			data: "a="+a+"&cabang="+cabang+"&dinas="+dinas+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{
				$("#tdsubdinas").html(response);
			}
		});
	}

	function changesubdinas() {
		var a = "SUBDINAS";
		var cabang=$("#cabang_dinas").val();
		var dinas=$("#dinas").val();
		var subdinas=$("#sub_dinas").val();
		$.ajax({
			type: "GET",
			url: "ajax_input.php",
			data: "a="+a+"&cabang="+cabang+"&dinas="+dinas+"&subdinas="+subdinas+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{
				$("#tdsubsubdinas").html(response);
			}
		});
	}
</script>
</head>
<body style="margin-bottom: 150px;">

<div align="center" style="width:1000px;margin:auto;height:100%;" >
<form action="form_input_do.php" method="post" >
<table align="center" style="width:90%;border:1px solid black;padding:10px;margin-bottom: 150px;" class="input">
<tr>
<td colspan="3" align="center"><strong>Input</strong></td>
</tr>
<tr>
<td colspan="3" align="center">&nbsp;</td>
</tr>
<tr>
<td width="30%">CIF</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="cif" name="cif" value="<?=$cif;?>" maxlength="50" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Nomor PK</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="no_pk" name="no_pk" value="<?=$no_pk;?>" maxlength="25" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Nomor Urut PK</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="no_urut_pk" name="no_urut_pk" value="<?=isset($no_urut_pk) ? $no_urut_pk : "001";?>" maxlength="30" style="width:500px;" /></td>
</tr>

<tr>
<td width="30%">Kode Produk Pinjaman</td>
<td width="5%"> : </td>
<td width="50%"><select type="text" id="kode_produk_pinjaman" name="kode_produk_pinjaman" value="<?=isset($kode_produk_pinjaman) ? $kode_produk_pinjaman : "";?>" maxlength="5" style="width:500px;" >
<option value="">-- Pilih Produk --</option>
<?
for($x=0;$x<count($get_kode_plan);$x++)
{
    $code = $get_kode_plan[$x]['code'];
    $show = $get_kode_plan[$x]['attribute'];
    ?>
    <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
    <?
}
?>
</select>
</td>
</tr>

<tr>
<td width="30%">Cabang</td>
<td width="5%"> : </td>
<td width="50%"><select type="text" id="cabang_dinas" name="cabang_dinas" value="<?=isset($cabang_dinas) ? $cabang_dinas : "";?>" maxlength="5" style="width:500px;" onchange="changecabang()">
<option value="">-- Pilih Cabang --</option>
<?for($x=0;$x<count($cust_dinas);$x++){
	$selected = "";
	isset($cabang_dinas) ? $cabang_dinas : "";
	if($cust_dinas[$x]['L3COD1'] == $cabang_dinas){ $selected = "selected";}
?>
<option value="<?=$cust_dinas[$x]['L3COD1']?>" <?=$selected?>  ><?=$cust_dinas[$x]['L3COD1']?> - <?=$cust_dinas[$x]['L3DES1']?></option>
<?}?>
</select></td>
</tr>
<tr>
<td width="30%">Dinas</td>
<td width="5%"> : </td>
<td width="50%" id="tddinas"><select type="text" id="dinas" name="dinas" value="<?=isset($dinas) ? $dinas : "";?>" maxlength="5" style="width:500px;">
<option value="">-- Pilih Cabang dahulu --</option>
</select></td>
</tr>
<tr>
<td width="30%">Sub Dinas</td>
<td width="5%"> : </td>
<td width="50%" id="tdsubdinas"><select type="text" id="sub_dinas" name="sub_dinas" value="<?=isset($sub_dinas) ? $sub_dinas : "";?>" maxlength="5" style="width:500px;">
<option value="">-- Pilih Dinas dahulu --</option>
</select></td>
</tr>
<tr>
<td width="30%">Sub - sub Dinas</td>
<td width="5%"> : </td>
<td width="50%" id="tdsubsubdinas"><select type="text" id="sub_sub_dinas" name="sub_sub_dinas" value="<?=isset($sub_sub_dinas) ? $sub_sub_dinas : "";?>" maxlength="5" style="width:500px;">
<option value="">-- Pilih Sub Dinas dahulu --</option>
</select></td></tr>

<tr>
<td width="30%">Marketing Officer</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="marketing_officer" name="marketing_officer" value="<?=isset($marketing_officer) ? $marketing_officer : "";?>" style="width:500px;">
        <option value="">-- Marketing Officer --</option>
        <?
        for($x=0;$x<count($marketing_kolektif);$x++)
        {
            $code = $marketing_kolektif[$x]['LONOOF'];
            $show = $marketing_kolektif[$x]['LODSNM'];
            $brnc = $marketing_kolektif[$x]['LONOBR'];
            ?>
            <option value="<?=$code;?>"><?=$brnc;?> - <?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Kolektif Officer</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="kolektif_officer" name="kolektif_officer" value="<?=isset($kolektif_officer) ? $kolektif_officer : "";?>" style="width:500px;">
        <option value="">-- Kolektif Officer --</option>
        <?
        for($x=0;$x<count($marketing_kolektif);$x++)
        {
            $code = $marketing_kolektif[$x]['LONOOF'];
            $show = $marketing_kolektif[$x]['LODSNM'];
            $brnc = $marketing_kolektif[$x]['LONOBR'];
            ?>
            <option value="<?=$code;?>"><?=$brnc;?> - <?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Frekwensi Pembayaran</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="frekwensi_pembayaran" name="frekwensi_pembayaran" value="<?=isset($frekwensi_pembayaran) ? $frekwensi_pembayaran : "1";?>" maxlength="2" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Grace Period</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="grace_period" name="grace_period" value="<?=isset($grace_period) ? $grace_period : "1";?>" maxlength="5" style="width:500px;" /></td>
</tr>
<!--
<tr>
<td width="30%">Status Rekening</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="status_rekening" name="status_rekening" value="<?=isset($status_rekening) ? $status_rekening : "1";?>" maxlength="1" style="width:500px;" /></td>
</tr>
-->
<tr>
<td width="30%">Jumlah Kontrak</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="jumlah_kontrak" name="jumlah_kontrak" value="<?=isset($jumlah_kontrak) ? $jumlah_kontrak : "0";?>" maxlength="20" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Sifat Kredit</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="sifat_kredit" name="sifat_kredit" value="<?=isset($sifat_kredit) ? $sifat_kredit : "";?>" style="width:500px;">
        <option value="">-- Sifat Kredit --</option>
        <?
        for($x=0;$x<count($get_sifat_kredit);$x++)
        {
            $code = $get_sifat_kredit[$x]['code'];
            $show = $get_sifat_kredit[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>
<tr>
<td width="30%">Kode Lokasi</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="kode_lokasi" name="kode_lokasi" value="<?=isset($kode_lokasi) ? $kode_lokasi : "";?>" style="width:500px;">
        <option value="">-- Kode Lokasi --</option>
        <?
        for($x=0;$x<count($get_kode_lokasi);$x++)
        {
            $code = $get_kode_lokasi[$x]['code'];
            $show = $get_kode_lokasi[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>
<tr>
<td width="30%">Orientasi Penggunaan</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="orientasi_penggunaan" name="orientasi_penggunaan" value="<?=isset($orientasi_penggunaan) ? $orientasi_penggunaan : "";?>" style="width:500px;">
        <option value="">-- Orientasi Penggunaan --</option>
        <?
        for($x=0;$x<count($get_orientasi_penggunaan);$x++)
        {
            $code = $get_orientasi_penggunaan[$x]['code'];
            $show = $get_orientasi_penggunaan[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>
<tr>
<td width="30%">Kategori Pengukuran</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="kategori_pengukuran" name="kategori_pengukuran" value="<?=isset($kategori_pengukuran) ? $kategori_pengukuran : "";?>" style="width:500px;">
        <option value="">-- Kategori Pengukuran --</option>
        <?
        for($x=0;$x<count($get_kategori_pengukuran);$x++)
        {
            $code = $get_kategori_pengukuran[$x]['code'];
            $show = $get_kategori_pengukuran[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>
<tr>
<td width="30%">Kategori Debitur</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="kategori_debitur" name="kategori_debitur" value="<?=isset($kategori_debitur) ? $kategori_debitur : "";?>" style="width:500px;">
        <option value="">-- Kategori Debitur --</option>
        <?
        for($x=0;$x<count($get_kategori_debitur);$x++)
        {
            $code = $get_kategori_debitur[$x]['code'];
            $show = $get_kategori_debitur[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>
<tr>
<td width="30%">Kategori Portofolio</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="kategori_portofolio" name="kategori_portofolio" value="<?=isset($kategori_portofolio) ? $kategori_portofolio : "";?>" style="width:500px;">
        <option value="">-- Kategori Portofolio --</option>
        <?
        for($x=0;$x<count($get_kategori_portofolio);$x++)
        {
            $code = $get_kategori_portofolio[$x]['code'];
            $show = $get_kategori_portofolio[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">Jenis Kredit</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="jenis_kredit" name="jenis_kredit" value="<?=isset($jenis_kredit) ? $jenis_kredit : "";?>" style="width:500px;">
        <option value="">-- Jenis Kredit --</option>
        <?
        for($x=0;$x<count($get_jenis_kredit);$x++)
        {
            $code = $get_jenis_kredit[$x]['code'];
            $show = $get_jenis_kredit[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">Jenis Suku Bunga</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="jenis_suku_bunga" name="jenis_suku_bunga" value="<?=isset($jenis_suku_bunga) ? $jenis_suku_bunga : "";?>" style="width:500px;">
        <option value="">-- Jenis Suku Bunga --</option>
        <?
        for($x=0;$x<count($get_jenis_suku_bunga);$x++)
        {
            $code = $get_jenis_suku_bunga[$x]['code'];
            $show = $get_jenis_suku_bunga[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">Jenis Garansi</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="jenis_garansi" name="jenis_garansi" value="<?=isset($jenis_garansi) ? $jenis_garansi : "";?>" style="width:500px;">
        <option value="">-- Jenis Garansi --</option>
        <?
        for($x=0;$x<count($get_jenis_garansi);$x++)
        {
            $code = $get_jenis_garansi[$x]['code'];
            $show = $get_jenis_garansi[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">Jaminan</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="jaminan" name="jaminan" value="<?=isset($jaminan) ? $jaminan : "";?>" style="width:500px;">
        <option value="">-- Jaminan --</option>
        <?
        for($x=0;$x<count($get_jaminan);$x++)
        {
            $code = $get_jaminan[$x]['code'];
            $show = $get_jaminan[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">Sektor Ekonomi</td>
<td width="5%"> : </td>
<td width="50%">

    <select type="text" id="sektor_ekonomi" name="sektor_ekonomi" value="<?=isset($sektor_ekonomi) ? $sektor_ekonomi : "";?>" style="width:500px;">
        <option value="">-- Sektor Ekonomi --</option>
        <?
        for($x=0;$x<count($get_sektor_ekonomi);$x++)
        {
            $code = $get_sektor_ekonomi[$x]['code'];
            $show = $get_sektor_ekonomi[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>

</td>
</tr>

<tr>
<td width="30%">PUPN / Non PUPN</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="pupn" name="pupn" value="<?=isset($pupn) ? $pupn : "";?>" style="width:500px;">
        <option value="">-- PUPN / Non PUPN --</option>
        <?
        for($x=0;$x<count($get_pupn);$x++)
        {
            $code = $get_pupn[$x]['code'];
            $show = $get_pupn[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Golongan Penjamin</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="golongan_penjamin" name="golongan_penjamin" value="<?=isset($golongan_penjamin) ? $golongan_penjamin : "";?>" style="width:500px;">
        <option value="">-- Golongan Penjamin --</option>
        <?
        for($x=0;$x<count($get_golongan_penjamin);$x++)
        {
            $code = $get_golongan_penjamin[$x]['code'];
            $show = $get_golongan_penjamin[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>


<tr>
<td width="30%">Jenis Pinjaman</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="jenis_pinjaman" name="jenis_pinjaman" value="<?=isset($jenis_pinjaman) ? $jenis_pinjaman : "";?>" style="width:500px;">
        <option value="">-- Jenis Pinjaman --</option>
        <?
        for($x=0;$x<count($get_jenis_pinjaman);$x++)
        {
            $code = $get_jenis_pinjaman[$x]['code'];
            $show = $get_jenis_pinjaman[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Sandi Realisasi</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="sandi_realisasi" name="sandi_realisasi" value="<?=isset($sandi_realisasi) ? $sandi_realisasi : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Sandi Realisasi --</option>
        <?
        for($x=0;$x<count($get_sandi_realisasi);$x++)
        {
            $code = $get_sandi_realisasi[$x]['code'];
            $show = $get_sandi_realisasi[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Golongan Debitur</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="golongan_debitur" name="golongan_debitur" value="<?=isset($golongan_debitur) ? $golongan_debitur : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Golongan Debitur --</option>
        <?
        for($x=0;$x<count($get_golongan_debitur);$x++)
        {
            $code = $get_golongan_debitur[$x]['code'];
            $show = $get_golongan_debitur[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Hubungan Dengan Bank</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="hubungan_dengan_bank" name="hubungan_dengan_bank" value="<?=isset($hubungan_dengan_bank) ? $hubungan_dengan_bank : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Hubungan Dengan Bank --</option>
        <?
        for($x=0;$x<count($get_hubungan_dengan_bank);$x++)
        {
            $code = $get_hubungan_dengan_bank[$x]['code'];
            $show = $get_hubungan_dengan_bank[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Status Debitur</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="status_debitur" name="status_debitur" value="<?=isset($status_debitur) ? $status_debitur : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Status Debitur --</option>
        <?
        for($x=0;$x<count($get_status_debitur);$x++)
        {
            $code = $get_status_debitur[$x]['code'];
            $show = $get_status_debitur[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Lbg. Pemeringkat</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="lembaga_pemeringkat" name="lembaga_pemeringkat" value="<?=isset($lembaga_pemeringkat) ? $lembaga_pemeringkat : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Lbg. Pemeringkat --</option>
        <?
        for($x=0;$x<count($get_lembaga_pemeringkat);$x++)
        {
            $code = $get_lembaga_pemeringkat[$x]['code'];
            $show = $get_lembaga_pemeringkat[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Negara Pihak Pemohon</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="negara_pihak_pemohon" name="negara_pihak_pemohon" value="<?=isset($negara_pihak_pemohon) ? $negara_pihak_pemohon : "";?>" style="width:500px;" >
        <option value="">-- Negara Pihak Pemohon --</option>
        <?
        for($x=0;$x<count($get_negara_pihak_pemohon);$x++)
        {
            $code = $get_negara_pihak_pemohon[$x]['code'];
            $show = $get_negara_pihak_pemohon[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Tipe Kredit Khusus</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="tipe_kredit_khusus" name="tipe_kredit_khusus" value="<?=isset($tipe_kredit_khusus) ? $tipe_kredit_khusus : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Tipe Kredit Khusus --</option>
        <?
        for($x=0;$x<count($get_tipe_kredit_khusus);$x++)
        {
            $code = $get_tipe_kredit_khusus[$x]['code'];
            $show = $get_tipe_kredit_khusus[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Jenis Asuransi</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="jenis_asuransi" name="jenis_asuransi" value="<?=isset($jenis_asuransi) ? $jenis_asuransi : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Jenis Asuransi --</option>
        <?
        for($x=0;$x<count($get_jenis_asuransi);$x++)
        {
            $code = $get_jenis_asuransi[$x]['code'];
            $show = $get_jenis_asuransi[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>

<tr>
<td width="30%">Jenis Pengikat</td>
<td width="5%"> : </td>
<td width="50%">
    <select type="text" id="jenis_pengikat" name="jenis_pengikat" value="<?=isset($jenis_pengikat) ? $jenis_pengikat : "";?>" maxlength="5" style="width:500px;" >
        <option value="">-- Jenis Pengikat --</option>
        <?
        for($x=0;$x<count($get_jenis_pengikat);$x++)
        {
            $code = $get_jenis_pengikat[$x]['code'];
            $show = $get_jenis_pengikat[$x]['attribute'];
            ?>
            <option value="<?=$code;?>"><?=$code;?> - <?=$show;?></option>
            <?
        }
        ?>
    </select>
</td>
</tr>
<tr>
<td width="30%">Pengadilan Negeri</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="pengadilan_negeri" name="pengadilan_negeri" value="<?=isset($pengadilan_negeri) ? $pengadilan_negeri : "";?>" maxlength="30" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Denda Overdraft</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="denda_overdraft" name="denda_overdraft" value="<?=isset($denda_overdraft) ? $denda_overdraft : "0";?>" maxlength="10" style="width:500px;" /></td>
</tr>
<!--
<tr>
<td width="30%">Nomor SPPK</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="no_sppk" name="no_sppk" value="<?=isset($no_sppk) ? $no_sppk : "";?>" maxlength="30" style="width:500px;" /></td>
</tr>
-->
<tr>
<td width="30%">Uang Muka Rmh/Kend</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="uang_muka" name="uang_muka" value="<?=isset($uang_muka) ? $uang_muka : "0";?>" maxlength="20" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Biaya Asr KBKR/KRGN</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="asr_kbkr_krgn" name="asr_kbkr_krgn" value="<?=isset($asr_kbkr_krgn) ? $asr_kbkr_krgn : "0";?>" maxlength="20" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">Biaya Pengikatan JMN</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="pengikatan_jmn" name="pengikatan_jmn" value="<?=isset($pengikatan_jmn) ? $pengikatan_jmn : "0";?>" maxlength="20" style="width:500px;" /></td>
</tr>
<tr>
<td width="30%">BUPLN</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="bupln" name="bupln" value="<?=isset($bupln) ? $bupln : "0";?>" maxlength="30" style="width:500px;" /></td>
</tr>
<!--
<tr>
<td width="30%">Lap Stock Barang (Bulan)</td>
<td width="5%"> : </td>
<td width="50%"><input type="text" id="stock_barang" name="stock_barang" value="<?=isset($stock_barang) ? $stock_barang : "1";?>" maxlength="10" style="width:500px;" /></td>
</tr>
-->
<tr>
<td colspan="3" align="center">&nbsp;</td>
</tr>
<tr>
<td colspan="3" align="center"><input type="submit" id="btnsubmit" name="btnsubmit" value="Submit" class="buttonsaveflow"/></td>
</tr>
</table>

<? include("../../requirepage/hiddenfield.php"); ?>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){

	otf('<?=$dinas_sel;?>', '<?=$sub_dinas_sel;?>', '<?=$sub_sub_dinas_sel;?>');

})
</script>

</body>
</html>
