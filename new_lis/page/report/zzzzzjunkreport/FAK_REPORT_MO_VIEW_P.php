<?
	
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	$custnomid = $_REQUEST['custnomid'];
	$userid = $_REQUEST['userid'];
	$custproccode="";
	$custfullname="";
	$custshortname="";
	$custsex="";
	$custktpno="";
	$custktpexp="";
	$custboddate="";
	$custbodplace="";
	$custmothername="";
	$custeducode="";
	$custmarcode="";
	$custmarname="";
	$custjmltanggungan="";
	$custaddr="";
	$custrt="";
	$custrw="";
	$custkel="";
	$custkec="";
	$custcity="";
	$custzipcode="";
	$custtelp="";
	$custhp="";
	$custhomestatus="";
	$custhomeyearlong="";
	$custhomemonthlong="";
	$custaddrktp="";
	$custrtktp="";
	$custrwktp="";
	$custkelktp="";
	$custkecktp="";
	$custcityktp="";
	$custzipcodektp="";
	$custbustype="";
	$custbusname="";
	$custbusaddr="";
	$custbustelp="";
	$custbusnpwp="";
	$custbussiup="";
	$custbustdp="";
	$custbusyearlong="";
	$custbusmonthlong="";
	$custcreditstatus="";
	$custnomomsetcode="";
	$custnomperkenalan="";
	$custpropendapatan="";
	$custapldate="";

	$strsql="SELECT custproccode,custfullname,custshortname,custsex,custktpno,custapldate,
			custktpexp,custboddate,custbodplace,custmothername,custeducode,custmarcode,
			custmarname,custjmltanggungan,custaddr,custrt,custrw,custkel,custkec,custcity,
			custzipcode,custtelp,custhp,custhomestatus,custhomeyearlong,custhomemonthlong,custaddrktp,
			custrtktp,custrwktp,custkelktp,custkecktp,custcityktp,custzipcodektp,custbustype,custbusname,
			custbusaddr,custbustelp,custbusnpwp,custbussiup,custbustdp,custbusyearlong,custbusmonthlong,
			custcreditstatus,custnomomsetcode,custnomperkenalan,
			custpropendapatan
			from tbl_customermasterperson where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custproccode=$rows['custproccode'];
			$custfullname=$rows['custfullname'];
			$custshortname=$rows['custshortname'];
			$custsex=$rows['custsex'];
			$custktpno=$rows['custktpno'];
			$custktpexp=$rows['custktpexp'];
			$custboddate=$rows['custboddate'];
			$custbodplace=$rows['custbodplace'];
			$custmothername=$rows['custmothername'];
			$custeducode=$rows['custeducode'];
			$custmarcode=$rows['custmarcode'];
			$custmarname=$rows['custmarname'];
			$custjmltanggungan=$rows['custjmltanggungan'];
			$custaddr=$rows['custaddr'];
			$custrt=$rows['custrt'];
			$custrw=$rows['custrw'];
			$custkel=$rows['custkel'];
			$custkec=$rows['custkec'];
			$custcity=$rows['custcity'];
			$custzipcode=$rows['custzipcode'];
			$custtelp=$rows['custtelp'];
			$custhp=$rows['custhp'];
			$custhomestatus=$rows['custhomestatus'];
			$custhomeyearlong=$rows['custhomeyearlong'];
			$custhomemonthlong=$rows['custhomemonthlong'];
			$custaddrktp=$rows['custaddrktp'];
			$custrtktp=$rows['custrtktp'];
			$custrwktp=$rows['custrwktp'];
			$custkelktp=$rows['custkelktp'];
			$custkecktp=$rows['custkecktp'];
			$custcityktp=$rows['custcityktp'];
			$custzipcodektp=$rows['custzipcodektp'];
			$custbustype=$rows['custbustype'];
			$custbusname=$rows['custbusname'];
			$custbusaddr=$rows['custbusaddr'];
			$custbustelp=$rows['custbustelp'];
			$custbusnpwp=$rows['custbusnpwp'];
			$custbussiup=$rows['custbussiup'];
			$custbustdp=$rows['custbustdp'];
			$custbusyearlong=$rows['custbusyearlong'];
			$custbusmonthlong=$rows['custbusmonthlong'];
			$custcreditstatus=$rows['custcreditstatus'];
			$custnomomsetcode=$rows['custnomomsetcode'];
			$custnomperkenalan=$rows['custnomperkenalan'];
			$custpropendapatan=$rows['custpropendapatan'];
			$custapldate=$rows['custapldate'];
		}
	}
	
	$lifeinsurance="";
	$otherlifeinsurance="";
	$lostinsurance="";
	$otherlostinsurance="";
	$strsql="select * from Tbl_Customerinsurance where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$lifeinsurance=$rows['code_life'];
			$otherlifeinsurance=$rows['other_life'];
			$lostinsurance=$rows['code_loss'];
			$otherlostinsurance=$rows['other_loss'];
		}
	}
	
	$username="";
	$user_branch_code="";
	$strsql = "select USER_NAME,user_branch_code from tbl_se_user where user_id='$userid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$username= $rows['USER_NAME'];
			$user_branch_code= $rows['user_branch_code'];
		}
	}
	
	
	$branchname="";
	$strsql = "select * from Tbl_Branch where branch_code='$user_branch_code'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$branchname= $rows['branch_name'];
		}
	}
	
	
	
	
	$usaha_jenisusaha="";
	$usaha_sektorindustri="";
	$usaha_lamausahamonth="";
	$usaha_lamausahayear="";
	$strsql = "select * from Tbl_LKCDUsaha where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$usaha_jenisusaha= $rows['usaha_jenisusaha'];
			$usaha_sektorindustri= $rows['usaha_sektorindustri'];
			$usaha_lamausahamonth= $rows['usaha_lamausahamonth'];
			$usaha_lamausahayear= $rows['usaha_lamausahayear'];
		}
	}
	

?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<form id="frm" name="frm" method="post">
			<div class="divcenter">
				<table id="tblform" border="1" style ="width:900px; border-color:black;">
					<tr>
						<td colspan="2">
							<table>
								<tr>
									<td>
										<div style="border:black 1px solid; font-size:7pt;">&nbsp; Form 1B &nbsp;</div>
									</td>
									<td>&nbsp;</td>
								</tr>					
								<tr>
									<td>&nbsp;</td>
									<td><h3>FORMULIR APLIKASI KREDIT &#40; PERORANGAN &#41;</h3></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table>
								<tr>
									<td style="width:100px;">Produk</td>
									<td><?echo $custproccode;?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="width:50%" valign="top" rowspan="2">
							<table class="tbl100">
								<tr>
									<td colspan="2"><h4>DATA PEMOHON</h4></td>
								</tr>
								<tr>
									<td colspan="2">Nama <i>&#40;sesuai KTP&#41;</i></td>
								</tr>
								<tr>
									<td colspan="2"><?echo $custfullname;?></td>
								</tr>
								<tr>
									<td>Nama Panggilan</td>
									<td  style="width:250px;"><?echo $custshortname;?></td>
								</tr>
								<tr>
									<td>Jenis Kelamin</td>
									<td>
										<?
											$gender ="";
											if($custsex=="1")
											{
												$gender='pria';
												
											}
											else if($custsex=="2")
											{
												$gender='waninta';
											}
										echo $gender;
										?>
									</td>
								</tr>
								<tr>
									<td>No. KTP</td>
									<td><?echo $custktpno;?></td>
								</tr>
								<tr>
									<td>KTP berlaku sampai</td>
									<td><?echo $custktpexp;?></td>
								</tr>
								<tr>
									<td>Tempat</td>
									<td><?echo $custbodplace;?></td>
								</tr>
								<tr>
									<td>Taggal lahir</td>
									<td><?echo $custboddate;?></td>
								</tr>
								<tr>
									<td>Nama Ibu Kandung</td>
									<td><?echo $custmothername;?></td>
								</tr>
								<tr>
									<td>Pendidikan akhir</td>
									<td>
											<?
												$strsql = "select * from Tblpendidikan where edu_code='$custeducode'";
												$sqlcon = sqlsrv_query($conn, $strsql);
												if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
												if(sqlsrv_has_rows($sqlcon))
												{
													while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
													{
														echo $rows['edu_name'];
													}
												}
											?>
									</td>
								</tr>
								<tr>
									<td>Status Perkawinan</td>
									<td>
										<?
											$statusperkawinan ="";
											if($custmarcode=="1")
											{
												$statusperkawinan='Lajang';
											}
											else if($custmarcode=="2")
											{
												$statusperkawinan='Menikah';
											}
											else if($custmarcode=="3")
											{
												$statusperkawinan='Duda &#47; Janda';
											}
											echo $statusperkawinan;
										?>
									</td>
								</tr>
								<tr>
									<td <? if($custmarcode!="2"){echo 'style="display:none";"';}?> id="td_relation">Nama suami&#47;istri</td>
									<td <? if($custmarcode!="2"){echo 'style="display:none";"';}?> id="td__relation" style="width:250px;"><?echo $custmarname;?>
									</td>
								</tr>
								<tr>
									<td>Jumlah Tanggungan</td>
									<td><?echo $custjmltanggungan;?> &nbsp; Orang</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2"><h3>Keterangan Tempat Tinggal</h3></td>
								</tr>
								<tr>
									<td colspan="2">Alamat Tempat Tinggal Saat Ini</td>
								</tr>
								<tr>
									<td colspan="2"><?echo $custaddr;?></td>
								</tr>
								<tr>
									<td>RT&#47;RW</td>
									<td style="width:250px;"><?echo $custrt;?>&#47;<?echo $custrw;?>
									</td>
								</tr>
								<tr>
									<td>Kecamatan</td>
									<td><?echo $custkec;?></td>
								</tr>
								<tr>
									<td>Kelurahan</td>
									<td><?echo $custkel;?></td>
								</tr>
								<tr>
									<td>Kota&#47;Kabupaten</td>
									<td><?echo $custcity;?></td>
								</tr>
								<tr>
									<td>Kode Pos</td>
									<td><?echo $custzipcode;?></td>
								</tr>
								<tr>
									<td>Telepon Rumah</td>
									<td><?echo $custtelp;?></td>
								</tr>
								<tr>
									<td>No HP</td>
									<td><?echo $custhp;?></td>
								</tr>
								<tr>
									<td>Status Tempat tinggal saat ini</td>
									<td>
										<?
											$strsql = "select * from TblStatusRumah where status_code='$custhomestatus'";
											$sqlcon = sqlsrv_query($conn, $strsql);
											if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
											if(sqlsrv_has_rows($sqlcon))
											{
												while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
												{
													echo $rows['status_name'];
												}
											}
										?>
									
									</td>
								</tr>
								<tr>
									<td>Lama Menempati Rumah</td>
									<td><?echo $custhomeyearlong?> &nbsp;&nbsp;Tahun
										&nbsp;&nbsp;&nbsp;&nbsp;
										 <?echo $custhomemonthlong?> &nbsp;&nbsp;Bulan
									</td>
								</tr>							
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2">Data Sesuai KTP</td>
								</tr>
								<tr>
									<td colspan="2" id="statusflag" colspan="2">
										<div>
											<table class="tbl100">
												<tr>
													<td colspan="2">Alamat Tempat Tinggal Saat Ini</td>
												</tr>
												<tr>
													<td colspan="2"><?echo $custaddrktp?></td>
												</tr>
												<tr>
													<td >RT&#47;RW</td>
													<td style="width:250px;">
													<?echo $custrtktp?>
														&#47;
														<?echo $custrwktp?>
													</td>
												</tr>
												<tr>
													<td>Kecamatan</td>
													<td><?echo $custkecktp?>
													</td>
												</tr>
												<tr>
													<td>Kelurahan</td>
													<td><?echo $custkelktp?></td>
												</tr>
												<tr>
													<td>Kota&#47;Kabupaten</td>
													<td><?echo $custcityktp?></td>
												</tr>
												<tr>
													<td>Kode Pos</td>
													<td><?echo $custzipcodektp?></td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</table>
						</td>
						<td style="width:50%" valign="top">
							<div style="width:100%;">
								<table class="tbl100">
									<tr>
										<td colspan="2"><h4>INFORMASI USAHA</h4></td>
									</tr>
									<tr>
										<td style="width:170px;">Nama Usaha&#47;Toko</td>
										<td style="width:250px;"><?echo $custbusname;?></td>
									</tr>
									<tr>
										<td>Jenis Usaha</td>
										<td><?echo $usaha_jenisusaha;?></td>
									</tr>
									<tr>
										<td colspan="2">Alamat Usaha</td>
									</tr>
									<tr>
										<td colspan="2"><?echo $custbusaddr;?></td>
									</tr>
									<tr>
										<td>Telepon Tempat Usaha</td>
										<td><?echo $custbustelp?></td>
									</tr>
									<tr>
										<td>NPWP</td>
										<td><?echo $custbusnpwp;?></td>
									</tr>
									<tr>
										<td>SIUP</td>
										<td><?echo $custbussiup;?></td>
									</tr>
									<tr>
										<td>TDP</td>
										<td><?echo $custbustdp;?></td>
									</tr>
									<tr>
										<td>Lama Usaha</td>
										<td><?echo $usaha_lamausahayear?>&nbsp;&nbsp; Tahun
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<?echo $usaha_lamausahamonth?> &nbsp;&nbsp; Bulan
										</td>
									</tr>
									
									<tr>
										<td colspan="2">
											<div>
												<table class="tbl100">
													<tr>
														<td colspan="2"><h3>Detail Fasilitas</h3></td>
													</tr>
													<tr>
														<td>Detail Fasilitas</td>
														<td style="width:250px;">
																<?
																	$strsql = "select * from TblCreditStatus where status_code='$custcreditstatus'";
																	$sqlcon = sqlsrv_query($conn, $strsql);
																	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
																	if(sqlsrv_has_rows($sqlcon))
																	{
																		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
																		{
																			echo $rows['status_name'];
																		}
																	}
																?>
														</td>
													</tr>
													<?
													$totalpalfond=0;
													$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,
																c.produk_type_description,a.custcreditplafond,a.custcreditlong 
																from tbl_CustomerFacility a 
																left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
																left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
																where a.custnomid ='".$custnomid."'";
															
													$sqlcon = sqlsrv_query($conn, $strsql);
													if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
													if(sqlsrv_has_rows($sqlcon))
													{
														while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
														{
															$totalpalfond+=$rows['custcreditplafond'];
															echo '
																<tr>
																	<td>Tujuan Pengajuan Kredit</td>
																	<td>'.$rows['credit_need_name'].'</td>
																</tr>
																<tr>
																	<td>Jenis Fasilitas</td>
																	<td>'.$rows['produk_type_description'].'</td>
																</tr>
																<tr>
																	<td>Plafond (IDR)</td>
																	<td>'.numberFormat($rows['custcreditplafond']).'</td>
																</tr>
																<tr>
																	<td>Jangka Waktu</td>
																	<td>'.$rows['custcreditlong'].'</td>
																</tr>
																<tr>
																	<td colspan="3">&nbsp;</td>
																</tr>
															';
														}	
													}
													?>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<table class="tbl100">
								<table class="tbl100">
								<tr>
									<td colspan="2"><h3>Asuransi</h3></td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:justify;">
										Sehubungan dengan fasilitas yang akan diperoleh, maka Cadeb tidak berkeberatan 
										apabila agunan maupun jiwa Cadeb dicover oleh salah satu rekanan asuransi Bank Mega.
										<br/>
										Sebagai berikut:
									</td>
								</tr>
								<tr>
									<td colspan="2" style="font-size:12px; font-weight:bold;">Asuransi Jiwa Kredit</td>
								</tr>
								<tr>
									<td colspan="2">
											<?
												$strsql = "select * from tbl_insurance where code like '%$lifeinsurance%'";
												$sqlcon = sqlsrv_query($conn, $strsql);
												if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
												if(sqlsrv_has_rows($sqlcon))
												{
													while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
													{
														echo $rows['name'];
													}
												}
											?>
									</td>
								</tr>
								<tr>
									<td <?if($lifeinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?> id="other_lifeinsurance" colspan="2">
										Jika Lainnya, Sebutkan :
									</td>
								</tr>
								<tr>
									<td <?if($lifeinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?>  id="other__lifeinsurance" colspan="2"><?echo $otherlifeinsurance?>
									</td>
								</tr>
								<tr>
									<td colspan="2" style="font-size:12px; font-weight:bold;">Asuransi Kerugian</td>
								</tr>
								<tr>
									<td colspan="2">
											<?
												$strsql = "select * from tbl_insurance where code like '%$lostinsurance%'";
												$sqlcon = sqlsrv_query($conn, $strsql);
												if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
												if(sqlsrv_has_rows($sqlcon))
												{
													while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
													{
														echo $rows['name'];
													}
												}
											?>
									</td>
								</tr>
								<tr>
									<td <?if($lostinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?> id="other_lostinsurance" colspan="2">
										Jika Lainnya, Sebutkan :
									</td>
								</tr>
								<tr>
									<td <?if($lostinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?>  id="other__lostinsurance" colspan="2"><?echo $otherlostinsurance?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="tbl100">
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:justify;">
										Semua informasi dalam formulir ini adalah lengkap dan benar. Saya/ Kami mengetahui bahwa Saya/ Kami tidak akan memberi imbalan dalam bentuk apapun kepada pihak Bank Mega atau pihak ketiga jika permohonan pinjaman ini disetujui. Saya/Kami menyetujui bahwa data yang Saya/Kami berikan menjadi milik Bank Mega. Dengan menandatangani formulir ini,  Saya/ Kami memberi kuasa kepada Bank Mega untuk memeriksa semua kebenaran data dengan cara bagaimanapun dan atau menghubungi sumber manapun yang layak menurut Bank Mega, termasuk dalam rangka mempergunakan data dan informasi tersebut untuk kegiatan promosi ataupun untuk tujuan komersial lainnya. Bank Mega telah memberikan penjelasan yang cukup mengenai karakteristik Kredit Usaha kecil yang akan Saya/ Kami manfaatkan, dan Saya/ Kami mengerti bahwa Bank Mega berhak menolak permohonan ini tanpa harus memberikan alasan apapun pada Saya/ Kami, dan semua dokumen yang telah diserahkan tidak akan dikembalikan.
									</td>
								</tr>
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2" style="text-align:justify;">
										<table class="tbl100">
											<tr>
												<td style="width:300px;">
													<div style="border:1px solid black; width:300px; padding-top:200px; font-style:italic;">
														Tanggal&#40;tgl&#47;bln&#47;thn&#41;
													</div>
												</td>
												<td valign="bottom">
													Tanda Tangan & Data pemohon
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="tdpadding">
							<table class="tbl100">
								<tr>
									<td style="font-weight:bold;">DIISI OLEH BANK</td>
									<td>Bagaimana Perkenalan Terjadi?</td>
									<td colspan="3">
											<?
												$strsql = "select * from Tbl_introduce where code='$custnomperkenalan'";
												$sqlcon = sqlsrv_query($conn, $strsql);
												if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
												if(sqlsrv_has_rows($sqlcon))
												{
													while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
													{
														echo $rows['name'];
													}
												}
											?>
									</td>
								</tr>
								<tr>
									<td style="width:150px;">Tanggal</td>
									<td style="width:195px;"><?echo $custapldate?></td>
									<td>&nbsp;</td>
									<td style="width:200px;">&nbsp;</td>
									<td style="width:130px;">&nbsp;</td>
								</tr>
								<tr>
									<td>No. Applikasi</td>
									<td><?echo $custnomid;?></td>
									<td colspan="2" style="font-size:7pt; font-style:italic;">Dengan menandatangani formulir ini, Saya menyatakan bahwa Saya telah</td>
									<td rowspan="3">
										<div valign="buttom" style="border:1px solid black; width:130px; padding-top:60px; text-align:center; font-size:10px;" >
											Tanda Tangan
											<br />
											MO/CMO
										</div>
									</td>
								</tr>
								<tr>
									<td>Cabang</td>
									<td><?echo $branchname;?></td>
									<td colspan="2" style="font-size:7pt; font-style:italic;">melakukan verifikasi dan konfirmasi terhadap kelayakan data Pemohon.</td>
								</tr>
								<tr>
									<td>Nama MO/CMO</td>
									<td><? echo $username;?></td>
									<td style="font-size:7pt; font-style:italic;">Saya merekomendasikan aplikasi ini</td>
									<td style="font-size:7pt; font-style:italic;">
										<?
											$recomen="";
											if($custpropendapatan=="1")
											{
												$recomen='Disetujui';
											}
											else if ($custpropendapatan=="2")
											{
												$recomen='Tidak Di setujui';
											}
											echo $recomen;
										?>
									</td>
								</tr>
							</table>
						</td>
					<tr>
				</table>
				<div>&nbsp;</div>
			</div>
		</form>
	</body>
</html>