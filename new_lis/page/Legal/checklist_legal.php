<?php
require ("../../lib/formatError.php");
require ("../../lib/open_con.php");
//require ("../../lib/open_con_dm.php");
require ("../../lib/open_con_dm_mysql.php");
require ("../../requirepage/parameter.php");

$vardoctambahan = "";
$statusreadonly = "";
if ($userpermission != "I")
{
   $statusreadonly = "readonly=readonly";
}

   //include ("../../lib/formatError.php");
  	//require ("../../lib/open_con.php");
  	//require ("../../lib/open_con_dm.php");

  $ipdm = "";
  $userdm = "";
	$tsql = "select control_value from ms_control where control_code='IPDM'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if ( $sqlConn === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$row[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}	

        $tuauserid = "";
        $tuatime = "";
    		$tsql = "select tua_userid,tua_time from Tbl_TemporariUserAkses 
    							where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
   			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			$params = array(&$_POST['query']);
   			$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				if($sqlConn === false)
				{
					die(FormatErrors(sqlsrv_errors()));
				}
	
				if(sqlsrv_has_rows($sqlConn))
				{
      			$rowCount = sqlsrv_num_rows($sqlConn);
      			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      			{
        			$tuauserid = $row[0];
        			$tuatime = $row[1];
      			}
   			}
   			sqlsrv_free_stmt( $sqlConn );

				if ($tuauserid == "" || $tuatime == "")
				{
					$tsql = "INSERT INTO Tbl_TemporariUserAkses values('$userwfid',
										'$userid','$custnomid','$userpermission',1,convert(varchar,getdate(),121))";
					$params = array(&$_POST['query']);
					$stmt = sqlsrv_prepare( $conn, $tsql, $params);
					if( $stmt )
					{
					} 
					else
					{
						echo "Error in preparing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
	
					if( sqlsrv_execute( $stmt))
					{
					}
					else
					{
						echo "Error in executing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
					sqlsrv_free_stmt( $stmt);
      	  $bisalanjut = "Y";
				}
				else
				{
					if ($tuauserid == $userid)
					{
						$tsql = "UPDATE Tbl_TemporariUserAkses
											set tua_count=tua_count+1,
											tua_action='$userpermission',
											tua_time=convert(varchar,getdate(),121)
											where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
						$params = array(&$_POST['query']);
						$stmt = sqlsrv_prepare( $conn, $tsql, $params);
						if( $stmt )
						{
						} 
						else
						{
							echo "Error in preparing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
	
						if( sqlsrv_execute( $stmt))
						{
						}
						else
						{
							echo "Error in executing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
						sqlsrv_free_stmt( $stmt);
      	  	$bisalanjut = "Y";
					}
					else
					{
						$varmsg = "Application ID <b>$custnomid</b> sudah diambil oleh user <font color=blue><b>$tuauserid</b></font> pada jam <font color=red><b>$tuatime</b></font> <BR>";
						echo $varmsg;
						exit;
					}
				}

   			  $tsql2 = "SELECT *
						        FROM Tbl_WorkflowDoc
						        WHERE wf_id='$userwfid'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

				  $datadoc = "";
          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
							     $datadoc = $row2['wf_doc'];
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
          $arrdoc=explode("|",$datadoc);

      $arrjaminan = array();   
      $jenisjaminan = "";
      $kondisijaminan = "";
      $tsql = "SELECT cust_jeniscol,col_id,group_col FROM Tbl_Cust_MasterCol
      				 WHERE ap_lisregno='$custnomid'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
         	  $vartemparr = $row[0] . "__" . $row[1] . "__" . $row[2];
      	    array_push($arrjaminan,$vartemparr);
         	  if (substr($row[0],0,1) == "B")
         	  {
         	     $jenisjaminan .= "Building, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	  }
         	  if (substr($row[0],0,1) == "V")
         	  {
         	     $jenisjaminan .= "Vehicle, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='V'";
         	  }
         	  if (substr($row[0],0,1) == "T")
         	  {
         	     $jenisjaminan .= "Tanah, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	  }
         	  if (substr($row[0],0,1) == "K")
         	  {
         	     $jenisjaminan .= "Kios, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	  }
/*         	  if (substr($row[0],0,1) == "D")
         	  {
         	     $jenisjaminan .= "Deposito, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='D'";
         	  }*/
         }
      }
      sqlsrv_free_stmt( $sqlConn );
	  array_push($arrjaminan,"PERSONAL__1__1");
      array_push($arrjaminan,"CORPORATE__1__1");
?>
<HTML>
   <HEAD>
      <META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>
      <META http-equiv='Pragma' content='no-cache'>
      <META content='MSHTML 5.50.4134.100' name=GENERATOR>
      <TITLE>SPIN UPLOAD</TITLE>
      <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
	  <link href="../../css/crw.css" rel="stylesheet" type="text/css" />
      <link href="../../css/d.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
      <Script Language="JavaScript">
        function toggleExpCol(idGrp, idContainer) {
            idContainer = idContainer ? idContainer + '_' : '';
            var div = document.getElementById(idContainer + 'divGrp' + idGrp);
            var vis = div.style.display = (div.style.display == 'none') ? 'block' : 'none';
        }
			function refreshhtml()
			{
				window.location.reload()
			}
			function uploadDocument(thelink)
			{
	        varwidth = 980;
	        varheight = 640;
	        varx = 0;
	        vary = 0;
          window.open(thelink,'lainnya','scrollbars=yes,width='+varwidth+',height='+varheight+',screenX='+varx+',screenY='+vary+',top='+varx+',left='+vary+',status=yes');
//          document.formsubmit.submit();
			}

	function cekthis()
	{
  		varreturn = "";
	  selectedArray = document.frm.hiddenfield.value.split(',');
	  for (i=0;i<selectedArray.length-1;i++)
	  {
	  	if (eval("document.frm.D" + selectedArray[i] + ".checked")==true)
	  	{
				varreturn = varreturn + 'D' + selectedArray[i] + ",";
	  	}
	  }
		  if(varreturn == "")
		  {
		  	alert("Harap Pilih Minimal Satu");
		  	return false;
		  }
		  document.frm.datachecked.value = varreturn;
		document.frm.target = "utama";
		document.frm.act.value = "saveform";
		document.frm.action = "./dospin_clg.php";
           submitform = window.confirm("<?=$confmsg;?>")
           if (submitform == true)
           {
	            document.frm.submit();
              return true;
           }
           else
           {
              return false;
           }
	}
      </Script>
   </HEAD>
   <BODY link=blue vlink=blue alink=blue>
<script language="JavaScript"><!--
name = 'utama';
//--></script>
      <div align=center>
      <TABLE cellPadding=5 width="100%" border=0 class="table">
        <TR>
    	  <TD vAlign=top width=1>
            <TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
              <TR>
                <TD class=backW vAlign=center>
          	</TD>
              </TR>
            </TABLE>
          </TD>
          <TD align=left valign=top>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <TR>
                <TD class=borderForm align=right bgColor=black>
                      <font style="font-size: 12;" color=#FFFFFF><B>Form type : Document Management &nbsp</B></font>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                  <TABLE cellSpacing=1 cellPadding=13 width="100%" border=0>
                    <TR>
                      <TD class=backW>
                  	<form id="frm" name="frm" method="post" action="./dospin_legal.php">
                  	   <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=1 border=0>                  	   	
<?
      $custcif = "";
      $tsql = "SELECT * FROM Tbl_CustomerMasterPerson2
      				 WHERE custnomid='$custnomid'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
         {
			$custcif = $row['custaplno'];
?>
							          <tr>
							          	 <td width=20% align=left valign=top>
							          	 	<font face=Arial size=2>Application ID</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=70% align=left valign=top>
							          	 	<font face=Arial size=2><? echo $custnomid; ?>  / <? echo $custcif; ?></font>							          	 	
							          	 	&nbsp &nbsp
							          	 	<a href="javascript:refreshhtml()">refresh</a>
							          	</td>
							          </tr>
							          <tr>
							          	 <td width=20% align=left valign=top>
							          	 	<font face=Arial size=2>Customer Name</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=70% align=left valign=top>
							          	 	<font face=Arial size=2><A HREF=../preview/previewall/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $custnomid; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL target=_blank><? echo $row['custfullname']; ?></A></font>
							          	</td>
							          </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
?>
                  	   </TABLE>
                  	   <BR>
<?
   $hiddenfield = "";
   for ($ki=0;$ki<count($arrjaminan);$ki++)
   {
      $vartemparr=explode("__",$arrjaminan[$ki]);
	  
	if ($vartemparr[0] == "PERSONAL")
	{
	$jenisjaminan = "NON COLATERAL";
	$tsql = "SELECT * FROM Tbl_DocPerson
					WHERE doc_segmen='PERSONAL'";
      }
	  else if($vartemparr[0] == "CORPORATE")
	  {
		  
         $jenisjaminan = "NON COLATERAL";
         $tsql = "SELECT * FROM Tbl_DocPerson
      				 		WHERE doc_segmen='CORPORATE'"; //echo $tsql;
	  }
      else
      {
         	  if (substr($vartemparr[0],0,1) == "B")
         	  {
         	     $jenisjaminan = "Building";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='B'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "V")
         	  {
         	     $jenisjaminan = "Vehicle";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='V'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "T")
         	  {
         	     $jenisjaminan = "Tanah";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='T'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "K")
         	  {
         	     $jenisjaminan = "Kios";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='K'";
         	  }
/*         	  if (substr($vartemparr[0],0,1) == "D")
         	  {
         	     $jenisjaminan = "Deposito";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='D'";
         	  }*/
         $tsql = "SELECT * FROM Tbl_DocPerson
         					WHERE doc_id<>''
      				 		$kondisijaminan";
							
							//echo $tsql;
      }
?>
                  	   <font face=Arial size=2><b>DOKUMEN <? echo $jenisjaminan ?> - <? echo $arrjaminan[$ki] ?></b></font>
                  	   <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 border=1 bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
												   <tr bgcolor=#CCCCCC>
												   	<th width=5% align=center valign=top>
												   		<font face=Arial size=2><b>CHK</b></font>
												   	</th>
												   	<th width=25% align=center valign=top>
												   		<font face=Arial size=2><b>NOTE</b></font>
												   	</th>
												   	<th width=25% align=center valign=top>
												   		<font face=Arial size=2><b>DOCUMENT</b></font>
												   	</th>
												   	<th width=20% align=center valign=top>
												   		<font face=Arial size=2><b>NOMOR</b></font>
												   	</th>
												   	<th width=15% align=center valign=top>
												   		<font face=Arial size=2><b>EXPIRED</b></font>
												   	</th>
												   	<th class="unprint" width=15% align=center valign=top>
												   		<font face=Arial size=2><b>PREVIEW</b></font>
												   	</th>
												  </tr>
<?
      if ($datadoc != "") 
      {      	
      	$tsql .= " AND (";
         for ($zz=0;$zz<count($arrdoc)-1;$zz++)
         {
      	   $tsql .= " doc_id='$arrdoc[$zz]' or ";
         }
         $vartemp = substr($tsql,0,strlen($tsql)-3) . ")";
         $tsql = $vartemp;
      }
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
      		 $vardocno = "";
      		 $vardocdate = "";
      		 $statuschecked = "";
      		 $vardocnotes = "";
      		 $vardocchecked = "";
			 		 $varflag = "";


			$sudahentry = "";
   			  $tsql2 = "SELECT *
						        FROM Tbl_CKPKLegal
						        WHERE docnomid='$custnomid'
						        AND doccode='$row[0]'
						        AND doc_jeniscol='$vartemparr[0]'
						        AND doc_colid='$vartemparr[1]'
						        AND doc_groupcol='$vartemparr[2]'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
      		   		$vardocno = $row2['docno'];
      		   		$vardocdate = $row2['docdate'];
      		   		$vardocchecked = $row2['doccheck'];
      		   		if ($row2['doccheck'] == "N")
      		   		{
      		   			$statuschecked = "";
      		   		}
					else if($row2['doccheck'] == "Y")
					{
						$statuschecked = "checked";
					}
					else
					{
						$statuschecked = "";
					}
      		   		$vardocnotes = $row2['docnotes'];
      		   		$varflag = $row2['doc_flag'];
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
   	      $namafield = $row[0] . "__" . $arrjaminan[$ki];
   	      $hiddenfield .= $namafield . ",";
					if ($vardocno == "" || $varflag == "E")
					{
						if($statusreadonly == "")
						{
  					    $sudahentry = "N";
						$vardoctambahan .= $namafield . "|";
						}
					}
?>
												   <tr>
												   	<td align="center" valign="top">
<?
														if($statusreadonly == "")
														{
?>
												   		<input type=checkbox name=D<? echo $namafield; ?> value='Y' <? echo $statuschecked; ?>>
<?
														}
														else
														{
?>
												   		<? 
														//echo $vardocchecked; 
														if($vardocchecked == "Y")
														{
															echo "<input type=\"checkbox\" disabled=\"disabled\" checked=\"checked\" value=\"\">";
														}
														else
														{
															echo "<input type=\"checkbox\" disabled=\"disabled\" value=\"\">";
														}
														?> &nbsp;
<?
														}
?>
												   	</td>
												   	<td align="left" valign=top>&nbsp;
<?
															if ($statusreadonly == "")
															{
?>
												   		   <input type=text style="width:90%" maxlength="500" name=NOTE<? echo $namafield; ?> value='<? echo $vardocnotes; ?>' <? echo $statusreadonly; ?>>
<?
															}
															else
															{
?>
												   		   <font face=Arial size=2><? echo $vardocnotes; ?></font>
<?
															}
?>
												   	</td>
												   	<td align=left valign=top>
												   		<font face=Arial size=2><? echo $row[0]; ?> - <? echo $row[1]; ?></font>
												   	</td>
												   	<td align=center valign=top>
<?
													if ($sudahentry == "")
													{
?>
												   		<font face=Arial size=2><? echo $vardocno; ?> &nbsp</font>
<?
													}
													else
													{
?>
												   		<input type=text name=DN<? echo $namafield ?> size=30 maxlength=30 value='<? echo $vardocno ?>'>
<?
													}
?>
												   	</td>
												   	<td width=15% align=center valign=top>
<?
													if ($sudahentry == "")
													{
?>
												   		<font face=Arial size=2><? echo $vardocdate; ?> &nbsp</font>
<?
													}
													else
													{
?>
												   		<input type=text name=DE<? echo $namafield ?> maxlength=10 style="width: 25mm;" value='<? echo $vardocdate ?>'>
<a href="javascript:NewCssCal('DE<? echo $namafield ?>','ddMMyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></a>
<?
													}
?>
												   	</td>
												   	<td class="unprint" align=center valign=top>
<?
     $statusdoc = $row[0] . ",";
//     $custcif = "CIF001";
		 $key = $custcif . "_" . $custnomid;

     $querytemp = "SELECT COUNT(*)
	   		                    FROM Tbl_Document
	        	                WHERE doc_index3='$custnomid'
	        	                AND doc_type='$row[0]'";
	   $resulttemp=mysql_query($querytemp);
	   $rowtemp = mysql_fetch_row($resulttemp);
	   $countrows=$rowtemp[0];

//     $linkdmI='http://'.$ipdm.'/Lis_mikro/spindm_new/external_upload.php?dmuserid=lismikro&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     $linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&dmuserorganization=PRIVATE&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     if ($countrows <= 0)
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload</A></div>";
     }
     else
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload / View</A></div>";
     }
     echo $uploadgambarI;
?>
												   	</td>
												  </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
      echo "</TABLE><BR>";
   }
?>
<center>
<?
if($userpermission == "I")
{
	?>
	<input type="button" id="btnsave" name="btnsave" value="save" style="width:300px;" class="button" onClick="cekthis();"/>
	<?
}
else
{
	require ("../../requirepage/btnview.php");
	require ("../../requirepage/btnprint.php");
}
require ("../../requirepage/hiddenfield.php");
?>
</center>
					<input type="hidden" name="vartambahan" value='<? echo $vardoctambahan ?>'>
                    <input type="hidden" name="datachecked">
                    <input type="hidden" name="act" value='simpandata'>
                    <input type="hidden" name="datadoc" value='<? echo $datadoc; ?>'>
                    <input type="hidden" name="hiddenfield" value='<? echo $hiddenfield; ?>'>
                  	</form>
                      </TD>
                    </TR>
                    <TR>
                      <TD class=backW></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                </TD>
              </TR>
              <TR>
                <TD height=15>
                   <table width=100% cellpadding=0 cellspacing=0 border=0>
                      <tr>
                         &nbsp
                      <tr>
                      <tr>
                      </tr>
                   </table>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
     </div>
   </BODY>
</HTML> 
