	<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	require ("../../requirepage/currency.php");
	
	$InfoFasilitasPinjamanBankMega_nocif = "";
	$InfoFasilitasPinjamanBankMega_nasabahsejak = "";
	$InfoFasilitasPinjamanBankMega_datapertanggal = "";
	$InfoFasilitasPinjamanBankMega_cb1 = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankMega2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankMega_nocif = $row["InfoFasilitasPinjamanBankMega_nocif"];
			$InfoFasilitasPinjamanBankMega_nasabahsejak = $row["InfoFasilitasPinjamanBankMega_nasabahsejak"];
			$InfoFasilitasPinjamanBankMega_datapertanggal = $row["InfoFasilitasPinjamanBankMega_datapertanggal"];
			$InfoFasilitasPinjamanBankMega_cb1 = $row["InfoFasilitasPinjamanBankMega_cb1"];
			
		}
	}
	
	$InfoFasilitasPinjamanBankMega_nocifORI = "";
	$InfoFasilitasPinjamanBankMega_nasabahsejakORI = "";
	$InfoFasilitasPinjamanBankMega_datapertanggalORI = "";
	$InfoFasilitasPinjamanBankMega_cb1ORI = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankMega where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankMega_nocifORI = $row["InfoFasilitasPinjamanBankMega_nocif"];
			$InfoFasilitasPinjamanBankMega_nasabahsejakORI = $row["InfoFasilitasPinjamanBankMega_nasabahsejak"];
			$InfoFasilitasPinjamanBankMega_datapertanggalORI = $row["InfoFasilitasPinjamanBankMega_datapertanggal"];
			$InfoFasilitasPinjamanBankMega_cb1ORI = $row["InfoFasilitasPinjamanBankMega_cb1"];
			
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<table width="100%" align="center" style="border:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK MEGA</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry1" name="formentry1" method="post">
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond Awal (<? echo $currency; ?>)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Outstanding (<? echo $currency; ?>)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran (<? echo $currency; ?>)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Tenor (bln)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Rate</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi (<? echo $currency; ?>)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$FasilitasPinjamanBankMega_jenisfasilitas = "";
			$FasilitasPinjamanBankMega_plafondawal = "";
			$FasilitasPinjamanBankMega_outstanding = "";
			$FasilitasPinjamanBankMega_angsuran = "";
			$FasilitasPinjamanBankMega_tenor = "";
			$FasilitasPinjamanBankMega_rate = "";
			$FasilitasPinjamanBankMega_provisi = "";
			$FasilitasPinjamanBankMega_kol = "";
			$FasilitasPinjamanBankMega_dpd = "";

			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankMega where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankMega_seq = $row["FasilitasPinjamanBankMega_seq"];
					$FasilitasPinjamanBankMega_jenisfasilitas = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
					$FasilitasPinjamanBankMega_plafondawal = $row["FasilitasPinjamanBankMega_plafondawal"];
					$FasilitasPinjamanBankMega_outstanding = $row["FasilitasPinjamanBankMega_outstanding"];
					$FasilitasPinjamanBankMega_angsuran = $row["FasilitasPinjamanBankMega_angsuran"];
					$FasilitasPinjamanBankMega_tenor = $row["FasilitasPinjamanBankMega_tenor"];
					$FasilitasPinjamanBankMega_rate = $row["FasilitasPinjamanBankMega_rate"];
					$FasilitasPinjamanBankMega_provisi = $row["FasilitasPinjamanBankMega_provisi"];
					$FasilitasPinjamanBankMega_kol = $row["FasilitasPinjamanBankMega_kol"];
					$FasilitasPinjamanBankMega_dpd = $row["FasilitasPinjamanBankMega_dpd"];
					
					$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega where JenisFasilitasBankMega_code = '$FasilitasPinjamanBankMega_jenisfasilitas'";
					$b2 = sqlsrv_query($conn, $tsql2);
					if ( $b2 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b2))
					{ 
						While($rowType2 = sqlsrv_fetch_array($b2, SQLSRV_FETCH_ASSOC))
						{
							$FasilitasPinjamanBankMega_jenisfasilitas = $rowType2["JenisFasilitasBankMega_nama"];
						}
					}

		
		?>
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_jenisfasilitas;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_plafondawal)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_outstanding)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_angsuran)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_tenor;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_rate;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_provisi)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_kol;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_dpd;?></td>		
			
		</tr>

		<?
				}
			}
		?>
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center">
				<?
					$FasilitasPinjamanBankMega_jenisfasilitas = "";
					$FasilitasPinjamanBankMega_plafondawal = "";
					$FasilitasPinjamanBankMega_outstanding = "";
					$FasilitasPinjamanBankMega_angsuran = "";
					$FasilitasPinjamanBankMega_tenor = "";
					$FasilitasPinjamanBankMega_rate = "";
					$FasilitasPinjamanBankMega_provisi = "";
					$FasilitasPinjamanBankMega_kol = "";
					$FasilitasPinjamanBankMega_dpd = "";
					
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="FasilitasPinjamanBankMega_jenisfasilitasAdd" name="FasilitasPinjamanBankMega_jenisfasilitasAdd"  nai="Jenis Fasilitas " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($FasilitasPinjamanBankMega_jenisfasilitas == $rowType["JenisFasilitasBankMega_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasBankMega_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasBankMega_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_plafondawalAdd" nai="Plafond Awal " style="width:75px;background:#ff0;" value="" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_plafondawalAdd')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_outstandingAdd" nai="Outstanding " style="width:75px;background:#ff0;" value="" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_outstandingAdd')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_angsuranAdd" nai="Angsuran " style="width:75px;background:#ff0;" value="" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_angsuranAdd')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_tenorAdd" nai="Tenor " style="width:60px;background:#ff0;" value="" maxlength="20" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_rateAdd" nai="Rate " style="width:60px;background:#ff0;" value="" maxlength="20" /></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_provisiAdd" nai="Provisi " style="width:75px;background:#ff0;" value="" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_provisiAdd')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center">
				<select id="FasilitasPinjamanBankMega_kolAdd" name="FasilitasPinjamanBankMega_kolAdd"  nai="kol " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
			</td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_dpdAdd" nai="DPD " style="width:60px;background:#ff0;" value="" maxlength="100"/></td>
			<td width="30%"><input type="button" value="SAVE" style="width:80px;background-color:blue;color:white;" onclick="sakura1()" /></td>
			
		</tr>
		</form>
		<form id="formedit1" name="formedit1" method="post">
		
		
		<?
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankMega2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankMega_seq = $row["FasilitasPinjamanBankMega_seq"];
					$FasilitasPinjamanBankMega_jenisfasilitas = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
					$FasilitasPinjamanBankMega_plafondawal = $row["FasilitasPinjamanBankMega_plafondawal"];
					$FasilitasPinjamanBankMega_outstanding = $row["FasilitasPinjamanBankMega_outstanding"];
					$FasilitasPinjamanBankMega_angsuran = $row["FasilitasPinjamanBankMega_angsuran"];
					$FasilitasPinjamanBankMega_tenor = $row["FasilitasPinjamanBankMega_tenor"];
					$FasilitasPinjamanBankMega_rate = $row["FasilitasPinjamanBankMega_rate"];
					$FasilitasPinjamanBankMega_provisi = $row["FasilitasPinjamanBankMega_provisi"];
					$FasilitasPinjamanBankMega_kol = $row["FasilitasPinjamanBankMega_kol"];
					$FasilitasPinjamanBankMega_dpd = $row["FasilitasPinjamanBankMega_dpd"];

		
		?>
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center">
				<?
					
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="FasilitasPinjamanBankMega_jenisfasilitasAdd<? echo $FasilitasPinjamanBankMega_seq;?>" name="FasilitasPinjamanBankMega_jenisfasilitasAdd<? echo $FasilitasPinjamanBankMega_seq;?>"  nai="Jenis Fasilitas " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($FasilitasPinjamanBankMega_jenisfasilitas == $rowType["JenisFasilitasBankMega_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasBankMega_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasBankMega_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_plafondawalAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Plafond Awal " style="width:75px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankMega_plafondawal);?>" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_plafondawalAdd<? echo $FasilitasPinjamanBankMega_seq;?>')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_outstandingAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Outstanding " style="width:75px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankMega_outstanding);?>" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_outstandingAdd<? echo $FasilitasPinjamanBankMega_seq;?>')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_angsuranAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Angsuran " style="width:75px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankMega_angsuran);?>" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_angsuranAdd<? echo $FasilitasPinjamanBankMega_seq;?>')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_tenorAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Tenor " style="width:60px;background:#ff0;" value="<? echo $FasilitasPinjamanBankMega_tenor;?>" maxlength="20" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_rateAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Rate " style="width:60px;background:#ff0;" value="<? echo $FasilitasPinjamanBankMega_rate?>" maxlength="20" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_provisiAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="Provisi " style="width:75px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankMega_provisi);?>" maxlength="20" onKeyUp="outputMoney('FasilitasPinjamanBankMega_provisiAdd<? echo $FasilitasPinjamanBankMega_seq;?>')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center">
				<select id="FasilitasPinjamanBankMega_kolAdd<? echo $FasilitasPinjamanBankMega_seq;?>" name="FasilitasPinjamanBankMega_kolAdd<? echo $FasilitasPinjamanBankMega_seq;?>"  nai="kol " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1"  <? if ($FasilitasPinjamanBankMega_kol == "1") { echo "selected";}?>>1</option>
					<option value="2"  <? if ($FasilitasPinjamanBankMega_kol == "2") { echo "selected";}?>>2</option>
					<option value="3"  <? if ($FasilitasPinjamanBankMega_kol == "3") { echo "selected";}?>>3</option>
					<option value="4"  <? if ($FasilitasPinjamanBankMega_kol == "4") { echo "selected";}?>>4</option>
					<option value="5"  <? if ($FasilitasPinjamanBankMega_kol == "5") { echo "selected";}?>>5</option>
				</select>
			</td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankMega_dpdAdd<? echo $FasilitasPinjamanBankMega_seq;?>" nai="DPD " style="width:60px;background:#ff0;" value="<? echo $FasilitasPinjamanBankMega_dpd;?>" maxlength="100"/></td>
			
			<td width="25%"><input type="hidden" id="FasilitasPinjamanBankMega_seq" value="<? echo $FasilitasPinjamanBankMega_seq;?>" /><input type="button" value="EDIT" style="width:40px;background-color:red;color:white;font-size:10px" onclick="rino1(<?echo $FasilitasPinjamanBankMega_seq;?>)" /><input type="button" value="HAPUS" style="width:50px;background-color:red;color:white;font-size:10px" onclick="sasshi1(<?echo $FasilitasPinjamanBankMega_seq;?>)" /></td>
		</tr>

		<?
				}
			}
		?>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td width="35%">No. CIF</td>
	<td width="65%"><div style="border:1px solid black;width:200px;" align="center"><? echo $InfoFasilitasPinjamanBankMega_nocifORI;?></div> <input type="text" id="nocif" style="width:200px;background:#FFF;" nai="No. CIF " value="<? echo $InfoFasilitasPinjamanBankMega_nocif;?>"  maxlength="20"/></td>
</tr>
<tr>
	<td width="35%">Nasabah sejak</td>
	<td width="65%"><div style="border:1px solid black;width:200px;" align="center"><? echo $InfoFasilitasPinjamanBankMega_nasabahsejakORI;?></div> <input type="text" disabled="disabled" id="nasabahsejak" style="width:200px;background:#FFF;" nai="Nasabah sejak " value="<? echo $InfoFasilitasPinjamanBankMega_nasabahsejak;?>"  maxlength="20"/> <a href="javascript:NewCssCal('nasabahsejak','MMddyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></a></td>
</tr>
<tr>
	<td width="35%">Data pertanggal</td>
	<td width="65%"><div style="border:1px solid black;width:200px;" align="center"><? echo $InfoFasilitasPinjamanBankMega_datapertanggalORI;?></div> <input type="text" disabled="disabled" id="datapertanggal" style="width:200px;background:#FFF;" nai="Data pertanggal " value="<? echo $InfoFasilitasPinjamanBankMega_datapertanggal;?>"  maxlength="20"/> <a href="javascript:NewCssCal('datapertanggal','MMddyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></a></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1">
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankMega_cb1ORI;?></div> 
		<select id="FasilitasBankMega2CB1" name="FasilitasBankMega2CB1"  nai="Informasi Negatif Komunitas 2" class="harus" style="background:#fff">
			<option value="YA"  <? if ($InfoFasilitasPinjamanBankMega_cb1 == "YA") { echo "selected";}?>>YA</option>
			<option value="TIDAK"  <? if ($InfoFasilitasPinjamanBankMega_cb1 == "TIDAK") { echo "selected";}?>>TIDAK</option>
		</select>
	</td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '1' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '2' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '3' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryAcol" name="formentryAcol" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:200px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:200px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:200px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '1' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '2' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '3' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryAcol" name="formentryAcol" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankMega_berapakali1" name="LKCDKolektibilitasTidakLancarBankMega_berapakali1"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali1 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1" name="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1" name="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankMega_berapakali2" name="LKCDKolektibilitasTidakLancarBankMega_berapakali2"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali2 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2" name="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2" name="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankMega_berapakali3" name="LKCDKolektibilitasTidakLancarBankMega_berapakali3"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankMega_berapakali3 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3" name="LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3" name="LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr>
			<td colspan="4" align="center"><input type="button" value="SAVE" style="width:100px;background-color:blue;color:white;" onclick="harupi1();" /></td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
<script type="text/javascript">
function harupi1() {
		var nan="SAVE8ACOL";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var LKCDKolektibilitasTidakLancarBankMega_berapakali1 = $("#LKCDKolektibilitasTidakLancarBankMega_berapakali1").val();
		var LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = $("#LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1").val();
		var LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = $("#LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1").val();
		var LKCDKolektibilitasTidakLancarBankMega_berapakali2 = $("#LKCDKolektibilitasTidakLancarBankMega_berapakali2").val();
		var LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = $("#LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2").val();
		var LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = $("#LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2").val();
		var LKCDKolektibilitasTidakLancarBankMega_berapakali3 = $("#LKCDKolektibilitasTidakLancarBankMega_berapakali3").val();
		var LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = $("#LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3").val();
		var LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = $("#LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3").val();
		
		var nocif = $("#nocif").val();
		var nasabahsejak = $("#nasabahsejak").val();
		var datapertanggal = $("#datapertanggal").val();
		var FasilitasBankMega2CB1 = $("#FasilitasBankMega2CB1").val();
		
		var FormName="formentryAcol";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		/*if(nocif == "")
		{
			alert("No. CIF HARUS DIISI");
			StatusAllowSubmit=false;
		}
		
		else if(nasabahsejak == "")
		{
			alert("Nasabah sejak HARUS DIISI");
			StatusAllowSubmit=false;
		}
		
		else if(datapertanggal == "")
		{
			alert("Data pertanggal HARUS DIISI");
			StatusAllowSubmit=false;
		}*/
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasBankMega2CB1="+FasilitasBankMega2CB1+"&nocif="+nocif+"&nasabahsejak="+nasabahsejak+"&datapertanggal="+datapertanggal+"&LKCDKolektibilitasTidakLancarBankMega_berapakali1="+LKCDKolektibilitasTidakLancarBankMega_berapakali1+"&LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1="+LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1+"&LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1="+LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1+"&LKCDKolektibilitasTidakLancarBankMega_berapakali2="+LKCDKolektibilitasTidakLancarBankMega_berapakali2+"&LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2="+LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2+"&LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2="+LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2+"&LKCDKolektibilitasTidakLancarBankMega_berapakali3="+LKCDKolektibilitasTidakLancarBankMega_berapakali3+"&LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3="+LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3+"&LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3="+LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					alert('Data berhasil disimpan');
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	function sakura1() {
		var nan="SAVE8A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankMega_jenisfasilitas = $("#FasilitasPinjamanBankMega_jenisfasilitasAdd").val();
		var FasilitasPinjamanBankMega_plafondawal = $("#FasilitasPinjamanBankMega_plafondawalAdd").val();
		var FasilitasPinjamanBankMega_outstanding = $("#FasilitasPinjamanBankMega_outstandingAdd").val();
		var FasilitasPinjamanBankMega_angsuran = $("#FasilitasPinjamanBankMega_angsuranAdd").val();
		var FasilitasPinjamanBankMega_tenor = $("#FasilitasPinjamanBankMega_tenorAdd").val();
		var FasilitasPinjamanBankMega_rate = $("#FasilitasPinjamanBankMega_rateAdd").val();
		var FasilitasPinjamanBankMega_provisi = $("#FasilitasPinjamanBankMega_provisiAdd").val();
		var FasilitasPinjamanBankMega_kol = $("#FasilitasPinjamanBankMega_kolAdd").val();
		var FasilitasPinjamanBankMega_dpd = $("#FasilitasPinjamanBankMega_dpdAdd").val();
			
		var FormName="formentry1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankMega_jenisfasilitas="+FasilitasPinjamanBankMega_jenisfasilitas+"&FasilitasPinjamanBankMega_plafondawal="+FasilitasPinjamanBankMega_plafondawal+"&FasilitasPinjamanBankMega_outstanding="+FasilitasPinjamanBankMega_outstanding+"&FasilitasPinjamanBankMega_angsuran="+FasilitasPinjamanBankMega_angsuran+"&FasilitasPinjamanBankMega_tenor="+FasilitasPinjamanBankMega_tenor+"&FasilitasPinjamanBankMega_rate="+FasilitasPinjamanBankMega_rate+"&FasilitasPinjamanBankMega_provisi="+FasilitasPinjamanBankMega_provisi+"&FasilitasPinjamanBankMega_kol="+FasilitasPinjamanBankMega_kol+"&FasilitasPinjamanBankMega_dpd="+FasilitasPinjamanBankMega_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function sasshi1(id){
		var nan="DEL8A"
		
		var custnomid=$("#custnomid").val();
		var FasilitasPinjamanBankMega_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&FasilitasPinjamanBankMega_seq="+FasilitasPinjamanBankMega_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}

	function rino1(id) {
		var nan="EDIT8A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankMega_jenisfasilitas = $("#FasilitasPinjamanBankMega_jenisfasilitasAdd"+id).val();
		var FasilitasPinjamanBankMega_plafondawal = $("#FasilitasPinjamanBankMega_plafondawalAdd"+id).val();
		var FasilitasPinjamanBankMega_outstanding = $("#FasilitasPinjamanBankMega_outstandingAdd"+id).val();
		var FasilitasPinjamanBankMega_angsuran = $("#FasilitasPinjamanBankMega_angsuranAdd"+id).val();
		var FasilitasPinjamanBankMega_tenor = $("#FasilitasPinjamanBankMega_tenorAdd"+id).val();
		var FasilitasPinjamanBankMega_rate = $("#FasilitasPinjamanBankMega_rateAdd"+id).val();
		var FasilitasPinjamanBankMega_provisi = $("#FasilitasPinjamanBankMega_provisiAdd"+id).val();
		var FasilitasPinjamanBankMega_kol = $("#FasilitasPinjamanBankMega_kolAdd"+id).val();
		var FasilitasPinjamanBankMega_dpd = $("#FasilitasPinjamanBankMega_dpdAdd"+id).val();
		var FasilitasPinjamanBankMega_seq = id;
			
		var FormName="formedit1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankMega_seq="+FasilitasPinjamanBankMega_seq+"&FasilitasPinjamanBankMega_jenisfasilitas="+FasilitasPinjamanBankMega_jenisfasilitas+"&FasilitasPinjamanBankMega_plafondawal="+FasilitasPinjamanBankMega_plafondawal+"&FasilitasPinjamanBankMega_outstanding="+FasilitasPinjamanBankMega_outstanding+"&FasilitasPinjamanBankMega_angsuran="+FasilitasPinjamanBankMega_angsuran+"&FasilitasPinjamanBankMega_tenor="+FasilitasPinjamanBankMega_tenor+"&FasilitasPinjamanBankMega_rate="+FasilitasPinjamanBankMega_rate+"&FasilitasPinjamanBankMega_provisi="+FasilitasPinjamanBankMega_provisi+"&FasilitasPinjamanBankMega_kol="+FasilitasPinjamanBankMega_kol+"&FasilitasPinjamanBankMega_dpd="+FasilitasPinjamanBankMega_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 