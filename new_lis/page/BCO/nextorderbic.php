<?php

require ("../../lib/formatError.php");
require ("../../lib/open_con.php");

$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
$userwfid=$_REQUEST['userwfid'];

$dataallchecked=$_REQUEST['datachecked'];
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>OrderBiChecking</title>
	<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
	<Script Language="JavaScript">
  	function cek()
  	{
  		varreturn = "";
		var uncheckbox="";
<?
	$dataall=explode(",",$_REQUEST['datachecked']);
	$tmpcount = count($dataall);
	for ($i=0; $i<$tmpcount-1; $i++)
	{
		$strsql = "	select * from(
					select b.custnomid,b.entryseq
					from Tbl_FBIC a
					join Tbl_CustomerSibling b on a.txn_id = b.custnomid
					where b.custnomid = '".$dataall[$i]."' and b.flagdelete='0'
					union 
					select b.custnomid,b.entryseq
					from Tbl_FBIC a
					join Tbl_algo_bco b on a.txn_id = b.custnomid
					where b.custnomid = '".$dataall[$i]."' and b.flagdelete='0'
					) tblxx
					";
					
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursorType);
		if	($sqlConn === false)die(FormatErrors( sqlsrv_errors()));
		if(sqlsrv_has_rows($sqlConn))
		{
			$rowCount = sqlsrv_num_rows($sqlConn);
			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			{
				$vartemp = $row['custnomid'].$row['entryseq'];
				?>
				if (document.orderBIC.<? echo $vartemp; ?>.checked == true)
				{
				varreturn = varreturn + '<? echo $vartemp; ?>' + ",";
				} 
				else if (document.orderBIC.<? echo $vartemp; ?>.checked == false)
				{
				uncheckbox = uncheckbox + '<? echo $vartemp; ?>' + ",";
				} 
				
				<?
			}
		}
	}
	sqlsrv_free_stmt( $sqlConn );
?>
		  if(varreturn == "")
		  {
		  	alert("Harap Pilih Minimal Satu");
		  	return false;
		  }
		  //document.orderBIC.target = "utama";
		  document.orderBIC.datachecked.value = varreturn;
		  document.orderBIC.uncheckbox.value = uncheckbox;
		  document.orderBIC.action = "do_orderBIC.php";
		  submitform = window.confirm("<?=$confmsg;?>")
		  if (submitform == true)
		  {
			  document.orderBIC.submit();
			  return true;
		  }
   }
  </Script>
</head>

<body>
<?

?>
<form id="orderBIC" name="orderBIC" method="post" action="do_orderBIC.php">
<table width="800" border="0" class="table" align="center">
	<tr>
		<td align="center" class="judul" ><b>ORDER BI CHECKING</b></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<table style="text-align:center;" width="100%" border="1">
				<tr>
					<td width="20%">BIC ID</td>
					<td width="20%">Nama Nasabah</td>
					<td width="20%">Kode cabang</td>
					<td width="20%">Status</td>
					<td width="5%">Action</td>
				</tr>
				<?
				$dataall=explode(",",$_REQUEST['datachecked']);
				$tmpcount = count($dataall);
				for ($i=0; $i<$tmpcount-1; $i++)
				{
					$strsql = " select c.tua_userid,b.custnomid,b.entryseq,b.orderseq, b.ktp,
								b.name,b.bod,b.address,b.gender,b.relation,b.phoneno, b.occupation,
								b.npwp,b.zipcode,b.workaddress,e.branch_name,e.branch_code, 
								'status' = case when b.orderseq='00'
								then 'belum pernah di order' 
								else 'sudah pernah di order oleh ' + isnull(c.tua_userid,'') + 
								' sebanyak ' + cast(cast(b.orderseq as int) as varchar) end 
								from Tbl_FBIC a 
								join Tbl_CustomerSibling b on a.txn_id = b.custnomid 
								left join Tbl_FBIO j on j.txn_id = a.txn_id 
								left join Tbl_TemporariUserAkses c on a.txn_id = c.tua_nomid 
								and j.txn_user_id = c.tua_userid
								join Tbl_CustomerMasterPerson d on d.custnomid = b.custnomid 
								join Tbl_Branch e on d.custbranchcode = e.branch_code 
								where b.custnomid='".$dataall[$i]."' and b.flagdelete='0'";
					$sqlcon = sqlsrv_query($conn, $strsql);
					if ( $sqlcon === false)die(FormatErrors( sqlsrv_errors()));
						if(sqlsrv_has_rows($sqlcon))
						{
							while($row = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								$custnomid = $row['custnomid'];
								$entryseq = $row['entryseq'];
								$name = $row['name'];
								$branchcode = $row['branch_code'];
								$branchname = $row['branch_name'];
								?>
								<tr>
									<td><? echo $custnomid.$entryseq; ?></td>
									<td>
									<!--<A HREF="./formpreviewI_v2.php?userid=<? echo $userid; ?>&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $custnomid; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL" target=_blank ><? echo $name;?></A>-->
									<A style="text-decoration:none; visited:blue; color:blue;" HREF="./argument.php?userid=<? echo $userid; ?>&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $custnomid; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL&datachecked=<?=$dataallchecked?>&nama=<?=$name?>&seq=<?=$entryseq?>"><? echo $name;?></A>
									
									</td>
									<td><? echo $branchcode."\t-\t".$branchname;?></td>
									<td><? echo  $row['status'];?></td>
									<td>
										<input type = "checkbox" name="<? echo $custnomid.$entryseq;?>" id="<? echo $custnomid.$entryseq;?>" value='Y'> 
									</td>
								
								</tr>
								<?
							}
						}	
				}
				?>
				</table>
			</td>
		</tr>
		<tr>
			<td align = "center">&nbsp; </td>
		</tr>
		<tr>
			<td align = "center">
				<table style="text-align:center;" width="100%" border="1">
					<tr>
						<td colspan="5"> Dari algoritma dan manual</td>
					</tr>
					<tr>
						<td width="20%">BIC ID</td>
						<td width="20%">Nama Nasabah</td>
						<td width="20%">Kode cabang</td>
						<td width="20%">Status</td>
						<td width="5%">Action</td>
					</tr>
					<?
					$dataall=explode(",",$_REQUEST['datachecked']);
					$tmpcount = count($dataall);
					for ($i=0; $i<$tmpcount-1; $i++)
					{
						$strsql="select b.custnomid+entryseq as 'numberseq' 
								,b.name,c.branch_code+' - '+ c.branch_name as 'branch',
								'status' = case when b.orderseq='00'
								then 'belum pernah di order' 
								else 'sudah pernah di order oleh ' + isnull(d.tua_userid,'') + 
								'sebanyak ' + cast(cast(b.orderseq as int) as varchar) end 
								from Tbl_FBIC a
								join Tbl_algo_bco b on a.txn_id = b.custnomid
								join Tbl_Branch c on c.branch_code = a.txn_branch_code
								left join Tbl_TemporariUserAkses d on a.txn_id = d.tua_nomid
								and b.custnomid = d.tua_nomid
								where (ISNULL(d.tua_wfid,'BIO')='BIO' or d.tua_wfid='BIO')
								and b.custnomid='".$dataall[$i]."' and b.flagdelete='0'";
								//echo $strsql;
								$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die(FormatErrors( sqlsrv_errors()));
						if(sqlsrv_has_rows($sqlcon))
						{
							while($row = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								$numberseq = $row['numberseq'];
								$name = $row['name'];
								$branch = $row['branch'];
								?>
								<tr>
									<td><? echo $numberseq; ?></td>
									<td><? echo $name;?></td>
									<td><? echo $branch;?></td>
									<td><? echo  $row['status'];?></td>
									<td>
										<input type = "checkbox" name="<? echo $numberseq;?>" id="<? echo $numberseq;?>" value='Y'> 
									</td>
								
								</tr>
								<?
							}
						}
					}
					?>
				</table>
			</td>
		</tr>
		<tr>
			<td align = "center">
				<input id="orderBIC" name="orderBIC" class="blue" type="button" value="Order BI Sekarang" onClick="cek()" />
				<input type="hidden" name="datachecked" id="datachecked" />
				<input type="hidden" name="uncheckbox" id="uncheckbox"/>
				<input type="hidden" name="userid"  value="<? echo $userid; ?>">
				<input type="hidden" name="userwfid"  value="<? echo $userwfid; ?>">
				<input type="hidden" name="userpwd"  value="<? echo $userpwd; ?>">
				<input type="hidden" name="custnomid"  value="<? echo $custnomid; ?>">
				<input type="hidden" name="userregion"  value="<? echo $userregion; ?>">
				<input type="hidden" name="userbranch"  value="<? echo $userbranch; ?>">
			</td>
		</tr>
	</table>
</form>
<?

	require('../../lib/close_con.php'); 
?>