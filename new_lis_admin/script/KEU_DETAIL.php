<?
include("../../new_lis/lib/sqlsrv.lis.php");
$db = new DB_LIS();
$db->connect();

$sql = "select * from KEU_DETAIL order by _sequence";
$db->executeQuery($sql);
$result_detail = $db->lastResults;

$sql = "select * from KEU_HEADER";
$db->executeQuery($sql);
$result_header = $db->lastResults;

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>PARAMETER</title>
</head>

<body>
<form method="post" action="KEU_DETAIL_process.php">
<table align="center" width="960" border="0" style="">
	<tr>
		<td align="center"><img src="../images/Header Mega (L).png" width="200px"></td>
	</tr>
	<tr>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><h3>DETAIL</h3></td>
	</tr>
</table>

<table border="0" align="center" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td><button type="reset" onClick="func_new()">New</button></td>
		<td width="1px">&nbsp;</td>

		<td style="text-align:right">Code</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td><input type="text" id="code" name="code" value="" style="width:50px;"></td>

		<td width="1px">&nbsp;</td>

		<td style="text-align:right">Name</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td width="50%"><input type="text" id="name" name="name" value="" style="width:90%"></td>

		<td width="1px">&nbsp;</td>

		<td style="text-align:right">Head</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td width="30%">
		<select id="head" name="head">
		<option value=""> - </option>
		<?
		for($x=0;$x<count($result_header);$x++)
		{
			$code = $result_header[$x]['_code'];
			$show = $result_header[$x]['_name'];

			$param_head[$code] = $show;
		?>
		<option value="<?=$code;?>">[<?=$code;?>] - <?=$show;?></option>
		<?
		}
		?>
		</select>
		</td>


		<td width="1px">&nbsp;</td>

		<td style="text-align:right">Type</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td width="30%">
		<select id="type" name="type">
		<option value=""> - </option>
		<option value="VALUE">VALUE</option>
		<option value="FORMULA">FORMULA</option>
		</select>
		</td>

		<td width="1px">&nbsp;</td>

		<td style="text-align:right">Seq</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td width="10%"><input type="text" id="seq" name="seq" value="0" style="width:60px"></td>

		<td width="1px">&nbsp;</td>
		<td width="30%"><button id="button_form" name="button_form" value="add" type="submit">Add</button></td>
	</tr>
</table>
<br>
<table style="background-color:#FFF;" width="1200px"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
	<tr>
		<th>Code</th>
		<th>Name</th>
		<th>Head</th>
		<th>Type</th>
		<th>Sequence</th>
		<th>Action</th>
	</tr>

	<?
	for($x=0;$x<count($result_detail);$x++)
	{
		$code_detail = $result_detail[$x]['_code_detail'];
		$code_header = $result_detail[$x]['_code_header'];
		$type = $result_detail[$x]['_type'];
		$seq = $result_detail[$x]['_sequence'];
		$name = $result_detail[$x]['_name'];
	?>

	<tr>
	<td><?=$code_detail;?></td>
	<td><?=$name;?></td>
	<td align="center">[<?=$code_header;?>] - <?=$param_head[$code_header];?></td>
	<td align="center"><?=$type;?></td>
	<td align="right"><?=$seq;?></td>
	<td align="center"><button type="button" onClick="func_edit('<?=$code_detail;?>','<?=$name;?>','<?=$code_header;?>','<?=$type;?>','<?=$seq;?>')">Edit</button> | <button type="button" onClick="func_delete('<?=$code_detail;?>','<?=$name;?>','<?=$code_header;?>','<?=$type;?>','<?=$seq;?>')">Delete</button></td>
	</tr>

	<?
	}
	?>

</table>

</form>

<script type="text/javascript">

function func_new()
{
	document.getElementById("code").readOnly = false;
	document.getElementById("button_form").innerHTML = "Add";
	document.getElementById("button_form").value = "add";
}

function func_delete(code,name,head,type,seq)
{
	document.getElementById("code").readOnly = true;
	document.getElementById("code").value = code;
	document.getElementById("name").value = name;
	document.getElementById("head").value = head;
	document.getElementById("type").value = type;
	document.getElementById("seq").value = seq;
	document.getElementById("button_form").innerHTML = "Delete";
	document.getElementById("button_form").value = "delete";
}

function func_edit(code,name,head,type,seq)
{
	document.getElementById("code").readOnly = true;
	document.getElementById("code").value = code;
	document.getElementById("name").value = name;
	document.getElementById("head").value = head;
	document.getElementById("type").value = type;
	document.getElementById("seq").value = seq;
	document.getElementById("button_form").innerHTML = "Edit";
	document.getElementById("button_form").value = "edit";
}

</script>

</body>
</html>
