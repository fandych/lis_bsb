<?
$strsql = "select count(*) as countrow from tbl_customerfacility where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$chcekrow=$rows['countrow'];
	}
}
$condition="";
if ($chcekrow!="0")
{
	$tmpcondition="";
	$strsql = "select custcreditneed from tbl_customerfacility where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$tmpcondition.="'".$rows['custcreditneed']."',";
		}
	}
	$condition="where credit_need_code not in (".substr($tmpcondition,0,-1).")";
}
?>
<table class="tbl100">
	<tr>
		<td>Tujuan Pengajuan Kredit</td>
		<td>
			<select id="creditneed" name="creditneed" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
				<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
				<?
					$strsql = "select * from tbl_Creditneed ".$condition;
					$sqlcon = sqlsrv_query($conn, $strsql);
					if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlcon))
					{
						while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
						{
							echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
						}
					}
				?>
			</select>
			
		</td>
	</tr>
	<tr>
		<td>Jenis Fasilitas</td>
		<td style="width:250px;" id="dtl_creditneed">
			<select id="codeproduct" name="codeproduct" style="width:100%;" class="nonmandatory">
				<option value="">-- Pilih Jenis Fasilitas --</option>
				<?
					$strsql = "select * from tbl_kodeproduk";
					$sqlcon = sqlsrv_query($conn, $strsql);
					if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlcon))
					{
						while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
						{
							echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
						}
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Plafond (<?echo $custcurcode?>)</td>
		<td>
			<input type="text" id="plafond" name="plafond" class="nonmandatory" onkeyup="currency(this.id);" maxlength="15"/>
		</td>
	</tr>
	<tr>
		<td>Jangka Waktu</td>
		<td>
			<input type="text" style="width:50px" id="period" name="period" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);"/> &nbsp;&nbsp; Bulan
		</td>
	</tr>
	<tr>
		<td>Suku Bunga</td>
		<td>
			<input type="text" style="width:50px" id="sukubunga" name="sukubunga" class="nonmandatory" maxlength="5" onkeyup="perkiraanAngsuran()" &nbsp;&nbsp; %
		</td>
	</tr>
	<tr>
		<td>Jenis Bunga</td>
		<td>
			<select name="jenisbunga" id="jenisbunga" class="nonmandatory" onchange="perkiraanAngsuran()">
				<option value="1">Flat</option>
				<option value="2">Efektif</option>
				<option value="3">Anuitas</option>
				<option value="4">Anuitas Tahunan</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Perkiraan Angsuran</td>
		<td>
			<input type="text" style="width:100%" id="perkiraanangsuran" name="perkiraanangsuran" readonly class="nonmandatory" maxlength="30" onkeyup="currency(this.id);" onkeypress="return isNumberKey(event);"/>
		</td>
	</tr>
	<tr>
		<td>Kegunaan Kredit</td>
		<td>
			<textarea name="credituse" id="credituse" class="nonmandatory" style="width:100%;" rows="3"></textarea>
		</td>
	</tr>
	<tr>
		<td>Harga Rumah / Penawaran</td>
		<td>
			<input type="text" class="nonmandatory" style="width:100%" id="hargapenawaran" name="hargapenawaran" maxlength="30" onkeyup="currency(this.id);" onkeypress="return isNumberKey(event);"/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right">
			<input type="button" id="btnadd" name="btnadd" value="save" class="button" onclick="adddtl();"/>
		</td>
	</tr>
</table>
<?
	
$totalpalfond=0;
$x=1;
$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,
			c.produk_type_description,a.custcreditplafond,a.custcreditlong,a.custcredituse,a.sukubunga,a.perkiraanangsuran,a.hargapenawaran ,a.jenisbunga, d._kemampuanbayar , a.uang_muka 
			from tbl_CustomerFacility a 
			left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
			left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
			left join tbl_KGB_infokerja d on a.custnomid = d._custnomid
			where a.custnomid ='".$custnomid."'";
		
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	echo '
		<table class="tbl100">
		';
	while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$totalpalfond+=$rows['custcreditplafond'];
		$jenisbunga=$rows['jenisbunga'];
		
		if($jenisbunga == "1")
		{
			$jenisbunga = "Flat";
		}else if($jenisbunga == "2")
		{
			$jenisbunga = "Efektif";
		}else if($jenisbunga == "4")
		{
			$jenisbunga = "Anuitas Tahunan";
		}else{
			$jenisbunga = "Anuitas";
		}
		
		$_kemampuanmembayar = $rows['_kemampuanbayar'];
		
		if(isset($_GET['kemampuanmembayar']))
		{
			if(str_replace(",","",$_GET['kemampuanmembayar']) >= str_replace(",","",$rows['perkiraanangsuran']))
			{
				$kemper = "<font color=green>Berkas Layak Dilanjutkan</font>";
			}else if(str_replace(",","",$_GET['kemampuanmembayar']) < str_replace(",","",$rows['perkiraanangsuran']))
			{
				$kemper = "Berkas Perlu Dipertimbangkan Kembali";
			}
		}else{
			if(str_replace(",","",$_kemampuanmembayar) >= str_replace(",","",$rows['perkiraanangsuran']))
			{
				$kemper = "<font color=green>Berkas Layak Dilanjutkan</font>";
			}else if(str_replace(",","",$_kemampuanmembayar) < str_replace(",","",$rows['perkiraanangsuran']))
			{
				$kemper = "Berkas Perlu Dipertimbangkan Kembali";
			}
		}
		
		echo '
			<tr>
				<td style="width:180px;">Tujuan Pengajuan Kredit</td>
				<td style="width:180px;">'.$rows['credit_need_name'].'</td>
				<td rowspan="2" style="text-align:right;">&nbsp;
					<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />
				</td>
			</tr>
			<tr>
				<td>Jenis Fasilitas</td>
				<td>'.$rows['produk_type_description'].'</td>
			</tr>
			<tr>
				<td>Plafond ('.$custcurcode.')</td>
				<td>'.numberFormat($rows['custcreditplafond']).'</td>
				<td rowspan="2" style="text-align:right;">
					<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />
				</td>
			</tr>
			<tr>
				<td>Jangka Waktu</td>
				<td>'.$rows['custcreditlong'].'</td>
			</tr>
			<tr>
				<td>Suku Bunga</td>
				<td>'.$rows['sukubunga'].' %</td>
			</tr>
			<tr>
				<td>Jenis Bunga</td>
				<td>'.$jenisbunga.'</td>
			</tr>
			<tr>
				<td>Perkiraan Angsuran</td>
				<td>'.numberFormat($rows['perkiraanangsuran']).'</td>
			</tr>
			<tr>
				<td>Kegunaan Kredit</td>
				<td style="max-width:190px;">'.$rows['custcredituse'].'</td>
			</tr>
			<tr>
				<td>Harga Rumah / Penawaran</td>
				<td>'.numberFormat($rows['hargapenawaran']).'</td>
			</tr>
			<tr>
				<td>Uang Muka / Self Financing</td>
				<td>'.$rows['uang_muka'].' %</td>
			</tr>
			<tr>
		';
	echo 		'<td colspan="2" style="font-size:18px;">';
	echo 			'<script>
						function nilaiLayak'.$x.'()
						{
							var kembay = document.getElementById("_kemampuanmembayar").value;
							var perang = '.$rows['perkiraanangsuran'].';
							var kembay = kembay.replace(",","");
							var kembay = kembay.replace(",","");
							var kembay = kembay.replace(",","");
							var kembay = kembay.replace(",","");
							var kembay = kembay.replace(",","");
							var berla = "<span style=\"color:green\">Berkas Layak Dilanjutkan</span>";
							var berlan = "<span style=\"color:red\">Berkas Perlu Dipertimbangkan Kembali</span>";
							
							if(kembay >= perang)
							{
								//alert(berla);
								document.getElementById("hasil'.$x.'").innerHTML = berla;
							}else{
								document.getElementById("hasil'.$x.'").innerHTML = berlan;
							}
						}
					</script>';
	echo 		'<span id="hasil'.$x.'">'.$kemper.'</span></td>';
	echo	'</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
		';
		$x++;
	}	
	echo '
		</table>
		<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
	';
}
?>