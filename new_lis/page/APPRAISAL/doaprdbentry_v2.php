<?php
$indra = $_REQUEST['indra'];
$indra = strtoupper($indra);

if($indra == "INSERT")
{
	include ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$type = $_POST['type'];
	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userpermission = $_POST['userpermission'];
	$buttonaction = $_POST['buttonaction'];
	
	//-------------------------------------------------------------
	/*$originalbranch = $userbranch;
	$originalregion = $userregion;
	if ($userwfid != "NOM")
	{
		$tsql = "SELECT txn_branch_code,txn_region_code FROM Tbl_FNOM
						WHERE txn_id='$custnomid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}

		if(sqlsrv_has_rows($sqlConn))
		{
	  $rowCount = sqlsrv_num_rows($sqlConn);
	  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	  {
			$originalbranch = $row[0];
			$originalregion = $row[1];
	  }
	}
	sqlsrv_free_stmt( $sqlConn );
	}

	if($userpermission=="I")
	{
	$tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid
	WHERE txn_id='$custnomid'
	AND txn_action='$userpermission'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);

	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

	if ( $sqlConn === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn))
	{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
	$rowcount = $row['b'];
	}
	}
	sqlsrv_free_stmt( $sqlConn );

	if ($rowcount <= 0)
	{
	$tsql = "INSERT INTO Tbl_F$userwfid VALUES('$custnomid','$userpermission',getdate(),'$userid','','$originalbranch','$originalregion')";
	$params = array(&$_POST['query']);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);
	if( $stmt )
	{
	} 
	else
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( sqlsrv_execute( $stmt))
	{
	}
	else
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	$tsql = "INSERT INTO Tbl_Txn_History
	VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid','$userbranch','$userregion')";
	$params = array(&$_POST['query']);

	$stmt = sqlsrv_prepare( $conn, $tsql, $params);
	if( $stmt )
	{
	} 
	else
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( sqlsrv_execute( $stmt))
	{
	}
	else
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	}
	}*/
	//-------------------------------------------------------------
	
	if($type == "TAN")
	{
		$lndap_lisregno = $_POST["lndap_lisregno"];
		$lndcol_id = $_POST["lndcol_id"];
		$lndcol_addr1 = $_POST["lndcol_addr1"];
		$lndcol_addr2 = $_POST["lndcol_addr2"];
		$lndcol_addr3 = $_POST["lndcol_addr3"];
		$lndcol_kodepos = $_POST["lndcol_kodepos"];
		$lndcol_certtype = $_POST["lndcol_certtype"];
		$lndcol_certno = $_POST["lndcol_certno"];
		$lndcol_certatasnama = $_POST["lndcol_certatasnama"];
		$lndcol_luastanah = $_POST["lndcol_luastanah"];
		$lndcol_certdate = $_POST["lndcol_certdate"];
		$lndcol_certdue = $_POST["lndcol_certdue"];
		$lndcol_relcode = $_POST["lndcol_relcode"];
		$lndcol_haktanggungan = $_POST["lndcol_haktanggungan"];
		$lndcol_haktanggungantgl = $_POST["lndcol_haktanggungantgl"];
		$lndcol_identification = $_POST["lndcol_identification"];
		$lndcol_njopyear = $_POST["lndcol_njopyear"];
		$lndcol_njopval = $_POST["lndcol_njopval"];
		$lndcol_remark = $_POST["lndcol_remark"];
		
		$lndcol_devname = "";
		if($lndcol_relcode=="DVL")
		{
			$lndcol_devname = $_POST["lndcol_devname"];
		}
		
		$lndcol_njopval = str_replace(",", "", $lndcol_njopval);
		
		if($lndcol_addr2 == "")
		{
			$lndcol_addr2 = "-";
		}	
		if($lndcol_addr3 == "")
		{
			$lndcol_addr3 = "-";
		}		
		if($lndcol_haktanggungan == "")
		{
			$lndcol_haktanggungan = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Land (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, 
				col_kodepos, col_certtype, col_certno, col_certatasnama, col_certluas, col_certdate, col_certdue, col_relcode, col_haktanggungan, 
				col_haktanggungantgl, col_identification, col_njopyear, col_njopval, col_remark, col_devname)
				VALUES('$lndap_lisregno','$lndcol_id','$lndcol_addr1','$lndcol_addr2','$lndcol_addr3',
				'$lndcol_kodepos','$lndcol_certtype','$lndcol_certno','$lndcol_certatasnama','$lndcol_luastanah','$lndcol_certdate','$lndcol_certdue','$lndcol_relcode','$lndcol_haktanggungan',
				'$lndcol_haktanggungantgl','$lndcol_identification','$lndcol_njopyear','$lndcol_njopval','$lndcol_remark', '$lndcol_devname')";
			         
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$lndcol_id'";
		$y = sqlsrv_query($conn, $ysql);
	}
	
	else if($type == "BA1")
	{
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		$lndcol_certtype = $_POST["lndbldcol_certtype"];
		$lndcol_certno = $_POST["lndbldcol_certno"];
		$lndcol_certatasnama = $_POST["lndbldcol_certatasnama"];
		$lndcol_luastanah = $_POST["lndbldcol_luastanah"];
		$lndcol_certdate = $_POST["lndbldcol_certdate"];
		$lndcol_certdue = $_POST["lndbldcol_certdue"];
		$lndcol_relcode = $_POST["lndbldcol_relcode"];
		$lndcol_haktanggungan = $_POST["lndbldcol_haktanggungan"];
		$lndcol_haktanggungantgl = $_POST["lndbldcol_haktanggungantgl"];
		$lndcol_identification = $_POST["lndbldcol_identification"];
		$lndcol_njopyear = $_POST["lndbldcol_njopyear"];
		$lndcol_njopval = $_POST["lndbldcol_njopval"];
		$lndcol_remark = $_POST["lndbldcol_remark"];
		$lndcol_gssuno = $_POST["lndbldcol_gssuno"];
		$lndcol_gssutgl = $_POST["lndbldcol_gssutgl"];
		
		$bldap_lisregno = $_POST["bldap_lisregno"];
		$bldcol_id = $_POST["bldcol_id"];
		$bldcol_addr1 = $_POST["bldcol_addr1"];
		$bldcol_addr2 = $_POST["bldcol_addr2"];
		$bldcol_addr3 = $_POST["bldcol_addr3"];
		$bldcol_kodepos = $_POST["bldcol_kodepos"];
		$bldcol_type = $_POST["bldcol_type"];
		$bldcol_imbno = $_POST["bldcol_imbno"];
		$bldcol_imbdate = $_POST["bldcol_imbdate"];
		$bldcol_imbluas = $_POST["bldcol_imbluas"];
		$bldcol_njopyear = $_POST["bldcol_njopyear"];
		$bldcol_njopval = $_POST["bldcol_njopval"];
		
		$lndap_lisregno = $bldap_lisregno;
		$lndcol_id = $bldcol_id;
		
		$bldcol_devname = "";
		if($lndcol_relcode=="DVL")
		{
			$bldcol_devname = $_POST["bldcol_devname"];
		}
		
		$lndcol_njopval = str_replace(",", "", $lndcol_njopval);
		$bldcol_njopval = str_replace(",", "", $bldcol_njopval);
		
		if($lndcol_addr2 == "")
		{
			$lndcol_addr2 = "-";
		}	
		if($lndcol_addr3 == "")
		{
			$lndcol_addr3 = "-";
		}
		if($bldcol_addr2 == "")
		{
			$bldcol_addr2 = "-";
		}	
		if($bldcol_addr3 == "")
		{
			$bldcol_addr3 = "-";
		}			
		if($bldcol_imbno == "")
		{
			$bldcol_imbno = "0";
		}
		if($bldcol_imbluas == "")
		{
			$bldcol_imbluas = "0";
		}
		if($lndcol_haktanggungan == "")
		{
			$lndcol_haktanggungan = "-";
		}
		if($lndcol_gssuno == "")
		{
			$lndcol_gssuno = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Land (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, 
					col_kodepos, col_certtype, col_certno, col_certatasnama, col_certluas, col_certdate, col_certdue, col_relcode, col_haktanggungan, 
					col_haktanggungantgl, col_identification, col_njopyear, col_njopval, col_remark, col_gssuno, col_gssutgl)
					VALUES('$lndap_lisregno','$lndcol_id','$lndcol_addr1','$lndcol_addr2','$lndcol_addr3',
					'$lndcol_kodepos','$lndcol_certtype','$lndcol_certno','$lndcol_certatasnama','$lndcol_luastanah','$lndcol_certdate','$lndcol_certdue','$lndcol_relcode','$lndcol_haktanggungan',
					'$lndcol_haktanggungantgl','$lndcol_identification','$lndcol_njopyear','$lndcol_njopval','$lndcol_remark', '$lndcol_gssuno', '$lndcol_gssutgl')
					
					INSERT INTO tbl_COL_Building (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, col_kodepos, 
					col_type, col_imbno, col_imbdate, col_imbluas, col_njopyear, col_njopval, col_devname)
					VALUES ('$bldap_lisregno', '$bldcol_id', '$bldcol_addr1', '$bldcol_addr2', '$bldcol_addr3', '$bldcol_kodepos', 
					'$bldcol_type', '$bldcol_imbno', '$bldcol_imbdate', '$bldcol_imbluas', '$bldcol_njopyear', '$bldcol_njopval', '$bldcol_devname')";
			 
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
			$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$lndcol_id'";
			$y = sqlsrv_query($conn, $ysql);
	}
	
	else if($type == "V01")
	{
		$vhcap_lisregno = $_POST["vhcap_lisregno"];
		$vhccol_id = $_POST["vhccol_id"];
		$vhccol_addr1 = $_POST["vhccol_addr1"];
		$vhccol_addr2 = $_POST["vhccol_addr2"];
		$vhccol_addr3 = $_POST["vhccol_addr3"];
		$vhccol_kodepos = $_POST["vhccol_kodepos"];
		$vhccol_nopol = $_POST["vhccol_nopol"];
		$vhccol_stnk_no = $_POST["vhccol_stnk_no"];
		$vhccol_stnkexp = $_POST["vhccol_stnkexp"];
		$vhccol_fakturno = $_POST["vhccol_fakturno"];
		$vhccol_fakturtgl = $_POST["vhccol_fakturtgl"];
		$vhccol_bpkbno = $_POST["vhccol_bpkbno"];
		$vhccol_bpkbtgl = $_POST["vhccol_bpkbtgl"];
		$vhccol_type = $_POST["vhccol_type"];
		$vhccol_model = $_POST["vhccol_model"];
		$vhccol_merk = $_POST["vhccol_merk"];
		$vhccol_tahunpembuatan = $_POST["vhccol_tahunpembuatan"];
		$vhccol_jeniskendaran = $_POST["vhccol_jeniskendaran"];
		$vhccol_silinder = $_POST["vhccol_silinder"];
		$vhccol_warna = $_POST["vhccol_warna"];
		$vhccol_norangka = $_POST["vhccol_norangka"];
		$vhccol_nomesin = $_POST["vhccol_nomesin"];
		$vhccol_MK_CODE = $_POST["vhccol_MK_CODE"];
		$vhccol_condition = $_POST["vhccol_condition"];
		$vhccol_bpkbnama = $_POST["vhccol_bpkbnama"];
		$vhccol_bpkbaddr1 = $_POST["vhccol_bpkbaddr1"];
		$vhccol_bpkbaddr2 = $_POST["vhccol_bpkbaddr2"];
		$vhccol_bpkbaddr3 = $_POST["vhccol_bpkbaddr3"];
		$vhccol_desc = $_POST["vhccol_desc"];

		if($vhccol_desc == "")
		{
			$vhccol_desc = "-";
		}
		if($vhccol_addr1 == "")
		{
			$vhccol_addr1 = "-";
		}
		if($vhccol_addr2 == "")
		{
			$vhccol_addr2 = "-";
		}			
		if($vhccol_addr3 == "")
		{
			$vhccol_addr3 = "-";
		}
		if($vhccol_bpkbaddr1 == "")
		{
			$vhccol_bpkbaddr1 = "-";
		}	
		if($vhccol_bpkbaddr2 == "")
		{
			$vhccol_bpkbaddr2 = "-";
		}	
		if($vhccol_bpkbaddr3 == "")
		{
			$vhccol_bpkbaddr3 = "-";
		}
		if($vhccol_kodepos == "")
		{
			$vhccol_kodepos = "-";
		}
		if($vhccol_type == "")
		{
			$vhccol_type = "-";
		}
		if($vhccol_model == "")
		{
			$vhccol_model = "-";
		}
		if($vhccol_merk == "")
		{
			$vhccol_merk = "-";
		}
		if($vhccol_tahunpembuatan == "")
		{
			$vhccol_tahunpembuatan = "-";
		}
		if($vhccol_jeniskendaran == "")
		{
			$vhccol_jeniskendaran = "-";
		}
		if($vhccol_silinder == "")
		{
			$vhccol_silinder = "0";
		}
		if($vhccol_warna == "")
		{
			$vhccol_warna = "-";
		}
		if($vhccol_norangka == "")
		{
			$vhccol_norangka = "0";
		}
		if($vhccol_nomesin == "")
		{
			$vhccol_nomesin = "0";
		}
		if($vhccol_MK_CODE == "")
		{
			$vhccol_MK_CODE = "0";
		}
		if($vhccol_bpkbnama == "")
		{
			$vhccol_bpkbnama = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Vehicle (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, 
					col_kodepos, col_nopol, col_stnk_no, col_stnkexp, col_fakturno, col_fakturtgl, col_bpkbno, col_bpkbtgl, 
					col_condition, col_type, 
					col_model, col_merk, col_tahunpembuatan, col_jeniskendaran, col_silinder,
					col_warna, col_norangka,col_nomesin,col_bpkbnama,col_bpkbaddr1, col_bpkbaddr2, col_bpkbaddr3, col_MK_CODE, col_desc)
					VALUES('$vhcap_lisregno','$vhccol_id','$vhccol_addr1','$vhccol_addr2','$vhccol_addr3',
					'$vhccol_kodepos','$vhccol_nopol','$vhccol_stnk_no','$vhccol_stnkexp','$vhccol_fakturno','$vhccol_fakturtgl','$vhccol_bpkbno','$vhccol_bpkbtgl',
					'$vhccol_condition','$vhccol_type',
					'$vhccol_model','$vhccol_merk','$vhccol_tahunpembuatan','$vhccol_jeniskendaran','$vhccol_silinder',
					'$vhccol_warna', '$vhccol_norangka', '$vhccol_nomesin', '$vhccol_bpkbnama', '$vhccol_bpkbaddr1', '$vhccol_bpkbaddr2', '$vhccol_bpkbaddr3','$vhccol_MK_CODE', '$vhccol_desc')";
			         $params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
			$tsql = "UPDATE Tbl_Scoring SET umur_kkb = '$vhccol_condition'
					WHERE custnomid = '$vhcap_lisregno'";
//echo $tsql;exit;
			         $params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$vhccol_id'";
		$y = sqlsrv_query($conn, $ysql);
	}
	
	else if($type == "D01" || $type == "TAB")
	{
		$cash_aplisregno = $_POST["cash_aplisregno"];
		$cash_colid = $_POST["cash_colid"];
		$cash_matauang = $_POST["cash_matauang"];
		$cash_nilai = $_POST["cash_nilai"];
		$cash_atasnama = $_POST["cash_atasnama"];
		$cash_hubungannasabah = $_POST["cash_hubungannasabah"];
		$cash_alamat1 = $_POST["cash_alamat1"];
		$cash_alamat2 = $_POST["cash_alamat2"];
		$cash_alamat3 = $_POST["cash_alamat3"];
		$cash_tanggaljatuhtempo = $_POST["cash_tanggaljatuhtempo"];
		$cover = $_POST["cover"];
		$keterangan = $_POST["keterangan"];
		$sukubunga = $_POST["cash_sukubunga"];
		$jangkawaktu = $_POST["cash_jangkawaktu"];
		
		if($type == "TAB")
		{
			$cash_noaccount = $_POST["cash_noaccount"];
			$cash_nobilyet = "-";
		}
		else if($type == "D01")
		{
			$cash_nobilyet = $_POST["cash_nobilyet"];
			$cash_noaccount = "-";
		}
		
		$cash_nilai = str_replace(",", "", $cash_nilai);
		
		if($cash_atasnama == "")
		{
			$cash_atasnama = "-";
		}
		if($cash_hubungannasabah == "")
		{
			$cash_hubungannasabah = "-";
		}
		if($cash_alamat1 == "")
		{
			$cash_alamat1 = "-";
		}
		if($cash_alamat2 == "")
		{
			$cash_alamat2 = "-";
		}
		if($cash_alamat3 == "")
		{
			$cash_alamat3 = "-";
		}
		if($sukubunga == "")
		{
			$sukubunga = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Cash (ap_lisregno, col_id, cash_type, cash_matauang, cash_nilai, cash_noaccount, cash_nobilyet, cash_atasnama, cash_hubungannasabah, cash_alamat1, cash_alamat2, cash_alamat3, cash_tanggaljatuhtempo, cash_cover, cash_keterangan, cash_sukubunga, cash_jangkawaktu)
				VALUES('$cash_aplisregno', '$cash_colid', '$type', '$cash_matauang', '$cash_nilai', '$cash_noaccount', '$cash_nobilyet', '$cash_atasnama', '$cash_hubungannasabah', '$cash_alamat1', '$cash_alamat2', '$cash_alamat3', '$cash_tanggaljatuhtempo', '$cover', '$keterangan', '$sukubunga', '$jangkawaktu')";
		echo $tsql;		
			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$cash_colid'";
		$y = sqlsrv_query($conn, $ysql);	
	}
	else if ($type == "KI2")
	{
		$kios_aplisregno = $_POST['kios_aplisregno'];
		$kios_colid = $_POST['kios_colid'];
		$kios_haktanggungan = $_POST['kios_haktanggungan'];
		$kios_haktanggungantgl = $_POST['kios_haktanggungantgl'];
		$kios_relcode = $_POST['kios_relcode'];
		$kios_njopyearlnd = $_POST['kios_njopyearlnd'];
		$kios_njopvallnd = $_POST['kios_njopvallnd'];
		$kios_keteranganlnd = $_POST['kios_keteranganlnd'];
		$kios_certno = $_POST['kios_certno'];
		$kios_certtype = $_POST['kios_certtype'];
		$kios_certluas = $_POST['kios_certluas'];
		$kios_certdate = $_POST['kios_certdate'];
		$kios_certdue = $_POST['kios_certdue'];
		$kios_certatasnama = $_POST['kios_certatasnama'];
		$kios_gssuno = $_POST['kios_gssuno'];
		$kios_gssutgl = $_POST['kios_gssutgl'];
		$kios_imbno = $_POST['kios_imbno'];
		$kios_imbdate = $_POST['kios_imbdate'];
		$kios_imbluas = $_POST['kios_imbluas'];
		$kios_njopyearbld = $_POST['kios_njopyearbld'];
		$kios_njopvalbld = $_POST['kios_njopvalbld'];
		$kios_keteranganbld = $_POST['kios_keteranganbld'];
		$kios_ppjb = $_POST['kios_ppjb'];
		
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		
		$kios_devname = "";
		if($kios_relcode=="DVL")
		{
			$kios_devname = $_POST["kios_devname"];
		}
		
		$kios_njopvallnd = str_replace(",", "", $kios_njopvallnd);
		$kios_njopvalbld = str_replace(",", "", $kios_njopvalbld);
		
		if($kios_haktanggungan == "")
		{
			$kios_haktanggungan = "-";
		}
		if($kios_keteranganlnd == "")
		{
			$kios_keteranganlnd = "-";
		}
		if($kios_certno == "")
		{
			$kios_certno = "-";
		}
		if($kios_certtype == "")
		{
			$kios_certtype = "-";
		}
		if($kios_certluas == "")
		{
			$kios_certluas = 0;
		}
		if($kios_certatasnama == "")
		{
			$kios_certatasnama = "-";
		}
		if($kios_gssuno == "")
		{
			$kios_gssuno = "-";
		}
		if($kios_keteranganbld == "")
		{
			$kios_keteranganbld = "-";
		}
		if($kios_ppjb == "")
		{
			$kios_ppjb = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Kios (ap_lisregno, col_id, col_haktanggungan, col_haktanggungantgl, col_relcode, col_njopyearlnd, col_njopvallnd, col_keteranganlnd, col_certno, col_certtype, col_certluas, col_certdate, col_certdue, col_certatasnama, col_gssuno, col_gssutgl, col_imbno, col_imbdate, col_imbluas, col_njopyearbld, col_njopvalbld, col_keteranganbld, col_ppjb, col_devname)
				VALUES('$kios_aplisregno', '$kios_colid', '$kios_haktanggungan', '$kios_haktanggungantgl', '$kios_relcode', '$kios_njopyearlnd', '$kios_njopvallnd', '$kios_keteranganlnd', '$kios_certno', '$kios_certtype', '$kios_certluas', '$kios_certdate', '$kios_certdue', '$kios_certatasnama', '$kios_gssuno', '$kios_gssutgl', '$kios_imbno', '$kios_imbdate', '$kios_imbluas', '$kios_njopyearbld', '$kios_njopvalbld', '$kios_keteranganbld', '$kios_ppjb', '$kios_devname')
				
				INSERT INTO tbl_COL_Land (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, col_kodepos)
					VALUES('$kios_aplisregno','$kios_colid','$lndcol_addr1','$lndcol_addr2','$lndcol_addr3','$lndcol_kodepos')
				";
		echo $tsql;		
			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$kios_colid'";
		$y = sqlsrv_query($conn, $ysql);	
	}
	else if ($type == "RUK")
	{
		$ruko_aplisregno = $_POST['ruko_aplisregno'];
		$ruko_colid = $_POST['ruko_colid'];
		$ruko_haktanggungan = $_POST['ruko_haktanggungan'];
		$ruko_haktanggungantgl = $_POST['ruko_haktanggungantgl'];
		$ruko_relcode = $_POST['ruko_relcode'];
		$ruko_njopyearlnd = $_POST['ruko_njopyearlnd'];
		$ruko_njopvallnd = $_POST['ruko_njopvallnd'];
		$ruko_keteranganlnd = $_POST['ruko_keteranganlnd'];
		$ruko_certno = $_POST['ruko_certno'];
		$ruko_certtype = $_POST['ruko_certtype'];
		$ruko_certluas = $_POST['ruko_certluas'];
		$ruko_certdate = $_POST['ruko_certdate'];
		$ruko_certdue = $_POST['ruko_certdue'];
		$ruko_certatasnama = $_POST['ruko_certatasnama'];
		$ruko_gssuno = $_POST['ruko_gssuno'];
		$ruko_gssutgl = $_POST['ruko_gssutgl'];
		$ruko_imbno = $_POST['ruko_imbno'];
		$ruko_imbdate = $_POST['ruko_imbdate'];
		$ruko_imbluas = $_POST['ruko_imbluas'];
		$ruko_njopyearbld = $_POST['ruko_njopyearbld'];
		$ruko_njopvalbld = $_POST['ruko_njopvalbld'];
		$ruko_keteranganbld = $_POST['ruko_keteranganbld'];
		$ruko_ppjb = $_POST['ruko_ppjb'];
		
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		
		$ruko_devname = "";
		if($ruko_relcode=="DVL")
		{
			$ruko_devname = $_POST["ruko_devname"];
		}
		
		$ruko_njopvallnd = str_replace(",", "", $ruko_njopvallnd);
		$ruko_njopvalbld = str_replace(",", "", $ruko_njopvalbld);
		
		if($ruko_haktanggungan == "")
		{
			$ruko_haktanggungan = "-";
		}
		if($ruko_keteranganlnd == "")
		{
			$ruko_keteranganlnd = "-";
		}
		if($ruko_certno == "")
		{
			$ruko_certno = "-";
		}
		if($ruko_certtype == "")
		{
			$ruko_certtype = "-";
		}
		if($ruko_certluas == "")
		{
			$ruko_certluas = 0;
		}
		if($ruko_certatasnama == "")
		{
			$ruko_certatasnama = "-";
		}
		if($ruko_gssuno == "")
		{
			$ruko_gssuno = "-";
		}
		if($ruko_keteranganbld == "")
		{
			$ruko_keteranganbld = "-";
		}
		if($ruko_ppjb == "")
		{
			$ruko_ppjb = "-";
		}
		
		$tsql = "INSERT INTO tbl_COL_Ruko (ap_lisregno, col_id, col_haktanggungan, col_haktanggungantgl, col_relcode, col_njopyearlnd, col_njopvallnd, col_keteranganlnd, col_certno, col_certtype, col_certluas, col_certdate, col_certdue, col_certatasnama, col_gssuno, col_gssutgl, col_imbno, col_imbdate, col_imbluas, col_njopyearbld, col_njopvalbld, col_keteranganbld, col_ppjb, col_devname)
				VALUES('$ruko_aplisregno', '$ruko_colid', '$ruko_haktanggungan', '$ruko_haktanggungantgl', '$ruko_relcode', '$ruko_njopyearlnd', '$ruko_njopvallnd', '$ruko_keteranganlnd', '$ruko_certno', '$ruko_certtype', '$ruko_certluas', '$ruko_certdate', '$ruko_certdue', '$ruko_certatasnama', '$ruko_gssuno', '$ruko_gssutgl', '$ruko_imbno', '$ruko_imbdate', '$ruko_imbluas', '$ruko_njopyearbld', '$ruko_njopvalbld', '$ruko_keteranganbld', '$ruko_ppjb', '$ruko_devname')
				
				INSERT INTO tbl_COL_Land (ap_lisregno, col_id, col_addr1, col_addr2, col_addr3, col_kodepos)
					VALUES('$ruko_aplisregno','$ruko_colid','$lndcol_addr1','$lndcol_addr2','$lndcol_addr3','$lndcol_kodepos')
				";
		//echo $tsql;		
			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$ruko_colid'";
		$y = sqlsrv_query($conn, $ysql);	
	}
	else
	{
		$lainnyaap_lisregno = $_POST['lainnyaap_lisregno'];
		$lainnyacol_id = $_POST['lainnyacol_id'];
		$lainnyacol_code = $type;
		$lainnyacol_nomordokumen = $_POST['lainnyacol_nomordokumen'];
		$lainnyacol_nilaijaminan = $_POST['lainnyacol_nilaijaminan'];
		$lainnyacol_nilaijaminan = str_replace(",", "", $lainnyacol_nilaijaminan);
		
		$tsql = "INSERT INTO tbl_COL_Lainnya (ap_lisregno, col_id, col_code, col_nomordokumen, col_nilaijaminan)
				VALUES('$lainnyaap_lisregno', '$lainnyacol_id', '$lainnyacol_code', '$lainnyacol_nomordokumen', '$lainnyacol_nilaijaminan')";
		echo $tsql;		
			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
		$ysql = "UPDATE Tbl_Cust_MasterCol SET FLAGINSERT = '1' WHERE COL_ID = '$lainnyacol_id'";
		$y = sqlsrv_query($conn, $ysql);	
	}
	
	$tsqlz = "UPDATE Tbl_Flow_Status SET txn_flow = 'COL', txn_action = 'I', txn_time = getdate(), txn_user_id = '$userid' WHERE txn_id = '$custnomid'";
	$az = sqlsrv_query($conn, $tsqlz);
	
	require ("../../requirepage/do_saveflow.php");
	
	Header("Location:aprdbentry_v2.php?userwfid=$userwfid&custnomid=$custnomid&tab=edit&userpermission=$userpermission&buttonaction=$buttonaction");
}
else if ($indra == "UPDATE")
{
	include ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	
	$type = $_POST['type'];
	$custnomid=$_POST['custnomid'];
	$userid=$_POST['userid'];
	$userpwd=$_POST['userpwd'];
	$userbranch=$_POST['userbranch'];
	$userregion=$_POST['userregion'];
	$userwfid=$_POST['userwfid'];
	$userpermission=$_POST['userpermission'];
	$buttonaction=$_POST['buttonaction'];
	
	if($type == "TAN")
	{
		$lndap_lisregno = $_POST["lndap_lisregno"];
		$lndcol_id = $_POST["lndcol_id"];
		$lndcol_addr1 = $_POST["lndcol_addr1"];
		$lndcol_addr2 = $_POST["lndcol_addr2"];
		$lndcol_addr3 = $_POST["lndcol_addr3"];
		$lndcol_kodepos = $_POST["lndcol_kodepos"];
		$lndcol_certtype = $_POST["lndcol_certtype"];
		$lndcol_certno = $_POST["lndcol_certno"];
		$lndcol_certatasnama = $_POST["lndcol_certatasnama"];
		$lndcol_luastanah = $_POST["lndcol_luastanah"];
		$lndcol_certdate = $_POST["lndcol_certdate"];
		$lndcol_certdue = $_POST["lndcol_certdue"];
		$lndcol_relcode = $_POST["lndcol_relcode"];
		$lndcol_haktanggungan = $_POST["lndcol_haktanggungan"];
		$lndcol_haktanggungantgl = $_POST["lndcol_haktanggungantgl"];
		$lndcol_identification = $_POST["lndcol_identification"];
		$lndcol_njopyear = $_POST["lndcol_njopyear"];
		$lndcol_njopval = $_POST["lndcol_njopval"];
		$lndcol_remark = $_POST["lndcol_remark"];
		
		$lndcol_devname = "";
		if($lndcol_relcode=="DVL")
		{
			$lndcol_devname = $_POST["lndcol_devname"];
		}
		
		$lndcol_njopval = str_replace(",", "", $lndcol_njopval);
		if($lndcol_addr2 == "")
		{
			$lndcol_addr2 = "-";
		}	
		if($lndcol_addr3 == "")
		{
			$lndcol_addr3 = "-";
		}		
		if($lndcol_haktanggungan == "")
		{
			$lndcol_haktanggungan = "-";
		}

		$tsql = "UPDATE tbl_COL_Land SET col_addr1 = '$lndcol_addr1', col_addr2 = '$lndcol_addr2', col_addr3 = '$lndcol_addr3', 
					col_kodepos = '$lndcol_kodepos', col_certtype = '$lndcol_certtype', col_certno = '$lndcol_certno', col_certatasnama = '$lndcol_certatasnama', 
					col_certluas = '$lndcol_luastanah', col_certdate = '$lndcol_certdate', col_certdue = '$lndcol_certdue', col_relcode = '$lndcol_relcode', col_haktanggungan = '$lndcol_haktanggungan', 
					col_haktanggungantgl = '$lndcol_haktanggungantgl', col_identification = '$lndcol_identification', col_njopyear = '$lndcol_njopyear', col_njopval = '$lndcol_njopval', col_remark = '$lndcol_remark', col_devname = '$lndcol_devname'
					WHERE ap_lisregno = '$lndap_lisregno' AND col_id = '$lndcol_id'";

			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);		
	}
	else if ($type == "BA1")
	{
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		$lndcol_certtype = $_POST["lndbldcol_certtype"];
		$lndcol_certno = $_POST["lndbldcol_certno"];
		$lndcol_certatasnama = $_POST["lndbldcol_certatasnama"];
		$lndcol_luastanah = $_POST["lndbldcol_luastanah"];
		$lndcol_certdate = $_POST["lndbldcol_certdate"];
		$lndcol_certdue = $_POST["lndbldcol_certdue"];
		$lndcol_relcode = $_POST["lndbldcol_relcode"];
		$lndcol_haktanggungan = $_POST["lndbldcol_haktanggungan"];
		$lndcol_haktanggungantgl = $_POST["lndbldcol_haktanggungantgl"];
		$lndcol_identification = $_POST["lndbldcol_identification"];
		$lndcol_njopyear = $_POST["lndbldcol_njopyear"];
		$lndcol_njopval = $_POST["lndbldcol_njopval"];
		$lndcol_remark = $_POST["lndbldcol_remark"];
		$lndcol_gssuno = $_POST["lndbldcol_gssuno"];
		$lndcol_gssutgl = $_POST["lndbldcol_gssutgl"];
		
		$bldap_lisregno = $_POST["bldap_lisregno"];
		$bldcol_id = $_POST["bldcol_id"];
		$bldcol_addr1 = $_POST["bldcol_addr1"];
		$bldcol_addr2 = $_POST["bldcol_addr2"];
		$bldcol_addr3 = $_POST["bldcol_addr3"];
		$bldcol_kodepos = $_POST["bldcol_kodepos"];
		$bldcol_type = $_POST["bldcol_type"];
		$bldcol_imbno = $_POST["bldcol_imbno"];
		$bldcol_imbdate = $_POST["bldcol_imbdate"];
		$bldcol_imbluas = $_POST["bldcol_imbluas"];
		$bldcol_njopyear = $_POST["bldcol_njopyear"];
		$bldcol_njopval = $_POST["bldcol_njopval"];
		
		$bldcol_devname = "";
		if($lndcol_relcode=="DVL")
		{
			$bldcol_devname = $_POST["bldcol_devname"];
		}
		
		$lndap_lisregno = $bldap_lisregno;
		$lndcol_id = $bldcol_id;
		
		$lndcol_njopval = str_replace(",", "", $lndcol_njopval);
		$bldcol_njopval = str_replace(",", "", $bldcol_njopval);
		
		if($lndcol_addr2 == "")
		{
			$lndcol_addr2 = "-";
		}	
		if($lndcol_addr3 == "")
		{
			$lndcol_addr3 = "-";
		}
		if($bldcol_addr2 == "")
		{
			$bldcol_addr2 = "-";
		}	
		if($bldcol_addr3 == "")
		{
			$bldcol_addr3 = "-";
		}			
		if($bldcol_imbno == "")
		{
			$bldcol_imbno = "0";
		}
		if($bldcol_imbluas == "")
		{
			$bldcol_imbluas = "0";
		}
		if($lndcol_haktanggungan == "")
		{
			$lndcol_haktanggungan = "-";
		}
		if($lndcol_gssuno == "")
		{
			$lndcol_gssuno = "-";
		}
		
		$tsql = "UPDATE tbl_COL_Land SET col_addr1 = '$lndcol_addr1', col_addr2 = '$lndcol_addr2', col_addr3 = '$lndcol_addr3', 
					col_kodepos = '$lndcol_kodepos', col_certtype = '$lndcol_certtype', col_certno = '$lndcol_certno', col_certatasnama = '$lndcol_certatasnama', 
					col_certluas = '$lndcol_luastanah', col_certdate = '$lndcol_certdate', col_certdue = '$lndcol_certdue', col_relcode = '$lndcol_relcode', col_haktanggungan = '$lndcol_haktanggungan', 
					col_haktanggungantgl = '$lndcol_haktanggungantgl', col_identification = '$lndcol_identification', col_njopyear = '$lndcol_njopyear', col_njopval = '$lndcol_njopval', col_remark = '$lndcol_remark',
					col_gssuno = '$lndcol_gssuno', col_gssutgl = '$lndcol_gssutgl'
					WHERE ap_lisregno = '$lndap_lisregno' AND col_id = '$lndcol_id'
					
					UPDATE tbl_COL_Building SET  col_addr1 = '$bldcol_addr1', col_addr2 = '$bldcol_addr2', col_addr3 = '$bldcol_addr3', col_kodepos = '$bldcol_kodepos', 
					col_type = '$bldcol_type', col_imbno = '$bldcol_imbno', col_imbdate = '$bldcol_imbdate', col_imbluas = '$bldcol_imbluas', col_njopyear = '$bldcol_njopyear', col_njopval = '$bldcol_njopval', col_devname = '$bldcol_devname'
					WHERE ap_lisregno = '$bldap_lisregno' AND col_id = '$bldcol_id'";
			 
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	else if ($type == "V01")
	{
		$vhcap_lisregno = $_POST["vhcap_lisregno"];
		$vhccol_id = $_POST["vhccol_id"];
		$vhccol_addr1 = $_POST["vhccol_addr1"];
		$vhccol_addr2 = $_POST["vhccol_addr2"];
		$vhccol_addr3 = $_POST["vhccol_addr3"];
		$vhccol_kodepos = $_POST["vhccol_kodepos"];
		$vhccol_nopol = $_POST["vhccol_nopol"];
		$vhccol_stnk_no = $_POST["vhccol_stnk_no"];
		$vhccol_stnkexp = $_POST["vhccol_stnkexp"];
		$vhccol_fakturno = $_POST["vhccol_fakturno"];
		$vhccol_fakturtgl = $_POST["vhccol_fakturtgl"];
		$vhccol_bpkbno = $_POST["vhccol_bpkbno"];
		$vhccol_bpkbtgl = $_POST["vhccol_bpkbtgl"];
		$vhccol_type = $_POST["vhccol_type"];
		$vhccol_model = $_POST["vhccol_model"];
		$vhccol_merk = $_POST["vhccol_merk"];
		$vhccol_tahunpembuatan = $_POST["vhccol_tahunpembuatan"];
		$vhccol_jeniskendaran = $_POST["vhccol_jeniskendaran"];
		$vhccol_silinder = $_POST["vhccol_silinder"];
		$vhccol_warna = $_POST["vhccol_warna"];
		$vhccol_norangka = $_POST["vhccol_norangka"];
		$vhccol_nomesin = $_POST["vhccol_nomesin"];
		$vhccol_MK_CODE = $_POST["vhccol_MK_CODE"];
		$vhccol_condition = $_POST["vhccol_condition"];
		$vhccol_bpkbnama = $_POST["vhccol_bpkbnama"];
		$vhccol_bpkbaddr1 = $_POST["vhccol_bpkbaddr1"];
		$vhccol_bpkbaddr2 = $_POST["vhccol_bpkbaddr2"];
		$vhccol_bpkbaddr3 = $_POST["vhccol_bpkbaddr3"];
		$vhccol_desc = $_POST["vhccol_desc"];
		
		if($vhccol_desc == "")
		{
			$vhccol_desc = "-";
		}
		if($vhccol_addr1 == "")
		{
			$vhccol_addr1 = "-";
		}
		if($vhccol_addr2 == "")
		{
			$vhccol_addr2 = "-";
		}			
		if($vhccol_addr3 == "")
		{
			$vhccol_addr3 = "-";
		}
		if($vhccol_bpkbaddr1 == "")
		{
			$vhccol_bpkbaddr1 = "-";
		}	
		if($vhccol_bpkbaddr2 == "")
		{
			$vhccol_bpkbaddr2 = "-";
		}	
		if($vhccol_bpkbaddr3 == "")
		{
			$vhccol_bpkbaddr3 = "-";
		}
		if($vhccol_kodepos == "")
		{
			$vhccol_kodepos = "-";
		}
		if($vhccol_type == "")
		{
			$vhccol_type = "-";
		}
		if($vhccol_model == "")
		{
			$vhccol_model = "-";
		}
		if($vhccol_merk == "")
		{
			$vhccol_merk = "-";
		}
		if($vhccol_tahunpembuatan == "")
		{
			$vhccol_tahunpembuatan = "-";
		}
		if($vhccol_jeniskendaran == "")
		{
			$vhccol_jeniskendaran = "-";
		}
		if($vhccol_silinder == "")
		{
			$vhccol_silinder = "0";
		}
		if($vhccol_warna == "")
		{
			$vhccol_warna = "-";
		}
		if($vhccol_norangka == "")
		{
			$vhccol_norangka = "0";
		}
		if($vhccol_nomesin == "")
		{
			$vhccol_nomesin = "0";
		}
		if($vhccol_MK_CODE == "")
		{
			$vhccol_MK_CODE = "0";
		}
		if($vhccol_bpkbnama == "")
		{
			$vhccol_bpkbnama = "-";
		}
		
		$tsql = "UPDATE tbl_COL_Vehicle SET col_addr1 = '$vhccol_addr1', col_addr2 = '$vhccol_addr2', col_addr3 = '$vhccol_addr3', 
					col_kodepos = '$vhccol_kodepos', col_nopol = '$vhccol_nopol', col_stnk_no = '$vhccol_stnk_no', col_stnkexp = '$vhccol_stnkexp', 
					col_fakturno = '$vhccol_fakturno', col_fakturtgl = '$vhccol_fakturtgl', col_bpkbno = '$vhccol_bpkbno', col_bpkbtgl = '$vhccol_bpkbtgl', 
					col_condition = '$vhccol_condition',col_type = '$vhccol_type', 
					col_model = '$vhccol_model', col_merk = '$vhccol_merk', col_tahunpembuatan = '$vhccol_tahunpembuatan', col_jeniskendaran = '$vhccol_jeniskendaran', 
					col_silinder = '$vhccol_silinder', col_warna = '$vhccol_warna', col_norangka = '$vhccol_norangka',col_nomesin = '$vhccol_nomesin',
					col_bpkbnama = '$vhccol_bpkbnama',col_bpkbaddr1 = '$vhccol_bpkbaddr1', col_bpkbaddr2 = '$vhccol_bpkbaddr2', col_bpkbaddr3 = '$vhccol_bpkbaddr3',
					col_MK_CODE='$vhccol_MK_CODE', col_desc = '$vhccol_desc'
					WHERE ap_lisregno = '$vhcap_lisregno' AND col_id = '$vhccol_id'";

			         $params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
			
			$tsql = "UPDATE Tbl_Scoring SET umur_kkb = '$vhccol_condition'
					WHERE custnomid = '$vhcap_lisregno'";
//echo $tsql;exit;
			         $params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	else if($type == "D01" || $type == "TAB")
	{
		$cash_aplisregno = $_POST["cash_aplisregno"];
		$cash_colid = $_POST["cash_colid"];
		$cash_matauang = $_POST["cash_matauang"];
		$cash_nilai = $_POST["cash_nilai"];
		$cash_atasnama = $_POST["cash_atasnama"];
		$cash_hubungannasabah = $_POST["cash_hubungannasabah"];
		$cash_alamat1 = $_POST["cash_alamat1"];
		$cash_alamat2 = $_POST["cash_alamat2"];
		$cash_alamat3 = $_POST["cash_alamat3"];
		$cash_tanggaljatuhtempo = $_POST["cash_tanggaljatuhtempo"];
		$cash_cover = $_POST["cover"];
		$cash_keterangan = $_POST["keterangan"];
		$sukubunga = $_POST["cash_sukubunga"];
		$jangkawaktu = $_POST["cash_jangkawaktu"];
		
		if($type == "TAB")
		{
			$cash_noaccount = $_POST["cash_noaccount"];
			$cash_nobilyet = "-";
		}
		else if($type == "D01")
		{
			$cash_nobilyet = $_POST["cash_nobilyet"];
			$cash_noaccount = "-";
		}
		
		$cash_nilai = str_replace(",", "", $cash_nilai);
		
		if($cash_atasnama == "")
		{
			$cash_atasnama = "-";
		}
		if($cash_hubungannasabah == "")
		{
			$cash_hubungannasabah = "-";
		}
		if($cash_alamat1 == "")
		{
			$cash_alamat1 = "-";
		}
		if($cash_alamat2 == "")
		{
			$cash_alamat2 = "-";
		}
		if($cash_alamat3 == "")
		{
			$cash_alamat3 = "-";
		}
		if($sukubunga == "")
		{
			$sukubunga = "-";
		}
		
		$tsql = "UPDATE tbl_COL_Cash SET cash_nobilyet = '$cash_nobilyet', cash_noaccount = '$cash_noaccount', cash_sukubunga = '$sukubunga', cash_jangkawaktu = '$jangkawaktu', cash_matauang = '$cash_matauang', cash_nilai = '$cash_nilai', cash_atasnama = '$cash_atasnama', cash_hubungannasabah = '$cash_hubungannasabah',
				cash_alamat1 = '$cash_alamat1', cash_alamat2 = '$cash_alamat2', cash_alamat3 = '$cash_alamat3', cash_tanggaljatuhtempo = '$cash_tanggaljatuhtempo', cash_cover = '$cash_cover', cash_keterangan = '$cash_keterangan' WHERE ap_lisregno = '$cash_aplisregno' AND col_id = '$cash_colid'";
				
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	else if ($type == "KI2")
	{
		$kios_aplisregno = $_POST['kios_aplisregno'];
		$kios_colid = $_POST['kios_colid'];
		$kios_haktanggungan = $_POST['kios_haktanggungan'];
		$kios_haktanggungantgl = $_POST['kios_haktanggungantgl'];
		$kios_relcode = $_POST['kios_relcode'];
		$kios_njopyearlnd = $_POST['kios_njopyearlnd'];
		$kios_njopvallnd = $_POST['kios_njopvallnd'];
		$kios_keteranganlnd = $_POST['kios_keteranganlnd'];
		$kios_certno = $_POST['kios_certno'];
		$kios_certtype = $_POST['kios_certtype'];
		$kios_certluas = $_POST['kios_certluas'];
		$kios_certdate = $_POST['kios_certdate'];
		$kios_certdue = $_POST['kios_certdue'];
		$kios_certatasnama = $_POST['kios_certatasnama'];
		$kios_gssuno = $_POST['kios_gssuno'];
		$kios_gssutgl = $_POST['kios_gssutgl'];
		$kios_imbno = $_POST['kios_imbno'];
		$kios_imbdate = $_POST['kios_imbdate'];
		$kios_imbluas = $_POST['kios_imbluas'];
		$kios_njopyearbld = $_POST['kios_njopyearbld'];
		$kios_njopvalbld = $_POST['kios_njopvalbld'];
		$kios_keteranganbld = $_POST['kios_keteranganbld'];
		$kios_ppjb = $_POST['kios_ppjb'];
		
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		
		$kios_devname = "";
		if($kios_relcode=="DVL")
		{
			$kios_devname = $_POST["kios_devname"];
		}
		
		$kios_njopvallnd = str_replace(",", "", $kios_njopvallnd);
		$kios_njopvalbld = str_replace(",", "", $kios_njopvalbld);
		
		if($kios_haktanggungan == "")
		{
			$kios_haktanggungan = "-";
		}
		if($kios_keteranganlnd == "")
		{
			$kios_keteranganlnd = "-";
		}
		if($kios_certno == "")
		{
			$kios_certno = "-";
		}
		if($kios_certtype == "")
		{
			$kios_certtype = "-";
		}
		if($kios_certluas == "")
		{
			$kios_certluas = 0;
		}
		if($kios_certatasnama == "")
		{
			$kios_certatasnama = "-";
		}
		if($kios_gssuno == "")
		{
			$kios_gssuno = "-";
		}
		if($kios_keteranganbld == "")
		{
			$kios_keteranganbld = "-";
		}
		if($kios_ppjb == "")
		{
			$kios_ppjb = "-";
		}
		
		$tsql = "UPDATE tbl_COL_Kios SET col_haktanggungan = '$kios_haktanggungan', col_haktanggungantgl = '$kios_haktanggungantgl', col_relcode = '$kios_relcode', col_njopyearlnd = '$kios_njopyearlnd', col_njopvallnd = '$kios_njopvallnd', col_keteranganlnd = '$kios_keteranganlnd', col_certno = '$kios_certno', col_certtype = '$kios_certtype', col_certluas = '$kios_certluas', col_certdate = '$kios_certdate', col_certdue = '$kios_certdue', col_certatasnama = '$kios_certatasnama', col_gssuno = '$kios_gssuno', col_gssutgl = '$kios_gssutgl', col_imbno = '$kios_imbno', col_imbdate = '$kios_imbdate', col_imbluas = '$kios_imbluas', col_njopyearbld = '$kios_njopyearbld', col_njopvalbld = '$kios_njopvalbld', col_keteranganbld = '$kios_keteranganbld', col_ppjb = '$kios_ppjb', col_devname = '$kios_devname'
				WHERE ap_lisregno = '$kios_aplisregno' AND col_id = '$kios_colid'
				
				UPDATE tbl_COL_Land SET col_addr1 = '$lndcol_addr1', col_addr2 = '$lndcol_addr2', col_addr3 = '$lndcol_addr3', 
					col_kodepos = '$lndcol_kodepos'
					WHERE ap_lisregno = '$kios_aplisregno' AND col_id = '$kios_colid'
				";
				
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	else if ($type == "RUK")
	{
		$ruko_aplisregno = $_POST['ruko_aplisregno'];
		$ruko_colid = $_POST['ruko_colid'];
		$ruko_haktanggungan = $_POST['ruko_haktanggungan'];
		$ruko_haktanggungantgl = $_POST['ruko_haktanggungantgl'];
		$ruko_relcode = $_POST['ruko_relcode'];
		$ruko_njopyearlnd = $_POST['ruko_njopyearlnd'];
		$ruko_njopvallnd = $_POST['ruko_njopvallnd'];
		$ruko_keteranganlnd = $_POST['ruko_keteranganlnd'];
		$ruko_certno = $_POST['ruko_certno'];
		$ruko_certtype = $_POST['ruko_certtype'];
		$ruko_certluas = $_POST['ruko_certluas'];
		$ruko_certdate = $_POST['ruko_certdate'];
		$ruko_certdue = $_POST['ruko_certdue'];
		$ruko_certatasnama = $_POST['ruko_certatasnama'];
		$ruko_gssuno = $_POST['ruko_gssuno'];
		$ruko_gssutgl = $_POST['ruko_gssutgl'];
		$ruko_imbno = $_POST['ruko_imbno'];
		$ruko_imbdate = $_POST['ruko_imbdate'];
		$ruko_imbluas = $_POST['ruko_imbluas'];
		$ruko_njopyearbld = $_POST['ruko_njopyearbld'];
		$ruko_njopvalbld = $_POST['ruko_njopvalbld'];
		$ruko_keteranganbld = $_POST['ruko_keteranganbld'];
		$ruko_ppjb = $_POST['ruko_ppjb'];
		
		$lndcol_addr1 = $_POST["lndbldcol_addr1"];
		$lndcol_addr2 = $_POST["lndbldcol_addr2"];
		$lndcol_addr3 = $_POST["lndbldcol_addr3"];
		$lndcol_kodepos = $_POST["lndbldcol_kodepos"];
		
		$ruko_devname = "";
		if($ruko_relcode=="DVL")
		{
			$ruko_devname = $_POST["ruko_devname"];
		}
		
		$ruko_njopvallnd = str_replace(",", "", $ruko_njopvallnd);
		$ruko_njopvalbld = str_replace(",", "", $ruko_njopvalbld);
		
		if($ruko_haktanggungan == "")
		{
			$ruko_haktanggungan = "-";
		}
		if($ruko_keteranganlnd == "")
		{
			$ruko_keteranganlnd = "-";
		}
		if($ruko_certno == "")
		{
			$ruko_certno = "-";
		}
		if($ruko_certtype == "")
		{
			$ruko_certtype = "-";
		}
		if($ruko_certluas == "")
		{
			$ruko_certluas = 0;
		}
		if($ruko_certatasnama == "")
		{
			$ruko_certatasnama = "-";
		}
		if($ruko_gssuno == "")
		{
			$ruko_gssuno = "-";
		}
		if($ruko_keteranganbld == "")
		{
			$ruko_keteranganbld = "-";
		}
		if($ruko_ppjb == "")
		{
			$ruko_ppjb = "-";
		}
		
		$tsql = "UPDATE tbl_COL_Ruko SET col_haktanggungan = '$ruko_haktanggungan', col_haktanggungantgl = '$ruko_haktanggungantgl', col_relcode = '$ruko_relcode', col_njopyearlnd = '$ruko_njopyearlnd', col_njopvallnd = '$ruko_njopvallnd', col_keteranganlnd = '$ruko_keteranganlnd', col_certno = '$ruko_certno', col_certtype = '$ruko_certtype', col_certluas = '$ruko_certluas', col_certdate = '$ruko_certdate', col_certdue = '$ruko_certdue', col_certatasnama = '$ruko_certatasnama', col_gssuno = '$ruko_gssuno', col_gssutgl = '$ruko_gssutgl', col_imbno = '$ruko_imbno', col_imbdate = '$ruko_imbdate', col_imbluas = '$ruko_imbluas', col_njopyearbld = '$ruko_njopyearbld', col_njopvalbld = '$ruko_njopvalbld', col_keteranganbld = '$ruko_keteranganbld', col_ppjb = '$ruko_ppjb', col_devname = '$ruko_devname'
				WHERE ap_lisregno = '$ruko_aplisregno' AND col_id = '$ruko_colid'
				
				UPDATE tbl_COL_Land SET col_addr1 = '$lndcol_addr1', col_addr2 = '$lndcol_addr2', col_addr3 = '$lndcol_addr3', 
					col_kodepos = '$lndcol_kodepos'
					WHERE ap_lisregno = '$ruko_aplisregno' AND col_id = '$ruko_colid'
				";
				
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	else
	{
		$lainnyaap_lisregno = $_POST['lainnyaap_lisregno'];
		$lainnyacol_id = $_POST['lainnyacol_id'];
		$lainnyacol_code = $type;
		$lainnyacol_nomordokumen = $_POST['lainnyacol_nomordokumen'];
		$lainnyacol_nilaijaminan = $_POST['lainnyacol_nilaijaminan'];
		
		$tsql = "UPDATE tbl_COL_Lainnya SET col_nomordokumen = '$lainnyacol_nomordokumen', col_nilaijaminan = '$lainnyacol_nilaijaminan'
				 WHERE ap_lisregno = '$lainnyaap_lisregno' AND col_id = '$lainnyacol_id'";
				
			$params = array(&$_POST['query']);

			$stmt = sqlsrv_prepare( $conn, $tsql, $params);
			if( $stmt )
			{
			} 
			else
			{
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			}

			if( sqlsrv_execute( $stmt))
			{
			}
			else
			{
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			}
			sqlsrv_free_stmt( $stmt);
	}
	
	$tsqlz = "UPDATE Tbl_Flow_Status SET txn_flow = 'COL', txn_action = 'I', txn_time = getdate(), txn_user_id = '$userid' WHERE txn_id = '$custnomid'";
	$az = sqlsrv_query($conn, $tsqlz);
	
	require ("../../requirepage/do_saveflow.php");
	
	Header("Location:aprdbentry_v2.php?userwfid=$userwfid&custnomid=$custnomid&tab=edit&userpermission=$userpermission&buttonaction=$buttonaction");
}
else if ($indra == "CHECK")
{
	require ("../../lib/open_con.php");

	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userpermission = $_POST['userpermission'];
	$buttonaction = $_POST['buttonaction'];
	
	
	
	$tsql5 = "UPDATE Tbl_CustomerFlag SET custflagapr = 'R' WHERE custnomid = '$custnomid' and custflagapr = 'N'";
	$a5 = sqlsrv_query($conn, $tsql5);
	
	include ("../../requirepage/do_saveflow.php");
	
	Header("Location:../formCOL.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&ButtonAction=$buttonaction");
	
	//Header("Location:flowdataentry.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}
else if ($indra == "APPROVE")
{
	require ("../../lib/open_con.php");

	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userwfid = "COL";
	$userpermission = "A";
	$buttonaction = $_POST['buttonaction'];
	$notes = $_POST['notes'];
	
	
	$tsql51 = "UPDATE Tbl_FCOL SET txn_action = 'A', txn_notes='$notes' WHERE txn_id = '$custnomid' AND txn_action='C'";
	$a51 = sqlsrv_query($conn, $tsql51);
	
	
	$tsql5 = "UPDATE Tbl_CustomerFlag SET custflagapr = 'D' WHERE custnomid = '$custnomid' and custflagapr = 'R'";
	$a5 = sqlsrv_query($conn, $tsql5);
	
	// GET ORIGINAL BRANCH
	$originalregion = $userregion;
	$originalbranch = $userbranch;
	$originalao = $userid;
	if ($userwfid != "START")
	{
		$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
						WHERE txn_id='$custnomid'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}

		if(sqlsrv_has_rows($sqlConn))
		{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$originalbranch = $row[0];
			$originalregion = $row[1];
			$originalao = $row[2];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	}
	
	$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
	$c = sqlsrv_query($conn, $tsqlc);
	
	//include ("../../requirepage/do_saveflow.php");
	Header("Location:../formCOL.php?userwfid=$userwfid&userpermission=$userpermission&ButtonAction=$buttonaction");
	//Header("Location:flowdataentry.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}
else if ($indra == "VIEWPIC")
{
	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userpermission = $_POST['userpermission'];
	$buttonaction = $_POST['buttonaction'];
	Header("Location:viewaprimage_v2.php?custnomid=$custnomid&userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
}
else if ($indra == "VIEWPICRESPONSE")
{
	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userpermission = $_POST['userpermission'];
	$buttonaction = $_POST['buttonaction'];
	Header("Location:viewresponseaprimage_v2.php?custnomid=$custnomid&userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
}
else if ($indra == "NILAIPBB")
{
	include ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	
	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = $_POST['userwfid'];
	$userpermission = $_POST['userpermission'];
	$buttonaction = $_POST['buttonaction'];
	
	$ap_lisregno =  $_POST['ap_lisregno'];
	$col_id =  $_POST['col_id'];
	$luas_tanah =  $_POST['luas_tanah'];
	$harga_permeter_tanah =  str_replace(",", "",$_POST['harga_permeter_tanah']);
	$luas_bangunan =  $_POST['luas_bangunan'];
	$harga_permeter_bangunan =  str_replace(",", "", $_POST['harga_permeter_bangunan']);

	$lndcol_njopyear =  $_POST['lndcol_njopyear'];
	$lndcol_njopval =  str_replace(",", "", $_POST['bldcol_njopval']);
	$bldcol_njopyear =  $_POST['lndcol_njopyear'];
	$bldcol_njopval =  str_replace(",", "", $_POST['bldcol_njopval']);

					$custjeniscol = "";
					$tsqlvehicle = "SELECT cust_jeniscol FROM Tbl_Cust_MasterCol where ap_lisregno = '$ap_lisregno' AND col_id = '$col_id'";
					$avehicle = sqlsrv_query($conn, $tsqlvehicle);
					if ( $avehicle === false)
						die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($avehicle))
					{  
						if($rowvehicle = sqlsrv_fetch_array($avehicle, SQLSRV_FETCH_ASSOC))
						{ 
							$custjeniscol = $rowvehicle['cust_jeniscol'];
						}
					}
	
	$tsql51 = "DELETE FROM tbl_col_nilaipbb WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	$a51 = sqlsrv_query($conn, $tsql51);
	
	$tsql51 = "INSERT INTO tbl_col_nilaipbb (ap_lisregno, col_id, luas_tanah, harga_permeter_tanah, luas_bangunan, harga_permeter_bangunan) VALUES ('$ap_lisregno', '$col_id', '$luas_tanah', '$harga_permeter_tanah', '$luas_bangunan', '$harga_permeter_bangunan')";
	$a51 = sqlsrv_query($conn, $tsql51);

  if ($custjeniscol == "BA1")
  {
	   $tsql51 = "UPDATE tbl_COL_Land set col_njopyear='$lndcol_njopyear', col_njopval='$lndcol_njopval' WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	   $a51 = sqlsrv_query($conn, $tsql51);

	   $tsql51 = "UPDATE tbl_COL_Building set col_njopyear='$bldcol_njopyear', col_njopval='$bldcol_njopval' WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	   $a51 = sqlsrv_query($conn, $tsql51);
	}

  if ($custjeniscol == "TAN")
  {
	   $tsql51 = "UPDATE tbl_COL_Land set col_njopyear='$lndcol_njopyear', col_njopval='$lndcol_njopval' WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	   $a51 = sqlsrv_query($conn, $tsql51);
	}

  if ($custjeniscol == "RUK")
  {
	   $tsql51 = "UPDATE tbl_COL_Ruko set col_njopyearlnd='$lndcol_njopyear', col_njopvallnd='$lndcol_njopval', col_njopyearbld='$bldcol_njopyear', col_njopvalbld='$bldcol_njopval' WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	   $a51 = sqlsrv_query($conn, $tsql51);
	}

  if ($custjeniscol == "KI2")
  {
	   $tsql51 = "UPDATE tbl_COL_Kios set col_njopyearlnd='$lndcol_njopyear', col_njopvallnd='$lndcol_njopval', col_njopyearbld='$bldcol_njopyear', col_njopvalbld='$bldcol_njopval' WHERE ap_lisregno = '$ap_lisregno' and col_id = '$col_id'";
	   $a51 = sqlsrv_query($conn, $tsql51);
	}
	
	Header("Location:aprdbentry_v2.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
}
else if ($indra == "APPROVERESPONSE")
{
	require ("../../lib/open_con.php");

	$custnomid=$_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$userwfid = "APR";
	$userpermission = 'A';
	$buttonaction = $_POST['buttonaction'];

	$tsql = "SELECT TXN_USER_ID from TBL_FSTART WHERE TXN_ID = '$custnomid'";

	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  

		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$userid = $row["TXN_USER_ID"];
		}
	}
	
	$tsql = "SELECT * FROM Tbl_FAPR where txn_id = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($a))
	{  
		$tsqlc = "DELETE FROM Tbl_FAPR WHERE txn_id = '$custnomid'";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
		
		$tsqlc = "INSERT INTO Tbl_FAPR ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code]) 
		VALUES ('$custnomid','$userpermission',getdate(),'$userid','$Notes', '$userbranch','$userregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	else
	{
		$tsqlc = "INSERT INTO Tbl_FAPR ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code]) 
		VALUES ('$custnomid','$userpermission',getdate(),'$userid','$Notes', '$userbranch','$userregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	
	//include ("../../requirepage/do_saveflow.php");
	
	// GET ORIGINAL BRANCH
		$originalregion = $userregion;
		$originalbranch = $userbranch;
		$originalao = $userid;
		if ($userwfid != "START")
		{
			$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
							WHERE txn_id='$custnomid'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

			if($sqlConn === false)
			{
				die(FormatErrors(sqlsrv_errors()));
			}

			if(sqlsrv_has_rows($sqlConn))
			{
			$rowCount = sqlsrv_num_rows($sqlConn);
			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
			{
				$originalbranch = $row[0];
				$originalregion = $row[1];
				$originalao = $row[2];
			}
		}
		sqlsrv_free_stmt( $sqlConn );
		}
		
		$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','I',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','C',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','A',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
		$c = sqlsrv_query($conn, $tsqlc);
	
	
	Header("Location:viewappraisalresponse_v3.php?custnomid=$custnomid&userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
}
?>