<?php


require('./sqlsrv.php');
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");


$custnomid= isset($_POST['custnomid']) ? $_POST['custnomid'] : "";
$nomorsp3k= isset($_POST['nomorsp3k']) ? $_POST['nomorsp3k'] : "";
$keagunankredit= isset($_POST['keagunankredit']) ? $_POST['keagunankredit'] : "";
$asuransijaminan= isset($_POST['asuransijaminan']) ? $_POST['asuransijaminan'] : "";
$otherasuransijaminan= isset($_POST['otherasuransijaminan']) ? $_POST['otherasuransijaminan'] : "";
$asuransijiwa= isset($_POST['asuransijiwa']) ? $_POST['asuransijiwa'] : "";
$otherasuransijiwa= isset($_POST['otherasuransijiwa']) ? $_POST['otherasuransijiwa'] : "";
$asuransikerugian= isset($_POST['asuransikerugian']) ? $_POST['asuransikerugian'] : "";
$otherasuransikerugian= isset($_POST['otherasuransikerugian']) ? $_POST['otherasuransikerugian'] : "";
$biayaasuransikebakaran= isset($_POST['biayaasuransikebakaran']) ? str_replace(',', '',$_POST['biayaasuransikebakaran']) : "";
$biayaasuransijiwa= isset($_POST['biayaasuransijiwa']) ? str_replace(',', '', $_POST['biayaasuransijiwa']) : "";
$pembukaantabungan= isset($_POST['pembukaantabungan']) ? str_replace(',', '', $_POST['pembukaantabungan']) : "";
$estimasibiayanotaris= isset($_POST['estimasibiayanotaris']) ?  str_replace(',', '', $_POST['estimasibiayanotaris']) : "";
$uangmuka= isset($_POST['uangmuka']) ?  str_replace(',', '', $_POST['uangmuka']) : "";
$namapemimpinbank= isset($_POST['namapemimpinbank']) ? $_POST['namapemimpinbank'] : "";
$jabatanpemimpinbank= isset($_POST['jabatanpemimpinbank']) ? $_POST['jabatanpemimpinbank'] : "";
$tanggal= isset($_POST['_tgl']) ? $_POST['_tgl'] : "";


$strsql = "SELECT * from SP3K_KKB where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);

if(count($rs)=="0")
{
$tsql = 'INSERT into SP3K_KKB (
        CUSTNOMID,
		NOMORSP3K,
		KEAGUNANKREDIT,
		ASURANSIJAMINAN,
		OTHERASURANSIJAMINAN,
		ASURANSIJIWA,
		OTHERASURANSIJIWA,
		UANGMUKA,
		BIAYAASURANSIKEBAKARAN,
		BIAYAASURANSIJIWA,
		PEMBUKAANTABUNGAN,
		ESTIMASIBIAYANOTARIS,
		NAMAPEMIMPINBANK,
		JABATANPEMIMPINBANK,
		ASURANSIKERUGIAN,
		OTHERASURANSIKERUGIAN,
		TANGGALSP3K
        )
        VALUES(
		\''.$custnomid.'\',
		\''.$nomorsp3k.'\',
		\''.$keagunankredit.'\',
		\''.$asuransijaminan.'\',
		\''.$otherasuransijaminan.'\',
		\''.$asuransijiwa.'\',
		\''.$otherasuransijiwa.'\',
		\''.$uangmuka.'\',
		\''.$biayaasuransikebakaran.'\',
		\''.$biayaasuransijiwa.'\',
		\''.$pembukaantabungan.'\',
		\''.$estimasibiayanotaris.'\',
		\''.$namapemimpinbank.'\',
		\''.$jabatanpemimpinbank.'\',
		\''.$asuransikerugian.'\',
		\''.$otherasuransikerugian.'\',
		\''.$tanggal.'\'
          )';
}
else
{
$tsql = 'UPDATE SP3K_KKB set
		CUSTNOMID=\''.$custnomid.'\',
		NOMORSP3K=\''.$nomorsp3k.'\',
		KEAGUNANKREDIT=\''.$keagunankredit.'\',
		ASURANSIJAMINAN=\''.$asuransijaminan.'\',
		OTHERASURANSIJAMINAN=\''.$otherasuransijaminan.'\',
		ASURANSIJIWA=\''.$asuransijiwa.'\',
		OTHERASURANSIJIWA=\''.$otherasuransijiwa.'\',
		UANGMUKA=\''.$uangmuka.'\',
		BIAYAASURANSIKEBAKARAN=\''.$biayaasuransikebakaran.'\',
		BIAYAASURANSIJIWA=\''.$biayaasuransijiwa.'\',
		PEMBUKAANTABUNGAN=\''.$pembukaantabungan.'\',
		ESTIMASIBIAYANOTARIS=\''.$estimasibiayanotaris.'\',
		NAMAPEMIMPINBANK=\''.$namapemimpinbank.'\',
		JABATANPEMIMPINBANK=\''.$jabatanpemimpinbank.'\',
		ASURANSIKERUGIAN=\''.$asuransikerugian.'\',
		OTHERASURANSIKERUGIAN=\''.$otherasuransikerugian.'\',
		TANGGALSP3K=\''.$tanggal.'\'
        where
        CUSTNOMID=\''.$custnomid.'\'';
}
$db->_EQ($tsql);

//header('location:./SP3K_KKB_KKB_form.php')
require ("../../requirepage/do_saveflow.php");
header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");

?>
