﻿<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");
	require_once ("../../lib/open_con_apr.php");
	
	$liscolid = $_GET["liscolid"];
	$custnomid = $_GET["custnomid"];
	
	// BAGIAN I
	
	$namacalondebitur = "";
	$alamat = "";
	$cabang = "";
	$namaao = "";
	$tanggalsurvey = "";
	$tanggalreport = "";
	$yangdihubungi = "";
	$bidangusaha = "";
	
	$tsql = "SELECT CUST_NAME, CUST_ADDR1, CUST_ADDR2, CUST_ADDR3, (SELECT BRANCH_NAME FROM TBL_BRANCH WHERE BRANCH_CODE IN (SELECT AP_BRANCH FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid')) AS CABANG, (SELECT USER_NAME FROM TBL_SE_USER WHERE USER_ID IN (SELECT TXN_USER_ID FROM TBL_FSTART  WHERE TXN_ID = '$custnomid')) AS NAMAAO, (SELECT AP_DATE FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid') AS TANGGALSURVEY, CUST_YANGDIHUBUNGI, (SELECT BT_DESC FROM RFBUSINESSTYPE WHERE BT_CODE IN (SELECT CUST_USAHA FROM TBL_COL_CUSTOMER WHERE AP_LISREGNO = '$custnomid')) AS BIDANGUSAHA FROM TBL_COL_CUSTOMER WHERE AP_LISREGNO = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$namacalondebitur = $row["CUST_NAME"];
			$alamat = $row["CUST_ADDR1"];
			$cabang = $row["CABANG"];
			$namaao = $row["NAMAAO"];
			$tanggalsurvey = $row["TANGGALSURVEY"]->format('d F Y');
			$tanggalreport = $row["TANGGALSURVEY"]->format('d F Y');
			$yangdihubungi = $row["CUST_YANGDIHUBUNGI"];
			$bidangusaha = $row["BIDANGUSAHA"];
			
			if($row["CUST_ADDR2"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR2"];
			}
			
			if($row["CUST_ADDR3"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR3"];
			}
		}
	}
	
	// BAGIAN II
	
	$LOKASI = ""; //
	$NOHAKTANGGUNGAN = "";
	$TGLHAKTANGGUNGAN = "";
	$HUBUNGANPEMEGANGHAK = "";
	$IDENTIFIKASI = "";
	$TOPOGRAFI = "";
	$BENTUK = "";
	$UKURANPANJANG = "";
	$UKURANLEBAR = "";
	$BATAS1 = "";
	$BATAS2 = "";
	$BATAS3 = "";
	$BATAS4 = "";
	$ARAH1 = "";
	$ARAH2 = "";
	$ARAH3 = "";
	$ARAH4 = "";
	$ISI1 = "";
	$ISI2 = "";
	$ISI3 = "";
	$ISI4 = "";
	$DAERAHTERMASUK = "";
	
	$tsql = "SELECT (SELECT RFCERTTYPE.CERTTYPE_DESC FROM RFCERTTYPE WHERE RFCERTTYPE.CERTTYPE_ID = APPRAISAL_LND.CERT_TYPE) AS JENISSERTIFIKAT,
			CERT_NO AS NOMORSERTIFIKAT, CERT_OWNER AS PEMEGANGHAK, CERT_LAND_DIM AS LUASTANAH, CERT_DUEDATE AS TGLBERAKHIRSERTIFIKAT, CERT_DATE AS TGLTERBITSERTIFIKAT,
			HYPOTIC_NO AS NOHAKTANGGUNGAN, HYPOTIC_DATE AS TGLHAKTANGGUNGAN, (SELECT RFRELATION.REL_DESC FROM RFRELATION WHERE RFRELATION.REL_CODE = APPRAISAL_LND.REL_CODE) AS HUBUNGANPEMEGANGHAK, 
			(SELECT RFIDENTIFICATION.IDTF_DESC FROM RFIDENTIFICATION WHERE RFIDENTIFICATION.IDTF_CODE = APPRAISAL_LND.IDTF_CODE) AS IDENTIFIKASI,
			(SELECT RFTOPOGRAPHY.TPGF_DESC FROM RFTOPOGRAPHY WHERE RFTOPOGRAPHY.TPGF_CODE = APPRAISAL_LND.TPGF_CODE) AS TOPOGRAFI,
			(SELECT RFBENTUKTANAH.BTKTNH_DESC FROM RFBENTUKTANAH WHERE RFBENTUKTANAH.BTKTNH_CODE = APPRAISAL_LND.BTKTNH_CODE) AS BENTUK,
			LND_LENM2 AS UKURANPANJANG, LND_WIDM2 AS UKURANLEBAR,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE1) AS BATAS1,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE2) AS BATAS2,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE3) AS BATAS3,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE4) AS BATAS4,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR1) AS ARAH1,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR2) AS ARAH2,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR3) AS ARAH3,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR4) AS ARAH4,
			BDRY_DATA1 AS ISI1, BDRY_DATA2 AS ISI2, BDRY_DATA3 AS ISI3, BDRY_DATA4 AS ISI4, 
			(SELECT RFKONDISIDAERAH.CNDDRH_DESC FROM RFKONDISIDAERAH WHERE RFKONDISIDAERAH.CNDDRH_CODE = APPRAISAL_LND.CNDDRH_CODE) AS DAERAHTERMASUK
			 FROM APPRAISAL_LND WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			//$LOKASI = $row["JENISSERTIFIKAT"];
			/*$JENISSERTIFIKAT = $row["JENISSERTIFIKAT"];
			$NOMORSERTIFIKAT = $row["NOMORSERTIFIKAT"];
			$PEMEGANGHAK = $row["PEMEGANGHAK"];
			$LUASTANAH = $row["LUASTANAH"];
			$TGLBERAKHIRSERTIFIKAT = $row["TGLBERAKHIRSERTIFIKAT"];
			$TGLTERBITSERTIFIKAT = $row["TGLTERBITSERTIFIKAT"];*/
			//$NOGSSU = $row["JENISSERTIFIKAT"]; //
			//$TGLGSSU = $row["JENISSERTIFIKAT"]; //
			$NOHAKTANGGUNGAN = $row["NOHAKTANGGUNGAN"];
			$TGLHAKTANGGUNGAN = $row["TGLHAKTANGGUNGAN"];
			$HUBUNGANPEMEGANGHAK = $row["HUBUNGANPEMEGANGHAK"];
			$IDENTIFIKASI = $row["IDENTIFIKASI"];
			$TOPOGRAFI = $row["TOPOGRAFI"];
			$BENTUK = $row["BENTUK"];
			$UKURANPANJANG = $row["UKURANPANJANG"];
			$UKURANLEBAR = $row["UKURANLEBAR"];
			$BATAS1 = $row["BATAS1"];
			$BATAS2 = $row["BATAS2"];
			$BATAS3 = $row["BATAS3"];
			$BATAS4 = $row["BATAS4"];
			$ARAH1 = $row["ARAH1"];
			$ARAH2 = $row["ARAH2"];
			$ARAH3 = $row["ARAH3"];
			$ARAH4 = $row["ARAH4"];
			$ISI1 = $row["ISI1"];
			$ISI2 = $row["ISI2"];
			$ISI3 = $row["ISI3"];
			$ISI4 = $row["ISI4"];
			$DAERAHTERMASUK = $row["DAERAHTERMASUK"];
			
			/*if($TGLBERAKHIRSERTIFIKAT != "")
			{
				$TGLBERAKHIRSERTIFIKAT = $row["TGLBERAKHIRSERTIFIKAT"]->format('d F Y');
			}
			if($TGLTERBITSERTIFIKAT != "")
			{
				$TGLTERBITSERTIFIKAT = $row["TGLTERBITSERTIFIKAT"]->format('d F Y');
			}*/
			if($TGLHAKTANGGUNGAN != "")
			{
				$TGLHAKTANGGUNGAN = $row["TGLHAKTANGGUNGAN"]->format('d F Y');
			}
		}
	}
	
	$alamatapr = "";
	
	$tsql = "SELECT * FROM COLLATERAL where COLMASTER_ID in (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$alamatapr = $row['COL_ADDR1'];
			
			if($row['COL_ADDR2'] != "")
			{
				$alamatapr = $alamatapr." ".$row['COL_ADDR2'];
			}
			
			if($row['COL_ADDR3'] != "")
			{
				$alamatapr = $alamatapr." ".$row['COL_ADDR3'];
			}
		}
	}
	
	// BAGIAN III
	
	$RANGKAUTAMA = "";
	$LANTAI = "";
	$DINDING = "";
	$LANGIT = "";
	$RANGKAATAP = "";
	$ATAP = "";
	$JUMLAHLANTAI = "";
	$LUASIMB = "";
	$LUASFISIK ="";
	$PEMBAGIANRUANG = "";
	$FASILITASBANGUNAN = "";
	$TAHUNDIBANGUN = "";
	$NOIMB = "";
	$TGLIMB = "";
	$DIHUNIOLEH = "";
	$KETERANGAN = "";
	
	$tsql = "SELECT RGKUT_CODE AS RANGKAUTAMA, LNTAI_CODE AS LANTAI, DDING_CODE AS DINDING, LNGIT_CODE AS LANGIT,  RGKAT_CODE AS RANGKAATAP,
			ATAP_CODE AS ATAP, JMLLT_CODE AS JUMLAHLANTAI, IMB_DIM AS LUASIMB, FIS_DIM AS LUASFISIK,
			PEMBAGIAN_RUANG AS PEMBAGIANRUANG, FASILITAS_BANGUNAN AS FASILITASBANGUNAN, BUILTYEAR AS TAHUNDIBANGUN, IMB_NO AS NOIMB, IMB_DATE AS TGLIMB,
			HUNI_CODE AS DIHUNIOLEH, NOTE AS KETERANGAN
			FROM APPRAISAL_BLD WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_BLD WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAUTAMA = $row["RANGKAUTAMA"];
			$LANTAI = $row["LANTAI"];
			$DINDING = $row["DINDING"];
			$LANGIT = $row["LANGIT"];
			$RANGKAATAP = $row["RANGKAATAP"];
			$ATAP = $row["ATAP"];
			$JUMLAHLANTAI = $row["JUMLAHLANTAI"];
			$LUASIMB = $row["LUASIMB"];
			$LUASFISIK =$row["LUASFISIK"];
			$PEMBAGIANRUANG = $row["PEMBAGIANRUANG"];
			$FASILITASBANGUNAN = $row["FASILITASBANGUNAN"];
			$TAHUNDIBANGUN = $row["TAHUNDIBANGUN"];
			$NOIMB = $row["NOIMB"];
			$TGLIMB = $row["TGLIMB"];
			$DIHUNIOLEH = $row["DIHUNIOLEH"];
			$KETERANGAN = $row["KETERANGAN"];
			
			if($TGLIMB != "")
			{
				$TGLIMB = $row["TGLIMB"]->format('d F Y');
			}
		}
	}
	
	$tsql = "SELECT * FROM RFRANGKAUTAMA WHERE RGKUT_CODE = '$RANGKAUTAMA'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAUTAMA = $row["RGKUT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFLANTAI WHERE LNTAI_CODE = '$LANTAI'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$LANTAI = $row["LNTAI_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFDINDING WHERE DDING_CODE = '$DINDING'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$DINDING = $row["DDING_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFRANGKAATAP WHERE RGKAT_CODE = '$RANGKAATAP'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAATAP = $row["RGKAT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFATAP WHERE ATAP_CODE = '$ATAP'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$ATAP = $row["ATAP_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFJUMLAHLANTAI WHERE JMLLT_CODE = '$JUMLAHLANTAI'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$JUMLAHLANTAI = $row["JMLLT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFPENGHUNI WHERE HUNI_CODE = '$DIHUNIOLEH'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$DIHUNIOLEH = $row["HUNI_DESC"];
		}
	}
	
	// BAGIAN VII
	
	$LUAS_TANAH="";
	$NILAI_TANAH_PERM2="";
	$NILAI_TANAH_TOTAL="";
	$LUAS_BANG_FISIK="";
	$NILAI_BANG_FISIK_PERM2="";
	$NILAI_BANG_FISIK_TOTAL="";
	$LUAS_BANG_IMB="";
	$NILAI_BANG_IMB_PERM2="";
	$NILAI_BANG_IMB_TOTAL="";
	$NILAI_TOTAL_FISIK="";
	$NILAI_TOTAL_IMB="";
	$SAFETY_MARGIN="";
	$NILAI_LIKUIDASI_FISIK="";
	$NILAI_LIKUIDASI_IMB="";
	$NILAI_BANG_FISIK_PERCENT="";
	$NILAI_BANG_IMB_PERCENT="";
	$SAFETY_MARGIN_FISIK="";
	
	$tsql = "SELECT * FROM APPRAISAL_TYPE1_PENILAIAN WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$LUAS_TANAH=$row["LUAS_TANAH"];
			$NILAI_TANAH_PERM2=$row["NILAI_TANAH_PERM2"];
			$NILAI_TANAH_TOTAL=$row["NILAI_TANAH_TOTAL"];
			$LUAS_BANG_FISIK=$row["LUAS_BANG_FISIK"];
			$NILAI_BANG_FISIK_PERM2=$row["NILAI_BANG_FISIK_PERM2"];
			$NILAI_BANG_FISIK_TOTAL=$row["NILAI_BANG_FISIK_TOTAL"];
			$LUAS_BANG_IMB=$row["LUAS_BANG_IMB"];
			$NILAI_BANG_IMB_PERM2=$row["NILAI_BANG_IMB_PERM2"];
			$NILAI_BANG_IMB_TOTAL=$row["NILAI_BANG_IMB_TOTAL"];
			$NILAI_TOTAL_FISIK=$row["NILAI_TOTAL_FISIK"];
			$NILAI_TOTAL_IMB=$row["NILAI_TOTAL_IMB"];
			$SAFETY_MARGIN=$row["SAFETY_MARGIN"];
			$NILAI_LIKUIDASI_FISIK=$row["NILAI_LIKUIDASI_FISIK"];
			$NILAI_LIKUIDASI_IMB=$row["NILAI_LIKUIDASI_IMB"];
			$NILAI_BANG_FISIK_PERCENT=$row["NILAI_BANG_FISIK_PERCENT"];
			$NILAI_BANG_IMB_PERCENT=$row["NILAI_BANG_IMB_PERCENT"];
			$SAFETY_MARGIN_FISIK=$row["SAFETY_MARGIN_FISIK"];
		}
	}
	
	// BAGIAN VIII
	
	$OPINIPOSITIF = "";
	$OPININEGATIF = "";
	$KONDISIPASAR = "";
	$CATATAN = "";
	
	$tsql = "SELECT EVALPOS AS OPINIPOSITIF, EVALNEG AS OPININEGATIF, MK_CODE AS KONDISIPASAR, EVALNOTE AS CATATAN FROM APPRAISAL_OPINI WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$OPINIPOSITIF = $row["OPINIPOSITIF"];
			$OPININEGATIF = $row["OPININEGATIF"];
			$KONDISIPASAR = $row["KONDISIPASAR"];
			$CATATAN = $row["CATATAN"];
		}
	}
	
	$tsql = "SELECT * FROM RFMK WHERE MK_ID = '$KONDISIPASAR'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$KONDISIPASAR = $row["MK_DESC"];
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>
	Letter Collateral Type 01 Page
</title><link href="../include/letterStyle.css" rel="stylesheet" />

    <style type="text/css">
        .style1
        {
            width: 1%;   
        }

        .tablebox1
        {
            background-color:white;
            border-collapse: collapse; 
            table-layout: fixed; 
            border: 1px solid #000000;         
        }
        .judul
        {
           background-color: #CCCCCC; 
        }
    
        .style2
        {
            text-align: right;
        }
    
    </style>
    <script type="text/javascript">
        function onPrint()
        {
            var f = document.form1;
            f.btn_print.style.display = "none"; 
            window.print();
            f.btn_print.style.display = "";
        
        }
    </script>
    
</head>
<body>
    <form name="form1" method="post" action="TPL_ColType01.aspx?tc=7.0&amp;regno=00095%2f116%2f2012&amp;curef=PE120504000262&amp;mcolid=PE12050400026201&amp;cetak=html&amp;rnq=1" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTc4MDcwNDYxMg9kFgICAw9kFrYBAg8PDxYCHgRUZXh0BRJUQU5BSCBEQU4gQkFOR1VOQU5kZAIRDw8WAh8ABSUwMDAwMDAwMDYvMDkxL0xhcC1BcHAvRkFDMDMvICAgVi8yMDEyZGQCEw8PFgIfAAULTkVXIFRBS1NBU0lkZAIVDw8WAh8ABQdTdXRhcmpvZGQCFw8PFgIfAAU2S3AuIFNhd2FoIEtlbHVyYWhhbiBUYXJpa29sb3QgS2VjYW1hdGFuIENpdGVyZXVwIEJvZ29yZGQCGQ8PFgIfAAUHVHJhZGluZ2RkAhsPDxYCHwAFDUtDUCBERVBPSyBJVENkZAIdDw8WAh8ABQhyb21pZTExNmRkAh8PDxYCHwAFCjcgTWF5IDIwMTJkZAIhDw8WAh8ABQo3IE1heSAyMDEyZGQCIw9kFgICAw9kFgICAQ8PFgIfAAUBLWRkAiUPDxYCHwAFB1N1dGFyam9kZAInDw8WAh8ABWZLYW1wdW5nIENpcGF5dW5nIFJUIDAwMi9SVyAwMDcgICwgS2VsLi8gRGVzYSBUZW5nYWgsIEtlYy4gQ2liaW5vbmcsIEtvdGEvIEthYi4gQm9nb3IsIFByb3AuIEphd2EgQmFyYXRkZAIpDzwrAA0CAA8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIBZAEQFgICAgIFFgI8KwAFAQAWAh4KRm9vdGVyVGV4dAUDNTUxPCsABQEAFgIeCkhlYWRlclRleHQFD05vLiBHUy9TVSAmIFRnbBYCZmYWAmYPZBYCAgEPZBYMZg8PFgIfAAUQU0hNLyAzMTczL1RFTkdBSGRkAgEPDxYCHwAFBktBU01JUmRkAgIPDxYCHwAFAzU1MWRkAgMPDxYCHwAFBiZuYnNwO2RkAgQPDxYCHwAFCzMwIE1heSAyMDA2ZGQCBQ8PFgIfAAUbMDUvVGVuZ2FoLzIwMDYvIDA4IE1hciAyMDA2ZGQCKw8PFgIfAGRkZAItDw8WAh8AZGRkAi8PDxYCHwAFDFBlbWlsaWsgTGFtYWRkAjEPDxYCHwAFKlRhbmFoIGRhbiBiYW5ndW5hbiBSdW1haCBUaW5nZ2FsIGRpYXRhc255YWRkAjMPDxYCHwAFF0xlYmloIHRpbmdnaSBkYXJpIGphbGFuZGQCNQ8PFgIfAAUJVHJhcGVzaXVtZGQCNw8PFgIfAAUEMzAuNmRkAjkPDxYCHwAFAjE4ZGQCOw8PFgIfAAUFREVQQU5kZAI9Dw8WAh8ABQpCQVJBVCBMQVVUZGQCPw8PFgIfAAUQSmFsYW4gTGluZ2t1bmdhbmRkAkEPDxYCHwAFBUtBTkFOZGQCQw8PFgIfAAUKVElNVVIgTEFVVGRkAkUPDxYCHwAFDVJ1bWFoIFRpbmdnYWxkZAJHDw8WAh8ABQhCRUxBS0FOR2RkAkkPDxYCHwAFCFRFTkdHQVJBZGQCSw8PFgIfAAUNUnVtYWggVGluZ2dhbGRkAk0PDxYCHwAFBEtJUklkZAJPDw8WAh8ABQpCQVJBVCBEQVlBZGQCUQ8PFgIfAAUKSmFsYW4gRGVzYWRkAlMPDxYCHwAFDEJlYmFzIGJhbmppcmRkAlUPDxYCHwAFD0JldG9uIGJlcnR1bGFuZ2RkAlcPDxYCHwAFC1JhYmF0IEJldG9uZGQCWQ8PFgIfAAUJQmF0dSBiYXRhZGQCWw8PFgIfAAUTR3lwc3VtYm9hcmQgUi4gS2F5dWRkAl0PDxYCHwAFBEtheXVkZAJfDw8WAh8ABQ9HZW50ZW5nIEtlcmFtaWtkZAJhDw8WAh8ABQExZGQCYw8PFgIfAAUBMGRkAmUPDxYCHwAFAzE5MmRkAmcPDxYCHwAFRFJ1YW5nIGtlbHVhcmdhLCBSdWFuZyB0YW11LCBLYW1hciB0aWR1ciwgS2FtYXIgbWFuZGksIERhcHVyLCBHdWRhbmcuZGQCaQ8PFgIfAAUYTGlzdHJpayAxMzAwLCBBaXIgUG9tcGEuZGQCaw8PFgIfAAUEMjAwNmRkAm0PDxYCHwAFBDIwMTFkZAJvDw8WAh8ABQEuZGQCcQ8PFgIfAGRkZAJzDw8WAh8ABQdEZWJpdHVyZGQCdQ8PFgIfAGRkZAJ3Dw8WAh8ABbkBTWVsYWx1aSBqYWxhbiByYXlhIGJvZ29yIGxhbHUgbWFzdWsgbWVsYWx1aSBqYWxhbiByYXlhIHBlbWRhIGxhbHUgbWFzdWsgbWVsYWx1aSBqbGFuIHJheWEgc3VrYWhhdGkgbGFsdSBtYXN1ayBtZWxhbHVpIGphbGFuIGFrc2VzIGthbXB1bmcsIG9iamVrIHRlcmxldGFrIMKxIDIwMCBtZXRlciBkaXNpc2kga2lyaSBqYWxhbi5kZAJ5Dw8WAh8ABQVCZXRvbmRkAnsPDxYCHwAFATNkZAJ9Dw8WAh8ABQtLdXJhbmcgQmFpa2RkAn8PDxYCHwAFA0R1YWRkAoEBDw8WAh8ABQZKYXJhbmdkZAKDAQ8PFgIfAAVRSmFyaW5nYW4gbGlzdHJpayBQTE4sIFRlbGtvbSwgUEFNL1BEQU0sIE1hc2ppZCwgU2Vrb2xhaCB0ZXJzZWRpYSBkaXNla2l0YXIgb2JqZWsuZGQChQEPDxYCHwAFJFRlcnNlZGlhIGJlcnVwYSBhbmdrdXRhbiBrb3RhLCBvamVrLmRkAocBDw8WAh8ABRdLYW50b3IgS2VsdXJhaGFuIFRlbmdhaGRkAokBDw8WAh8ABQlQZW11a2ltYW5kZAKLAQ88KwANAQAPFgQfAWcfAmZkZAKNAQ88KwANAQAPFgQfAWcfAgIDZBYCZg9kFggCAQ9kFg5mDw8WAh8ABR5LYW1wdW5nIENpcGF5dW5nIFJUIDAwMS9SVyAwMDdkZAIBDw8WAh8ABQMxNDRkZAICDw8WAh8ABQI0NWRkAgMPDxYCHwAFCzEzNSwwMDAsMDAwZGQCBA8PFgIfAAULMTE0LDc1MCwwMDBkZAIFDw8WAh8ABSBSaW8vQWdlbiBQcm9wZXJ0eSAoMDgxMjE5OTI1NTc5KWRkAgYPDxYCHwAFBzQ5Niw4NzVkZAICD2QWDmYPDxYCHwAFHkthbXB1bmcgQ2lwYXl1bmcgUlQgMDAyL1JXIDAwN2RkAgEPDxYCHwAFAzE0M2RkAgIPDxYCHwAFAjc2ZGQCAw8PFgIfAAULMTc1LDAwMCwwMDBkZAIEDw8WAh8ABQsxNDgsNzUwLDAwMGRkAgUPDxYCHwAFIUtpbGlrL0FnZW4gUHJvZXBydHkgKDA4MTgwODMwMzgwKWRkAgYPDxYCHwAFBzQ5OCwxMTJkZAIDD2QWDmYPDxYCHwAFJEthbXB1bmcgQ2lwYXl1bmcgTm8uIDYgUlQgMDA1L1JXIDAwN2RkAgEPDxYCHwAFBDEyMDBkZAICDw8WAh8ABQM3MDBkZAIDDw8WAh8ABQ0xLDUwMCwwMDAsMDAwZGQCBA8PFgIfAAUNMSwyNzUsMDAwLDAwMGRkAgUPDxYCHwAFIUJhZ2FzL0FnZW4gUHJvcGVydHkgKDA4MTI4NTkzODQ3KWRkAgYPDxYCHwAFBzQxNyw5MTdkZAIEDw8WAh4HVmlzaWJsZWhkZAKPAQ8PFgIfAAUEMjAxMmRkApEBDw8WAh8ABQY4MiwwMDBkZAKTAQ8PFgIfAAUBMGRkApUBDw8WAh8ABQM1NTFkZAKXAQ8PFgIfAAUHMzUwLDAwMGRkApkBDw8WAh8ABQEwZGQCmwEPDxYCHwAFATBkZAKdAQ8PFgIfAAUBMGRkAp8BDw8WAh8ABQMxOTJkZAKhAQ8PFgIfAAUJMSw0MDAsMDAwZGQCowEPDxYCHwAFAjYwZGQCpQEPDxYCHwAFCzE5Miw4NTAsMDAwZGQCpwEPDxYCHwAFATBkZAKpAQ8PFgIfAAULMTkyLDg1MCwwMDBkZAKrAQ8PFgIfAAULMTkyLDg1MCwwMDBkZAKtAQ8PFgIfAAULMTYxLDI4MCwwMDBkZAKvAQ8PFgIfAAULMzU0LDEzMCwwMDBkZAKxAQ8PFgIfAAUCMzBkZAKzAQ8PFgIfAAUCMzBkZAK1AQ8PFgIfAAULMTM0LDk5NSwwMDBkZAK3AQ8PFgIfAAULMjQ3LDg5MSwwMDBkZAK5AQ8WAh8FZxYCZg9kFiQCAQ8PFgIfAAUDNTUxZGQCAw8PFgIfAAUHMzYxLDAwMGRkAgUPDxYCHwAFATBkZAIHDw8WAh8ABQEwZGQCCQ8PFgIfAAUBMGRkAgsPDxYCHwAFAzE5MmRkAg0PDxYCHwAFCTEsNDAwLDAwMGRkAg8PDxYCHwAFAjYwZGQCEQ8PFgIfAAULMTk4LDkxMSwwMDBkZAITDw8WAh8ABQEwZGQCFQ8PFgIfAAULMTk4LDkxMSwwMDBkZAIXDw8WAh8ABQsxOTgsOTExLDAwMGRkAhkPDxYCHwAFCzE2MSwyODAsMDAwZGQCGw8PFgIfAAULMzYwLDE5MSwwMDBkZAIdDw8WAh8ABQIzMGRkAh8PDxYCHwAFAjMwZGQCIQ8PFgIfAAULMTM5LDIzNyw3MDBkZAIjDw8WAh8ABQsyNTIsMTMzLDcwMGRkArsBDw8WAh8AZWRkAr0BDw8WAh8AZWRkAr8BDw8WAh8ABQpNYXJrZXRhYmxlZGQCwQEPDxYCHwAFoA4qIFBhZGEgc2FhdCBtZWxha3VrYW4gdGluamF1YW4vc3VydmV5IGRpbGFwYW5nYW4ga2FtaSBkaWRhbXBpbmdpIG9sZWggQnBrLiBTYW1oaXIgKHRldGFuZ2dhIGNhZGViKS48YnI+KiBPYmplayBiZXJ1cGEgcnVtYWggdGluZ2dhbCB5YW5nIGJlcmFkYSBkaWxpbmdrdW5nYW4gcGVya2FtcHVuZ2FuIGRlbmdhbiB0aW5na2F0IGh1bmlhbiBjdWt1cCByYW1haSBkYW4gbGluZ2t1bmdhbiB5YW5nIHRlcnRhdGEgZGVuZ2FuIGJhaWsuPGJyPiogS29uZGlzaSBvYmplayBzYWF0IGluaSBkYWxhbSBrZWFkYWFuIHRhaGFwIHBlbWJhbmd1bmFuIHlhbmcgdGVyaGVudGkgwrEgNiBidWxhbiAtIDEgdGFodW4sIG5hbXVuIGxheWFrIGh1bmkuPGJyPiogQ29weSBJTUIgYmVsdW0ga2FtaSB0ZXJpbWEsIHNlaGluZ2dhIG5pbGFpIElNQiB0aWRhayBrYW1pIHBlcmhpdHVuZ2thbi48YnI+KiBOaWxhaSBzZXN1YWkgZmlzaWsgYmVybGFrdSBqaWthIHRlbGFoIGRpbGVuZ2thcGkgZGVuZ2FuIElNQiB5YW5nIGx1YXNueWEgc2VzdWFpLiBKaWthIHRlbGFoIGRpbGVuZ2thcGkgZGFuIHRlcmRhcGF0IHBlcmJlZGFhbiBsdWFzIG1ha2EgcGVuaWxhaWFuIGluaSBha2FuIGRpcmV2aXNpPGJyPiogQ2VrIEJQTiBrYXJlbmEgc2VydGlmaWthdCB0aWRhayBtZW5jYW50dW1rYW4gYWxhbWF0IGxldGFrIHBlcnNpbCwgc2ViZWx1bSBwZW5naWthdGFuIGtyZWRpdCB1bnR1ayBtZW1hc3Rpa2FuIGxldGFrIHBlcnNpbCBzZXN1bmdndWhueWEuLjxicj4qIENlayBrZSBEaW5hcyBUYXRhIFJ1YW5nIHNlYmVsdW0gcGVuZ2lrYXRhbiBrcmVkaXQgdW50dWsgbWVuZ2V0YWh1aSBwZXJ1bnR1a2FuLCBLREIsIEtMQiwgR1NQICYgR1NCLjxicj4qIENlayBrZSBEaW5hcyBCaW5hIE1hcmdhIHNlYmVsdW0gcGVuZ2lrYXRhbiBrcmVkaXQgdW50dWsgbWVuZ2V0YWh1aSByZW5jYW5hIHBlbGViYXJhbiBqYWxhbi48YnI+KiBTZXJ0aWZpa2F0IG1hc2loIGF0YXMgbmFtYSBwZW1pbGlrIHNlYmVsdW1ueWEuIFNlYmFpa255YSBzZXJ0aWZpa2F0IGRpYmFsaWsgbmFtYSBrZSBuYW1hIGNhZGViLjxicj4qIFNlbHVydWggcGVtYmFuZGluZyBiZXJhZGEgZGlsaW5na3VuZ2FuIHlhbmcgc2FtYSBkZW5nYW4gb2JqZWsgZGVuZ2FuIGtvbmRpc2kgZGFuIGFrc2VzIGphbGFuIHlhbmcgbGViaWggYmFpaywgYmVydXBhIHJ1bWFoIHRpbmdnYWwgMSBsYW50YWkgZGVuZ2FuIGtvbmRpc2kgIGN1a3VwIGJhaWs8YnI+KiBPYmplayBtZW1pbGlraSBuaWxhaSB0YW5haCBsZWJpaCByZW5kYWggZGFyaXBhZGEgcGVtYmFuZGluZyBrYXJlbmEga29uZGlzaSBqYWxhbiBkZXBhbiBwZW1iYW5kaW5nIGxlYmloIGJhaWsgPGJyPiogTGFtcGlyYW4gZG9rdW1lbiB5YW5nIGRpYmVyaWthbi8gZGlsYXBpcmthbiBkaSBkb2N1bWVudCBjaGVja2luZyBzYWF0IHBlbmlsYWlhbiBtZXJ1cGFrYW4gZG9rdW1lbiBGb3RvIENvcHksIExQSiBpbmkgYmVybGFrdSBiaWxhbWFuYSB0aWRhayBhZGEgcGVyYmVkYWFuIGFudGFyYSBsYW1waXJhbiBkb2t1bWVuIHRlcnNlYnV0IGRlbmdhbiBkb2t1bWVuIEFzbGlueWEuPGJyPiogTGFwb3JhbiB0YWtzYXNpIGluaSBtZXJ1cGFrYW4gaW5wdXQgdWxhbmcgZGFyaSBsYXBvcmFuIHRha3Nhc2kgc2ViZWx1bW55YSBOby4gMDAwMDAwMDI1LzA5MS9MYXAtQXBwL0ZBQzA0LyBJVi8yMDEyLCB0YW5nZ2FsIDMwIEFwcmlsIDIwMTIga2FyZW5hIGxhcG9yYW4gdGFrc2FzaSBzZWJlbHVtbnlhIHRpZGFrIHNhbXBhaSBrZSBzaXN0aW0gcGluY2EgS0NQIERFUE9LIElUQy8gc2lzdGltIHNldGVsYWggZGkga2lyaW0vIGRpIEFwcHJvdmVsIG55YXNhci8gZXJvci5kZALDAQ8PFgIfAAWPAzx0YWJsZSB3aWR0aD0iMTAwJSI+IDx0cj48dGQgd2lkdGg9IjMzJSI+UGVuaWxhaTwvdGQ+PHRkPlBlbWVyaWtzYTwvdGQ+PHRkIHdpZHRoPSIzMyUiPkNvbmZpcm0vIE92ZXJyaWRlPC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+KEFyYXN5IEF1bGlhIFRvcGFuIC0gS0MgQm9nb3IpPC90ZD48dGQ+KCBZdWRoaSBIZW5kcmlhdG5vIC0gUk8gSmFrYXJ0YSk8L3RkPjx0ZD4oUHVzcGEgUmVuZ2dhbmlzKTwvdGQ+PC90cj48L3RhYmxlPmRkGAMFEUdyaWRWaWV3SW5mb0hhcmdhDzwrAAoBCAIBZAUMR3JpZFZpZXdJbnNzDzwrAAoBCGZkBQ9HcmlkVmlld0NvbF9MTkQPPCsACgEIAgFkBPXANnDP3lSkoBVqvQgY3GYC1Z4=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/LOSAPPRAISAL-MEGA/WebResource.axd?d=drJ8Nveh6li0uxJL6mD9EA2&amp;t=634516166785781250" type="text/javascript"></script>

<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
WebForm_ReEnableControls();
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
	<input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWBwL0iISPDwK3zoG4DQLGnaHCDALxguzqAwKkh9WRCAKo6PHFAgKn3u2kBETjbwbeRGC+CqQ/ioCuODxvfg6F" />
</div>
    <div>
        <input name="h_mcolid" type="hidden" id="h_mcolid" value="PE12050400026201" />
        <input name="h_colid_lnd" type="hidden" id="h_colid_lnd" value="PE12050400026201LND01" />
        <input name="h_sum_cert_lnd" type="hidden" id="h_sum_cert_lnd" value="551" />
        
        <input name="h_APP_OFFICER" type="hidden" id="h_APP_OFFICER" value="Arasy Aulia Topan - KC Bogor" />
        <input name="h_SPV_OFFICER" type="hidden" id="h_SPV_OFFICER" value="Yudhi Hendriatno - RO Jakarta" />
        <input name="h_PINCA" type="hidden" id="h_PINCA" value="Puspa Rengganis" />
        
        <table width="100%" class="tablebox1" border="1">
            <tr>
                <td>
                    <table width="100%" class="judul">
                        <tr>
                            <td rowspan="5" class="style1">
                                <!--<img src="../Image/megalogo.jpg" /> </td>-->
                        </tr>
                        <tr>
                            <td align="center" ><strong>LAPORAN PENILAIAN</strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>
                                <span id="COLTYPE_DESC">TANAH DAN BANGUNAN</span></strong></td>
                        </tr>
                        <!--<tr>
                            <td align="center">
                                NO.&nbsp<span id="NOREPORT">000000006/091/Lap-App/FAC03/   V/2012</span>
                            </td>
                        </tr>-->
                        <tr>
                            <td align="center"><strong>
                                <span id="TAXATYPE_DESC">NEW TAKSASI</span></strong>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td style="width: 100%">
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>I.</strong></td>
                            <td><strong>UMUM</strong> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td>1.</td>
                            <td width="30%">Nama Calon Debitur</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="CU_NAME"><? echo $namacalondebitur;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Alamat</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="CU_ADDR"><? echo $alamat;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Bidang Usaha</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="BT_DESC"><? echo $bidangusaha;?></span>
                            </td>
                        </tr>

                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Cabang / AO</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                                <span id="BRANCHNAME"><? echo $cabang;?></span>&nbsp/&nbsp<span id="AO_USERID"><? echo $namaao;?></span> 
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Tanggal Survey / Report</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                                <span id="EVALSURVEYDATE"><? echo $tanggalsurvey;?></span>&nbsp/&nbsp<span id="TGL_TRACK_APR"><? echo $tanggalreport;?></span>
                            </td>
                        </tr>
                        <tr id="Tr1" class="text1">
	<td>6.</td>
	<td width="30%">Penilaian Terdahulu</td>
	<td width="2%">:</td>
	<td colspan="2" width="15%">
                            <span id="LASTTAXA_DATE1"></span>
                            
                            </td>
</tr>

                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Yang dihubungi</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                            <span id="CU_CONTACT"><? echo $yangdihubungi;?></span>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>II.</strong></td>
                            <td><strong>DESKRIPSI TANAH</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Lokasi Agunan</td>
                            <td width="2%">:</td>
                            <td><span id="COL_ADDR"><? echo $alamatapr;?></span>
                            
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Status</td>
                            <td width="2%">:</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td colspan="4">
							<?
								$JENISSERTIFIKAT = "";
								$NOMORSERTIFIKAT = "";
								$PEMEGANGHAK = "";
								$LUASTANAH = "";
								$TGLBERAKHIRSERTIFIKAT = "";
								$TGLTERBITSERTIFIKAT = "";
								$NOGSSU = ""; //
								$TGLGSSU = ""; //
								$JMLLUAS = 0;
								
								$tsql = "SELECT * FROM COL_CERT_LND WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
								$a = sqlsrv_query($connapr, $tsql);
								if ( $a === false)
								die( FormatErrors( sqlsrv_errors() ) );
								
								if(sqlsrv_has_rows($a))
								{ 	
									
									
								
							?>
                                <div>
	<table cellspacing="0" cellpadding="2" rules="all" border="1" id="GridViewCol_LND" style="font-family:Arial;font-size:XX-Small;width:100%;border-collapse:collapse;">
		<tr>
			<th align="center" scope="col">Jenis Hak/ No.Sertifikat</th><th align="center" scope="col">Pemegang Hak</th><th align="center" scope="col">Luas (m2)</th><th align="center" scope="col">Berakhir Hak</th><th align="center" scope="col">Tgl Penerbitan Sertifikat</th><th align="center" scope="col">No. GS &amp; Tgl</th>
		</tr>
							<?
									while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
									{	
										$JMLLUAS = $JMLLUAS + $row["CERT_LAND_DIM"];
									
										$JENISSERTIFIKAT = $row["CERT_TYPE"];
										$NOMORSERTIFIKAT = $row["CERT_NO"];
										$PEMEGANGHAK = $row["CERT_OWNER"];
										$LUASTANAH = $row["CERT_LAND_DIM"];
										$TGLBERAKHIRSERTIFIKAT = $row["CERT_DUEDATE"];
										$TGLTERBITSERTIFIKAT = $row["CERT_DATE"];
										$NOGSSU = $row["GB_NO"];
										$TGLGSSU = $row["GB_DATE"];

										if($TGLBERAKHIRSERTIFIKAT != "")
										{
											$TGLBERAKHIRSERTIFIKAT = $row["CERT_DUEDATE"]->format('d F Y');
										}
										if($TGLTERBITSERTIFIKAT != "")
										{
											$TGLTERBITSERTIFIKAT = $row["CERT_DATE"]->format('d F Y');
										}
										if($TGLGSSU != "")
										{
											$TGLGSSU = $row["GB_DATE"]->format('d F Y');
										}
										
							?>
		<tr>
			<td align="left"><? echo $JENISSERTIFIKAT;?>/ <? echo $NOMORSERTIFIKAT;?></td><td align="left"><? echo $PEMEGANGHAK;?></td><td align="right"><? echo $LUASTANAH;?></td><td align="right">&nbsp;<? echo $TGLBERAKHIRSERTIFIKAT;?></td><td align="right"><? echo $TGLTERBITSERTIFIKAT;?></td><td align="right"><? echo $NOGSSU;?>/ <? echo $TGLGSSU;?></td>
		</tr>
							<?
									}
							?>
		<tr>
			<td>&nbsp;</td><td>&nbsp;</td><td align="right"><? echo $JMLLUAS;?></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>
	</table>
</div>
                            <?
								}
							?>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">No. Hak Tanggungan</td>
                            <td width="2%">:</td>
                            <td><span id="HYPOTIC_NO"><? echo $NOHAKTANGGUNGAN;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Tgl. Hak Tanggungan</td>
                            <td width="2%">:</td>
                            <td><span id="HYPOTIC_DATE"><? echo $TGLHAKTANGGUNGAN;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Hubungan pemegang hak (Calon Debitur)</td>
                            <td width="2%">:</td>
                            <td><span id="REL_DESC"><? echo $HUBUNGANPEMEGANGHAK;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Identifikasi</td>
                            <td width="2%">:</td>
                            <td><span id="IDTF_DESC"><? echo $IDENTIFIKASI;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Topografi</td>
                            <td width="2%">:</td>
                            <td><span id="TPGF_DESC"><? echo $TOPOGRAFI;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>8.</td>
                            <td width="30%">Bentuk Type</td>
                            <td width="2%">:</td>
                            <td><span id="BTKTNH_DESC"><? echo $BENTUK;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>9.</td>
                            <td width="30%">Ukuran Type</td>
                            <td width="2%">:</td>
                            <td>panjang : <span id="LND_LENM2"><? echo $UKURANPANJANG;?></span>&nbsp
                                lebar : <span id="LND_WIDM2"><? echo $UKURANLEBAR;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>10.</td>
                            <td width="30%">Batas-batas Type</td>
                            <td width="2%">:</td>
                            <td>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td colspan="3">
                                <table width="100%">
                                    <tr>
                                        <td><span id="SIDE_DESC1"><? echo $BATAS1;?></span>/
                                        <span id="CDNDIR_DESC1"><? echo $ARAH1;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA1"><? echo $ISI1;?></span>
                                        </td>
                                        <td><span id="SIDE_DESC3"><? echo $BATAS3;?></span>/
                                        <span id="CDNDIR_DESC3"><? echo $ARAH3;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA3"><? echo $ISI3;?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span id="SIDE_DESC2"><? echo $BATAS2;?></span>/
                                        <span id="CDNDIR_DESC2"><? echo $ARAH2;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA2"><? echo $ISI2;?></span>
                                        </td>
                                        <td><span id="SIDE_DESC4"><? echo $BATAS4;?></span>/
                                        <span id="CDNDIR_DESC4"><? echo $ARAH4;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA4"><? echo $ISI4;?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>11.</td>
                            <td width="30%">Daerah termasuk</td>
                            <td width="2%">:</td>
                            <td><span id="CNDDRH_DESC"><? echo $DAERAHTERMASUK;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>III.</strong> </td>
                            <td><strong>DESKRIPSI BANGUNAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Material Bangunan</td>
                            <td width="2%">&nbsp;</td>
                            <td>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Rangka Utama</td>
                            <td width="2%">:</td>
                            <td><span id="RGKUT_DESC"><? echo $RANGKAUTAMA;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Lantai</td>
                            <td width="2%">:</td>
                            <td><span id="LNTAI_DESC"><? echo $LANTAI;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Dinding</td>
                            <td width="2%">:</td>
                            <td><span id="DDING_DESC"><? echo $DINDING;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Langit-langit</td>
                            <td width="2%">:</td>
                            <td><span id="LNGIT_DESC"><? echo $LANGIT;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Rangkap atap</td>
                            <td width="2%">:</td>
                            <td><span id="RGKAT_DESC"><? echo $RANGKAATAP;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Atap</td>
                            <td width="2%">:</td>
                            <td><span id="ATAP_DESC"><? echo $ATAP;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Jumlah lantai</td>
                            <td width="2%">:</td>
                            <td><span id="JMLLT_DESC"><? echo $JUMLAHLANTAI;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Luas</td>
                            <td width="2%">:</td>
                            <td><span id="IMB_DIM"><? echo $LUASIMB;?></span>&nbsp m2 &nbsp(IMB)
                                <span id="FIS_DIM"><? echo $LUASFISIK;?></span>&nbsp m2 &nbsp(Fisik)
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Pembagian ruang</td>
                            <td width="2%">:</td>
                            <td><span id="PEMBAGIAN_RUANG"><? echo $PEMBAGIANRUANG;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Fasilitas Bangunan</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_BANGUNAN"><? echo $FASILITASBANGUNAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Tahun di bangun</td>
                            <td width="2%">:</td>
                            <td><span id="BUILTYEAR"><? echo $TAHUNDIBANGUN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">IMB No.</td>
                            <td width="2%">:</td>
                            <td><span id="IMB_NO"><? echo $NOIMB;?></span>&nbsp Tgl :
                            &nbsp<span id="IMB_DATE"><? echo $TGLIMB;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Dihuni oleh</td>
                            <td width="2%">:</td>
                            <td><span id="HUNI_DESC"><? echo $DIHUNIOLEH;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Keterangan lain</td>
                            <td width="2%">:</td>
                            <td><span id="BLD_NOTE"><? echo $KETERANGAN;?></span></td>
                        </tr>
                    
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    
                    </table>
                </td>
            </tr>


            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>IV.</strong></td>
                            <td><strong>ANALISA LINGKUNGAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Jalan Pencapaian</td>
                            <td width="2%">:</td>
                            <td><span id="JALAN_PENCAPAIAN"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Jalan didepan lokasi</td>
                            <td width="2%">:</td>
                            <td><span id="PMKJLN_DESC"></span>&nbsp;lebar&nbsp;
                                <span id="LEBAR_JLNDPNLOK">0</span>&nbsp;m&nbsp;&nbsp;kondisi&nbsp;
                                <span id="CNDJLN_DESC"></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Arus lalu lintas</td>
                            <td width="2%">:</td>
                            <td><span id="LLARAH_DESC"></span>&nbsp;arah&nbsp;dgn intensitas
                                <span id="LLINTN_DESC"></span>&nbsp;
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>4</td>
                            <td width="30%">Fasilitas Umum</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_UMUM"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Fasilitas Angkutan</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_ANGKUTAN"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Obyek Penting dekat lokasi</td>
                            <td width="2%">:</td>
                            <td><span id="OBJEK_PENTING_DEKAT_LOKASI"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Peruntukan terbaik</td>
                            <td width="2%">:</td>
                            <td><span id="PRNTK_DESC"></span></td>
                        </tr>
                                            
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                                            
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1"><strong>V.</strong></td>
                            <td><strong>ASURANSI</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <div>

</div>
                </td>
            </tr>

            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VI.</strong></td>
                            <td><strong>KONSULTASI/ INFORMASI HARGA</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td align="left">
                    <table width="100%">
                        <tr class="text1">
                            <td width="3%">1.</td>
                            <td width="20%">Ditawarkan/ dijual</td>
                            <td width="2%">:</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div>&nbsp;</div>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td>NJOP Tahun (<span id="NJOP_YYYY">2012</span>)</td>
                            <td>:</td>
                            <td>&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VII.</strong></td>
                            <td><strong>PENILAIAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <table width="100%" style="border-collapse:collapse" border="1">
                        <tr>
                            <td width="70%" colspan="2" rowspan="2" style="text-align: center"><b>URAIAN</b></td>
                            <td colspan="2" style="text-align: center"><b>PERHITUNGAN</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><b>SESUAI IMB</b></td>
                            <td style="text-align: center"><b>FISIK</b></td>
                        </tr>
                        <tr>
                            <td width="3%">1.</td>
                            <td>Nilai Wajar</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">&nbsp;</td>
                            <td>
                                <table width="100%">
                                    <tr valign="top">
                                        <td width="25%">Tanah</td>
                                        <td width="2%">:</td>
                                        <td><span id="LUAS_TANAH"><? echo $LUAS_TANAH;?></span></td>
                                        <td width="2%">X</td>
                                        <td><span id="NILAI_TANAH_PERM2"><? echo numberFormat($NILAI_TANAH_PERM2);?></span></td>
                                        <td width="2%">&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan (IMB)</td>
                                        <td>:</td>
                                        <td><span id="LUAS_BANG_IMB"><? echo $LUAS_BANG_IMB;?></span></td>
                                        <td>X</td>
                                        <td><span id="NILAI_BANG_IMB_PERM2"><? echo numberFormat($NILAI_BANG_IMB_PERM2);?></span></td>
                                        <td>X</td>
                                        <td><span id="NILAI_BANG_IMB_PERCENT"><? echo $NILAI_BANG_IMB_PERCENT;?></span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan (Fisik)</td>
                                        <td>:</td>
                                        <td><span id="LUAS_BANG_FISIK"><? echo $LUAS_BANG_FISIK;?></span></td>
                                        <td>X</td>
                                        <td><span id="NILAI_BANG_FISIK_PERM2"><? echo numberFormat($NILAI_BANG_FISIK_PERM2);?></span></td>
                                        <td>X</td>
                                        <td><span id="NILAI_BANG_FISIK_PERCENT"><? echo $NILAI_BANG_FISIK_PERCENT;?></span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" style="text-align: center"><b>TOTAL NILAI TANAH DAN BANGUNAN</b></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr valign="top">
                                        <td class="style2"><span id="NILAI_TANAH_TOTAL"><? echo numberFormat($NILAI_TANAH_TOTAL);?></span></td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="NILAI_BANG_IMB_TOTAL"><? echo numberFormat($NILAI_BANG_IMB_TOTAL);?></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="style2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="NILAI_TOTAL_IMB"><? echo numberFormat($NILAI_TOTAL_IMB);?></span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td class="style2"><span id="TX_NILAI_TANAH_TOTAL"><? echo numberFormat($NILAI_TANAH_TOTAL);?></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="NILAI_BANG_FISIK_TOTAL"><? echo numberFormat($NILAI_BANG_FISIK_TOTAL);?></span></td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="NILAI_TOTAL_FISIK"><? echo numberFormat($NILAI_TOTAL_FISIK);?></span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="3%">2.</td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">Safety Margin IMB</td>
                                        <td width="3%">:</td>
                                        <td><span id="SAFETY_MARGIN"><? echo $SAFETY_MARGIN;?></span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Safety Margin Fisik</td>
                                        <td width="3%">:</td>
                                        <td><span id="SAFETY_MARGIN_FISIK"><? echo $SAFETY_MARGIN_FISIK;?></span>&nbsp;%</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">3.</td>
                            <td>
                                Nilai Likuidasi</td>
                            <td class="style2"><span id="NILAI_LIKUIDASI_IMB"><? echo numberFormat($NILAI_LIKUIDASI_IMB);?></span>
                                </td>
                            <td class="style2"><span id="NILAI_LIKUIDASI_FISIK"><? echo numberFormat($NILAI_LIKUIDASI_FISIK);?></span>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="TR_OVR" class="text1">
	<td>
                    <table width="100%" style="border-collapse:collapse" border="1">
                        <tr>
                            <td width="70%" colspan="2" rowspan="2" style="text-align: center"><b>URAIAN OVERRIDE</b></td>
                            <td colspan="2" style="text-align: center"><b>PERHITUNGAN&nbsp;</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: center"><b>SESUAI IMB</b></td>
                            <td style="text-align: center"><b>FISIK</b></td>
                        </tr>
                        <tr>
                            <td width="3%">1.</td>
                            <td>Nilai Wajar</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">&nbsp;</td>
                            <td>
                                <table width="100%">
                                    <tr valign="top">
                                        <td width="25%">Tanah</td>
                                        <td width="2%">:</td>
                                        <td><span id="OVR_LUAS_TANAH">0</span></td>
                                        <td width="2%">X</td>
                                        <td><span id="OVR_LND_VAL">0</span></td>
                                        <td width="2%">&nbsp;</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan (IMB)</td>
                                        <td>:</td>
                                        <td><span id="OVR_LUAS_BANG_IMB">0</span></td>
                                        <td>X</td>
                                        <td><span id="OVR_BLD_IMB_VAL">0</span></td>
                                        <td>X</td>
                                        <td><span id="OVR_NILAI_BANG_IMB_PERCENT">0</span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td>Bangunan (Fisik)</td>
                                        <td>:</td>
                                        <td><span id="OVR_LUAS_BANG_FISIK">0</span></td>
                                        <td>X</td>
                                        <td><span id="OVR_BLD_FIS_VAL">0</span></td>
                                        <td>X</td>
                                        <td><span id="OVR_NILAI_BANG_FISIK_PERCENT">0</span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" style="text-align: center"><b>TOTAL NILAI TANAH DAN BANGUNAN</b></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr valign="top">
                                        <td class="style2"><span id="OVR_LND_TOT">0</span></td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="OVR_BLD_IMB_TOT">0</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" class="style2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="OVR_TOT_IMB">0</span></td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td class="style2"><span id="T_OVR_LND_TOT">0</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="OVR_BLD_FIS_TOT">0</span></td>
                                    </tr>
                                    <tr>
                                        <td class="style2"><span id="OVR_TOT_FIS">0</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="3%">2.</td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">Safety Margin IMB</td>
                                        <td width="3%">:</td>
                                        <td><span id="OVR_SAFETY_MARGIN">0</span>&nbsp;%</td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Safety Margin Fisik</td>
                                        <td width="3%">:</td>
                                        <td><span id="OVR_SAFETY_MARGIN_FISIK">0</span>&nbsp;%</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">3.</td>
                            <td>
                                Nilai Likuidasi</td>
                            <td class="style2"><span id="OVR_LIK_IMB">0</span>
                                </td>
                            <td class="style2"><span id="OVR_LIK_FIS">0</span>
                                </td>
                        </tr>
                    </table>
                </td>
</tr>


            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VIII.</strong></td>
                            <td><strong>OPINI PENILAI</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td align="left" width="2%">-</td>
                            <td align="left" width="15%">Positif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALPOS"><? echo $OPINIPOSITIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Negatif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNEG"><? echo $OPININEGATIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Kondisi Pasar</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="MK_DESC"><? echo $KONDISIPASAR;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Catatan/ Alasan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNOTE"><? echo $CATATAN;?></span></td>
                        </tr>
                    
                    </table>
                </td>
            </tr>

            <!--<tr class="text1">
                <td align="left">
                    <span id="TTD"><table width="100%"> <tr><td width="33%">Penilai</td><td>Pemeriksa</td><td width="33%">Confirm/ Override</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>(Arasy Aulia Topan - KC Bogor)</td><td>( Yudhi Hendriatno - RO Jakarta)</td><td>(Puspa Rengganis)</td></tr></table></span>
                </td>
            </tr>-->
            </table>    
    <input type="button" id="btn_print" value="Print" onclick="onPrint()" />
    </div>
    
<script type="text/javascript">
//<![CDATA[
var __enabledControlArray =  new Array('h_mcolid', 'h_colid_lnd', 'h_sum_cert_lnd', 'h_APP_OFFICER', 'h_SPV_OFFICER', 'h_PINCA');
//]]>
</script>



</form>
</body>
</html>
