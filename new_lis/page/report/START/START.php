<?
	include("../../../requirepage/session.php");
	require_once ("../../../lib/open_con.php");
	require_once ("../../../lib/formatError.php");
	/*$userid=$_REQUEST['userid'];
	$userbranch = $_REQUEST['userbranch'];
	$userregion = $_REQUEST['userregion'];*/
	
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
		<script src="../../../jquery/js/jquery-1.11.3.js"></script>
		<script src="../../../bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="../../../js/jquery-latest.min.js" type="text/javascript"></script>
		<script src="../../../js/menu_ce.js" type="text/javascript"></script>
		<link href="../../../css/menu_ce.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../../js/accounting.js" ></script>
		<link href="../../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			var userid = "<?echo $userid?>";
			var userbranch = "<?echo $userbranch?>";
			var userregion = "<?echo $userregion?>";
			function validation()
			{
				
				var rand_no1 = Math.random();
				var rand_no2 = Math.random();
				var rand_no = rand_no1 * rand_no2;
				
				var daily1 = $("#daily1").val();
				var daily2 = $("#daily2").val();
				var StatusAllowSubmit=true;
				
				if (daily1=="")
				{
					alert("Dari tanggal Harus di isi");
					$("#daily1").focus();
				}
				else if (daily2=="")
				{
					alert("Sampai tanggal Harus di isi");
					$("#daily2").focus();
				}
				else
				{
					$.ajax	
					({
						type: "GET",
						url: "ajax.php",
						data: "userid="+userid+"&userbranch="+userbranch+"&userregion="+userregion+"&random="+ rand_no +"&daily1="+ daily1 +"&daily2="+ daily2 +"",
						success: function(response)
						{
							//alert(response);
							$("#result").html(response);
						}
					});
				}
			}
			
			function btnpage(thisid)
			{
				var rand_no1 = Math.random();
				var rand_no2 = Math.random();
				var rand_no = rand_no1 * rand_no2;
				var page = $("#"+thisid).val();
				var daily1 = $("#daily1").val();
				var daily2 = $("#daily2").val();
				//alert (page);
				$.ajax	
				({
					type: "GET",
					url: "ajax.php",
					data: "page="+page+"&userid="+userid+"&userbranch="+userbranch+"&userregion="+userregion+"&random="+ rand_no +"&daily1="+ daily1 +"&daily2="+ daily2 +"",
					success: function(response)
					{
						//alert(response);
						$("#result").html(response);
					}
				});
			}
		</script>
	</head>
	<body>
		<div style="position:fixed;">
				<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
			</div>
			<div style="border:0px solid black;width:23%;height:545px;margin-top:7%;float:left;position:fixed;">
<!--Open Menu-->
				<div id="cssmenu">
					<ul>
<?
	$userwfid=$_REQUEST['userwfid'];
	if($userwfid==""){
		$userwfid=$_POST['userwfid'];
	}
	
	$tsql2 = "select program_group, program_code, program_desc from Tbl_SE_Program
			where program_code like '%$userwfid%'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$programgcd = $row2[0];
			$programcd = $row2[1];
			$programname = $row2[2];
			//echo $programgcd;
		}
	}
	sqlsrv_free_stmt( $sqlConn2 );
	
	$d = date("d");
	$m = date("m");
	$y = date("y");
	$h = date("h");
	$i = date("i");
	$s = date("s");
	$randomtime = $h.$i.$s;
	
	$tsql2 = "select * from Tbl_SE_User U LEFT JOIN Tbl_Branch B ON U.user_branch_code = B.branch_code where user_id='$userid'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$userbranch = $row2[7];
			$userpwd = $row2[9];
			$userregion = $row2[21];
		}
	}
			
	$tsql = "select Tbl_SE_GrpProgram.grp_code, Tbl_SE_GrpProgram.grp_name from Tbl_SE_GrpProgram, Tbl_SE_Program, Tbl_SE_UserProgram
				where Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
				AND Tbl_SE_Program.program_group=Tbl_SE_GrpProgram.grp_code
				AND Tbl_SE_UserProgram.user_id='$userid'
				GROUP BY Tbl_SE_GrpProgram.grp_code,Tbl_SE_GrpProgram.grp_name, Tbl_SE_GrpProgram.grp_urut
				ORDER BY Tbl_SE_GrpProgram.grp_urut,Tbl_SE_GrpProgram.grp_name";
	  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	  $params = array(&$_POST['query']);

	  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	  if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	  if(sqlsrv_has_rows($sqlConn))
	  {
		 $rowCount = sqlsrv_num_rows($sqlConn);
		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		 {
			$tsql2 = "SELECT Tbl_SE_Program.program_code,
						Tbl_SE_Program.program_name,Tbl_SE_Program.program_desc
						FROM Tbl_SE_Program, Tbl_SE_UserProgram
						WHERE Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
						AND Tbl_SE_UserProgram.user_id='$userid'
						AND Tbl_SE_Program.program_group='$row[0]'
						ORDER BY program_urut";
			  $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			  $params2 = array(&$_POST['query']);

			  $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
			  if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			  if(sqlsrv_has_rows($sqlConn2))
			  {
				 $rowCount2 = sqlsrv_num_rows($sqlConn2);
				 
				 if($row['0']=="$programgcd"){
					 echo "<li class='selected'>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }else{
					 echo "<li>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }
				 
				 if($rowCount2 > '15')
				 {
					echo "<ul style='overflow-x:hidden;overflow-y:scroll;white-space:nowrap;height:150px;'>";
				 }else{
					echo "<ul>";
				 }
				 
				 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				 {
					if($row2[2]=="$programname" && $row2[0]=="$programcd")
					{
						echo "<li class='sub_selected'><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}else{
						echo "<li><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}
				 }
			  }
			  sqlsrv_free_stmt( $sqlConn2 );
				 echo "</ul>";
				 echo "</li>";
		 }
	  }
	  sqlsrv_free_stmt( $sqlConn );
?>
						<li>
							<a href="#"><span>SETTING</span></a>
							<ul>
								<li><a href='../../changepassword.php?userwfid=changepassword'>Change Password</a></li>
								<li><a href='../../logout.php'>Log Out</a></li>
							</ul>
						</li>
					</ul>
				</div>
<!--Close Menu-->
				<img src="../../../images/gimmick_logo.png" style="position:absolute;bottom:0;width:60%;margin-left:20%;margin-right:20%;"></img>
			</div>
			<div style="float:right;width:75%;margin-top:7%;margin-right:2%;">	
		<form id="formentry" name="formentry" method="post">
			<table align="center" border="0" style=" border:1px solid black;">
				<tr>
					<td colspan="4" style="font-size:16;font-weight:bold; text-align:center;">REPORT APLIKASI MASUK</td>
				</tr>
				<tr>
					<td colspan="4" style="font-size:16;font-weight:bold; text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:150px; padding-left:10px;">Dari Tanggal</td>
					<td><input style="background:#ff0; width:150px;" type="text" id="daily1" name="daily1" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');"/></td>
					<td style="width:150px; padding-left:10px;">Sampai Tanggal</td>
					<td><input style="background:#ff0; width:150px;margin-right:10px;" type="text" id="daily2" name="daily2" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');"/></td>
				</tr>
			</table>
			<div>&nbsp;</div>
			<div style="text-align:center;"><input type="button" id="btnsubmit" name="btnsubmit" style="width:100px;" class="button" value="submit"onclick="validation()"/></div>
			<div>&nbsp;</div>
			<div id="result">
				
			</div>
		</form>
			</div>
	</body>
</html>