<?
require_once ("../../lib/formatError.php");
require_once ("../../lib/open_con.php");
require_once ("../../requirepage/parameter.php");


//echo $userwfid;

$cr_notes = "";
$cr_recomend = "";

$tsql = "SELECT * FROM Tbl_CustomerMasterBRA WHERE custnomid = '$custnomid'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);

$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
  $rowCount = sqlsrv_num_rows($sqlConn);
  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
  {
	  $cr_notes = $row['notes'];
	  $cr_recomend = $row['recomend'];
  }
}
sqlsrv_free_stmt( $sqlConn );


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<link href="../../css/d.css" rel="stylesheet" type="text/css" />

<title>Branch Review</title>
</head>

<body>
<div align="center">
<span style="font-weight:bold">Branch Review</span>

<div align="center" style="border:1px solid black;width:960px">

<table width="960px" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?
  $param = "custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction&userbranch=$userbranch&userregion=$userregion&userid=&userpwd=$userpwd";
  ?>
  <tr>
    <th scope="col">&nbsp;</th>
    <th scope="col"><a href="../analisa5c/analisa5c.php?<?=$param;?>">Hasil Analisa Calon Debitur</a></th>
    <th scope="col"><a href="../neraca/neraca.php?<?=$param;?>">Neraca Rugi Laba</a></th>
    <th scope="col"><a href="../mkk/view_mkk_n.php?<?=$param;?>">Memo Keputusan Kredit</a></th>
    <th scope="col">&nbsp;</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
    <p>Notes :</p>
    <p><?=$cr_notes;?></p>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <?
	if($cr_recomend == "Y")
	{
		echo "DIREKOMENDASIKAN";
	}
	else
	{
		echo "TIDAK DIREKOMENDASIKAN";
	}
    
	?>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">
    <form name="frm" id="frm" method="get" action="">
	<?
	
	require("../../requirepage/btnview.php");
	require("../../requirepage/hiddenfield.php");
	?>
    
    &nbsp;
    </form>
    </td>
    <td>&nbsp;
	
    </td>
    <td>&nbsp;</td>
  </tr>
</table>

</div>
</div>
</body>
</html>