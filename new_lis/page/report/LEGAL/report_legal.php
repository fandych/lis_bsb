<?php
include("../../../requirepage/session.php");
require ("../../../lib/formatError.php");
require ("../../../lib/open_con.php");

/*
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];*/
$admin = $userregion;
$userwfid=$_REQUEST['userwfid'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="../../../js/jquery-latest.min.js" type="text/javascript"></script>
<script src="../../../js/menu_ce.js" type="text/javascript"></script>
<link href="../../../css/menu_ce.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
<script src="../../../js/jquery-1.7.2.min.js"></script>
<script>
function getParamRegion() 
{	
    var regioncode=$("#regioncode").val();
    
	//alert(regioncode);
    $.ajax({
        type: "GET",
        url: "get_branch.php",
        data: "regioncode="+regioncode+"&random="+ <?php echo time(); ?> +"",
        success: function(response)
        {
            //alert(response);
            $("#resultRegion").html(response);
        }
    });
}
</script>
<title>REPORT LEGAL</title>
</head>
<body style="margin:0px;padding:0px;">
	<form name="formsubmit" method="post" action="result_report_legal.php?r=<?echo $randomtime?>&userwfid=<?echo $userwfid?>">
	<div style="position:fixed;">
				<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
			</div>
			<div style="border:0px solid black;width:23%;height:545px;margin-top:7%;float:left;position:fixed;">
<!--Open Menu-->
				<div id="cssmenu">
					<ul>
<?
	
	$tsql2 = "select program_group, program_code, program_desc from Tbl_SE_Program
			where program_code like '%$userwfid%'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$programgcd = $row2[0];
			$programcd = $row2[1];
			$programname = $row2[2];
			//echo $programgcd;
		}
	}
	sqlsrv_free_stmt( $sqlConn2 );
	
	$d = date("d");
	$m = date("m");
	$y = date("y");
	$h = date("h");
	$i = date("i");
	$s = date("s");
	$randomtime = $h.$i.$s;
	
	$tsql2 = "select * from Tbl_SE_User U LEFT JOIN Tbl_Branch B ON U.user_branch_code = B.branch_code where user_id='$userid'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$userbranch = $row2[7];
			$userpwd = $row2[9];
			$userregion = $row2[21];
		}
	}
			
	$tsql = "select Tbl_SE_GrpProgram.grp_code, Tbl_SE_GrpProgram.grp_name from Tbl_SE_GrpProgram, Tbl_SE_Program, Tbl_SE_UserProgram
				where Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
				AND Tbl_SE_Program.program_group=Tbl_SE_GrpProgram.grp_code
				AND Tbl_SE_UserProgram.user_id='$userid'
				GROUP BY Tbl_SE_GrpProgram.grp_code,Tbl_SE_GrpProgram.grp_name, Tbl_SE_GrpProgram.grp_urut
				ORDER BY Tbl_SE_GrpProgram.grp_urut,Tbl_SE_GrpProgram.grp_name";
	  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	  $params = array(&$_POST['query']);

	  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	  if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	  if(sqlsrv_has_rows($sqlConn))
	  {
		 $rowCount = sqlsrv_num_rows($sqlConn);
		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		 {
			$tsql2 = "SELECT Tbl_SE_Program.program_code,
						Tbl_SE_Program.program_name,Tbl_SE_Program.program_desc
						FROM Tbl_SE_Program, Tbl_SE_UserProgram
						WHERE Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
						AND Tbl_SE_UserProgram.user_id='$userid'
						AND Tbl_SE_Program.program_group='$row[0]'
						ORDER BY program_urut";
			  $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			  $params2 = array(&$_POST['query']);

			  $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
			  if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			  if(sqlsrv_has_rows($sqlConn2))
			  {
				 $rowCount2 = sqlsrv_num_rows($sqlConn2);
				 
				 if($row['0']=="$programgcd"){
					 echo "<li class='selected'>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }else{
					 echo "<li>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }
				 
				 if($rowCount2 > '15')
				 {
					echo "<ul style='overflow-x:hidden;overflow-y:scroll;white-space:nowrap;height:150px;'>";
				 }else{
					echo "<ul>";
				 }
				 
				 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				 {
					if($row2[2]=="$programname" && $row2[0]=="$programcd")
					{
						echo "<li class='sub_selected'><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}else{
						echo "<li><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}
				 }
			  }
			  sqlsrv_free_stmt( $sqlConn2 );
				 echo "</ul>";
				 echo "</li>";
		 }
	  }
	  sqlsrv_free_stmt( $sqlConn );
?>
						<li>
							<a href="#"><span>SETTING</span></a>
							<ul>
								<li><a href='../../changepassword.php?userwfid=changepassword'>Change Password</a></li>
								<li><a href='../../logout.php'>Log Out</a></li>
							</ul>
						</li>
					</ul>
				</div>
<!--Close Menu-->
				<img src="../../../images/gimmick_logo.png" style="position:absolute;bottom:0;width:60%;margin-left:20%;margin-right:20%;"></img>
			</div>
			<div style="float:right;width:75%;margin-top:7%;margin-right:2%;">
	<table align="center" >
	<tr><td>
		<table  cellpadding="0" cellspacing="0" align="center" width="400" style="margin-top:100px;">
			<tr>
				<td valign="bottom">REPORT LEGAL</td>
			</tr>
			<tr>
				<td valign="top"><img src="../../../images/black_line.png"/></td>
			</tr>
		</table>
		</td></tr>
		<tr><td>
		<table  style="background:#FFF;" width="400"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="black" bordercolorlight="#000000" bordercolordark="#ffffff" >
			<tr>
				<td>Select date</td>
				<td>&nbsp;&nbsp;<input style="width: 70px;background-color: #FF0;" type="text" id="tgl1" name="tgl1"/>
				<a href="javascript:NewCssCal('tgl1','yyyymmdd')"><img src="../../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a>
				-----
				<input style="width: 70px;background-color: #FF0;" type="text" id="tgl2" name="tgl2"/>
				<a href="javascript:NewCssCal('tgl2','yyyymmdd')"><img src="../../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a>
				</td>
			</tr>
			<tr>
				<td>Regional</td>
				<td>
					&nbsp;
					<select name=regioncode id=regioncode nai="Region" style="background-color:white" onchange="getParamRegion();">
					<option value='all'>-- All --</option>
					<?
						if($admin =="888"){
							$sql = "select * from tbl_region";
							$rs = sqlsrv_query($conn,$sql);
							if($rs===false){
								die(FormatErrors(sqlsrv_errors()));
							} 
							if (sqlsrv_has_rows($rs)){
								while($row = sqlsrv_fetch_array($rs,SQLSRV_FETCH_ASSOC)){
									echo "<option value='$row[region_code]'>[$row[region_code]] $row[region_name]</option>";
								}
							}
						} else {
							$sql = "select * from tbl_region where region_code='$admin'";
							$rs = sqlsrv_query($conn,$sql);
							if($rs===false){
								die(FormatErrors(sqlsrv_errors()));
							} 
							if (sqlsrv_has_rows($rs)){
								while($row = sqlsrv_fetch_array($rs,SQLSRV_FETCH_ASSOC)){
									echo "<option value='$row[region_code]'>[$row[region_code]] $row[region_name]</option>";
								}
							}
						}
					?>
					</select>
				</td>
			</tr>
            <!--
			<tr>
				<td>Per Cabang</td>
				<td>
					<span id="resultRegion">
						&nbsp;
						<select name=branchcode id=branchcode nai="Cabang" style="background-color:white">
							<option value=''>-- All --</option>
						</select>
					</span>
				</td>
			</tr>
    
			<tr>
				<td>Status</td>
				<td>
					&nbsp;
						<select name=status id=status nai="Status" style="background-color:white">
							<option value=''>-- All --</option>
							<option value='A'>Approve</option>
							<option value='I'>In process</option>
							<option value='R'>Reject</option>
						</select>
				</td>
			</tr>
            !-->
			<tr>
				<td align="center" colspan="2"><input type="submit" value="Submit"></td>
				<input type=hidden name=inadmin value='<? echo $admin; ?>'>
			</tr>
		</table>
		</tr></td>
		</table>
	</div>
	</form>
</body>
</html>
