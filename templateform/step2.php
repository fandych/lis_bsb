<?
session_start();
ob_start();
require_once("sqlsrv.php");


ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");



$templatecontent="";
if(isset($_POST['hidval']))
{
	$templatecontent = $_SESSION['templatecontent'] = str_replace('|||','"',$_POST['hidval']);
	$templatename = $_SESSION['templatename'] = str_replace('|||','"',$_POST['tempname']);
	
}
else
{
	if(isset($_SESSION['templatecontent']))
	{
		$templatecontent = $_SESSION['templatecontent'] = str_replace('|||','"',$_SESSION['templatecontent']);
		$templatename = $_SESSION['templatename'] = str_replace('|||','"',$_SESSION['templatename']);
	}
}



?>

<!DOCTYPE html>
<html>
<head>
<title>Create Tempalte</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />



<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="js/jquery-1.12.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>

<script>
function focusid(thisid)
{
	//alert(thisid);
	//detailtable
	$(".detailtable").css({ 'display' : 'none'});
	$("#"+thisid).css({'display' : 'block'});
	//$('#'+thisid).scrollIntoView();
}
</script>


<style>
	td{padding:5px;}
</style>
</head>
<body>
<form id="frm" name="frm" method="post" >
	<div class="container">

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="width:1200px;">

		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Table and Field From Database</h4>
				</div>
				<div class="modal-body" style="height:420px;">
				
					<div style="width:370px;border:0px solid black;height:400px;overflow-y: scroll;float:left">
						
						<?
								$strsql="select TABLE_NAME from information_schema.TABLES where TABLE_CATALOG='".$db."'
										 and TABLE_NAME in(select distinct(TABLE_NAME) from INFORMATION_SCHEMA.COLUMNS
										 where COLUMN_NAME like '%custnomid%')";
								//echo $strsql;
								$rs = $condb->_RQ($strsql);
								for( $i=0; $i<count($rs); $i++)
								{
									$tablename=$rs[$i]['TABLE_NAME'];

									$activetab="";
									if($i==0)
									{
										$activetab="class=\"active\"";
									}
									?><div style="width:350px;padding:5px;" onclick="focusid('<?=$tablename?>');" <?=$activetab?>><a data-toggle="tab" href="#<?=$tablename?>"><?=$tablename?></a></div><?

								}
								/*

								$rs2 = $condb->_RQ("select COLUMN_NAME from information_schema.COLUMNS where TABLE_SCHEMA='".$db."' and TABLE_NAME='".$tablename."'");
								foreach($rs2 as $key2 => $value2) {
									$columnname = $rs2[$key2]['COLUMN_NAME'];
									echo $columnname."<br/>";
								}
								echo "</br>";
								*/
							?>
						
					</div>
					<div style="width:795px;border:0px solid black;height:400px;overflow-y: scroll;float:left;">
					
						<?php
							//$strsql="select TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA='".$db."'";
							//echo $strsql;
							$rs = $condb->_RQ($strsql);
							if(!is_array($rs)){echo $rs;}else{
								$i=0;
								foreach($rs as $key => $value) {

									$tablename = $rs[$key]['TABLE_NAME'];

									$activetab="";
									if($i==0)
									{
										$activetab="in active";
									}

									?><div class="detailtable" style="margin-top:10px; padding:5px;display:none;" id="<?=$tablename?>" ><?

									echo $tablename."<hr>";
									$rs2 = $condb->_RQ("select COLUMN_NAME from information_schema.COLUMNS where TABLE_CATALOG='".$db."' and TABLE_NAME='".$tablename."'");
									//echo "select COLUMN_NAME from information_schema.COLUMNS where TABLE_SCHEMA='".$db."' and TABLE_NAME='".$tablename."'";

									foreach($rs2 as $key2 => $value2) {
										$columnname = $rs2[$key2]['COLUMN_NAME'];
										?>
										<input onclick="setfieldtabledb(this.id)" id="<?=$tablename.$columnname?>" name="<?=$tablename.$columnname?>" data-dismiss="modal" type="button" class="btn btn-info" style="margin:10px 5px;height:30px;" havevalue="<?=$tablename.".".$columnname?>" value="<?=$columnname?>">
										<?
									}
									$i++;
									?></div><?
								}
							}

						?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	




		<?php

		$arrysetget = array("Textbox","Textarea","Combobox_Y_N");
		$arryvalue = array("Mandatory","NonMandatory");
		$arrytype = array("Text","Number","Currency","Pickerdate");
		//echo str_word_count($_POST['hidval']);


		if(str_word_count($templatecontent)>0)
		{
			?>
			<div style="text-align:left;font-size:30px;line-height:45px;">Variable  <? echo $_SESSION['template']. "  " .$_SESSION['templatename'] ?>
				<div style="float:right;line-height:45px;">
					<a href="tmp.php?a=3&b=<?=$_SESSION['template']?>" class="btn btn-warning btn-md" role="button"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
				</div>

			</div>
			<hr>
			<table style="width:100%;" border="0">
			<?
			$rs=str_word_count($templatecontent, 1, '0..3');
			$namevar="";

			$totalvariable = 0;
			for( $i=0; $i<count($rs); $i++)
			{
				$checkvalues1=strpos($rs[$i], "varspin0");
				$checkvalues2=strpos($rs[$i], "varspin1");
				if ($checkvalues1!==false || $checkvalues2!==false)
				{
					//set tidak hanya varspin0 or varspin1
					if(strlen($rs[$i])>8)
					{


						$totalvariable++;
						//check to set or get
						// 1 = get, 0 = set
						$checkgetorset = strpos($rs[$i], "varspin1");
						$_setget="Browse to DB";
						$_value="tr_template._name";
						$_type="";
						$_param="";

						$strsql="select * from tmp_templatedetail where _template_code='".$_SESSION['template']."' and _namevar='".$rs[$i]."'";
						///echo $strsql;
						$rss = $condb->_RQ($strsql);
						if(!is_array($rss)){echo $rss;}else
						{
							foreach($rss as $key => $value) {
								//print_r($value);
								$_setget=$rss[$key]['_setget'];
								$_value=$rss[$key]['_value'];
								$_type=$rss[$key]['_type'];
								$_param=$rss[$key]['_param'];
							}
						}
						$namevar.=$rs[$i].",";

						?>
						<tr>
							<td style="width:300px;"><?=$rs[$i]?></td>

							<td>
							<?
							//set or get
							if ($checkgetorset!==false)
							{
							?>
								<input data-toggle="modal" onclick="settextboxdb(this.id)" onfocus="settextboxdb(this.id)" data-target="#myModal" style="width:150px;text-align:left;padding-left:15px;background:#f0f0f0;cursor:pointer" type="text" readonly="readonly" id="<?=$rs[$i]?>" name="<?//=$rs[$i]?>setget[]" value ="<?=$_setget?>">
							<?
							}
							else
							{
							?>
								<select style="width:150px;" id="<?=$rs[$i]?>" name="<?//=$rs[$i]?>setget[]">
									<?
										foreach($arrysetget as $eachset)
										{
											$selected="";
											if($_setget==$eachset)
											{
												$selected="selected=\"selected\"";
											}
											?><option <?=$selected?> value="<?=$eachset?>"><?=$eachset?></option><?
										}
									?>
								</select>
							<?
							}
							?>
							</td>

							<td>
							<?
							// mandatory or nama table dan field from db
							if ($checkgetorset!==false)
							{
							?>
								<input type="text" style="width:250px;" readonly="readonly" id="value<?=$rs[$i]?>" name="value[]<?//=$rs[$i]?>" value="<?=$_value?>">
							<?
							}
							else
							{
							?>
								<select style="width:250px;" id="value<?=$rs[$i]?>" name="value[]<?//=$rs[$i]?>">
								<?
									foreach($arryvalue as $eachvalue)
									{
										$selected="";
										if($_value==$eachvalue)
										{
											$selected="selected=\"selected\"";
										}
										?><option <?=$selected?> value="<?=$eachvalue?>"><?=$eachvalue?></option><?
									}
								?>
								</select>
							<?
							}
							?>
							</td>
							<td style="text-align:right;">
								<select style="width:200px;" id="type<?=$rs[$i]?>" name="type[]<?//=$rs[$i]?>">
									<?
									//type data dari set or get
									if ($checkgetorset!==false)
									{
										foreach($arrytype as $eachtype)
										{
											$selected="";
											if($_type==$eachtype)
											{
												$selected="selected=\"selected\"";
											}
											if($eachtype!="Number" && $eachtype!="Pickerdate"){
											?><option <?=$selected?> value="<?=$eachtype?>"><?=$eachtype?></option><?
											}
										}
									}
									else
									{
										foreach($arrytype as $eachtype)
										{
											$selected="";
											if($_type==$eachtype)
											{
												$selected="selected=\"selected\"";
											}
											?><option <?=$selected?> value="<?=$eachtype?>"><?=$eachtype?></option><?
										}

									}
									?>
								</select>
								
							</td>
							<td>
								<input type="text" style="width:200px;" id="param<?=$rs[$i]?>" name="param[]" value="<?=$_param?>">
							</td>
						</tr>
						<?
					}
				}
			}
			?>
			</table>
			<?


		}

		?>
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input onclick="onsubmitformnext()" style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
			<div>
				<input onclick="onsubmitformprev()" style="float:left;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="<< Prev">
			</div>
		</div>
	</div>
	<input type="hidden" name="namevar" id="namevar" value="<?=substr($namevar,0,-1)?>">
	<input type="hidden" name="tablefield" id="tablefield" >
</form>
<script>
	function onsubmitformnext()
	{
		if(<?=$totalvariable?>==0)
		{
			strerrmsg("Masukkan Variable Kedalam Template di Step 1")
		}
		else
		{
			$("#frm").attr('action', 'step3.php');
			$("#frm").submit();
		}
	}
	function onsubmitformprev()
	{
		$("#frm").attr('action', 'step1.php');
		$("#frm").submit();
	}
	function settextboxdb(thisid)
	{
		$("#tablefield").val(thisid);
	}
	function setfieldtabledb(thisid)
	{
		//alert(thisid);
		var settextbox = "value"+$("#tablefield").val();
		//alert($("#"+thisid).attr("havevalue"));
		$("#"+settextbox).val($("#"+thisid).attr("havevalue"))
		//alert($("#"+thisid).attr("havevalue"));
		$("#"+settextbox).focus();

	}
</script>
</body>
</html>
