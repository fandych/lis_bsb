<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");
	require_once ("../../requirepage/parameter.php");

	/*
	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	*/
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;

	require ("../../requirepage/currency.php");

	$tanggalkunjungan = "";
	$cabang = "";

	$tsql = "select * from Tbl_customermasterperson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
		}
	}


	//Tahap 1

	$jenispermohonankredit = "";

	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["custcreditstatus"];
		}
	}

	$tsql = "select status_name from TblCreditStatus where status_code = '$jenispermohonankredit'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["status_name"];
		}
	}


	$bunga = 0;
	$bungatotal = 0;
	$bungapersen = 0;

	$tsql = "select custcreditplafond from tbl_CustomerFacility where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["custcreditplafond"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	$tsql = "SELECT * FROM TBL_LKCDFASILITASPINJAMANBANKMEGA WHERE FASILITASPINJAMANBANKMEGA_JENISFASILITAS IN (SELECT JENISFASILITASBANKMEGA_CODE FROM TBL_LKCDJENISFASILITASBANKMEGA WHERE JENISFASILITASBANKMEGA_FLAG = 'UKM') AND CUSTNOMID = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["FasilitasPinjamanBankMega_outstanding"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	//$tsqlproduk = "select BUNGA_PERSEN from TBL_MASTER_BUNGA where BUNGA_MIN < '$bungatotal' AND BUNGA_MAX >= '$bungatotal'";
	$tsqlproduk = "SELECT sukubunga as BUNGA_PERSEN FROM tbl_customerfacility WHERE custnomid = '$custnomid'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungapersen = $rowproduk["BUNGA_PERSEN"];
		}
	}

	// Tahap 2
	//----------

	// Tahap 7

	$InfoFasilitasPinjamanBankLain_cb1 = "";
	$InfoFasilitasPinjamanBankLain_cb2 = "";
	$InfoFasilitasPinjamanBankLain_cb3 = "";

	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankLain where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankLain_cb1 = $row["InfoFasilitasPinjamanBankLain_cb1"];
			$InfoFasilitasPinjamanBankLain_cb2 = $row["InfoFasilitasPinjamanBankLain_cb2"];
			$InfoFasilitasPinjamanBankLain_cb3 = $row["InfoFasilitasPinjamanBankLain_cb3"];

		}
	}

	//Tahap 8

	$InfoFasilitasPinjamanBankMega_nocif = "";
	$InfoFasilitasPinjamanBankMega_nasabahsejak = "";
	$InfoFasilitasPinjamanBankMega_datapertanggal = "";
	$InfoFasilitasPinjamanBankMega_cb1 = "";

	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankMega where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankMega_nocif = $row["InfoFasilitasPinjamanBankMega_nocif"];
			$InfoFasilitasPinjamanBankMega_nasabahsejak = $row["InfoFasilitasPinjamanBankMega_nasabahsejak"];
			$InfoFasilitasPinjamanBankMega_datapertanggal = $row["InfoFasilitasPinjamanBankMega_datapertanggal"];
			$InfoFasilitasPinjamanBankMega_cb1 = $row["InfoFasilitasPinjamanBankMega_cb1"];

		}
	}

	//Tahap 9

	$TradeCheck_tanggalpemasok1 = "";
	$TradeCheck_namapemberiinfopemasok1 = "";
	$TradeCheck_namapemasok1 = "";
	$TradeCheck_noteleponpemasok1 = "";
	$TradeCheck_alamatpemasok1 = "";
	$TradeCheck_lamahubunganpemasok1 = "";
	$TradeCheck_jenisbarangpemasok1 = "";
	$TradeCheck_ratapenjualanpemasok1 = 0;
	$TradeCheck_termpembayaranpemasok1 = "";
	$TradeCheck_karaktercadebpemasok1 = "";
	$TradeCheck_infonegatifpemasok1 = "";
	$TradeCheck_ketepatanpemasok1 = "";
	$TradeCheck_tanggalpemasok2 = "";
	$TradeCheck_namapemberiinfopemasok2 = "";
	$TradeCheck_namapemasok2 = "";
	$TradeCheck_noteleponpemasok2 = "";
	$TradeCheck_alamatpemasok2 = "";
	$TradeCheck_lamahubunganpemasok2 = "";
	$TradeCheck_jenisbarangpemasok2 = "";
	$TradeCheck_ratapenjualanpemasok2 = 0;
	$TradeCheck_termpembayaranpemasok2 = "";
	$TradeCheck_karaktercadebpemasok2 = "";
	$TradeCheck_infonegatifpemasok2 = "";
	$TradeCheck_ketepatanpemasok2 = "";
	$TradeCheck_tanggalpembeli1 = "";
	$TradeCheck_namapemberiinfopembeli1 = "";
	$TradeCheck_namapembeli1 = "";
	$TradeCheck_noteleponpembeli1 = "";
	$TradeCheck_alamatpembeli1 = "";
	$TradeCheck_lamahubunganpembeli1 = "";
	$TradeCheck_jenisbarangpembeli1 = "";
	$TradeCheck_ratapenjualanpembeli1 = 0;
	$TradeCheck_termpembayaranpembeli1 = "";
	$TradeCheck_karaktercadebpembeli1 = "";
	$TradeCheck_infonegatifpembeli1 = "";
	$TradeCheck_tanggalpembeli2 = "";
	$TradeCheck_namapemberiinfopembeli2 = "";
	$TradeCheck_namapembeli2 = "";
	$TradeCheck_noteleponpembeli2 = "";
	$TradeCheck_alamatpembeli2 = "";
	$TradeCheck_lamahubunganpembeli2 = "";
	$TradeCheck_jenisbarangpembeli2 = "";
	$TradeCheck_ratapenjualanpembeli2 = 0;
	$TradeCheck_termpembayaranpembeli2 = "";
	$TradeCheck_karaktercadebpembeli2 = "";
	$TradeCheck_infonegatifpembeli2 = "";
	$TradeCheck_tanggalkomunitas1 = "";
	$TradeCheck_namapemberiinfokomunitas1 = "";
	$TradeCheck_namakomunitas1 = "";
	$TradeCheck_noteleponkomunitas1 = "";
	$TradeCheck_alamatkomunitas1 = "";
	$TradeCheck_lamahubungankomunitas1 = "";
	$TradeCheck_jenisbarangkomunitas1 = "";
	$TradeCheck_ratapenjualankomunitas1 = 0;
	$TradeCheck_termpembayarankomunitas1 = "";
	$TradeCheck_karaktercadebkomunitas1 = "";
	$TradeCheck_infonegatifkomunitas1 = "";
	$TradeCheck_ketepatankomunitas1 = "";
	$TradeCheck_tanggalkomunitas2 = "";
	$TradeCheck_namapemberiinfokomunitas2 = "";
	$TradeCheck_namakomunitas2 = "";
	$TradeCheck_noteleponkomunitas2 = "";
	$TradeCheck_alamatkomunitas2 = "";
	$TradeCheck_lamahubungankomunitas2 = "";
	$TradeCheck_jenisbarangkomunitas2 = "";
	$TradeCheck_ratapenjualankomunitas2 = 0;
	$TradeCheck_termpembayarankomunitas2 = "";
	$TradeCheck_karaktercadebkomunitas2 = "";
	$TradeCheck_infonegatifkomunitas2 = "";
	$TradeCheck_ketepatankomunitas2 = "";

	$tsql = "select * from Tbl_LKCDTradeCheck where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$TradeCheck_tanggalpemasok1 = $row["TradeCheck_tanggalpemasok1"];
			$TradeCheck_namapemberiinfopemasok1 = $row["TradeCheck_namapemberiinfopemasok1"];
			$TradeCheck_namapemasok1 = $row["TradeCheck_namapemasok1"];
			$TradeCheck_noteleponpemasok1 = $row["TradeCheck_noteleponpemasok1"];
			$TradeCheck_alamatpemasok1 = $row["TradeCheck_alamatpemasok1"];
			$TradeCheck_lamahubunganpemasok1 = $row["TradeCheck_lamahubunganpemasok1"];
			$TradeCheck_jenisbarangpemasok1 = $row["TradeCheck_jenisbarangpemasok1"];
			$TradeCheck_ratapenjualanpemasok1 = $row["TradeCheck_ratapenjualanpemasok1"];
			$TradeCheck_termpembayaranpemasok1 = $row["TradeCheck_termpembayaranpemasok1"];
			$TradeCheck_karaktercadebpemasok1 = $row["TradeCheck_karaktercadebpemasok1"];
			$TradeCheck_infonegatifpemasok1 = $row["TradeCheck_infonegatifpemasok1"];
			$TradeCheck_ketepatanpemasok1 = $row["TradeCheck_ketepatanpemasok1"];
			$TradeCheck_tanggalpemasok2 = $row["TradeCheck_tanggalpemasok2"];
			$TradeCheck_namapemberiinfopemasok2 = $row["TradeCheck_namapemberiinfopemasok2"];
			$TradeCheck_namapemasok2 = $row["TradeCheck_namapemasok2"];
			$TradeCheck_noteleponpemasok2 = $row["TradeCheck_noteleponpemasok2"];
			$TradeCheck_alamatpemasok2 = $row["TradeCheck_alamatpemasok2"];
			$TradeCheck_lamahubunganpemasok2 = $row["TradeCheck_lamahubunganpemasok2"];
			$TradeCheck_jenisbarangpemasok2 = $row["TradeCheck_jenisbarangpemasok2"];
			$TradeCheck_ratapenjualanpemasok2 = $row["TradeCheck_ratapenjualanpemasok2"];
			$TradeCheck_termpembayaranpemasok2 = $row["TradeCheck_termpembayaranpemasok2"];
			$TradeCheck_karaktercadebpemasok2 = $row["TradeCheck_karaktercadebpemasok2"];
			$TradeCheck_infonegatifpemasok2 = $row["TradeCheck_infonegatifpemasok2"];
			$TradeCheck_ketepatanpemasok2 = $row["TradeCheck_ketepatanpemasok2"];
			$TradeCheck_tanggalpembeli1 = $row["TradeCheck_tanggalpembeli1"];
			$TradeCheck_namapemberiinfopembeli1 = $row["TradeCheck_namapemberiinfopembeli1"];
			$TradeCheck_namapembeli1 = $row["TradeCheck_namapembeli1"];
			$TradeCheck_noteleponpembeli1 = $row["TradeCheck_noteleponpembeli1"];
			$TradeCheck_alamatpembeli1 = $row["TradeCheck_alamatpembeli1"];
			$TradeCheck_lamahubunganpembeli1 = $row["TradeCheck_lamahubunganpembeli1"];
			$TradeCheck_jenisbarangpembeli1 = $row["TradeCheck_jenisbarangpembeli1"];
			$TradeCheck_ratapenjualanpembeli1 = $row["TradeCheck_ratapenjualanpembeli1"];
			$TradeCheck_termpembayaranpembeli1 = $row["TradeCheck_termpembayaranpembeli1"];
			$TradeCheck_karaktercadebpembeli1 = $row["TradeCheck_karaktercadebpembeli1"];
			$TradeCheck_infonegatifpembeli1 = $row["TradeCheck_infonegatifpembeli1"];
			$TradeCheck_tanggalpembeli2 = $row["TradeCheck_tanggalpembeli2"];
			$TradeCheck_namapemberiinfopembeli2 = $row["TradeCheck_namapemberiinfopembeli2"];
			$TradeCheck_namapembeli2 = $row["TradeCheck_namapembeli2"];
			$TradeCheck_noteleponpembeli2 = $row["TradeCheck_noteleponpembeli2"];
			$TradeCheck_alamatpembeli2 = $row["TradeCheck_alamatpembeli2"];
			$TradeCheck_lamahubunganpembeli2 = $row["TradeCheck_lamahubunganpembeli2"];
			$TradeCheck_jenisbarangpembeli2 = $row["TradeCheck_jenisbarangpembeli2"];
			$TradeCheck_ratapenjualanpembeli2 = $row["TradeCheck_ratapenjualanpembeli2"];
			$TradeCheck_termpembayaranpembeli2 = $row["TradeCheck_termpembayaranpembeli2"];
			$TradeCheck_karaktercadebpembeli2 = $row["TradeCheck_karaktercadebpembeli2"];
			$TradeCheck_infonegatifpembeli2 = $row["TradeCheck_infonegatifpembeli2"];
			$TradeCheck_tanggalkomunitas1 = $row["TradeCheck_tanggalkomunitas1"];
			$TradeCheck_namapemberiinfokomunitas1 = $row["TradeCheck_namapemberiinfokomunitas1"];
			$TradeCheck_namakomunitas1 = $row["TradeCheck_namakomunitas1"];
			$TradeCheck_noteleponkomunitas1 = $row["TradeCheck_noteleponkomunitas1"];
			$TradeCheck_alamatkomunitas1 = $row["TradeCheck_alamatkomunitas1"];
			$TradeCheck_lamahubungankomunitas1 = $row["TradeCheck_lamahubungankomunitas1"];
			$TradeCheck_jenisbarangkomunitas1 = $row["TradeCheck_jenisbarangkomunitas1"];
			$TradeCheck_ratapenjualankomunitas1 = $row["TradeCheck_ratapenjualankomunitas1"];
			$TradeCheck_termpembayarankomunitas1 = $row["TradeCheck_termpembayarankomunitas1"];
			$TradeCheck_karaktercadebkomunitas1 = $row["TradeCheck_karaktercadebkomunitas1"];
			$TradeCheck_infonegatifkomunitas1 = $row["TradeCheck_infonegatifkomunitas1"];
			$TradeCheck_ketepatankomunitas1 = $row["TradeCheck_ketepatankomunitas1"];
			$TradeCheck_tanggalkomunitas2 = $row["TradeCheck_tanggalkomunitas2"];
			$TradeCheck_namapemberiinfokomunitas2 = $row["TradeCheck_namapemberiinfokomunitas2"];
			$TradeCheck_namakomunitas2 = $row["TradeCheck_namakomunitas2"];
			$TradeCheck_noteleponkomunitas2 = $row["TradeCheck_noteleponkomunitas2"];
			$TradeCheck_alamatkomunitas2 = $row["TradeCheck_alamatkomunitas2"];
			$TradeCheck_lamahubungankomunitas2 = $row["TradeCheck_lamahubungankomunitas2"];
			$TradeCheck_jenisbarangkomunitas2 = $row["TradeCheck_jenisbarangkomunitas2"];
			$TradeCheck_ratapenjualankomunitas2 = $row["TradeCheck_ratapenjualankomunitas2"];
			$TradeCheck_termpembayarankomunitas2 = $row["TradeCheck_termpembayarankomunitas2"];
			$TradeCheck_karaktercadebkomunitas2 = $row["TradeCheck_karaktercadebkomunitas2"];
			$TradeCheck_infonegatifkomunitas2 = $row["TradeCheck_infonegatifkomunitas2"];
			$TradeCheck_ketepatankomunitas2 = $row["TradeCheck_ketepatankomunitas2"];
		}
	}

	//END TAHAP 9

	$counttab = "0";

	$tsql = "select count(*) as count from Tbl_InfoTab where custnomid = '$custnomid' AND InfoTab_Flow = 'LKCD'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$count = $row["count"];
		}
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD VIEW</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<form id="formprint" name="formprint" method="post">
<?
if($userwfid != "" && $userid != "")
{
?>
	<div style="float: right;margin-right: 190px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
	<br>
	<br>
<?
}
?>
<table width="950px" align="center" style="border:1px solid black;" class="preview">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">LAPORAN KUNJUNGAN SETEMPAT</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td >Tgl Kunjungan / Wawancara</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td>
</tr>
<tr>
	<td >Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td >No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<!--<tr id="TAHAP 3">
	<td colspan="2"><? include("./new_pasangan/pasangan_view.php"); ?></td>
</tr>-->
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<tr id="TAHAP 4">
	<td colspan="2"><? include("./survey/survey_v.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<?
	$custfullname = "";

	$tsqls = "select * from tbl_customermasterperson2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$custfullname = $rows["custfullname"];
		}
	}

	$tsqls = "select * from tbl_lkcdcallmemo where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$tanggal = $rows['tanggal'];
			$tanggal_call = $rows['tanggal_call'];
			$tanggal_call_sebelumnya = $rows['tanggal_call_sebelumnya'];
			$bentuk_call = $rows['bentuk_call'];
			$pejabat_melakukan_call = $rows['pejabat_melakukan_call'];
			$pejabat_di_call = $rows['pejabat_di_call'];
			$tujuan_call = $rows['tujuan_call'];
			$hasil = $rows['hasil'];
		}
	}

	if($tanggal === "" && $bentuk_call === "" && $hasil === "")
	{

	}
	else {
?>
<tr id="TAHAP 5">
	<td colspan="2">
		<table width="100%" align="center"  >
		<tr>
			<td colspan="2" style="text-align:center;font-weight:bold;">CALL MEMO</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="1" width="95%%" align="center" id="tblpreview" cellspacing="0" >
			<tr>
				<td colspan="4" width="66%">&nbsp;</td>
				<td colspan="2" width="34%">TANGGAL : <?php echo $tanggal;?></td>
			</tr>
			<tr>
				<td colspan="3" style="padding:5px;" width="50%">
					Nama Nasabah </br>
					<strong><?=$custfullname;?></strong>
				</td>
				<td colspan="3" style="padding:5px;" width="50%">
					<table border="0" width="90%">
					<tr>
						<td>Tanggal Call</td>
						<td>: <?php echo $tanggal_call;?></td>
					</tr>
					<tr>
						<td>Tanggal Call Sebelumnya</td>
						<td>: <?php echo $tanggal_call_sebelumnya;?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Bentuk Call :</strong> </br>
					<?=$bentuk_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang melakukan Call :</strong> </br>
					<?=$pejabat_melakukan_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang di Call :</strong> </br>
					<?=$pejabat_di_call;?>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Tujuan Call :</strong> </br>
					<?=$tujuan_call;?>
				</tr>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Hasil :</strong> </br>
					<?
						$hasil = str_replace("\n","</br>",$hasil);
					?>
					<?=$hasil;?>
				</tr>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	</td>
</tr>

<?
}
?>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 6">
	<td colspan="2" style="text-align:center;font-weight:bold;">MUTASI REKENING KORAN / TABUNGAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">

		<?
			$tsqls = "select * from Tbl_LKCDMutasiRekeningNamaBank where custnomid = '$custnomid' and flag = '1'";
			$as = sqlsrv_query($conn, $tsqls);

			if ( $as === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($as))
			{
				while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
				{
					$idx = $rows["idx"];
					$nama_bank = $rows["nama_bank"];
					$saldo_terakhir = $rows["saldo_terakhir"];
					$nomor_rekening = $rows["nomor_rekening"];
					$jenis_rekening = $rows["jenis_rekening"];
					$atas_nama = $rows["atas_nama"];
					$keterangan = $rows["keterangan"];

					$tsqld = "select * from param_bank where code = '$nama_bank'";
					$d = sqlsrv_query($conn, $tsqld);

					if ( $d === false)
					die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($d))
					{
						while($rowd = sqlsrv_fetch_array($d, SQLSRV_FETCH_ASSOC))
						{
							$att_bank = $rowd["attribute"];
						}
					}
					?>

					<table width="100%" align="center" style="border:0px solid black;">
					<tr>
						<td colspan="5" style="text-align:center;font-weight:bold;">MUTASI REKENING <?=$nama_bank;?></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3">
							<form id="formsaldo" name="formsaldo" method="post">
							<table width="100%" align="center" style="border:0px solid black;">
							<tr>
								<td width="20%"><strong>Nama Bank</strong></td>
								<td width="20%"> : <?=$nama_bank;?></td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%"><strong>Nomor Rekening</strong></td>
								<td width="20%"> : <?=$nomor_rekening;?></td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%"><strong>Jenis Rekening</strong></td>
								<td width="20%"> : <?=$jenis_rekening;?></td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%"><strong>Atas Nama</strong></td>
								<td width="20%"> : <?=$atas_nama;?></td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%"><strong>Keterangan</strong></td>
								<td width="20%"> : <?=$keterangan;?></td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							</table>
							</form>
						</td>
					</tr>
					<tr>
						<td  style="border:1px solid black;" align="center"><strong>NAMA BULAN</strong></td>
						<td  style="border:1px solid black;" align="center"><strong>SALDO AWAL (<? echo $currency; ?>)</strong></td>
						<td  style="border:1px solid black;" align="center"><strong>MUTASI DEBET (<? echo $currency; ?>)</strong></td>
						<td  style="border:1px solid black;" align="center"><strong>MUTASI KREDIT (<? echo $currency; ?>)</strong></td>
						<td  style="border:1px solid black;" align="center"><strong>SALDO AKHIR (<? echo $currency; ?>)</strong></td>
					</tr>

					<?
						$ct = 0;
						$seqMutasi = "";
						$MutasiRekening_namabulan = "";
						$MutasiRekening_mutasidebet = "";
						$MutasiRekening_mutasikredit = "";
						$MutasiRekening_saldoakhir = "";
						$Totalsaldoawal = 0;
						$Totaldebet = 0;
						$Totalkredit = 0;
						$Totalsaldoakhir = 0;
						$tempsaldoawal = 0;
						$tempdebet = 0;
						$tempkredit = 0;
						$tempsaldoakhir = 0;

						$tsql = "select * from Tbl_LKCDMutasiRekening where custnomid = '$custnomid' and MutasiRekening_flag = '$idx' order by MutasiRekening_namabulan";
						$a = sqlsrv_query($conn, $tsql);

						if ( $a === false)
						die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($a))
						{
							while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
							{
								$seqMutasi = $row["seq"];
								$MutasiRekening_namabulan = $row["MutasiRekening_namabulan"];
								$MutasiRekening_saldoawal = number_format($row["MutasiRekening_saldoawal"]);
								$MutasiRekening_mutasidebet = number_format($row["MutasiRekening_mutasidebet"]);
								$MutasiRekening_mutasikredit = number_format($row["MutasiRekening_mutasikredit"]);
								$MutasiRekening_saldoakhir = number_format($row["MutasiRekening_saldoakhir"]);
								
								$tempsaldoawal = str_replace(",","", $MutasiRekening_saldoawal);
								$Totalsaldoawal = $Totalsaldoawal + $tempsaldoawal;
								
								$tempdebet = str_replace(",","", $MutasiRekening_mutasidebet);
								$Totaldebet = $Totaldebet + $tempdebet;

								$tempkredit = str_replace(",","", $MutasiRekening_mutasikredit);
								$Totalkredit = $Totalkredit + $tempkredit;

								$tempsaldoakhir = str_replace(",","", $MutasiRekening_saldoakhir);
								$Totalsaldoakhir = $Totalsaldoakhir + $tempsaldoakhir;
								$ct++;
					?>
					<tr>
						<td  style="border:1px solid black;" align="center"><? echo $MutasiRekening_namabulan?></td>
						<td  style="border:1px solid black;" align="right"><? echo $MutasiRekening_saldoawal?></td>
						<td  style="border:1px solid black;" align="right"><? echo $MutasiRekening_mutasidebet?></td>
						<td  style="border:1px solid black;" align="right"><? echo $MutasiRekening_mutasikredit?></td>
						<td  style="border:1px solid black;" align="right"><? echo $MutasiRekening_saldoakhir?></td>
					</tr>

					<?
							}
						}
					?>

					<!--<tr>
						<td  style="border:1px solid black;" align="center"><strong>TOTAL</strong></td>
						<td  style="border:1px solid black;" align="center"><? echo number_format($Totaldebet)?></td>
						<td  style="border:1px solid black;" align="center"><? echo number_format($Totalkredit)?></td>
						<td  style="border:1px solid black;" align="center"><? echo number_format($Totalsaldoakhir)?></td>
					</tr>-->
					<tr>
						<td style="border:1px solid black;width:100px;" align="center"><strong>Rata - rata</strong></td>
						<td style="border:1px solid black;width:100px;" align="right"><? echo number_format($Totalsaldoawal/$ct,2)?></td>
						<td style="border:1px solid black;width:100px;" align="right"><? echo number_format($Totaldebet/$ct,2)?></td>
						<td style="border:1px solid black;width:100px;" align="right"><? echo number_format($Totalkredit/$ct,2)?></td>
						<td style="border:1px solid black;width:100px;" align="right"><? echo number_format($Totalsaldoakhir/$ct,2)?></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<!--<tr>
						<td  align="left"><strong>Saldo Akhir <?=$att_bank?></strong></td>
						<td  align="center"><? echo number_format($saldo_terakhir)?></td>
						<td  align="center">&nbsp;</td>
					</tr>-->

					</table>
					<br>
					<br>
					<?
				}
			}
		?>

	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<!--
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">REKAP PENJUALAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">

		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td  style="border:1px solid black;" align="center"><strong>NAMA BULAN</strong></td>
			<td  style="border:1px solid black;" align="center"><strong>PENJUALAN (<? echo $currency; ?>)</strong></td>
		</tr>
		<?
			$seqRekap = "";
			$RekapPenjualan_namabulan = "";
			$RekapPenjualan_penjualan = "";
			$Totalpenjualan = 0;
			$temppenjualan = 0;

			$tsql = "select * from Tbl_LKCDRekapPenjualan where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqRekap = $row["seq"];
					$RekapPenjualan_namabulan = $row["RekapPenjualan_namabulan"];
					$RekapPenjualan_penjualan = number_format($row["RekapPenjualan_penjualan"]);

					$temppenjualan = str_replace(",","", $RekapPenjualan_penjualan);
					$Totalpenjualan = $Totalpenjualan + $temppenjualan;


		?>
		<tr>
			<td  style="border:1px solid black;" align="center"><? echo $RekapPenjualan_namabulan?></td>
			<td  style="border:1px solid black;" align="center"><? echo $RekapPenjualan_penjualan?></td>

		</tr>

		<?
				}
			}
		?>

		<tr>
			<td  style="border:1px solid black;" align="center"><strong>TOTAL</strong></td>
			<td  style="border:1px solid black;" align="center"><? echo number_format($Totalpenjualan)?></td>
		</tr>
		</table>

	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
-->
<tr id="TAHAP 7">
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK LAIN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="800px">Apakah Debitur memiliki fasilitas pinjaman di Bank lain?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb1;?></td>
</tr>
<tr>
	<td colspan="2">
<table width="100%" align="center" <?if($InfoFasilitasPinjamanBankLain_cb1=="YA"){?> style="display:block;"<?}else{?> style="display:none;"<?}?>>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>A. FASILITAS PINJAMAN YANG SEDANG BERJALAN  <i>(tidak memperhitungkan fasilitas yang akan di take over)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">

		<tr>
			<td style="border:1px solid black;width:200px;" align="center"><strong>JENIS FASILITAS</strong></td>
			<td style="border:1px solid black;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td style="border:1px solid black;" align="center"><strong>OUTSTANDING (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>ANGSURAN (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>KOL</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Keterangan</strong></td>
		</tr>
				<?
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";

				?>

		<?



			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'PT'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];

				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}

		?>
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td style="border:1px solid black;" align="center"><? echo number_format($FasilitasPinjamanBankLain_outstanding)?></td>
			<td style="border:1px solid black;" align="center"><? echo number_format($FasilitasPinjamanBankLain_angsuran)?></td>
			<td style="border:1px solid black;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td style="border:1px solid black;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
		</tr>

		<?
				}
			}
		?>

		</table>


	</td>
</tr>
</table>
</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="800px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb2;?></td>
</tr>
<tr>
	<td colspan="2">
<table width="100%" align="center" <?if($InfoFasilitasPinjamanBankLain_cb2=="YA"){?> style="display:block;"<?}else{?> style="display:none;"<?}?>>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}


	?>

		<table width="150%" align="center" style="border:0px solid black;">
		<tr>
			<td style="width:200px" align="center">&nbsp;</td>
			<td style="border:1px solid black;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Jenis Fasilitas</strong></td>
		</tr>
		<tr id="1">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan s.d 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1;?>
			</td>
		</tr>
		<tr id="2">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan > 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2;?>
			</td>
		</tr>
		<tr id="3">
			<td align="center">Kolektibilitas 3-5 (NPL)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3;?>
			</td>
		</tr>
		<tr>
			&nbsp;<!--<td colspan="4" align="center"><input type="button" value="SAVE" style="background-color:blue;color:white;" onclick="nonA1()" /></td>-->
		</tr>
		</table>

	</td>
</tr>
</table>
</td>
</tr>
<tr>
	<td colspan="2"><strong>B. FASILITAS PINJAMAN YANG AKAN DI TAKE OVER (Jika ada)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">

		<tr>
			<td style="border:1px solid black;width:200px;" align="center"><strong>JENIS FASILITAS</strong></td>
			<td style="border:1px solid black;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td style="border:1px solid black;" align="center"><strong>OUTSTANDING (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>ANGSURAN (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>KOL</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Lama Tunggakan</strong></td>
		</tr>
				<?
					$FasilitasPinjamanBankLain_seq2 = "";
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";

				?>

		<?



			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'TO'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq2 = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];

				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}

		?>
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankLain_outstanding?></td>
			<td style="border:1px solid black;" align="center"><? echo number_format($FasilitasPinjamanBankLain_angsuran)?></td>
			<td style="border:1px solid black;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td style="border:1px solid black;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
		</tr>

		<?
				}
			}
		?>
		</table>

	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb3;?></td>
</tr>
<tr>
	<td colspan="2">
<table width="100%" align="center" <?if($InfoFasilitasPinjamanBankLain_cb3=="YA"){?> style="display:block;"<?}else{?> style="display:none;"<?}?>>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}


	?>

		<table width="150%" align="center" style="border:0px solid black;">
		<tr>
			<td style="width:200px" align="center">&nbsp;</td>
			<td style="border:1px solid black;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Jenis Fasilitas</strong></td>
		</tr>
		<tr id="1">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan s.d 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1;?>
			</td>
		</tr>
		<tr id="2">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan > 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2;?>
			</td>
		</tr>
		<tr id="3">
			<td align="center">Kolektibilitas 3-5 (NPL)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3;?>
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>
</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr id="TAHAP 8">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK SENDIRI</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
		<table width="100%" align="center" style="border:0px solid black;">

		<tr>
			<td style="border:1px solid black;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Plafond Awal (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Outstanding (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Angsuran (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Tenor (bln)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Rate</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Provisi (<? echo $currency; ?>)</strong></td>
			<td style="border:1px solid black;" align="center"><strong>KOL</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Lama Tunggakan</strong></td>
		</tr>
				<?
					$FasilitasPinjamanBankMega_jenisfasilitas = "";
					$FasilitasPinjamanBankMega_plafondawal = "";
					$FasilitasPinjamanBankMega_outstanding = "";
					$FasilitasPinjamanBankMega_angsuran = "";
					$FasilitasPinjamanBankMega_tenor = "";
					$FasilitasPinjamanBankMega_rate = "";
					$FasilitasPinjamanBankMega_provisi = "";
					$FasilitasPinjamanBankMega_kol = "";
					$FasilitasPinjamanBankMega_dpd = "";
				?>



		<?

			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankMega where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankMega_seq = $row["FasilitasPinjamanBankMega_seq"];
					$FasilitasPinjamanBankMega_jenisfasilitas = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
					$FasilitasPinjamanBankMega_plafondawal = $row["FasilitasPinjamanBankMega_plafondawal"];
					$FasilitasPinjamanBankMega_outstanding = $row["FasilitasPinjamanBankMega_outstanding"];
					$FasilitasPinjamanBankMega_angsuran = $row["FasilitasPinjamanBankMega_angsuran"];
					$FasilitasPinjamanBankMega_tenor = $row["FasilitasPinjamanBankMega_tenor"];
					$FasilitasPinjamanBankMega_rate = $row["FasilitasPinjamanBankMega_rate"];
					$FasilitasPinjamanBankMega_provisi = $row["FasilitasPinjamanBankMega_provisi"];
					$FasilitasPinjamanBankMega_kol = $row["FasilitasPinjamanBankMega_kol"];
					$FasilitasPinjamanBankMega_dpd = $row["FasilitasPinjamanBankMega_dpd"];


		?>
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankMega_jenisfasilitas;?></td>
			<td style="border:1px solid black;" align="center"><? echo  number_format($FasilitasPinjamanBankMega_plafondawal)?></td>
			<td style="border:1px solid black;" align="center"><? echo  number_format($FasilitasPinjamanBankMega_outstanding)?></td>
			<td style="border:1px solid black;" align="center"><? echo  number_format($FasilitasPinjamanBankMega_angsuran)?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankMega_tenor;?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankMega_rate;?></td>
			<td style="border:1px solid black;" align="center"><? echo  number_format($FasilitasPinjamanBankMega_provisi)?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankMega_kol;?></td>
			<td style="border:1px solid black;" align="center"><? echo $FasilitasPinjamanBankMega_dpd;?></td>
		</tr>

		<?
				}
			}
		?>
		</table>

	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td width="35%">No. CIF</td>
	<td width="65%"><div style="border:1px solid black;width:250px;">&nbsp;<? echo $InfoFasilitasPinjamanBankMega_nocif;?></div></td>
</tr>
<tr>
	<td width="35%">Nasabah sejak</td>
	<td width="65%"><div style="border:1px solid black;width:250px;">&nbsp;<? echo $InfoFasilitasPinjamanBankMega_nasabahsejak;?></div></td>
</tr>
<tr>
	<td width="35%">Data pertanggal</td>
	<td width="65%"><div style="border:1px solid black;width:250px;">&nbsp;<? echo $InfoFasilitasPinjamanBankMega_datapertanggal;?></div></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1">
		<div style="border:1px solid black;width:50px;"><? echo $InfoFasilitasPinjamanBankMega_cb1;?></div>
	</td>
</tr>
<?	
	if($InfoFasilitasPinjamanBankMega_cb1=="YA"){
?>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = "";

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '1' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '2' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}

		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '3' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}


	?>

		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td style="width:200px" align="center">&nbsp;</td>
			<td style="border:1px solid black;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td style="border:1px solid black;" align="center"><strong>Jenis Fasilitas</strong></td>
		</tr>
		<tr id="1">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan s.d 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali1;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1;?>
			</td>
		</tr>
		<tr id="2">
			<td align="center">Kolektibilitas 2 (Lama Tunggakan > 30 hari)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali2;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2;?>
			</td>
		</tr>
		<tr id="3">
			<td align="center">Kolektibilitas 3-5 (NPL)</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali3;?>
			</td>
			<td style="border:1px solid black;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3;?>
			</td>
		</tr>
		</table>

	</td>
</tr>
<?
	}
?>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="TAHAP 1">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td colspan="7" align="center">
				<strong>IDENTIFIKASI FASILITAS YANG DIAJUKAN</strong>
			</td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td style="border:1px solid black;" align="center">Jenis Fasilitas</td>
			<td style="border:1px solid black;" align="center">Tujuan Pengajuan Kredit</td>
			<td style="border:1px solid black;" align="center">Plafond  (<? echo $currency; ?>)</td>
			<td style="border:1px solid black;" align="center">Jangka waktu (Bulan)</td>
			<td style="border:1px solid black;" align="center">Suku bunga yang diberikan (%)</td>
			<td style="border:1px solid black;" align="center">Suku bunga minimal</td>
			<td style="border:1px solid black;" align="center">Keterangan</td>
		</tr>

		<?
			$jenisfasilitas = "";
			$tujuanpengajuankredit = "";
			$plafond = 0;
			$jangkawaktu = "";
			$seq = "";
			$bungadiberikan = "";


			$tsql = "select custcredittype, custcreditneed, custcreditplafond, custcreditlong, custfacseq, sukubungayangdiberikan, jenisbunga from tbl_CustomerFacility where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$jenisfasilitas = $row["custcredittype"];
					$tujuanpengajuankredit = $row["custcreditneed"];
					$plafond = number_format($row["custcreditplafond"]);
					$jangkawaktu = $row["custcreditlong"];
					$seq = $row["custfacseq"];
					$bungadiberikan = $row["sukubungayangdiberikan"];
					
					if ($row["jenisbunga"] == "1")
					{
					   $jenisbunga = "Flat";
					}
					if ($row["jenisbunga"] == "2")
					{
					   $jenisbunga = "Efektif";
					}
					if ($row["jenisbunga"] == "3")
					{
					   $jenisbunga = "Anuitas";
					}
					if ($row["jenisbunga"] == "4")
					{
					   $jenisbunga = "Anuitas";
					}

					$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitas'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$jenisfasilitas = $rowproduk["produk_type_description"];
						}
					}

					$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanpengajuankredit'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$tujuanpengajuankredit = $rowproduk["credit_need_name"];
						}
					}



		?>

		<tr>
			<td style="border:1px solid black;" align="center"><? echo $jenisfasilitas;?></td>
			<td style="border:1px solid black;" align="center"><? echo $tujuanpengajuankredit;?></td>
			<td style="border:1px solid black;" align="center"><? echo $plafond;?></td>
			<td style="border:1px solid black;" align="center"><? echo $jangkawaktu;?></td>
			<td style="border:1px solid black;" align="center">&nbsp;<? echo $bungadiberikan;?> </td>
			<td style="border:1px solid black;" align="center"><? echo $bungapersen; ?></td>
			<td style="border:1px solid black;" align="center"><?=$jenisbunga;?>
			<input type="hidden" id="<? echo 'seq'.$seq;?>" name="seq"  value='<? echo $seq; ?>'></td>
		</tr>

		<?
				}
			}
			sqlsrv_free_stmt( $a );
		?>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<?
/*require_once ("../../lib/open_conLISAPR.php");
?>
<tr id="TAHAP 8.5"> <!-- APPRAISAL VIEW-->
	<!--<td colspan="2"><? //include("../Appraisal/viewappraisalresponse_v3.php"); ?></td>-->
	<td colspan="2">
	<?php
		$strsql = "SELECT * FROM Tbl_Cust_MasterCol WHERE ap_lisregno = '$custnomid' and group_col = 'N' and flaginsert = '1' and flagdelete = '0'";
		$sqlcon = sqlsrv_query($connapr, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				$theid = $rows['col_id'];
				$appjenis = $rows['cust_jeniscol'];
				if($appjenis == "V01")
				{
					$strsqlv01="SELECT * FROM appraisal_vhc WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$v_type = $rowsv01['_type'];
							//echo $v_type;
							$v_merk = $rowsv01['_merk'];
							$v_model = $rowsv01['_model'];
							$v_jns = $rowsv01['_jns_kendaraan'];
							$v_thnpem = $rowsv01['_thnpembuatan'];
							$v_silinder_isi = $rowsv01['_silinder_isi'];
							$v_silinder_warna = $rowsv01['_silinder_wrn'];
							$v_norangka = $rowsv01['_norangka'];
							$v_nomesin = $rowsv01['_nomesin'];
							$v_bpkb_tgl = $rowsv01['_bpkb_tgl'];
							$v_stnk_exp = $rowsv01['_stnk_exp'];
							$v_faktur_tgl = $rowsv01['_faktur_tgl'];
							$v_bahanbakar = $rowsv01['_bahanbakar'];
							$v_bpkb_nama = $rowsv01['_bpkb_nama'];
							$v_bpkb_addr1 = $rowsv01['_bpkb_addr1'];
							$v_bpkb_addr2 = $rowsv01['_bpkb_addr2'];
							$v_bpkb_addr3 = $rowsv01['_bpkb_addr3'];
							$v_perlengkapan = $rowsv01['_perlengkapan'];
							$v_notes = $rowsv01['_notes'];
							$v_opini = $rowsv01['_opini'];
						}
					}

					$strsqlv01="SELECT * FROM appraisal_vhc_value WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$value_kend = $rowsv01['_nilai_kendaraan'];
							//echo $v_type;
							$value_savety = $rowsv01['_safety_margin'];
							$value_likuidasi = $rowsv01['_nilai_likuidasi'];
							$value_opini = $rowsv01['_opini'];
							$value_jml = $rowsv01['jml'];
						}
					}

					?>
						<table>
						<tr>
							<td style="width:50%" valign="top" rowspan="2">
								<table class="tbl100">
									<tr>
										<td colspan="2"><h4>DATA KENDARAAN <?=$theid?></h4></td>
									</tr>
									<tr>
										<td style="width:300px;">Type Kendaraan</td>
										<td  style="width:500px;">
											: <?php echo $v_type;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Merk Kendaraan</td>
										<td  style="width:500px;">
											: <?php echo $v_merk;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Model Kendaraan</td>
										<td  style="width:500px;">
											: <?php echo $v_model;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Jenis Kendaraan</td>
										<td  style="width:500px;">
											: <?php echo $v_jns;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Tahun Pembuatan</td>
										<td  style="width:500px;">
											: <?php echo $v_thnpem;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Isi Silinder</td>
										<td  style="width:500px;">
											: <?php echo $v_silinder_isi;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Warna Silinder</td>
										<td  style="width:500px;">
											: <?php echo $v_silinder_warna;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Nomer Rangka</td>
										<td  style="width:500px;">
											: <?php echo $v_norangka;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Nomer Mesin</td>
										<td  style="width:500px;">
											: <?php echo $v_nomesin;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Tanggal BPKB</td>
										<td  style="width:500px;">
											: <?php echo $v_bpkb_tgl;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">STNK Expired</td>
										<td  style="width:500px;">
											: <?php echo $v_stnk_exp;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Tanggal Faktur</td>
										<td  style="width:500px;">
											: <?php echo $v_faktur_tgl;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Bahan Bakar</td>
										<td  style="width:500px;">
											: <?php echo $v_bahanbakar;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Nama BPKB</td>
										<td  style="width:500px;">
											: <?php echo $v_bpkb_nama;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Alamat BPKB</td>
										<td  style="width:500px;">
											: <?php echo $v_bpkb_addr1;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Perlengkapan</td>
										<td  style="width:500px;">
											: <?php echo $v_perlengkapan;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Notes</td>
										<td  style="width:500px;">
											: <?php echo $v_notes;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Opini</td>
										<td  style="width:500px;">
											: <?php echo $v_opini;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
								</table>
							</td>
							<td style="width:50%" valign="top">
								<div>
									<table class="tbl100">
										<tr>
											<td colspan="2"><h4>NILAI KENDARAAN</h4></td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Kendaraan</td>
											<td style="width:250px;">
												: <?echo number_format($value_kend);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Safety Margin</td>
											<td style="width:250px;">
												: <?echo $value_savety;?> %
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Likuidasi</td>
											<td style="width:250px;">
												: <?echo number_format($value_likuidasi);?>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												Opini :
											</td>
										</tr>
										<tr>
											<td colspan="2" style="padding-left: 40px;">
												<?echo $value_opini;?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						</table>
					<?
				}
				else if($appjenis=="BA1" || $appjenis=="RUK" || $appjenis=="KI2")
				{
					$strsqlv01="SELECT * FROM appraisal_tnb WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$v_type = $rowsv01['_type_jaminan'];
							//echo $v_type;
							$v_lokasi = $rowsv01['_lokasi_rumah'];
							$v_jmllnt = $rowsv01['_jumlah_lantai'];
							$v_luas_bang = $rowsv01['_luas_bangunan'];
							$v_panj_bang = $rowsv01['_panjang_bangunan'];
							$v_leb_bang = $rowsv01['_lebar_bangunan'];
							$v_arah_bang = $rowsv01['_arah_bangunan'];
							$v_umur_bang = $rowsv01['_umur_bangunan'];
							$v_thn_bang = $rowsv01['_tahun_dibangun'];
							$v_thn_renov = $rowsv01['_tahun_renovasi'];
							$v_luas_tnh = $rowsv01['_luas_tanah'];
							$v_pan_tnh = $rowsv01['_panjang_tanah'];
							$v_leb_tnh = $rowsv01['_lebar_tanah'];
							$v_utara = $rowsv01['_sisi_utara'];
							$v_timur = $rowsv01['_sisi_timur'];
							$v_selatan = $rowsv01['_sisi_selatan'];
							$v_barat = $rowsv01['_sisi_barat'];
							$v_lat = $rowsv01['_latitude'];
							$v_long = $rowsv01['_longitude'];
							$v_notes = $rowsv01['_notes'];
							$v_opini = $rowsv01['_opini'];
						}
					}

					$strsqlv01="SELECT * FROM appraisal_tnb_value WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$value_tnhpm = $rowsv01['_nilai_tanah_perm2'];
							//echo $v_type;
							$value_tnht = $rowsv01['_nilai_tanah_total'];
							$value_lbf = $rowsv01['_luas_bang_fisik'];
							$value_bfpm = $rowsv01['_nilai_bang_fisik_perm2'];
							$value_bft = $rowsv01['_nilai_bang_fisik_total'];
							$value_bimb = $rowsv01['_luas_bang_imb'];
							$value_bimbpm = $rowsv01['_nilai_bang_imb_perm2'];
							$value_bimbt = $rowsv01['_nilai_bang_imb_total'];
							$value_tf = $rowsv01['_nilai_total_fisik'];
							$value_ti = $rowsv01['_nilai_total_imb'];
							$value_sm = $rowsv01['_safety_margin'];
							$value_lf = $rowsv01['_nilai_likuidasi_fisik'];
							$value_li = $rowsv01['_nilai_likuidasi_imb'];
							$value_bfp = $rowsv01['_nilai_bang_fisik_percent'];
							$value_bip = $rowsv01['_nilai_bang_imb_percent'];
							$value_smf = $rowsv01['_safety_margin_fisik'];
							$value_op = $rowsv01['_opini'];
						}
					}

					?>
						<table>
						<tr>
							<td style="width:50%" valign="top" rowspan="2">
								<table class="tbl100">
									<tr>
										<td colspan="2"><h4>DATA TANAH DAN BANGUNAN <?=$theid?></h4></td>
									</tr>
									<tr>
										<td style="width:300px;">Type Jaminan</td>
										<td  style="width:500px;">
											: <?php echo $v_type;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Lokasi Rumah</td>
										<td  style="width:500px;">
											: <?php echo $v_lokasi;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Jumlah Lantai</td>
										<td  style="width:500px;">
											: <?php echo $v_jmllnt;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Luas Bangunan</td>
										<td  style="width:500px;">
											: <?php echo $v_luas_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Panjang Bangunan</td>
										<td  style="width:500px;">
											: <?php echo $v_panj_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Lebar Bangunan</td>
										<td  style="width:500px;">
											: <?php echo $v_leb_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Arah Bangunan</td>
										<td  style="width:500px;">
											: <?php echo $v_arah_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Umur Bangunan</td>
										<td  style="width:500px;">
											: <?php echo $v_umur_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Tahun Dibangun</td>
										<td  style="width:500px;">
											: <?php echo $v_thn_bang;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Tahun Renovasi</td>
										<td  style="width:500px;">
											: <?php echo $v_thn_renov;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Luas Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_luas_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Panjang Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_pan_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Lebar Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_leb_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Utara</td>
										<td  style="width:500px;">
											: <?php echo $v_utara;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Timur</td>
										<td  style="width:500px;">
											: <?php echo $v_timur;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Selatan</td>
										<td  style="width:500px;">
											: <?php echo $v_selatan;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Barat</td>
										<td  style="width:500px;">
											: <?php echo $v_barat;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Latitude</td>
										<td  style="width:500px;">
											: <?php echo $v_lat;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Longitude</td>
										<td  style="width:500px;">
											: <?php echo $v_long;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Notes</td>
										<td  style="width:500px;">
											: <?php echo $v_notes;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Opini</td>
										<td  style="width:500px;">
											: <?php echo $v_opini;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
								</table>
							</td>
							<td style="width:50%" valign="top">
								<div>
									<table class="tbl100">
										<tr>
											<td colspan="2"><h4>NILAI TANAH DAN BANGUNAN</h4></td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Tanah Per m<sup>2</sup></td>
											<td style="width:250px;">
												: <?echo number_format($value_tnhpm);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Tanah Total</td>
											<td style="width:250px;">
												: <?echo number_format($value_tnht);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Luas Bangunan Fisik</td>
											<td style="width:250px;">
												: <?echo $value_lbf;?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan Fisik Per m<sup>2<sup></td>
											<td style="width:250px;">
												: <?echo number_format($value_bfpm);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan Fisik Total</td>
											<td style="width:250px;">
												: <?echo number_format($value_bft);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Luas Bangunan IMB</td>
											<td style="width:250px;">
												: <?echo $value_bimb;?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan IMB Per m<sup>2<sup></td>
											<td style="width:250px;">
												: <?echo number_format($value_bimbpm);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan IMB Total</td>
											<td style="width:250px;">
												: <?echo number_format($value_bimbt);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Total Fisik</td>
											<td style="width:250px;">
												: <?echo number_format($value_tf);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Safety Margin Fisik</td>
											<td style="width:250px;">
												: <?echo $value_smf;?> %
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Likuidasi Fisik</td>
											<td style="width:250px;">
												: <?echo number_format($value_lf);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Total IMB</td>
											<td style="width:250px;">
												: <?echo number_format($value_ti);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Safety Margin IMB</td>
											<td style="width:250px;">
												: <?echo $value_sm;?> %
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Likuidasi IMB</td>
											<td style="width:250px;">
												: <?echo number_format($value_li);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan Fisik Percent</td>
											<td style="width:250px;">
												: <?echo $value_bfp;?> %
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Bangunan IMB Percent</td>
											<td style="width:250px;">
												: <?echo $value_bip;?> %
											</td>
										</tr>
										<tr>
											<td colspan="4">
												Opini :
											</td>
										</tr>
										<tr>
											<td colspan="2" style="padding-left: 40px;">
												<?echo $value_op;?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						</table>
					<?
				}
				else if($appjenis=="TAN")
				{
					$strsqlv01="SELECT * FROM appraisal_lnd WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$v_luas_tnh = $rowsv01['_luas_tanah'];
							$v_pan_tnh = $rowsv01['_panjang_tanah'];
							$v_leb_tnh = $rowsv01['_lebar_tanah'];
							$v_utara = $rowsv01['_sisi_utara'];
							$v_timur = $rowsv01['_sisi_timur'];
							$v_selatan = $rowsv01['_sisi_selatan'];
							$v_barat = $rowsv01['_sisi_barat'];
							$v_lat = $rowsv01['_latitude'];
							$v_long = $rowsv01['_longitude'];
							$v_notes = $rowsv01['_notes'];
							$v_opini = $rowsv01['_opini'];
						}
					}

					$strsqlv01="SELECT * FROM appraisal_lnd_value WHERE _collateral_id = '$theid'";
					//echo $strsqlv01;
					$sqlconv01 = sqlsrv_query($connapr, $strsqlv01);
					if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($sqlconv01))
					{
						if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
						{
							$value_tnhpm = $rowsv01['_nilai_tanah_perm2'];
							//echo $v_type;
							$value_tnht = $rowsv01['_nilai_tanah_total'];
							$value_sm = $rowsv01['_safety_margin'];
							$value_lf = $rowsv01['_nilai_likuidasi'];
							$value_op = $rowsv01['_opini'];
						}
					}

					?>

						<table>
						<tr>
							<td style="width:50%" valign="top" rowspan="2">
								<table class="tbl100">
									<tr>
										<td colspan="2"><h4>DATA TANAH <?=$theid?></h4></td>
									</tr>
									<tr>
										<td style="width:300px;">Luas Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_luas_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Panjang Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_pan_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Lebar Tanah</td>
										<td  style="width:500px;">
											: <?php echo $v_leb_tnh;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Utara</td>
										<td  style="width:500px;">
											: <?php echo $v_utara;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Timur</td>
										<td  style="width:500px;">
											: <?php echo $v_timur;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Selatan</td>
										<td  style="width:500px;">
											: <?php echo $v_selatan;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Sisi Barat</td>
										<td  style="width:500px;">
											: <?php echo $v_barat;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Latitude</td>
										<td  style="width:500px;">
											: <?php echo $v_lat;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Longitude</td>
										<td  style="width:500px;">
											: <?php echo $v_long;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Notes</td>
										<td  style="width:500px;">
											: <?php echo $v_notes;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">Opini</td>
										<td  style="width:500px;">
											: <?php echo $v_opini;?>
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
								</table>
							</td>
							<td style="width:50%" valign="top">
								<div>
									<table class="tbl100">
										<tr>
											<td colspan="2"><h4>NILAI TANAH</h4></td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Tanah Per m<sup>2</sup></td>
											<td style="width:250px;">
												: <?echo number_format($value_tnhpm);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Tanah Total</td>
											<td style="width:250px;">
												: <?echo number_format($value_tnht);?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Safety Margin</td>
											<td style="width:250px;">
												: <?echo $value_sm;?> %
											</td>
										</tr>
										<tr>
											<td style="width:180px;">Nilai Likuidasi</td>
											<td style="width:250px;">
												: <?echo number_format($value_lf);?>
											</td>
										</tr>
										<tr>
											<td colspan="4">
												Opini :
											</td>
										</tr>
										<tr>
											<td colspan="2" style="padding-left:40px;">
												<?echo $value_op;?>
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
												<input type="button" id="btnsave" name="btnsave" value="Save" class="buttonsaveflow" onClick="savelnd('<?php echo $theid?>');"/>
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
	<?
					}else{
	?>
						<tr>
							<td style="width:50%" valign="top" rowspan="2">
								<table class="tbl100">
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
									<tr>
										<td style="width:300px;">&nbsp;</td>
										<td  style="width:500px;">
											&nbsp;
										</td>
									</tr>
								</table>
							</td>
							<td style="width:50%" valign="top">
								<div>
									<table class="tbl100">
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
										<tr>
											<td style="width:180px;">&nbsp;</td>
											<td style="width:250px;">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						</table>

					<?
				}

	?>

	<?php
			}
		}
	?>
	</td>
</tr>*/?>
<tr id="TAHAP 10">
	<td colspan="2"><? include("./analisa_kebutuhan_kredit/include_for_lkcd.php");
	include("./analisa_kebutuhan_kredit/view_data.php"); ?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<tr id="TAHAP 10">
	<td colspan="2"><? include("./laporan_verifikasi/laporan_verifikasi_view_lkcd.php"); ?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<?
	$vs1 = "0";
	$desc1="";
	$tsql = "select * from LAPORAN_VERIFIKASI_DATA where custnomid = '$custnomid' AND user_verifikasi = '$userid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usr1 = $row['user_verifikasi'];
			$vs1 = "1";
			
		}
	}
	
	
	
	$vs2 = "0";
	$desc2 = "";
	$tsql = "select * from LAPORAN_VERIFIKASI_DATA where custnomid = '$custnomid' AND user_verifikasi_dua = '$userid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usr2 = $row['user_verifikasi_dua'];
			$vs2 = "1";
			
		}
	}
	
	$tsql = "select * from LAPORAN_VERIFIKASI_DATA where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usr2 = $row['user_verifikasi_dua'];
			$usr1 = $row['user_verifikasi'];
			
		}
	}
	
	$tsql = "select user_name from Tbl_SE_User where user_id = '$usr1'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usrname1 = $row['user_name'];
		}
	}
	
	$tsql = "select user_name from Tbl_SE_User where user_id = '$usr2'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usrname2 = $row['user_name'];
		}
	}
	
	$hidden=0;
	$tsql = "select * from LKS_SIGN where custnomid = '$custnomid' AND userid = '$usr1'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$desc1 = $row['description'];
			$hidden++;
		}
	}
	
	$tsql = "select * from LKS_SIGN where custnomid = '$custnomid' AND userid = '$usr2'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$desc2 = $row['description'];
			$hidden++;
		}
	}

	
?>

<tr>
	<td colspan="2">
	<form name="frmsign" id="frmsign" action="do_sign.php">
	<table width="100%" align="center">
	<tr align="center">
		<td>
		Tanda Tangan 1
		</td>
		<td>
		Tanda Tangan 2
		</td>
	</tr>
	<tr align="center">
		<td>
		<?=$usrname1;?>
		</td>
		<td>
		<?=$usrname2;?>
		</td>
	</tr>
	<tr align="center">
		<td>
		Deskripsi
		</td>
		<td>
		Deskripsi
		</td>
	</tr>
	<tr align="center">
		<td>
		<input type="text" id="desc1" name="desc1" value="<?=$desc1;?>" style="width:250px;" <?if($desc1!=""){?> readonly <?}?>/>
		</td>
		<td>
		<input type="text" id="desc2" name="desc2" value="<?=$desc2;?>" style="width:250px;" <?if($desc2!=""){?> readonly <?}?>/>
		</td>
	</tr>
	<tr align="center">
		<td>
		<?if($vs1=="1"){if($desc1==""){?>
		<input type="submit" id="sign1" name="sign1" value="SIGN" class="blue"/>&nbsp;
		<?}}?>
		</td>
		<td>
		<?if($vs2=="1"){if($desc2==""){?>
		<input type="submit" id="sign2" name="sign2" value="SIGN" class="blue"/>&nbsp;
		<?}}?>
		</td>
	</tr>
	<tr align="center">
		<td>
		<?if($desc1!=""){?>
		<span style="color:red;"><i>already signed&nbsp; </i></span>
		<?}?>
		</td>
		<td>
		<?if($desc2!=""){?>
		<span style="color:red;"><i>already signed&nbsp; </i></span>
		<?}?>
		</td>
	</tr>
	</table>
	<?require ("../../requirepage/hiddenfield.php");?>
	</form>
	</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>

</table>
</form>
<div>&nbsp;</div>
<?
	$tsql = "select * from Tbl_FLKCD where TXN_ID = '$custnomid' AND TXN_ACTION = 'A'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{

	}
	else
	{
		if($userwfid!="")
		{
			if($count == "8" && $userid != "")
			{
		?>
			<form name="frm" id="frm">
			<div style="text-align:center; border:0px solid black;">
				<?if($hidden==2){require ("../../requirepage/btnview.php");}
				require ("../../requirepage/hiddenfield.php");?>
			</div>
			</form>
		<?
			}
			else if($count != "8" && $userid != "")
			{
				echo '<div align="center" style="color:red;font-size:20px"><i>Harap semua flow diisi</i></div>';
			}
		}
	}

	ECHO "<div style=\"border:0px solid black;margin-bottom:100px;\">";
	require("../../requirepage/btnprint.php");
	echo "</div>";
?>
</body>
</html>
