<?php
include ("../new_lis/lib/formatError.php");
require ("../new_lis/lib/open_con.php");
require ("../new_lis/requirepage/parameter.php");
require ("../new_lis/requirepage/security.php");

ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");

if($userpermission == "I")
{
    $button_show = "Input";
}
else if ($userpermission == "C")
{
    $button_show = "Check";
}
else if ($userpermission == "A")
{
    $button_show = "Approve";
}

if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	//header("location:restricted.php");
}

$tsql = "select count(*) as ct from tr_templategroup where _flow = '$userwfid'";
$a = sqlsrv_query($conn, $tsql);
if ( $a === false)
die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($a))
{
	while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
	{
		$numrows = $row["ct"];
	}
}

$rowsperpage = 1;
// find out total pages
$totalpages = ceil($numrows / $rowsperpage);



// get the current page or set a default
if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
   // cast var as int
   $currentpage = (int) $_GET['currentpage'];
} else {
   // default page num
   $currentpage = 1;
} // end if

// if current page is greater than total pages...
if ($currentpage > $totalpages) {
   // set current page to last page
   $currentpage = $totalpages;
} // end if
// if current page is less than first page...
if ($currentpage < 1) {
   // set current page to first page
   $currentpage = 1;
} // end if

// the offset of the list, based on current page 
$offset = ($currentpage - 1) * $rowsperpage;
$untilpage = $offset + $rowsperpage;

$tsql = "SELECT * FROM ( 
SELECT *, ROW_NUMBER() OVER (ORDER BY _seq) as row FROM tr_templategroup where _flow = '$userwfid'
 ) a WHERE row > $offset and row <= $untilpage";
$a = sqlsrv_query($conn, $tsql);
if ( $a === false)
die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($a))
{
	while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
	{
		//$numrows = $row["ct"];
		
		$tid = $row["_template_id"];
		
		//echo $list['_template_id'] . " : " . $list['_seq'] . "<br />";
	}
}
/******  build the pagination links ******/
// range of num links to show
$range = 3;
?>
<div style="width:100%;text-align:center;">
<?
// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <div style='background-color:blue;width:50px;display:inline-block;'><a style='color:white;text-decoration:none;font-weight:bold;font-size:25px;' href='{$_SERVER['PHP_SELF']}?currentpage=1&userwfid=$userwfid&custnomid=$custnomid&userpermission=$userpermission'><< </a></div>";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <div style='background-color:blue;width:50px;display:inline-block;'><a style='color:white;text-decoration:none;font-weight:bold;font-size:25px;' href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage&userwfid=$userwfid&custnomid=$custnomid&userpermission=$userpermission'><</a></div> ";
} // end if 

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " <div style='background-color:blue;width:45px;display:inline-block;'> <span style='color:yellow;text-decoration:none;font-weight:bold;font-size:25px;'>[<b>$x</b>]</span> </div>";
      // if not current page...
      } else {
         // make it a link
         echo " <div style='background-color:blue;width:30px;display:inline-block;'> <a style='color:white;text-decoration:none;font-weight:bold;font-size:25px;' href='{$_SERVER['PHP_SELF']}?currentpage=$x&userwfid=$userwfid&custnomid=$custnomid&userpermission=$userpermission'>$x</a></div> ";
      } // end else
   } // end if 
} // end for

// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " <div style='background-color:blue;width:30px;display:inline-block;'> <a style='color:white;text-decoration:none;font-weight:bold;font-size:25px;' href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage&userwfid=$userwfid&custnomid=$custnomid&userpermission=$userpermission'>></a> </div>";
   // echo forward link for lastpage
   echo " <div style='background-color:blue;width:30px;display:inline-block;'> <a style='color:white;text-decoration:none;font-weight:bold;font-size:25px;' href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages&userwfid=$userwfid&custnomid=$custnomid&userpermission=$userpermission'>>></a> </div>";
} // end if
/****** end build pagination links ******/

$a = $tid;
$b = $custnomid;

?>
</div>

<div style="width:100%;text-align:center;" align="center">

<div style="width:1000px;text-align:center;margin:auto;" align="center">

<? //require("../../templateform/viewtemplate.php"); ?>

<? echo '</br><iframe src="./viewtemplate.php?a='.$a.'&b='.$b.'" height="100%" width="100%"></iframe>';?>


<form method="post" action="process_no_form.php">
	<?
	require ("../new_lis/requirepage/hiddenfield.php");
	?>
	</br>
	<input type="submit" id="btnsave" name="btnsave" value="<?=$button_show;?>" class="buttonsaveflow" />
</form>
</div>


</div>
<?
//require ("../lib/close_con.php");
?>

