<?php

function call_cif_service($info = array())
{
	$url = "http://172.17.34.58:8181/elos/cari/";    

	$arr_content = array(
		'user'=>"elos",
		'pass'=>"IsysElo5",
		'nama'=>$info['name']
	);

	$arr_header = array(
		"Content-type: application/json",
		"Authorization: Basic dXNlcjAxOlBhJCRXb3JEIQ==",
	);

	$content = json_encode($arr_content); 
	$header = $arr_header;

	$curl = curl_init($url);

	curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

	$response = curl_exec($curl);

	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	//echo "STS : " . $status . "<br/>";

	if ( $status != 200 ) {
	    die("ERROR : call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
	}

	curl_close($curl);

	//$response = json_encode($response, true);

	$response = explode("\r\n", $response);
	$data = json_decode($response[6],true);

	//echo "<pre>";
	//print_r($data);
	//echo "</pre>";

	return $data;	
}

?>