
CREATE TABLE [dbo].[tbl_sms_payment](
	[custnomid] [varchar](100) NULL,
	[notelp] [text] NULL,
	[message] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[tbl_collect_payment](
	[custnomid] [varchar](100) NOT NULL,
	[seq] [varchar](5) NULL,
	[type] [text] NULL,
	[need] [text] NULL,
	[nominal] [int] NULL,
	[datepayment] [datetime] NULL,
	[statussms] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

