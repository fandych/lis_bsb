<?php

require('./sqlsrv.php');
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");

$custnomid= isset($_POST['custnomid']) ? $_POST['custnomid'] : "";
$kartukeluarga= isset($_POST['kartukeluarga']) ? $_POST['kartukeluarga'] : "";
$aktanikah= isset($_POST['aktanikah']) ? $_POST['aktanikah'] : "";
$copyimb= isset($_POST['copyimb']) ? $_POST['copyimb'] : "";
$copypbb= isset($_POST['copypbb']) ? $_POST['copypbb'] : "";
$copysertifikat= isset($_POST['copysertifikat']) ? $_POST['copysertifikat'] : "";
$suratketerangankerja= isset($_POST['suratketerangankerja']) ? $_POST['suratketerangankerja'] : "";
$slipgaji= isset($_POST['slipgaji']) ? $_POST['slipgaji'] : "";
$copyrekeningtabungan= isset($_POST['copyrekeningtabungan']) ? $_POST['copyrekeningtabungan'] : "";
$untukpembelian= isset($_POST['untukpembelian']) ? $_POST['untukpembelian'] : "";
$lb= isset($_POST['lb']) ? $_POST['lb'] : "";
$lt= isset($_POST['lt']) ? $_POST['lt'] : "";
$perumahan= isset($_POST['perumahan']) ? $_POST['perumahan'] : "";
$buktikepemilikan= isset($_POST['buktikepemilikan']) ? $_POST['buktikepemilikan'] : "";
$tujuanpermohonankredit= isset($_POST['tujuanpermohonankredit']) ? $_POST['tujuanpermohonankredit'] : "";
$kewenanganmeminjam= isset($_POST['kewenanganmeminjam']) ? $_POST['kewenanganmeminjam'] : "";
$kewenanganmengagunkan= isset($_POST['kewenanganmengagunkan']) ? $_POST['kewenanganmengagunkan'] : "";
$rencanaagunankredit= isset($_POST['rencanaagunankredit']) ? $_POST['rencanaagunankredit'] : "";
$syaratdanbentukpengikatan= isset($_POST['syaratdanbentukpengikatan']) ? $_POST['syaratdanbentukpengikatan'] : "";
$datainformasidebitur= isset($_POST['datainformasidebitur']) ? $_POST['datainformasidebitur'] : "";
$tanggallegal= isset($_POST['tanggallegal']) ? $_POST['tanggallegal'] : "";
$menyiapkan= isset($_POST['menyiapkan']) ? $_POST['menyiapkan'] : "";
$titlemenyiapkan= isset($_POST['titlemenyiapkan']) ? $_POST['titlemenyiapkan'] : "";
$namapemimpin= isset($_POST['namapemimpin']) ? $_POST['namapemimpin'] : "";
$titlepemimpin= isset($_POST['titlepemimpin']) ? $_POST['titlepemimpin'] : "";
$usulanlegal= isset($_POST['usulanlegal']) ? $_POST['usulanlegal'] : "";
$jaminan= isset($_POST['jaminan']) ? $_POST['jaminan'] : "";
$laporankeuangan= isset($_POST['laporankeuangan']) ? $_POST['laporankeuangan'] : "";
$nomor= isset($_POST['nlp']) ? $_POST['nlp'] : "";


$strsql = "SELECT * from LEGAL_OPINION where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);

if(count($rs)=="0")
{
$tsql = 'INSERT into LEGAL_OPINION (
          CUSTNOMID,
          KARTUKELUARGA,
          AKTANIKAH,
          COPYIMB,
          COPYPBB,
          COPYSERTIFIKAT,
          SURATKETERANGANKERJA,
          SLIPGAJI,
          COPYREKENINGTABUNGAN,
          UNTUKPEMBELIAN,
          LB,
          LT,
          PERUMAHAN,
          BUKTIKEPEMILIKAN,
          TUJUANPERMOHONANKREDIT,
          KEWENANGANMEMINJAM,
          KEWENANGANMENGAGUNKAN,
          RENCANAAGUNANKREDIT,
          SYARATDANBENTUKPENGIKATAN,
          DATAINFORMASIDEBITUR,
          TANGGALLEGAL,
          MENYIAPKAN,
          TITLEMENYIAPKAN,
          NAMAPEMIMPIN,
          TITLEPEMIMPIN,
          USULAN,
          JAMINAN,
          LAPORANKEUANGAN,
		  NOMOR
        )
        VALUES(
          \''.$custnomid.'\',
          \''.$kartukeluarga.'\',
          \''.$aktanikah.'\',
          \''.$copyimb.'\',
          \''.$copypbb.'\',
          \''.$copysertifikat.'\',
          \''.$suratketerangankerja.'\',
          \''.$slipgaji.'\',
          \''.$copyrekeningtabungan.'\',
          \''.$untukpembelian.'\',
          \''.$lb.'\',
          \''.$lt.'\',
          \''.$perumahan.'\',
          \''.$buktikepemilikan.'\',
          \''.$tujuanpermohonankredit.'\',
          \''.$kewenanganmeminjam.'\',
          \''.$kewenanganmengagunkan.'\',
          \''.$rencanaagunankredit.'\',
          \''.$syaratdanbentukpengikatan.'\',
          \''.$datainformasidebitur.'\',
          \''.$tanggallegal.'\',
          \''.$menyiapkan.'\',
          \''.$titlemenyiapkan.'\',
          \''.$namapemimpin.'\',
          \''.$titlepemimpin.'\',
          \''.$usulanlegal.'\',
          \''.$jaminan.'\',
          \''.$laporankeuangan.'\',
          \''.$nomor.'\'

          )';
}
else
{
$tsql = 'UPDATE LEGAL_OPINION set

        KARTUKELUARGA=\''.$kartukeluarga.'\',
        AKTANIKAH=\''.$aktanikah.'\',
        COPYIMB=\''.$copyimb.'\',
        COPYPBB=\''.$copypbb.'\',
        COPYSERTIFIKAT=\''.$copysertifikat.'\',
        SURATKETERANGANKERJA=\''.$suratketerangankerja.'\',
        SLIPGAJI=\''.$slipgaji.'\',
        COPYREKENINGTABUNGAN=\''.$copyrekeningtabungan.'\',
        UNTUKPEMBELIAN=\''.$untukpembelian.'\',
        LB=\''.$lb.'\',
        LT=\''.$lt.'\',
        PERUMAHAN=\''.$perumahan.'\',
        BUKTIKEPEMILIKAN=\''.$buktikepemilikan.'\',
        TUJUANPERMOHONANKREDIT=\''.$tujuanpermohonankredit.'\',
        KEWENANGANMEMINJAM=\''.$kewenanganmeminjam.'\',
        KEWENANGANMENGAGUNKAN=\''.$kewenanganmengagunkan.'\',
        RENCANAAGUNANKREDIT=\''.$rencanaagunankredit.'\',
        SYARATDANBENTUKPENGIKATAN=\''.$syaratdanbentukpengikatan.'\',
        DATAINFORMASIDEBITUR=\''.$datainformasidebitur.'\',
        TANGGALLEGAL=\''.$tanggallegal.'\',
        MENYIAPKAN=\''.$menyiapkan.'\',
        TITLEMENYIAPKAN=\''.$titlemenyiapkan.'\',
        NAMAPEMIMPIN=\''.$namapemimpin.'\',
        TITLEPEMIMPIN=\''.$titlepemimpin.'\',
        USULAN=\''.$usulanlegal.'\',
        JAMINAN=\''.$jaminan.'\',
        LAPORANKEUANGAN=\''.$laporankeuangan.'\',
        NOMOR=\''.$nomor.'\'
        where
        CUSTNOMID=\''.$custnomid.'\'';
}
$db->_EQ($tsql);
require ("../../requirepage/do_saveflow.php");
header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");

?>
