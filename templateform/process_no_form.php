<?
require ("../new_lis/lib/open_con.php");
require ("../new_lis/lib/formatError.php");
require ("../new_lis/requirepage/parameter.php");

echo "<pre>";
print_r($_REQUEST);
echo "</pre>";

$originalregion = $userregion;
$originalbranch = $userbranch;
$originalao = $userid;

$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART WHERE txn_id='$custnomid'";echo $tsql."<br><br>";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
    die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
    $rowCount = sqlsrv_num_rows($sqlConn);
    while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
    {
        $originalbranch = $row[0];
        $originalregion = $row[1];
        $originalao = $row[2];
    }
}
sqlsrv_free_stmt( $sqlConn );


if ($userpermission == "I")
{
	$wfaction = "";
	$tsql = "SELECT wf_action as b FROM Tbl_Workflow WHERE wf_id='$userwfid'";echo $tsql."<br><br>";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

	if ( $sqlConn === false)
		die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$wfaction = $row['b'];
		}
	}

	sqlsrv_free_stmt( $sqlConn );

	if ($wfaction == "I")
	{
		$userpermission = "A";
	}
	if ($wfaction == "IA")
	{
		$userpermission = "C";
	}

	$tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid WHERE txn_id='$custnomid' AND txn_action='$userpermission'";echo $tsql."<br><br>";

	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

	if ( $sqlConn === false)
		die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$rowcount = $row['b'];
		}
	}
	sqlsrv_free_stmt( $sqlConn );

	if ($rowcount <= 0)
	{
		$tsql = "INSERT INTO Tbl_F$userwfid ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code])
		VALUES('$custnomid','$userpermission',getdate(),'$userid','', '$originalbranch','$originalregion')";echo $tsql."<br><br>";
		$params = array(&$_POST['query']);
		$stmt = sqlsrv_prepare( $conn, $tsql, $params);
		if( $stmt )
		{
		}
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}

		if( sqlsrv_execute( $stmt))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt);

		$tsql = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";echo $tsql."<br><br>";
		$stmt = sqlsrv_prepare( $conn, $tsql, $params);
		if( $stmt )
		{
		}
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}

		if( sqlsrv_execute( $stmt))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt);
	}
}
else if ($userpermission == "C")
{
    $wfaction = "";
    $tsql = "SELECT wf_action as b FROM Tbl_Workflow WHERE wf_id='$userwfid'";echo $tsql."<br><br>";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

    if ( $sqlConn === false)
        die( FormatErrors( sqlsrv_errors() ) );

    if(sqlsrv_has_rows($sqlConn))
    {
        $rowCount = sqlsrv_num_rows($sqlConn);
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $wfaction = $row['b'];
        }
    }
    sqlsrv_free_stmt( $sqlConn );

    if ($wfaction == "IC")
    {
        $userpermission = "A";
    }

    $tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid WHERE txn_id='$custnomid' AND txn_action='I'";echo $tsql."<br><br>";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

    if ( $sqlConn === false)
        die( FormatErrors( sqlsrv_errors() ) );

    if(sqlsrv_has_rows($sqlConn))
    {
        $rowCount = sqlsrv_num_rows($sqlConn);
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $rowcount = $row['b'];
        }
    }
    sqlsrv_free_stmt( $sqlConn );

    if ($rowcount > 0)
    {
        $tsql = "UPDATE Tbl_F$userwfid set txn_action='$userpermission', txn_time=getdate() WHERE txn_id='$custnomid'";echo $tsql."<br><br>";
        $params = array(&$_POST['query']);
        $stmt = sqlsrv_prepare( $conn, $tsql, $params);
        if( $stmt )
        {
        }
        else
        {
            echo "Error in preparing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }

        if( sqlsrv_execute( $stmt))
        {
        }
        else
        {
            echo "Error in executing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt( $stmt);

        $tsql = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";echo $tsql."<br><br>";
        $params = array(&$_POST['query']);
        $stmt = sqlsrv_prepare( $conn, $tsql, $params);
        if( $stmt )
        {
        }
        else
        {
            echo "Error in preparing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }

        if( sqlsrv_execute( $stmt))
        {
        }
        else
        {
            echo "Error in executing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt( $stmt);
    }
}
else if ($userpermission == "A")
{
    $notes = "APPROVED BY DEVUSER ON TMPFORM";

    $tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid WHERE txn_id='$custnomid' AND (txn_action='C' or txn_action='I')";echo $tsql."<br><br>";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

    if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

    if(sqlsrv_has_rows($sqlConn))
    {
        $rowCount = sqlsrv_num_rows($sqlConn);
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $rowcount = $row['b'];
        }
    }
    sqlsrv_free_stmt( $sqlConn );

    if ($rowcount > 0)
    {
        $tsql = "UPDATE Tbl_F$userwfid set txn_action='$userpermission', txn_notes='$notes', txn_time=getdate() WHERE txn_id='$custnomid'";echo $tsql."<br><br>";
        $params = array(&$_POST['query']);
        $stmt = sqlsrv_prepare( $conn, $tsql, $params);
        if( $stmt )
        {
        }
        else
        {
            echo "Error in preparing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }

        if( sqlsrv_execute( $stmt))
        {
        }
        else
        {
            echo "Error in executing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt( $stmt);

        $tsql = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";echo $tsql."<br><br>";
        $params = array(&$_POST['query']);
        $stmt = sqlsrv_prepare( $conn, $tsql, $params);
        if( $stmt )
        {
        }
        else
        {
            echo "Error in preparing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }

        if( sqlsrv_execute( $stmt))
        {
        }
        else
        {
            echo "Error in executing statement.\n";
            die( print_r( sqlsrv_errors(), true));
        }
        sqlsrv_free_stmt( $stmt);
    }
}

header("location:../new_lis/page/flow.php?userwfid=$userwfid");
?>
