<?
	ini_set("display_errors", 0);
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../lib/open_conLISAPR.php");

	require ("../../requirepage/parameter.php");

	/*
	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	*/
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;


	require ("../neraca/perhitungan.php");
	require ("../analisa5c/hasilanalisa.php");

	$tanggalkunjungan = "";
	$cabang = "";
	$namanasabah = "";
	$custsex = "";

	$tsql = "select * from Tbl_customermasterperson2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
			$custsex = $row["custsex"];

			if($custsex == "0")
			{
				$namanasabah = $row["custbusname"];
			}
			else
			{
				$namanasabah = $row["custfullname"];
			}
		}
	}

	$mkk_kebutuhan = "";
	$mkk_character = "";
	$mkk_condition = "";
	$mkk_capacity = "";
	$mkk_capital = "";
	$mkk_collateral = "";
	$rekomen = "";

	$tsql = "select * from Tbl_MKKAnalisa5c where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$mkk_kebutuhan = $row['analisa5c_kebutuhan'];
			$mkk_character = $row['analisa5c_character'];
			$mkk_condition = $row['analisa5c_condition'];
			$mkk_capacity = $row['analisa5c_capacity'];
			$mkk_capital = $row['analisa5c_capital'];
			$mkk_collateral = $row['analisa5c_collateral'];
			$rekomen = $row['analisa5c_rekomendasi'];
		}
	}

	$backtoback = "";

	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";

	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$backtoback = $row["custcreditstatus"];
		}
	}

	$flat = "Flat p.a.";

	if($backtoback == "BB")
	{
		$flat = "Effective p.a.";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MKK</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000px" align="center" style="border:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">MEMO KEPUTUSAN KREDIT</td>
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">Nama Nasabah</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $namanasabah;?></div></td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td width="25%">Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td>
</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>

</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">SUMMARY HASIL ANALISA 5C</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="formentry" name="formentry" method="post">
		<table width="96%" align="center" style="border:0px solid black;" class="input">
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center"><strong>No.</strong></td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"><strong>Hasil Analisa</strong></td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><strong>Hasil</strong></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><strong>Keterangan</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">1</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Kebutuhan</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $kesanalisakebutuhandebitur;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_kebutuhan" nai="Kebutuhan " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_kebutuhan;?>"/></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">2</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Character</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $keschar;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_character" nai="Character " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_character;?>"/></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">3</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Condition</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $kescon;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_condition" nai="Condition " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_condition;?>"/></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">4</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Capacity</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $kescapacity;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_capacity" nai="Capacity " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_capacity;?>"/></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">5</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Capital</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $kescapital;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_capital" nai="Capital " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_capital;?>"/></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">6</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Collateral</td>
				<!--<td width="15%" style="border:1px solid black;width:170px;" align="center"><? echo $hsllikuidasi;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><input type="text" id="mkk_collateral" nai="Collateral " style="width:98%;background:#ff0;"  maxlength="250" value="<? echo $mkk_collateral;?>"/></td>
			</tr>
			<tr>
				<td colspan="4" align="center"><!--<input type="button" value="SIMPAN" style="width:120px;background-color:blue;color:white;" onclick="yuria()" />--></td>
			</tr>
			<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
			<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
			<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
			<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
			<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
			<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
			<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
			<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS BARU KREDIT USAHA KECIL</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$fasilitasseq = "";
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;

				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$fasilitasseq = $row["custfacseq"];
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];

						$biayaadmbaru = $plafondbaru * 0.0005;

						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}
						numberFormat($biayaadmbaru);
						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";

						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}

						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}

						$angsuranbaru = (($plafondbaru+($plafondbaru*$sukubungabaru/100*($jangkawaktubaru/12)))/$jangkawaktubaru);

						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

						$angsuranbaru = round($angsuranbaru);
						//echo $angsuranbaru;
						$tsqlproduk = "update tbl_CustomerFacility2 set mkknewangsuran = '$angsuranbaru', mkknewadm = '$biayaadmbaru', mkknewprovisi = '1' where custnomid = '$custnomid' AND custfacseq = '$fasilitasseq'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);


			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($plafondbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru."% ".$flat; ?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($biayaadmbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($angsuranbaru));?></td>
			</tr>

			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo  numberFormat(round($totalplafond));?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo  numberFormat(round($totalangsuran));?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS DI BANK SUMSELBABEL (BARU + EXIST)</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;

				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];

						$biayaadmbaru = $plafondbaru * 0.0005;

						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}

						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";

						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}

						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}

						$angsuranbaru = (($plafondbaru+($plafondbaru*$sukubungabaru/100*($jangkawaktubaru/12)))/$jangkawaktubaru);

						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($plafondbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru."% ".$flat;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($biayaadmbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($angsuranbaru));?></td>
			</tr>

			<?
					}
				}
			?>
			<?
				$jenisfasilitasexist = "";
				$plafondexist = "";
				$sukubungaexist = "";
				$biayaadmexist = "-";
				$tujuanexist = "";
				$jangkawaktuexist = "";
				$provisiexist = "1.00% FLAT";
				$angsuranexist = "";
				$flatexist = "Flat p.a.";
				$saklarflat = "";

				$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankmega2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$jenisfasilitasexist = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
						$plafondexist = $row["FasilitasPinjamanBankMega_plafondawal"];
						$sukubungaexist = $row["FasilitasPinjamanBankMega_rate"];
						$jangkawaktuexist = $row["FasilitasPinjamanBankMega_tenor"];
						$angsuranexist = $row["FasilitasPinjamanBankMega_angsuran"];

						$totalplafond = $totalplafond + $plafondexist;
						$totalangsuran = $totalangsuran + $angsuranexist;

						if($jenisfasilitasexist == "1" || $jenisfasilitasexist == "2" || $jenisfasilitasexist == "3" || $jenisfasilitasexist == "6")
						{
							$tujuanexist = "Modal Kerja";
						}
						else if ($jenisfasilitasexist == "4" || $jenisfasilitasexist == "5")
						{
							$tujuanexist = "Investasi";
						}
						else
						{
							$tujuanexist = "Konsumtif";
						}

						$tsqlproduk = "select JenisFasilitasBankMega_nama, JenisFasilitasBankMega_flag from Tbl_LKCDJenisFasilitasBankMega where JenisFasilitasBankMega_code = '$jenisfasilitasexist'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasexist = $rowproduk["JenisFasilitasBankMega_nama"];
								$saklarflat = $rowproduk["JenisFasilitasBankMega_flag"];
							}
						}

						if($saklarflat == "BTB")
						{
							$flatexist = "Effective p.a.";
						}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($plafondexist));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungaexist."% ".$flatexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $biayaadmexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktuexist;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisiexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($angsuranexist));?></td>
			</tr>
			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo  numberFormat(round($totalplafond))?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo  numberFormat(round($totalangsuran));?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">REKOMENDASI KREDIT OFFICER</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="rekomendasi" name="rekomendasi">
		<table width="96%" align="center" style="border:0px solid black;" class="input">
		<tr>
			<td>Catatan :</td>
		</tr>
		<?
				$notes = "";

				$tsql = "select * from tbl_FMKK where txn_id = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$notes = $row["txn_notes"];
					}
				}

		?>
		<tr>
			<td><input type="text" id="rekomendasikreditofficer" name="rekomendasikreditofficer" style="background-color:#ff0;width:950px;height:50px;" maxlength="100" value="<? echo $notes; ?>" nai="Catatan Rekomendasi Kredit Officer "></input></td>
		</tr>
		<tr>
			<td align="right">
				<select id="selectrekomendasikreditofficer" name="selectrekomendasikreditofficer"  nai="Rekomendasi Kredit Officer" class="harus" style="width:200px;background:#ff0">
					<option value="" selected="selected">- Pilih -</option>
					<option value="DIREKOMENDASIKAN" <? if ($rekomen == "DIREKOMENDASIKAN" ){ echo "selected";}?> >DIREKOMENDASIKAN</option>
					<option value="TIDAK REKOMENDASI" <? if ($rekomen == "TIDAK REKOMENDASI") { echo "selected";}?>>TIDAK REKOMENDASI</option>
				</select>
			</td>
		</tr>
		<tr>
			<td align="center"><input type="button" value="SAVE" class="blue" onclick="yuria();" /></td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>

</table>
<script type="text/javascript">
	function yuria() {
		var nan="MKK";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();

		var mkk_kebutuhan=$("#mkk_kebutuhan").val();
		var mkk_character=$("#mkk_character").val();
		var mkk_condition=$("#mkk_condition").val();
		var mkk_capacity=$("#mkk_capacity").val();
		var mkk_capital=$("#mkk_capital").val();
		var mkk_collateral=$("#mkk_collateral").val();

		var FormName="formentry";
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{

				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false
					break;
				}
			}
		}

		if(StatusAllowSubmit == true)
		{
			$.ajax({
				type: "POST",
				url: "MKK_ajax_n.php",
				data: "nan="+nan+"&mkk_kebutuhan="+mkk_kebutuhan+"&mkk_character="+mkk_character+"&mkk_condition="+mkk_condition+"&mkk_capacity="+mkk_capacity+"&mkk_capital="+mkk_capital+"&mkk_collateral="+mkk_collateral+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{
					kizaki();
				}
			});
		}
	}

	function kizaki() {
		var nan="MKKFLOW";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();

		var rekomendasikreditofficer=$("#rekomendasikreditofficer").val();
		var selectrekomendasikreditofficer=$("#selectrekomendasikreditofficer").val();


		var FormName="rekomendasi";
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{

				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false
					break;
				}
			}
		}

		if(StatusAllowSubmit == true)
		{
			submitform = window.confirm("<? echo $confmsg;?>")
			if (submitform == true)
			{
					$.ajax({
					type: "POST",
					url: "MKK_ajax_n.php",
					data: "nan="+nan+"&rekomendasikreditofficer="+rekomendasikreditofficer+"&selectrekomendasikreditofficer="+selectrekomendasikreditofficer+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{
						//alert('Data berhasil disimpan');
						document.location.href = "../flow.php?userwfid="+userwfid+"";
						//window.location.reload();
					}
				});
				return true;
			}
			else
			{
				return false;
			}

		}
	}

</script>
</body>
</html>
