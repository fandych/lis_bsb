<?php
require_once('../lib/formatError.php');
require_once('../lib/open_con.php');

$appsID = $_REQUEST['in_custnomid'];
$userID = $_REQUEST['in_userid'];
$notesReason = $_REQUEST['in_reason'];



// GET ORIGINAL BRANCH

$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
WHERE txn_id='$appsID'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
$rowCount = sqlsrv_num_rows($sqlConn);
while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
{
$originalbranch = $row[0];
$originalregion = $row[1];
$originalao = $row[2];
}
}
sqlsrv_free_stmt( $sqlConn );


// SAVE FLOW
// SELECT ASSOC

$tsql = "select Flow from Tbl_PrevFlow where Flow <> 'VIEW'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);

$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$EachFlow = $row['Flow'];
		
		//UPDATE TO REJECT PER FLOW
		$tsql_del = "update Tbl_F$EachFlow set txn_action = 'J', txn_user_id = '$userID', txn_time = getdate(), txn_notes = '$notesReason'  where txn_id = '$appsID'";
		
		$params_del = array(&$_POST['query']);
		$stmt_del = sqlsrv_prepare( $conn, $tsql_del, $params_del);
		if( $stmt_del )
		{
		} 
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		
		if( sqlsrv_execute( $stmt_del))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt_del);
	}
}
sqlsrv_free_stmt( $sqlConn );

$tsql = "select * from tbl_flow_status where txn_id = '$appsID'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);

$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

$checkExist = 0;
if(sqlsrv_has_rows($sqlConn))
{
	$checkExist = 1;
}
		
if($checkExist == 1)
{
		//UPDATE TO REJECT TBL FLOW STATUS
		$tsql_del = "update Tbl_Flow_status set txn_action = 'R', txn_user_id = '$userID', txn_time = getdate()  where txn_id = '$appsID'";
		
		$params_del = array(&$_POST['query']);
		$stmt_del = sqlsrv_prepare( $conn, $tsql_del, $params_del);
		if( $stmt_del )
		{
		} 
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		
		if( sqlsrv_execute( $stmt_del))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt_del);
}
else
{
		//UPDATE TO REJECT TBL FLOW STATUS
		$tsql_del = "insert into Tbl_Flow_status values ('$appsID','-','R',getdate(),'$userID','$originalbranch','$originalregion','$originalao')";
		
		$params_del = array(&$_POST['query']);
		$stmt_del = sqlsrv_prepare( $conn, $tsql_del, $params_del);
		if( $stmt_del )
		{
		} 
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		
		if( sqlsrv_execute( $stmt_del))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt_del);
}


echo "<center>Success Reject Application ID $appsID</center>";

?>