
if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
	CKEDITOR.tools.enableHtml5Elements( document );

CKEDITOR.config.height = 450;
CKEDITOR.config.width = 'auto';
CKEDITOR.config.customConfig = './custom_config.js';

var initLain = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'lain_lain' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml('');
		}

		if ( wysiwygareaAvailable ) {
				CKEDITOR.replace( 'lain_lain');
		}
		else
		{
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'lain_lain' );
		}
	};

	function isWysiwygareaAvailable() {
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();


var initRealisasi = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'syarat_realisasi' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml('');
		}

		if ( wysiwygareaAvailable ) {
			CKEDITOR.replace( 'syarat_realisasi');
		}
		else
		{
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'syarat_realisasi' );
		}
	};

	function isWysiwygareaAvailable() {
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();


var initTTD = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'syarat_ttd_pk' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml('');
		}

		if ( wysiwygareaAvailable ) {
			CKEDITOR.replace( 'syarat_ttd_pk');
		}
		else
		{
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'syarat_ttd_pk' );
		}
	};

	function isWysiwygareaAvailable() {
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();
