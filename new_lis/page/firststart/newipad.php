<?
	
	require ("../../lib/open_con.php");
	require ("../../requirepage/parameter.php");
	require ("../../requirepage/security.php");
	
	$checkrows=0;
	$strsql="select * from tbl_se_user where user_id like '".$userid."' and user_level_code='150'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		$checkrows=1;
	}
	if ($checkrows==0)
	{
		$tmpcreditstatus="where status_code<>'TU'";
	}
	else
	{
		$tmpcreditstatus="where status_code='TU'";
	}
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		
		function save()
		{
			var product=$("#product").val();
			var typeloan=$("#typeloan").val();
			var custcreditstatus=$("#custcreditstatus").val();
			var custname=$("#custname").val();
			var currency=$("#currency").val();
			<?
			if ($checkrows==1)
			{
			echo 'var noref=$("#noref").val();';
			}
			?>
			if (product=="")
			{
				alert("Silahkan pilih produk.");
				$("#product").focus();
			}
			else if(currency=="")
			{
				alert("Silahkan pilih mata uang.");
				$("#currency").focus();
			}
			else if(typeloan=="")
			{
				alert("Silahkan pilih tipe Peminjaman.");
				$("#typeloan").focus();
			}
			else if(custname=="")
			{
				alert("Silahkan isi nama debitur.");
				$("#custname").focus();
			}
			else if(custcreditstatus=="")
			{
				alert("Silahkan Pilih Jenis Permohonan Kredit.");
				$("#custcreditstatus").focus();
			}
			<?
			if ($checkrows==1)
			{
			echo '
				else if(custcreditstatus=="")
				{
					alert("Silahkan Isi No refrensi.");
					$("#noref").focus();
				}
			';
			}
			?>
			else
			{
				document.getElementById("frmindex").action="do_new.php";
				document.getElementById("frmindex").submit();
			}
			
		}
		</script>
	</head>
	<body>
		<form id="frmindex" name="frmindex" method="POST" ACTION='./do_new.php'>
			<div style="text-align:center; padding:auto;" >
				<table id="tblform" border="1" style ="width:500px; border-color:black;">
					<tr>
						<td colspan="2" style="text-align:center;">
							<h3>START FLOW</h3>
						</td>
					</tr>
					<tr>
						<td>
							Pilih Produk
						</td>
						<td style="width:250px">
							<select id="product" name="product" style="width:100%;">
							<option value=""> -- Pilih Produk -- </option>
							<?
								$strsql = "select * from Tbl_processing";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										echo '<option value="'.$rows['proc_code'].'">'.$rows['proc_code'].'</option>';
									}
								}
							?>
						</td>
					</tr>
					<tr>
						<td>
							Pilih Mata Uang
						</td>
						<td style="width:250px">
							<select id="currency" name="currency" style="width:100%;">
							<option value=""> -- Pilih Mata Uang -- </option>
							<?
								$strsql = "select * from Tbl_Currency";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										echo '<option value="'.$rows['curr_code'].'">'.$rows['curr_code'].'</option>';
									}
								}
							?>
						</td>
					</tr>
					<tr>
						<td>Tipe Debitur</td>
						<td>
							<select id="typeloan" name="typeloan">
							<option value=""> -- Pilih Tipe Debitur -- </option>
							<option value="0"> Badan Usaha </option>
							<option value="1"> Perorangan </option>
						</td>
					</tr>
					<tr>
						<td>Nama Debitur</td>
						<td>
							<input type="text" name="custname" id="custname" maxlength="50" style="width:98.5%;"/>
							<input type="text" value="asdas" style="width:0%;" tabindex="50"/>
						</td>
					</tr>
					<tr>
						<td>Jenis Permohonan Kredit</td>
						<td>
							<select id="custcreditstatus" name="custcreditstatus" style="width:100%;">
							<option value="">-- Pilih Jenis Permohonan Kredit --</option>
							<?
							$strsql = "select * from TblCreditStatus $tmpcreditstatus";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo '<option value="'.$rows['status_code'].'">'.$rows['status_name'].'</option>';
								}
							}
							?>
							</select>
						</td>
					</tr>
					<?
					if ($checkrows==1)
					{
					echo'
					<tr>
						<td>No. Reference</td>
						<td>
							<input type="text" name="noref" id="noref" maxlength="25"/>
						</td>
					</tr>
					';
					}?>
					<tr>
						<td colspan="2" style="text-align:center;">
							<input type="submit" id="btnsave" name="btnsave" class="buttonsaveflow" value="Save" >
							<?	require ("../../requirepage/hiddenfield.php"); ?>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>