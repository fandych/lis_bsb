<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<form id="formentry" name="formentry" method="post">
<table width="100%" align="center" style="border:1px solid black;">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">DATA KEUANGAN LAINNYA</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<?
	$tsql = "SELECT * FROM Tbl_ParamLKCDDataKeuangan where DK_grup = '0'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
?>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><strong><? echo $row["DK_judul"];?></strong></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			
			<?
				$DK_id = $row["DK_ID"];
				$DataKeuangan_value = 0;
				$tsql2 = "SELECT * FROM Tbl_ParamLKCDDataKeuangan where DK_grup <> '0' AND DK_grup = '$DK_id'";
				$a2 = sqlsrv_query($conn, $tsql2);

				if ( $a2 === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a2))
				{  
					while($rowIsi = sqlsrv_fetch_array($a2, SQLSRV_FETCH_ASSOC))
					{
						$IDisi = $rowIsi["DK_ID"];
						
						$tsql3 = "SELECT * FROM TBL_LKCDDataKeuangan where custnomid = '$custnomid' AND DataKeuangan_ID = '$IDisi'";
						$a3 = sqlsrv_query($conn, $tsql3);

						if ( $a3 === false)
						die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($a3))
						{  
							if($rowDetail = sqlsrv_fetch_array($a3, SQLSRV_FETCH_ASSOC))
							{
								$DataKeuangan_value = $rowDetail["DataKeuangan_value"];
							}
						}
			?>
			
			<tr>
				<td><? echo $rowIsi["DK_isi1"]; ?></td>
				<td><input type="text" id="<? echo $IDisi;?>" name="<? echo $IDisi;?>" style="width:200px;<? if($rowIsi["DK_mandatory"]=="YES"){echo "background:#FF0;";} ?>" nai="<? echo $rowIsi["DK_isi1"];?> " value="<? echo numberFormat($DataKeuangan_value);?>"  maxlength="<? echo $rowIsi["DK_length"];?>"  <? if($rowIsi["DK_type"]=="MONEY"){ echo "onKeyUp=\"outputMoney('"; echo  $IDisi. "')\" onKeyPress=\"return isNumberKey(event)\""; } ?> /> <? echo $rowIsi["DK_isi2"]; ?></td>
			</tr>
			
<?						$DataKeuangan_value = 0;
					}
				}
			
		}
	}

?>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" /></td>
</tr>
	<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
	<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
	<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
	<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
	<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
	<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
	<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
	<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
</form>
<script type="text/javascript">
	function kumi() {
							
		var FormName="formentry";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_ajax5_n.php",
				data: $('#formentry').serialize(),
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
					alert('Data berhasil disimpan');	
				}
			});
		}
		
	}
</script>
</body>
</html> 