<?
session_start();
ob_start();
require_once("sqlsrv.php");


?>

<!DOCTYPE html>
<html>
<head>
<title>Create Tempalte</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />



<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="js/jquery-1.12.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>


	
<style>
	td{padding:5px;}
</style>
</head>
<body>
<form id="frm" name="frm" method="post" >
	<div class="container">
	
	

		
		<select style="width:200px;" id="<?=$rs[$i]?>" name="<?//=$rs[$i]?>setget[]">
				<?php
			foreach($arrysetget as $eachset)
			{
				$selected="";
				if($_setget==$eachset)
				{
					$selected="selected=\"selected\"";
				}
				?><option <?=$selected?> value="<?=$eachset?>"><?=$eachset?></option><?
			}
		?>
		</select>
		
		
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input onclick="onsubmitformnext()" style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
			<div>
				<input onclick="onsubmitformprev()" style="float:left;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="<< Prev">
			</div>
		</div>
	</div>
	<input type="hidden" name="hidval" id="hidval" value="<?=str_replace('"',"|||",$_POST['hidval'])?>">
	<input type="hidden" name="tempname" id="tempname" value="<?=str_replace('"',"|||",$_POST['tempname'])?>">
	<input type="hidden" name="namevar" id="namevar" value="<?=substr($namevar,0,-1)?>">
	<input type="hidden" name="tablefield" id="tablefield" >
</form>
<script>
	function onsubmitformnext()
	{
		if(<?=$totalvariable?>==0)
		{
			strerrmsg("Masukkan Variable Kedalam Template di Step 1")
		}
		else
		{
			$("#frm").attr('action', 'step3.php');
			$("#frm").submit();
		}
	}
	function onsubmitformprev()
	{
		$("#frm").attr('action', 'step1.php');
		$("#frm").submit();
	}
	function settextboxdb(thisid)
	{
		$("#tablefield").val(thisid);
	}
	function setfieldtabledb(thisid)
	{
		//alert(thisid);
		var settextbox = "value"+$("#tablefield").val();
		//alert($("#"+thisid).attr("havevalue"));
		$("#"+settextbox).val($("#"+thisid).attr("havevalue"))
		$("#"+settextbox).focus();
		
	}
</script>
</body>
</html>
