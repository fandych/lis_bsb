<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require ("../../lib/open_con_dm_mysql.php");
require ("../../requirepage/parameter.php");

$statusreadonly = "";
$statusdisabled = "";
$colorinput = "#FF0";
if ($userpermission != "I")
{
   $statusreadonly = "readonly=readonly";
   $statusdisabled = "disabled";
   $colorinput = "#CCC";
}

  $ipdm = "";
  $userdm = "";
	$tsql = "select control_value from ms_control where control_code='IPDM'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if ( $sqlConn === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$row[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}	

        $tuauserid = "";
        $tuatime = "";
    		$tsql = "select tua_userid,tua_time from Tbl_TemporariUserAkses 
    							where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
   			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			$params = array(&$_POST['query']);
   			$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				if($sqlConn === false)
				{
					die(FormatErrors(sqlsrv_errors()));
				}
	
				if(sqlsrv_has_rows($sqlConn))
				{
      			$rowCount = sqlsrv_num_rows($sqlConn);
      			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      			{
        			$tuauserid = $row[0];
        			$tuatime = $row[1];
      			}
   			}
   			sqlsrv_free_stmt( $sqlConn );

				if ($tuauserid == "" || $tuatime == "")
				{
					$tsql = "INSERT INTO Tbl_TemporariUserAkses values('$userwfid',
										'$userid','$custnomid','$userpermission',1,convert(varchar,getdate(),121))";
					$params = array(&$_POST['query']);
					$stmt = sqlsrv_prepare( $conn, $tsql, $params);
					if( $stmt )
					{
					} 
					else
					{
						echo "Error in preparing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
	
					if( sqlsrv_execute( $stmt))
					{
					}
					else
					{
						echo "Error in executing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
					sqlsrv_free_stmt( $stmt);
      	  $bisalanjut = "Y";
				}
				else
				{
					if ($tuauserid == $userid)
					{
						$tsql = "UPDATE Tbl_TemporariUserAkses
											set tua_count=tua_count+1,
											tua_action='$userpermission',
											tua_time=convert(varchar,getdate(),121)
											where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
						$params = array(&$_POST['query']);
						$stmt = sqlsrv_prepare( $conn, $tsql, $params);
						if( $stmt )
						{
						} 
						else
						{
							echo "Error in preparing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
	
						if( sqlsrv_execute( $stmt))
						{
						}
						else
						{
							echo "Error in executing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
						sqlsrv_free_stmt( $stmt);
      	  	$bisalanjut = "Y";
					}
					else
					{
						if($userid!="")
						{
						$varmsg = "Application ID <b>$custnomid</b> sudah diambil oleh user <font color=blue><b>$tuauserid</b></font> pada jam <font color=red><b>$tuatime</b></font> <BR>";
						echo $varmsg;
						exit;
						}
					}
				}

?>
<HTML>
   <HEAD>
      <META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>
      <META http-equiv='Pragma' content='no-cache'>
      <META content='MSHTML 5.50.4134.100' name=GENERATOR>
      <TITLE>CKPK ADMIN</TITLE>
      <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
      <link href="../../css/d.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
      <Script Language="JavaScript">
        function toggleExpCol(idGrp, idContainer) {
            idContainer = idContainer ? idContainer + '_' : '';
            var div = document.getElementById(idContainer + 'divGrp' + idGrp);
            var vis = div.style.display = (div.style.display == 'none') ? 'block' : 'none';
        }
			function refreshhtml()
			{
				window.location.reload()
			}
			function uploadDocument(thelink)
			{
	        varwidth = 980;
	        varheight = 640;
	        varx = 0;
	        vary = 0;
          window.open(thelink,'lainnya','scrollbars=yes,width='+varwidth+',height='+varheight+',screenX='+varx+',screenY='+vary+',top='+varx+',left='+vary+',status=yes');
//          document.formsubmit.submit();
			}
function cekBTAO()
{
	if (document.frm.STSBTAO.options[document.frm.STSBTAO.selectedIndex].value == "")
	{
	   alert("Harap Pilih Status Back To AO");
	   document.frm.STSBTAO.focus();
	   return false;
	}
	if (document.frm.KETBTAO.value.length < 10)
	{
	   alert("Harap Isi Keterangan Back To AO");
	   document.frm.KETBTAO.focus();
	   return false;
	}
		document.frm.target = "utama";
		document.frm.act.value = "saveform";
		document.frm.userpermission.value = "B";
		document.frm.action = "./do_saveflow.php";
		submitform = window.confirm("Back To AO ?")
		if (submitform == true)
		{
			document.frm.submit();
			return true;
		}
}

function goSave()
{
<?
      $tsql = "SELECT * FROM Tbl_DocAdmin";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
?>
						if (document.frm.STS<? echo $row[0]; ?>[document.frm.STS<? echo $row[0]; ?>.selectedIndex].value == "")
						{
							document.frm.STS<? echo $row[0]; ?>.focus();
							return false;
						}
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
?>
		document.frm.target = "utama";
		document.frm.userpermission.value = "I";
		document.frm.act.value = "saveform";
		document.frm.action = "./do_saveflow.php";
		//submitform = window.confirm("Save FLOW ?")
		submitform = window.confirm("<?=$confmsg;?>")
		if (submitform == true)
		{
			document.frm.submit();
			return true;
		}
}
function goApprove(theid)
{
   document.frm.target = "utama";
   document.frm.approvepermission.value = theid;
   document.frm.act.value = "saveform";
   document.frm.action = "./do_saveflow.php";

  if (theid == "A")
  {
	   varmsg = "Approve  ?";
	}
  if (theid == "R")
  {
	   varmsg = "Revision  ?";
	}
  if (theid == "J")
  {
	   varmsg = "Reject ?";
	}

   submitform = window.confirm(varmsg);
   if (submitform == true)
   {
			document.frm.submit();
			return true;
   }
   else
   {
	  return false;
   } 
}
	function cekthis()
	{
     document.frm.target = "utama";
     document.frm.action = "./dospin_admin.php";
           submitform = window.confirm("Submit Data ?")
           if (submitform == true)
           {
	            document.frm.submit();
              return true;
           }
           else
           {
              return false;
           }
	}
      </Script>
   </HEAD>
   <BODY link=blue vlink=blue alink=blue>
<script language="JavaScript"><!--
name = 'utama';
//--></script>
      <div align=center>
      <TABLE cellPadding=5 width="100%" border=0 class="table">
        <TR>
    	  <TD vAlign=top width=1>
            <TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
              <TR>
                <TD class=backW vAlign=center>
          	</TD>
              </TR>
            </TABLE>
          </TD>
          <TD align=left valign=top>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <TR>
                <TD class=borderForm align=right bgColor=black>
                      <font style="font-size: 12;" color=#FFFFFF><B>Form type : Acceptance CKPK Admin &nbsp;</B></font>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                  <TABLE cellSpacing=1 cellPadding=13 width="100%" border=0>
                    <TR>
                      <TD class=backW>
                  	<form name="frm" id="frm" method=post action=./dospin_admin.php>
                  	   <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=1 border=0>                  	   	
<?
      $jenisjaminan = "";
      $kondisijaminan = "";
      $tsql = "SELECT cust_jeniscol FROM Tbl_Cust_MasterCol
      				 WHERE ap_lisregno='$custnomid'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
         	  if (substr($row[0],0,1) == "B")
         	  {
         	     $jenisjaminan .= "Building, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='B'";
         	  }
         	  if (substr($row[0],0,1) == "V")
         	  {
         	     $jenisjaminan .= "Vehicle, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='V'";
         	  }
         	  if (substr($row[0],0,1) == "T")
         	  {
         	     $jenisjaminan .= "Tanah, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	  }
         }
      }
      sqlsrv_free_stmt( $sqlConn );

      $custcif = "";
      $tsql = "SELECT * FROM Tbl_CustomerMasterPerson
      				 WHERE custnomid='$custnomid'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
         {
			$custcif = $row['custaplno'];
			 
			 if(trim($row['custbusname'])!="")
			 {
				 $tampilnama = $row['custbusname'];
			 }
			 else
			 {
				 $tampilnama = $row['custfullname'];
			 }
			 
?>
							          <tr>
							          	 <td width=20% align=left valign=top>
							          	 	<font face=Arial size=2>Application ID / CIF</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=70% align=left valign=top>
							          	 	<font face=Arial size=2><? echo $custnomid; ?>  / <? echo $custcif; ?></font>
							          	 	&nbsp &nbsp
							          	 	<a href="javascript:refreshhtml()">refresh</a>
							          	</td>
							          </tr>
							          <tr>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>Customer Name</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=80% align=left valign=top>
							          	 	<font face=Arial size=2><A href="../Preview/PreviewAll/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $custnomid; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL"  target=_blank ><? echo $tampilnama; ?></A></font>
							          	</td>
							          </tr>
							          <tr>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>Jaminan</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=80% align=left valign=top>
							          	 	<font face=Arial size=2><? echo $jenisjaminan; ?></font>
							          	</td>
							          </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
?>
                  	   </TABLE>
                  	   <BR>
                  	   <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=0 border=1 bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
												   <tr bgcolor=#CCCCCC>
												   	<td width=30% align=center valign=top>
												   		<font face=Arial size=2><b>DOCUMENT</b></font>
												   	</td>
												   	<td width=10% align=center valign=top>
												   		<font face=Arial size=2><b>STATUS</b></font>
												   	</td>
												   	<td width=15% align=center valign=top>
												   		<font face=Arial size=2><b>TGL TBO</b></font>
												   	</td>
												   	<td width=25% align=center valign=top>
												   		<font face=Arial size=2><b>NOTES</b></font>
												   	</td>
												   	<td width=20% align=center valign=top>
												   		<font face=Arial size=2><b>PREVIEW</b></font>
                                                        
												   	</td>
												  </tr>
<?
      $tsql = "SELECT * FROM Tbl_DocAdmin where doc_flag <> 'L'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
      		 $vardocstatus = "";
      		 $vardocnotes = "";
      		 $varyes = "";
      		 $vartbo = "";
      		 $varflag = "";
	  
			  $tsql2 = "SELECT *
						        FROM Tbl_CKPKAdmin
						        WHERE docnomid='$custnomid'
						        AND doccode='$row[0]'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
      		   	$vardocstatus = $row2['docstatus'];
      		   	$vardocnotes = $row2['docnotes'];
      		   	$varflag = $row2['doc_flag'];
      		   	if ($vardocstatus == "YES")
      		   	{
      		   		$varyes = "selected";
      		   		$vartbo = "";
      		    }
      		   	if ($vardocstatus == "TBO")
      		   	{
      		   		$varyes = "";
      		   		$vartbo = "selected";
      		    }
      		    
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
?>
												   <tr>
												   	<td width=30% align=left valign=top>
												   		<font face=Arial size=2><? echo $row[0]; ?> - <? echo $row[1]; ?></font>
												   	</td>
												   	<td width=10% align=center valign=top>
<?
														if($statusdisabled == "disabled")
														{
															if($vardocstatus=="")
															{
																$vardocstatus = "&nbsp;";
															}
															echo $vardocstatus;
														}else{
?>
												   		<select style="background-color:<?=$colorinput;?>" name=STS<? echo $row[0]; ?> <? echo $statusdisabled; ?>>
												   			 <option value=''>--Pilih--</option>
												   			 <option value='YES' <? echo $varyes; ?>>DONE</option>
												   			 <option value='TBO' <? echo $vartbo; ?>>TBO</option>
												   		</select>
<?															
														}
?>
												   	</td>
												   	<td width=15% align=center valign=top>
<?
														if($statusreadonly=="readonly=readonly")
														{
															if($varflag=="")
															{
																$varflag="&nbsp;";
															}
															echo $varflag;
														}else{
?>
												   		<input type=text style="background-color:<?=$colorinput;?>" id=DATE<? echo $row[0]; ?> name=DATE<? echo $row[0]; ?> maxlength=10 style="width: 25mm;" value='<? echo $varflag; ?>' <? echo $statusreadonly; ?>>
														<a href="javascript:NewCssCal('DATE<? echo $row[0]; ?>','yyyymmdd')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
<?														
														}
?>														
												   	</td>
												   	<td width=25% align=center valign=top>
<?
														if($statusreadonly=="readonly=readonly")
														{
															if($vardocnotes=="")
															{
																$vardocnotes = "&nbsp;";
															}
															echo $vardocnotes;
														}else{
?>
												   		<textarea style="background-color:<?=$colorinput;?>" name=NOTE<? echo $row[0]; ?> rows=4 cols=30 <? echo $statusreadonly; ?>><? echo $vardocnotes; ?></textarea>
<?														
														}
?>				
												   	</td>
												   	<td width=20% align=center valign=top>
<?
     $statusdoc = $row[0] . ",";
//     $custcif = "CIF001";
		 $key = $custcif . "_" . $custnomid;

     $querytemp = "SELECT COUNT(*)
	   		                    FROM Tbl_Document
	        	                WHERE doc_index3='$custnomid'
	        	                AND doc_type='$row[0]'";
	   $resulttemp=mysql_query($querytemp);
	   $rowtemp = mysql_fetch_row($resulttemp);
	   $countrows=$rowtemp[0];



//     $linkdmI='http://'.$ipdm.'/Lis_mikro/spindm_new/external_upload.php?dmuserid=lismikro&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     $linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&dmuserorganization=PRIVATE&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     if ($countrows <= 0)
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload</A></div>";
     }
     else
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload / View</A></div>";
     }
     echo $uploadgambarI;
?>
                                  
                                  &nbsp
												   	</td>
												  </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
?>
                       </TABLE>


<center>
<?
if($userpermission == "I")
{
	?>
	<input type="button" id="btnsave" name="btnsave" value="save" style="width:300px;" class="button" onClick="cekthis();"/>
	<?
}
else
{
	if($userid != "")
	{
	require ("../../requirepage/btnview.php");
	}
}
require ("../../requirepage/hiddenfield.php");
?>
<input type=hidden name=act value='simpandata'>
<input type=hidden name=datadoc value='<? echo $datadoc; ?>'>
<input type=hidden name=hiddenfield value='<? echo $hiddenfield; ?>'>
</center>  
</form>
                      </TD>
                    </TR>
                    <TR>
                      <TD class=backW></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                </TD>
              </TR>
              <TR>
                <TD height=15>
                   <table width=100% cellpadding=0 cellspacing=0 border=0>
                      <tr>
                         &nbsp
                      <tr>
                      <tr>
                      </tr>
                   </table>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
     </div>
   </BODY>
</HTML>
