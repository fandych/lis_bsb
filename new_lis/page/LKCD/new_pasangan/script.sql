USE [db_new_lis]
GO
/****** Object:  Table [dbo].[pl_custpasangan]    Script Date: 04/28/2016 12:51:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pl_custpasangan](
	[_custnomid] [varchar](30) NOT NULL,
	[_pasangan_namadepan] [varchar](100) NOT NULL,
	[_pasangan_namatengah] [varchar](50) NULL,
	[_pasangan_namabelakang] [varchar](50) NULL,
	[_pasangan_tempatlahir] [varchar](50) NOT NULL,
	[_pasangan_tgllahir] [date] NOT NULL,
	[_pasangan_nomorktp] [varchar](30) NOT NULL,
	[_pasangan_expiredktp] [date] NULL,
	[_pasangan_pekerjaan_id] [varchar](10) NOT NULL,
	[_pasangan_mulaibekerjan] [date] NULL,
	[_pasangan_namaperusahaan] [varchar](50) NULL,
	[_pasangan_telpperusahaan] [varchar](20) NULL,
	[_pasangan_penghasilanbersih] [decimal](14, 0) NULL,
	[_pasangan_flag] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[_custnomid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[pl_custpasangan] ([_custnomid], [_pasangan_namadepan], [_pasangan_namatengah], [_pasangan_namabelakang], [_pasangan_tempatlahir], [_pasangan_tgllahir], [_pasangan_nomorktp], [_pasangan_expiredktp], [_pasangan_pekerjaan_id], [_pasangan_mulaibekerjan], [_pasangan_namaperusahaan], [_pasangan_telpperusahaan], [_pasangan_penghasilanbersih], [_pasangan_flag]) VALUES (N'KUK4172300059', N'Andi', N'Guna', N'Wibawa', N'Jakarta', CAST(0x493B0B00 AS Date), N'987654321', CAST(0x4A3B0B00 AS Date), N'PK01', CAST(0x76250B00 AS Date), N'PT. Aoa', N'098765544', CAST(2000000 AS Decimal(14, 0)), N'')
