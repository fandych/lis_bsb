<?php 


session_start();
ob_start();
require_once("sqlsrv.php");



//ini_set( 'default_charset', 'ISO-8859-1' );
//header("Content-Type: text/html;charset=ISO-8859-1");

ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");


$templatecontent="";
$templatename="";




if(isset($_SESSION['templatecontent']))
{
	
	$templatecontent = $_SESSION['templatecontent'] = str_replace('|||','"',$_SESSION['templatecontent']);
	$templatename = $_SESSION['templatename'] = str_replace('|||','"',$_SESSION['templatename']);
}
else
{
	$strsql="select _body,_name from tr_template where _idx='".$_SESSION['template']."'";	
	$rs = $condb->_RQ($strsql);
	if(!is_array($rs)){echo $rs;}else
	{
		$no=0;
		for($i=0; $i<count($rs);$i++) 
		{
			$templatecontent = $_SESSION['templatecontent'] =str_replace("|||","'",$rs[$i]['_body']);
			$templatename = $_SESSION['templatename'] =str_replace("|||","'",$rs[$i]['_name']);
		}
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Classic theme development file</title>


<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />


<script src="js/jquery-1.12.3.min.js"></script>
<script src="js/tinymce/tinymce.dev.js"></script>
<script src="js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="js/tinymce/plugins/spellchecker/plugin.dev.js"></script>

<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>

<script>
	tinymce.init({
		selector: "textarea#elm1",
		theme: "modern",
		
		height: window.innerHeight-300,
		plugins: [
			"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor colorpicker codesample"
		],
		external_plugins: {
			//"moxiemanager": "/moxiemanager-php/plugin.js"
		},
		content_css: "css/development.css",
		add_unload_trigger: false,
		autosave_ask_before_unload: false,

		toolbar1: "styleselect formatselect fontselect fontsizeselect forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | ",
		toolbar2: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media help code | insertdatetime preview ",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | insertfile insertimage codesample",
		menubar: false,
		toolbar_items_size: 'small',

		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		templates: [
			//{title: 'My template 1', description: 'Some fancy template 1', content: 'My html'},
			//{title: 'My template 2', description: 'Some fancy template 2', url: 'development.html'}
		],

        spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}
	});
	
	
	function checkerik(id)
	{
		$("#hidval").val(tinymce.get('elm1').getContent());
		if(fc(id)!=0)
		{
			$("#frm").attr('action', 'step2.php');
			$("#frm").submit();
		}
	}


</script>
<style>
	
</style>
</head>
<body>
<form id="frm" name="frm" method="post">
	<div class="container" id="test">
		<div style="text-align:left;font-size:30px;line-height:45px;">Editor <? echo $_SESSION['template']. "  " .$_SESSION['templatename'] ?>
			<div style="float:right;line-height:45px;">
				<a href="tmp.php?a=3&b=<?=$_SESSION['template']?>" class="btn btn-warning btn-md" role="button"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
			</div>
		</div>
		<hr>
		<div style="line-height:50px;">
			Template Name <input required style="width:800px" maxlength="100" type="text" id="tempname" name="tempname" value="<?=$templatename?>" erikmsg="Template Name"/>
		</div>
		<textarea  id="elm1" name="elm1" rows="15" cols="80" style="width: 80%" >
			<?=$templatecontent?>
		</textarea>
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input tv="test" onclick="checkerik(this.id);" id="btnnext" style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
		</div>
		<input required type="hidden" name="hidval" id="hidval" erikmsg="Template Content">
	</div>
</form>

</body>
</html>
