<?php
	require ("../../lib/open_conCOLL.php");
	require ("../../lib/formatError.php");
    
    $namey = "";
    $getyearquery = "select * from collect_paramloan";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $getyearquery, $params, $cursorType);
    $rowsyear = sqlsrv_num_rows($sqlConn);
    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
    if(sqlsrv_has_rows($sqlConn))
    {
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $namey.= "'".$row['_loan_name']."',";
        }
        $namey =substr($namey,0,-1);
    }

    
    $namex = "";
    $getyearquery = "select isnull(SUM(b._txn_nominal),0)as 'total',a._cust_listype 
                from collect_custmaster a
                join collect_custtxn b on b._custnomid = a._custnomid
                where  b._txn_statusbayar='N'
                and a._cust_kolektibilitas_id <>'L'
                group by a._cust_listype";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $getyearquery, $params, $cursorType);
    $rowsyear = sqlsrv_num_rows($sqlConn);
    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
    if(sqlsrv_has_rows($sqlConn))
    {
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $namex.= round($row['total'],2).",";
        }
        $namex =substr($namex,0,-1);
    }

    //echo $namex;

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>

		<script type="text/javascript" src="../../bootstrap/dist/js/jquery-1.11.3.js" ></script>
		
		<script src="../../js/highcharts.js"></script>
        <script src="../../js/modules/exporting.js"></script>

		<script type="text/javascript">
        $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Non Performing Loan'
                },
                xAxis: {
                    categories: [<?=$namey?>],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IDR ',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' millions'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                
                credits: {enabled: false},
                exporting: { enabled: false },
                series: [{
                    showInLegend: false,
                    name: 'Total',
                    data: [<?=$namex?>]
                }]
            });
        });
		</script>
	</head>
	<body>

<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

	</body>
</html>
