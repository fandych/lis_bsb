<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$tanggalkunjungan = "";
	$cabang = "";
	
	$tsql = "select * from Tbl_customermasterperson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
		}
	}
	
	$colortab1 = "blue";
	$colortab2 = "blue";
	$colortab3 = "blue";
	$colortab4 = "blue";
	$colortab5 = "blue";
	$colortab6 = "blue";
	$colortab7 = "blue";
	$colortab8 = "blue";
	$colortab9 = "blue";
	$colortab10 = "blue";
	
	$colorseq = 0;
	
	
	$tsql = "select * from Tbl_InfoTab where custnomid = '$custnomid' AND InfoTab_Flow = 'LKCDVID'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$colorseq = $row["InfoTab_Seq"];
			
			if($colorseq == '1')
			{
				$colortab1 = "green";
			}
			if($colorseq == '2')
			{
				$colortab2 = "green";
			}
			if($colorseq == '3')
			{
				$colortab3 = "green";
			}
			if($colorseq == '4')
			{
				$colortab4 = "green";
			}
			if($colorseq == '5')
			{
				$colortab5 = "green";
			}
			if($colorseq == '6')
			{
				$colortab6 = "green";
			}
			if($colorseq == '7')
			{
				$colortab7 = "green";
			}
			if($colorseq == '8')
			{
				$colortab8 = "green";
			}
			if($colorseq == '9')
			{
				$colortab9 = "green";
			}
			if($colorseq == '10')
			{
				$colortab10 = "green";
			}
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000px" align="center" style="border:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">LAPORAN KUNJUNGAN CALON DEBITUR</td>
</tr>
<tr>
	<td width="25%">Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td> 
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>

<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td align="center" id="yukirin">
		<input type="button" value="Kebutuhan Kredit Investasi" style="width:200px;background-color:<? echo $colortab2;?>;color:white;" onclick="nannan(2)" title="IDENTIFIKASI KEBUTUHAN KREDIT INVESTASI"/>
		<input type="button" value="Data Neraca" style="width:200px;background-color:<? echo $colortab3;?>;color:white;" onclick="nannan(3)" title="IDENTIFIKASI KEBUTUHAN MODAL KERJA"/>
		<input type="button" value="Data Usaha" style="width:200px;background-color:<? echo $colortab4;?>;color:white;" onclick="nannan(4)" title="IDENTIFIKASI DATA-DATA USAHA CALON DEBITUR"/>
		<!--<input type="button" value="5" style="width:80px;background-color:<? echo $colortab5;?>;color:white;" onclick="nannan(5)" title="DATA KEUANGAN LAINNYA"/>-->
		<input type="button" value="Mutasi Rekening" style="width:200px;background-color:<? echo $colortab6;?>;color:white;" onclick="nannan(6)" title="MUTASI REKENING KORAN / TABUNGAN"/>
		<input type="button" value="Pinjaman Bank Lain" style="width:200px;background-color:<? echo $colortab7;?>;color:white;" onclick="nannan(7)" title="FASILITAS PINJAMAN DI BANK LAIN"/>
		<input type="button" value="Pinjaman Bank Mega" style="width:200px;background-color:<? echo $colortab8;?>;color:white;" onclick="nannan(8)" title="FASILITAS PINJAMAN DI BANK MEGA (KHUSUS DEBITUR EXISTING)"/>
		<input type="button" value="Fasilitas yang diajukan" style="width:200px;background-color:<? echo $colortab1;?>;color:white;" onclick="nannan(1)" title="IDENTIFIKASI FASILITAS YANG DIAJUKAN"/>
		<input type="button" value="Trade & Community Check" style="width:200px;background-color:<? echo $colortab9;?>;color:white;" onclick="nannan(9)" title="TRADE & COMMUNITY CHECKING"/>
		<!--<input type="button" value="10" style="width:80px;background-color:<? echo $colortab10;?>;color:white;" onclick="nannan(10)" title="NERACA PROFORMA"/>-->
		
	</td>
</tr>
</table>

<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td id="sugananako">&nbsp;</td>
</tr>
</table>

<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<!--<td align="center"><input type="button" value="SAVE FLOW" style="width:150px;background-color:blue;color:white;" onclick="saveflow()" title="SAVE FLOW"/></td>-->
</tr>
</table>

<script type="text/javascript">
	function nannan(id) {
		var nan="SWAPVIEW";
		var id = id;
		//alert(id);
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&id="+id+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	//alert(response);
				$("#sugananako").html(response);
				refresh();
			}
		});
	}
	
	function refresh()
	{
		var nan = "REFRESH";
		var custnomid=$("#custnomid").val();
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&custnomid="+custnomid+"",
			success: function(response)
			{	
				$("#yukirin").html(response);
			}
		});
	}
	
	function saveflow()
	{
		var nan = "SAVEFLOW";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	alert(response);
				//$("#sugananako").html(response);
				//alert('Berhasil save flow');
			}
		});
	}
</script>
</body>
</html> 