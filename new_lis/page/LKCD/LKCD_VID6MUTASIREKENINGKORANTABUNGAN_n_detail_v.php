<?	
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	$view = $_GET['view'];
	$taginput = "";
	$tagselect = "";
	$warna = "class=\"input\"";
	$button = "type=\"button\"";
	$tampil = "1";
	
	if($view == "preview")
	{
		$taginput = "readonly=\"readonly\"";
		$tagselect = "disabled=\"disabled\"";
		$warna = "class=\"preview2\"";
		$button = "type=\"hidden\"";
		$tampil = "0";
	}
	
	require("../../requirepage/currency.php");
	
	$arrMonth=array("JANUARI", "FEBRUARI", "MARET", "APRIL", "MEI", "JUNI", "JULI", "AGUSTUS", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER");
	
	$tsql = "select * from Tbl_LKCDMutasiRekening2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$seqMutasi = $row["seq"];
			$MutasiRekening_namabulan = $row["MutasiRekening_namabulan"];
			
			if (($key = array_search($MutasiRekening_namabulan, $arrMonth)) !== false) {
			unset($arrMonth[$key]);
			}
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/my.js" ></script>
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
	function addCommas(nStr)
	{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
	
	function hitungdebet(seq, val, idx){
		if(seq!=1){
			var prevseq = parseInt(seq) - 1;
			document.getElementById("MutasiRekening_saldoawal"+seq+idx).value = document.getElementById("MutasiRekening_saldoakhir"+prevseq+idx).value;
		}
		if(val==0){
			document.getElementById("MutasiRekening_mutasidebet"+seq+idx).value = 0;
			val = 0;
		}
		var saldoawal = replace(document.getElementById("MutasiRekening_saldoawal"+seq+idx).value, ',', '');
		var saldokredit = replace(document.getElementById("MutasiRekening_mutasikredit"+seq+idx).value, ',', '');
		var val = replace(val, ',', '');		
		var saldoakhir = parseInt(saldoawal) - parseInt(val)+ parseInt(saldokredit);

		document.getElementById("MutasiRekening_saldoakhir"+seq+idx).value = addCommas(saldoakhir);	
	}
	function hitungkredit(seq, val, idx){
		if(seq!=1){
			var prevseq = parseInt(seq) - 1;
			document.getElementById("MutasiRekening_saldoawal"+seq+idx).value = document.getElementById("MutasiRekening_saldoakhir"+prevseq+idx).value;
		}
		if(val==0){
			document.getElementById("MutasiRekening_mutasikredit"+seq+idx).value = 0;
			val = 0;
		}
		var saldoawal = replace(document.getElementById("MutasiRekening_saldoawal"+seq+idx).value, ',', '');
		var saldodebit = replace(document.getElementById("MutasiRekening_mutasidebet"+seq+idx).value, ',', '');
		var val = replace(val, ',', '');		
		var saldoakhir = parseInt(saldoawal) + parseInt(val) - parseInt(saldodebit);
		
		document.getElementById("MutasiRekening_saldoakhir"+seq+idx).value = addCommas(saldoakhir);			
	}
	
	function fillmonth(monthnow, idx){
		var monthnow = monthnow;
		
		var arrMonth=["01 JANUARI", "02 FEBRUARI", "03 MARET", "04 APRIL", "05 MEI", "06 JUNI", "07 JULI", "08 AGUSTUS", "09 SEPTEMBER", "10 OKTOBER", "11 NOVEMBER", "12 DESEMBER"];

		for(var i=1; i<6; i++){
			var monthnext = arrMonth.indexOf(monthnow)+i;		
			if(monthnext>11){
				monthnext = monthnext - 12;
			}
			var x = i+1;
			document.getElementById("MutasiRekening_namabulan"+x+idx).value = arrMonth[monthnext];
		}
	}
</script>
</head>
<body>
<table width="100%" align="center" style="border:1px solid black;" <? echo $warna;?>>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry1" name="formentry1" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td colspan="5" style="text-align:center;font-weight:bold;">MUTASI REKENING <?=$nama_bank;?></td>
		</tr>
		<tr>
			<td colspan="4" style="text-align:center;font-weight:bold;">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
				<form id="formsaldo" name="formsaldo" method="post">
				<table width="100%" align="center" style="border:0px solid black;text-align:left;">
				<tr>
					<td width="20%"><strong>Nama Bank</strong></td>
					<td width="20%"> : <?=$nama_bank;?></td>
					<td width="20%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%"><strong>Nomor Rekening</strong></td>
					<td width="20%"> : <?=$nomor_rekening;?></td>
					<td width="20%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%"><strong>Jenis Rekening</strong></td>
					<td width="20%"> : <?=$jenis_rekening;?></td>
					<td width="20%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%"><strong>Atas Nama</strong></td>
					<td width="20%"> : <?=$atas_nama;?></td>
					<td width="20%">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%"><strong>Keterangan</strong></td>
					<td width="20%"> : <?=$keterangan;?></td>
					<td width="20%">&nbsp;</td>
				</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BULAN</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>SALDO AWAL (<? echo $currency; ?>)</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>MUTASI DEBET (<? echo $currency; ?>)</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>MUTASI KREDIT (<? echo $currency; ?>)</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>SALDO AKHIR (<? echo $currency; ?>)</strong></td>
		</tr>
			<?
			$seqMutasi = "";
			$MutasiRekening_namabulan = "-";
			$MutasiRekening_saldoawal = "-";
			$MutasiRekening_mutasidebet = "-";
			$MutasiRekening_mutasikredit = "-";
			$MutasiRekening_saldoakhir = "-";
			$Totalsaldoawal = 0;
			$Totaldebet = 0;
			$Totalkredit = 0;
			$Totalsaldoakhir = 0;
			$tempsaldoawal = 0;
			$tempdebet = 0;
			$tempkredit = 0;
			$tempsaldoakhir = 0;
			$ct = 0;
			
			$tsql = "select * from Tbl_LKCDMutasiRekening where custnomid = '$custnomid' and MutasiRekening_flag = '$idx'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqMutasi = $row["seq"];
					$MutasiRekening_namabulan = $row["MutasiRekening_namabulan"];
					$MutasiRekening_saldoawal = $row["MutasiRekening_saldoawal"];
					$MutasiRekening_mutasidebet = $row["MutasiRekening_mutasidebet"];
					$MutasiRekening_mutasikredit = $row["MutasiRekening_mutasikredit"];
					$MutasiRekening_saldoakhir = $row["MutasiRekening_saldoakhir"];
				
					$tempsaldoawal = str_replace(",","", $MutasiRekening_saldoawal);
					$Totalsaldoawal = $Totalsaldoawal + $tempsaldoawal;
				
					$tempdebet = str_replace(",","", $MutasiRekening_mutasidebet);
					$Totaldebet = $Totaldebet + $tempdebet;
					
					$tempkredit = str_replace(",","", $MutasiRekening_mutasikredit);
					$Totalkredit = $Totalkredit + $tempkredit;
					
					$tempsaldoakhir = str_replace(",","", $MutasiRekening_saldoakhir);
					$Totalsaldoakhir = $Totalsaldoakhir + $tempsaldoakhir;
					$ct++;
		?>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_namabulan?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_saldoawal)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_mutasidebet)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_mutasikredit)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_saldoakhir)?></td>
		</tr>
		<?		}
			}
		?>
		
		<!--<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totaldebet)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalkredit)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoakhir)?></td>
			<td width="20%">&nbsp;</td>
		</tr>-->
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>Rata - rata</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoawal/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totaldebet/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalkredit/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoakhir/$ct,2)?></td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<!--<tr>
	<td colspan="2">
		<form id="formsaldo" name="formsaldo" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="20%"><strong>Saldo akhir <?=$att_bank?> : </strong></td>
			<td width="20%"><?=number_format($saldo_terakhir)?></td>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>-->
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
		<table width="100%" align="center" style="border:0px solid black;">
		<?if($tampil == "0"){?>
		<?
			$seqMutasi = "";
			$MutasiRekening_namabulan = "-";
			$MutasiRekening_saldoawal = "-";
			$MutasiRekening_mutasidebet = "-";
			$MutasiRekening_mutasikredit = "-";
			$MutasiRekening_saldoakhir = "-";
			$Totalsaldoawal = 0;
			$Totaldebet = 0;
			$Totalkredit = 0;
			$Totalsaldoakhir = 0;
			$tempsaldoawal = 0;
			$tempdebet = 0;
			$tempkredit = 0;
			$tempsaldoakhir = 0;
			$ct = 0;
			
			$tsql = "select * from Tbl_LKCDMutasiRekening2 where custnomid = '$custnomid' and MutasiRekening_flag = '$idx'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqMutasi = $row["seq"];
					$MutasiRekening_namabulan = $row["MutasiRekening_namabulan"];
					$MutasiRekening_saldoawal = $row["MutasiRekening_saldoawal"];
					$MutasiRekening_mutasidebet = $row["MutasiRekening_mutasidebet"];
					$MutasiRekening_mutasikredit = $row["MutasiRekening_mutasikredit"];
					$MutasiRekening_saldoakhir = $row["MutasiRekening_saldoakhir"];
					
					$tempsaldoawal = str_replace(",","", $MutasiRekening_saldoawal);
					$Totalsaldoawal = $Totalsaldoawal + $tempsaldoawal;
				
					$tempdebet = str_replace(",","", $MutasiRekening_mutasidebet);
					$Totaldebet = $Totaldebet + $tempdebet;
					
					$tempkredit = str_replace(",","", $MutasiRekening_mutasikredit);
					$Totalkredit = $Totalkredit + $tempkredit;
					
					$tempsaldoakhir = str_replace(",","", $MutasiRekening_saldoakhir);
					$Totalsaldoakhir = $Totalsaldoakhir + $tempsaldoakhir;
					$ct++;
			
		?>	

		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_namabulan?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_namabulan?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_mutasidebet)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_mutasikredit)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($MutasiRekening_saldoakhir)?></td>
		</tr>
		<?
				}
			}
			
		?>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>Rata - rata</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoawal/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totaldebet/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalkredit/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoakhir/$ct,2)?></td>
		</tr>
		<?
		}else{
		?>
		<form id="formsavethis<?=$idx;?>" name="formsavethis<?=$idx;?>" method="post">
		<?
			$ct = 0;
			$seqMutasi = "";
			$Totalsaldoawal = 0;
			$Totaldebet = 0;
			$Totalkredit = 0;
			$Totalsaldoakhir = 0;
			$tempsaldoawal = 0;
			$tempdebet = 0;
			$tempkredit = 0;
			$tempsaldoakhir = 0;
			
			for($y=1;$y<=6;$y++){
				${'MutasiRekening_namabulan'.$y} = "";
				${'MutasiRekening_saldoawal'.$y} = 0;
				${'MutasiRekening_mutasidebet'.$y} = 0;
				${'MutasiRekening_mutasikredit'.$y} = 0;
				${'MutasiRekening_saldoakhir'.$y} = 0;
			}
			
			$tsql = "select * from Tbl_LKCDMutasiRekening2 where custnomid = '$custnomid' and MutasiRekening_flag = '$idx'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqMutasi = $row["seq"];
					
					${'MutasiRekening_namabulan'.$seqMutasi} = $row["MutasiRekening_namabulan"];
					${'MutasiRekening_saldoawal'.$seqMutasi} = $row["MutasiRekening_saldoawal"];
					${'MutasiRekening_mutasidebet'.$seqMutasi} = $row["MutasiRekening_mutasidebet"];
					${'MutasiRekening_mutasikredit'.$seqMutasi} = $row["MutasiRekening_mutasikredit"];
					${'MutasiRekening_saldoakhir'.$seqMutasi} = $row["MutasiRekening_saldoakhir"];
					
					$tempsaldoawal = str_replace(",","", $row["MutasiRekening_saldoawal"]);
					$Totalsaldoawal = $Totalsaldoawal + $tempsaldoawal;

					$tempdebet = str_replace(",","",  $row["MutasiRekening_mutasidebet"]);
					$Totaldebet = $Totaldebet + $tempdebet;
					
					$tempkredit = str_replace(",","", $row["MutasiRekening_mutasikredit"]);
					$Totalkredit = $Totalkredit + $tempkredit;
					
					$tempsaldoakhir = str_replace(",","", $row["MutasiRekening_saldoakhir"]);
					$Totalsaldoakhir = $Totalsaldoakhir + $tempsaldoakhir;
					$ct++;
				}
			}
		?>	
		
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center">
				<select id="MutasiRekening_namabulan1<?=$idx;?>" name="MutasiRekening_namabulan1<?=$idx;?>"  nai="Nama Bulan Mutasi Rekening " class="harus" style="width:200px;background:#ff0" onchange="fillmonth(this.value, <?=$idx;?>);">
					<option value="" selected="selected">- Pilih -</option>
					<option value="01 JANUARI" <? if ($MutasiRekening_namabulan1 == "01 JANUARI") { echo "selected";}?>>01 JANUARI</option>
					<option value="02 FEBRUARI" <? if ($MutasiRekening_namabulan1 == "02 FEBRUARI") { echo "selected";}?>>02 FEBRUARI</option>
					<option value="03 MARET" <? if ($MutasiRekening_namabulan1 == "03 MARET") { echo "selected";}?>>03 MARET</option>
					<option value="04 APRIL" <? if ($MutasiRekening_namabulan1 == "04 APRIL") { echo "selected";}?>>04 APRIL</option>
					<option value="05 MEI" <? if ($MutasiRekening_namabulan1 == "05 MEI") { echo "selected";}?>>05 MEI</option>
					<option value="06 JUNI" <? if ($MutasiRekening_namabulan1 == "06 JUNI") { echo "selected";}?>>06 JUNI</option>
					<option value="07 JULI" <? if ($MutasiRekening_namabulan1 == "07 JULI") { echo "selected";}?>>07 JULI</option>
					<option value="08 AGUSTUS" <? if ($MutasiRekening_namabulan1 == "08 AGUSTUS") { echo "selected";}?>>08 AGUSTUS</option>
					<option value="09 SEPTEMBER" <? if ($MutasiRekening_namabulan1 == "09 SEPTEMBER") { echo "selected";}?>>09 SEPTEMBER</option>
					<option value="10 OKTOBER" <? if ($MutasiRekening_namabulan1 == "10 OKTOBER") { echo "selected";}?>>10 OKTOBER</option>
					<option value="11 NOVEMBER" <? if ($MutasiRekening_namabulan1 == "11 NOVEMBER") { echo "selected";}?>>11 NOVEMBER</option>
					<option value="12 DESEMBER" <? if ($MutasiRekening_namabulan1 == "12 DESEMBER") { echo "selected";}?>>12 DESEMBER</option>
				</select>
			</td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal1<?=$idx;?>" name="MutasiRekening_saldoawal1<?=$idx;?>" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal1);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet1<?=$idx;?>" name="MutasiRekening_mutasidebet1<?=$idx;?>" onkeyup="hitungdebet(1, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet1);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit1<?=$idx;?>" name="MutasiRekening_mutasikredit1<?=$idx;?>" onkeyup="hitungkredit(1, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit1);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir1<?=$idx;?>" name="MutasiRekening_saldoakhir1<?=$idx;?>"readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir1);?>" maxlength="20" /></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_namabulan2<?=$idx;?>" name="MutasiRekening_namabulan2<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:200px;background:#4286f4;text-align:left;" value="<?=$MutasiRekening_namabulan2?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal2<?=$idx;?>" name="MutasiRekening_saldoawal2<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal2);?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet2<?=$idx;?>" name="MutasiRekening_mutasidebet2<?=$idx;?>" onkeyup="hitungdebet(2, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet2);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit2<?=$idx;?>" name="MutasiRekening_mutasikredit2<?=$idx;?>" onkeyup="hitungkredit(2, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit2);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir2<?=$idx;?>" name="MutasiRekening_saldoakhir2<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir2);?>" maxlength="20" /></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_namabulan3<?=$idx;?>" name="MutasiRekening_namabulan3<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:200px;background:#4286f4;text-align:left;" value="<?=$MutasiRekening_namabulan3?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal3<?=$idx;?>" name="MutasiRekening_saldoawal3<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal3);?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet3<?=$idx;?>" name="MutasiRekening_mutasidebet3<?=$idx;?>" onkeyup="hitungdebet(3, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet3);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit3<?=$idx;?>" name="MutasiRekening_mutasikredit3<?=$idx;?>" onkeyup="hitungkredit(3, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit3);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir3<?=$idx;?>" name="MutasiRekening_saldoakhir3<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir3);?>" maxlength="20" /></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_namabulan4<?=$idx;?>" name="MutasiRekening_namabulan4<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:200px;background:#4286f4;text-align:left;" value="<?=$MutasiRekening_namabulan4?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal4<?=$idx;?>" name="MutasiRekening_saldoawal4<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal4);?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet4<?=$idx;?>" name="MutasiRekening_mutasidebet4<?=$idx;?>" onkeyup="hitungdebet(4, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet4);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit4<?=$idx;?>" name="MutasiRekening_mutasikredit4<?=$idx;?>" onkeyup="hitungkredit(4, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit4);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir4<?=$idx;?>" name="MutasiRekening_saldoakhir4<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir4);?>" maxlength="20" /></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_namabulan5<?=$idx;?>" name="MutasiRekening_namabulan5<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:200px;background:#4286f4;text-align:left;" value="<?=$MutasiRekening_namabulan5?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal5<?=$idx;?>" name="MutasiRekening_saldoawal5<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal5);?>" maxlength="20"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet5<?=$idx;?>" name="MutasiRekening_mutasidebet5<?=$idx;?>" onkeyup="hitungdebet(5, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet5);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit5<?=$idx;?>" name="MutasiRekening_mutasikredit5<?=$idx;?>" onkeyup="hitungkredit(5, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit5);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir5<?=$idx;?>" name="MutasiRekening_saldoakhir5<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir5);?>" maxlength="20"/></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_namabulan6<?=$idx;?>" name="MutasiRekening_namabulan6<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:200px;background:#4286f4;text-align:left;" value="<?=$MutasiRekening_namabulan6?>" maxlength="20" /></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoawal6<?=$idx;?>" name="MutasiRekening_saldoawal6<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoawal6);?>" maxlength="20"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasidebet6<?=$idx;?>" name="MutasiRekening_mutasidebet6<?=$idx;?>" onkeyup="hitungdebet(6, this.value, <?=$idx;?>);" nai="Mutasi Debet " style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasidebet6);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_mutasikredit6<?=$idx;?>" name="MutasiRekening_mutasikredit6<?=$idx;?>" onkeyup="hitungkredit(6, this.value, <?=$idx;?>);" nai="Mutasi Kredit "style="width:150px;background:#ff0;text-align:right;" value="<? echo number_format($MutasiRekening_mutasikredit6);?>" maxlength="20" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);"/></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="MutasiRekening_saldoakhir6<?=$idx;?>" name="MutasiRekening_saldoakhir6<?=$idx;?>" readonly nai="Mutasi Kredit "style="width:150px;background:#4286f4;text-align:right;" value="<? echo number_format($MutasiRekening_saldoakhir6);?>" maxlength="20"/></td>
		</tr>
		<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>Rata - rata</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoawal/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totaldebet/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalkredit/$ct,2)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoakhir/$ct,2)?></td>
		</tr>
		<tr>
			<td colspan="5" align="center">&nbsp;</td>
		</tr>
		<input type="hidden" id="idx" name="idx" value="<?=$idx;?>" />
		<input type="hidden" id="custnomid" name="custnomid" value="<?=$custnomid;?>" />
		<tr>
			<td colspan="5" align="center"><input type="button" value="SAVE" class="bluesmall" onclick="savethis('<?=$idx?>')" /></td>
		</tr>
		</form>
		<?
		}
		?>
		
		<!--<tr>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totaldebet)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalkredit)?></td>
			<td width="20%" style="border:1px solid black;width:100px;" align="center"><? echo number_format($Totalsaldoakhir)?></td>
			<td width="20%">&nbsp;</td>
		</tr>-->
		
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<!--
<?if($tampil == "0"){?>
<tr>
	<td colspan="2">
		<form id="formsaldo" name="formsaldo" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="20%"><strong>Saldo akhir <?=$att_bank?> : </strong></td>
			<td width="20%"><?=number_format($saldo_terakhir2)?></td>
			<td width="20%">&nbsp;</td>
			<td width="20%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<?}else{?>
<tr>
	<td colspan="2">
		<form id="formsaldo" name="formsaldo" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="20%"><strong>Saldo akhir <?=$att_bank?> : </strong></td>
			<td width="20%"><input type="text" id="MutasiRekening_saldoakhir<?=$nama_bank?>" nai="Mutasi Nama Bank " style="width:170px;background:#ff0;" value="<?=number_format($saldo_terakhir2)?>" maxlength="22" onKeyUp="outputMoney('MutasiRekening_saldoakhir<?=$nama_bank?>')" onKeyPress="return isNumberKey(event)"/></td>
			<td width="20%"><input type="button" value="Save" class="bluesmall" onclick="save6('<?=$nama_bank?>')" /></td>
			<td width="20%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<?}?>
-->
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>


<script type="text/javascript">
	function nanako(){
		var nan="SAVE6"
		
		var custnomid=$("#custnomid").val();
		var StatusAllowSubmit=true;
		if(StatusAllowSubmit == true)
		{	
			submitform = window.confirm("<? echo $confmsg;?>")
			if (submitform == true)
			{
				$.ajax({
					type: "POST",
					url: "LKCD_VIDajax_n.php",
					data: "nan="+nan+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{	
						//alert('Data berhasil disimpan');	
					}
				});
			}
		}
	}
	
	function kumi() {
		var nan="SAVE6A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var MutasiRekening_namabulan = $("#MutasiRekening_namabulanAdd").val();
		var MutasiRekening_mutasidebet = $("#MutasiRekening_mutasidebetAdd").val();
		var MutasiRekening_mutasikredit = $("#MutasiRekening_mutasikreditAdd").val();
			
		var FormName="formentry1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&MutasiRekening_namabulan="+MutasiRekening_namabulan+"&MutasiRekening_mutasidebet="+MutasiRekening_mutasidebet+"&MutasiRekening_mutasikredit="+MutasiRekening_mutasikredit+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function kizaki(id) {
		var nan="EDIT6A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var MutasiRekening_namabulan = $("#MutasiRekening_namabulanAdd"+id).val();
		var MutasiRekening_mutasidebet = $("#MutasiRekening_mutasidebetAdd"+id).val();
		var MutasiRekening_mutasikredit = $("#MutasiRekening_mutasikreditAdd"+id).val();
		var MutasiRekening_seq = id;	
		
		var FormName="formedit1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&MutasiRekening_seq="+MutasiRekening_seq+"&MutasiRekening_namabulan="+MutasiRekening_namabulan+"&MutasiRekening_mutasidebet="+MutasiRekening_mutasidebet+"&MutasiRekening_mutasikredit="+MutasiRekening_mutasikredit+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena(id){
		var nan="DEL6A"
		
		var custnomid=$("#custnomid").val();
		var seqMutasi = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&seqMutasi="+seqMutasi+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria() {
		var nan="SAVE6B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var RekapPenjualan_namabulan = $("#RekapPenjualan_namabulanAdd").val();
		var RekapPenjualan_penjualan = $("#RekapPenjualan_penjualanAdd").val();
			
		var FormName="formentry2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&RekapPenjualan_namabulan="+RekapPenjualan_namabulan+"&RekapPenjualan_penjualan="+RekapPenjualan_penjualan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function akari(id){
		var nan="DEL6B"
		
		var custnomid=$("#custnomid").val();
		var seqRekap = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&seqRekap="+seqRekap+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function suga(id) {
		var nan="EDIT6B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var RekapPenjualan_namabulan = $("#RekapPenjualan_namabulanAdd"+id).val();
		var RekapPenjualan_penjualan = $("#RekapPenjualan_penjualanAdd"+id).val();
		var RekapPenjualan_seq = id;	
		
		var FormName="formedit2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&RekapPenjualan_seq="+RekapPenjualan_seq+"&RekapPenjualan_namabulan="+RekapPenjualan_namabulan+"&RekapPenjualan_penjualan="+RekapPenjualan_penjualan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 