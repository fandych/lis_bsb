<?php
function FormatErrors( $errors )
{
    /* Display errors. */
    echo "Error information: <br/>";
	
	//echo "<pre>";
	//print_r($errors);
	//echo "</pre>";
	
    foreach ( $errors as $error )
    {
        echo "SQLSTATE: ".$error['SQLSTATE']."<br/>";
        echo "Code: ".$error['code']."<br/>";
        echo "Message: ".$error['message']."<br/>";
    }
}

function numberFormat($num)
{ 
	return preg_replace("/(?<=\d)(?=(\d{3})+(?!\d))/",",",$num); 
}

?>