<?

$lkcdsektorindustri=0;
$lkcdjenisusaha=0;
$lkcdlamausahayear=0;
$lkcdlamausahamonth=0;
$lkcdkepemilikan=0;
$lkcdjumlahharikerjausaha=0;
$lkcdjenisjumlahharikerjausaha=0;
$lkcdratajumlahpembeli=0;
$lkcdratapembelian=0;
$lkcdkenaikanjumlahpembeli=0;
$lkcdkenaikantiketsize=0;
$lkcdjenishpp="";
$lkcdhpp=0;
$lkcdmaginusaha=0;
$lkcdgajirataperpegawai=0;
$lkcdjumlahkaryawan=0;
$lkcdbiayasewa=0;
$lkcdbiayatelepon=0;
$lkcdsampah=0;
$lkcdoperasionallainnya=0;
$lkcdbiayarumahtangga=0;
$lkcdtujuanmodalkerja=0;
$lkcdjenispersediaan=0;
$lkcdpersediaan=0;
$lkcdcarabayarpiutang=0;
$lkcdjenispiutang=0;
$lkcdpiutang=0;
$lkcdcarabayarhutang=0;
$lkcdjenisbayarhutang=0;
$lkcdhutang=0;
$lkcdsaldotabungandibank=0;
$lkcddeposito=0;
$lkcdkaskecil=0;
$lkcdlainnya1=0;
$lkcdtanahkosong=0;
$lkcdtanahdanbangunan=0;
$lkcdkendaraan=0;
$lkcdruko=0;
$lkcdmesin=0;
$lkcdlainnya2=0;

$strsql="select * from Tbl_LKCDDataKeuangan2 where custnomid='$custnomid'";
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$lkcdsektorindustri=$rows['lkcdsektorindustri'];
		$lkcdjenisusaha=$rows['lkcdjenisusaha'];
		$lkcdlamausahayear=$rows['lkcdlamausahayear'];
		$lkcdlamausahamonth=$rows['lkcdlamausahamonth'];
		$lkcdkepemilikan=$rows['lkcdkepemilikan'];
		$lkcdjumlahharikerjausaha=$rows['lkcdjumlahharikerjausaha'];
		$lkcdjenisjumlahharikerjausaha=$rows['lkcdjenisjumlahharikerjausaha'];
		$lkcdratajumlahpembeli=$rows['lkcdratajumlahpembeli'];
		$lkcdratapembelian=$rows['lkcdratapembelian'];
		$lkcdkenaikanjumlahpembeli=$rows['lkcdkenaikanjumlahpembeli'];
		$lkcdkenaikantiketsize=$rows['lkcdkenaikantiketsize'];
		$lkcdjenishpp=$rows['lkcdjenishpp'];
		$lkcdhpp=$rows['lkcdhpp'];
		$lkcdmaginusaha=$rows['lkcdmaginusaha'];
		$lkcdgajirataperpegawai=$rows['lkcdgajirataperpegawai'];
		$lkcdjumlahkaryawan=$rows['lkcdjumlahkaryawan'];
		$lkcdbiayasewa=$rows['lkcdbiayasewa'];
		$lkcdbiayatelepon=$rows['lkcdbiayatelepon'];
		$lkcdsampah=$rows['lkcdsampah'];
		$lkcdoperasionallainnya=$rows['lkcdoperasionallainnya'];
		$lkcdbiayarumahtangga=$rows['lkcdbiayarumahtangga'];
		$lkcdtujuanmodalkerja=$rows['lkcdtujuanmodalkerja'];
		$lkcdjenispersediaan=$rows['lkcdjenispersediaan'];
		$lkcdpersediaan=$rows['lkcdpersediaan'];
		$lkcdcarabayarpiutang=$rows['lkcdcarabayarpiutang'];
		$lkcdjenispiutang=$rows['lkcdjenispiutang'];
		$lkcdpiutang=$rows['lkcdpiutang'];
		$lkcdcarabayarhutang=$rows['lkcdcarabayarhutang'];
		$lkcdjenisbayarhutang=$rows['lkcdjenisbayarhutang'];
		$lkcdhutang=$rows['lkcdhutang'];
		$lkcdsaldotabungandibank=$rows['lkcdsaldotabungandibank'];
		$lkcddeposito=$rows['lkcddeposito'];
		$lkcdkaskecil=$rows['lkcdkaskecil'];
		$lkcdlainnya1=$rows['lkcdlainnya1'];
		$lkcdtanahkosong=$rows['lkcdtanahkosong'];
		$lkcdtanahdanbangunan=$rows['lkcdtanahdanbangunan'];
		$lkcdkendaraan=$rows['lkcdkendaraan'];
		$lkcdruko=$rows['lkcdruko'];
		$lkcdmesin=$rows['lkcdmesin'];
		$lkcdlainnya2=$rows['lkcdlainnya2'];
	}
}
sqlsrv_free_stmt( $execsql );

//====================PROFORMA=====================///
$perkalian=0;
if ($lkcdjenisjumlahharikerjausaha=="1")
{
	$perkalian=48;
}
else if ($lkcdjenisjumlahharikerjausaha=="2")
{
	$perkalian=12;
}
else if ($lkcdjenisjumlahharikerjausaha=="3")
{
	$perkalian=1;
}
//echo $lkcdjumlahharikerjausaha;
$proforma_omset=$perkalian*$lkcdjumlahharikerjausaha*$lkcdratajumlahpembeli*$lkcdratapembelian;

$proforma_hpp=0;
if($lkcdmaginusaha=="0")
{
	if($lkcdjenishpp=="1")
	{
		$proforma_hpp=$lkcdhpp;
	}
	else if($lkcdjenishpp=="2")
	{
		$proforma_hpp=($lkcdhpp/100)*$proforma_omset;
	}
}
else
{
	$proforma_hpp=(1-($lkcdmaginusaha/100))*$proforma_omset;
}


///proforma_margin
$proforma_margin=$proforma_omset-$proforma_hpp;

//biaya operasional
$proforma_biayaoperasional=(($lkcdjumlahkaryawan*$lkcdgajirataperpegawai)+$lkcdbiayasewa+$lkcdbiayasewa+$lkcdbiayasewa+$lkcdbiayatelepon+$lkcdsampah+$lkcdoperasionallainnya)*12;


///proforma_angsuran
$proforma_angsuran=0;

$strsql="
		select isnull(sum(a),0) as angsuran,custnomid from (
		select isnull(sum(cast(FasilitasPinjamanBankLain_angsuran as DEC)),0) as a,custnomid 
		from Tbl_LKCDFasilitasPinjamanBankLain2 where FasilitasPinjamanBankLain_type<>'TO'
		group by custnomid
		union
		select isnull(sum(cast(FasilitasPinjamanBankMega_angsuran as DEC)),0) as a,custnomid 
		from Tbl_LKCDFasilitasPinjamanBankMega2
		group by custnomid
		) tblxx
		where custnomid='$custnomid'
		group by custnomid";
		
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$proforma_angsuran=$rows['angsuran'];
	}
}
sqlsrv_free_stmt( $execsql );


////proforma_labasebelumpajak
$proforma_labasebelumpajak=$proforma_margin-($proforma_biayaoperasional+($proforma_angsuran*12));

////pajakproforma_pajak10%
$proforma_pajak=$proforma_labasebelumpajak/10;

////laba proforma_lababersih
$proforma_lababersih=$proforma_labasebelumpajak-$proforma_pajak;


///$proforma_rumahtangga
$proforma_rumahtangga=($lkcdbiayarumahtangga*12);


//PendapatanBersih
$proforma_pendapatanbersih=$proforma_lababersih-$proforma_rumahtangga;

//===========================NERACA PROFORMA==============================//
///Neraca_kasdanbank
$Neraca_kasdanbank=$lkcdsaldotabungandibank+$lkcddeposito+$lkcdkaskecil+$lkcdlainnya1;

//Neraca_piutang
$Neraca_piutang=$lkcdpiutang;

if($lkcdcarabayarpiutang=="1")
{
	$Neraca_piutang=0;
}
else
{
	if($lkcdjenispiutang=="2")
	{
		$Neraca_piutang=$lkcdpiutang/360*$proforma_omset;
	}
}

//Neraca_persediaan

$Neraca_persediaan=$lkcdpersediaan;
if($lkcdjenispersediaan=="2")
{
	$Neraca_persediaan=$lkcdpersediaan/360*$proforma_hpp;
}

////Neraca_totalaktivalancar
$Neraca_totalaktivalancar=$Neraca_kasdanbank+$Neraca_piutang+$Neraca_persediaan;

///Neraca_totalaktivatetap


$Neraca_totalaktivatetap=$lkcdtanahkosong+$lkcdtanahdanbangunan+$lkcdkendaraan+$lkcdruko+$lkcdmesin+$lkcdlainnya2;

//Neraca_totalaktiva
$Neraca_totalaktiva=$Neraca_totalaktivatetap+$Neraca_totalaktivalancar;

//Neraca_hutang
$Neraca_hutang=0;
$Neraca_hutang=$lkcdhutang;

if($lkcdcarabayarhutang=="1")
{
	$Neraca_hutang=0;
}
else
{
	if($lkcdjenisbayarhutang=="2")
	{
		$Neraca_hutang=$lkcdhutang/360*$proforma_omset;
	}
}

///Neraca_hutangjkpendek
$Neraca_hutangjkpendek=$proforma_angsuran*12;

//Neraca_hutangjkpanjang
$outstanding=0;
$strsql="
select isnull(sum(a),0) as outstanding,custnomid from (
select isnull(sum(cast(FasilitasPinjamanBankLain_outstanding as DEC)),0) as a,custnomid 
from Tbl_LKCDFasilitasPinjamanBankLain2 where FasilitasPinjamanBankLain_type<>'TO'
group by custnomid

union
select isnull(sum(cast(FasilitasPinjamanBankMega_outstanding as DEC)),0) as a,custnomid 
from Tbl_LKCDFasilitasPinjamanBankMega2

group by custnomid
union
select isnull(sum(cast(FasilitasPinjamanBankLain_outstanding as DEC))*-1,0) as a,custnomid 
from Tbl_LKCDFasilitasPinjamanBankLain2 where FasilitasPinjamanBankLain_type='TO'
group by custnomid

) tblxx
where custnomid='$custnomid'
group by custnomid";
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$outstanding=$rows['outstanding'];
	}
}
sqlsrv_free_stmt( $execsql );

//echo number_format($outstanding);
$Neraca_hutangjkpanjang=$outstanding-$Neraca_hutangjkpendek;

///Neraca_totalhutanglancar
$Neraca_totalhutanglancar=$Neraca_hutangjkpendek+$Neraca_hutang;

///Neraca_totalhutang
$Neraca_totalhutang=$Neraca_totalhutanglancar+$Neraca_hutangjkpanjang;

////Neraca_labaditahan
$Neraca_labaditahan=$Neraca_totalaktiva-$Neraca_totalhutang;

//Neraca_totalhutangdanmodal
$Neraca_totalhutangdanmodal=$Neraca_totalhutang+$Neraca_labaditahan;


//======================== proyeksi laba rugi proforma =======================

$angsuranbaru=0;
$totalplafondyangdiajukan=0;
$strsql="select sum((CAST(custcreditplafond as dec)+ 
(CAST(custcreditplafond as dec) * (cast(sukubungayangdiberikan as dec(18,2))/100
)*cast(custcreditlong as dec)/12))/cast(custcreditlong as dec)) as 'angsuran',
sum(CAST(custcreditplafond as dec)) as 'totalplafondyangdiajukan' from tbl_CustomerFacility2 
where custnomid='$custnomid' group by custcreditlong";
/*
$strsql="select sum((CAST(custcreditplafond as dec)+ CAST(custcreditplafond as dec)* cast(sukubungayangdiberikan as dec)*
((cast(custcreditlong as dec)/12))) /cast(custcreditlong as dec))/
(cast(custcreditlong as dec)) as 'angsuran',sum(CAST(custcreditplafond as dec))
as 'totalplafondyangdiajukan'
from tbl_CustomerFacility2 where custnomid='$custnomid'
group by custcreditlong";
*/
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$angsuranbaru=$rows['angsuran'];
		$totalplafondyangdiajukan=$rows['totalplafondyangdiajukan'];
	}
}
//echo $angsuranbaru;
///proyeksi_proforma_omset

//$proforma_omset=$perkalian*$lkcdjumlahharikerjausaha*$lkcdratajumlahpembeli*$lkcdratapembelian;


$proyeksi_proforma_omset=$perkalian*$lkcdjumlahharikerjausaha*((1+($lkcdkenaikanjumlahpembeli/100))*$lkcdratajumlahpembeli)*($lkcdratapembelian*(1+($lkcdkenaikantiketsize/100)));
//echo $perkalian
//$proyeksi_proforma_omset=$proforma_omset+($proforma_omset*($lkcdkenaikanjumlahpembeli/100)*($lkcdkenaikantiketsize/100));

$proyeksi_proforma_hpp=0;

if($lkcdmaginusaha=="0")
{
	if($lkcdjenishpp=="1")
	{
		$proyeksi_proforma_hpp=$lkcdhpp;
	}
	else if($lkcdjenishpp=="2")
	{
		$proyeksi_proforma_hpp=$lkcdhpp*$proyeksi_proforma_omset/100;
	}
}
else
{
	$proyeksi_proforma_hpp=(1-($lkcdmaginusaha/100))*$proyeksi_proforma_omset;
}

//proyeksi_proforma_marginusaha
$proyeksi_proforma_marginusaha=$proyeksi_proforma_omset-$proyeksi_proforma_hpp;

///proyeksi_proforma_biayaoperasional
$proyeksi_proforma_biayaoperasional=$proforma_biayaoperasional;

////proyeksi_proforma_angsuran
$proyeksi_proforma_angsuran=$Neraca_hutangjkpendek+($angsuranbaru*12);

//proyeksi_proforma_labasebelumpajak
$proyeksi_proforma_labasebelumpajak=$proyeksi_proforma_marginusaha-$proyeksi_proforma_biayaoperasional-$proyeksi_proforma_angsuran;

//proyeksi_proforma_pajak
$proyeksi_proforma_pajak=$proyeksi_proforma_labasebelumpajak/10;

//proyeksi_proforma_lababersih
$proyeksi_proforma_lababersih=$proyeksi_proforma_labasebelumpajak-$proyeksi_proforma_pajak;

//proforma_rumahtangga
$proyeksi_proforma_rumahtangga=$proforma_rumahtangga;

//PendapatanBersih
$proyeksi_proforma_pendapatanbersih=$proyeksi_proforma_lababersih-$proyeksi_proforma_rumahtangga;

//======================== proyeksi NERACA =======================//
$proyeksi_neraca=0;
$totalkebutuhan=0;
$strsql="SELECT isnull(SUM(cast(a as dec)),0) as 'totalkebutuhan',custnomid FROM( 
		select jenisbarang_total as 'a',custnomid from Tbl_LKCDJenisBarang2 
		union 
		select tempatusaha_total as 'a',custnomid from Tbl_LKCDTempatUsaha2 
		union 
		select rencanapembangunan_total as 'a',custnomid from Tbl_LKCDRencanaPembangunan2 
		union 
		select KebutuhanTakeover_totaloutstanding as 'a',custnomid from Tbl_LKCDKebutuhanTakeover2 
		)TBLZXX 
		where custnomid='$custnomid' 
		group by custnomid";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totalkebutuhan=$rows['totalkebutuhan'];
	}
}

///proyeksi_neraca_kasdanbank
$proyeksi_neraca_kasdanbank=$Neraca_kasdanbank+(0.3*$totalkebutuhan);

//proyeksi_neraca_piutang
$proyeksi_neraca_piutang=$lkcdpiutang;

if($lkcdcarabayarpiutang=="1")
{
	$proyeksi_neraca_piutang=0;
}
else
{
	if($lkcdjenispiutang=="2")
	{
		$proyeksi_neraca_piutang=$lkcdpiutang/360*$proyeksi_proforma_omset;
	}
}
//proyeksi_neraca_persediaan
$proyeksi_neraca_persediaan=$lkcdpersediaan;
if($lkcdjenispersediaan=="2")
{
	$proyeksi_neraca_persediaan=$lkcdpersediaan/360*$proyeksi_proforma_hpp;
}


/*
if($lkcdjenispersediaan!="0")
{
	$proyeksi_neraca_persediaan=$lkcdpersediaan/360*$proyeksi_proforma_hpp;
}
*/
////proyeksi_neraca_totalaktivalancar
$proyeksi_neraca_totalaktivalancar=$proyeksi_neraca_kasdanbank+$proyeksi_neraca_piutang+$proyeksi_neraca_persediaan;


$kebutuhaninvestasi=0;
$strsql="SELECT isnull(SUM(cast(a as dec)),0) as 'totalkebutuhan',custnomid FROM( 
		select jenisbarang_total as 'a',custnomid from Tbl_LKCDJenisBarang2 
		union 
		select tempatusaha_total as 'a',custnomid from Tbl_LKCDTempatUsaha2 
		union 
		select rencanapembangunan_total as 'a',custnomid from Tbl_LKCDRencanaPembangunan2 
		union 
		select KebutuhanTakeover_totaloutstanding as 'a',custnomid from Tbl_LKCDKebutuhanTakeover2 
		)TBLZXX 
		where custnomid='$custnomid' 
		group by custnomid";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$kebutuhaninvestasi=$rows['totalkebutuhan'];
	}
}


///proyeksi_neraca_totalaktivatetap

$proyeksi_neraca_totalaktivatetap=$Neraca_totalaktivatetap+$kebutuhaninvestasi;

//proyeksi_neraca_totalaktiva
$proyeksi_neraca_totalaktiva=$proyeksi_neraca_totalaktivatetap+$proyeksi_neraca_totalaktivalancar;

//proyeksi_neraca_hutang
$proyeksi_neraca_hutang=$lkcdhutang;

if($lkcdjenisbayarhutang=="1")
{
	$proyeksi_neraca_hutang=0;
}
else
{
	if($lkcdjenisbayarhutang=="2")
	{
		$proyeksi_neraca_hutang=$lkcdhutang/360*$proyeksi_proforma_hpp;
	}
}
/*
$strsql="
select custnomid,isnull(SUM(cast(custcreditplafond as int)),0) as totalplafond
from tbl_CustomerFacility2 
 where custnomid='$custnomid'
group by custnomid";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$proyeksi_neraca_hutang=$rows['totalplafond'];
	}
}
sqlsrv_free_stmt( $execsql );
*/

///proyeksi_neraca_hutangjkpendek
$proyeksi_neraca_hutangjkpendek=$Neraca_hutangjkpendek+($angsuranbaru*12);


//proyeksi_neraca_hutangjkpanjang
$outstanding=0;
$strsql="
select isnull(sum(a),0) as outstanding,custnomid from ( 
select isnull(sum(cast(FasilitasPinjamanBankLain_outstanding as DEC)),0) as a,custnomid 
from Tbl_LKCDFasilitasPinjamanBankLain2 where FasilitasPinjamanBankLain_type<>'TO' 
group by custnomid 
union 
select isnull(sum(cast(FasilitasPinjamanBankMega_outstanding as DEC)),0) as a,custnomid 
from Tbl_LKCDFasilitasPinjamanBankMega2 group by custnomid 
union
 select isnull(sum(cast(custcreditplafond as DEC)),0) as a,custnomid 
 from tbl_CustomerFacility2
 group by custnomid
  )
 tblxx where custnomid='$custnomid' group by custnomid ";
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$outstanding=$rows['outstanding'];
	}
}
sqlsrv_free_stmt( $execsql );
//echo $outstanding;
$proyeksi_neraca_hutangjkpanjang=$outstanding-$proyeksi_neraca_hutangjkpendek;


///Neraca_totalhutanglancar
$proyeksi_neraca_totalhutanglancar=$proyeksi_neraca_hutangjkpendek+$proyeksi_neraca_hutang;

///proyeksi_neraca_totalhutang
$proyeksi_neraca_totalhutang=$proyeksi_neraca_totalhutanglancar+$proyeksi_neraca_hutangjkpanjang;

////proyeksi_neraca_labaditahan
$proyeksi_neraca_labaditahan=$proyeksi_neraca_totalaktiva-$proyeksi_neraca_totalhutang;


//proyeksi_neraca_totalhutangdanmodal
$proyeksi_neraca_totalhutangdanmodal=$proyeksi_neraca_totalhutang+$proyeksi_neraca_labaditahan;





?>