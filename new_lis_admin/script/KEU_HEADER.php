<?
include("../../new_lis/lib/sqlsrv.lis.php");
$db = new DB_LIS();
$db->connect();

$sql = "select * from KEU_HEADER";
$db->executeQuery($sql);
$result = $db->lastResults;

?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>PARAMETER</title>
</head>

<body>
<form method="post" action="KEU_HEADER_process.php">
<table align="center" width="960" border="0" style="">
	<tr>
		<td align="center"><img src="../images/Header Mega (L).png" width="200px"></td>
	</tr>
	<tr>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><h3>HEADER</h3></td>
	</tr>
</table>

<table border="0" align="center" width="640px" cellpadding="0" cellspacing="0">
	<tr>		
		<td><button type="reset" onClick="func_new()">New</button></td>
		<td width="1px">&nbsp;</td>
		
		<td style="text-align:right">Code</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td><input type="text" id="code" name="code" value="" style="width:50px;"></td>
		<td width="1px">&nbsp;</td>
		
		<td style="text-align:right">Name</td>
		<td width="1px">&nbsp;:&nbsp;</td>
		<td width="30%"><input type="text" id="name" name="name" value="" style="width:90%"></td>
		<td width="1px">&nbsp;</td>
		
		<td width="30%"><button id="button_form" name="button_form" value="add" type="submit">Add</button></td>
	</tr>
</table>
<br>
<table style="background-color:#FFF;" width="640px"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
	<tr>
		<th>Code</th>
		<th>Name</th>
		<th>Action</th>
	</tr>
	
	<?
	for($x=0;$x<count($result);$x++)
	{
		$code = $result[$x]['_code'];
		$show = $result[$x]['_name'];
	?>
	
	<tr>
	<td><?=$code;?></td>
	<td><?=$show;?></td>
	<td align="center"><button type="button" onClick="func_edit('<?=$code;?>','<?=$show;?>')">Edit</button> | <button type="button" onClick="func_delete('<?=$code;?>','<?=$show;?>')">Delete</button></td>
	</tr>
	
	<?
	}
	?>

</table>

</form>

<script type="text/javascript">

function func_new()
{
	document.getElementById("code").readOnly = false;
	document.getElementById("button_form").innerHTML = "Add";
	document.getElementById("button_form").value = "add";
}

function func_delete(code,name)
{
	document.getElementById("code").readOnly = true;
	document.getElementById("code").value = code;
	document.getElementById("name").value = name;
	document.getElementById("button_form").innerHTML = "Delete";
	document.getElementById("button_form").value = "delete";
}

function func_edit(code,name)
{
	document.getElementById("code").readOnly = true;
	document.getElementById("code").value = code;
	document.getElementById("name").value = name;
	document.getElementById("button_form").innerHTML = "Edit";
	document.getElementById("button_form").value = "edit";
}

</script>

</body>
</html>