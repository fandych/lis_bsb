<?	
	//require ("../../lib/formatError.php");
	//require ("../../lib/open_con.php");
	//require ("../../requirepage/parameter.php");

	$strsql="select COUNT(*) as b from GL_TxnSaldo where gl_custnomid='$custnomid'";
	//echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
		$rowscounthaha=$rows['b'];
		}
	}
	//echo $rowscounthaha;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Expires" CONTENT="0">
		<meta http-equiv="Cache-Control" CONTENT="no-cache">
		<meta http-equiv="Pragma" CONTENT="no-cache">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LKCD 3</title>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">
			<div>
				<table border="0" align="center" style="border:1px solid black; width:900px;">
					<tr>
						<td colspan="2" align="center">INFORMASI KEUANGAN (IDR)</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">MODAL KERJA</td>
								</tr>
								<?
								
								if($rowscounthaha>0)
								{
									$strsql = "select a.gl_db_cr,a.gl_code,a.gl_name,
												'dbcr'=case
												when isnull(b.gl_amount_cr,0)=0 and isnull(b.gl_amount_db,0)=0 then 0
												when isnull(b.gl_amount_cr,0)=0 then isnull(b.gl_amount_db,0)
												when isnull(b.gl_amount_db,0)=0 then isnull(b.gl_amount_cr,0) end
												from GL_Master a
												left join GL_TxnSaldo b on a.gl_code = b.gl_code
												join GL_Group c on c.group_code = left(a.gl_code,2)
											where (a.gl_code like '%AT1%' or  a.gl_code like '%AL1%' or a.gl_code like '%HT0%')
											and group_bs_is = 'bs' and gl_custnomid='".$custnomid."'" ;
								}
								else
								{
									$strsql = "select a.gl_db_cr,a.gl_code,a.gl_name,
												0 as 'dbcr'
												from GL_Master a
												left join GL_TxnSaldo b on a.gl_code = b.gl_code
												join GL_Group c on c.group_code = left(a.gl_code,2)
												where (a.gl_code like '%AT1%' or  a.gl_code like '%AL1%' or a.gl_code like '%HT0%')
												and group_bs_is = 'bs'" ;
								}
								
								
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$value=$rows['dbcr'];
										$db_cr=$rows['gl_db_cr'];
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">KAS DAN BANK</td>
								</tr>
								<?
								if($rowscounthaha>0)
								{
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,
											'dbcr'=case
											when isnull(b.gl_amount_cr,0)=0 and isnull(b.gl_amount_db,0)=0 then 0
											when isnull(b.gl_amount_cr,0)=0 then isnull(b.gl_amount_db,0)
											when isnull(b.gl_amount_db,0)=0 then isnull(b.gl_amount_cr,0) end
											from GL_Master a
											left join GL_TxnSaldo b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(a.gl_code,2)
											where (a.gl_code like '%KB0%')
											and c.group_bs_is = 'BS' and gl_custnomid='".$custnomid."'";
								}
								else
								{
								$strsql = "select a.gl_db_cr,a.gl_code,a.gl_name,
										0 as 'dbcr'
										from GL_Master a
										left join GL_TxnSaldo b on a.gl_code = b.gl_code
										join GL_Group c on c.group_code = left(a.gl_code,2)
										where (a.gl_code like '%KB0%')
										and c.group_bs_is = 'BS'";
								}
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$value=$rows['dbcr'];
										$db_cr=$rows['gl_db_cr'];
									
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">AKTIFA TETAP</td>
								</tr>
								<?
								if($rowscounthaha>0)
								{
									$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,
												'dbcr'=case
												when isnull(b.gl_amount_cr,0)=0 and isnull(b.gl_amount_db,0)=0 then 0
												when isnull(b.gl_amount_cr,0)=0 then isnull(b.gl_amount_db,0)
												when isnull(b.gl_amount_db,0)=0 then isnull(b.gl_amount_cr,0) end
												from GL_Master a
												left join GL_TxnSaldo b on a.gl_code = b.gl_code
												join GL_Group c on c.group_code = left(a.gl_code,2)
												where (a.gl_code like '%AT0%')
												and c.group_bs_is = 'BS' and gl_custnomid='".$custnomid."'";
								}
								else
								{
									$strsql = "select a.gl_db_cr,a.gl_code,a.gl_name,
										0 as 'dbcr'
										from GL_Master a
										left join GL_TxnSaldo b on a.gl_code = b.gl_code
										join GL_Group c on c.group_code = left(a.gl_code,2)
										where (a.gl_code like '%AT0%')
										and c.group_bs_is = 'BS'";
								}
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$value=$rows['dbcr'];
										$db_cr=$rows['gl_db_cr'];
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2" align="center">INCOME STATEMENT</td>
								</tr>
								<tr>
									<td colspan="2">BIAYA OPERASIONAL USAHA</td>
								</tr>
								<?
								if($rowscounthaha>0)
								{
									$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,
												'dbcr'=case
												when isnull(b.gl_amount_cr,0)=0 and isnull(b.gl_amount_db,0)=0 then 0
												when isnull(b.gl_amount_cr,0)=0 then isnull(b.gl_amount_db,0)
												when isnull(b.gl_amount_db,0)=0 then isnull(b.gl_amount_cr,0) end
												from GL_Master a
												left join GL_TxnSaldo b on a.gl_code = b.gl_code
												join GL_Group c on c.group_code = left(a.gl_code,2)
												where (a.gl_code like '%BY%') and gl_custnomid='".$custnomid."'";
								}
								else
								{
									$strsql = "select a.gl_db_cr,a.gl_code,a.gl_name,
												0 as 'dbcr'
												from GL_Master a
												left join GL_TxnSaldo b on a.gl_code = b.gl_code
												join GL_Group c on c.group_code = left(a.gl_code,2)
												where (a.gl_code like '%BY%')";
								}
								
							
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$value=$rows['dbcr'];
										$db_cr=$rows['gl_db_cr'];
									?>
										<tr>
											<td style="width:200px;">
												<?echo $rows['gl_name']?>
											</td>
											<td>
											<?
												echo numberFormat($value)."&nbsp;&nbsp;RUPIAH";
												?>
											</td>
										</tr>
									<?
									}
								}
								?>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
				
				<?
					//require ("../../requirepage/hiddenfield.php");
				?>
			</div>
		</form>
	</body>
</html>
