<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../requirepage/parameter.php");
	
	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$custfullname = "";
	
	$tsqls = "select * from tbl_customermasterperson2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{  
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$custfullname = $rows["custfullname"];
		}
	}
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../bin/bootstrap/dist/js/jquery-1.11.3.js" ></script>
<script type="text/javascript" src="../../bin/bootstrap/dist/js/bootstrap.min.js" ></script>

<script type="text/javascript" src="../../bin/js/datatable.js"></script>
<script type="text/javascript" src="../../bin/js/bootstraptable.js"></script>
<script type="text/javascript" src="../../bin/js/bootstrap-datepicker.min.js"></script>

<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
<link href="../../bin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="../../bin/javascript/niceforms.js"></script>
<link href="../../bin/css/table.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" media="screen" href="../../bin/css/styles_idr.css" >
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
			$('#example').DataTable();
			$('.tanggal').datepicker({format: 'yyyy-mm-dd',todayBtn: "linked"});
			$(".tanggal").keydown(function(e){
				e.preventDefault();
			});
		} );
	
</script>
</head>
<body class="table">
<form id="formentry" name="formentry" method="post">
<?
$tsqls = "select * from tbl_lkcdcallmemo where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{  
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$tanggal = $rows['tanggal'];
			$tanggal_call = $rows['tanggal_call'];
			$tanggal_call_sebelumnya = $rows['tanggal_call_sebelumnya'];
			$bentuk_call = $rows['bentuk_call'];
			$pejabat_melakukan_call = $rows['pejabat_melakukan_call'];
			$pejabat_di_call = $rows['pejabat_di_call'];
			$tujuan_call = $rows['tujuan_call'];
			$hasil = $rows['hasil'];
		}
	}
?>
<table width="100%" align="center"  >
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">CALL MEMO</td>
</tr>
<tr>
	<td colspan="2"></td>
</tr>
<tr>
	<td colspan="2">
	<table width="100%" align="center"  >
		<tr>
			<td colspan="2">
			<table border="1" width="95%%" align="center" id="tblpreview" cellspacing="0" >	
			<tr>
				<td colspan="4" width="66%">&nbsp;</td>
				<td colspan="2" width="34%">TANGGAL : <?php echo $tanggal;?></td>
			</tr>
			<tr>
				<td colspan="3" style="padding:5px;" width="50%">
					Nama Nasabah </br>
					<strong><?=$custfullname;?></strong>
				</td>
				<td colspan="3" style="padding:5px;" width="50%">
					<table border="0" width="90%">
					<tr>
						<td>Tanggal Call</td>
						<td>: <?php echo $tanggal_call;?></td>
					</tr>
					<tr>
						<td>Tanggal Call Sebelumnya</td>
						<td>: <?php echo $tanggal_call_sebelumnya;?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Bentuk Call :</strong> </br>
					<?=$bentuk_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang melakukan Call :</strong> </br>
					<?=$pejabat_melakukan_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang di Call :</strong> </br>
					<?=$pejabat_di_call;?>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Tujuan Call :</strong> </br>
					<?=$tujuan_call;?>
				</tr>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Hasil :</strong> </br>
					<?=$hasil;?>
				</tr>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
</table>
<?
$tsqls = "select * from tbl_lkcdcallmemo2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{  
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$tanggal = $rows['tanggal'];
			$tanggal_call = $rows['tanggal_call'];
			$tanggal_call_sebelumnya = $rows['tanggal_call_sebelumnya'];
			$bentuk_call = $rows['bentuk_call'];
			$pejabat_melakukan_call = $rows['pejabat_melakukan_call'];
			$pejabat_di_call = $rows['pejabat_di_call'];
			$tujuan_call = $rows['tujuan_call'];
			$hasil = $rows['hasil'];
		}
	}
?>
<table width="100%" align="center"  >
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">CALL MEMO</td>
</tr>
<tr>
	<td colspan="2"></td>
</tr>
<tr>
	<td colspan="2">
	<table border="1" width="95%%" align="center" id="tblpreview" cellspacing="0" >	
	<tr>
		<td colspan="4" width="66%">&nbsp;</td>
		<td colspan="2" width="34%">TANGGAL : <input type="text" id="tanggal" name="tanggal" maxlength="50" value="<?php echo $tanggal;?>" class="tanggal" <?php if($act=="v"){echo "disabled";} ?> required/></td>
	</tr>
	<tr>
		<td colspan="3" style="padding:5px;" width="50%">
			Nama Nasabah </br>
			<strong><?=$custfullname;?></strong>
		</td>
		<td colspan="3" style="padding:5px;" width="50%">
			<table border="0" width="90%">
			<tr>
				<td>Tanggal Call</td>
				<td>: <input style="align:right;" type="text" id="tanggal_call" name="tanggal_call" maxlength="50" value="<?php echo $tanggal_call;?>" class="tanggal" <?php if($act=="v"){echo "disabled";} ?> required/> </br></td>
			</tr>
			<tr>
				<td>Tanggal Call Sebelumnya</td>
				<td>: <input style="align:right;" type="text" id="tanggal_call_sebelumnya" name="tanggal_call_sebelumnya" maxlength="50" value="<?php echo $tanggal_call_sebelumnya;?>" class="tanggal" <?php if($act=="v"){echo "disabled";} ?> required/> </br></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:5px;" width="33%">
			<strong>Bentuk Call :</strong> </br>
			<textarea id="bentuk_call" name="bentuk_call" maxlength="250" style="width:100%;height:70px;" <?php if($act=="v"){echo "disabled";} ?> required ><?=$bentuk_call;?></textarea>
		</td>
		<td colspan="2" style="padding:5px;" width="33%">
			<strong>Pejabat yang melakukan Call :</strong> </br>
			<textarea id="pejabat_melakukan_call" name="pejabat_melakukan_call" maxlength="250" style="width:100%;height:70px;" <?php if($act=="v"){echo "disabled";} ?> required ><?=$pejabat_melakukan_call;?></textarea>
		</td>
		<td colspan="2" style="padding:5px;" width="33%">
			<strong>Pejabat yang di Call :</strong> </br>
			<textarea id="pejabat_di_call" name="pejabat_di_call" maxlength="250" style="width:100%;height:70px;" <?php if($act=="v"){echo "disabled";} ?> required ><?=$pejabat_di_call;?></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="6" style="padding:5px;" width="100%">
			<strong>Tujuan Call :</strong> </br>
			<textarea id="tujuan_call" name="tujuan_call" maxlength="250" style="width:100%;height:70px;" <?php if($act=="v"){echo "disabled";} ?> required ><?=$tujuan_call;?></textarea>
		</tr>
	</tr>
	<tr>
		<td colspan="6" style="padding:5px;" width="100%">
			<strong>Hasil :</strong> </br>
			<textarea id="hasil" name="hasil" maxlength="250" style="width:100%;height:120px;" <?php if($act=="v"){echo "disabled";} ?> required ><?=$hasil;?></textarea>
		</tr>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" /></td>
</tr>
	<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
	<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
	<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
	<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
	<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
	<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
	<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
	<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
</form>
<script type="text/javascript">
	function kumi() {
							
		var FormName="formentry";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax5_n.php",
				data: $('#formentry').serialize(),
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
					alert('Data berhasil disimpan');	
				}
			});
		}
		
	}
</script>
</body>
</html> 