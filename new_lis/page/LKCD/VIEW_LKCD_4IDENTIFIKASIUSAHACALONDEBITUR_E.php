<?
	//require ("../../lib/formatError.php");
	//require ("../../lib/open_con.php");
	require ("../../requirepage/parameter.php");
	

	
	$custcurcode="IDR";
	$strsql = "select * From tbl_customermasterperson where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcurcode=$rows['custcurcode'];
		}
	}

	
	
	
	$lkcdsektorindustri="";
	$lkcdjenisusaha="";
	$lkcdlamausahayear=0;
	$lkcdlamausahamonth=0;
	$lkcdkepemilikan="";
	$lkcdjumlahharikerjausaha=0;
	$lkcdjenisjumlahharikerjausaha=0;
	$lkcdratajumlahpembeli=0;
	$lkcdratapembelian=0;
	$lkcdkenaikanjumlahpembeli=0;
	$lkcdkenaikantiketsize=0;
	$lkcdjenishpp=0;
	$lkcdhpp=0;
	$lkcdmaginusaha=0;
	$lkcdgajirataperpegawai=0;
	$lkcdjumlahkaryawan=0;
	$lkcdbiayasewa=0;
	$lkcdbiayatelepon=0;
	$lkcdsampah=0;
	$lkcdoperasionallainnya=0;
	$lkcdbiayarumahtangga=0;
	$strsql="select * from Tbl_LKCDDataKeuangan where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$lkcdsektorindustri=$rows['lkcdsektorindustri'];
			$lkcdjenisusaha=$rows['lkcdjenisusaha'];
			$lkcdlamausahayear=$rows['lkcdlamausahayear'];
			$lkcdlamausahamonth=$rows['lkcdlamausahamonth'];
			$lkcdkepemilikan=$rows['lkcdkepemilikan'];
			$lkcdjumlahharikerjausaha=$rows['lkcdjumlahharikerjausaha'];
			$lkcdjenisjumlahharikerjausaha=$rows['lkcdjenisjumlahharikerjausaha'];
			$lkcdratajumlahpembeli=$rows['lkcdratajumlahpembeli'];
			$lkcdratapembelian=$rows['lkcdratapembelian'];
			$lkcdkenaikanjumlahpembeli=$rows['lkcdkenaikanjumlahpembeli'];
			$lkcdkenaikantiketsize=$rows['lkcdkenaikantiketsize'];
			$lkcdjenishpp=$rows['lkcdjenishpp'];
			$lkcdhpp=$rows['lkcdhpp'];
			$lkcdmaginusaha=$rows['lkcdmaginusaha'];
			$lkcdgajirataperpegawai=$rows['lkcdgajirataperpegawai'];
			$lkcdjumlahkaryawan=$rows['lkcdjumlahkaryawan'];
			$lkcdbiayasewa=$rows['lkcdbiayasewa'];
			$lkcdbiayatelepon=$rows['lkcdbiayatelepon'];
			$lkcdsampah=$rows['lkcdsampah'];
			$lkcdoperasionallainnya=$rows['lkcdoperasionallainnya'];
			$lkcdbiayarumahtangga=$rows['lkcdbiayarumahtangga'];
		}
	}
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">	
			<table align="center" style="border:1px solid black;background-color:#F2F6F8; width:900px;" border="0">
				<tr>
					<td colspan="4" style="font-size:12pt;font-weight:bold;">
						DATA KEUANGAN
						<div>&nbsp;</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="font-weight:bold;">Segmentasi</td>
				</tr>
				<tr>
					<td>Sektor Industri</td>
					<td colspan="3">
							<?
							$strsql = "select * From Tbl_SektorIndustri where sektor_code='$lkcdsektorindustri'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['sektor_nama'];
								
								}
							}
							?>
					</td>
				</tr>
				<tr>
					<td>Jenis Usaha</td>
					<td colspan="3"><?echo $lkcdjenisusaha; ?></td>
				</tr>
				<tr>
					<td>Lama usaha</td>
					<td colspan="3">
						<? echo $lkcdlamausahayear; ?>  Tahun
						<? echo $lkcdlamausahamonth; ?>  Bulan
					</td>
				</tr>
				<tr>
					<td>Kepemilikan</td>
					<td colspan="3">
							<?
							$strsql = "select * From tblstatusrumah where status_code='$lkcdkepemilikan'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['status_name'];
								}
							}
							?>
					</td>
				</tr>
				<tr>
					<td style="width:45%;">&nbsp;</td>
					<td style="width:3%;">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="4" style="font-weight:bold;">IDENTIFIKASI DATA - DATA KEUANGAN CALON DEBITUR</td>
				</tr>
				<tr>
					<td>Jumlah hari kerja usaha/ jumlah hari penjualan</td>
					<td colspan="3">
						<?echo $lkcdjumlahharikerjausaha?> kali dalam 1 
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							$opt3="";
							if ($lkcdjenisjumlahharikerjausaha=="1")
							{
								echo 'Minggu';
							}
							else if($lkcdjenisjumlahharikerjausaha=="2")
							{
								echo 'Bulan';
							}
							else if($lkcdjenisjumlahharikerjausaha=="3")
							{
								echo 'Tahun';
							}
						?>
					</td>
				</tr>
				<tr>
					<td>Rata-rata jumlah pembeli/pemesanan per-penjualan</td>
					<td colspan="3"><?echo $lkcdratajumlahpembeli?>  CUSTOMER</td>
				</tr>
				<tr>
					<td>Rata-rata pembelian/belanja per-customer/Ticket size</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdratapembelian)?></td>
				</tr>
				<tr>
					<td colspan="4" style="font-weight:bold;">
						<div>&nbsp;<div>
						Estimasi Proyeksi Omset penjualan usaha
					</td>
				</tr>
				<tr>
					<td>Kenaikan jumlah pembeli/pemesan</td>
					<td colspan="2"><?echo $lkcdkenaikanjumlahpembeli.'&nbsp;%'?></td>
				</tr>
				<tr>
					<td>Kenaikan Ticket size</td>
					<td colspan="3"><?echo $lkcdkenaikantiketsize.'&nbsp;%'?></td>
				</tr>
				<tr>
					<td>Harga pokok penjualan</td>
					
						<?
						
							if ($lkcdjenishpp=="1")
							{
								echo '
								<td>'.$custcurcode.'</td>
								<td colspan="2">'.numberFormat($lkcdhpp).'</td>
								';
							}
							else if($lkcdjenishpp=="2")
							{
								echo '
								<td colspan="3">'.$lkcdhpp.'&nbsp;%</td>
								';
							}
						?>
					
				</tr>
				<tr>
					<td style="font-weight:bold;">
						Persentase Margin Usaha
					</td>
					<td colspan="3"><? echo numberFormat($lkcdmaginusaha).'&nbsp;%'?></td>
				</tr>
				<tr>
					<td colspan="4" style="font-weight:bold;">
						<div>&nbsp;<div>
						Biaya Operasional Usaha
					</td>
				</tr>
				<tr>
					<td>Biaya Gaji Pegawai:</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2">
						<? echo numberFormat($lkcdgajirataperpegawai*$lkcdjumlahkaryawan)?>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp; Gaji rata-rata per-pegawai</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdgajirataperpegawai)?></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp; Jumlah Karyawan</td>
					<td><?echo 'orang'?></td>
					<td colspan="2"><? echo numberFormat($lkcdjumlahkaryawan)?></td>
				</tr>
				<tr>
					<td>Biaya Sewa/Kontrak</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdbiayasewa)?></td>
				</tr>
				<tr>
					<td>Biaya Telepon, Listrik, Air</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdbiayatelepon)?></td>
				</tr>
				<tr>
					<td>Biaya Sampah, Keamanan, Transportasi</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdsampah)?></td>
				</tr>
				<tr>
					<td>Biaya Operasional Lainnya</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdoperasionallainnya)?></td>
				</tr>
				<tr>
					<td colspan="2" style="font-weight:bold;">
					<div>&nbsp;<div>
						Biaya Lainnya
					</td>
				</tr>
				<tr>
					<td>Biaya rumah tangga</td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><?echo numberFormat($lkcdbiayarumahtangga)?></td>
				</tr>
			</table>
			<div>&nbsp;
			
				<?require ("../../requirepage/hiddenfield.php");?>
			</div>
		</form>
	</body>
</html>