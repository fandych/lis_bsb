<?
	require ("../../lib/open_conCOLL.php");
	require ("../../lib/formatError.php");
	//require ("../../requirepage/parameter.php");
	//require ("../../requirepage/security.php");
   
    
    
    
    $getyearquery = "select distinct(YEAR(_cust_tglpinjam)) as'year' from collect_custmaster";
    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $getyearquery, $params, $cursorType);
    $rowsyear = sqlsrv_num_rows($sqlConn);
    $rowcolspan=$rowsyear+1;
    /*
    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
    if(sqlsrv_has_rows($sqlConn))
    {
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
        {
            $lancar2015 = number_format($row['lancar']);
        }
    }
    */
?>
<html>
	<head>
		<title>APPRAISAL</title>
		
		<script type="text/javascript" src="../../bootstrap/dist/js/jquery-1.11.3.js" ></script>
		<script type="text/javascript" src="../../bootstrap/dist/js/bootstrap.min.js" ></script>
        
        <script type="text/javascript" src="../../js/datatable.js"></script>
        <script type="text/javascript" src="../../js/bootstraptable.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datepicker.min.js"></script>
        
		<link href="../../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
        <link rel="stylesheet" href="../../bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
        
		<link href="../../css/table.css" rel="stylesheet" type="text/css" />
        
		<script type="text/javascript">
        </script>
        <style>
            table tr
            {
                border-bottom:1px solid black;
                line-height:30px;
            }
            .center
            {
                
                text-align:center;
            }
        </style>
    </head>        
	<body>
        <form id="formcheker" method="get" action="" enctype="multipart/form-data">
            <table border="0" align="center" style="width:1000px;margin-top:30px;">
                <tr style="border-bottom:1px solid black;">
                    <td colspan="<?=$rowcolspan?>" style="text-align:center;">
                        Kualitas Kredit Bank Perkreditan Rakyat secara Nasional</br>
                        (tahun 2011-2014)
                    </td>
                </tr>
                <tr>
                    <td rowspan="3" style="width:500px">Kualitas Kredit</td>
                    <td colspan="<?=$rowsyear?>" style="text-align:center;">Desember</td>
                </tr>
                <tr>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            echo "<td class=\"center\">".$row['year']."</td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            echo "<td class=\"center\">IDR</td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Kualitas Lancar</td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id='L'";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Pertumbuhan Kualitas Lancar</td>
                    <td class="center">1</td>
                    <td class="center">2</td>
                </tr>
                <tr>
                    <td colspan="<?=$rowcolspan?>" style="font-style:italic">Non Perfoming loan :</td>
                </tr>
                <tr>
                    <td>Kualitas Kurang lancar</td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id='COL1'";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Kualitas Diragukan</td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id in ('COL2','COL3') ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Kualitas Macet</td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id in ('M') ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Total <span style="font-style: italic;">Non Perfoming Loans<span></td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id not in ('L') ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td colspan="<?=$rowcolspan?>">Pertumbuhan <span style="font-style: italic;">Non Perfoming Loans<span></td>
                </tr>
                <tr>
                    <td>Total Kredit</td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),0) as 'lancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    echo "<td class=\"center\">".number_format($row2['lancar'])."</td>";
                                }
                            }
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Pertumbuhan Kredit</td>
                    <td class="center">1</td>
                    <td class="center">2</td>
                </tr>
                <tr>
                    <td>Rasio <span style="font-style: italic;">Non Perfoming Loans<span></td>
                    <?
                    $tsql = $getyearquery;
                    $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                    $params = array(&$_POST['query']);
                    $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                    $rows = sqlsrv_num_rows($sqlConn);
                    if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                    if(sqlsrv_has_rows($sqlConn))
                    {
                        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                        {
                            $bukanlancar="";
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),1) as 'bukanlancar'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            and a._cust_kolektibilitas_id not in ('L') ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    $bukanlancar=$row2['bukanlancar'];
                                }
                            }
                            
                            
                            $strsql2 = "   select isnull(SUM(b._txn_nominal),1) as 'total'
                                            from collect_custmaster a
                                            join collect_custtxn b on b._custnomid = a._custnomid
                                            where year(a._cust_tglpinjam)='".$row['year']."'
                                            and b._txn_statusbayar='N'
                                            ";
                            $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                            $params2 = array(&$_POST['query']);
                            $sqlConn2 = sqlsrv_query($conn, $strsql2, $params2, $cursorType2);
                            $totalrow2s = sqlsrv_num_rows($sqlConn2);

                            if ( $sqlConn2 === false)die( FormatErrors( sqlsrv_errors() ) );
                            if(sqlsrv_has_rows($sqlConn2))
                            {
                                while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
                                {
                                    
                                    $total=$row2['total'];
                                }
                            }
                            $npl = $bukanlancar/$total*100;
                            echo "<td class=\"center\">".round($npl,2)."%</td>";
                        }
                    }
                    ?>
                </tr>
            </table>
            <?
                require ("../../requirepage/hiddenfield.php");
            ?>
            
        </form>
	</body>
</html>