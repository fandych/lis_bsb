<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");

//ini_set("display_error",0);

set_time_limit(100);

//DECLARE VARIABLES
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
$userwfid=$_REQUEST['userwfid'];
//$userpermission=$_REQUEST['userpermission'];
//$buttonaction=$_REQUEST['buttonaction'];
//$custnomid=$_REQUEST['custnomid'];


/*
echo "userid ".$userid;
echo "<br>";
echo "userpwd ".$userpwd;
echo "<br>";
echo "userbranch ".$userbranch;
echo "<br>";
echo "userregion ".$userregion;
echo "<br>";
echo "userwfid ".$userwfid;
echo "<br>";
echo "userpermission ".$userpermission;
echo "<br>";
echo "buttonaction ".$buttonaction;
echo "<br>";
echo "custnomid ".$custnomid;
*/

//SECURITY PROFILE AND WORKFLOW SETTINGS

if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	echo "<br>";
	//echo "general security";
	//header("location:restricted.php");
}

// SECURITY
   $tsql = "SELECT COUNT(*) FROM Tbl_SE_User
   					WHERE user_id='$userid'
   					AND user_pwd='$userpwd'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $thecount = $row[0];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   if ($thecount == 0)
   {
	   echo "<br>";
	   //echo "security user password";
   	   //header("location:restricted.php");
   }

   $tsql = "SELECT COUNT(*) FROM Tbl_SE_UserProgram
   					WHERE user_id='$userid'
   					AND program_code='$userwfid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $thecount = $row[0];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   if ($thecount == 0)
   {
	   echo "<br>";
	   //echo "security user program";
   	   //header("location:restricted.php");
   }

   $tsql = "SELECT * FROM Tbl_SE_UserProgram
   					WHERE user_id='$userid'
   					AND program_code='$userwfid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   $userpinp = "";
   $userpchk = "";
   $userpapr = "";
   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
		$userpinp = substr($row[2],1-1,1);
		$userpchk = substr($row[2],2-1,1);
		$userpapr = substr($row[2],3-1,1);
      }
   }
   sqlsrv_free_stmt( $sqlConn );

// END SECURITY
// PROFILE USER ID (AO / TL / PINCA)
   $tsql = "SELECT user_ao_code, user_level_code, user_child FROM Tbl_SE_User
   					WHERE user_id='$userid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $profileaocode = $row[0];
   		   $profilelevelcode = $row[1];
   		   $profileuserchild = $row[2];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   
//END PROFILE

// BRANCH DETAIL
   $tsql = "SELECT branch_flag_bi, branch_flag_apr, branch_flag_leg, branch_flag_dd
   					FROM Tbl_Branch
   					WHERE branch_code='$userbranch'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $branchflagbi = $row[0];
   		   $branchflagapf = $row[1];
   		   $branchflagleg = $row[2];
   		   $branchflagdd = $row[3];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
// END BRANCH

//SELECT From Tbl_Workflow & PrevFlow
    $tsql = "select * from Tbl_Workflow where wf_id='$userwfid'";
   	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   	$params = array(&$_POST['query']);
   	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if($sqlConn === false)
	{
		die(FormatErrors(sqlsrv_errors()));
	}
	
	if(sqlsrv_has_rows($sqlConn))
	{
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $wfname = $row[1];
   		   $wftime = $row[3];
   		   $wfscore = $row[4];
   		   $wfaction = $row[5];
   		   $wfflag = $row[7];
      }
   }
   sqlsrv_free_stmt( $sqlConn );

   // FLAG BI CHECK / APR / LEG / DD
   $kondisibranch = "";
   $kondisiflag5 = "";
   $aksesflag5 = "";
   $selectflag5 = "";
   switch (substr($wfflag,5-1,1))
   {
      // 0 = BI Check
   	  case "0" : 
   	  	$kondisiflag5  = "0"; 
   			$aksesflag5 = $branchflagbi;
   			$selectflag5 = "branch_flag_bi";
   	  	break;
      // 1 = APRAISAL Check
   	  case "1" : 
   	  	$kondisiflag5  = "1"; 
   			$aksesflag5 = $branchflagapr;
   			$selectflag5 = "branch_flag_apr";
   	  	break;
      // 2 = LEGAL
   	  case "2" : 
   	  	$kondisiflag5  = "2"; 
   			$aksesflag5 = $branchflagleg;
   			$selectflag5 = "branch_flag_leg";
   	  	break;
      // 3 = DRAWDAWN
   	  case "3" : 
   	  	$kondisiflag5  = "3"; 
   			$aksesflag5 = $branchflagdd;
   			$selectflag5 = "branch_flag_dd";
   	  	break;
   }

   if ($kondisibranch == "")
   {
   	  if ($userregion != $userbranch)
   	  {
   	  	if ($kondisiflag5 != "")
   	  	{
   	     $kondisibranch = "AND (txn_branch_code='$userbranch' or txn_region_code='$userregion' or txn_region_code='888'";

   	  	if ($kondisiflag5 != "")
   	  	{

    		 $tsql = "select branch_code from Tbl_Branch where $selectflag5='".$userbranch."'";
   			 $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			 $params = array(&$_POST['query']);
   			 $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				 if($sqlConn === false)
				 {
					 die(FormatErrors(sqlsrv_errors()));
				 }
	
				 if(sqlsrv_has_rows($sqlConn))
				 {
      		 $rowCount = sqlsrv_num_rows($sqlConn);
      		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      		 {
   		   		 $kondisibranch .= " or txn_branch_code='$row[0]' ";
      		 }
   			 }
   			 sqlsrv_free_stmt( $sqlConn );
   	  	}
   		   $kondisibranch .= ")";

   	    }
   	    else
   	    {
   	     $kondisibranch = "AND (txn_branch_code='$userbranch' or txn_region_code='$userregion' or txn_region_code='888'";

				  if ($selectflag5 != "")
				  {
    		 $tsql = "select branch_code from Tbl_Branch where $selecflag5='".$userbranch."'";
   			 $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			 $params = array(&$_POST['query']);
   			 $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				 if($sqlConn === false)
				 {
					 die(FormatErrors(sqlsrv_errors()));
				 }
	
				 if(sqlsrv_has_rows($sqlConn))
				 {
      		 $rowCount = sqlsrv_num_rows($sqlConn);
      		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      		 {
   		   		 $kondisibranch .= " or txn_branch_code='$row[0]' ";
      		 }
   			 }
   			 sqlsrv_free_stmt( $sqlConn );
				  }
   		   $kondisibranch .= ")";
   	    }

   	  }
   	  else
   	  {
   	     $kondisibranch = "AND (txn_branch_code='$userbranch' or txn_region_code='$userregion' or txn_region_code='888' ";

   	  	if ($kondisiflag5 != "")
   	  	{
    		 $tsql = "select branch_code from Tbl_Branch where $selectflag5='".$userregion."'";
   			 $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			 $params = array(&$_POST['query']);
   			 $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				 if($sqlConn === false)
				 {
					 die(FormatErrors(sqlsrv_errors()));
				 }
	
				 if(sqlsrv_has_rows($sqlConn))
				 {
      		 $rowCount = sqlsrv_num_rows($sqlConn);
      		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      		 {
   		   		 $kondisibranch .= " or txn_branch_code='$row[0]' ";
      		 }
   			 }
   			 sqlsrv_free_stmt( $sqlConn );
   	  	}
   		   $kondisibranch .= ")";

   	  }
   }
      

//GET PARAM BRANCH

$tsql = "select * from Tbl_branch";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
  $rowCount = sqlsrv_num_rows($sqlConn);
  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
  {
	   $param_branch[$row[0]]=$row[1];
  }
}
sqlsrv_free_stmt( $sqlConn );

//print_r($param_branch);
//END GET PARAM BRANCH

//GET PREV FLOW DRQ
$tsql = "select * from tbl_prevflow where flow = 'LOOP'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
  $rowCount = sqlsrv_num_rows($sqlConn);
  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
  {
	   $prevFlow = $row[1];
  }
}
sqlsrv_free_stmt( $sqlConn );

//echo $prevFlow."<br>";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DRAWDOWN_REQ</title>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<link href="../../css/d.css" rel="stylesheet" type="text/css" />
<script language="javascript">
function reqDrawdown()
{
	document.formrequest.action = "do_drawdownrequest.php";
	//alert(document.formrequest.action);
	var elLength = document.formrequest.elements.length;

	//alert(elLength);
	var result = "";

    for (i=0; i<elLength; i++)
    {
        var type = formrequest.elements[i].type;
        if (type=="checkbox" && formrequest.elements[i].checked)
		{
            //alert("Form element in position " + i + " is of type checkbox and is checked.");
			//alert(formrequest.elements[i].id);
			temp = formrequest.elements[i].value;
			result = result + temp + "|" ;
        }
        else if (type=="checkbox") 
		{
            //alert("Form element in position " + i + " is of type checkbox and is not checked.");
        }
        else 
		{
        }
    }
	
	//alert(result);
	document.formrequest.drawdown_request_list.value = result;
	//alert(document.formrequest.drawdown_request_list.value);
	document.formrequest.submit();
	
}

function remakeTextFile(remakeNomID)
{
	document.formrequest.action = "do_remaketextfile.php";
	document.formrequest.drawdown_remake.value = remakeNomID;
	document.formrequest.submit();
	//alert(document.formrequest.action);
	//alert(remakeNomID);
	
}

</script>
</head>

<body>
<form name="formrequest" id="formrequest" action="" method="post">
<div align="center">
  <div align="center">
    <table width="800" border="0" >
      <tr>
        <td>&nbsp;</td>
        <td><div class="judul">
          <div align="center">DRAWDOWN PICK LIST</div>
        </div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="800" border="1" cellspacing="1" cellpadding="1">
          <tr>
            <th scope="col">ID</th>
            <th scope="col">CALON DEBITUR</th>
            <th scope="col">CABANG</th>
            <th scope="col">DETAIL</th>
            <th scope="col">ACTION</th>
          </tr>
          
<?

$tsql = "select * from tbl_customerFlag where custflaghost = 'V'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);

$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if ( $sqlConn === false)
die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	{
		$SaveCustNomId[$row[0]] = 0;
	}
}
sqlsrv_free_stmt( $sqlConn );

//echo $prevFlow;

$ArrFlow = explode("|",$prevFlow);

//echo "TOTAL PREV FLOW = ".count($ArrFlow)."<br>";

for($x=0;$x<count($ArrFlow);$x++)
{
//GET CUSTNOMID LIST WHICH READY TO DRAWDOWN
$tsql = "select * from Tbl_CustomerFlag CF left join Tbl_F$ArrFlow[$x] CV on CF.custnomid = CV.txn_id where CV.txn_action = 'A' and CF.custflaghost = 'V' $kondisibranch";
//echo $tsql;
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
$rowCount = sqlsrv_num_rows($sqlConn);
while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
{
//echo $row[0]."<br>";
$SaveCustNomId[$row[0]] = $SaveCustNomId[$row[0]] + 1;

//echo $row[0]." = ".$SaveCustNomId[$row[0]]."<br>";

if($SaveCustNomId[$row[0]] == count($ArrFlow) )
{
	
?>
<tr>
<td><? echo $row[0]; ?></td>
<?
//GET INFO CUSTOMER
$tsql_cust = "select * from tbl_CustomerMasterPerson2 where custnomid = '$row[0]'";
echo $tsql_cust."<br>";
$cursorType_cust = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cust = array(&$_POST['query']);
$sqlConn_cust = sqlsrv_query($conn, $tsql_cust, $params_cust, $cursorType_cust);

if($sqlConn_cust === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn_cust))
{
	$rowCount_cust = sqlsrv_num_rows($sqlConn_cust);
	while( $row_cust = sqlsrv_fetch_array( $sqlConn_cust, SQLSRV_FETCH_BOTH))
	{
		
		if($row_cust['custsex'] == "0")
		{
			$infoCust_Name = $row_cust['custbusname']." (Badan Usaha)";
		}
		else
		{
			$infoCust_Name = $row_cust['custfullname']." (Perorangan)";
		}
		
		$infoCust_Branch = $param_branch[$row_cust[5]];
	}
}
sqlsrv_free_stmt( $sqlConn_cust );
?>            
<td><? echo $infoCust_Name; ?>&nbsp;</td>
<td><? echo $infoCust_Branch; ?>&nbsp;</td>
<td><a href="../Preview/PreviewAll/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $row[0]; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL" target="_blank" >Click Here</a>&nbsp;</td>
<td><div align="center"><input type="checkbox" name="checkbox_action" id="checkbox_<? echo $row[0]?>" value="<? echo $row[0]?>" /></div></td>

<?
}
?>
</tr>
<?
}
}
sqlsrv_free_stmt( $sqlConn );
}
?>

          
        </table>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><div align="center">
          <input type="hidden" name="userid"  value='<? echo $userid; ?>'/>
          <input type="hidden" name="userpwd" value='<? echo $userpwd; ?>'/>
          <input type="hidden" name="userbranch" value='<? echo $userbranch; ?>'/>
          <input type="hidden" name="userregion" value='<? echo $userregion; ?>'/>
          <input type="hidden" name="userpermission" value='<? echo $userpermission; ?>' />
          <input type="hidden" name="buttonaction" value='<? echo $buttonaction; ?>'/>
          <input type="hidden" name="userwfid"  value='<? echo $userwfid; ?>'/>
          <input type="hidden" id="drawdown_remake" name="drawdown_remake" value="" />
          <input type="hidden" name="drawdown_request_list" id="drawdown_request_list" value="" />
          <input type="button" name="request" id="request" class="blue" value="Request" onclick="reqDrawdown()"/>
        </div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><!--
        <div class="judul">
          <div align="center">DRAWDOWN SCHEDULED</div>
        </div>!-->
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
        <!--<table width="800" border="1" cellspacing="1" cellpadding="1">
          <tr>
            <th scope="col">ID</th>
            <th scope="col">CALON DEBITUR</th>
            <th scope="col">CABANG</th>
            <th scope="col">DETAIL</th>
            </tr>
<?
//GET CUSTNOMID LIST WHICH SCHEDULED

$tsql = "select * from tbl_customerflag where custflaghost='O'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
if($sqlConn === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	{
		
		?>
        <tr>
        <td><? echo $row[0];?>&nbsp;</td>

<?    
//GET INFO CUSTOMER
$tsql_cust = "select * from tbl_CustomerMasterPerson where custnomid = '$row[0]'";
echo $tsql_cust."<br>";
$cursorType_cust = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cust = array(&$_POST['query']);
$sqlConn_cust = sqlsrv_query($conn, $tsql_cust, $params_cust, $cursorType_cust);

if($sqlConn_cust === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn_cust))
{
	$rowCount_cust = sqlsrv_num_rows($sqlConn_cust);
	while( $row_cust = sqlsrv_fetch_array( $sqlConn_cust, SQLSRV_FETCH_BOTH))
	{
		if($row_cust['custsex'] == "0")
		{
			$infoCust_Name = $row_cust['custbusname']." (Badan Usaha)";
		}
		else
		{
			$infoCust_Name = $row_cust['custfullname']." (Perorangan)";
		}		
		
		$infoCust_Branch = $param_branch[$row_cust[5]];
	}
}
sqlsrv_free_stmt( $sqlConn_cust );
?>
        
        <td><? echo $infoCust_Name ?>&nbsp;</td>
        <td><? echo $infoCust_Branch ?>&nbsp;</td>
        <td><a href="../Preview/PreviewAll/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $row[0]; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL" target="_blank" >Click Here</a>&nbsp;</td>
        </tr>
        <?
	}
}
sqlsrv_free_stmt( $sqlConn );
//END GET CUSTNOMID LIST WHICH SCHEDULED
?>
          
        </table>
        !-->
        
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><div class="judul" align="center">DRAWDOWN STATUS</div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="800" border="1" cellspacing="1" cellpadding="1">
          <tr>
            <th scope="col">ID</th>
            <th scope="col">CALON DEBITUR</th>
            <th scope="col">CABANG</th>
            <th scope="col">DETAIL</th>
            <th scope="col">STATUS</th>
            <th scope="col">ACTION</th>
          </tr>
<?

//GET CUSTNOMID LIST WHICH TEXTFILE READY

$tsql = "select * from tbl_customerflag cf right join tbl_flove flove on cf.custnomid = flove.txn_id where custflaghost = 'T' or custflaghost = 'F' or custflaghost = 'S' $kondisibranch";//echo $tsql;
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
if($sqlConn === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	{
	?>
	<tr>
	<td><? echo $row[0];?>&nbsp;</td>
    
<?
$tsql_cust = "select * from tbl_CustomerMasterPerson where custnomid = '$row[0]'";
//echo $tsql_cust."<br>";
$cursorType_cust = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cust = array(&$_POST['query']);
$sqlConn_cust = sqlsrv_query($conn, $tsql_cust, $params_cust, $cursorType_cust);

if($sqlConn_cust === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn_cust))
{
	$rowCount_cust = sqlsrv_num_rows($sqlConn_cust);
	while( $row_cust = sqlsrv_fetch_array( $sqlConn_cust, SQLSRV_FETCH_BOTH))
	{
		if($row_cust['custsex'] == "0")
		{
			$infoCust_Name = $row_cust['custbusname']." (Badan Usaha)";
		}
		else
		{
			$infoCust_Name = $row_cust['custfullname']." (Perorangan)";
		}		
		
		$infoCust_Branch = $param_branch[$row_cust[5]];
	}
}
sqlsrv_free_stmt( $sqlConn_cust );
?>  
    
	<td><? echo $infoCust_Name ?>&nbsp;</td>
    <td><? echo $infoCust_Branch ?>&nbsp;</td>
    <td><a href="../Preview/PreviewAll/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $row[0]; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL" target="_blank" >Click Here</a>&nbsp;</td>
    <?
    switch($row[3])
	{
		case "T" :
		$StatusDrawdown = "Text File Created<td>&nbsp;</td>";
		break;
		
		case "F" :
		$StatusDrawdown = "Text File Failed<td><input type='button' class='orange' id='btn_remake' name='button_remake' onclick=remakeTextFile('$row[0]') value='Recreate'></td>";
		break;
		
		case "S" :
		$StatusDrawdown = "Send to Host<td>&nbsp;</td>";
		break;
	}
	?>
    <td><? echo $StatusDrawdown;?>&nbsp;</td>
	</tr>
	<?
	}
}
sqlsrv_free_stmt( $sqlConn );

?>          
          
          
        </table></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><div align="center">END</div></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
</div>
</form>
</body>
</html>
<center>
<?
//DI SINI JALANIN EXE INDRA
echo exec('run.bat');
//SELESAI

include ("new_drawdown_request.php");

?>
</center>