<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	require ("../../requirepage/currency.php");
	
	$InfoFasilitasPinjamanBankLain_cb1 = "";
	$InfoFasilitasPinjamanBankLain_cb2 = "";
	$InfoFasilitasPinjamanBankLain_cb3 = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankLain2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankLain_cb1 = $row["InfoFasilitasPinjamanBankLain_cb1"];
			$InfoFasilitasPinjamanBankLain_cb2 = $row["InfoFasilitasPinjamanBankLain_cb2"];
			$InfoFasilitasPinjamanBankLain_cb3 = $row["InfoFasilitasPinjamanBankLain_cb3"];
			
		}
	}
	
	$InfoFasilitasPinjamanBankLain_cb1ORI = "";
	$InfoFasilitasPinjamanBankLain_cb2ORI = "";
	$InfoFasilitasPinjamanBankLain_cb3ORI = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankLain where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankLain_cb1ORI = $row["InfoFasilitasPinjamanBankLain_cb1"];
			$InfoFasilitasPinjamanBankLain_cb2ORI = $row["InfoFasilitasPinjamanBankLain_cb2"];
			$InfoFasilitasPinjamanBankLain_cb3ORI = $row["InfoFasilitasPinjamanBankLain_cb3"];
			
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<table width="100%" align="center" style="border:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK LAIN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah Debitur memiliki fasilitas pinjaman di Bank lain?</td>
	<td colspan="1">
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankLain_cb1ORI;?></div>
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankLain_cb1;?></div>
	</td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>A. FASILITAS PINJAMAN TETAP  <i>(tidak memperhitungkan fasilitas yang akan di take over)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentryA1" name="formentryA1" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis FASILITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>OUTSTANDING (<? echo $currency; ?>)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>ANGSURAN (<? echo $currency; ?>)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";
			
				?>
		<?
			
			
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'PT'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				
				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{	
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}
		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_outstanding)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_angsuran)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		</form>
		<form id="formeditA1" name="formeditA1" method="post">
		
		<?
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain2 where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'PT'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="FasilitasPinjamanBankLain_jenisfasilitasAdd<? echo $FasilitasPinjamanBankLain_seq;?>" name="FasilitasPinjamanBankLain_jenisfasilitasAdd<? echo $FasilitasPinjamanBankLain_seq;?>"  nai="Jenis Fasilitas " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($FasilitasPinjamanBankLain_jenisfasilitas == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasBankLain_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasBankLain_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_namabank;?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_outstanding);?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_angsuran);?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $FasilitasPinjamanBankLain_kol;?>
			
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_dpd;?></td>			
			<td width="25%">&nbsp;</td>
		</tr>

		<?
				}
			}
		?>
		
		</table>
		</form>
	
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1">
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankLain_cb2ORI;?></div>
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankLain_cb2;?></div>
	</td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA1col" name="formentryA1col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:500px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr>
			&nbsp;<!--<td colspan="4" align="center"><input type="button" value="SAVE" style="width:100px;background-color:blue;color:white;" onclick="nonA1()" /></td>-->
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA1col" name="formentryA1col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:500px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr>
			&nbsp;<!--<td colspan="4" align="center"><input type="button" value="SAVE" style="width:100px;background-color:blue;color:white;" onclick="nonA1()" /></td>-->
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2"><strong>B. FASILITAS PINJAMAN YANG AKAN DI TAKE OVER (Jika ada)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentryA2" name="formentryA2" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis FASILITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>OUTSTANDING (<? echo $currency; ?>)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>ANGSURAN (<? echo $currency; ?>)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
					$FasilitasPinjamanBankLain_seq2 = "";
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";
			
		?>
		<?
			
			
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'TO'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq2 = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				
				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{	
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_outstanding?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_angsuran)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
			
		</tr>

		<?
				}
			}
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$FasilitasPinjamanBankLain_seq2 = "";
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";
			
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="FasilitasPinjamanBankLain_jenisfasilitasAdd2" name="FasilitasPinjamanBankLain_jenisfasilitasAdd2"  nai="Jenis Fasilitas " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($FasilitasPinjamanBankLain_jenisfasilitas == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasBankLain_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasBankLain_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_namabankAdd2" nai="Nama Bank " style="width:120px;background:#ff0;" value="" maxlength="100"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_outstandingAdd2" nai="Outstanding " style="width:120px;background:#ff0;" value="" maxlength="20" onKeyPress="return isNumberKey(event)" onKeyUp="outputMoney('FasilitasPinjamanBankLain_outstandingAdd2')"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_angsuranAdd2" nai="Angsuran " style="width:120px;background:#ff0;" value=""  onKeyPress="return isNumberKey(event)" onKeyUp="outputMoney('FasilitasPinjamanBankLain_angsuranAdd2')"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="FasilitasPinjamanBankLain_kolAdd2" name="FasilitasPinjamanBankLain_kolAdd2"  nai="kol " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
			
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_dpdAdd2" nai="DPD " style="width:120px;background:#ff0;" value="" maxlength="10"/></td>
			<td width="30%"><input type="button" value="SAVE" style="width:120px;background-color:blue;color:white;" onclick="yuriaA2()" /></td>
			
		</tr>
		</form>
		<form id="formeditA2" name="formeditA2" method="post">
		
		<?
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain2 where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'TO'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq2 = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="FasilitasPinjamanBankLain_jenisfasilitasAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" name="FasilitasPinjamanBankLain_jenisfasilitasAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>"  nai="Jenis Fasilitas " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($FasilitasPinjamanBankLain_jenisfasilitas == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasBankLain_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasBankLain_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_namabankAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" nai="Nama Bank " style="width:120px;background:#ff0;" value="<? echo $FasilitasPinjamanBankLain_namabank;?>" maxlength="100"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_outstandingAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" nai="Outstanding " style="width:120px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankLain_outstanding);?>" maxlength="20" onKeyPress="return isNumberKey(event)" onKeyUp="outputMoney('FasilitasPinjamanBankLain_outstandingAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>')"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_angsuranAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" nai="Angsuran " style="width:120px;background:#ff0;" value="<? echo numberFormat($FasilitasPinjamanBankLain_angsuran);?>"  onKeyPress="return isNumberKey(event)" onKeyUp="outputMoney('FasilitasPinjamanBankLain_angsuranAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>')"/></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="FasilitasPinjamanBankLain_kolAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" name="FasilitasPinjamanBankLain_kolAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>"  nai="kol " class="harus" style="background:#ff0;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($FasilitasPinjamanBankLain_kol == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($FasilitasPinjamanBankLain_kol == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($FasilitasPinjamanBankLain_kol == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($FasilitasPinjamanBankLain_kol == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($FasilitasPinjamanBankLain_kol == "5" ){ echo "selected";}?>>5</option>
				</select>
			
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><input type="text" id="FasilitasPinjamanBankLain_dpdAdd2<? echo $FasilitasPinjamanBankLain_seq2;?>" nai="DPD " style="width:120px;background:#ff0;" value="<? echo $FasilitasPinjamanBankLain_dpd;?>" maxlength="10"/></td>
			<td width="25%"><input type="hidden" id="FasilitasPinjamanBankLain_seq2" value="<? echo $FasilitasPinjamanBankLain_seq2;?>" /><input type="button" value="EDIT" style="width:50px;background-color:red;color:white;font-size:10px" onclick="mayuA2(<?echo $FasilitasPinjamanBankLain_seq2;?>)" /><input type="button" value="HAPUS" style="width:60px;background-color:red;color:white;font-size:10px" onclick="yukiA2(<?echo $FasilitasPinjamanBankLain_seq2;?>)" /></td>
		</tr>

		<?
				}
			}
		?>
		
		</table>
		</form>
	
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1">
		<div style="border:1px solid black;width:50px;" align="center"><? echo $InfoFasilitasPinjamanBankLain_cb3ORI;?></div>
		<select id="FasilitasBankLainCB3" name="FasilitasBankLainCB3"  nai="Informasi Negatif Komunitas 2" class="harus" style="background:#fff">
			<option value="YA"  <? if ($InfoFasilitasPinjamanBankLain_cb3 == "YA") { echo "selected";}?>>YA</option>
			<option value="TIDAK"  <? if ($InfoFasilitasPinjamanBankLain_cb3 == "TIDAK") { echo "selected";}?>>TIDAK</option>
		</select>
	</td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA2col" name="formentryA2col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:500px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">&nbsp;
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA2col" name="formentryA2col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:300px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:500px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankLain_berapakali1B" name="LKCDKolektibilitasTidakLancarBankLain_berapakali1B"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali1 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1B" name="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1B" name="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankLain_berapakali2B" name="LKCDKolektibilitasTidakLancarBankLain_berapakali2B"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali2 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2B" name="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2B" name="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<select id="LKCDKolektibilitasTidakLancarBankLain_berapakali3B" name="LKCDKolektibilitasTidakLancarBankLain_berapakali3B"  nai="Berapa kali dalam 6-12 bulan " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<option value="1" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == "1" ){ echo "selected";}?>>1</option>
					<option value="2" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == "2" ){ echo "selected";}?>>2</option>
					<option value="3" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == "3" ){ echo "selected";}?>>3</option>
					<option value="4" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == "4" ){ echo "selected";}?>>4</option>
					<option value="5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == "5" ){ echo "selected";}?>>5</option>
					<option value=">5" <? if ($LKCDKolektibilitasTidakLancarBankLain_berapakali3 == ">5" ){ echo "selected";}?>>>5</option>
				</select>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3B" name="LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["JenisFasilitasKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["JenisFasilitasKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
				<select id="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3B" name="LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3B"  nai="Jenis Fasilitas " class="harus" style="background:#fff;">
					<option value="" selected="selected">- Pilih -</option>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							}
					?>	
							<option value="<? echo $rowType["KhususKartuKredit_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["KhususKartuKredit_nama"];?></option>
					<?
						}
					?>
				</select>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr>
			<td colspan="4" align="center"><input type="button" value="SAVE" style="width:100px;background-color:blue;color:white;" onclick="nonA1();nonA2()" /></td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
<script type="text/javascript">
function nonA2() {
		var nan="SAVE7A2COL";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali1B").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1B").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1B").val();
		var LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali2B").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2B").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2B").val();
		var LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali3B").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3B").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3B").val();
		
		var InfoFasilitasPinjamanBankLain_cb1 = $("#FasilitasBankLainCB1").val();
		var InfoFasilitasPinjamanBankLain_cb2 = $("#FasilitasBankLainCB2").val();
		var InfoFasilitasPinjamanBankLain_cb3 = $("#FasilitasBankLainCB3").val();
		
		
		var FormName="formentryA2col";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&InfoFasilitasPinjamanBankLain_cb1="+InfoFasilitasPinjamanBankLain_cb1+"&InfoFasilitasPinjamanBankLain_cb2="+InfoFasilitasPinjamanBankLain_cb2+"&InfoFasilitasPinjamanBankLain_cb3="+InfoFasilitasPinjamanBankLain_cb3+"&LKCDKolektibilitasTidakLancarBankLain_berapakali1="+LKCDKolektibilitasTidakLancarBankLain_berapakali1+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1+"&LKCDKolektibilitasTidakLancarBankLain_berapakali2="+LKCDKolektibilitasTidakLancarBankLain_berapakali2+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2+"&LKCDKolektibilitasTidakLancarBankLain_berapakali3="+LKCDKolektibilitasTidakLancarBankLain_berapakali3+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	function yuriaA2() {
		var nan="SAVE7A2";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankLain_jenisfasilitas = $("#FasilitasPinjamanBankLain_jenisfasilitasAdd2").val();
		var FasilitasPinjamanBankLain_namabank = $("#FasilitasPinjamanBankLain_namabankAdd2").val();
		var FasilitasPinjamanBankLain_outstanding = $("#FasilitasPinjamanBankLain_outstandingAdd2").val();
		var FasilitasPinjamanBankLain_angsuran = $("#FasilitasPinjamanBankLain_angsuranAdd2").val();
		var FasilitasPinjamanBankLain_kol = $("#FasilitasPinjamanBankLain_kolAdd2").val();
		var FasilitasPinjamanBankLain_dpd = $("#FasilitasPinjamanBankLain_dpdAdd2").val();
			
		var FormName="formentryA2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankLain_jenisfasilitas="+FasilitasPinjamanBankLain_jenisfasilitas+"&FasilitasPinjamanBankLain_namabank="+FasilitasPinjamanBankLain_namabank+"&FasilitasPinjamanBankLain_outstanding="+FasilitasPinjamanBankLain_outstanding+"&FasilitasPinjamanBankLain_angsuran="+FasilitasPinjamanBankLain_angsuran+"&FasilitasPinjamanBankLain_kol="+FasilitasPinjamanBankLain_kol+"&FasilitasPinjamanBankLain_dpd="+FasilitasPinjamanBankLain_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function yukiA2(id){
		var nan="DEL7A2"
		
		var custnomid=$("#custnomid").val();
		var FasilitasPinjamanBankLain_seq2 = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&FasilitasPinjamanBankLain_seq="+FasilitasPinjamanBankLain_seq2+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function mayuA2(id) {
		var nan="EDIT7A2";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankLain_jenisfasilitas = $("#FasilitasPinjamanBankLain_jenisfasilitasAdd2"+id).val();
		var FasilitasPinjamanBankLain_namabank = $("#FasilitasPinjamanBankLain_namabankAdd2"+id).val();
		var FasilitasPinjamanBankLain_outstanding = $("#FasilitasPinjamanBankLain_outstandingAdd2"+id).val();
		var FasilitasPinjamanBankLain_angsuran = $("#FasilitasPinjamanBankLain_angsuranAdd2"+id).val();
		var FasilitasPinjamanBankLain_kol = $("#FasilitasPinjamanBankLain_kolAdd2"+id).val();
		var FasilitasPinjamanBankLain_dpd = $("#FasilitasPinjamanBankLain_dpdAdd2"+id).val();
		var FasilitasPinjamanBankLain_seq = id;
		
		var FormName="formeditA2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankLain_seq="+FasilitasPinjamanBankLain_seq+"&FasilitasPinjamanBankLain_jenisfasilitas="+FasilitasPinjamanBankLain_jenisfasilitas+"&FasilitasPinjamanBankLain_namabank="+FasilitasPinjamanBankLain_namabank+"&FasilitasPinjamanBankLain_outstanding="+FasilitasPinjamanBankLain_outstanding+"&FasilitasPinjamanBankLain_angsuran="+FasilitasPinjamanBankLain_angsuran+"&FasilitasPinjamanBankLain_kol="+FasilitasPinjamanBankLain_kol+"&FasilitasPinjamanBankLain_dpd="+FasilitasPinjamanBankLain_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function nonA1() {
		var nan="SAVE7A1COL";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali1").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1").val();
		var LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali2").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2").val();
		var LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $("#LKCDKolektibilitasTidakLancarBankLain_berapakali3").val();
		var LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $("#LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3").val();
		var LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $("#LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3").val();

		var FormName="formentryA1col";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&LKCDKolektibilitasTidakLancarBankLain_berapakali1="+LKCDKolektibilitasTidakLancarBankLain_berapakali1+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1+"&LKCDKolektibilitasTidakLancarBankLain_berapakali2="+LKCDKolektibilitasTidakLancarBankLain_berapakali2+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2+"&LKCDKolektibilitasTidakLancarBankLain_berapakali3="+LKCDKolektibilitasTidakLancarBankLain_berapakali3+"&LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3="+LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3+"&LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3="+LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					alert('Data berhasil disimpan');
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	function yuriaA1() {
		var nan="SAVE7A1";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankLain_jenisfasilitas = $("#FasilitasPinjamanBankLain_jenisfasilitasAdd").val();
		var FasilitasPinjamanBankLain_namabank = $("#FasilitasPinjamanBankLain_namabankAdd").val();
		var FasilitasPinjamanBankLain_outstanding = $("#FasilitasPinjamanBankLain_outstandingAdd").val();
		var FasilitasPinjamanBankLain_angsuran = $("#FasilitasPinjamanBankLain_angsuranAdd").val();
		var FasilitasPinjamanBankLain_kol = $("#FasilitasPinjamanBankLain_kolAdd").val();
		var FasilitasPinjamanBankLain_dpd = $("#FasilitasPinjamanBankLain_dpdAdd").val();
			
		var FormName="formentryA1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankLain_jenisfasilitas="+FasilitasPinjamanBankLain_jenisfasilitas+"&FasilitasPinjamanBankLain_namabank="+FasilitasPinjamanBankLain_namabank+"&FasilitasPinjamanBankLain_outstanding="+FasilitasPinjamanBankLain_outstanding+"&FasilitasPinjamanBankLain_angsuran="+FasilitasPinjamanBankLain_angsuran+"&FasilitasPinjamanBankLain_kol="+FasilitasPinjamanBankLain_kol+"&FasilitasPinjamanBankLain_dpd="+FasilitasPinjamanBankLain_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function yukiA1(id){
		var nan="DEL7A1"
		
		var custnomid=$("#custnomid").val();
		var FasilitasPinjamanBankLain_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&FasilitasPinjamanBankLain_seq="+FasilitasPinjamanBankLain_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function mayuA1(id) {
		var nan="EDIT7A1";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var FasilitasPinjamanBankLain_jenisfasilitas = $("#FasilitasPinjamanBankLain_jenisfasilitasAdd"+id).val();
		var FasilitasPinjamanBankLain_namabank = $("#FasilitasPinjamanBankLain_namabankAdd"+id).val();
		var FasilitasPinjamanBankLain_outstanding = $("#FasilitasPinjamanBankLain_outstandingAdd"+id).val();
		var FasilitasPinjamanBankLain_angsuran = $("#FasilitasPinjamanBankLain_angsuranAdd"+id).val();
		var FasilitasPinjamanBankLain_kol = $("#FasilitasPinjamanBankLain_kolAdd"+id).val();
		var FasilitasPinjamanBankLain_dpd = $("#FasilitasPinjamanBankLain_dpdAdd"+id).val();
		var FasilitasPinjamanBankLain_seq = id;
		
		var FormName="formeditA1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&FasilitasPinjamanBankLain_seq="+FasilitasPinjamanBankLain_seq+"&FasilitasPinjamanBankLain_jenisfasilitas="+FasilitasPinjamanBankLain_jenisfasilitas+"&FasilitasPinjamanBankLain_namabank="+FasilitasPinjamanBankLain_namabank+"&FasilitasPinjamanBankLain_outstanding="+FasilitasPinjamanBankLain_outstanding+"&FasilitasPinjamanBankLain_angsuran="+FasilitasPinjamanBankLain_angsuran+"&FasilitasPinjamanBankLain_kol="+FasilitasPinjamanBankLain_kol+"&FasilitasPinjamanBankLain_dpd="+FasilitasPinjamanBankLain_dpd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 