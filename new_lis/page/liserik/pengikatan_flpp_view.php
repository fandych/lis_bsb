<?php

require('./sqlsrv.php');
require('../../requirepage/parameter.php');
require('./terbilang.php');



$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";

$confmsg = "Are You Sure ??";

$dayList = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
);

$mountList = array(
	'1' => 'Januari',
	'2' => 'Februari',
	'3' => 'Maret',
	'4' => 'April',
	'5' => 'Mei',
	'6' => 'Juni',
	'7' => 'Juli',
	'8' => 'Agustus',
	'9' => 'September',
	'10' => 'Oktober',
	'11' => 'November',
	'12' => 'Desember'
);

function param_pekerjaan($pek_id){
	global $db;
	
	$v_kerjaan_name = "";
	$strsql='SELECT attribute from param_pekerjaan where code=\''.$pek_id.'\'';
	$rs = $db->_RQ($strsql);
	for($i=0;$i<count($rs);$i++)
	{
	  $v_kerjaan_name = $rs[$i]['attribute'];
	}
	
	return $v_kerjaan_name;
	
}

$custcreditplafond='';
$strsql='SELECT custcreditplafond,custcredittype FROM tbl_CustomerFacility2 WHERE custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custcreditplafond = $rs[$i]['custcreditplafond'];
  $custcredittype = $rs[$i]['custcredittype'];
}



$index_id = '';
$source = '';
$cr_option = '';
$fac_seq = '';
$plafond = '';
$tenor = '';
$installment = '';
$interest = '';
$provisi = '';
$opinion = '';
$description = '';
$strsql='SELECT *  FROM CR_ANALYST_APPROVAL WHERE custnomid=\''.$custnomid.'\'';
$custnpwpno='';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $index_id= $rs[$i]['index_id'];
  $custnomid= $rs[$i]['custnomid'];
  $source= $rs[$i]['source'];
  $cr_option= $rs[$i]['cr_option'];
  $fac_seq= $rs[$i]['fac_seq'];
  $plafond= $rs[$i]['plafond'];
  $tenor= $rs[$i]['tenor'];
  $installment= $rs[$i]['installment'];
  $interest= $rs[$i]['interest'];
  $provisi= $rs[$i]['provisi'];
  $opinion= $rs[$i]['opinion'];
  $description= $rs[$i]['description'];
}

$cust_jeniscol='';
$col_id='';
$strsql='SELECT cust_jeniscol,col_id FROM Tbl_Cust_MasterCol WHERE ap_lisregno=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $cust_jeniscol= $rs[$i]['cust_jeniscol'];
  $col_id= $rs[$i]['col_id'];
}


$harga_jual = '';
$uang_muka = '';
$rencana_kredit = '';
$persentase_setoran_uang_muka = '';
$persentase_pembiayaan = '';
$max_pembiayaan = '';
$ket_identitas = '';
$ket_penghasilan = '';
$ket_kredit = '';
$kesimpulan = '';
$data_identitas = '';
$data_pekerjaan = '';
$data_penghasilan = '';
$data_syarat_kredit = '';
$ket_bunga = '';
$ket_angsuran = '';
$pengikatan_jaminan = '';
$biaya_adm = '';
$syarat_ttd_pk = '';
$syarat_realisasi = '';
$lain_lain = '';
$custnpwpno='';
$strsql='SELECT *  FROM CR_DATA WHERE custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custnomid= $rs[$i]['custnomid'];
  $harga_jual= $rs[$i]['harga_jual'];
  $uang_muka= $rs[$i]['uang_muka'];
  $rencana_kredit= $rs[$i]['rencana_kredit'];
  $persentase_setoran_uang_muka= $rs[$i]['persentase_setoran_uang_muka'];
  $persentase_pembiayaan= $rs[$i]['persentase_pembiayaan'];
  $max_pembiayaan= $rs[$i]['max_pembiayaan'];
  $ket_identitas= $rs[$i]['ket_identitas'];
  $ket_penghasilan= $rs[$i]['ket_penghasilan'];
  $ket_kredit= $rs[$i]['ket_kredit'];
  $kesimpulan= $rs[$i]['kesimpulan'];
  $data_identitas= $rs[$i]['data_identitas'];
  $data_pekerjaan= $rs[$i]['data_pekerjaan'];
  $data_penghasilan= $rs[$i]['data_penghasilan'];
  $data_syarat_kredit= $rs[$i]['data_syarat_kredit'];
  $ket_bunga= $rs[$i]['ket_bunga'];
  $ket_angsuran= $rs[$i]['ket_angsuran'];
  $pengikatan_jaminan= $rs[$i]['pengikatan_jaminan'];
  $biaya_adm= $rs[$i]['biaya_adm'];
  $syarat_ttd_pk= $rs[$i]['syarat_ttd_pk'];
  $syarat_realisasi= $rs[$i]['syarat_realisasi'];
  $lain_lain= $rs[$i]['lain_lain'];
}

$custktpno='';
$custaddr='';
$custfullname='';
$custproccode='';
$v_custtarid='';
$strsql='SELECT custktpno,custaddr,custfullname,custnpwpno,custproccode,custtarid  FROM Tbl_CustomerMasterPerson2 WHERE custnomid=\''.$custnomid.'\'';
$custnpwpno='';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custktpno=$rs[$i]['custktpno'];
  $custaddr=$rs[$i]['custaddr'];
  $custfullname=$rs[$i]['custfullname'];
  $custnpwpno=$rs[$i]['custnpwpno'];
  $custproccode=$rs[$i]['custproccode'];
  $v_custtarid = $rs[$i]['custtarid'];
}

$v_pasangan_nomorktp = ' - ';
$v_pasangan_namadepan = ' - ';
$v_pasangan_namatengah = ' - ';
$v_pasangan_namabelakang = ' - ';

$strsql = 'SELECT _pasangan_nomorktp,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang,_pasangan_pekerjaan_id
			FROM pl_custpasangan2 WHERE _custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$v_pasangan_nomorktp = $rs[$i]['_pasangan_nomorktp'];
		$v_pasangan_namadepan = $rs[$i]['_pasangan_namadepan'];
		$v_pasangan_namatengah = $rs[$i]['_pasangan_namatengah'];
		$v_pasangan_namabelakang = $rs[$i]['_pasangan_namabelakang'];
	}
}

if($v_custtarid == "1")
{
	$info_kerja = "SELECT _pekerjaan, _dinasname as _office FROM tbl_KGB_infokerja2 WHERE _custnomid = '$custnomid'";
}
else if($v_custtarid == "2")
{
	$info_kerja = "SELECT _jenispekerjaan as _pekerjaan, _namaperusahan as _office FROM tbl_KPR_infokerja2 WHERE _custnomid = '$custnomid'";
}
$rs = $db->_RQ($info_kerja);
$v_pekerjaan = param_pekerjaan($rs[0]['_pekerjaan']);

$nomorpk = '';
$tanggalpk = '';
$rektabungan = '';
$atasnama = '';
$rekpinjaman = '';
$strsql='SELECT * from PK_KGS where CUSTNOMID=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $nomorpk= $rs[$i]['NOMORPK'];
  $tanggalpk= $rs[$i]['TANGGALPK']->format('Y-m-d');
  $rektabungan= $rs[$i]['P4_REKTABUNGAN'];
  $atasnama= $rs[$i]['P4_ATASNAMA'];
  $rekpinjaman= $rs[$i]['P4_REKPINJAMAN'];
}



$nomororder = '';
$tanggalordernotaris = date("Y/m/d");
$kepada_notaris = '';
$tempat_notaris = '';
$pemimpin = '';
$akte = '';
$tandatangan = '';
$tanggalsuratkuasa = date("Y/m/d");
$tanggaltandaterima = date("Y/m/d");
$tanggaltandaterima2 = date("Y/m/d");
$pemimpinsuratkuasa = "";
$pemimpintandaterima = "";
$strsql = 'SELECT * FROM ORDER_NOTARIS WHERE CUSTNOMID=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $nomororder= $rs[$i]['NOMORORDER'];
  $tanggalordernotaris= $rs[$i]['TANGGALORDERNOTARIS']->format('d-m-Y');
  $kepada_notaris= $rs[$i]['KEPADA_NOTARIS'];
  $tempat_notaris= $rs[$i]['TEMPAT_NOTARIS'];
  $pemimpin= $rs[$i]['PEMIMPIN'];
  $akte= $rs[$i]['AKTE'];
  $tandatangan= $rs[$i]['TANDATANGAN'];
  $tanggalsuratkuasa= $rs[$i]['TGLSURATKUASA']->format('d-m-Y');
  $pemimpinsuratkuasa= $rs[$i]['PIMPINANSURATKUASA'];
  $tanggaltandaterima= $rs[$i]['TGLTANDATERIMA']->format('d-m-Y');
  $tanggaltandaterima2= $rs[$i]['TGLTANDATERIMA2']->format('d-m-Y');
  $pemimpintandaterima= $rs[$i]['PIMPINANTANDATERIMA'];
}



$_cond_code = '';
$_type = '';
$_merk = '';
$_model = '';
$_jns_kendaraan = '';
$_thnpembuatan = '';
$_silinder_isi = '';
$_silinder_wrn = '';
$_norangka = '';
$_nomesin = '';
$_bpkb_tgl = '';
$_stnk_exp = '';
$_faktur_tgl = '';
$_bahanbakar = '';
$_bpkb_nama = '';
$_bpkb_addr1 = '';
$_bpkb_addr2 = '';
$_bpkb_addr3 = '';
$_perlengkapan = '';
$_notes = '';
$_opini = '';
$_officer_code = '';
$_tanggal_kunjungan = '';
$strsql='SELECT * FROM appraisal_vhc WHERE _collateral_id=\''.$col_id.'\'';
$rs = $apr->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_cond_code= $rs[$i]['_cond_code'];
  $_type= $rs[$i]['_type'];
  $_merk= $rs[$i]['_merk'];
  $_model= $rs[$i]['_model'];
  $_jns_kendaraan= $rs[$i]['_jns_kendaraan'];
  $_thnpembuatan= $rs[$i]['_thnpembuatan'];
  $_silinder_isi= $rs[$i]['_silinder_isi'];
  $_silinder_wrn= $rs[$i]['_silinder_wrn'];
  $_norangka= $rs[$i]['_norangka'];
  $_nomesin= $rs[$i]['_nomesin'];
  $_bpkb_tgl= $rs[$i]['_bpkb_tgl'];
  $_stnk_exp= $rs[$i]['_stnk_exp'];
  $_faktur_tgl= $rs[$i]['_faktur_tgl'];
  $_bahanbakar= $rs[$i]['_bahanbakar'];
  $_bpkb_nama= $rs[$i]['_bpkb_nama'];
  $_bpkb_addr1= $rs[$i]['_bpkb_addr1'];
  $_bpkb_addr2= $rs[$i]['_bpkb_addr2'];
  $_bpkb_addr3= $rs[$i]['_bpkb_addr3'];
  $_perlengkapan= $rs[$i]['_perlengkapan'];
  $_notes= $rs[$i]['_notes'];
  $_opini= $rs[$i]['_opini'];
  $_officer_code= $rs[$i]['_officer_code'];
  $_tanggal_kunjungan= $rs[$i]['_tanggal_kunjungan'];
}


$_type_jaminan = '';
$_lokasi_rumah = '';
$_jumlah_lantai = '';
$_luas_bangunan = '';
$_panjang_bangunan = '';
$_lebar_bangunan = '';
$_arah_bangunan = '';
$_umur_bangunan = '';
$_tahun_dibangun = '';
$_tahun_renovasi = '';
$_luas_tanah = '';
$_panjang_tanah = '';
$_lebar_tanah = '';
$_sisi_utara = '';
$_sisi_timur = '';
$_sisi_selatan = '';
$_sisi_barat = '';
$_latitude = '';
$_longitude = '';
$_notes = '';
$_opini = '';
$_officer_code = '';
$_tanggal_kunjungan = '';
$strsql='SELECT * FROM appraisal_tnb WHERE _collateral_id=\''.$col_id.'\'';
$rs = $apr->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

  $_collateral_id= $rs[$i]['_collateral_id'];
  $_type_jaminan= $rs[$i]['_type_jaminan'];
  $_lokasi_rumah= $rs[$i]['_lokasi_rumah'];
  $_jumlah_lantai= $rs[$i]['_jumlah_lantai'];
  $_luas_bangunan= $rs[$i]['_luas_bangunan'];
  $_panjang_bangunan= $rs[$i]['_panjang_bangunan'];
  $_lebar_bangunan= $rs[$i]['_lebar_bangunan'];
  $_arah_bangunan= $rs[$i]['_arah_bangunan'];
  $_umur_bangunan= $rs[$i]['_umur_bangunan'];
  $_tahun_dibangun= $rs[$i]['_tahun_dibangun'];
  $_tahun_renovasi= $rs[$i]['_tahun_renovasi'];
  $_luas_tanah= $rs[$i]['_luas_tanah'];
  $_panjang_tanah= $rs[$i]['_panjang_tanah'];
  $_lebar_tanah= $rs[$i]['_lebar_tanah'];
  $_sisi_utara= $rs[$i]['_sisi_utara'];
  $_sisi_timur= $rs[$i]['_sisi_timur'];
  $_sisi_selatan= $rs[$i]['_sisi_selatan'];
  $_sisi_barat= $rs[$i]['_sisi_barat'];
  $_latitude= $rs[$i]['_latitude'];
  $_longitude= $rs[$i]['_longitude'];
  $_notes= $rs[$i]['_notes'];
  $_opini= $rs[$i]['_opini'];
  $_officer_code= $rs[$i]['_officer_code'];
  $_tanggal_kunjungan= $rs[$i]['_tanggal_kunjungan'];
}


$col_addr1='';
$col_imbluas='';
$col_certluas='';
$col_certtype='';
$col_certno='';
$col_certdate='';
$col_certatasnama='';
$strsql = "
select '' as col_addr1,col_imbluas,col_certluas,col_certtype,col_certno,col_certdate,col_certatasnama from tbl_COL_Ruko
where ap_lisregno='".$custnomid."'
union
select col_addr1,col_imbluas,'','','','','' from tbl_COL_Building
where ap_lisregno='".$custnomid."'
union
select col_addr1,'0',col_certluas,col_certtype,col_certno,col_certdate,col_certatasnama from tbl_COL_Land
where ap_lisregno='".$custnomid."'
union
select '',col_imbluas,col_certluas,col_certtype,col_certno,col_certdate,col_certatasnama from tbl_COL_Kios
where ap_lisregno='".$custnomid."'";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $col_addr1=$rs[$i]['col_addr1'];
  $col_imbluas=$rs[$i]['col_imbluas'];
  $col_certluas=$rs[$i]['col_certluas'];
  $col_certtype=$rs[$i]['col_certtype'];
  $col_certno= $rs[$i]['col_certno'];
  $col_certdate=$rs[$i]['col_certdate']->format('Y-m-d');
  $col_certatasnama=$rs[$i]['col_certatasnama'];
}

$col_imbluas='';
$col_imbno='';
$strsql = "
select col_addr1,col_imbluas,col_imbno,'','','','' from tbl_COL_Building
where ap_lisregno='".$custnomid."'
";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

  $col_imbluas=$rs[$i]['col_imbluas'];
  $col_imbno=$rs[$i]['col_imbno'];

}

$UNTUKPEMBELIAN = '';
$strsql="SELECT * FROM LEGAL_OPINION WHERE custnomid = '$custnomid'";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $UNTUKPEMBELIAN= $rs[$i]['UNTUKPEMBELIAN'];
}

$col_certluas = '';
$col_certtype = '';
$col_certdate = '';
$col_certatasnama = '';
$strsql="select * from tbl_COL_Land where col_id in (select col_id from Tbl_Cust_MasterCol where ap_lisregno = '$custnomid' and flaginsert = '1' and flagdelete = '0' and cust_jeniscol = 'BA1')";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $col_certluas= $rs[$i]['col_certluas'];
  $col_certtype= $rs[$i]['col_certtype'];
  $col_certno= $rs[$i]['col_certno'];
  $col_certatasnama= $rs[$i]['col_certatasnama'];
  $col_certdate= $rs[$i]['col_certdate']->format('d-m-Y');
}


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Order Opinion</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>

    <style media="screen">
      .tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}
    </style>
  </head>
  <body>
    <form id="frm" action="pengikatan_flpp_exec.php" method="post">
    <?
      if($userid != "" && $userwfid!="")
      {
    ?>
      <div style="float: right;margin-right: 220px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
    <?
      }
    ?>
      <br />
      <br />
      <table id="tableform" border="0" align="center" style="page-break-after: always;">
        <tr>
          <td style="width:50%" valign="top">
            <table>
              <tr>
                <td style="width:100px">Nomor</td>
                <td>:</td>
                <td>
                  <? echo $nomororder; ?>
                </td>
              </tr>
              <tr>
                <td>Laporan</td>
                <td>:</td>
                <td>1 (satu) Berkas</td>
              </tr>
              <tr>
                <td>Perihal</td>
                <td>:</td>
                <td>Pengikatan Jaminan</td>
              </tr>
            </table>
          </td>
          <td>&nbsp;</td>
          <td style="width:300px;" valign="top">
            Palembang, <? echo $tanggalordernotaris; ?>
            <br/><br/>
            Kepada Yth,
            <br/>
            <? echo $kepada_notaris; ?>
            <br/>
            Notaris / PPAT
            <br/>
            di <? echo $tempat_notaris; ?>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <p>
              Bersama ini kami sampaikan kepada Saudara berkas debitur kami, untuk dapat dibuatkan
              <?php echo nl2br($akte);?>

              <table>
                <tr>
                  <td style="width:150px;">Nama Debitur</td>
                  <td>:</td>
                  <td><?php echo  $custfullname;?></td>
                </tr>
                <tr>
                  <td>No Identitas/KTP</td>
                  <td>:</td>
                  <td><?php echo  $custktpno;?></td>
                </tr>
                <tr>
                  <td>Alamat Debitur</td>
                  <td>:</td>
                  <td><?php echo  $custaddr;?></td>
                </tr>
              </table>
            </p>
            <p>Barang yang dijaminkan:
              <br/>

                  <?

                  if($custproccode=="KKB")
                  {

                    echo '
                    <table>
                      <tr>
                        <td coslpan="3" valign="top">KENDARAAN</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Merek</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_merk.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Model</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_model.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Jenis Kendaraan</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_jns_kendaraan.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">No Rangka</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_norangka.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">No Mesin</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_nomesin.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">BPKB Nama </td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_bpkb_nama.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">BPKB Alamat</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_bpkb_addr1.'<br/>'.$_bpkb_addr2.'<br/>'.$_bpkb_addr3.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">BPKB tanggal</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_bpkb_tgl.'</td>
                      </tr>
                    </table>';

                  }
                  else
                  {
                      ?>

                      <?echo $UNTUKPEMBELIAN.', '.$col_addr1.' dengan Luas Bangunan/Tanah '.$col_imbluas.'/'.$col_certluas.' M2 '.$col_certtype.' No. '.$col_certno.' Tgl. '.$col_certdate.', An. '.$col_certatasnama.' (Akan dibalik nama atas nama pemohon).';?>
                    <?
                    /*
                    echo '
                    <table>
                      <tr>
                        <td coslpan="3" valign="top">BANGUNAN</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Lokasi Rumah</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_lokasi_rumah.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Jumlah Lantai</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_jumlah_lantai.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Luas Bangunan</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_luas_bangunan.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Arah Bangunan</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_arah_bangunan.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Umur Bangunan</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_umur_bangunan.'</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width:300px;">Letak latitude / longtitude</td>
                        <td valign="top">:</td>
                        <td valign="top">'.$_latitude.'/'.$_longitude.'</td>
                      </tr>
                    </table>';
                    */
                  }

                  ?>

                <table>
                  <tr>
                    <td style="width:150px;">Pengikatan</td>
                    <td>:</td>
                    <td><strong><?php echo $pengikatan_jaminan;?></strong></td>
                  </tr>
                  <tr>
                    <td>Plafond Kredit</td>
                    <td>:</td>
                    <td><?php echo number_format($plafond);?></td>
                  </tr>
                  <tr>
                    <td>Perjanjian Kredit</td>
                    <td>:</td>
                    <td><?echo $nomorpk.' tanggal '.$tanggalpk.' atas nama '.$custfullname?></td>
                  </tr>
                </table>
            </p>
            <p>Atas seluruh biaya yang timbul untuk keperluan pengikatan jaminan kredit yang merupakan beban debitur, dapat saudara ajukan kepada kami untuk kami bebankan pada yang bersangkutan, dan untuk keperluan pencairan Fasilitas Kredit calon debitur diatas, mohon saudara terbitkan Surat Keterangan (cover note) yang menyatakan bahwa sertifikat tanah yang diagunkan tersebut telah dilakukan pengecekan/verifikasi di kantor badan pertanahan setempat, tidak bermasalah dan dapat diikat secara yuridis (dengan sempurna);
              <?php echo nl2br($tandatangan);?>
            </p>


            <p>Agar seluruh asli dokumen diserahkan kepada kami pada kesempatan pertama, bilamana proses pengikatan telah selesai dilaksanakan.</p>

            <p>Demikian agar Saudara maklum, atas perhatian dan kerjasama yang baik dari pihak Saudara kami sampaikan terima kasih.</p>

            <p>PT Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung Satuan Kredit Konsumen</p>
            <div>
              <b style="text-decoration:underline;"><? echo $pemimpin; ?></b><br/>
              Pemimpin<br/><br/><br/><br/>
              Jaminan asli telah diterima dan dapat diikat Sempurna
              <br/><br/><br/><br/>
              <b style="text-decoration:underline;">
                <div id="notaris">Nama notaris</div>
              </b>
              Notaris / PPAT

            </div>
          </td>
        </tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
    </table>
    <table id="tableform" border="0" align="center" style="page-break-after: always;">
		<tr>
			<td colspan="3" align="center"><h3><b>SURAT KUASA</br>MEMBLOKIR DAN MEMOTONG REKENING SIMPANAN/PINJAMAN</b></h3></td>
		</tr>
		<tr>
			<td colspan="3">
				<p>
					Yang bertanda tangan dibawah ini :
					
					<table>
						<tr>
							<td style="width:150px;">Nama</td>
							<td>:</td>
							<td><?php echo  $custfullname;?></td>
						</tr>
						<tr>
							<td>Pekerjaan</td>
							<td>:</td>
							<td><?php echo  $v_pekerjaan;?></td>
						</tr>
						<tr>
							<td>No. KTP</td>
							<td>:</td>
							<td><?php echo  $custktpno;?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td>:</td>
							<td><?php echo  $custaddr;?></td>
						</tr>
					</table>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<p>
					Dengan ini memberi  kuasa sepenuhnya yang tidak dapat ditarik kembali dengan hak substitusi kepada PT. Bank Pembangunan Daerah Sumatera Selatan Satuan Kredit Konsumen untuk memotong/menerima dan atau memindahbukukan rekening pinjaman Kredit Griya Sejahtera (KGS) FLPP <?php echo $rekpinjaman;?> An. <?php echo $custfullname;?> dan rekening Tabungan <?php echo $atasnama;?> atau rekening tabungan milik saya lainnya untuk dipergunakan sebagai Pencairan Kredit ,Pembayaran Provisi, Administrasi, Asuransi, Pembayaran angsuran kredit, ataupun Biaya-Biaya yang berhubungan dengan pinjaman saya tersebut apabila diperlukan sampai dengan kredit lunas.
				</p>
				<p>
					Demikian surat kuasa ini saya buat dengan sebenarnya dalam keadaan sadar serta ditandatangani diatas kertas bermeterai cukup untuk dapat dipergunakan seperlunya.
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p align="center">
					&nbsp;
					</br>
					Yang menerima kuasa
					</br>
					PT Bank Pembangunan Daerah Sumatera Selatan
					</br>
					Dan Bangka Belitung
					</br>
					Satuan Kredit Konsumen
				</p>
			</td>
			<td>
				&nbsp;
			</td>
			<td valign="top">
				<p>
					Palembang, <? echo $tanggalsuratkuasa; ?>
					</br>
					Yang Memberi Kuasa
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<p align="center">
					<b style="text-decoration:underline;"><? echo $pemimpinsuratkuasa; ?></b><br/>
					Pemimpin<br/><br/><br/><br/>
				</p>
			</td>
			<td>
				&nbsp;
			</td>
			<td valign="top">
				<p>
					<b style="text-decoration:underline;"><?php echo $custfullname;?></b>
					</br>
					Pemilik Rekening
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr style="page-break-after: always;">
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
    </table>
    <table id="tableform" border="0" align="center">
		<tr>
			<td colspan="3" align="center"><h3><b>TANDA TERIMA JAMINAN</b></h3></td>
		</tr>
		<tr>
			<td colspan="3">
				<p>
					Pada hari ini <?php echo $dayList[date("D",strtotime($tanggaltandaterima))];?> tanggal <?php echo ucwords(Terbilang(date("j",strtotime($tanggaltandaterima))))?> bulan <?php echo $mountList[date("n",strtotime($tanggaltandaterima))];?> tahun <?php echo ucwords(Terbilang(date("Y",strtotime($tanggaltandaterima))))?> ( <?php echo date("d-m-Y",strtotime($tanggaltandaterima));?> ), kami yang bertandatangan di bawah ini dengan bukti diri :
					
					<ul>
						<li>Nama    : <?php echo $custfullname;?></li>
						<li>No. KTP : <?php echo $custktpno;?></li>
					</ul>
					
					<ul>
						<li>Nama    : <?php echo $v_pasangan_namadepan." ".$v_pasangan_namatengah." ".$v_pasangan_namabelakang;?></li>
						<li>No. KTP : <?php echo $v_pasangan_nomorktp;?></li>
					</ul>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<p>
					Bahwa, kami dengan secara sadar dan tanpa paksaan dari pihak manapun  selaku  penjamin hutang  atas  pelunasan  Kredit Griya Sejahtera (KGS) FLPP  yang   akan  diterima oleh  dari PT Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung, dengan ini kami menyerahkan kepada PT Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung Satuan Kredit Konsumen  harta milik kami yang bebas dari sengketa pihak manapun juga, yaitu :
				</p>
				<p>
<?
				if($custproccode=="KKB")
				{
?>
					<ul>
						<li><?php echo 'Jenis Kendaraan '.$_jns_kendaraan;?></li>
						<li><?php echo 'No. Rangka '.$_norangka;?></li>
						<li><?php echo 'No. Mesin '.$_nomesin;?></li>
					</ul>
<?
				}else{
?>
					<ul>
						<li><?php echo $col_certtype.' No. '.$col_certno.' Tgl. '.$col_certdate.', An. '.$col_certatasnama.' (Akan dibalik nama atas nama pemohon).';?></li>
						<li><?php echo 'IMB No. '.$col_imbno;?></li>
					</ul>
<?
				}
?>
				</p>
				<p>
					Sebagai jaminan pelunasan Kredit Griya Sejahtera (KGS) FLPP atas nama <?php echo $custfullname;?>, maka kami bersedia hadir menghadap notaris untuk menandatangani Pengikatan Jaminan.
				</p>
			</td>
		</tr>
		<tr>
			<td>
				<p align="center">
					&nbsp;
					</br>
					Yang menerima kuasa
					</br>
					PT Bank Pembangunan Daerah Sumatera Selatan
					</br>
					Dan Bangka Belitung
					</br>
					Satuan Kredit Konsumen
				</p>
			</td>
			<td valign="top" align="center" colspan="2">
				<p>
					Palembang, <? echo $tanggaltandaterima2; ?>
					</br>
					Yang Menyerahkan
					</br>
					Pemilik Jaminan
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<p align="center">
					<b style="text-decoration:underline;"><? echo $pemimpintandaterima; ?></b><br/>
					Pemimpin<br/><br/><br/><br/>
				</p>
			</td>
			<td valign="top" align="center">
				<p>
					<b style="text-decoration:underline;"><?php echo $custfullname;?></b>
					</br>
					Debitur
				</p>
			</td>
			<td valign="top" align="center">
				<p>
					<b style="text-decoration:underline;"><?php echo $v_pasangan_namadepan;?></b>
					</br>
					Suami / Isteri
				</p>
			</td>
		</tr>
        <tr>
          <td colspan="3" style="text-align:center;">
            <?php
            if($userid != "" && $userwfid!="")
            {
            require ("../../requirepage/btnview.php");
            require ("../../requirepage/hiddenfield.php");
            }
            require("../../requirepage/btnprint.php");
            ?>

          </td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">
      <? require ("../../requirepage/hiddenfield.php"); ?>
    </form>
  </body>
</html>
