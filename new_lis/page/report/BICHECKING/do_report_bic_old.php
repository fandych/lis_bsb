<?
	require ('../../../lib/formatError.php');
	require ('../../../lib/open_con.php');
	
	//GET PARAM FLOW NAME
	$sql_query = "SELECT * FROM Tbl_workflow order by wf_urut";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $sql_query, $params, $cursorType);
	if ( $sqlConn === false)
	{
		die( FormatErrors( sqlsrv_errors() ) );
	}
	if(sqlsrv_has_rows($sqlConn))
	{
		$x=0;
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			
			$paramFlowName[$row['wf_id']] = $row['wf_name'];
			$paramSLATime[$row['wf_id']] = $row['wf_time'];
			
			$allFlow[$x] = $row['wf_id'];
			$x++;
		}
	}
	sqlsrv_free_stmt( $sqlConn);
	//END PARAM FLOW NAME
	
	$tgl1=$_REQUEST['tgl1'];
	$tgl2=$_REQUEST['tgl2'];
	$region=$_REQUEST['regioncode'];
	$branch=$_REQUEST['branchcode'];
	$status=$_REQUEST['status'];
	$admin=$_REQUEST['inadmin'];
	
	if($tgl1==""){
		$tglStart = "";
	} else {
		$tglStart = date("d/m/Y", strtotime("$tgl1"));
	}
	
	if($tgl1==""){
		$tglFinish = "";
	} else {
		$tglFinish = date("d/m/Y", strtotime("$tgl2"));
	}
	if($admin == "888"){
		if($region == "" || $region == "all"){
			$KondisiRegion = "";
		} else {
			$KondisiRegion = "and fn.txn_region_code='$region'";
		} 
	} else {
		if($region == "" || $region == "all"){
			$KondisiRegion = "and fn.txn_region_code='$admin'";
		} else {
			$KondisiRegion = "and fn.txn_region_code='$region'";
		} 
	}
	
	if($branch ==""){
		$KondisiBranch = "";
	} else {
		$KondisiBranch = "and fn.txn_branch_code='$branch'";
	}
	
	$Kondisi = "";
	
	/*
	if($status=="A")
	{
		$Kondisi = ",Tbl_FBIR fb, Tbl_FSTART fn, Tbl_CustomerMasterPerson2 cmp where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=cmp.custnomid and  custflagbi='F' and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code";
	} 
	else if ($status=="I")
	{
		$Kondisi = ",Tbl_FBIC fb, Tbl_FSTART fn, Tbl_CustomerMasterPerson2 cmp where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=cmp.custnomid and (custflagbi='I' or custflagbi='Y') and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code";
	} 
	else if ($status=="R")
	{
	} 
	else 
	{
		//$Kondisi = ",Tbl_FBIR fb, Tbl_FNOM fn, Tbl_CustomerMasterPerson cmp,Tbl_FIBC fi where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=fi.txn_id and cf.custnomid=cmp.custnomid and (((custflagbi='I' or custflagbi='Y') and fi.txn_action='A' and fi.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121)) or (custflagbi='F' and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121))) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code"; 
		$Kondisi = ",Tbl_FBIR fb, Tbl_FSTART fn, Tbl_CustomerMasterPerson2 cmp , Tbl_FBIC fi where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=cmp.custnomid  and cf.custnomid=fi.txn_id and ((custflagbi='F' and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'2012/04/20',121) and CONVERT(datetime,'2012/06/29',121)) or ((custflagbi='I' or custflagbi='Y') and fi.txn_action='A' and fi.txn_time between CONVERT(datetime,'2012/04/20',121) and CONVERT(datetime,'2012/06/29',121))) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code";
	}
	
	*/
	
	
	
?>
<html>	
<head>
<script type="text/javascript" src="js/datetimepicker_css.js"></script>
<title>REPORT BICHECKING</title>
</head>
<body>
<div align="center">
<table align="center">
	<tr>
		<td>REPORT BI CHECKING  Tgl <strong><? echo $tglStart;?></strong> s/d <strong><? echo $tglFinish;?></strong></td>
	</tr>
</table>
<?
	$sql = "select cmp.custsex,cmp.custbusname, cf.custnomid as [id] ,fn.txn_region_code as [region] ,fn.txn_branch_code as [branch], custfullname , fb.txn_action as[status] from Tbl_CustomerFlag cf $Kondisi";
	//echo $sql; exit;
	$rs = sqlsrv_query($conn,$sql);
	$errs = sqlsrv_errors();
	if ( $rs === false){die( FormatErrors( sqlsrv_errors()));}
	if(sqlsrv_has_rows($rs))
	{
?>
	<table cellspacing="0" cellpadding="0" border="1" width="800" align="center">
		<tr>
			<td width="2%"><div align="center"><strong>NO</strong></div></td>
			<td width="10%"><div align="center"><strong>REGIONAL</strong></div></td>
			<td width="10%"><div align="center"><strong>CABANG</strong></div></td>
			<td width="20%"><div align="center"><strong>APPLICATION ID</strong></div></td>
			<td width="35%"><div align="center"><strong>NAMA DEBITUR</strong></div></td>
			<td width="15%"><div align="center"><strong>PROCESS</strong></div></td>
			<td width="8%"><div align="center"><strong>APLLICATION STATUS</strong></div></td>
		</tr>
<?
		$Flow = "&nbsp;-&nbsp;";
		$i=0;
		$grandtot="";
		while($rowrs = sqlsrv_fetch_array( $rs, SQLSRV_FETCH_ASSOC))
		{
			$ApplicationID = $rowrs['id'];
			$RegionCode = $rowrs['region'];
			$BranchCode = $rowrs['branch'];
			if($rowrs['custsex']=="0"){
				$NamaDebitur = $rowrs['custbusname'];
			} else {
				$NamaDebitur = $rowrs['custfullname'];
			}
			$StatusApp = $rowrs['status'];
			$sqls = "select txn_flow from Tbl_Flow_Status where txn_id='$ApplicationID'";
			//echo $sqls;
			$rss = sqlsrv_query($conn,$sqls);
			if($rss === false){
				die( FormatErrors( sqlsrv_errors()));
			}
			if(sqlsrv_has_rows($rss)){
				if($row=sqlsrv_fetch_array ($rss, SQLSRV_FETCH_ASSOC)){
					$Flow = $row['txn_flow'];
				}
			}
			
			//echo $ApplicationID;
			$i++;
			/*$NamaDebitur = $rowrs['custfullname'];
			$Plafond1 = $rowrs['custcreditplafond1'];
			$Plafond2 = $rowrs['custcreditplafond2'];
			$tempPla = $Plafond1+$Plafond2;
			$grandtot = $grandtot + $tempPla;
			$totPla = number_format($Plafond1+$Plafond2);
			$KodeCabang = $rowrs['custbranchcode'];
			$tsql = "select * from Tbl_Branch where branch_code = '$KodeCabang'";
			$rstsql = sqlsrv_query ($conn,$tsql);
			if ( $rstsql === false){die( FormatErrors( sqlsrv_errors()));}
			if(sqlsrv_has_rows($rstsql))
			{
				if($rowtsql = sqlsrv_fetch_array( $rstsql, SQLSRV_FETCH_ASSOC))
				{
					$NamaCabang = $rowtsql['branch_name'];
				}
			}
			else { $NamaCabang = "";}
			$NomorPk = $rowrs['pk_no'];
			*/
			/*?>
				<tr><td><? echo $i;?></td><td><? echo $NamaDebitur;?></td><td><? echo $KodeCabang." - ".$NamaCabang;?></td><td><div align="right"><? echo $NomorPk;?></div></td><td><div align="right"><? echo $totPla;?></div></td></tr>
			<?*/
			?>
				<tr>
                <td align="center"><? echo $i;?></td>
                <td align="center"><? echo $RegionCode; ?></td>
                <td align="center"><? echo $BranchCode;?></td>
                <td align="center"><? echo $ApplicationID; ?></td>
                <td align="left"><? echo $NamaDebitur;?></td>
                <td align="center"><? echo $paramFlowName[$Flow];?></td>
                <td align="center"><? echo $StatusApp;?></td>
                </tr>
			<?
			?>
			</table>
            <?
		}
	}
	else
	{
		?>
			<table cellspacing="0" cellpadding="0" border="1" width="800" align="center">
			<tr>
				<td><div align="center"><strong>Tidak ada data dalam Periode <? echo $tglStart;?> s/d <? echo $tglFinish;?></strong></div></td>
			</tr>
            </table>
		<?
	}
?>
</div>
<br><br><br>
<div align="center">
<table border="0">
<tr>
<td>
<ul>
<li>A = Terima dari LIS</li>
<li>I = Telah di order ke BI</li>
<li>R = Belum di order ke BI</li>
<li>R = Telah dikirim kembali ke LIS</li>
</ul>
</td>
</tr>
</table>
</div>
</body>
</html>