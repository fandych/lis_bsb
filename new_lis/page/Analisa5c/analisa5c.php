<?	
	//ini_set("display_errors", 0);
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../lib/open_conLISAPR.php");
	//require ("../../lib/open_con_apr.php");
	require ("../../requirepage/parameter.php");

	
	$tanggalkunjungan = "";
	$cabang = "";
	$namanasabah = "";
	$custsex = "";
	
	$tsql = "select * from Tbl_customermasterperson2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
			$custsex = $row["custsex"];
			
			if($custsex == "0")
			{
				$namanasabah = $row["custbusname"];
			}
			else
			{
				$namanasabah = $row["custfullname"];
			}
		}
	}
	
	$mkk_kebutuhan = "";
	$mkk_character = "";
	$mkk_condition = "";
	$mkk_capacity = "";
	$mkk_capital = "";
	$mkk_collateral = "";
	
	$tsql = "select * from Tbl_MKKAnalisa5c where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$mkk_kebutuhan = $row['analisa5c_kebutuhan'];
			$mkk_character = $row['analisa5c_character'];
			$mkk_condition = $row['analisa5c_condition'];
			$mkk_capacity = $row['analisa5c_capacity'];
			$mkk_capital = $row['analisa5c_capital'];
			$mkk_collateral = $row['analisa5c_collateral'];
		}
	}
	
	require ("../neraca/perhitungan.php");
	require("hasilanalisa.php");

for($x=1;$x<=5;$x++)
	{
		//echo 'angsuranpertahun'.$x.'</br>';
		$angsuranpertahun[$x]=array();
		for($i=1;$i<=5;$i++)
		{
			
			$strsql="select cast(((".$x."00000000+(".$x."00000000* CAST(BUNGA_PERSEN as dec(18,2)))*".$i."/100)/(12*".$i.")) as int) 
					as 'angsuran' from TBL_MASTER_BUNGA
					where BUNGA_MIN<".$x."00000000 and BUNGA_MAX>=".$x."00000000
					";
					//echo $strsql."<br/>";
			$a = sqlsrv_query($conn, $strsql);
			if ( $a === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($a))
			{  
				if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					//echo $manmaximumangsuranbaru.'>'.$row['angsuran'].'</br>';
					if($manmaximumangsuranbaru>$row['angsuran'])
					{
						 
						$tmptblangsuran=number_format($row['angsuran']);
					}
					else
					{
						$tmptblangsuran='&nbsp;';
					}
				}
			}
			array_push($angsuranpertahun[$x],$tmptblangsuran);
		}
		//echo '<br/>';
	}
	
	
	

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MKK</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js" ></script>
<script type="text/javascript" src="../../js/accounting.js" ></script>
<link href="../../css/d.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" >
function save()
{
	document.getElementById("frm").action = "do_analisa.php";
	submitform = window.confirm("<?=$confmsg;?>")
	if (submitform == true)
	{
		document.getElementById("frm").submit();
		return true;
	}
	else
	{
		return false;
	}
}
</script>

</head>
<body>
<table width="1000px" align="center" style="border:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Hasil Analisa 5 C Calon Debitur</td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:700px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:700px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">Nama Calon Debitur</td>
	<td width="75%"><div style="border:1px solid black;width:700px;"><? echo $namanasabah;?></div></td>
</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
	
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Kebutuhan Debitur</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Kebutuhan Investasi</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>ADA KEBUTUHAN</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($idrkebutuhaninvestasi);?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslkebutuhaninvestasi;?></td>
				<td style="border:1px solid black;width:250px;" rowspan="2" align="center"><?echo $kesanalisakebutuhandebitur?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Kebutuhan Modal Kerja (WI Needs) </td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>WI Positif / ADA KEBUTUHAN</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($idrwimodalkerja);?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslwimodalkerja;?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Character Calon Debitur</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Hasil BI Checking</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>BAIK</i></td>
				<td style="border:1px solid black;width:500px;" colspan="2" align="center"><?echo $hslbic?></td>
				<td style="border:1px solid black;width:250px;" rowspan="3" align="center"><?echo $keschar?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Hasil Trade Checking</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>BAIK</i></td>
				<td style="border:1px solid black;width:500px;" colspan="2" align="center"><? echo $hslsupp?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Hasil Community Checking</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>BAIK</i></td>
				<td style="border:1px solid black;width:500px;" colspan="2" align="center"><?echo $hslkomunitas?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Condition Calon Debitur</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Lama Usaha (tahun)</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Minimum 2 tahun</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $lkcdlamausahayear?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hsllamausaha?></td>
				<td style="border:1px solid black;width:250px;" rowspan="4" align="center"><?echo $kescon?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Sektor Industri</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Diutamakan Trading/Service</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $sektor_nama?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslsektornama?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Omset Usaha/ Sales</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Minimum 300 juta/th</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($proyeksi_proforma_omset)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslomset?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Profitability</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Positif</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($proyeksi_proforma_pendapatanbersih)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslprofit?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Capacity Calon Debitur</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Repayment Capacity (Kemampuan membayar angsuran baru di Bank Mega)</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Ketentuan: Angsuran baru maksimum 70% dari pendapatan bersih</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($idrratio).'%'?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslratio?></td>
				<td style="border:1px solid black;width:250px;" rowspan="4" align="center"><?echo $kescapacity;?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Repayment Capacity Ratio (Repayment Capacity dibagi angsuran baru di Bank Mega)</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Ketentuan: Minimum 1 kali</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo substr($idrcapratio,0,4)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslcapratio?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Maksimum Plafond yang diajukan</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><?echo number_format($idrmaximumplafond);?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($totalplafondyangdiajukan)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslmaxplafond?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Maksimum angsuran baru</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><?echo number_format($manmaximumangsuranbaru)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($idrmaximumangsuranbaru)?></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $hslmaximumangsuranbaru?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Capital Debitur</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" rowspan="2" align="center">Persentase Pembiayaan Investasi </td>
				<td style="border:1px solid black;width:250px;color:red;" rowspan="2" align="center"><i>Max 70% dari Kebutuhan Investasi</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo round($prnkebutuhaninvestasi)?> %</td>
				<td style="border:1px solid black;width:250px;" rowspan="2" align="center"><?echo $hslprnkebutuhaninvestasi?></td>
				<td style="border:1px solid black;width:250px;" rowspan="4" align="center"><?echo $kescapital?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($totalinvestasi)?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" rowspan="2" align="center">Persentase Pembiayaan Modal Kerja</td>
				<td style="border:1px solid black;width:250px;color:red;" rowspan="2" align="center"><i>Max 80% dari Kebutuhan Modal Kerja*</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><?echo $prnkebutuhanmodalkerja;?></td>
				<td style="border:1px solid black;width:250px;" rowspan="2" align="center"><?echo $hslkebutuhanmodalkerja?></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center"><?echo number_format($idrkebutuhanmodalkerja);?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan="3"><i>*) Maksimal 80% kebutuhan modal kerja yang dibiayai oleh Bank (termasuk Bank lain)</i></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;">Analisa Collateral</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" >
			<tr>
				<td style="width:250px;" align="center">&nbsp;</td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Mandatory</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" colspan="2" align="center"><strong>Hasil</strong></td>
				<td style="border:1px solid black;width:250px;background-color:green;" align="center"><strong>Kesimpulan</strong></td>
			</tr>
			<tr>
				<td style="border:1px solid black;width:250px;" align="center">Total Colateral Coverage - TCC (%)</td>
				<td style="border:1px solid black;width:250px;color:red;" align="center"><i>Maksimum 100%</i></td>
				<td style="border:1px solid black;width:250px;" align="center"><? echo round($prnlikuidasi)?> %</td>
				<td style="border:1px solid black;width:250px;" align="center"><? echo $hsllikuidasi?></td>
				<td style="border:1px solid black;width:250px;" rowspan="4" align="center"><? echo $hsllikuidasi?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="input">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:18px;background-color:gray">BATASAN PLAFOND DAN ANGSURAN</td>
</tr>
<tr>
	<td colspan="2" style="" align="left" ><strong>Kombinasi angsuran dan tenor yang dapat dipilih</strong></td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="input" border="0">
			<tr>
				<td style="width:60%">
					<table width="96%" align="center" style="border:0px solid black;" class="input" >
			
					<tr>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>Plafond</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>1 Tahun</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>2 Tahun</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>3 Tahun</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>4 Tahun</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>5 Tahun</strong></td>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">100 juta</td>
						<?
						for ($i=0;$i<=count($angsuranpertahun[1])-1;$i++)
						{
							echo '<td style="border:1px solid black;width:100px;" align="center">'.$angsuranpertahun[1][$i].'</td>';
						}
						?>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">s.d 200 Juta</td>
						<?
						for ($i=0;$i<=count($angsuranpertahun[2])-1;$i++)
						{
							echo '<td style="border:1px solid black;width:100px;" align="center">'.$angsuranpertahun[2][$i].'</td>';
						}
						?>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">s.d 300 Juta</td>
						<?
						for ($i=0;$i<=count($angsuranpertahun[3])-1;$i++)
						{
							echo '<td style="border:1px solid black;width:100px;" align="center">'.$angsuranpertahun[3][$i].'</td>';
						}
						?>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">s.d 400 Juta</td>
						<?
						for ($i=0;$i<=count($angsuranpertahun[4])-1;$i++)
						{
							echo '<td style="border:1px solid black;width:100px;" align="center">'.$angsuranpertahun[4][$i].'</td>';
						}
						?>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">s.d 500 Juta</td>
						<?
						for ($i=0;$i<=count($angsuranpertahun[5])-1;$i++)
						{
							echo '<td style="border:1px solid black;width:100px;" align="center">'.$angsuranpertahun[5][$i].'</td>';
						}
						?>
					</tr>
					</table>
				</td>
				<td style="width:360px">
					<table width="96%" align="center" style="border:0px solid black;" class="input" border="0">
					<tr>
						<td style="border:1px solid black;width:100%;background-color:gray;" colspan="3" align="center">
						<strong>Plafond dan angsuran sesuai capacity dan kecukupan agunan cadeb</strong>
						</td>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center">&nbsp;</td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>Maksimum Pembiayaan</strong></td>
						<td style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>Diajukan Cadeb</strong></td>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center"><strong>Plafond</strong></td>
						<td style="border:1px solid black;width:100px;" align="center"><?echo number_format($idrmaximumplafond);?></td>
						<td style="border:1px solid black;width:100px;" align="center"><?echo number_format($totalplafondyangdiajukan)?></td>
					</tr>
					<tr>
						<td style="border:1px solid black;width:100px;" align="center"><strong>Angsuran (per-bln)</strong></td>
						<td style="border:1px solid black;width:100px;" align="center"><?echo number_format($manmaximumangsuranbaru)?></td>
						<td style="border:1px solid black;width:100px;" align="center"><?echo number_format($idrmaximumangsuranbaru)?></td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style=""><i>Keterangan : Jika angka total angsuran keluar, artinya kombinasi plafond dan tenor tersebut dapat dipilih  </i></td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
	
	<form id="frm" name="frm" action="do_analisa.php" method="post">
	<? 
	
	require ("../../requirepage/hiddenfield.php");
	if($userid != "" &&$userwfid=="HACD")
	{
	?>
	
	<div style="text-align:center;">
		<input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="buttonsaveflow" onclick="save()"/>
	</div>
	<?
	}
	
	?>
	</form>
	<?require("../../requirepage/btnprint.php");?>
</body>
</html>