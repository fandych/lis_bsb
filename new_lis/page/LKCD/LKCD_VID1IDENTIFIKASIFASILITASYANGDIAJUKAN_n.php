<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	$view = $_GET['view'];
	$taginput = "";
	$tagselect = "";
	$warna = "class=\"input\"";
	$button = "type=\"button\"";
	$tampil = "1";

	if($view == "preview")
	{
		$taginput = "readonly=\"readonly\"";
		$tagselect = "disabled=\"disabled\"";
		$warna = "class=\"preview2\"";
		$button = "type=\"hidden\"";
		$tampil = "0";
	}


	require ("../../requirepage/currency.php");

	$jenispermohonankredit = "";
	$backtoback = "";

	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["custcreditstatus"];
			$backtoback = $row["custcreditstatus"];
		}
	}

	$tsql = "select status_name from TblCreditStatus where status_code = '$jenispermohonankredit'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["status_name"];
		}
	}


	$bunga = 0;
	$bungatotal = 0;
	$bungapersen = 0;

	$tsql = "select custcreditplafond from tbl_CustomerFacility2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["custcreditplafond"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	$tsql = "SELECT * FROM TBL_LKCDFASILITASPINJAMANBANKMEGA2 WHERE FASILITASPINJAMANBANKMEGA_JENISFASILITAS IN (SELECT JENISFASILITASBANKMEGA_CODE FROM TBL_LKCDJENISFASILITASBANKMEGA WHERE JENISFASILITASBANKMEGA_FLAG = 'UKM') AND CUSTNOMID = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["FasilitasPinjamanBankMega_outstanding"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	//$tsqlproduk = "select BUNGA_PERSEN from TBL_MASTER_BUNGA where BUNGA_MIN < '$bungatotal' AND BUNGA_MAX >= '$bungatotal'";
	$tsqlproduk = "SELECT sukubunga as BUNGA_PERSEN FROM tbl_customerfacility WHERE custnomid = '$custnomid'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungapersen = $rowproduk["BUNGA_PERSEN"];
		}
	}

	//bunga ORI

	$bungaORI = 0;
	$bungatotalORI = 0;
	$bungapersenORI = 0;

	$tsql = "select custcreditplafond from tbl_CustomerFacility where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bungaORI = $row["custcreditplafond"];
			$bungatotalORI = $bungatotalORI + $bungaORI;
		}
	}

	$tsql = "SELECT * FROM TBL_LKCDFASILITASPINJAMANBANKMEGA WHERE FASILITASPINJAMANBANKMEGA_JENISFASILITAS IN (SELECT JENISFASILITASBANKMEGA_CODE FROM TBL_LKCDJENISFASILITASBANKMEGA WHERE JENISFASILITASBANKMEGA_FLAG = 'UKM') AND CUSTNOMID = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bungaORI = $row["FasilitasPinjamanBankMega_outstanding"];
			$bungatotalORI = $bungatotalORI + $bungaORI;
		}
	}
	
	//$tsqlproduk = "select BUNGA_PERSEN from TBL_MASTER_BUNGA where BUNGA_MIN < '$bungatotal' AND BUNGA_MAX >= '$bungatotal'";
	$tsqlproduk = "SELECT sukubunga as BUNGA_PERSEN FROM tbl_customerfacility WHERE custnomid = '$custnomid'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungapersenORI = $rowproduk["BUNGA_PERSEN"];
		}
	}

	$bungaBTB = 0;
	$tsqlproduk = "SELECT CONTROL_VALUE from MS_CONTROL where CONTROL_CODE = 'BTB'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungaBTB = $rowproduk["CONTROL_VALUE"];
		}
	}

	if($backtoback == "BB")
	{
		$bungapersen = $bungaBTB;
		$bungapersenORI = $bungaBTB;
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="formentry1" name="formentry1" method="post">
<table width="100%" align="center" style="border:1px solid black;"  <? echo $warna; ?>>
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">IDENTIFIKASI FASILITAS YANG DIAJUKAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td width="20%">Jenis Permohonan Kredit</td>
	<td width="80%"><div style="border:1px solid black;width:200px;"><? echo $jenispermohonankredit;?></div></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td style="border:1px solid black;width:100px;" align="center">Jenis Fasilitas</td>
			<td style="border:1px solid black;width:100px;" align="center">Tujuan Pengajuan Kredit</td>
			<td style="border:1px solid black;width:100px;" align="center">Plafond (<? echo $currency; ?>)</td>
			<td style="border:1px solid black;width:100px;" align="center">Jangka waktu (Bulan)</td>
			<td style="border:1px solid black;width:100px;" align="center">Suku bunga yang diberikan (%)</td>
			<td style="border:1px solid black;width:100px;" align="center">Suku bunga minimal (%)</td>
			<td style="border:1px solid black;width:100px;" align="center">Keterangan</td>
		</tr>

		<?
			//ori
			$jenisfasilitasORI = "";
			$tujuanpengajuankreditORI = "";
			$plafondORI = 0;
			$jangkawaktuORI = "";
			$seqORI = "";
			$bungadiberikanORI = "";


			$tsqlORI = "select custcredittype, custcreditneed, custcreditplafond, custcreditlong, custfacseq, sukubungayangdiberikan from tbl_CustomerFacility where custnomid = '$custnomid'";
			$aORI = sqlsrv_query($conn, $tsqlORI);

			if ( $aORI === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($aORI))
			{
				while($rowORI = sqlsrv_fetch_array($aORI, SQLSRV_FETCH_ASSOC))
				{
					$jenisfasilitasORI = $rowORI["custcredittype"];
					$tujuanpengajuankreditORI = $rowORI["custcreditneed"];
					$plafondORI = $rowORI["custcreditplafond"];
					$jangkawaktuORI = $rowORI["custcreditlong"];
					$seqORI = $rowORI["custfacseq"];
					$bungadiberikanORI = $rowORI["sukubungayangdiberikan"];

					$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasORI'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$jenisfasilitasORI = $rowproduk["produk_type_description"];
						}
					}

					$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanpengajuankreditORI'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$tujuanpengajuankreditORI = $rowproduk["credit_need_name"];
						}
					}






		?>
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $jenisfasilitasORI;?></td>
			<td style="border:1px solid black;" align="center"><? echo $tujuanpengajuankreditORI;?></td>
			<td style="border:1px solid black;" align="center"><? echo numberFormat($plafondORI);?></td>
			<td style="border:1px solid black;" align="center"><? echo $jangkawaktuORI;?></td>
			<td style="border:1px solid black;" align="center"><? echo $bungadiberikanORI;?> </td>
			<td style="border:1px solid black;" align="center"><? echo $bungapersenORI; ?></td>
			<td style="border:1px solid black;" align="center"><? if($backtoback == "BB"){ echo "Effective p.a"; } else { echo "Flat p.a";}?>
			<input type="hidden" id="<? echo 'seq'.$seq;?>" name="seq"  value='<? echo $seqORI; ?>'></td>
		</tr>

				<?
				}
			}
			sqlsrv_free_stmt( $a );


			//table 2
			$jenisfasilitas = "";
			$tujuanpengajuankredit = "";
			$plafond = 0;
			$jangkawaktu = "";
			$seq = "";
			$bungadiberikan = "";
			$idseq = 0;


			$tsql = "select custcredittype, custcreditneed, custcreditplafond, custcreditlong, custfacseq, sukubungayangdiberikan from tbl_CustomerFacility2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$idseq++;
					$jenisfasilitas = $row["custcredittype"];
					$tujuanpengajuankredit = $row["custcreditneed"];
					$plafond = $row["custcreditplafond"];
					$jangkawaktu = $row["custcreditlong"];
					$seq = $row["custfacseq"];
					$bungadiberikan = $row["sukubungayangdiberikan"];

					$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitas'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$jenisfasilitas = $rowproduk["produk_type_description"];
						}
					}

					$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanpengajuankredit'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$tujuanpengajuankredit = $rowproduk["credit_need_name"];
						}
					}
			if($tampil == "0")
			{
		?>

		<tr>
			<td style="border:1px solid black;" align="center"><? echo $jenisfasilitas;?></td>
			<td style="border:1px solid black;" align="center"><? echo $tujuanpengajuankredit;?></td>
			<td style="border:1px solid black;" align="center"><? echo numberFormat($plafond);?></td>
			<td style="border:1px solid black;" align="center"><? echo $jangkawaktu;?></td>
			<td style="border:1px solid black;" align="center"><? echo $bungadiberikan;?> </td>
			<td style="border:1px solid black;" align="center"><? echo $bungapersen; ?></td>
			<td style="border:1px solid black;" align="center"><? if($backtoback == "BB"){ echo "Effective p.a"; } else { echo "Flat p.a";}?>
			<input type="hidden" id="<? echo 'seq'.$seq;?>" name="seq"  value='<? echo $seq; ?>'></td>
		</tr>
			<?}else{?>
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $jenisfasilitas;?></td>
			<td style="border:1px solid black;" align="center"><? echo $tujuanpengajuankredit;?></td>
			<td style="border:1px solid black;" align="center"><? echo numberFormat($plafond);?></td>
			<td style="border:1px solid black;" align="center"><input type="text" <? echo $taginput; ?> id="<? echo 'jangkawaktu'.$idseq;?>" nai="Jangka waktu <? echo $seq;?>" onKeyPress="return isNumberKey(event)"  style="width:120px;background:#ff0;" value="<? echo $jangkawaktu;?>" /> </td>
			<td style="border:1px solid black;" align="center"><input type="text" <? echo $taginput; ?> id="<? echo 'bungadiberikan'.$idseq;?>" nai="Bunga diberikan <? echo $seq;?>" style="width:120px;background:#ff0;" value="<? echo $bungadiberikan;?>" maxlength="5"/> </td>
			<td style="border:1px solid black;" align="center"><? echo $bungapersen; ?></td>
			<td style="border:1px solid black;" align="center"><? if($backtoback == "BB"){ echo "Effective p.a"; } else { echo "Flat p.a";}?>
			<input type="hidden" id="<? echo 'seq'.$seq;?>" name="seq"  value='<? echo $seq; ?>'></td>
		</tr>



		<?
		}
				}
			}
		?>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="7"><input <? echo $button; ?> value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" title="IDENTIFIKASI FASILITAS YANG DIAJUKAN"/></td>
		</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
		</table>
	</td>
</tr>
</table>
</form>
<script type="text/javascript">
	function kumi() {
		var nan="SAVE1";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();

		var max = <? echo $seq;?>;

			var Chars ="0123456789.";
		for (vark = 1; vark<=<? echo $seq ?>; vark++)
		{
		    hitungkoma = 0;
			for (var i = 0; i < eval("document.formentry1.bungadiberikan" + vark + ".value.length"); i++)
			{
				if (Chars.indexOf(eval("document.formentry1.bungadiberikan" + vark + ".value.charAt(i)")) == -1)
				{
					alert("Harus Berupa Angka atau .");
					eval("document.formentry1.bungadiberikan" + vark + ".focus()");
					return false;
				}
				if (eval("document.formentry1.bungadiberikan" + vark + ".value.substring(i,i+1)") == ".")
				{
					hitungkoma++;
				}
			}
			if (hitungkoma > 1)
			{
				alert("Koma 1 saja");
				eval("document.formentry1.bungadiberikan" + vark + ".focus()");
				return false;
			}
			varlen = eval("document.formentry1.bungadiberikan" + vark + ".value.length");

			if (eval("document.formentry1.bungadiberikan" + vark + ".value.substring(varlen-1)") == ".")
			{
				alert("Koma jangan Di belakang");
				eval("document.formentry1.bungadiberikan" + vark + ".focus()");
				return false;
			}
		}
		//alert(seq);

		var FormName="formentry1";
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{

				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false
					break;
				}
			}
		}
		var StatusAllowSubmit=true;
		if(StatusAllowSubmit == true)
		{
			submitform = window.confirm("<? echo $confmsg;?>")
			if (submitform == true)
			{
			for (var i = 01; i<= max; i++)
			{
				var jangkawaktu=$("#jangkawaktu"+i+"").val();
				var bungadiberikan=$("#bungadiberikan"+i+"").val();
				var seq=$("#seq"+i+"").val();

				if(bungadiberikan < <? echo $bungapersen;?>)
				{
					alert('Bunga yang diberikan harus lebih besar dari suku bunga minimal');
					break;
				}
				else if (bungadiberikan > 99)
				{
					alert('Bunga yang diberikan tidak boleh lebih dari 100%');
					break;
				}
				else
				{
					$.ajax({
						type: "POST",
						url: "LKCD_VIDajax_n.php",
						data: "nan="+nan+"&seq="+seq+"&jangkawaktu="+jangkawaktu+"&bungadiberikan="+bungadiberikan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
						success: function(response)
						{	//alert(response);
							$("#sugananako").html(response);
						}
					});
					alert('Suku bunga ke '+i+' berhasil disimpan');
				}
			}
			}
		}



		//alert('Data berhasil disimpan');

	}
</script>
</body>
</html>
