<?php
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");

function PMT($i, $n, $p) {

return $i * $p * pow((1 + $i), $n) / (1 - pow((1 + $i), $n));

}

$plafond = $_REQUEST["plafond"];
$periode = $_REQUEST["periode"];
$bunga = $_REQUEST["bunga"];

$resultPMT = round(-PMT($bunga/100/12, $periode, $plafond), 2);


$angsuranpokok = 0;
$saldopokok = $plafond;
$jmlangsuran = 0;
$jmlangsuranpokok = 0;
$jmgangsuranbunga = 0;
$angsuranperbulan = $resultPMT;
for($i = 1; $i <= $periode ; $i++)
{	
	//$saldopokok = $saldopokok - $angsuranpokok;
	
	$angsuranbunga = round($saldopokok*($bunga/12/100), 2);
	$angsuranpokok = round($resultPMT - $angsuranbunga, 2);
	$saldopokok = round($saldopokok - $angsuranpokok, 2);
	

	
	$jmlangsuran = $jmlangsuran + $angsuranperbulan;
	$jmlangsuranpokok = $jmlangsuranpokok + $angsuranpokok;
	$jmgangsuranbunga = $jmgangsuranbunga + $angsuranbunga;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<title>Anuitas</title>
</head>
<body>

<table align="center" cellspacing="0" border="0px" cellpadding="5px">
<tr>
	<td width="150px"></td>
	<td width="150px" style="border:1px solid black;text-align:right;" bgcolor="green"><font color="white"><strong>Pokok</strong></font></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat($plafond);?></td>
	<td width="150px" style="border:1px solid black;">Rupiah</td>
	<td width="150px"></td>
</tr>
<tr>
	<td width="150px"></td>
	<td width="150px" style="border:1px solid black;text-align:right;" bgcolor="green"><font color="white"><strong>Jumlah Periode</strong></font></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo $periode;?></td>
	<td width="150px" style="border:1px solid black;">Bulan</td>
	<td width="150px"></td>
</tr>
<tr>
	<td width="150px"></td>
	<td width="150px" style="border:1px solid black;text-align:right;" bgcolor="green"><font color="white"><strong>Bunga Efektif</strong></font></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo $bunga;?></td>
	<td width="150px" style="border:1px solid black;">% per anum</td>
	<td width="150px"></td>
</tr>
<tr>
	<td width="150px"></td>
	<td width="150px" style="border:1px solid black;text-align:right;" bgcolor="green"><font color="white"><strong>Angsuran</strong></font></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat($resultPMT);?></td>
	<td width="150px" style="border:1px solid black;">Rp per bulan</td>
	<td width="150px"></td>
</tr>
<tr bgcolor="green">
	<td width="150px" style="border:1px solid black;"><font color="white"><strong>Periode Angsuran</strong></font></td>
	<td width="150px" style="border:1px solid black;"><font color="white"><strong>Jumlah Angsuran</strong></font></td>
	<td width="150px" style="border:1px solid black;"><font color="white"><strong>Saldo Pokok</strong></font></td>
	<td width="150px" style="border:1px solid black;"><font color="white"><strong>Angsuran Pokok</strong></font></td>
	<td width="150px" style="border:1px solid black;"><font color="white"><strong>Angsuran Bunga</strong></font></td>
</tr>

<tr bgcolor="yellow">
	<td width="150px" style="border:1px solid black;text-align:center;">0</td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($jmlangsuran,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($plafond,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($jmlangsuranpokok,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($jmgangsuranbunga,0));?></td>
</tr>

<?php

$angsuranpokok = 0;
$saldopokok = $plafond;
$angsuranperbulan = $resultPMT;
for($i = 1; $i <= $periode ; $i++)
{	
	//$saldopokok = $saldopokok - $angsuranpokok;
	
	$angsuranbunga = round($saldopokok*($bunga/12/100), 2);
	$angsuranpokok = round($resultPMT - $angsuranbunga, 2);
	$saldopokok = round($saldopokok - $angsuranpokok, 2);
	
	
?>
<tr>
	<td width="150px" style="border:1px solid black;text-align:center;"><?php echo $i;?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($angsuranperbulan,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($saldopokok,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($angsuranpokok,0));?></td>
	<td width="150px" style="border:1px solid black;text-align:right;"><?php echo numberFormat(round($angsuranbunga,0));?></td>
</tr>

<?php

}
?>
</table>
</body>
</html>