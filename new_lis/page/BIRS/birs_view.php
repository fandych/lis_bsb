<?
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");

  $custcif = "";
	$strsql="SELECT * FROM Tbl_CustomerMasterPerson2
      				 WHERE custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcif = $rows['custaplno'];
		}
	}

    $statusdoc="";
    $kondisistatusdoc = "";
		$strsqlv01="SELECT * FROM Tbl_DocPerson WHERE doc_segmen = 'BIC'";
		$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
		if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlconv01))
		{
			while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
			{
		    $statusdoc .= $rowsv01['doc_id'] . ",";
        $kondisistatusdoc .= "doc_type='" . $rowsv01['doc_id'] . "' or ";
			}
		}

    if ($kondisistatusdoc != "")
    {
	     $kondisistatusdoc = "AND (" . substr($kondisistatusdoc,0,strlen($kondisistatusdoc)-3) . ")";
    } 

  $ipdm = "";
  $userdm = "";
	$strsql="select control_value from ms_control where control_code='IPDM'";
	//echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$rows[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}
$key = $custcif . "_" . $custnomid;
$linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
$linkdmA = $ipdm. '/external_view.php?dmuserid=' . $userdm . '&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=cek&key=' . $key . '&dmstatusdoc=' . $statusdoc;
	
	
	
?>
<html>
	<head>
		<title>Appraisal</title>
		<link rel="stylesheet" href="../../bin/css/css-bj.css" type="text/css" />
		<link rel="shortcut icon" href="../../bin/img/favicon.png" type="image/x-icon">
		<link href="../../bin/css/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
		<script src="../../bin/css/SpryMenuBar.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/jquery-1.11.3.js" ></script>
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/bootstrap.min.js" ></script>
		
		<script type="text/javascript" src="../../bin/js/datatable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstraptable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstrap-datepicker.min.js"></script>
		
		<link href="../../bin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
		
		<script language="javascript" type="text/javascript" src="../../bin/javascript/niceforms.js"></script>
		<link href="../../bin/css/table.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable();
			} );
		</script>
		
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<script type="text/javascript" src="../../js/my.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script>
			function uploadDocument(thelink)
			{
	        varwidth = 980;
	        varheight = 640;
	        varx = 0;
	        vary = 0;
          window.open(thelink,'lainnya','scrollbars=yes,width='+varwidth+',height='+varheight+',screenX='+varx+',screenY='+vary+',top='+varx+',left='+vary+',status=yes');
			}
		</script>
	</head>
	<body style="margin:0;" class="preview2">
		<br>
		<form id="frm" name="frm" method="post">
			<?
		      if($userid != "" && $userwfid!="")
		      {
		    ?>
		      <div style="float: right;margin-right: 220px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
		    <?
		      }
		    ?>
		    <BR><BR>
			<div class="divcenter">
				<table border="1" style ="width:900px; border-color:black;" align="center" id="tblpreview">
					<tr>
						<td align="center" colspan="2" style="font-size:14pt;height:70px;">BI Result - <A HREF="javascript:uploadDocument('<? echo $linkdmA ?>')">Lihat Hasil Upload</A></td>
					</tr>
					<tr>
						<td colspan="2">
						</br>
						<?echo '<iframe src="birs_view_detail.php?custnomid='.$custnomid.'&userid='.$userid.'&userpwd='.$userpwd.'&userbranch='.$userbranch.'&userregion='.$userregion.'&userwfid=PRO&userpermission=A&buttonaction='.$buttonaction.'&view=preview" height="400" width="100%"></iframe>';?>
								
						</br>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" height="50px">
							<?
							if($userid != "" && $userwfid!="")
							{
							require ("../../requirepage/btnview.php");
							require ("../../requirepage/hiddenfield.php");
							}
							require("../../requirepage/btnprint.php");
						?>
						</td>
					</tr>
				</table>
				<div>&nbsp;</div>
										<input type="hidden" id="txtcustlisid" name="txtcustlisid" alt="Customer LIS ID" maxlength="50" value="<?php echo $_custlistid?>" style="width:100%; background-color:#ffff00;"/>
										<input id="txtnorek" name="txtnorek" alt="No Rekening" type="hidden" maxlength="100" value="<?php echo $_norek?>" style="width:100%; background-color:#ffff00;" />
		</form>
	<?
    require("../../requirepage/backtoflow.php");
	?>
	</body>
</html>