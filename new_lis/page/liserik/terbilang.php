<?php
	function Terbilang($x)
	{
	  $abil = array(" ", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

	  if ($x < 12)
		return " " . $abil[$x];
	  elseif ($x < 20)
		return Terbilang($x - 10) . " belas";
	  elseif ($x < 100)
		return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
	  elseif ($x < 200)
		return " seratus" . Terbilang($x - 100);
	  elseif ($x < 1000)
		return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
	  elseif ($x < 2000)
		return " seribu" . Terbilang($x - 1000);
	  elseif ($x < 1000000)
		return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
	  elseif ($x < 1000000000)
		return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
	}
	
	function TerbilangPersen($x)
	{
	  $x = explode(".",$x);
	  $dk = $x[0];
	  $bk = 0;
	  if(count($x) > 1)
		$bk = $x[1];
	  
	  if ($dk == 0)
		return "nol koma" . Terbilang($bk);
	  elseif ($bk == "" || $bk == 00)
		return Terbilang($dk);
	  elseif ($bk != "")
		return Terbilang($dk) . " koma" . Terbilang($bk);
	}


	//Cara penggunaannya
	//echo ucwords(Terbilang("200000"));
?>
