<?	
require ("../../lib/formatError.php");
require ("../../lib/open_con.php");

date_default_timezone_set('Asia/Jakarta');
$date = date('d/m/Y', time());
$datemonthbefore = "";
$datemonthbeforemin = "";
$datemonthbeforeplus = "";
$nominal = "";
$jatuhtempo = "";
$custnomid = "";
$nohp = "";

$tsql = "SELECT replace(convert(NVARCHAR, DATEADD(MONTH, -1, GETDATE()), 103), ' ', '/') as now, replace(convert(NVARCHAR, DATEADD(MONTH, -1, DATEADD(DAY, -3, GETDATE())), 111), ' ', '/') as now2, replace(convert(NVARCHAR, DATEADD(MONTH, -1, DATEADD(DAY, 3, GETDATE())), 111), ' ', '/') as now3";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$datemonthbefore = $row['now'];
		$datemonthbeforemin = $row['now2'];
		$datemonthbeforeplus = $row['now3'];
	}
}
sqlsrv_free_stmt( $sqlConn );

//Hari H

$tsql = "select a.custnomid, b.mkknewangsuran, c.custhp from tbl_newpk a, tbl_CustomerFacility2 b, Tbl_CustomerMasterPerson2 c where a.pktanggal = '$datemonthbefore' and a.custnomid = b.custnomid AND b.custnomid = c.custnomid AND ( b.custfacseq NOT IN (SELECT c.seq from tbl_collect_payment c where c.custnomid = b.custnomid))";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$custnomid = $row['custnomid'];
		$nohp = $row['custhp'];
		$nominal = $row['mkknewangsuran'];
		$jatuhtempo = $date;
		$message = "Kpd Yth ".$custnomid.", tagihan Anda sebesar ".$nominal.", jatuh tempo tanggal ".$jatuhtempo.". Mohon segera lakukan pembayaran.";
		
		$tsql2 = "INSERT INTO tbl_sms_payment (custnomid, notelp, message) VALUES ('$custnomid', '$nohp', '$message')";
		$b = sqlsrv_query($conn, $tsql2);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	}
}
sqlsrv_free_stmt( $sqlConn );

//3 hari sebelum

$tsql = "select a.custnomid, b.mkknewangsuran, c.custhp from tbl_newpk a, tbl_CustomerFacility2 b, Tbl_CustomerMasterPerson2 c where a.pktanggal = '$datemonthbeforemin' and a.custnomid = b.custnomid AND b.custnomid = c.custnomid AND ( b.custfacseq NOT IN (SELECT c.seq from tbl_collect_payment c where c.custnomid = b.custnomid))";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$custnomid = $row['custnomid'];
		$nohp = $row['custhp'];
		$nominal = $row['mkknewangsuran'];
		$jatuhtempo = $date;
		$message = "Kpd Yth ".$custnomid.", sekedar mengingatkan tagihan Anda sebesar ".$nominal.", jatuh tempo tanggal ".$jatuhtempo.". Mohon segera lakukan pembayaran.";
		
		$tsql2 = "INSERT INTO tbl_sms_payment (custnomid, notelp, message) VALUES ('$custnomid', '$nohp', '$message')";
		$b = sqlsrv_query($conn, $tsql2);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	}
}
sqlsrv_free_stmt( $sqlConn );


//3 hari sesudah

$tsql = "select a.custnomid, b.mkknewangsuran, c.custhp from tbl_newpk a, tbl_CustomerFacility2 b, Tbl_CustomerMasterPerson2 c where a.pktanggal = '$datemonthbeforeplus' and a.custnomid = b.custnomid AND b.custnomid = c.custnomid AND ( b.custfacseq NOT IN (SELECT c.seq from tbl_collect_payment c where c.custnomid = b.custnomid))";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$custnomid = $row['custnomid'];
		$nohp = $row['custhp'];
		$nominal = $row['mkknewangsuran'];
		$jatuhtempo = $date;
		$message = "Kpd Yth ".$custnomid.", tagihan Anda sebesar ".$nominal.", jatuh tempo tanggal ".$jatuhtempo.". Mohon segera lakukan pembayaran. Sebelum kami pindahkan ke divisi collector.";
		
		$tsql2 = "INSERT INTO tbl_sms_payment (custnomid, notelp, message) VALUES ('$custnomid', '$nohp', '$message')";
		$b = sqlsrv_query($conn, $tsql2);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	}
}
sqlsrv_free_stmt( $sqlConn );


//Telah Bayar

$tsql = "select z.custnomid, z.seq, c.custhp, z.datepayment, z.nominal from tbl_collect_payment z, Tbl_CustomerMasterPerson2 c where statussms = 'N' AND z.custnomid = c.custnomid";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);

if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$custnomid = $row['custnomid'];
		$nohp = $row['custhp'];
		$nominal = $row['nominal'];
		$jatuhtempo = $row['datepayment']->format('Y/m/d');
		$seq = $row['seq'];
		
		$message = "Nasabah Yth, terima kasih pembayaran angsuran anda ".$custnomid.". Sebesar ".$nominal." telah kami terima tanggal ".$jatuhtempo.". Untuk menghubungi kami silakan telepon call center XXX.";
		
		$tsql2 = "INSERT INTO tbl_sms_payment (custnomid, notelp, message) VALUES ('$custnomid', '$nohp', '$message')";
		$b = sqlsrv_query($conn, $tsql2);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		
		$tsql2 = "UPDATE tbl_collect_payment set statussms = 'Y' where custnomid = '$custnomid' AND seq = '$seq'";
		$b = sqlsrv_query($conn, $tsql2);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	}
}
sqlsrv_free_stmt( $sqlConn );


	
?>