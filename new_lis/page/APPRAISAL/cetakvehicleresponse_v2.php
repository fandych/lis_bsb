<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");
	require_once ("../../lib/open_con_apr.php");
	
	$liscolid = $_GET["liscolid"];
	$custnomid = $_GET["custnomid"];
	
	//ECHO $liscolid;
	
	$namacalondebitur = "";
	$alamat = "";
	$cabang = "";
	$namaao = "";
	$tanggalsurvey = "";
	$tanggalreport = "";
	$yangdihubungi = "";
	
	$tsql = "SELECT CUST_NAME, CUST_ADDR1, CUST_ADDR2, CUST_ADDR3, (SELECT BRANCH_NAME FROM TBL_BRANCH WHERE BRANCH_CODE IN (SELECT AP_BRANCH FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid')) AS CABANG, (SELECT USER_NAME FROM TBL_SE_USER WHERE USER_ID IN (SELECT TXN_USER_ID FROM TBL_FSTART  WHERE TXN_ID = '$custnomid')) AS NAMAAO, (SELECT AP_DATE FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid') AS TANGGALSURVEY, CUST_YANGDIHUBUNGI FROM TBL_COL_CUSTOMER WHERE AP_LISREGNO = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$namacalondebitur = $row["CUST_NAME"];
			$alamat = $row["CUST_ADDR1"];
			$cabang = $row["CABANG"];
			$namaao = $row["NAMAAO"];
			$tanggalsurvey = $row["TANGGALSURVEY"]->format('d F Y');
			$tanggalreport = $row["TANGGALSURVEY"]->format('d F Y');
			$yangdihubungi = $row["CUST_YANGDIHUBUNGI"];
			
			if($row["CUST_ADDR2"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR2"];
			}
			
			if($row["CUST_ADDR3"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR3"];
			}
		}
	}
	
	$NOMORPOLISI = "";
	$MERK = "";
	$TYPE = "";
	$MODEL = "";
	$JENISKENDARAAN = "";
	$TAHUNPEMBUATAN = "";
	$ISISILINDER = "";
	$WARNA = "";
	$NOMORCHASIS = "";
	$NOMORMESIN = "";
	$BAHANBAKAR = "";
	$NOBPKB = "";
	$TANGGALBPKB = "";
	$NOSTNK = "";
	$TANGGALSTNK = "";
	$NOFAKTUR = "";
	$TANGGALFAKTUR = "";
	$NAMABPKB = "";
	$ALAMATBPKB = "";
	$PERLENGKAPAN = "";
	$KETERANGAN = "";
	$NILAIWAJAR = "";
	$NILAILIKUIDASI = "";
	$SAFETYMARGIN = "";
	
	$tsql = "SELECT POLICE_NO AS NOMORPOLISI, MERK AS MERK, TYPE AS TYPE, MODEL AS MODEL, JNS_KENDARAAN AS JENISKENDARAAN, THNPEMBUATAN AS TAHUNPEMBUATAN, SILINDER_ISI AS ISISILINDER, SILINDER_WRN AS WARNA, NORANGKA AS NOMORCHASIS, NOMESIN AS NOMORMESIN, BAHANBAKAR AS BAHANBAKAR, BPKB_NO AS NOBPKB, BPKB_TGL AS TANGGALBPKB, STNK_NO AS NOSTNK, STNK_EXP AS TANGGALSTNK, FAKTUR_NO AS NOFAKTUR, FAKTUR_TGL AS TANGGALFAKTUR, BPKB_NAMA AS NAMABPKB, BPKB_ADDR1, BPKB_ADDR2, BPKB_ADDR3, PERLENGKAPAN AS PERLENGKAPAN, KETERANGAN AS KETERANGAN, NILAIWAJAR, SAFETYMARGIN, NIL_LIKUIDASI AS NILAILIKUIDASI FROM APPRAISAL_VHC WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_VHC WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$NOMORPOLISI = $row["NOMORPOLISI"];
			$MERK = $row["MERK"];
			$TYPE = $row["TYPE"];
			$MODEL = $row["MODEL"];
			$JENISKENDARAAN = $row["JENISKENDARAAN"];
			$TAHUNPEMBUATAN = $row["TAHUNPEMBUATAN"];
			$ISISILINDER = $row["ISISILINDER"];
			$WARNA = $row["WARNA"];
			$NOMORCHASIS = $row["NOMORCHASIS"];
			$NOMORMESIN = $row["NOMORMESIN"];
			$BAHANBAKAR = $row["BAHANBAKAR"];
			$NOBPKB = $row["NOBPKB"];
			$TANGGALBPKB = $row["TANGGALBPKB"];
			$NOSTNK = $row["NOSTNK"];
			$TANGGALSTNK = $row["TANGGALSTNK"];
			$NOFAKTUR = $row["NOFAKTUR"];
			$TANGGALFAKTUR = $row["TANGGALFAKTUR"];
			$NAMABPKB = $row["NAMABPKB"];
			$PERLENGKAPAN = $row["PERLENGKAPAN"];
			$NILAIWAJAR = $row["NILAIWAJAR"];
			$NILAILIKUIDASI = $row["NILAILIKUIDASI"];
			$SAFETYMARGIN = $row["SAFETYMARGIN"];
			$ALAMATBPKB = $row["BPKB_ADDR1"];
			
			if($row["BPKB_ADDR2"] != "")
			{
				$ALAMATBPKB = $ALAMATBPKB." ".$row["BPKB_ADDR2"];
			}
			
			if($row["BPKB_ADDR3"] != "")
			{
				$ALAMATBPKB = $ALAMATBPKB." ".$row["BPKB_ADDR3"];
			}
			
			if($TANGGALBPKB != "")
			{
				$TANGGALBPKB = $row["TANGGALBPKB"]->format('d F Y');
			}
			
			if($TANGGALSTNK != "")
			{
				$TANGGALSTNK = $row["TANGGALSTNK"]->format('d F Y');
			}
			
			if($TANGGALFAKTUR != "")
			{
				$TANGGALFAKTUR = $row["TANGGALFAKTUR"]->format('d F Y');
			}
		}
	}
	
	$tsql = "SELECT * FROM RFMERK_VHC WHERE MERK_CODE = '$MERK'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$MERK = $row["MERK_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFTYPE_VHC WHERE TYPEID = '$TYPE'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$TYPE = $row["TYPE_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFMODEL_VHC WHERE MODEL_CODE = '$MODEL'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$MODEL = $row["MODEL_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFJENIS_VHC WHERE JENIS_CODE = '$JENISKENDARAAN'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$JENISKENDARAAN = $row["JENIS_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFBB_VHC WHERE BB_CODE = '$BAHANBAKAR'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$BAHANBAKAR = $row["BB_DESC"];
		}
	}
	
	$OPINIPOSITIF = "";
	$OPININEGATIF = "";
	$KONDISIPASAR = "";
	$CATATAN = "";
	
	$tsql = "SELECT EVALPOS AS OPINIPOSITIF, EVALNEG AS OPININEGATIF, MK_CODE AS KONDISIPASAR, EVALNOTE AS CATATAN FROM APPRAISAL_OPINI WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_VHC WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$OPINIPOSITIF = $row["OPINIPOSITIF"];
			$OPININEGATIF = $row["OPININEGATIF"];
			$KONDISIPASAR = $row["KONDISIPASAR"];
			$CATATAN = $row["CATATAN"];
		}
	}
	
	$tsql = "SELECT * FROM RFMK WHERE MK_ID = '$KONDISIPASAR'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$KONDISIPASAR = $row["MK_DESC"];
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>
	Letter ColType04 Page
</title>

    <style type="text/css">
        .text1
        {
	        font-family:Arial;
	        font-size:x-small;
	        vertical-align:top;
	        text-align:justify;
        }
        
        .style1
        {
            width: 1%;   
        }

        .tablebox1
        {
            background-color:white;
            border-collapse: collapse; 
            table-layout: fixed; 
        }
        .judul
        {
           background-color: #CCCCCC; 
        }
    
    </style>
    <script type="text/javascript">
        function onPrint()
        {
            var f = document.form1;
            f.btn_print.style.display = "none"; window.print();
            f.btn_print.style.display = "";
        
        }
    </script>


    </head>
<body>
    <form name="form1" method="post" action="TPL_ColType04.aspx?tc=9.0&amp;regno=00012%2f805%2f2013&amp;curef=PE130206000001&amp;mcolid=PE13020600000103&amp;cetak=html&amp;rnq=1" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTE4NjkyNjk3Mg9kFgICAw9kFlYCEQ8PFgIeBFRleHQFCUtFTkRBUkFBTmRkAhMPDxYCHwAFJTAwMDAwMDAwNy84MDUvTGFwLUFwcC9GQUMwNC8gIElJLzIwMTNkZAIVDw8WAh8ABQtORVcgVEFLU0FTSWRkAhcPDxYCHwAFClBUIENBTERFUkFkZAIZDw8WAh8ABQMxICBkZAIbDw8WAh8ABRJSRUdJT05BTCBKQUtBUlRBIElkZAIdDw8WAh8AZWRkAh8PDxYCHwAFDzYgRmVicnVhcnkgMjAxM2RkAiEPDxYCHwAFDzYgRmVicnVhcnkgMjAxM2RkAiMPDxYCHwBkZGQCJQ8PFgIfAAUEb2NlcGRkAicPDxYCHwAFBDEyMzRkZAIpDw8WAh8ABRVBVURJLyBTRURBTi8gU1QgV0FHT05kZAIrDw8WAh8ABQtNT0JJTCBCRUJBTmRkAi0PDxYCHwAFBDIwMTJkZAIvDw8WAh8ABQQxNTEwZGQCMQ8PFgIfAAUGc2lsdmVyZGQCMw8PFgIfAAUMOTU5NTk1OTU5NTk1ZGQCNQ8PFgIfAAUJOTg3OTk3OTc5ZGQCNw8PFgIfAAUHUHJlbWl1bWRkAjkPDxYCHwAFBDc2NzZkZAI7Dw8WAh8ABQ82IEZlYnJ1YXJ5IDIwMTNkZAI9Dw8WAh8ABQQ1NDMyZGQCPw8PFgIfAAUPNiBGZWJydWFyeSAyMDEzZGQCQQ8PFgIfAAUDNDU2ZGQCQw8PFgIfAAUPNiBGZWJydWFyeSAyMDEzZGQCRQ8PFgIfAAUNYXByIG5hbWEgYnBrYmRkAkcPDxYCHwAFEWFwciBhbGFtYXQgYnBrYiAgZGQCSQ8PFgIfAAUQYXByIHBlcmxlbmdrYXBhbmRkAksPDxYCHwAFDmFwciBrZXRlcmFuZ2FuZGQCTQ88KwANAQAPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnRmZGQCTw88KwANAQAPFgQfAWcfAmZkZAJRDw8WAh8ABRVBVURJLyBTRURBTi8gU1QgV0FHT05kZAJTDw8WAh8ABQs1MDAsMDAwLDAwMGRkAlUPDxYCHwAFCzUwMCwwMDAsMDAwZGQCVw8PFgIfAAUCMTBkZAJZDw8WAh8ABQs0NTAsMDAwLDAwMGRkAlsPFgIeB1Zpc2libGVnFgJmD2QWCgIBDw8WAh8ABRVBVURJLyBTRURBTi8gU1QgV0FHT05kZAIDDw8WAh8ABQEwZGQCBQ8PFgIfAAUBMGRkAgcPDxYCHwAFAjEwZGQCCQ8PFgIfAAUBMGRkAl0PDxYCHwAFBWFwciArZGQCXw8PFgIfAAUFYXByIC1kZAJhDw8WAh8ABQpNYXJrZXRhYmxlZGQCYw8PFgIfAAULYXByIGNhdGF0YW5kZAJlDw8WAh8ABeoCPHRhYmxlIHdpZHRoPSIxMDAlIj4gPHRyPjx0ZCB3aWR0aD0iMzMlIj5QZW5pbGFpPC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQgd2lkdGg9IjMzJSI+UGVtZXJpa3NhPC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+Jm5ic3A7PC90ZD48L3RyPjx0cj48dGQ+KFIuIFRvbm55IEhhcnRvbm8pPC90ZD48dGQ+Jm5ic3A7PC90ZD48dGQ+KEVyZmFuIFlvaGFubmVzIC0gUk8gSmFrYXJ0YSk8L3RkPjwvdHI+PC90YWJsZT5kZBgCBRFHcmlkVmlld0luZm9IYXJnYQ88KwAKAQhmZAUMR3JpZFZpZXdJbnNzDzwrAAoBCGZkio/BEmhka0cMSlG3XaSzDKJBqY4=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/LOSAPPRAISAL-MEGA_DEV/WebResource.axd?d=E4Ieyflk7p3-YbMDJHLeVw2&amp;t=634354376266562500" type="text/javascript"></script>

<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
WebForm_ReEnableControls();
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
	<input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWCALH/6v7DgKjo8rIBwLW/LanAwLohPPtCQK1ltGAAgKkh9WRCAKo6PHFAgKn3u2kBIhe9a1KRnyn223tAwCblW03ulxC" />
</div>
    <div>
        <input name="h_AP_REGNO" type="hidden" id="h_AP_REGNO" />
        <input name="h_COLMASTER_ID" type="hidden" id="h_COLMASTER_ID" value="PE13020600000103" />
        <input name="h_COL_ID" type="hidden" id="h_COL_ID" value="PE13020600000103VHC01" />
        <input name="h_CU_REF" type="hidden" id="h_CU_REF" />
    
        <input name="h_APP_OFFICER" type="hidden" id="h_APP_OFFICER" value="R. Tonny Hartono" />
        <input name="h_SPV_OFFICER" type="hidden" id="h_SPV_OFFICER" value="Erfan Yohannes - RO Jakarta" />
        <input name="h_PINCA" type="hidden" id="h_PINCA" />

        <table width="100%" class="tablebox1" border="1">
            <tr>
                <td colspan="6">
                    <table width="100%" class="judul">
                        <tr>
                            <td rowspan="5" class="style1">
                                <!--<img src="../Image/megalogo.jpg" id="Img1" />--> </td>
                        </tr>
                        <tr>
                            <td align="center" ><strong>LAPORAN PENILAIAN</strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>
                                <span id="COLTYPE_DESC">KENDARAAN</span></strong></td>
                        </tr>
                        <!--<tr>
                            <td align="center">
                                NO.&nbsp<span id="NOREPORT">000000007/805/Lap-App/FAC04/  II/2013</span>
                            </td>
                        </tr>-->
                        <tr>
                            <td align="center"><strong>
                                <span id="TAXATYPE_DESC">NEW TAKSASI</span></strong>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td style="width: 100%" colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">I.</td>
                            <td>UMUM</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td>1.</td>
                            <td width="30%">Nama Calon Debitur</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%"><span id="CU_NAME"><? echo $namacalondebitur;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Alamat</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%"><span id="CU_ADDR"><? echo $alamat;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Cabang / AO</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                                <span id="BRANCHNAME"><? echo $cabang;?></span>&nbsp/&nbsp<span id="AO_USERID"> <? echo $namaao;?></span>                                
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Tanggal Survey / Report</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="EVALSURVEYDATE"><? echo $tanggalsurvey;?></span>&nbsp/&nbsp<span id="TGL_TRACK_APR"><? echo $tanggalreport?></span>
                            </td> 
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Penilaian Terdahulu</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%"><span id="LASTTAXA_DATE1"></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Yang dihubungi</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                            <span id="CU_CONTACT"><? echo $yangdihubungi;?></span>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">II.</td>
                            <td>IDENTIFIKASI KENDARAAN</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Nomor Polisi</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="POLICE_NO"><? echo $NOMORPOLISI;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Merk/ Type/ Model</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="MERK_TYPE1"><? ECHO $MERK;?>/ <? ECHO $TYPE;?>/ <? ECHO $MODEL;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Jenis Kendaraan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="JENIS_DESC"><? ECHO $JENISKENDARAAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Tahun Pebuatan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="THNPEMBUATAN"><? ECHO $TAHUNPEMBUATAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Isi Silinder/Warna</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="SILINDER_ISI"><? ECHO $ISISILINDER;?></span>/
                                            <span id="SILINDER_WRN"><? ECHO $WARNA;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Nomor Chasis/ Rangka</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="NORANGKA"><? ECHO $NOMORCHASIS;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Nomor Mesin</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="NOMESIN"><? ECHO $NOMORMESIN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>8.</td>
                            <td width="30%">Bahan Bakar</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="BB_DESC"><? ECHO $BAHANBAKAR;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">9.</td>
                            <td align="left" width="30%">No. BPKB/ Tanggal</td>
                            <td width="2%">:</td>
                            <td><span id="BPKB_NO"><? ECHO $NOBPKB;?></span></td>
                            <td>Tgl.<span id="BPKB_TGL"><? ECHO $TANGGALBPKB;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>10.</td>
                            <td width="30%">No. STNK/ Berlaku s.d</td>
                            <td width="2%">:</td>
                            <td><span id="STNK_NO"><? ECHO $NOSTNK;?></span></td>
                            <td>Tgl.<span id="STNK_EXP"><? ECHO $TANGGALSTNK;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>11.</td>
                            <td width="30%">No. Faktur</td>
                            <td width="2%">:</td>
                            <td><span id="FAKTUR_NO"><? ECHO $NOFAKTUR;?></span></td>
                            <td>Tgl.<span id="FAKTUR_TGL"><? ECHO $TANGGALFAKTUR;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>12.</td>
                            <td width="30%">Nama/ Alamat</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="BPKB_NAMA"><? ECHO $NAMABPKB;?></span>/ 
                                            <span id="BPKB_ADDR"><? ECHO $ALAMATBPKB;?>  </span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>13.</td>
                            <td width="30%">Perlengkapan Kendaraan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="PERLENGKAPAN"><? ECHO $PERLENGKAPAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>14.</td>
                            <td width="30%">Keterangan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="KETERANGAN"><? ECHO $KETERANGAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">III.</td>
                            <td>ASURANSI</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">
                    <div>

</div>
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">IV.</td>
                            <td>KONSULTASI/ INFORMASI HARGA</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td align="left" colspan="6">
                    <div>

</div>
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">V.</td>
                            <td>PENILAIAN</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">Uraian
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td width="4%">1.</td>
                            <td width="20%">Nilai Wajar</td>
                            <td width="2%">:</td>
                            <td width="25%"><span id="MERK_TYPE"><? ECHO $MERK;?>/ <? ECHO $TYPE;?>/ <? ECHO $MODEL;?></span>
                                &nbsp;</td>
                            <td width="25%"><span id="NILAIWAJAR"><? ECHO numberFormat($NILAIWAJAR);?></span>
                                &nbsp;</td>
                            <td width="2%">=&nbsp;</td>
                            <td align="right"><span id="TOTAL_NILWAJAR"><? ECHO numberFormat($NILAIWAJAR);?></span>
                                &nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td>Safety Margin</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                            <td><span id="SAFETYMARGIN"><? ECHO $SAFETYMARGIN;?></span>
                                &nbsp;%</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td>Nilai Likuidasi</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right"><span id="NIL_LIKUIDASI"><? ECHO numberFormat($NILAILIKUIDASI);?></span>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td colspan="6">Uraian Override
                </td>
            </tr>
            
            <tr id="TR_OVR" class="text1">
	<td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td width="4%">1.</td>
                            <td width="20%">Nilai Wajar</td>
                            <td width="2%">:</td>
                            <td width="25%"><span id="T_MERK_TYPE">AUDI/ SEDAN/ ST WAGON</span>
                                &nbsp;</td>
                            <td width="25%"><span id="OVR_NILAIWAJAR">0</span>
                                &nbsp;</td>
                            <td width="2%">
                                =&nbsp;</td>
                            <td align="right"><span id="OVR_TOTAL_NILAIWAJAR">0</span>
                                &nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td>Safety Margin</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                            <td><span id="T_SAFETYMARGIN"><? echo $SAFETYMARGIN;?></span>
                                &nbsp; %</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td>Nilai Likuidasi</td>
                            <td>:</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right"><span id="OVR_LIKUIDASI">0</span>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
</tr>

            
            <tr class="text1">
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">VI.</td>
                            <td>OPINI PENILAI</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <table width="100%">
                        <tr class="text1">
                            <td align="left" width="2%">-</td>
                            <td align="left" width="15%">Positif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALPOS"><? ECHO $OPINIPOSITIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Negatif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNEG"><? ECHO $OPININEGATIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Kondisi Pasar</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="MK_DESC"><? ECHO $KONDISIPASAR;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Catatan/ Alasan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNOTE"><? ECHO $CATATAN;?></span></td>
                        </tr>
                    
                    </table>
                </td>
            </tr>

            <!--<tr class="text1">
                <td align="left" colspan="6">
                    <span id="TTD"><table width="100%"> <tr><td width="33%">Penilai</td><td>&nbsp;</td><td width="33%">Pemeriksa</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>(R. Tonny Hartono)</td><td>&nbsp;</td><td>(Erfan Yohannes - RO Jakarta)</td></tr></table></span>
                </td>
            </tr>-->
            </table>  
    <input type="button" id="btn_print" value="Print" onclick="onPrint()" />
              
    </div>
    
<script type="text/javascript">
//<![CDATA[
var __enabledControlArray =  new Array('h_AP_REGNO', 'h_COLMASTER_ID', 'h_COL_ID', 'h_CU_REF', 'h_APP_OFFICER', 'h_SPV_OFFICER', 'h_PINCA');
//]]>
</script>

</form>
</body>
</html>
