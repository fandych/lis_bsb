<?
	require ('../../lib/formatError.php');
	require ('../../lib/open_con.php');
	
	$tgl1=$_REQUEST['tgl1'];
	$tgl2=$_REQUEST['tgl2'];
	$region=$_REQUEST['regioncode'];
	$branch=$_REQUEST['branchcode'];
	$status=$_REQUEST['status'];
	$admin=$_REQUEST['inadmin'];
	
	if($tgl1==""){
		$tglStart = "";
	} else {
		$tglStart = date("d/m/Y", strtotime("$tgl1"));
	}
	
	if($tgl1==""){
		$tglFinish = "";
	} else {
		$tglFinish = date("d/m/Y", strtotime("$tgl2"));
	}
	if($admin == "888"){
		if($region == "" || $region == "all"){
			$KondisiRegion = "";
		} else {
			$KondisiRegion = "and fn.txn_region_code='$region'";
		} 
	} else {
		if($region == "" || $region == "all"){
			$KondisiRegion = "and fn.txn_region_code='$admin'";
		} else {
			$KondisiRegion = "and fn.txn_region_code='$region'";
		} 
	}
	
	if($branch ==""){
		$KondisiBranch = "";
	} else {
		$KondisiBranch = "and fn.txn_branch_code='$branch'";
	}
	
	if($status=="A"){
		$KondisiApprove = ",Tbl_FBIR fb, Tbl_FNOM fn, Tbl_CustomerMasterPerson cmp where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=cmp.custnomid and  custflagbi='F' and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code";
	} else if ($status=="I"){
		$KondisiApprove = ",Tbl_FIBC fb, Tbl_FNOM fn, Tbl_CustomerMasterPerson cmp where cf.custnomid=fb.txn_id and cf.custnomid=fn.txn_id and cf.custnomid=cmp.custnomid and (custflagbi='I' or custflagbi='Y') and fb.txn_action='A' and fb.txn_time between CONVERT(datetime,'$tgl1',121) and CONVERT(datetime,'$tgl2',121) $KondisiRegion $KondisiBranch order by fn.txn_region_code,fn.txn_branch_code";
	}
	
	
	
	
?>
<html>
<head>
<script type="text/javascript" src="js/datetimepicker_css.js"></script>
<title>REPORT BICHECKING</title>
</head>
<body>
<table align="center">
	<tr>
		<td>Laporan Pengikatan Harian Tgl <strong><? echo $tglStart;?></strong> s/d <strong><? echo $tglFinish;?></strong></td>
	</tr>
</table>
<?
	$sql = "select custfullname,custbranchcode,custcreditplafond1,custcreditplafond2,pk_no from Tbl_FLGP f, Tbl_CustomerMasterPerson cmp, tbl_pk p where f.txn_id = cmp.custnomid and p.pk_custnomid = cmp.custnomid and txn_action='A' and TXN_TIME BETWEEN '$tgl1' AND dateadd(d,1,'$tgl2')";
	$sql = "select cmp.custsex,cmp.custbusname, cf.custnomid as [id] ,fn.txn_region_code as [region] ,fn.txn_branch_code as [branch], custfullname , fb.txn_action as[status] from Tbl_CustomerFlag cf $KondisiApprove";
	//echo $sql; exit;
	$rs = sqlsrv_query($conn,$sql);
	$errs = sqlsrv_errors();
	if ( $rs === false){die( FormatErrors( sqlsrv_errors()));}
	if(sqlsrv_has_rows($rs))
	{
?>
	<table cellspacing="0" cellpadding="0" border="1" width="800" align="center">
		<tr>
			<td width="2%"><div align="center"><strong>NO</strong><div></td>
			<td width="10%"><div align="center"><strong>REGIONAL</strong><div></td>
			<td width="10%"><div align="center"><strong>CABANG</strong><div></td>
			<td width="20%"><div align="center"><strong>APPLICATION ID</strong><div></td>
			<td width="35%"><div align="center"><strong>NAMA DEBITUR</strong><div></td>
			<td width="15%"><div align="center"><strong>PROCESS</strong><div></td>
			<td width="8%"><div align="center"><strong>APLLICATION STATUS</strong><div></td>
		</tr>
<?
		$i=0;
		$grandtot="";
		while($rowrs = sqlsrv_fetch_array( $rs, SQLSRV_FETCH_ASSOC))
		{
			$ApplicationID = $rowrs['id'];
			$RegionCode = $rowrs['region'];
			$BranchCode = $rowrs['branch'];
			if($rowrs['custsex']=="0"){
				$NamaDebitur = $rowrs['custbusname'];
			} else {
				$NamaDebitur = $rowrs['custfullname'];
			}
			$StatusApp = $rowrs['status'];
			$sqls = "select txn_flow from Tbl_Flow_Status where txn_id='$ApplicationID'";
			$rss = sqlsrv_query($conn,$sqls);
			if($rss === false){
				die( FormatErrors( sqlsrv_errors()));
			}
			if(sqlsrv_has_rows($rss)){
				if($row=sqlsrv_fetch_array ($rss, SQLSRV_FETCH_ASSOC)){
					$Flow = $row['txn_flow'];
				}
			}
			
			//echo $ApplicationID;
			$i++;
			/*$NamaDebitur = $rowrs['custfullname'];
			$Plafond1 = $rowrs['custcreditplafond1'];
			$Plafond2 = $rowrs['custcreditplafond2'];
			$tempPla = $Plafond1+$Plafond2;
			$grandtot = $grandtot + $tempPla;
			$totPla = number_format($Plafond1+$Plafond2);
			$KodeCabang = $rowrs['custbranchcode'];
			$tsql = "select * from Tbl_Branch where branch_code = '$KodeCabang'";
			$rstsql = sqlsrv_query ($conn,$tsql);
			if ( $rstsql === false){die( FormatErrors( sqlsrv_errors()));}
			if(sqlsrv_has_rows($rstsql))
			{
				if($rowtsql = sqlsrv_fetch_array( $rstsql, SQLSRV_FETCH_ASSOC))
				{
					$NamaCabang = $rowtsql['branch_name'];
				}
			}
			else { $NamaCabang = "";}
			$NomorPk = $rowrs['pk_no'];
			*/
			/*?>
				<tr><td><? echo $i;?></td><td><? echo $NamaDebitur;?></td><td><? echo $KodeCabang." - ".$NamaCabang;?></td><td><div align="right"><? echo $NomorPk;?></div></td><td><div align="right"><? echo $totPla;?></div></td></tr>
			<?*/
			?>
				<tr><td><?echo $i;?></td><td><? echo $RegionCode; ?></td><td><? echo $BranchCode;?></td><td><?echo $ApplicationID; ?></td><td><? echo $NamaDebitur;?></td><td><? echo $Flow;?></td><td><? echo $status;?></td></tr>
			<?
		}
	}
	else
	{
		?>
			<table cellspacing="0" cellpadding="0" border="1" width="800" align="center">
			<tr>
				<td><div align="center"><strong>Tidak ada data dalam Periode <? echo $tglStart;?> s/d <? echo $tglFinish;?></strong></div></td>
			</tr>
		<?
	}
?>
</body>
</html>