<?php
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");

if(isset($_REQUEST['act'])){
		$act = $_REQUEST['act'];
}else{
	$act = "";
}

if($act=="")
{
	$id = $_POST["id"];
	$custnomid = $_POST["custnomid"];
	$nama_debitur = $_POST["nama_debitur"];
	$sifat = $_POST["sifat"];
	$norek = $_POST["norek"];
	$tanggal_permintaan = $_POST["tanggal_permintaan"];
	$nama_bank = $_POST["nama_bank"];
	$ket_kondisi = $_POST["ket_kondisi"];
	$jenis_fasilitas = $_POST["jenis_fasilitas"];
	$baki_debet = str_replace(",", "", $_POST["baki_debet"]);
	$sektor_ekonomi = $_POST["sektor_ekonomi"];
	$tanggal_mulai = $_POST["tanggal_mulai"];
	$akad_awal = $_POST["tanggal_mulai"];
	$jatuh_tempo = $_POST["jatuh_tempo"];
	$input_sid = $_POST["input_sid"];
	$kolektibilitas = $_POST["kolektibilitas"];
	$kolektibilitas_terendah = $_POST["kolektibilitas_terendah"];
	$jumlah_hari_tunggakan = $_POST["jumlah_hari_tunggakan"];
	$flag = $_POST["flag"];
	
	if($id!="")
	{
		$strsql = "UPDATE tbl_birs set custnomid = '$custnomid',
					nama_debitur = '$nama_debitur',
					sifat = '$sifat',
					norek = '$norek',
					tanggal_permintaan = '$tanggal_permintaan',
					nama_bank = '$nama_bank',
					ket_kondisi = '$ket_kondisi',
					jenis_fasilitas = '$jenis_fasilitas',
					baki_debet = '$baki_debet',
					sektor_ekonomi = '$sektor_ekonomi',
					tanggal_mulai = '$tanggal_mulai',
					akad_awal = '$akad_awal',
					jatuh_tempo = '$jatuh_tempo',
					input_sid = '$input_sid',
					kolektibilitas = '$kolektibilitas',
					kolektibilitas_terendah = '$kolektibilitas_terendah',
					flag = '$flag',
					jumlah_hari_tunggakan = '$jumlah_hari_tunggakan'
				where idx = '$id'
		";
		$stmt = sqlsrv_prepare( $conn, $strsql);
		if(!$stmt)
		{
		echo "Error in preparing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}
		if(!sqlsrv_execute( $stmt))
		{
		echo "Cannot insert table ". $strsql;
		die( print_r( sqlsrv_errors(), true));
		}	
		sqlsrv_free_stmt( $stmt);
		
		if($flag=="1"){
			$strsql = "UPDATE Tbl_LKCDFasilitasPinjamanBankLain set custnomid = '$custnomid',
						FasilitasPinjamanBankLain_type = 'PT',
						FasilitasPinjamanBankLain_jenisfasilitas = '$jenis_fasilitas',
						FasilitasPinjamanBankLain_namabank = '$nama_bank',
						FasilitasPinjamanBankLain_outstanding = '$baki_debet',
						FasilitasPinjamanBankLain_angsuran = '0',
						FasilitasPinjamanBankLain_kol = '$kolektibilitas',
						FasilitasPinjamanBankLain_dpd = '$jumlah_hari_tunggakan',
						FasilitasPinjamanBankLain_namadebitur = '$nama_debitur',
						FasilitasPinjamanBankLain_jatuhtempo = '$jatuh_tempo'
					where FasilitasPinjamanBankLain_flag = '$id'
					
					UPDATE Tbl_LKCDFasilitasPinjamanBankLain2 set custnomid = '$custnomid',
						FasilitasPinjamanBankLain_type = 'PT',
						FasilitasPinjamanBankLain_jenisfasilitas = '$jenis_fasilitas',
						FasilitasPinjamanBankLain_namabank = '$nama_bank',
						FasilitasPinjamanBankLain_outstanding = '$baki_debet',
						FasilitasPinjamanBankLain_angsuran = '0',
						FasilitasPinjamanBankLain_kol = '$kolektibilitas',
						FasilitasPinjamanBankLain_dpd = '$jumlah_hari_tunggakan',
						FasilitasPinjamanBankLain_namadebitur = '$nama_debitur',
						FasilitasPinjamanBankLain_jatuhtempo = '$jatuh_tempo'
					where FasilitasPinjamanBankLain_flag = '$id'
			";
			$stmt = sqlsrv_prepare( $conn, $strsql);
			if(!$stmt)
			{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
			}
			if(!sqlsrv_execute( $stmt))
			{
			echo "Cannot insert table ". $strsql;
			die( print_r( sqlsrv_errors(), true));
			}	
			sqlsrv_free_stmt( $stmt);
			
		}else if($flag=="0"){
			$strsql = "UPDATE Tbl_LKCDFasilitasPinjamanBankMega set custnomid = '$custnomid',
						FasilitasPinjamanBankMega_type = '',
						FasilitasPinjamanBankMega_jenisfasilitas = '$jenis_fasilitas',
						FasilitasPinjamanBankMega_plafondawal = '$sifat',
						FasilitasPinjamanBankMega_outstanding = '$baki_debet',
						FasilitasPinjamanBankMega_angsuran = '0',
						FasilitasPinjamanBankMega_tenor = '0',
						FasilitasPinjamanBankMega_rate = '0',
						FasilitasPinjamanBankMega_provisi = '0',
						FasilitasPinjamanBankMega_kol = '$kolektibilitas',
						FasilitasPinjamanBankMega_dpd = '$jumlah_hari_tunggakan',
						FasilitasPinjamanBankMega_namadebitur = '$nama_debitur',
						FasilitasPinjamanBankMega_jatuhtempo = '$jatuh_tempo'
					where FasilitasPinjamanBankMega_flag = '$id'
					
					UPDATE Tbl_LKCDFasilitasPinjamanBankMega2 set custnomid = '$custnomid',
						FasilitasPinjamanBankMega_type = '',
						FasilitasPinjamanBankMega_jenisfasilitas = '$jenis_fasilitas',
						FasilitasPinjamanBankMega_plafondawal = '$sifat',
						FasilitasPinjamanBankMega_outstanding = '$baki_debet',
						FasilitasPinjamanBankMega_angsuran = '0',
						FasilitasPinjamanBankMega_tenor = '0',
						FasilitasPinjamanBankMega_rate = '0',
						FasilitasPinjamanBankMega_provisi = '0',
						FasilitasPinjamanBankMega_kol = '$kolektibilitas',
						FasilitasPinjamanBankMega_dpd = '$jumlah_hari_tunggakan',
						FasilitasPinjamanBankMega_namadebitur = '$nama_debitur',
						FasilitasPinjamanBankMega_jatuhtempo = '$jatuh_tempo'
					where FasilitasPinjamanBankMega_flag = '$id'
			";
			$stmt = sqlsrv_prepare( $conn, $strsql);
			if(!$stmt)
			{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
			}
			if(!sqlsrv_execute( $stmt))
			{
			echo "Cannot insert table ". $strsql;
			die( print_r( sqlsrv_errors(), true));
			}	
			sqlsrv_free_stmt( $stmt);
		}
	}
	else{
		$strsqlv01="SELECT max(idx)+1 as counter, count(*) as count FROM tbl_birs";
		$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
		if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlconv01))
		{
			if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
			{
				$idx = $rowsv01['counter'];	
				$count = $rowsv01['count'];	
			}
		}
		
		if($count=="0"){$idx = 1;}
		
		$strsql = "INSERT INTO tbl_birs (idx,
					custnomid, 
					nama_debitur,
					sifat,
					norek,
					tanggal_permintaan,
					nama_bank,
					ket_kondisi,
					jenis_fasilitas,
					baki_debet,
					sektor_ekonomi,
					tanggal_mulai,
					akad_awal,
					jatuh_tempo,
					input_sid,
					kolektibilitas,
					kolektibilitas_terendah,
					flag,
					jumlah_hari_tunggakan
		) VALUES ('$idx',
					'$custnomid',
					'$nama_debitur',
					'$sifat',
					'$norek',
					'$tanggal_permintaan',
					'$nama_bank',
					'$ket_kondisi',
					'$jenis_fasilitas',
					'$baki_debet',
					'$sektor_ekonomi',
					'$tanggal_mulai',
					'$akad_awal',
					'$jatuh_tempo',
					'$input_sid',
					'$kolektibilitas',
					'$kolektibilitas_terendah',
					'$flag',
					'$jumlah_hari_tunggakan'
		)";
		//echo $strsql;exit;
		$stmt = sqlsrv_prepare( $conn, $strsql);
		if(!$stmt)
		{
		echo "Error in preparing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}
		if(!sqlsrv_execute( $stmt))
		{
		echo "Cannot insert table ". $strsql;
		die( print_r( sqlsrv_errors(), true));
		}	
		sqlsrv_free_stmt( $stmt);
		
		if($flag=="1"){		
			$counter="1";
			$strsqlv01="SELECT max(FasilitasPinjamanBankLain_seq)+1 as counter FROM Tbl_LKCDFasilitasPinjamanBankLain WHERE custnomid = '$custnomid'";
			$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
			if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($sqlconv01))
			{
				if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
				{
					$counter = $rowsv01['counter'];	
				}
			}
			
			$strsql = "INSERT INTO Tbl_LKCDFasilitasPinjamanBankLain (custnomid, 
						FasilitasPinjamanBankLain_seq,
						FasilitasPinjamanBankLain_type,
						FasilitasPinjamanBankLain_jenisfasilitas,
						FasilitasPinjamanBankLain_namabank,
						FasilitasPinjamanBankLain_outstanding,
						FasilitasPinjamanBankLain_angsuran,
						FasilitasPinjamanBankLain_kol,
						FasilitasPinjamanBankLain_dpd,
						FasilitasPinjamanBankLain_flag,
						FasilitasPinjamanBankLain_namadebitur,
						FasilitasPinjamanBankLain_jatuhtempo
			) VALUES ('$custnomid',
						'$counter',
						'PT',
						'$jenis_fasilitas',
						'$nama_bank',
						'$baki_debet',
						'0',
						'$kolektibilitas',
						'$jumlah_hari_tunggakan',
						'$idx',
						'$nama_debitur',
						'$jatuh_tempo')
						
						INSERT INTO Tbl_LKCDFasilitasPinjamanBankLain2 (custnomid, 
						FasilitasPinjamanBankLain_seq,
						FasilitasPinjamanBankLain_type,
						FasilitasPinjamanBankLain_jenisfasilitas,
						FasilitasPinjamanBankLain_namabank,
						FasilitasPinjamanBankLain_outstanding,
						FasilitasPinjamanBankLain_angsuran,
						FasilitasPinjamanBankLain_kol,
						FasilitasPinjamanBankLain_dpd,
						FasilitasPinjamanBankLain_flag,
						FasilitasPinjamanBankLain_namadebitur,
						FasilitasPinjamanBankLain_jatuhtempo
			) VALUES ('$custnomid',
						'$counter',
						'PT',
						'$jenis_fasilitas',
						'$nama_bank',
						'$baki_debet',
						'0',
						'$kolektibilitas',
						'$jumlah_hari_tunggakan',
						'$idx',
						'$nama_debitur',
						'$jatuh_tempo'
			)";
			$stmt = sqlsrv_prepare( $conn, $strsql);
			if(!$stmt)
			{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
			}
			if(!sqlsrv_execute( $stmt))
			{
			echo "Cannot insert table ". $strsql;
			die( print_r( sqlsrv_errors(), true));
			}	
			sqlsrv_free_stmt( $stmt);
		
		}else if($flag=="0"){
			$counter="1";
			$strsqlv01="SELECT max(FasilitasPinjamanBankMega_seq)+1 as counter FROM Tbl_LKCDFasilitasPinjamanBankMega WHERE custnomid = '$custnomid'";
			$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
			if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($sqlconv01))
			{
				if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
				{
					$counter = $rowsv01['counter'];	
				}
			}
			
			$strsql = "INSERT INTO Tbl_LKCDFasilitasPinjamanBankMega (custnomid, 
						FasilitasPinjamanBankMega_seq,
						FasilitasPinjamanBankMega_type,
						FasilitasPinjamanBankMega_jenisfasilitas,
						FasilitasPinjamanBankMega_plafondawal,
						FasilitasPinjamanBankMega_outstanding,
						FasilitasPinjamanBankMega_angsuran,
						FasilitasPinjamanBankMega_tenor,
						FasilitasPinjamanBankMega_rate,
						FasilitasPinjamanBankMega_provisi,
						FasilitasPinjamanBankMega_kol,
						FasilitasPinjamanBankMega_dpd,
						FasilitasPinjamanBankMega_flag,
						FasilitasPinjamanBankMega_namadebitur,
						FasilitasPinjamanBankMega_jatuhtempo
			) VALUES ('$custnomid',
						'$counter',
						'',
						'$jenis_fasilitas',
						'$sifat',
						'$baki_debet',
						'0',
						'0',
						'0',
						'0',
						'$kolektibilitas',
						'$jumlah_hari_tunggakan',
						'$idx',
						'$nama_debitur',
						'$jatuh_tempo'
						)
						
						INSERT INTO Tbl_LKCDFasilitasPinjamanBankMega2 (custnomid, 
						FasilitasPinjamanBankMega_seq,
						FasilitasPinjamanBankMega_type,
						FasilitasPinjamanBankMega_jenisfasilitas,
						FasilitasPinjamanBankMega_plafondawal,
						FasilitasPinjamanBankMega_outstanding,
						FasilitasPinjamanBankMega_angsuran,
						FasilitasPinjamanBankMega_tenor,
						FasilitasPinjamanBankMega_rate,
						FasilitasPinjamanBankMega_provisi,
						FasilitasPinjamanBankMega_kol,
						FasilitasPinjamanBankMega_dpd,
						FasilitasPinjamanBankMega_flag,
						FasilitasPinjamanBankMega_namadebitur,
						FasilitasPinjamanBankMega_jatuhtempo
			) VALUES ('$custnomid',
						'$counter',
						'',
						'$jenis_fasilitas',
						'$sifat',
						'$baki_debet',
						'0',
						'0',
						'0',
						'0',
						'$kolektibilitas',
						'$jumlah_hari_tunggakan',
						'$idx',
						'$nama_debitur',
						'$jatuh_tempo'
			)";
			$stmt = sqlsrv_prepare( $conn, $strsql);
			if(!$stmt)
			{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
			}
			if(!sqlsrv_execute( $stmt))
			{
			echo "Cannot insert table ". $strsql;
			die( print_r( sqlsrv_errors(), true));
			}	
			sqlsrv_free_stmt( $stmt);
		}
	}
	
	header("Location:birs.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction&userbranch=$userbranch&userregion=$userregion&userid=$userid&userpwd=$userpwd");
}
else if($act=="saveflow")
{
	require ("../../requirepage/do_saveflow.php");	
	
	header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}
else
{
	$id = $_REQUEST['id'];
	
	$strsql = "DELETE FROM tbl_birs where idx = '$id'";
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}	
	sqlsrv_free_stmt( $stmt);
	
	$strsql = "DELETE FROM Tbl_LKCDFasilitasPinjamanBankMega where FasilitasPinjamanBankMega_flag = '$id'";
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}	
	sqlsrv_free_stmt( $stmt);
	
	$strsql = "DELETE FROM Tbl_LKCDFasilitasPinjamanBankMega2 where FasilitasPinjamanBankMega_flag = '$id'";
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}	
	sqlsrv_free_stmt( $stmt);
	
	$strsql = "DELETE FROM Tbl_LKCDFasilitasPinjamanBankLain where FasilitasPinjamanBankLain_flag = '$id'";
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}	
	sqlsrv_free_stmt( $stmt);
	
	$strsql = "DELETE FROM Tbl_LKCDFasilitasPinjamanBankLain2 where FasilitasPinjamanBankLain_flag = '$id'";
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}	
	sqlsrv_free_stmt( $stmt);
	header("Location:birs.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction&userbranch=$userbranch&userregion=$userregion&userid=$userid&userpwd=$userpwd");
}


?>