<?php
require('./sqlsrv.php');
require('../../requirepage/parameter.php');

$arrayinputselect = array('Ada','Tidak Ada');

$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";


$custktpno='';
$custaddr='';
$custfullname='';
$strsql='SELECT custktpno,custaddr,custfullname,custnpwpno  FROM Tbl_CustomerMasterPerson2 WHERE custnomid=\''.$custnomid.'\'';
$custnpwpno='';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custktpno=$rs[$i]['custktpno'];
  $custaddr=$rs[$i]['custaddr'];
  $custfullname=$rs[$i]['custfullname'];
  $custnpwpno=$rs[$i]['custnpwpno'];
}

$_pasangan_nomorktp='';
$strsql = 'SELECT _pasangan_nomorktp FROM pl_custpasangan2 WHERE _custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_pasangan_nomorktp=$rs[$i]['_pasangan_nomorktp'];
}


$kartukeluarga = '';
$aktanikah = '';
$copysertifikat = '';
$suratketerangankerja = '';
$slipgaji = '';
$copyrekeningtabungan = '';
$lb = '';
$lt = '';
$perumahan = '';
$buktikepemilikan = '';
$tujuanpermohonankredit = '';
$kewenanganmeminjam = '';
$kewenanganmengagunkan = '';
$rencanaagunankredit = '';
$syaratdanbentukpengikatan = '';
$datainformasidebitur = '';
$tanggallegal = date('Y-m-d');
$menyiapkan = '';
$titlemenyiapkan = '';
$namapemimpin = '';
$titlepemimpin = '';
$untukpembelian = '';
$nomor = '';
$strsql = 'SELECT * FROM LEGAL_OPINION WHERE CUSTNOMID=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $kartukeluarga= $rs[$i]['KARTUKELUARGA'];
  $aktanikah= $rs[$i]['AKTANIKAH'];
  $copysertifikat= $rs[$i]['COPYSERTIFIKAT'];
  $suratketerangankerja= $rs[$i]['SURATKETERANGANKERJA'];
  $slipgaji= $rs[$i]['SLIPGAJI'];
  $copyrekeningtabungan= $rs[$i]['COPYREKENINGTABUNGAN'];
  $lb= $rs[$i]['LB'];
  $lt= $rs[$i]['LT'];
  $perumahan= $rs[$i]['PERUMAHAN'];
  $buktikepemilikan= $rs[$i]['BUKTIKEPEMILIKAN'];
  $tujuanpermohonankredit= $rs[$i]['TUJUANPERMOHONANKREDIT'];
  $kewenanganmeminjam= $rs[$i]['KEWENANGANMEMINJAM'];
  $kewenanganmengagunkan= $rs[$i]['KEWENANGANMENGAGUNKAN'];
  $rencanaagunankredit= $rs[$i]['RENCANAAGUNANKREDIT'];
  $syaratdanbentukpengikatan= $rs[$i]['SYARATDANBENTUKPENGIKATAN'];
  $datainformasidebitur= $rs[$i]['DATAINFORMASIDEBITUR'];
  $tanggallegal= is_null($rs[$i]['TANGGALLEGAL'])? '': $rs[$i]['TANGGALLEGAL']->format('Y-m-d');
  $menyiapkan= $rs[$i]['MENYIAPKAN'];
  $titlemenyiapkan= $rs[$i]['TITLEMENYIAPKAN'];
  $namapemimpin= $rs[$i]['NAMAPEMIMPIN'];
  $titlepemimpin= $rs[$i]['TITLEPEMIMPIN'];
  $untukpembelian= $rs[$i]['UNTUKPEMBELIAN'];
  $nomor= $rs[$i]['NOMOR'];
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Legal Opinion</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
    <script type="text/javascript" src="../../js/tinymce/tinymce.min.js" ></script>

    <script type="text/javascript">
      tinymce.init({  
        selector: "textarea",  
        plugins: [  
          "advlist autolink lists link image charmap anchor",  
          "searchreplace visualblocks code fullscreen",  
          "insertdatetime media table contextmenu paste"  
        ],  
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"  
      });
      function submitform()
      {
		var tanggallegal=$("#tanggallegal").val();

		var tgl = new Date();
		var tglle = new Date(tanggallegal);

        if(tinyMCE.get('tujuanpermohonankredit').getContent()=="")
        {
          $('#tujuanpermohonankredit').focus();
          alert('Tujuan Permohonan Kredit Harus diisi');
        }
        else if($('#nlp').val()=="")
        {
          $('#nlp').focus();
          alert('Nomor Harus diisi');
        }
        else if(tinyMCE.get('kewenanganmeminjam').getContent()=="")
        {
          $('#kewenanganmeminjam').focus();
          alert('Kewenangan Meminjam Harus diisi');
        }
        else if(tinyMCE.get('kewenanganmengagunkan').getContent()=="")
        {
          $('#kewenanganmengagunkan').focus();
          alert('Kewenangan Mengagunkan Harus diisi');

        }
        else if(tinyMCE.get('rencanaagunankredit').getContent()=="")
        {
          $('#rencanaagunankredit').focus();
          alert('Rencana Agunan Kredit Harus diisi');
        }
        else if(tinyMCE.get('syaratdanbentukpengikatan').getContent()=="")
        {
          $('#syaratdanbentukpengikatan').focus();
          alert('Syarat dan Bentuk Pengikatan Harus diisi');
        }
        else if(tinyMCE.get('datainformasidebitur').getContent()=="")
        {
          $('#datainformasidebitur').focus();
          alert('Data Informasi Debitur Harus diisi');
        }
        else if($('#menyiapkan').val()==""){
          $('#menyiapkan').focus();
          alert('Nama Yang Menyiapkan Harus Diisi');
        }
        else if($('#titlemenyiapkan').val()==""){
          $('#titlemenyiapkan').focus();
          alert('Pangkat Yang Menyiapkan Harus Diisi');
        }
        else if($('#namapemimpin').val()==""){
          $('#namapemimpin').focus();
          alert('Nama Pemimpin Harus Diisi');
        }
        else if($('#titlepemimpin').val()==""){
          $('#titlepemimpin').focus();
          alert('Pangkat Pemimpin Harus Diisi');
        }
		else if(tanggallegal!="" && +tglle >= +tgl){
			alert("Tanggal tidak boleh lebih dari tanggal hari ini.");
			$("#tanggallegal").focus();
        }
        else
        {
          $('#frm').submit();
        }

      }
    </script>

    <style media="screen">
      #tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}

    </style>
  </head>
  <body>
    <form id="frm" action="legal_opinion_kkb_exec.php" method="post">
      <div style="float: right;margin-right: 207px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
      <br />
      <br />
      <table id="tableform" border="0" align="center">
        <tr>
          <td>
              <p style="text-align:center;"><b style="text-decoration:underline;">LEGAL OPINION</b></p>
			  <p align="center">Nomor : <input type="text" name="nlp" id="nlp" maxlegnth="50" value="<? echo $nomor; ?>" ></p>
              <p style="text-align:center;font-size:13px;"><a href="../preview/PreviewALL/PreviewALL_n.php?custnomid=<?=$custnomid;?>"><?php echo $custnomid; ?></a></p><br/>
				  
              <p>Sehubungan dengan Permohonan Kredit atas nama <?php echo $custfullname; ?>, maka dapat kami sampaikan hal-hal sebagai berikut :</p>

              <table>
                <tr>
                  <td valign="top" style="width:300px;">Nama Pemohon</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custfullname; ?></td>
                </tr>
                <tr>
                  <td valign="top">Alamat</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custaddr;?></td>
                </tr>
				<tr>
                  <td colspan="3" style="font-weight:bold;">&nbsp;</td>
                </tr>
				<tr>
                  <td colspan="3" style="font-weight:bold;">Legalitas</td>
                </tr>
                <tr>
                  <td valign="top">NIK</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top">NPWP Pribadi</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custnpwpno;?></td>
                </tr>
                <tr>
                  <td valign="top">Kartu Keluarga</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="kartukeluarga" id="kartukeluarga">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$kartukeluarga)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td valign="top">Akte Nikah</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="aktanikah" id="aktanikah">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$aktanikah)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td valign="top">NIK Pasangan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $_pasangan_nomorktp;?></td>
                </tr>
                <tr>
                  <td valign="top">Copy BPKB & STNK</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="copysertifikat" id="copysertifikat">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$copysertifikat)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
              </table>

                <br/>
              <table>
                <tr>
                  <td colspan="3" style="font-weight:bold;"><b>Sumber Pendapatan</b></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Surat Keterangan Pekerjaan</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="suratketerangankerja" id="suratketerangankerja">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$suratketerangankerja)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Slip Gaji</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="slipgaji" id="slipgaji">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$slipgaji)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Copy Rekening Tabungan</td>
                  <td valign="top">:</td>
                  <td valign="top">
                    <select name="copyrekeningtabungan" id="copyrekeningtabungan">
                    <?
                      foreach ($arrayinputselect as $key => $value) {
                        $select='';
                        if($value==$copyrekeningtabungan)
                        {
                          $select='selected = "selected"';
                        }
                        echo '<option '.$select.' value="'.$value.'">'.$value.'</option>';
                      }
                    ?>
                  </td>
                </tr>
              </table>



              <ol type="1">
                <li>
                  <b style="text-decoration:underline;">Maksud dan Tujuan pemohon kredit</b>
                  <br/>
                  <textarea name="tujuanpermohonankredit" id="tujuanpermohonankredit" rows="8" cols="80"><?php echo $tujuanpermohonankredit;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Kewenangan  Meminjam</b><br/>
                  <textarea name="kewenanganmeminjam" id="kewenanganmeminjam" rows="8" cols="80"><?php echo $kewenanganmeminjam;?></textarea>
                </br/>
                </li>
                <li>
                  <b style="text-decoration:underline;">Kewenangan Mengagunkan</b><br/>
                  <textarea name="kewenanganmengagunkan" id="kewenanganmengagunkan" rows="8" cols="80"><?php echo $kewenanganmengagunkan;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Rencana Agunan Kredit</b><br/>
                  <textarea name="rencanaagunankredit" id="rencanaagunankredit" rows="8" cols="80"><?php echo $rencanaagunankredit;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Syarat dan Bentuk Pengikatan</b>
                  <br/>
                  <textarea name="syaratdanbentukpengikatan" id="syaratdanbentukpengikatan" rows="8" cols="80"><?php echo $syaratdanbentukpengikatan;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Data Informasi Debitur</b>
                  <br/>
				  <!--<div>Pinjaman Bank</div>
				  <?=$custfullname."<br>"?>-->
                  <textarea name="datainformasidebitur" id="datainformasidebitur" rows="8" cols="80"><?php echo $datainformasidebitur;?></textarea>
                </li>
              </ol>
			  <!--
			  <hr>
			  <?php
				$custktpno='';
				$custaddr='';
				$custfullname='';
				$strsql='	select FasilitasPinjamanBankLain_namabank,FasilitasPinjamanBankLain_outstanding,FasilitasPinjamanBankLain_angsuran,FasilitasPinjamanBankLain_kol,FasilitasPinjamanBankLain_dpd from Tbl_LKCDFasilitasPinjamanBankLain2
							where custnomid = \''.$custnomid.'\'';
							//echo $strsql;
				$custnpwpno='';
				$rs = $db->_RQ($strsql);
				$lastrow = count($rs)-1;
				for($i=0;$i<count($rs);$i++)
				{
					
					$FasilitasPinjamanBankLain_namabank=$rs[$i]['FasilitasPinjamanBankLain_namabank'];
					$FasilitasPinjamanBankLain_outstanding=$rs[$i]['FasilitasPinjamanBankLain_outstanding'];
					$FasilitasPinjamanBankLain_angsuran=$rs[$i]['FasilitasPinjamanBankLain_angsuran'];
					$FasilitasPinjamanBankLain_kol=$rs[$i]['FasilitasPinjamanBankLain_kol'];
					$FasilitasPinjamanBankLain_dpd=$rs[$i]['FasilitasPinjamanBankLain_dpd'];
					
					if($i==0)
					{
						?>
						<table border="1" style="border-collapse:collapse;width:100%;">
						<tr style="text-align:center">
							<td>Nama Bank</td>
							<td>Outstanding</td>
							<td>Angsuran</td>
							<td>Kolektibilitas</td>
							<td>DPD</td>
						</tr>
						<?
					}
					?>
						<tr style="text-align:center">
							<td><?=$FasilitasPinjamanBankLain_namabank?></td>
							<td><?=number_format($FasilitasPinjamanBankLain_outstanding)?></td>
							<td><?=number_format($FasilitasPinjamanBankLain_angsuran)?></td>
							<td><?=$FasilitasPinjamanBankLain_kol?></td>
							<td><?=$FasilitasPinjamanBankLain_dpd?></td>
						</tr>
					
					<?
					
					if($i == $lastrow)
					{
						?>
						</table>
						<?
					}
				}

			  
			  
			  ?>
			  -->
			  
              <p><b style="text-decoration:underline;">USULAN</b></p>

              <p style="text-align:justify">Berdasarkan uraian diatas, maka atas permohonan kredit <?php echo $custfullname?>,  dapat diproses lebih lanjut sesuai dengan ketentuan yang berlaku. </p>
              <p style="text-align:justify">Analisa ini kami berikan terbatas dari sisi Hukum. Hal-hal yang bersifat teknis, operasional & komersial adalah berada diluar kompetensi kami. Analisa ini didasarkan pada dokumen yang disampaikan kepada kami dengan asumsi bahwa keterangan dokumen tersebut adalah benar dan dikutip sesuai dengan dokumen aslinya. Apabila dikemudian hari terdapat dokumen pendukung tambahan maka analisa ini dapat disesuaikan.</p>
              <p style="text-align:justify">Demikianlah data dan usulan yang dapat kami sampaikan, keputusan Bapak adalah dasar pelaksanaan kami selanjutnya.</p>

              <p>Palembang, <input type="text" name="tanggallegal" id="tanggallegal" readonly="readonly" onFocus="NewCssCal(this.id,'YYYYMMDD');"  value="<? echo $tanggallegal; ?>" ></p>
			  <!--
              <table border="0" style="border-collapse:collapse;">

                <tr>
                  <td style="width:500px">Dipersiapkan oleh:</td>
                  <td>Mengetahui</td>
                </tr>
                <tr>
                  <td colspan="2" style="height:100px;"></td>
                </tr>
                <tr>
                  <td><input type="text" name="menyiapkan" id="menyiapkan" maxlegnth="50" value="<? echo $menyiapkan; ?>" ><br/><input type="text" name="titlemenyiapkan" id="titlemenyiapkan" maxlegnth="50" value="<? echo $titlemenyiapkan; ?>" ></td>
                  <td><input type="text" name="namapemimpin" id="namapemimpin" maxlegnth="50" value="<? echo $namapemimpin; ?>" ><br/><input type="text" name="titlepemimpin" id="titlepemimpin" maxlegnth="50" value="<? echo $titlepemimpin; ?>" ></td>
                </tr>
              </table>
			  -->
          </td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:right;"><input type="button" onclick="submitform()" value="Submit"></td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">
      <? require ("../../requirepage/hiddenfield.php"); ?>
    </form>
  </body>
</html>
