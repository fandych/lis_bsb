<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];

	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$tanggalkunjungan = "";
	$cabang = "";
	
	$tsql = "select * from Tbl_customermasterperson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
		}
	}
	
	
	//Tahap 1
	
	$jenispermohonankredit = "";
	
	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["custcreditstatus"];
		}
	}
	
	$tsql = "select status_name from TblCreditStatus where status_code = '$jenispermohonankredit'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["status_name"];
		}
	}
	
	
	$bunga = 0;
	$bungatotal = 0;
	$bungapersen = 0;
	
	$tsql = "select custcreditplafond from tbl_CustomerFacility where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["custcreditplafond"];
			$bungatotal = $bungatotal + $bunga;
		}
	}
	
	$tsqlproduk = "select BUNGA_PERSEN from TBL_MASTER_BUNGA where BUNGA_MIN < '$bungatotal' AND BUNGA_MAX > '$bungatotal'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{  
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungapersen = $rowproduk["BUNGA_PERSEN"];
		}
	}
	
	// Tahap 2
	//----------
	
	// Tahap 7
	
	$InfoFasilitasPinjamanBankLain_cb1 = "";
	$InfoFasilitasPinjamanBankLain_cb2 = "";
	$InfoFasilitasPinjamanBankLain_cb3 = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankLain where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankLain_cb1 = $row["InfoFasilitasPinjamanBankLain_cb1"];
			$InfoFasilitasPinjamanBankLain_cb2 = $row["InfoFasilitasPinjamanBankLain_cb2"];
			$InfoFasilitasPinjamanBankLain_cb3 = $row["InfoFasilitasPinjamanBankLain_cb3"];
			
		}
	}
	
	//Tahap 8
	
	$InfoFasilitasPinjamanBankMega_nocif = "";
	$InfoFasilitasPinjamanBankMega_nasabahsejak = "";
	$InfoFasilitasPinjamanBankMega_datapertanggal = "";
	$InfoFasilitasPinjamanBankMega_cb1 = "";
	
	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankMega where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankMega_nocif = $row["InfoFasilitasPinjamanBankMega_nocif"];
			$InfoFasilitasPinjamanBankMega_nasabahsejak = $row["InfoFasilitasPinjamanBankMega_nasabahsejak"];
			$InfoFasilitasPinjamanBankMega_datapertanggal = $row["InfoFasilitasPinjamanBankMega_datapertanggal"];
			$InfoFasilitasPinjamanBankMega_cb1 = $row["InfoFasilitasPinjamanBankMega_cb1"];
			
		}
	}
	
	//Tahap 9
	
	$TradeCheck_tanggalpemasok1 = "";
	$TradeCheck_namapemberiinfopemasok1 = "";
	$TradeCheck_namapemasok1 = "";
	$TradeCheck_noteleponpemasok1 = "";
	$TradeCheck_alamatpemasok1 = "";
	$TradeCheck_lamahubunganpemasok1 = "";
	$TradeCheck_jenisbarangpemasok1 = "";
	$TradeCheck_ratapenjualanpemasok1 = 0;
	$TradeCheck_termpembayaranpemasok1 = "";
	$TradeCheck_karaktercadebpemasok1 = "";
	$TradeCheck_infonegatifpemasok1 = "";
	$TradeCheck_ketepatanpemasok1 = "";
	$TradeCheck_tanggalpemasok2 = "";
	$TradeCheck_namapemberiinfopemasok2 = "";
	$TradeCheck_namapemasok2 = "";
	$TradeCheck_noteleponpemasok2 = "";
	$TradeCheck_alamatpemasok2 = "";
	$TradeCheck_lamahubunganpemasok2 = "";
	$TradeCheck_jenisbarangpemasok2 = "";
	$TradeCheck_ratapenjualanpemasok2 = 0;
	$TradeCheck_termpembayaranpemasok2 = "";
	$TradeCheck_karaktercadebpemasok2 = "";
	$TradeCheck_infonegatifpemasok2 = "";
	$TradeCheck_ketepatanpemasok2 = "";
	$TradeCheck_tanggalpembeli1 = "";
	$TradeCheck_namapemberiinfopembeli1 = "";
	$TradeCheck_namapembeli1 = "";
	$TradeCheck_noteleponpembeli1 = "";
	$TradeCheck_alamatpembeli1 = "";
	$TradeCheck_lamahubunganpembeli1 = "";
	$TradeCheck_jenisbarangpembeli1 = "";
	$TradeCheck_ratapenjualanpembeli1 = 0;
	$TradeCheck_termpembayaranpembeli1 = "";
	$TradeCheck_karaktercadebpembeli1 = "";
	$TradeCheck_infonegatifpembeli1 = "";
	$TradeCheck_tanggalpembeli2 = "";
	$TradeCheck_namapemberiinfopembeli2 = "";
	$TradeCheck_namapembeli2 = "";
	$TradeCheck_noteleponpembeli2 = "";
	$TradeCheck_alamatpembeli2 = "";
	$TradeCheck_lamahubunganpembeli2 = "";
	$TradeCheck_jenisbarangpembeli2 = "";
	$TradeCheck_ratapenjualanpembeli2 = 0;
	$TradeCheck_termpembayaranpembeli2 = "";
	$TradeCheck_karaktercadebpembeli2 = "";
	$TradeCheck_infonegatifpembeli2 = "";
	$TradeCheck_tanggalkomunitas1 = "";
	$TradeCheck_namapemberiinfokomunitas1 = "";
	$TradeCheck_namakomunitas1 = "";
	$TradeCheck_noteleponkomunitas1 = "";
	$TradeCheck_alamatkomunitas1 = "";
	$TradeCheck_lamahubungankomunitas1 = "";
	$TradeCheck_jenisbarangkomunitas1 = "";
	$TradeCheck_ratapenjualankomunitas1 = 0;
	$TradeCheck_termpembayarankomunitas1 = "";
	$TradeCheck_karaktercadebkomunitas1 = "";
	$TradeCheck_infonegatifkomunitas1 = "";
	$TradeCheck_ketepatankomunitas1 = "";
	$TradeCheck_tanggalkomunitas2 = "";
	$TradeCheck_namapemberiinfokomunitas2 = "";
	$TradeCheck_namakomunitas2 = "";
	$TradeCheck_noteleponkomunitas2 = "";
	$TradeCheck_alamatkomunitas2 = "";
	$TradeCheck_lamahubungankomunitas2 = "";
	$TradeCheck_jenisbarangkomunitas2 = "";
	$TradeCheck_ratapenjualankomunitas2 = 0;
	$TradeCheck_termpembayarankomunitas2 = "";
	$TradeCheck_karaktercadebkomunitas2 = "";
	$TradeCheck_infonegatifkomunitas2 = "";
	$TradeCheck_ketepatankomunitas2 = "";
	
	$tsql = "select * from Tbl_LKCDTradeCheck where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$TradeCheck_tanggalpemasok1 = $row["TradeCheck_tanggalpemasok1"];
			$TradeCheck_namapemberiinfopemasok1 = $row["TradeCheck_namapemberiinfopemasok1"];
			$TradeCheck_namapemasok1 = $row["TradeCheck_namapemasok1"];
			$TradeCheck_noteleponpemasok1 = $row["TradeCheck_noteleponpemasok1"];
			$TradeCheck_alamatpemasok1 = $row["TradeCheck_alamatpemasok1"];
			$TradeCheck_lamahubunganpemasok1 = $row["TradeCheck_lamahubunganpemasok1"];
			$TradeCheck_jenisbarangpemasok1 = $row["TradeCheck_jenisbarangpemasok1"];
			$TradeCheck_ratapenjualanpemasok1 = $row["TradeCheck_ratapenjualanpemasok1"];
			$TradeCheck_termpembayaranpemasok1 = $row["TradeCheck_termpembayaranpemasok1"];
			$TradeCheck_karaktercadebpemasok1 = $row["TradeCheck_karaktercadebpemasok1"];
			$TradeCheck_infonegatifpemasok1 = $row["TradeCheck_infonegatifpemasok1"];
			$TradeCheck_ketepatanpemasok1 = $row["TradeCheck_ketepatanpemasok1"];
			$TradeCheck_tanggalpemasok2 = $row["TradeCheck_tanggalpemasok2"];
			$TradeCheck_namapemberiinfopemasok2 = $row["TradeCheck_namapemberiinfopemasok2"];
			$TradeCheck_namapemasok2 = $row["TradeCheck_namapemasok2"];
			$TradeCheck_noteleponpemasok2 = $row["TradeCheck_noteleponpemasok2"];
			$TradeCheck_alamatpemasok2 = $row["TradeCheck_alamatpemasok2"];
			$TradeCheck_lamahubunganpemasok2 = $row["TradeCheck_lamahubunganpemasok2"];
			$TradeCheck_jenisbarangpemasok2 = $row["TradeCheck_jenisbarangpemasok2"];
			$TradeCheck_ratapenjualanpemasok2 = $row["TradeCheck_ratapenjualanpemasok2"];
			$TradeCheck_termpembayaranpemasok2 = $row["TradeCheck_termpembayaranpemasok2"];
			$TradeCheck_karaktercadebpemasok2 = $row["TradeCheck_karaktercadebpemasok2"];
			$TradeCheck_infonegatifpemasok2 = $row["TradeCheck_infonegatifpemasok2"];
			$TradeCheck_ketepatanpemasok2 = $row["TradeCheck_ketepatanpemasok2"];
			$TradeCheck_tanggalpembeli1 = $row["TradeCheck_tanggalpembeli1"];
			$TradeCheck_namapemberiinfopembeli1 = $row["TradeCheck_namapemberiinfopembeli1"];
			$TradeCheck_namapembeli1 = $row["TradeCheck_namapembeli1"];
			$TradeCheck_noteleponpembeli1 = $row["TradeCheck_noteleponpembeli1"];
			$TradeCheck_alamatpembeli1 = $row["TradeCheck_alamatpembeli1"];
			$TradeCheck_lamahubunganpembeli1 = $row["TradeCheck_lamahubunganpembeli1"];
			$TradeCheck_jenisbarangpembeli1 = $row["TradeCheck_jenisbarangpembeli1"];
			$TradeCheck_ratapenjualanpembeli1 = $row["TradeCheck_ratapenjualanpembeli1"];
			$TradeCheck_termpembayaranpembeli1 = $row["TradeCheck_termpembayaranpembeli1"];
			$TradeCheck_karaktercadebpembeli1 = $row["TradeCheck_karaktercadebpembeli1"];
			$TradeCheck_infonegatifpembeli1 = $row["TradeCheck_infonegatifpembeli1"];
			$TradeCheck_tanggalpembeli2 = $row["TradeCheck_tanggalpembeli2"];
			$TradeCheck_namapemberiinfopembeli2 = $row["TradeCheck_namapemberiinfopembeli2"];
			$TradeCheck_namapembeli2 = $row["TradeCheck_namapembeli2"];
			$TradeCheck_noteleponpembeli2 = $row["TradeCheck_noteleponpembeli2"];
			$TradeCheck_alamatpembeli2 = $row["TradeCheck_alamatpembeli2"];
			$TradeCheck_lamahubunganpembeli2 = $row["TradeCheck_lamahubunganpembeli2"];
			$TradeCheck_jenisbarangpembeli2 = $row["TradeCheck_jenisbarangpembeli2"];
			$TradeCheck_ratapenjualanpembeli2 = $row["TradeCheck_ratapenjualanpembeli2"];
			$TradeCheck_termpembayaranpembeli2 = $row["TradeCheck_termpembayaranpembeli2"];
			$TradeCheck_karaktercadebpembeli2 = $row["TradeCheck_karaktercadebpembeli2"];
			$TradeCheck_infonegatifpembeli2 = $row["TradeCheck_infonegatifpembeli2"];
			$TradeCheck_tanggalkomunitas1 = $row["TradeCheck_tanggalkomunitas1"];
			$TradeCheck_namapemberiinfokomunitas1 = $row["TradeCheck_namapemberiinfokomunitas1"];
			$TradeCheck_namakomunitas1 = $row["TradeCheck_namakomunitas1"];
			$TradeCheck_noteleponkomunitas1 = $row["TradeCheck_noteleponkomunitas1"];
			$TradeCheck_alamatkomunitas1 = $row["TradeCheck_alamatkomunitas1"];
			$TradeCheck_lamahubungankomunitas1 = $row["TradeCheck_lamahubungankomunitas1"];
			$TradeCheck_jenisbarangkomunitas1 = $row["TradeCheck_jenisbarangkomunitas1"];
			$TradeCheck_ratapenjualankomunitas1 = $row["TradeCheck_ratapenjualankomunitas1"];
			$TradeCheck_termpembayarankomunitas1 = $row["TradeCheck_termpembayarankomunitas1"];
			$TradeCheck_karaktercadebkomunitas1 = $row["TradeCheck_karaktercadebkomunitas1"];
			$TradeCheck_infonegatifkomunitas1 = $row["TradeCheck_infonegatifkomunitas1"];
			$TradeCheck_ketepatankomunitas1 = $row["TradeCheck_ketepatankomunitas1"];
			$TradeCheck_tanggalkomunitas2 = $row["TradeCheck_tanggalkomunitas2"];
			$TradeCheck_namapemberiinfokomunitas2 = $row["TradeCheck_namapemberiinfokomunitas2"];
			$TradeCheck_namakomunitas2 = $row["TradeCheck_namakomunitas2"];
			$TradeCheck_noteleponkomunitas2 = $row["TradeCheck_noteleponkomunitas2"];
			$TradeCheck_alamatkomunitas2 = $row["TradeCheck_alamatkomunitas2"];
			$TradeCheck_lamahubungankomunitas2 = $row["TradeCheck_lamahubungankomunitas2"];
			$TradeCheck_jenisbarangkomunitas2 = $row["TradeCheck_jenisbarangkomunitas2"];
			$TradeCheck_ratapenjualankomunitas2 = $row["TradeCheck_ratapenjualankomunitas2"];
			$TradeCheck_termpembayarankomunitas2 = $row["TradeCheck_termpembayarankomunitas2"];
			$TradeCheck_karaktercadebkomunitas2 = $row["TradeCheck_karaktercadebkomunitas2"];
			$TradeCheck_infonegatifkomunitas2 = $row["TradeCheck_infonegatifkomunitas2"];
			$TradeCheck_ketepatankomunitas2 = $row["TradeCheck_ketepatankomunitas2"];
		}
	}
	
	//END TAHAP 9
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD VIEW</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../../lib/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>

<table width="1000px" align="center" style="border:1px solid black;" >
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">LAPORAN KUNJUNGAN CALON DEBITUR</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td width="25%">Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td> 
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="TAHAP 1">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td colspan="7" align="center">
				<strong>IDENTIFIKASI FASILITAS YANG DIAJUKAN</strong>
			</td>
		</tr>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<tr>
			<td style="border:1px solid black;width:100px;" align="center">Jenis Fasilitas</td>
			<td style="border:1px solid black;width:100px;" align="center">Tujuan Pengajuan Kredit</td>
			<td style="border:1px solid black;width:100px;" align="center">Plafond</td>
			<td style="border:1px solid black;width:100px;" align="center">Jangka waktu (Bulan)</td>
			<td style="border:1px solid black;width:100px;" align="center">Suku bunga yang diberikan (%)</td>
			<td style="border:1px solid black;width:100px;" align="center">Suku bunga minimal</td>
			<td style="border:1px solid black;width:100px;" align="center">Keterangan</td>
		</tr>
		
		<?
			$jenisfasilitas = "";
			$tujuanpengajuankredit = "";
			$plafond = 0;
			$jangkawaktu = "";
			$seq = "";
			$bungadiberikan = "";
			
			
			$tsql = "select custcredittype, custcreditneed, custcreditplafond, custcreditlong, custfacseq, sukubungayangdiberikan from tbl_CustomerFacility where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$jenisfasilitas = $row["custcredittype"];
					$tujuanpengajuankredit = $row["custcreditneed"];
					$plafond = numberFormat($row["custcreditplafond"]);
					$jangkawaktu = $row["custcreditlong"];
					$seq = $row["custfacseq"];
					$bungadiberikan = $row["sukubungayangdiberikan"];
			
					$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitas'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{  
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$jenisfasilitas = $rowproduk["produk_type_description"];
						}
					}
					
					$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanpengajuankredit'";
					$aproduk = sqlsrv_query($conn, $tsqlproduk);

					  if ( $aproduk === false)
					  die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aproduk))
					{  
						if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
						{
							$tujuanpengajuankredit = $rowproduk["credit_need_name"];
						}
					}
					
					
					
		?>
		
		<tr>
			<td style="border:1px solid black;" align="center"><? echo $jenisfasilitas;?></td>
			<td style="border:1px solid black;" align="center"><? echo $tujuanpengajuankredit;?></td>
			<td style="border:1px solid black;" align="center"><? echo $plafond;?></td>
			<td style="border:1px solid black;" align="center"><? echo $jangkawaktu;?></td>
			<td style="border:1px solid black;" align="center"><? echo $bungadiberikan;?> </td>
			<td style="border:1px solid black;" align="center"><? echo $bungapersen; ?></td>
			<td style="border:1px solid black;" align="center"><? if($jenispermohonankredit == "Back to Back"){ echo "Effective p.a"; } else { echo "Flat p.a";}?>
			<input type="hidden" id="<? echo 'seq'.$seq;?>" name="seq"  value='<? echo $seq; ?>'></td>
		</tr>
		
		<?
				}
			}
			sqlsrv_free_stmt( $a );
		?>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 2">
	<td colspan="2" style="text-align:center;font-weight:bold;">IDENTIFIKASI KEBUTUHAN KREDIT INVESTASI</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Jenis Barang yang dibeli</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry1" name="formentry1" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS BARANG</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>MERK</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TYPE</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KUANTITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>HARGA SATUAN</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		
		<?
			$jenisbarang_seq = "";
			$jenisbarang_nama = "";
			$jenisbarang_merk = "";
			$jenisbarang_type = "";
			$jenisbarang_kuantitas = "";
			$jenisbarang_hargasatuan = "";
			$jenisbarang_total = "";
			
			$AllTotalbiaya = 0;
			$AllTotaltemp = 0;
			
			$tsql = "select * from Tbl_LKCDJenisBarang where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$jenisbarang_seq = $row["jenisbarang_seq"];
					$jenisbarang_nama = $row["jenisbarang_nama"];
					$jenisbarang_merk = $row["jenisbarang_merk"];
					$jenisbarang_type = $row["jenisbarang_type"];
					$jenisbarang_kuantitas = $row["jenisbarang_kuantitas"];
					$jenisbarang_hargasatuan = $row["jenisbarang_hargasatuan"];
					$jenisbarang_total = $row["jenisbarang_total"];
				
					$AllTotaltemp = str_replace(",","", $jenisbarang_total);
					$AllTotalbiaya = $AllTotalbiaya + $AllTotaltemp;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_merk?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_type?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($jenisbarang_hargasatuan)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($jenisbarang_total)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Tempat Usaha yang dibeli</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry2" name="formentry2" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS TEMPAT USAHA</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>LOKASI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>LT, LB, dan Lantai</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KUANTITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>HARGA Pembelian</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>

		<?
			$tempatusaha_seq = "";
			$tempatusaha_nama = "";
			$tempatusaha_lokasi = "";
			$tempatusaha_lbltlantai = "";
			$tempatusaha_kuantitas = "";
			$tempatusaha_hargasatuan = "";
			$tempatusaha_total = "";
			
			$AllTotalbiaya2 = 0;
			$AllTotaltemp2 = 0;
			
			$tsql = "select * from Tbl_LKCDTempatUsaha where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$tempatusaha_seq = $row["tempatusaha_seq"];
					$tempatusaha_nama = $row["tempatusaha_nama"];
					$tempatusaha_lokasi = $row["tempatusaha_lokasi"];
					$tempatusaha_lbltlantai = $row["tempatusaha_lbltlantai"];
					$tempatusaha_kuantitas = $row["tempatusaha_kuantitas"];
					$tempatusaha_hargasatuan = $row["tempatusaha_hargasatuan"];
					$tempatusaha_total = $row["tempatusaha_total"];
				
					$AllTotaltemp2 = str_replace(",","", $tempatusaha_total);
					$AllTotalbiaya2 = $AllTotalbiaya2 + $AllTotaltemp2;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lokasi?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lbltlantai?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_hargasatuan)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_total)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya2)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Rencana Pembangunan/ Renovasi yang akan dilakukan</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry3" name="formentry3" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis bangunan yang akan dibangun/direnovasi</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Luas bangungan/renovasi (m2)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Estimasi waktu pengerjaan</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya per-m2</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>

		<?
			$rencanapembangunan_seq = "";
			$rencanapembangunan_nama = "";
			$rencanapembangunan_luas = "";
			$rencanapembangunan_estimasiwaktu = "";
			$rencanapembangunan_biayaperm2 = "";
			$rencanapembangunan_total = "";
			
			$AllTotalbiaya3 = 0;
			$AllTotaltemp3 = 0;
			
			$tsql = "select * from Tbl_LKCDRencanaPembangunan where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$rencanapembangunan_seq = $row["rencanapembangunan_seq"];
					$rencanapembangunan_nama = $row["rencanapembangunan_nama"];
					$rencanapembangunan_luas = $row["rencanapembangunan_luas"];
					$rencanapembangunan_estimasiwaktu = $row["rencanapembangunan_estimasiwaktu"];
					$rencanapembangunan_biayaperm2 = $row["rencanapembangunan_biayaperm2"];
					$rencanapembangunan_total = $row["rencanapembangunan_total"];
				
					$AllTotaltemp3 = str_replace(",","", $rencanapembangunan_total);
					$AllTotalbiaya3 = $AllTotalbiaya3 + $AllTotaltemp3;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_luas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_estimasiwaktu?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_biayaperm2)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_total)?></td>
		
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya3)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Kebutuhan Takeover Kredit Investasi</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry4" name="formentry4" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas Kredit Investasi yang akan di takeover</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Nama Bank atau institusi</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Outstanding</strong></td>
			<td width="25%" colspan="3">&nbsp;</td>
		</tr>

		<?
			$KebutuhanTakeover_seq = "";
			$KebutuhanTakeover_jenisfasilitas = "";
			$KebutuhanTakeover_namabank = "";
			$KebutuhanTakeover_totaloutstanding = "";
			
			$AllTotalbiaya4 = 0;
			$AllTotaltemp4 = 0;
			
			$tsql = "select * from Tbl_LKCDKebutuhanTakeover where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$KebutuhanTakeover_seq = $row["KebutuhanTakeover_seq"];
					$KebutuhanTakeover_jenisfasilitas = $row["KebutuhanTakeover_jenisfasilitas"];
					$KebutuhanTakeover_namabank = $row["KebutuhanTakeover_namabank"];
					$KebutuhanTakeover_totaloutstanding = $row["KebutuhanTakeover_totaloutstanding"];
				
					$AllTotaltemp4 = str_replace(",","", $KebutuhanTakeover_totaloutstanding);
					$AllTotalbiaya4 = $AllTotalbiaya4 + $AllTotaltemp4;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($KebutuhanTakeover_totaloutstanding)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya4)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 3">
	<td colspan="2"><? include("VIEW_LKCD_3DATAKEUANGAN_ALL_n.php"); ?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 4">
	<td colspan="2"><? include("VIEW_LKCD_3DATAKEUANGANLAINNYA_INFORMASI_PENDUKUNG_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 6">
	<td colspan="2" style="text-align:center;font-weight:bold;">MUTASI REKENING KORAN / TABUNGAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry1" name="formentry1" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BULAN</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>MUTASI DEBET</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>MUTASI KREDIT</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$seqMutasi = "";
			$MutasiRekening_namabulan = "";
			$MutasiRekening_mutasidebet = "";
			$MutasiRekening_mutasikredit = "";
			$Totaldebet = 0;
			$Totalkredit = 0;
			$tempdebet = 0;
			$tempkredit = 0;
			
			$tsql = "select * from Tbl_LKCDMutasiRekening where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqMutasi = $row["seq"];
					$MutasiRekening_namabulan = $row["MutasiRekening_namabulan"];
					$MutasiRekening_mutasidebet = numberFormat($row["MutasiRekening_mutasidebet"]);
					$MutasiRekening_mutasikredit = numberFormat($row["MutasiRekening_mutasikredit"]);
				
					$tempdebet = str_replace(",","", $MutasiRekening_mutasidebet);
					$Totaldebet = $Totaldebet + $tempdebet;
					
					$tempkredit = str_replace(",","", $MutasiRekening_mutasikredit);
					$Totalkredit = $Totalkredit + $tempkredit;
		
		?>
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_namabulan?></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_mutasidebet?></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo $MutasiRekening_mutasikredit?></td>
		
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($Totaldebet)?></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($Totalkredit)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">REKAP PENJUALAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<form id="formentry2" name="formentry2" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BULAN</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>PENJUALAN</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$seqRekap = "";
			$RekapPenjualan_namabulan = "";
			$RekapPenjualan_penjualan = "";
			$Totalpenjualan = 0;
			$temppenjualan = 0;
			
			$tsql = "select * from Tbl_LKCDRekapPenjualan where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$seqRekap = $row["seq"];
					$RekapPenjualan_namabulan = $row["RekapPenjualan_namabulan"];
					$RekapPenjualan_penjualan = numberFormat($row["RekapPenjualan_penjualan"]);
				
					$temppenjualan = str_replace(",","", $RekapPenjualan_penjualan);
					$Totalpenjualan = $Totalpenjualan + $temppenjualan;
					
		
		?>
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo $RekapPenjualan_namabulan?></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo $RekapPenjualan_penjualan?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($Totalpenjualan)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 7">
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK LAIN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="800px">Apakah Debitur memiliki fasilitas pinjaman di Bank lain?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb1;?></td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>A. FASILITAS PINJAMAN TETAP  <i>(tidak memperhitungkan fasilitas yang akan di take over)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentryA1" name="formentryA1" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS FASILITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>OUTSTANDING</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>ANGSURAN</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
				<?
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";
			
				?>
		</form>
		<?
			
			
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'PT'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				
				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{	
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}
		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_outstanding)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_angsuran)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		</table>
		</form>
	
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="800px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb2;?></td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'PT'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA1col" name="formentryA1col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr>
			&nbsp;<!--<td colspan="4" align="center"><input type="button" value="SAVE" style="width:100px;background-color:blue;color:white;" onclick="nonA1()" /></td>-->
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2"><strong>B. FASILITAS PINJAMAN YANG AKAN DI TAKE OVER (Jika ada)</i></strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentryA2" name="formentryA2" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS FASILITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>NAMA BANK / INSTITUSI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>OUTSTANDING</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>ANGSURAN</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
				<?
					$FasilitasPinjamanBankLain_seq2 = "";
					$FasilitasPinjamanBankLain_type = "";
					$FasilitasPinjamanBankLain_jenisfasilitas = "";
					$FasilitasPinjamanBankLain_namabank = "";
					$FasilitasPinjamanBankLain_outstanding = "";
					$FasilitasPinjamanBankLain_angsuran = "";
					$FasilitasPinjamanBankLain_kol = "";
					$FasilitasPinjamanBankLain_dpd = "";
			
				?>
		</form>
		<?
			
			
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankLain where custnomid = '$custnomid' AND FasilitasPinjamanBankLain_type = 'TO'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankLain_seq2 = $row["FasilitasPinjamanBankLain_seq"];
					$FasilitasPinjamanBankLain_jenisfasilitas = $row["FasilitasPinjamanBankLain_jenisfasilitas"];
					$FasilitasPinjamanBankLain_namabank = $row["FasilitasPinjamanBankLain_namabank"];
					$FasilitasPinjamanBankLain_outstanding = $row["FasilitasPinjamanBankLain_outstanding"];
					$FasilitasPinjamanBankLain_angsuran = $row["FasilitasPinjamanBankLain_angsuran"];
					$FasilitasPinjamanBankLain_kol = $row["FasilitasPinjamanBankLain_kol"];
					$FasilitasPinjamanBankLain_dpd = $row["FasilitasPinjamanBankLain_dpd"];
				
				$tsql2 = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain WHERE JenisFasilitasBankLain_code = '$FasilitasPinjamanBankLain_jenisfasilitas'";
				$b = sqlsrv_query($conn, $tsql2);
				if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($b))
				{	
					if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$FasilitasPinjamanBankLain_jenisfasilitas = $rowType["JenisFasilitasBankLain_nama"];
					}
				}
		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankLain_outstanding?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($FasilitasPinjamanBankLain_angsuran)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_kol?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  $FasilitasPinjamanBankLain_dpd?></td>
			
		</tr>

		<?
				}
			}
		?>
		</table>
	
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1"><? echo $InfoFasilitasPinjamanBankLain_cb3;?></td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '1' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '2' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankLain where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankLain_seq = '3' AND LKCDKolektibilitasTidakLancarBankLain_type = 'TO'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankLain_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_berapakali"];
				$LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankLain_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryA2col" name="formentryA2col" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas1 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas2 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankLain_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankLain";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_jenisfasilitas3 == $rowType["JenisFasilitasBankLain_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankLain_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankLain_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr id="TAHAP 8">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">FASILITAS PINJAMAN DI BANK MEGA</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td width="35%">No. CIF</td>
	<td width="65%"><? echo $InfoFasilitasPinjamanBankMega_nocif;?></td>
</tr>
<tr>
	<td width="35%">Nasabah sejak</td>
	<td width="65%"><? echo $InfoFasilitasPinjamanBankMega_nasabahsejak;?></td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry1" name="formentry1" method="post">
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond Awal</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Outstanding</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Tenor (bln)</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Rate</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>KOL</strong></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><strong>DPD</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
				<?
					$FasilitasPinjamanBankMega_jenisfasilitas = "";
					$FasilitasPinjamanBankMega_plafondawal = "";
					$FasilitasPinjamanBankMega_outstanding = "";
					$FasilitasPinjamanBankMega_angsuran = "";
					$FasilitasPinjamanBankMega_tenor = "";
					$FasilitasPinjamanBankMega_rate = "";
					$FasilitasPinjamanBankMega_provisi = "";
					$FasilitasPinjamanBankMega_kol = "";
					$FasilitasPinjamanBankMega_dpd = "";
				?>
		</form>
		<form id="formedit1" name="formedit1" method="post">
		
		<?
			
			$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankMega where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$FasilitasPinjamanBankMega_seq = $row["FasilitasPinjamanBankMega_seq"];
					$FasilitasPinjamanBankMega_jenisfasilitas = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
					$FasilitasPinjamanBankMega_plafondawal = $row["FasilitasPinjamanBankMega_plafondawal"];
					$FasilitasPinjamanBankMega_outstanding = $row["FasilitasPinjamanBankMega_outstanding"];
					$FasilitasPinjamanBankMega_angsuran = $row["FasilitasPinjamanBankMega_angsuran"];
					$FasilitasPinjamanBankMega_tenor = $row["FasilitasPinjamanBankMega_tenor"];
					$FasilitasPinjamanBankMega_rate = $row["FasilitasPinjamanBankMega_rate"];
					$FasilitasPinjamanBankMega_provisi = $row["FasilitasPinjamanBankMega_provisi"];
					$FasilitasPinjamanBankMega_kol = $row["FasilitasPinjamanBankMega_kol"];
					$FasilitasPinjamanBankMega_dpd = $row["FasilitasPinjamanBankMega_dpd"];

		
		?>
		<tr>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_jenisfasilitas;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_plafondawal)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_outstanding)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_angsuran)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_tenor;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_rate;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($FasilitasPinjamanBankMega_provisi)?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_kol;?></td>
			<td width="10%" style="border:1px solid black;width:100px;" align="center"><? echo $FasilitasPinjamanBankMega_dpd;?></td>		
			
		</tr>

		<?
				}
			}
		?>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td width="35%">Data pertanggal</td>
	<td width="65%"><? echo $InfoFasilitasPinjamanBankMega_datapertanggal;?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="1" width="350px">Apakah selama 6 - 12 bulan terakhir diantara fasilitas diatas terdapat kolektibilitas tidak lancar (kolektibilitas 2-5)?</td>
	<td colspan="1">
		<? echo $InfoFasilitasPinjamanBankMega_cb1;?>
	</td>
</tr>
<tr>
	<td colspan="2">Jika <strong>YA</strong> mohon lengkapi data di bawah ini.</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="kashiwagi">
	<?
		$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = "";
		$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = "";
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '1' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '2' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
		
		$tsql = "SELECT * FROM Tbl_LKCDKolektibilitasTidakLancarBankMega where custnomid = '$custnomid' AND LKCDKolektibilitasTidakLancarBankMega_seq = '3' ";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
			if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
			{
				$LKCDKolektibilitasTidakLancarBankMega_berapakali3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_berapakali"];
				$LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas"];
				$LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 = $rowType["LKCDKolektibilitasTidakLancarBankMega_khususkartukredit"];
			}
		}
	
	
	?>
		<form id="formentryAcol" name="formentryAcol" method="post">
		<table width="100%" align="center" style="border:0px solid black;">
		<tr>
			<td width="15%" style="width:200px" align="center">&nbsp;</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Berapa kali dalam 6-12 bulan</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Diisi khusus oleh kartu kredit</strong></td>
		</tr>
		<tr id="1">
			<td width="15%" align="center">Kolektibilitas 2 (DPD s.d 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali1;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas1 == $rowType["JenisFasilitasBankMega_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankMega_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit1 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="2">
			<td width="15%" align="center">Kolektibilitas 2 (DPD > 30 hari)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali2;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas2 == $rowType["JenisFasilitasBankMega_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankMega_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit2 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		<tr id="3">
			<td width="15%" align="center">Kolektibilitas 3-5 (NPL)</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<? echo $LKCDKolektibilitasTidakLancarBankMega_berapakali3;?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDJenisFasilitasBankMega";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_jenisfasilitas3 == $rowType["JenisFasilitasBankMega_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["JenisFasilitasBankMega_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			</td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center">
				<?
					$tsql = "SELECT * FROM Tbl_LKCDKhususKartuKredit";
					$b = sqlsrv_query($conn, $tsql);
					if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
					if(sqlsrv_has_rows($b))
					{ 
				?>
					<?
						While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
						{
							$varselected = "";
							if($LKCDKolektibilitasTidakLancarBankMega_khususkartukredit3 == $rowType["KhususKartuKredit_code"])
							{
								$varselected = "selected";
							
					?>	
							<? echo $rowType["KhususKartuKredit_nama"];?>
					<?
							}
						}
					?>
				<?
					}
				?>
			
			</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 9">
	<td colspan="2" style="text-align:center;font-weight:bold;">TRADE DAN COMMUNITY CHECKING</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">
		<table width="100%" style="border:0px solid black;">
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">Pemasok 1</td>
			<td width="25%">Pembeli 1</td>
			<td width="25%">Komunitas 1</td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Tanggal Checking</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalkomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Nama dan Jabatan Pemberi Informasi</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfopemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfopembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfokomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Nama Pemasok/Pembeli/Komunitas</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namakomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">No Telepon</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponkomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Alamat</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatkomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Lama Hubungan dengan Cadeb</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubunganpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubunganpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubungankomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Jenis Brg yg dipasok/dibeli dari Debitur</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangkomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Rata2 penjualan/pembelian per-bulan</td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualanpemasok1);?></td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualanpembeli1);?></td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualankomunitas1);?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Term Pembayaran (Hari)</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayaranpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayaranpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayarankomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Karakter Cadeb</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebpemasok1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebpembeli1;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebkomunitas1;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Apakah terdapat informasi negatif Mengenai debitur/cadeb</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifpemasok1;?>
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifpembeli1;?>
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifkomunitas1;?>
			</td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Ketepatan Pembayaran Cadeb</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_ketepatanpemasok1;?>
			</td>
			<td width="25%" style="border:0px solid black;">
				&nbsp;
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_ketepatankomunitas1;?>
			</td>
		</tr>
		<tr>
			<td width="25%" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">Pemasok 2</td>
			<td width="25%">Pembeli 2</td>
			<td width="25%">Komunitas 2</td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Tanggal Checking</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_tanggalkomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Nama dan Jabatan Pemberi Informasi</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfopemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfopembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemberiinfokomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Nama Pemasok/Pembeli/Komunitas</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namapembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_namakomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">No Telepon</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_noteleponkomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Alamat</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_alamatkomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Lama Hubungan dengan Cadeb</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubunganpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubunganpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_lamahubungankomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Jenis Brg yg dipasok/dibeli dari Debitur</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_jenisbarangkomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Rata2 penjualan/pembelian per-bulan</td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualanpemasok2);?></td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualanpembeli2);?></td>
			<td width="25%" style="border:1px solid black;"><? echo numberFormat($TradeCheck_ratapenjualankomunitas2);?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Term Pembayaran (Hari)</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayaranpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayaranpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_termpembayarankomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Karakter Cadeb</td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebpemasok2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebpembeli2;?></td>
			<td width="25%" style="border:1px solid black;"><? echo $TradeCheck_karaktercadebkomunitas2;?></td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Apakah terdapat informasi negatif Mengenai debitur/cadeb</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifpemasok2;?>
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifpembeli2;?>
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_infonegatifkomunitas2;?>
			</td>
		</tr>
		<tr>
			<td width="25%" style="border:1px solid black;">Ketepatan Pembayaran Cadeb</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_ketepatanpemasok2;?>
			</td>
			<td width="25%" style="border:0px solid black;">
				&nbsp;
			</td>
			<td width="25%" style="border:1px solid black;">
				<? echo $TradeCheck_ketepatankomunitas2;?>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
</table>
<div>&nbsp;</div>
</body>
</html> 