<?php
require('./sqlsrv.php');


$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";

$custktpno="";
$custaddr="";
$custfullname="";
$custnpwpno="";
$strsql="select custktpno,custaddr,custfullname,custnpwpno  from Tbl_CustomerMasterPerson where custnomid='".$custnomid."' ";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custktpno=$rs[$i]['custktpno'];
  $custaddr=$rs[$i]['custaddr'];
  $custfullname=$rs[$i]['custfullname'];
  $custnpwpno=$rs[$i]['custnpwpno'];
}

$_pasangan_nomorktp='';
$_pasangan_namadepan='';
$_pasangan_namatengah='';
$_pasangan_namabelakang='';
$strsql = "select _pasangan_nomorktp,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang from pl_custpasangan where _custnomid='".$custnomid."' ";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_pasangan_nomorktp= $rs[$i]['_pasangan_nomorktp'];
  $_pasangan_namadepan= $rs[$i]['_pasangan_namadepan'];
  $_pasangan_namatengah= $rs[$i]['_pasangan_namatengah'];
  $_pasangan_namabelakang= $rs[$i]['_pasangan_namabelakang'];
}


$tujuanpermohonankredit= "";
$kewenanganmeminjam= "";
$kewenanganmengagunkan= "";
$rencanaagunankredit= "";
$syaratdanbentukpengikatan= "";
$datainformasidebitur= "";
$strsql = "select * from LEGAL_OPINION_KGS where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $tujuanpermohonankredit=$rs[$i]['TUJUANPERMOHONANKREDIT'];
  $kewenanganmeminjam=$rs[$i]['KEWENANGANMEMINJAM'];
  $kewenanganmengagunkan=$rs[$i]['KEWENANGANMENGAGUNKAN'];
  $rencanaagunankredit=$rs[$i]['RENCANAAGUNANKREDIT'];
  $syaratdanbentukpengikatan=$rs[$i]['SYARATDANBENTUKPENGIKATAN'];
  $datainformasidebitur=$rs[$i]['DATAINFORMASIDEBITUR'];
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Legal Opinion</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>


    <script type="text/javascript">
      function submitform()
      {
        if($('#tujuanpermohonankredit').val()=="")
        {
          $('#tujuanpermohonankredit').focus();
          alert('Tujuan Permohonan Kredit Harus diisi');
        }
        else if($('#kewenanganmeminjam').val()=="")
        {
          $('#kewenanganmeminjam').focus();
          alert('Kewenangan Meminjam Harus diisi');
        }
        else if($('#kewenanganmengagunkan').val()=="")
        {
          $('#kewenanganmengagunkan').focus();
          alert('Kewenangan Mengagunkan Harus diisi');

        }
        else if($('#rencanaagunankredit').val()=="")
        {
          $('#rencanaagunankredit').focus();
          alert('Rencana Agunan Kredit Harus diisi');
        }
        else if($('#syaratdanbentukpengikatan').val()=="")
        {
          $('#syaratdanbentukpengikatan').focus();
          alert('Syarat dan Bentuk Pengikatan Harus diisi');
        }
        else
        {
          $('#frm').submit();
        }

      }
    </script>

    <style media="screen">
      #tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}

    </style>
  </head>
  <body>
    <form id="frm" action="legal_opinion_kgs_exec.php" method="post">

      <?
        //header
      ?>
      <table id="tableform" border="0" align="center">
        <tr>
          <td>
              <p style="text-align:center;"><b style="text-decoration:underline;">LEGAL OPINION</b></p><br/>

              <p>Sehubungan dengan Permohonan Kredit atas nama <?php echo $custfullname; ?>, maka dapat kami sampaikan hal-hal sebagai berikut :</p>

              <table>
                <tr>
                  <td valign="top" style="width:300px;">Nama Pemohon</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custfullname; ?></td>
                </tr>
                <tr>
                  <td valign="top">Alamat</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custaddr;?></td>
                </tr>
              </table>

              <br/>

              <table>
                <tr>
                  <td colspan="3" style="font-weight:bold;">Legalitas Pemohon</td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">NIK</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top">NPWP Pribadi</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custnpwpno;?></td>
                </tr>
                <tr>
                  <td valign="top">Kartu Keluarga</td>
                  <td valign="top">:</td>
                  <td valign="top">BELUM TAU DARI MANA</td>
                </tr>
                <tr>
                  <td valign="top">Akte Nikah</td>
                  <td valign="top">:</td>
                  <td valign="top">BELUM TAU DARI MANA</td>
                </tr>
                <tr>
                  <td valign="top">Nama Pasangan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $_pasangan_namadepan.' '.$_pasangan_namatengah.' '.$_pasangan_namabelakang;?></td>
                </tr>
                <tr>
                  <td valign="top">NIK Pasangan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $_pasangan_nomorktp;?></td>
                </tr>
                <tr>
                  <td valign="top">Copy BPKB & STNK</td>
                  <td valign="top">:</td>
                  <td valign="top">TIDAK TAU AMBIL DARI MANA</td>
                </tr>
              </table>

              <br/>
              <table>
                <tr>
                  <td colspan="3" style="font-weight:bold;">Pendapatan</td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Surat Keterangan Pekerjaan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Slip Gaji</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Copy Rekening Tabungan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
              </table>

              <br/>
              <table>
                <tr>
                  <td colspan="3" style="font-weight:bold;">Rencana Penggunaan Kredit</td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Untuk Pembelian</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">LB/LT</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Perumahan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
                <tr>
                  <td valign="top" style="width:300px;">Bukti kepemilikan</td>
                  <td valign="top">:</td>
                  <td valign="top"><?php echo $custktpno;?></td>
                </tr>
              </table>


              Untuk Pembelian 1 (satu) unit rumah	:

              LB/LT	:

              Perumahan	:

              Bukti kepemilikan	:


              <p><b style="text-decoration:underline;">Sumber Pendapatan</b></p>

              <ol type="1">
                <li>
                  <b style="text-decoration:underline;">Maksud dan Tujuan pemohon kredit</b>
                  <br/>
                  <textarea name="tujuanpermohonankredit" id="tujuanpermohonankredit" rows="8" cols="80"><?php echo $tujuanpermohonankredit;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Kewenangan  Meminjam</b><br/>
                  <textarea name="kewenanganmeminjam" id="kewenanganmeminjam" rows="8" cols="80"><?php echo $kewenanganmeminjam;?></textarea>
                </br/>
                </li>
                <li>
                  <b style="text-decoration:underline;">Kewenangan Mengagunkan</b><br/>
                  <textarea name="kewenanganmengagunkan" id="kewenanganmengagunkan" rows="8" cols="80"><?php echo $kewenanganmengagunkan;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Rencana Agunan Kredit</b><br/>
                  <textarea name="rencanaagunankredit" id="rencanaagunankredit" rows="8" cols="80"><?php echo $rencanaagunankredit;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Syarat dan Bentuk Pengikatan</b>
                  <br/>
                  <textarea name="syaratdanbentukpengikatan" id="syaratdanbentukpengikatan" rows="8" cols="80"><?php echo $syaratdanbentukpengikatan;?></textarea>
                </li>
                <li>
                  <b style="text-decoration:underline;">Data Informasi Debitur</b>
                  <br/>
                  <p>Berdasarkan Dari data Sistem Informasi Debitur Bank Indonesia:</p>
                  <table style="border-collapse:collapse;" border="1">
                    <tr>
                      <td rowspan="2" style="width:100px;">Bank</td>
                      <td rowspan="2" style="width:150px;">Plafon</td>
                      <td rowspan="2" style="width:100px;">Baki</td>
                      <td rowspan="2" style="width:75px;">Tgl diberi</td>
                      <td rowspan="2" style="width:75px;">Tgl tempo</td>
                      <td colspan="2" style="width:200px;">Kolektibilitas</td>
                      <td rowspan="2" style="width:150px;">Lama Tunggakan</td>
                    </tr>
                    <tr>
                      <td>Tertera</td>
                      <td>Saat ini</td>
                    </tr>
                  </table>
                </li>
              </ol>

              <p><b style="text-decoration:underline;">USULAN</b></p>

              <p>Berdasarkan uraian diatas, maka atas permohonan kredit dapat diproses lebih lanjut sesuai dengan ketentuan yang berlaku, dengan catatan ats point 5 wajib dipenuhi akan menjadi syarat penandatanganan kredit serta melengkapi IMB dan PBB.</p>
              <p>Analisa ini kami berikan terbatas dari sisi hukum. Analisa ini didasarkan pada dokumen yang disampaikan kepada kami dengan asumsi bahwa keterangan dokumen tersebut adalah benar dan di kutip sesuai dengan dokumen aslinya. Apabila dekemudian hari terdapat dokumen Pendukung</p>
              <p>Demikian data dan usulan yang dapat kami sampaikan, atas perhatianya kami ucapkan terima kasih.</p>

              <p>Palembang, <?php echo date('d-m-Y')?></p>
              <table border="0" style="border-collapse:collapse;">

                <tr>
                  <td style="width:500px">Dipersiapkan oleh:</td>
                  <td>Mengetahui</td>
                </tr>
                <tr>
                  <td colspan="2" style="height:100px;"></td>
                </tr>
                <tr>
                  <td>Rotua Marlina<br/>Pengelola Legal & Dokumentasi	</td>
                  <td>Mas Ely Warsal<br/>Pgs.Pemimpin</td>
                </tr>
              </table>
          </td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:right;"><input type="button" onclick="submitform()" value="Submit"></td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">

    </form>
  </body>
</html>
