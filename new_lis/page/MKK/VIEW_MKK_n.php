<?
	ini_set("display_errors", 0);
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../lib/open_conLISAPR.php");
	//require ("../../lib/open_con_apr.php");
	require ("../../requirepage/parameter.php");

	/*
	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	*/
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;

	require ("../neraca/perhitungan.php");
	require ("../analisa5c/hasilanalisa.php");

	$tanggalkunjungan = "";
	$cabang = "";
	$namanasabah = "";
	$custsex = "";

	$tsql = "select * from Tbl_customermasterperson2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
			$custsex = $row["custsex"];

			if($custsex == "0")
			{
				$namanasabah = $row["custbusname"];
			}
			else
			{
				$namanasabah = $row["custfullname"];
			}
		}
	}

	$mkk_kebutuhan = "";
	$mkk_character = "";
	$mkk_condition = "";
	$mkk_capacity = "";
	$mkk_capital = "";
	$mkk_collateral = "";
	$rekomen = "";

	$tsql = "select * from Tbl_MKKAnalisa5c where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$mkk_kebutuhan = $row['analisa5c_kebutuhan'];
			$mkk_character = $row['analisa5c_character'];
			$mkk_condition = $row['analisa5c_condition'];
			$mkk_capacity = $row['analisa5c_capacity'];
			$mkk_capital = $row['analisa5c_capital'];
			$mkk_collateral = $row['analisa5c_collateral'];
			$rekomen = $row['analisa5c_rekomendasi'];
		}
	}

	$backtoback = "";

	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";

	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$backtoback = $row["custcreditstatus"];
		}
	}

	$flat = "Flat p.a.";

	if($backtoback == "BB")
	{
		$flat = "Effective p.a.";
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MKK</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<link href="../../css/d.css" rel="stylesheet" type="text/css" />
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="1000px" align="center" style="border:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">MEMO KEPUTUSAN KREDIT</td>
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">Nama Nasabah</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $namanasabah;?></div></td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td width="25%">Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td>
</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>

</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">SUMMARY HASIL ANALISA 5C</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="formentry" name="formentry">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center"><strong>No.</strong></td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center"><strong>Hasil Analisa</strong></td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><strong>Hasil</strong></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><strong>Keterangan</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">1</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Kebutuhan</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $kesanalisakebutuhandebitur;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_kebutuhan;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">2</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Character</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $keschar;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_character;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">3</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Condition</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $kescon;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_condition;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">4</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Capacity</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $kescapacity;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_capacity;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">5</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Capital</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $kescapital;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_capital;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">6</td>
				<td width="15%" style="border:1px solid black;width:200px;" align="center">Collateral</td>
				<!--<td width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $hsllikuidasi;?></td>-->
				<td width="15%" style="border:1px solid black;width:400px;" align="center">&nbsp;<? echo $mkk_collateral;?></td>
			</tr>
			<tr>
				<td colspan="4" align="center"><!--<input type="button" value="SIMPAN" style="width:120px;background-color:blue;color:white;" onclick="yuria()" />--></td>
			</tr>
			<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
			<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
			<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
			<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
			<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
			<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
			<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
			<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS BARU KREDIT USAHA KECIL</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;

				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];

						$biayaadmbaru = $plafondbaru * 0.0005;

						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}

						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";

						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}

						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}

						$angsuranbaru = ($plafondbaru+($plafondbaru*$sukubungabaru/100*($jangkawaktubaru/12)))/$jangkawaktubaru;

						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($plafondbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru."% ".$flat;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($biayaadmbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($angsuranbaru));?></td>
			</tr>

			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo numberFormat(round($totalplafond));?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo numberFormat(round($totalangsuran));?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS DI BANK SUMSELBABEL (BARU + EXIST)</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;

				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];

						$biayaadmbaru = $plafondbaru * 0.0005;

						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}

						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";

						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}

						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}

						$angsuranbaru = ($plafondbaru+($plafondbaru*$sukubungabaru/100*($jangkawaktubaru/12)))/$jangkawaktubaru;

						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($plafondbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru."% ".$flat;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($biayaadmbaru));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat(round($angsuranbaru));?></td>
			</tr>

			<?
					}
				}
			?>
			<?
				$jenisfasilitasexist = "";
				$plafondexist = "";
				$sukubungaexist = "";
				$biayaadmexist = "-";
				$tujuanexist = "";
				$jangkawaktuexist = "";
				$provisiexist = "1.00% FLAT";
				$angsuranexist = "";
				$flatexist = "Flat p.a.";
				$saklarflat = "";

				$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankmega2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$jenisfasilitasexist = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
						$plafondexist = $row["FasilitasPinjamanBankMega_plafondawal"];
						$sukubungaexist = $row["FasilitasPinjamanBankMega_rate"];
						$jangkawaktuexist = $row["FasilitasPinjamanBankMega_tenor"];
						$angsuranexist = $row["FasilitasPinjamanBankMega_angsuran"];

						$totalplafond = $totalplafond + $plafondexist;
						$totalangsuran = $totalangsuran + $angsuranexist;

						if($jenisfasilitasexist == "1" || $jenisfasilitasexist == "2" || $jenisfasilitasexist == "3" || $jenisfasilitasexist == "6")
						{
							$tujuanexist = "Modal Kerja";
						}
						else if ($jenisfasilitasexist == "4" || $jenisfasilitasexist == "5")
						{
							$tujuanexist = "Investasi";
						}
						else
						{
							$tujuanexist = "Konsumtif";
						}

						$tsqlproduk = "select JenisFasilitasBankMega_nama from Tbl_LKCDJenisFasilitasBankMega where JenisFasilitasBankMega_code = '$jenisfasilitasexist'";

						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasexist = $rowproduk["JenisFasilitasBankMega_nama"];
								$saklarflat = $rowproduk["JenisFasilitasBankMega_flag"];
							}
						}

						if($saklarflat == "BTB")
						{
							$flatexist = "Effective p.a.";
						}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($plafondexist));?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungaexist."% ".$flatexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $biayaadmexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktuexist;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisiexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat(round($angsuranexist));?></td>
			</tr>
			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo numberFormat(round($totalplafond));?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo numberFormat(round($totalangsuran));?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="1000px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">REKOMENDASI KREDIT OFFICER</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="rekomendasi" name="rekomendasi">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
		<tr>
			<td>Catatan :</td>
		</tr>
		<?
				$notes = "";

				$tsql = "select * from tbl_FMKK where txn_id = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$notes = $row["txn_notes"];
					}
				}

		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:700px;height:30px;" align="left"><? echo $notes; ?></td>
		</tr>
		<tr>
			<td align="right">
				<div width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $rekomen ?></div>
			</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
			<?
				$namaTL = "";
				$noteTL = "";

				if($userbranch == "" && $userregion == "")
				{
					$tsql = "SELECT * FROM TBL_FSTART WHERE TXN_ID = '$custnomid'";
					$a = sqlsrv_query($conn, $tsql);
					if ( $a === false)
					die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($a))
					{
						if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
						{
							$userbranch = $row["txn_branch_code"];
							$userregion = $row["txn_region_code"];
						}
					}
				}
				//echo $userbranch;
				$tsql = "SELECT (SELECT USER_NAME FROM TBL_SE_USER WHERE USER_CHILD LIKE '%'+TBL_FLKCD.TXN_USER_ID+'%' AND user_branch_code = '$userbranch') AS NAMATL,* FROM TBL_FLKCD WHERE TXN_ID = '$custnomid' AND txn_branch_code = '$userbranch' and txn_region_code = '$userregion'";
				//echo $tsql;
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$namaTL = $row["NAMATL"];
						$noteTL = $row["txn_notes"];
					}
				}
			?>

			<table width="100%" align="center" style="border:1px solid black;" class="preview2" cellspacing="0" >
			<tr>
				<td width="15%" colspan="2" style="border:1px solid black;background-color:gray;" align="center"><strong>PERSETUJUAN BRANCH CREDIT MANAGER</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;" align="center"><strong>TTD</strong><br><br><br><br><br><br><br><? echo $namaTL;?><? //echo $namajabatan;?></td>
				<td width="15%" style="border:1px solid black;" align="center" valign="top"><strong>SETUJU / TIDAK SETUJU</strong><br>Catatan :<br><br><? echo $noteTL;?></td>
			</tr>
			</table>
			<br>
			<?// bagian branch review?>
			<?
				$namaTL = "";
				$noteTL = "";

				if($userbranch == "" && $userregion == "")
				{
					$tsql = "SELECT * FROM TBL_FSTART WHERE TXN_ID = '$custnomid'";
					$a = sqlsrv_query($conn, $tsql);
					if ( $a === false)
					die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($a))
					{
						if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
						{
							$userbranch = $row["txn_branch_code"];
							$userregion = $row["txn_region_code"];
						}
					}
				}
				//echo $userbranch;
				$tsql = "SELECT '' as NAMA, 'Catatan dan Keterangan' as notes, 'Rekomendasi berdasarkan alasan' as rekomen";
				//echo $tsql;
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$nama = $row["NAMA"];
						$noteTL = $row["notes"];
						$rekomen = $row["rekomen"];

						if($rekomen == "Y")
						{
							$rekomen = "REKOMENDASI";
						}
						else if($rekomen == "N")
						{
							$rekomen = "TIDAK REKOMENDASI";
						}
						else
						{
							$rekomen = "-";
						}
			?>

			<table width="100%" align="center" style="border:1px solid black;" class="preview2" cellspacing="0" >
			<tr>
				<td width="15%" colspan="2" style="border:1px solid black;background-color:gray;" align="center"><strong>PERSETUJUAN BRANCH REVIEW</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;" align="center"><strong>TTD</strong><br><br><br><br><br><br><br><? echo $nama;?><? //echo $namajabatan;?></td>
				<td width="15%" style="border:1px solid black;" align="center" valign="top"><strong><? echo $rekomen;?></strong><br>Catatan :<br><br><? echo $noteTL;?></td>
			</tr>
			</table>
			<?
					}
				}
			?>
			<br>
			<?// bagian credit review?>
			<?
				$namaTL = "";
				$noteTL = "";

				if($userbranch == "" && $userregion == "")
				{
					$tsql = "SELECT * FROM TBL_FSTART WHERE TXN_ID = '$custnomid'";
					$a = sqlsrv_query($conn, $tsql);
					if ( $a === false)
					die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($a))
					{
						if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
						{
							$userbranch = $row["txn_branch_code"];
							$userregion = $row["txn_region_code"];
						}
					}
				}
				//echo $userbranch;
				$tsql = "SELECT '' as NAMA, 'Catatan dan Keterangan' as notes, 'Rekomendasi berdasarkan alasan' as rekomen";
				//echo $tsql;
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$nama = $row["NAMA"];
						$noteTL = $row["notes"];
						$rekomen = $row["rekomen"];

						if($rekomen == "Y")
						{
							$rekomen = "REKOMENDASI";
						}
						else if($rekomen == "N")
						{
							$rekomen = "TIDAK REKOMENDASI";
						}
						else
						{
							$rekomen = "-";
						}
			?>

			<table width="100%" align="center" style="border:1px solid black;" class="preview2" cellspacing="0" >
			<tr>
				<td width="15%" colspan="2" style="border:1px solid black;background-color:gray;" align="center"><strong>PERSETUJUAN CREDIT REVIEW</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;" align="center"><strong>TTD</strong><br><br><br><br><br><br><br><? echo $nama;?><? //echo $namajabatan;?></td>
				<td width="15%" style="border:1px solid black;" align="center" valign="top"><strong><? echo $rekomen;?></strong><br>Catatan :<br><br><? echo $noteTL;?></td>
			</tr>
			</table>
			<?
					}
				}
			?>
			<br>
			<?
				/*
				$kodejabatan = "";
				$namajabatan = "";
				$notejabatan = "";
				$namauser = "";

				$tsql = "SELECT * FROM TBL_SE_LEVEL WHERE LEVEL_CODE  LIKE '1%' AND LEVEL_CODE NOT IN ('110', '140', '150', '160', '170') ORDER BY LEVEL_CODE DESC";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$namajabatan = $row["level_name"];
						$kodejabatan = $row["level_code"];

						$tsql2 = "SELECT * FROM tbl_Plafond_Checklevel WHERE txn_id = '$custnomid' AND txn_user_level = '$kodejabatan'";
						$a2 = sqlsrv_query($conn, $tsql2);
						if ( $a2 === false)
						die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($a2))
						{
							if($row2 = sqlsrv_fetch_array($a2, SQLSRV_FETCH_ASSOC))
							{
								$notejabatan = $row2["txn_notes"];
								$namauser = $row2["txn_user_id"];
							}
						}
						else
						{
							$notejabatan = "";
						}
						if($notejabatan != "")
						{

							$tsql2 = "SELECT * FROM Tbl_SE_User WHERE user_id = '$namauser'";
							$a2 = sqlsrv_query($conn, $tsql2);
							if ( $a2 === false)
							die( FormatErrors( sqlsrv_errors() ) );

							if(sqlsrv_has_rows($a2))
							{
								if($row2 = sqlsrv_fetch_array($a2, SQLSRV_FETCH_ASSOC))
								{
									$namauser = $row2["user_name"];
								}
							}

			?>
				<table width="100%" align="center" style="border:1px solid black;" class="preview2" cellspacing="0" >
				<tr>
					<td width="15%" colspan="2" style="border:1px solid black;background-color:gray;" align="center"><strong>PERSETUJUAN <? echo $namajabatan;?></strong></td>
				</tr>
				<tr>
					<td width="15%" style="border:1px solid black;" align="center"><strong>TTD</strong><br><br><br><br><br><br><br><? echo $namauser;?><? //echo $namajabatan;?></td>
					<td width="15%" style="border:1px solid black;" align="center" valign="top"><strong>SETUJU / TIDAK SETUJU</strong><br>Catatan :<br><br><? echo $notejabatan;?></td>
				</tr>
				</table><br>
			<?
						}
					}
				}
				*/
			?>
			</td>
		</tr>

		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<?
			$statuspermission = "";
			$tsql = "SELECT * FROM Tbl_FMKK where txn_id = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);
			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($a))
			{
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$statuspermission = $row["txn_action"];
				}
			}

			if($statuspermission == "C" && $userid != "")
			{
		?>
		</form>
		<form id="frm" name="frm">
		<tr>
			<td align="center">
				<?
				require ("../../requirepage/hiddenfield.php");
				require ("../../requirepage/btnview.php");
				?>
			</td>
			<!--<td align="center"><input type="button" value="APPROVE" style="width:120px;background-color:blue;color:white;" onclick="approve();" /></td>-->
		</tr>
		</form>
		<?
			}
			else if($statuspermission == "A" && $userid != "" &&$userwfid!="")
			{
		?>
		<tr>
			<td align="center" style="color:red;font-size:20px"><i>Telah Diapprove</i></td>
		</tr>
		<?
			}
		?>
		</table>
		<? require("../../requirepage/btnprint.php");?>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<script type="text/javascript">
	function approve() {
		var nan="VIEWMKK";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();

		$.ajax({
			type: "POST",
			url: "MKK_ajax_n.php",
			data: "nan="+nan+"&custnomid="+custnomid+"userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{
				//alert(response);
				alert('Berhasil Diapprove');
				window.location.reload();
			}

		})
	}

</script>
</body>
</html>
