<?php
include("../../../requirepage/session.php");
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");
require_once("../../../lib/class.sqlserver.php");
$sql = new SQLSRV();
$sql->connect();


$paramSyntax = "SELECT * FROM tbl_Branch";
$sql->executeQuery($paramSyntax);
$paramResult = $sql->lastResults;
$paramCount = count($paramResult);

if($sql->debug == true){
/*echo $paramSyntax;
echo "<pre>";
print_r($paramResult);
echo "</pre>";*/
}



//GETDATA
if(isset($_POST['regopt'])){
	
	//echo "IF<br />";
	//print_r($_POST['regopt']);
	$getOption = $_POST['regopt'];
}
else{
	
	$getOption = array();
	for($x=0;$x<$paramCount;$x++){
		$paramCode = $paramResult[$x]['branch_code'];
		$paramName = $paramResult[$x]['branch_name'];
		array_push($getOption,$paramCode);
	}
	//echo "ELSE<br />";
	//print_r($getOption);
}

$totalOption = count($getOption);


$yeslike .= "WHERE ";
for($x=0;$x<$totalOption;$x++){
	$like = $getOption[$x];
	$yeslike .= "branch_code LIKE '$like' OR ";
}
$yeslike = substr($yeslike,0,-3);

$branchSyntax = "SELECT * FROM tbl_Branch $yeslike";
$sql->executeQuery($branchSyntax);
$branchResult = $sql->lastResults;
$branchCount = count($branchResult);

if($sql->debug == true){
/*echo $branchSyntax;
echo "<pre>";
print_r($branchResult);
echo "</pre>";*/
}

$arrayTemp = array();
for($x=0;$x<$branchCount;$x++){
	$color = randomColorOne();
	$branchCode = $branchResult[$x]['branch_code'];
	$branchName = $branchResult[$x]['branch_name'];
	$dataSyntax = "SELECT SUM(custcreditplafond)AS total,'$branchName' AS branch,'$color' AS color FROM tbl_CustomerFacility cf JOIN tbl_FFAK fak ON cf.custnomid = fak.txn_id WHERE fak.txn_branch_code = '$branchCode'";
	$sql->executeQuery($dataSyntax);
	$dataResult = $sql->lastResults;
	$dataCount = count($dataResult);
	
	if($dataResult[0]['total']=="" ){}
	else{array_push($arrayTemp,$dataResult[0]);}
	
	if($sql->debug == true){
	/*echo $dataSyntax;
	echo "<pre>";
	print_r($dataResult);
	echo "</pre>";*/
	}
}

//echo "<pre>";
//print_r($arrayTemp);
//echo "</pre>";

$totalData = count($arrayTemp);
$json =  json_encode($arrayTemp);

function randomColorOne() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 


function randomColorTwo() { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
    shuffle($possibilities);
    $color = "#";
    for($i=1;$i<=6;$i++){
        $color .= $possibilities[rand(0,14)];
    }
    return $color;
} 


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Total Plafond Report</title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>
<script src="../amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
var chart;

var chartData = <?=$json;?>;

AmCharts.ready(function () {
	// SERIAL CHART
	chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "branch";
	// the following two lines makes chart 3D
	chart.depth3D = 20;
	chart.angle = 30;

	// AXES
	// category
	var categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 90;
	categoryAxis.dashLength = 5;
	categoryAxis.gridPosition = "start";

	// value
	var valueAxis = new AmCharts.ValueAxis();
	valueAxis.title = "Total Plafond Based on Branch";
	valueAxis.dashLength = 5;
	chart.addValueAxis(valueAxis);

	// GRAPH
	var graph = new AmCharts.AmGraph();
	graph.valueField = "total";
	graph.colorField = "color";
	graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	graph.type = "column";
	graph.lineAlpha = 0;
	graph.fillAlphas = 1;
	chart.addGraph(graph);

	// CURSOR
	var chartCursor = new AmCharts.ChartCursor();
	chartCursor.cursorAlpha = 0;
	chartCursor.zoomable = false;
	chartCursor.categoryBalloonEnabled = false;
	chart.addChartCursor(chartCursor);

	chart.creditsPosition = "top-right";

	// WRITE
	chart.write("chartdiv");
});
</script>
<script>
function refreshData()
{
	$("#formreport").submit();
}
</script>
</head>
<body style="padding:0px;margin:0px;">
<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
<div id="chartdiv" style="width:<?=($totalData*75)+100;?>px; height:450px;"></div>
<div id="parameter">
<h1 style="margin:0; margin-top:10px; padding:0; padding-left:25px; padding-bottom:10px; font-family:sans-serif;">Option</h1>
<div style="background:#444; color:#fafafa; padding:10px;">
<form name="formreport" id="formreport" method="post" action="#">
<table>
<tr>
<?php
for($x=0;$x<$paramCount;$x++){
$paramCode = $paramResult[$x]['branch_code'];
$paramName = $paramResult[$x]['branch_name'];
if(in_array($paramCode,$getOption)){$checked="checked=\"checked\"";}else{$checked="";}
if ($x % 5 === 0) { ?></tr><tr><? }?>
<td><input type="checkbox" name="regopt[]" onchange="refreshData();" id="<?=$paramCode;?>" value="<?=$paramCode;?>" class="css-checkbox" <?=$checked;?> /><label for="<?=$paramCode;?>" class="css-label">&nbsp;<?=strtoupper($paramName);?>&nbsp;</label></td>
<? }?>
</tr>
</table>
</form>
</div>
<p style="padding-left:25px;">Property of <a href="http://spin-international.com">SPIN-International</a></p>
</div>
</body>
</html>