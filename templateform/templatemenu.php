<?
session_start();
ob_start();
require("sqlsrv.php");

unset($_SESSION['template']);
//echo $_SESSION['template'];
?>

<html>
	<head>
		<title>Create Tempalte</title>
		
		<script src="js/jquery-1.12.3.min.js"></script>
		<script src="bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="js/accounting.min.js"></script>

		<script type="text/javascript" src="js/datatable.js"></script>
        <script type="text/javascript" src="js/bootstraptable.js"></script>

		<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
		<link href="css/table.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="css/d.css"></link>
		
		
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable();
			} );
		</script>
	</head>
<body>
	<div class="container">
		<h1>Template</h1>
		<hr>
			<div style="margin-bottom:20px;">
			<a href="tmp.php?a=0&b=1" class="btn btn-primary btn-md" role="button"><span class="glyphicon glyphicon-plus"></span> Adding</a>
			</div>
			<table id="example" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
				<thead>
					<tr>
						<th style="width:30px;">No. </th>
						<th>Template Code</th>
						<th>Tamplate Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>No. </th>
						<th>Template Code</th>
						<th>Tamplate Name</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					<?
					
					$rs = $condb->_RQ("select _name,_idx from tr_template");
					if(!is_array($rs)){echo $rs;}else
					{
						$no=0;
						foreach($rs as $key => $value) 
						{
							$no++;
							?>
								<tr>
									<td><?=$no?></td>
									<td><?=$rs[$key]['_idx']?></td>
									<td><?=$rs[$key]['_name']?></td>
									<td style="width:200px;text-align:center;">
										<a href="tmp.php?a=1&b=<?=$rs[$key]['_idx']?>" class="btn btn-info btn-xs" role="button"><span class="glyphicon glyphicon-edit"></span> Edit</a>
										<a href="tmp.php?a=2&b=<?=$rs[$key]['_idx']?>" class="btn btn-danger btn-xs" role="button"><span class="glyphicon glyphicon-trash"></span> Delete</a>									
									</td>
								</tr>
							<?
							
						}
					}
					?>
				</tbody>
			</table>
		
	</div>
</body>
</html>