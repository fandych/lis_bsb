<?php
include("../../../requirepage/session.php");
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");
require_once("../../../lib/class.sqlserver.php");
$sql = new SQLSRV();
$sql->connect();


$paramSyntax = "SELECT * FROM tbl_CreditNeed";
$sql->executeQuery($paramSyntax);
$paramResult = $sql->lastResults;
$paramCount = count($paramResult);

if($sql->debug == true){
/*echo $paramSyntax;
echo "<pre>";
print_r($paramResult);
echo "</pre>";*/
}

$arrayTemp = array();

for($x=0;$x<$paramCount;$x++){
	$code = $paramResult[$x]['credit_need_code'];
	$type = $paramResult[$x]['credit_need_name'];
	$color = randomColorOne();
	$dataSyntax = "SELECT '$type' as type,COUNT(*) AS data,'$color' AS color FROM tbl_CustomerFacility WHERE custcreditneed = $code";
	$sql->executeQuery($dataSyntax);
	$dataResult = $sql->lastResults;
	$dataCount = count($dataResult);
	
	array_push($arrayTemp,$dataResult[0]);
	
	if($sql->debug == true){
	/*echo $dataSyntax;
	echo "<pre>";
	print_r($dataResult);
	echo "</pre>";*/
	}
}

//echo "<pre>";
//print_r($arrayTemp);
//echo "</pre>";

$totalData = count($arrayTemp);

$json =  json_encode($arrayTemp);



function randomColorOne() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 


function randomColorTwo() { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
    shuffle($possibilities);
    $color = "#";
    for($i=1;$i<=6;$i++){
        $color .= $possibilities[rand(0,14)];
    }
    return $color;
} 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Credit Need Report</title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>
<script src="../amcharts/pie.js" type="text/javascript"></script>
<script type="text/javascript">
var chart;

var chartData = <?=$json;?>;

AmCharts.ready(function () {
	// PIE CHART
	chart = new AmCharts.AmPieChart();
	chart.dataProvider = chartData;
	chart.titleField = "type";
	chart.valueField = "data";
	chart.colorField = "color";
	chart.outlineColor = "#FFFFFF";
	chart.outlineAlpha = 0.8;
	chart.outlineThickness = 2;
	chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
	// this makes the chart 3D
	chart.depth3D = 15;
	chart.angle = 30;

	// WRITE
	chart.write("chartdiv");
});
</script>
</head>
<body style="padding:0px;margin:0px;">
<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
<div id="chartdiv" style="width: 100%; height: 400px;"></div>
<div id="parameter">
<h1 style="margin:0; margin-top:10px; padding:0; padding-left:25px; padding-bottom:10px; font-family:sans-serif;"></h1>
<div style="background:#444; color:#fafafa; padding:10px;"><!--
<form name="formreport" id="formreport">
<table>
<tr>
<?php
for($x=0;$x<$totalData;$x++){
$crcode = $arrayTemp[$x]['type'];
?>
<td><input type="checkbox" name="cropt" checked="checked" id="<?=$crcode;?>" class="css-checkbox" /><label for="<?=$crcode;?>" class="css-label">&nbsp;<?=strtoupper($crcode);?>&nbsp;</label></td>
<? }?>
</tr>
</table>
</form>!-->
</div>
<p style="padding-left:25px;">Property of <a href="http://spin-international.com">SPIN-International</a></p>
</div>
</body>
</html>