<?
require("../../lib/sqlsrv.lis.php");
require("../../lib/class.odbc.php");
ob_start();

$SIFAT_KREDIT = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD16'";
$ORIENTASI_PENGGUNAAN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD19'";
$JAMINAN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LMCDUA'";
$SEKTOR_EKONOMI = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD53'";
$PUPN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'WBI661'";
$SANDI_REALISASI = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD31'";
$HUBUNGAN_DENGAN_BANK = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'C' AND XFLDNM = 'CNCDU7'";
$STATUS_DEBITUR = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'C' AND XFLDNM = 'CNXC02'";
$LEMBAGA_PEMERINGKAT = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'C' AND XFLDNM = 'CNXH03'";
$NEGARA_PIHAK_PEMOHON = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'D' AND XFLDNM = 'DLDEPB'";
$TIPE_KREDIT_KHUSUS = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LPCDST'";

$GOLONGAN_PENJAMIN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LNCDBI'";
$GOLONGAN_DEBITUR = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'C' AND XFLDNM = 'CNXH01'";

$KATEGORI_PENGUKURAN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD15'";
$KATEGORI_DEBITUR = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD22'";
$KATEGORI_PORTOFOLIO = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD23'";

$JENIS_ASURANSI = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'WBI662'";
$JENIS_PENGIKAT = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'WBI663'";
$JENIS_KREDIT = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD24'";
$JENIS_SUKU_BUNGA = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD18'";
$JENIS_GARANSI = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LXCD25'";
$JENIS_PINJAMAN = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'L' AND XFLDNM = 'LNBI65'";

$KODE_LOKASI = "SELECT XFMLCD,XFDESC FROM INDONESIAT.XTCOD WHERE XFAPCD = 'C' AND XFLDNM = 'CARGCD'";
$KODE_PLAN = "SELECT LPCDPL AS XFMLCD,LPDESC AS XFDESC FROM VISIONT.LPLAN";

$MARKETING_KOLEKTIF = "SELECT LONOBR,LONOOF,LODSNM FROM VISIONT.LOFF";
$BRANCH_DINAS = "SELECT L3COD1,L3DES1,L3COD2,L3DES2,L3COD3,L3DES3,L3COD4,L3DES4 FROM VISIONT.LDIN3";

process_param($SIFAT_KREDIT,strtolower(get_var_name($SIFAT_KREDIT)));
process_param($ORIENTASI_PENGGUNAAN,strtolower(get_var_name($ORIENTASI_PENGGUNAAN)));
process_param($JAMINAN,strtolower(get_var_name($JAMINAN)));
process_param($SEKTOR_EKONOMI,strtolower(get_var_name($SEKTOR_EKONOMI)));
process_param($PUPN,strtolower(get_var_name($PUPN)));
process_param($SANDI_REALISASI,strtolower(get_var_name($SANDI_REALISASI)));
process_param($HUBUNGAN_DENGAN_BANK,strtolower(get_var_name($HUBUNGAN_DENGAN_BANK)));
process_param($STATUS_DEBITUR,strtolower(get_var_name($STATUS_DEBITUR)));
process_param($LEMBAGA_PEMERINGKAT,strtolower(get_var_name($LEMBAGA_PEMERINGKAT)));
process_param($NEGARA_PIHAK_PEMOHON,strtolower(get_var_name($NEGARA_PIHAK_PEMOHON)));
process_param($TIPE_KREDIT_KHUSUS,strtolower(get_var_name($TIPE_KREDIT_KHUSUS)));
process_param($GOLONGAN_PENJAMIN,strtolower(get_var_name($GOLONGAN_PENJAMIN)));
process_param($GOLONGAN_DEBITUR,strtolower(get_var_name($GOLONGAN_DEBITUR)));
process_param($KATEGORI_PENGUKURAN,strtolower(get_var_name($KATEGORI_PENGUKURAN)));
process_param($KATEGORI_DEBITUR,strtolower(get_var_name($KATEGORI_DEBITUR)));
process_param($KATEGORI_PORTOFOLIO,strtolower(get_var_name($KATEGORI_PORTOFOLIO)));
process_param($JENIS_ASURANSI,strtolower(get_var_name($JENIS_ASURANSI)));
process_param($JENIS_PENGIKAT,strtolower(get_var_name($JENIS_PENGIKAT)));
process_param($JENIS_KREDIT,strtolower(get_var_name($JENIS_KREDIT)));
process_param($JENIS_SUKU_BUNGA,strtolower(get_var_name($JENIS_SUKU_BUNGA)));
process_param($JENIS_GARANSI,strtolower(get_var_name($JENIS_GARANSI)));
process_param($JENIS_PINJAMAN,strtolower(get_var_name($JENIS_PINJAMAN)));
process_param($KODE_LOKASI,strtolower(get_var_name($KODE_LOKASI)));
process_param($KODE_PLAN,strtolower(get_var_name($KODE_PLAN)));

process_officer($MARKETING_KOLEKTIF, strtolower(get_var_name($MARKETING_KOLEKTIF)));
process_branch($BRANCH_DINAS,strtolower(get_var_name($BRANCH_DINAS)));

function process_param($query,$param_name)
{
    $db_lis = new DB_LIS();
    $db_lis->connect();

    $ibm_connection = "
    Driver={Client Access ODBC Driver (32-bit)};
    System=172.17.115.1;
    Uid=ELOSUSR;
    Pwd=ELOSUSR;
    ";

    $db_ibm = new DB_ODBC();
    $db_ibm->connect($ibm_connection,"ELOSUSR","ELOSUSR");

    echo "================================================<br/>";
    echo "QUERY " .$query . " DONE.<br/>";

    $db_ibm->queryWithResult($query);
    $param_result = $db_ibm->result;

    echo "DATA FROM - " . $param_name . " SAVED.<br/>";

    $table_name = "param_" . $param_name;

    $truncate = "TRUNCATE TABLE $table_name";
    $db_lis->executeNonQuery($truncate);

    echo "TRUNCATE TABLE - " . $table_name . " DONE.<br/>";

    for($x=0;$x<count($param_result);$x++)
    {
        $code = trim($param_result[$x]['XFMLCD']);
        $val = trim(str_replace("'","",$param_result[$x]['XFDESC']));
        $insert = "INSERT INTO $table_name (code,attribute) VALUES ('$code','$val')";
        $db_lis->executeNonQuery($insert);
    }

    echo "INSERT DATA - " . $table_name . " DONE.<br/>";
    echo "================================================<br/><br/><br/>";
}

function process_officer($query,$param_name)
{
    $db_lis = new DB_LIS();
    $db_lis->connect();

    $ibm_connection = "
    Driver={Client Access ODBC Driver (32-bit)};
    System=172.17.115.1;
    Uid=ELOSUSR;
    Pwd=ELOSUSR;
    ";

    $db_ibm = new DB_ODBC();
    $db_ibm->connect($ibm_connection,"ELOSUSR","ELOSUSR");

    echo "================================================<br/>";
    echo "QUERY " .$query . " DONE.<br/>";

    $db_ibm->queryWithResult($query);
    $param_result = $db_ibm->result;

    echo "DATA FROM - " . $param_name . " SAVED.<br/>";

    $table_name = "param_" . $param_name;

    $truncate = "TRUNCATE TABLE $table_name";
    $db_lis->executeNonQuery($truncate);

    echo "TRUNCATE TABLE - " . $table_name . " DONE.<br/>";

    for($x=0;$x<count($param_result);$x++)
    {
        $_0 = trim($param_result[$x]['LONOBR']);
        $_1 = trim($param_result[$x]['LONOOF']);
        $_2 = trim($param_result[$x]['LODSNM']);

        $insert = "INSERT INTO $table_name (LONOBR,LONOOF,LODSNM) VALUES ('$_0','$_1','$_2')";
        $db_lis->executeNonQuery($insert);
    }

    echo "INSERT DATA - " . $table_name . " DONE.<br/>";
    echo "================================================<br/><br/><br/>";
}


function process_branch($query,$param_name)
{
    $db_lis = new DB_LIS();
    $db_lis->connect();

    $ibm_connection = "
    Driver={Client Access ODBC Driver (32-bit)};
    System=172.17.115.1;
    Uid=ELOSUSR;
    Pwd=ELOSUSR;
    ";

    $db_ibm = new DB_ODBC();
    $db_ibm->connect($ibm_connection,"ELOSUSR","ELOSUSR");

    echo "================================================<br/>";
    echo "QUERY " .$query . " DONE.<br/>";

    $db_ibm->queryWithResult($query);
    $param_result = $db_ibm->result;

    echo "DATA FROM - " . $param_name . " SAVED.<br/>";

    $table_name = "param_" . $param_name;

    $truncate = "TRUNCATE TABLE $table_name";
    $db_lis->executeNonQuery($truncate);

    echo "TRUNCATE TABLE - " . $table_name . " DONE.<br/>";

    for($x=0;$x<count($param_result);$x++)
    {
        $_0 = trim(str_replace("'","",$param_result[$x]['L3COD1']));
        $_1 = trim(str_replace("'","",$param_result[$x]['L3DES1']));
        $_2 = trim(str_replace("'","",$param_result[$x]['L3COD2']));
        $_3 = trim(str_replace("'","",$param_result[$x]['L3DES2']));
        $_4 = trim(str_replace("'","",$param_result[$x]['L3COD3']));
        $_5 = trim(str_replace("'","",$param_result[$x]['L3DES3']));
        $_6 = trim(str_replace("'","",$param_result[$x]['L3COD4']));
        $_7 = trim(str_replace("'","",$param_result[$x]['L3DES4']));

        $insert = "INSERT INTO $table_name (L3COD1,L3DES1,L3COD2,L3DES2,L3COD3,L3DES3,L3COD4,L3DES4) VALUES ('$_0','$_1','$_2','$_3','$_4','$_5','$_6','$_7')";
        $db_lis->executeNonQuery($insert);
    }

    echo "INSERT DATA - " . $table_name . " DONE.<br/>";
    echo "================================================<br/><br/><br/>";
}

function get_var_name(){
    $bt   = debug_backtrace();
    $file = file($bt[0]['file']);
    $src  = $file[$bt[0]['line']-1];
    $pat  = '#(.*)get_var_name *?\( *?\$(.*) *?\)(.*)#i';
    $var  = preg_replace($pat, '$2', $src);
    $var  = str_replace("$", "", $var);
    $var  = str_replace(")", "", $var);
    return trim($var);
}
?>
