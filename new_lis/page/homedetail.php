<?php

require ("../lib/formatError.php");
require ("../lib/open_con.php");
//require ("../lib/open_con_dm.php");


$userid=$_REQUEST['userid'];
$act=$_REQUEST['act'];
$thestatus=$_REQUEST['thestatus'];



$kondisistatus = "";
if ($thestatus == "P")
{
	$theheader = "DETAIL SELURUH APPLICATION ID ";
}
else
{
	$kondisistatus = "AND Tbl_Flow_Status.txn_action='$thestatus'";
	$theheader = "DETAIL APPLICATION ID ";
	if ($thestatus == "I")
	{
		$theheader .= " YANG MASIH POSISI INPUT";
	}
	else if($thestatus == "A")
	{
		$theheader .= " YANG SUDAH POSISI APPROVE";
	}
	else if($thestatus == "J")
	{
		$theheader .= " YANG SUDAH POSISI REJECT";
	}

}



if ($act == "delnotif")
{
      DELNOTIF();
}

if ($act == "rollbacknotif")
{
      RBNOTIF();
}

  $slacount = 0;
	$tsql = "select sum(Tbl_Workflow.wf_score)
   					from Tbl_Workflow";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

	if ( $sqlConn === false)
		die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$slacount = $row[0];
		}
	}
	sqlsrv_free_stmt( $sqlConn );

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Expired" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-Cache">
<meta http-equiv="Pragma" CONTENT="no-Cache">
<head >
    <title></title>
    <style type="text/css">
        body
        {
            margin-top: 0;
            margin-bottom: 0;
            margin-left: 0;
            margin-right: 0;
            padding-left: 0;
            padding-right: 0;
        }
		
		.meter {
			height: 20px;  /* Can be anything */
			position: relative;
			/* margin: 60px 0 20px 0; */
			/* background: #555; */
			-moz-border-radius: 25px;
			-webkit-border-radius: 25px;
			border-radius: 25px;
			padding: 10px;
			-webkit-box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
			-moz-box-shadow   : inset 0 -1px 1px rgba(255,255,255,0.3);
			box-shadow        : inset 0 -1px 1px rgba(255,255,255,0.3);
		}
		.meter > span {
			display: block;
			height: 100%;
			   -webkit-border-top-right-radius: 8px;
			-webkit-border-bottom-right-radius: 8px;
				   -moz-border-radius-topright: 8px;
				-moz-border-radius-bottomright: 8px;
					   border-top-right-radius: 8px;
					border-bottom-right-radius: 8px;
				-webkit-border-top-left-radius: 20px;
			 -webkit-border-bottom-left-radius: 20px;
					-moz-border-radius-topleft: 20px;
				 -moz-border-radius-bottomleft: 20px;
						border-top-left-radius: 20px;
					 border-bottom-left-radius: 20px;
			
			background-color: rgb(43,194,83);
			
			background-image: -webkit-gradient(
			  linear,
			  left bottom,
			  left top,
			  color-stop(0, rgb(43,194,83)),
			  color-stop(1, rgb(84,240,84))
			 );
			 
			background-image: -moz-linear-gradient(
			  center bottom,
			  rgb(43,194,83) 37%,
			  rgb(84,240,84) 69%
			 );
			 
			-webkit-box-shadow:
			  inset 0 2px 9px  rgba(255,255,255,0.3),
			  inset 0 -2px 6px rgba(0,0,0,0.4);
			-moz-box-shadow:
			  inset 0 2px 9px  rgba(255,255,255,0.3),
			  inset 0 -2px 6px rgba(0,0,0,0.4);
			box-shadow:
			  inset 0 2px 9px  rgba(255,255,255,0.3),
			  inset 0 -2px 6px rgba(0,0,0,0.4);
			  
			position: relative;
			overflow: hidden;
		}
		.meter > span:after, .animate > span > span {
			content: "";
			position: absolute;
			top: 0; left: 0; bottom: 0; right: 0;
			background-image:
			   -webkit-gradient(linear, 0 0, 100% 100%,
				  color-stop(.25, rgba(255, 255, 255, .2)),
				  color-stop(.25, transparent), color-stop(.5, transparent),
				  color-stop(.5, rgba(255, 255, 255, .2)),
				  color-stop(.75, rgba(255, 255, 255, .2)),
				  color-stop(.75, transparent), to(transparent)
			   );
			background-image:
				-moz-linear-gradient(
				  -45deg,
				  rgba(255, 255, 255, .2) 25%,
				  transparent 25%,
				  transparent 50%,
				  rgba(255, 255, 255, .2) 50%,
				  rgba(255, 255, 255, .2) 75%,
				  transparent 75%,
				  transparent
			   );
			z-index: 1;
			-webkit-background-size: 50px 50px;
			-moz-background-size: 50px 50px;
			background-size: 50px 50px;
			-webkit-animation: move 2s linear infinite;
			-moz-animation: move 2s linear infinite;
			   -webkit-border-top-right-radius: 8px;
			-webkit-border-bottom-right-radius: 8px;
				   -moz-border-radius-topright: 8px;
				-moz-border-radius-bottomright: 8px;
					   border-top-right-radius: 8px;
					border-bottom-right-radius: 8px;
				-webkit-border-top-left-radius: 20px;
			 -webkit-border-bottom-left-radius: 20px;
					-moz-border-radius-topleft: 20px;
				 -moz-border-radius-bottomleft: 20px;
						border-top-left-radius: 20px;
					 border-bottom-left-radius: 20px;
			overflow: hidden;
		}

		.animate > span:after {
			display: none;
		}

		@-webkit-keyframes move {
			0% {
			   background-position: 0 0;
			}
			100% {
			   background-position: 50px 50px;
			}
		}

		@-moz-keyframes move {
			0% {
			   background-position: 0 0;
			}
			100% {
			   background-position: 50px 50px;
			}
		}


		.orange > span {
			background-color: #f1a165;
			background-image: -moz-linear-gradient(top, #f1a165, #f36d0a);
			background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f1a165),color-stop(1, #f36d0a));
			background-image: -webkit-linear-gradient(#f1a165, #f36d0a);
		}

		.red > span {
			background-color: #f0a3a3;
			background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);
			background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));
			background-image: -webkit-linear-gradient(#f0a3a3, #f42323);
		}

		.nostripes > span > span, .nostripes > span:after {
			-webkit-animation: none;
			-moz-animation: none;
			background-image: none;
		}
    </style>
	<script src="/eoffice/Includes/speedo/jquery-1.4.min.js"></script>


	<script src="/eoffice/Includes/speedo/jquery.speedometer.js"></script>
	<script src="/eoffice/Includes/speedo/jquery.jqcanvas-modified.js"></script>
	<script src="/eoffice/Includes/speedo/excanvas-modified.js"></script>
    <link href="/eoffice/Includes/CSS/niceforms-default.css" type="text/css" rel="stylesheet" />
    <link href="/eoffice/Includes/CSS/greenyours.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="/eoffice/Includes/Javascript/datetimepicker_css.js"></script>

    <script language="javascript" type="text/javascript" src="/eoffice/Includes/Javascript/niceforms.js"></script>
    <Script type="text/javascript">
       function viewDetail()
       {
           document.formhome.act.value = "";
           document.formhome.target = "lainnya";
           document.formhome.action = "./homedetail.php";
           document.formhome.submit();
       }
    </script>
	<script>

		$(function(){
			$('#test').speedometer();

			$('.changeSpeedometer').click(function(){
				$('#test').speedometer({ percentage: $('.speedometer').val() || 0 });
			});

		});



	</script>

</head>
<body link=black alink=black vlink=black onload=self.focus()>
    <form name=formhome id="form1" class="niceform" method=post>
    	<div align=center>
    		<BR>
    		<center><font face=Arial size=3><b> <? echo $theheader ?></b></font></center>
    		<BR><BR>
    	   <table width="700px" cellpadding=5 cellspacing=0 border=1 bordercolor="#000000" bordercolorlight="#000000" bordercolordark="#FFFFFF">
    	   	 <tr>
    	   	   <Td width=15% align=center valign=top>
    	   	   	 <font face=Arial size=2><b>ID</b></font>
    	   	  </td>
    	   	   <Td width=40% align=center valign=top>
    	   	   	 <font face=Arial size=2><b>NAMA</b></font>
    	   	  </td>
    	   	   <Td width=15% align=center valign=top>
    	   	   	 <font face=Arial size=2><b>FLOW</b></font>
    	   	  </td>
    	   	   <Td width=30% align=center valign=top>
    	   	   	 <font face=Arial size=2><b>PERSENTASE</b></font>
    	   	  </td>
    	   	 </tr>
<?
		  $oldtxnid = "";
      $tsql = "SELECT Tbl_Flow_Status.txn_id, Tbl_Flow_Status.txn_flow, Tbl_Flow_Status.txn_action
      				 FROM Tbl_Flow_Status, Tbl_SLADetail
      				  WHERE Tbl_Flow_Status.txn_id=Tbl_SLADetail.sla_nomid
      				  AND Tbl_SLADetail.sla_userid='$userid'
      				  $kondisistatus
      				  ORDER BY Tbl_Flow_Status.txn_time";
					  //echo $tsql;
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      $hitung = 0;
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );
      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
         	 if ($oldtxnid != $row[0])
         	 {
         	 	  $oldtxnid = $row[0];
   			  		$tsql2 = "SELECT custfullname
						        		FROM Tbl_CustomerMasterPerson
						        		WHERE custnomid='$row[0]'";
          		$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  		$params2 = array(&$_POST['query']);
   		    		$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
							$custname = "&nbsp";
          		if ( $sqlConn2 === false)
      					die( FormatErrors( sqlsrv_errors() ) );

   			  		if(sqlsrv_has_rows($sqlConn2))
   			  		{
      		   		$rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 		while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
      		   		{
      		   			$custname .= $row2[0];
      		   		}
   						}
   	      		sqlsrv_free_stmt( $sqlConn2 );

							$wfurut = 0;
   			  		$tsql2 = "select wf_urut
   									from Tbl_Workflow
   									WHERE wf_id='$row[1]'";
          		$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  		$params2 = array(&$_POST['query']);
   		    		$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
          		if ( $sqlConn2 === false)
      					die( FormatErrors( sqlsrv_errors() ) );

   			  		if(sqlsrv_has_rows($sqlConn2))
   			  		{
      		   		$rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 		while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
      		   		{
      		   			$wfurut = $row2[0];
      		   		}
   						}
   	      		sqlsrv_free_stmt( $sqlConn2 );

							$slasum = 0;
   			  		$tsql2 = "select sum(Tbl_Workflow.wf_score)
   											from Tbl_Workflow
   											WHERE wf_urut <= '$wfurut'";
          		$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  		$params2 = array(&$_POST['query']);

   		    		$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
          		if ( $sqlConn2 === false)
      					die( FormatErrors( sqlsrv_errors() ) );

   			  		if(sqlsrv_has_rows($sqlConn2))
   			  		{
      		   		$rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 		while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
      		   		{
      		   			$slasum = $row2[0];
      		   		}
   						}
   	      		sqlsrv_free_stmt( $sqlConn2 );
?>
    	   	 		<tr>
    	   	   		<Td width=15% align=center valign=top>
    	   	   	 		<font face=Arial size=2><? echo $row[0]; ?></font>
    	   	  		</td>
    	   	   		<Td width=40% align=center valign=top>
    	   	   	 		<font face=Arial size=2><? echo $custname; ?></font>
    	   	  		</td>
    	   	   		<Td width=15% align=center valign=top>
    	   	   	 		<font face=Arial size=2><? echo $row[1]; ?> <b>(<? echo $row[2]; ?>)</b></font>
    	   	  		</td>
    	   	   		<Td width="70%" align=left valign=top>
<?
     							$persen = round(($slasum/$slacount) * 100,2);
								
	   							$varpersen = $persen * 1;
								
								if($thestatus == "J" || $row[2] == "J")
								{
									$varpersen = 100;
									$persen = 100;
								}
								
	   							if ($persen > 50)
	   							{
									echo "
									<div class=\"meter\" style=\"text-align: center;\">
										<span style=\"width: ".$varpersen."%;\"></span>
										<font style=\"font-size: 12px; font-family: helvetica;\">".$persen."%</font>
									</div>
									";
	   							}
	   							else
	   							{
									echo "
									<div class=\"meter red\" style=\"text-align: center;\">
										<span style=\"width: ".$varpersen."%;\"></span>
										<font style=\"font-size: 12px; font-family: helvetica;\">".$persen."%</font>
									</div>
									";
	   							}
?>
					
					
					
    	   	  		</td>
    	   	 		</tr>
<?
           }
         }
      }
      sqlsrv_free_stmt( $sqlConn );

?>

    	   </table>
      </div>
    <input type=hidden name=userid value=bhartoyo>
    <input type=hidden name=userpwd value=4448dd9a39ab97e1>
    <input type=hidden name=userprgcode>
    <input type=hidden name=act>
    </form>
</body>
</html>

<?

function DELNOTIF()
{
require ("../lib/open_con.php");
//require ("../lib/open_con_dm.php");

   $thenomid=$_POST['thenomid'];
   $thetanggal=$_POST['thetanggal'];
   $userid=$_REQUEST['userid'];

	$tsql = "DELETE FROM Tbl_Notification
						WHERE custnomid='$thenomid' AND note_userid = '$userid'
						AND cast(note_tanggal as varchar)='$thetanggal'";
	$params = array(&$_REQUEST['query']);
	$stmt = sqlsrv_prepare( $conn, $tsql, $params);
//   $stmt = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if( $stmt )
	{
	}
	else
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( sqlsrv_execute( $stmt))
	{
	}
	else
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

   Header("Location:./home.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");

}

function RBNOTIF()
{
require ("../lib/open_con.php");
require ("../lib/open_con_dm.php");

   $thenomid=$_POST['thenomid'];
   $thetanggal=$_POST['thetanggal'];
   $thewfid=$_POST['thewfid'];
   $userid=$_REQUEST['userid'];
	$tsql = "DELETE FROM Tbl_Notification
						WHERE custnomid='$thenomid'
						AND cast(note_tanggal as varchar)='$thetanggal'";
	$params = array(&$_REQUEST['query']);
	$stmt = sqlsrv_prepare( $conn, $tsql, $params);
	if( $stmt )
	{
	}
	else
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( sqlsrv_execute( $stmt))
	{
	}
	else
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	$tsql = "UPDATE Tbl_F$thewfid
						set txn_action='I'
						WHERE txn_id='$thenomid'";
	$params = array(&$_REQUEST['query']);
	$stmt = sqlsrv_prepare( $conn, $tsql, $params);
//   $stmt = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if( $stmt )
	{
	}
	else
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( sqlsrv_execute( $stmt))
	{
	}
	else
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

   Header("Location:./home.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");

}
