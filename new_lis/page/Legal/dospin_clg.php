<?php

require_once('../../lib/formatError.php');
require_once('../../lib/open_con.php');
require_once('../../requirepage/parameter.php');

$hiddenfield=$_POST['hiddenfield'];

$vartambahan=$_POST['vartambahan'];
$datadoc=$_POST['datadoc'];
$arrdoc=explode("|",$datadoc);


if ($vartambahan != "")
{
   $arrvartambahan = explode("|",$vartambahan);
   for ($i = 0; $i<count($arrvartambahan)-1;$i++)
   {
      $vartemparr=explode("__",$arrvartambahan[$i]);
	   $vartemp = "DN" . $arrvartambahan[$i];
	   $vardocno=$_POST[$vartemp];
	   $vartemp = "DE" . $arrvartambahan[$i];
	   $vardocexp=$_POST[$vartemp];

		$tsql = "select COUNT(*)
					FROM Tbl_CKPKLegal
					WHERE docnomid='$custnomid'
					AND doccode='$vartemparr[0]' 
					AND doc_jeniscol='$vartemparr[1]'
								 AND doc_colid='$vartemparr[2]'
								 AND doc_groupcol='$vartemparr[3]'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);

		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		if ( $sqlConn === false)
			  die( FormatErrors( sqlsrv_errors() ) );

		if(sqlsrv_has_rows($sqlConn))
		{
			  $rowCount = sqlsrv_num_rows($sqlConn);
			  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
			  {
				   $thecount = $row[0];
			  }
		}
		sqlsrv_free_stmt( $sqlConn );
	    if ($thecount <= 0)
		{
      	  $tsql = "INSERT INTO Tbl_CKPKLegal VALUES('$custnomid','$vartemparr[0]',
      	  				 '$vardocno','$vardocexp','','','E',
      	  				 '$vartemparr[1]','$vartemparr[2]','$vartemparr[3]')";
		}
	    else
		{
      	  $tsql = "UPDATE Tbl_CKPKLegal 
		  			SET docno='$vardocno', docdate='$vardocexp', doc_flag = 'E'
					WHERE docnomid='$custnomid'
					AND doccode='$vartemparr[0]' 
					AND doc_jeniscol='$vartemparr[1]'
								 AND doc_colid='$vartemparr[2]'
								 AND doc_groupcol='$vartemparr[3]' ";
		}
      	  $params = array(&$_POST['query']);

      	  $stmt = sqlsrv_prepare( $conn, $tsql, $params);
      		if( $stmt )
      	  {
      	  } 
      	  else
      	  {
         		echo "Error in preparing statement.\n";
         		die( print_r( sqlsrv_errors(), true));
      	  }

      		if( sqlsrv_execute( $stmt))
      		{
      		}
      		else
      		{
        		echo "Error in executing statement.\n";
        		die( print_r( sqlsrv_errors(), true));
      		}
      		sqlsrv_free_stmt( $stmt);

   }
}

   $arrjaminan=explode(",",$hiddenfield);

   for($ki=0;$ki<count($arrjaminan);$ki++)
   {
      $vartemparr=explode("__",$arrjaminan[$ki]);
      $doccode = $vartemparr[0];
		  $vartemp = "NOTE" . $arrjaminan[$ki];
			$docnotes=$_POST[$vartemp];
		  $vartemp = "D" . $arrjaminan[$ki];
		  $doccheck = "N";
		  if (isset($_POST[$vartemp]))
		  {
			   $doccheck=$_POST[$vartemp];
			}
      
				$tsql = "UPDATE Tbl_CKPKLegal set docnotes='$docnotes',doccheck='$doccheck'
								 WHERE docnomid='$custnomid'
								 AND doccode='$doccode'
								 AND doc_jeniscol='$vartemparr[1]'
								 AND doc_colid='$vartemparr[2]'
								 AND doc_groupcol='$vartemparr[3]'";
				$params = array(&$_POST['query']);
				$stmt = sqlsrv_prepare( $conn, $tsql, $params);
				if( $stmt )
				{
				} 
				else
				{
					echo "Error in preparing statement.\n";
					die( print_r( sqlsrv_errors(), true));
				}
	
				if( sqlsrv_execute( $stmt))
				{
				}
				else
				{
					echo "Error in executing statement.\n";
					die( print_r( sqlsrv_errors(), true));
				}
				sqlsrv_free_stmt( $stmt);
	}

/*      for ($i=0;$i<$countdataall;$i++)
      {
      	$doccode = substr($dataall[$i],2-1);
				$tsql = "UPDATE Tbl_CKPKLegal set doccheck='Y'
								 WHERE docnomid='$custnomid'
								 AND doccode='$doccode'";
				$params = array(&$_POST['query']);
				$stmt = sqlsrv_prepare( $conn, $tsql, $params);
				if( $stmt )
				{
				} 
				else
				{
					echo "Error in preparing statement.\n";
					die( print_r( sqlsrv_errors(), true));
				}
	
				if( sqlsrv_execute( $stmt))
				{
				}
				else
				{
					echo "Error in executing statement.\n";
					die( print_r( sqlsrv_errors(), true));
				}
				sqlsrv_free_stmt( $stmt);
      }*/

						$tsql = "DELETE FROM Tbl_TemporariUserAkses
											where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
						$params = array(&$_POST['query']);
						$stmt = sqlsrv_prepare( $conn, $tsql, $params);
						if( $stmt )
						{
						} 
						else
						{
							echo "Error in preparing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
	
						if( sqlsrv_execute( $stmt))
						{
						}
						else
						{
							echo "Error in executing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
						sqlsrv_free_stmt( $stmt);

require_once('../../requirepage/do_saveflow.php');	
   header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
   
?> 
