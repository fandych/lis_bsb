<?
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");
?>

<table border="1" width="2200px" align="center" id="tblpreview" cellspacing="0">	
<tr>
	<td><strong>Nama Debitur</strong></td>
	<td><strong>Nama Bank</strong></td>
	<td><strong>Input SID</strong></td>
	<td><strong>Plafond</strong></td>
	<td><strong>No Rekening</strong></td>
	<td><strong>Tanggal Permintaan</strong></td>
	<td><strong>Keterangan Kondisi</strong></td>
	<td><strong>Jenis Fasilitas</strong></td>
	<td><strong>Baki Debet</strong></td>
	<td><strong>Sektor Ekonomi</strong></td>
	<td><strong>Tanggal Mulai</strong></td>
	<td><strong>Akad Awal</strong></td>
	<td><strong>Jatuh Tempo</strong></td>
	<td><strong>Kolektibilitas saat ini</strong></td>
	<td><strong>Kolektibilitas terendah</strong></td>
	<td><strong>Jumlah Hari Tunggakan</strong></td>
</tr>

<?
	$nama_debitur = "";
	$sifat = "";
	$norek = "";
	$tanggal_permintaan = "";
	$nama_bank = "";
	$ket_kondisi = "";
	$jenis_fasilitas = "";
	$baki_debet = 0;
	$sektor_ekonomi = "";
	$tanggal_mulai = "";
	$akad_awal = "";
	$jatuh_tempo = "";
	$input_sid = "";
	$kolektibilitas = "";
	$kolektibilitas_terendah = "";
	$jumlah_hari_tunggakan = "";
	$ct = 0;
	$strsqlv01="SELECT * FROM tbl_birs where custnomid = '$custnomid'";
	$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
	if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlconv01))
	{
		while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
		{
			$custnomid = $rowsv01['custnomid'];
			$nama_debitur = $rowsv01['nama_debitur'];
			$sifat = $rowsv01['sifat'];
			$norek = $rowsv01['norek'];
			$tanggal_permintaan = $rowsv01['tanggal_permintaan'];
			$nama_bank = $rowsv01['nama_bank'];
			$ket_kondisi = $rowsv01['ket_kondisi'];
			$jenis_fasilitas = $rowsv01['jenis_fasilitas'];
			$baki_debet = $rowsv01['baki_debet'];
			$sektor_ekonomi = $rowsv01['sektor_ekonomi'];
			$tanggal_mulai = $rowsv01['tanggal_mulai'];
			$akad_awal = $rowsv01['akad_awal'];
			$jatuh_tempo = $rowsv01['jatuh_tempo'];
			$input_sid = $rowsv01['input_sid'];
			$kolektibilitas = $rowsv01['kolektibilitas'];
			$kolektibilitas_terendah = $rowsv01['kolektibilitas_terendah'];
			$jumlah_hari_tunggakan = $rowsv01['jumlah_hari_tunggakan'];
			$ct++;
			
			$tsql2 = "SELECT * FROM param_sid where code = '$input_sid'";
			$b2 = sqlsrv_query($conn, $tsql2);
			if ( $b2 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($b2))
			{ 
				if($rowType2 = sqlsrv_fetch_array($b2, SQLSRV_FETCH_ASSOC))
				{
					$attribute = $rowType2['attribute'];
				}
			}

      $varsektorekonomi = $sektor_ekonomi;
			$tsql2 = "select ekonomi_code,ekonomi_name from Tbl_SektorEkonomi where ekonomi_code = '$sektor_ekonomi'";
			$b2 = sqlsrv_query($conn, $tsql2);
			if ( $b2 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($b2))
			{ 
				if($rowType2 = sqlsrv_fetch_array($b2, SQLSRV_FETCH_ASSOC))
				{
					$varsektorekonomi = $sektor_ekonomi . " - " . $rowType2['ekonomi_name'];
				}
			}
?>

<tr>
	<td><?=$nama_debitur?></td>
	<td><?=$nama_bank?></td>
	<td><?=$attribute?></td>
	<td><?=$sifat?></td>
	<td><?=$norek?></td>
	<td><?=$tanggal_permintaan?></td>
	<td><?=$ket_kondisi?></td>
	<td><?=$jenis_fasilitas?></td>
	<td><?=numberFormat($baki_debet)?></td>
	<td><?=$varsektorekonomi?></td>
	<td><?=$tanggal_mulai?></td>
	<td><?=$akad_awal?></td>
	<td><?=$jatuh_tempo?></td>
	<td><?=$kolektibilitas?></td>
	<td><?=$kolektibilitas_terendah?></td>
	<td><?=$jumlah_hari_tunggakan?></td>
</tr>

				
<?			
		}
	}
?>

</table>