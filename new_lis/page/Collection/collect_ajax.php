<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");

$nan=$_POST['nan'];

if($nan == "SUBMIT")
{
	$custnomid = $_POST['custnomid'];

	$seq = "";
	$type = "";
	$need = "";
	$angsuran = "";
	$nominal = "";
	$sukubunga = "";
	$periode = "";
	$custname = "";
	$jangkawaktu1 = "";
	$jangkawaktu2 = "";
	$collectnom = "";
	$collectdate = "<font color=red><b>Belum Pernah Bayar</b></font>";
	
	
//	$tsql = "SELECT * from tbl_customerfacility2 b where custnomid = '$custnomid' AND b.custfacseq NOT IN (SELECT c.seq from tbl_collect_payment c where c.custnomid = b.custnomid)";
	$tsql = "SELECT * from tbl_customerfacility2 b where custnomid = '$custnomid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	$rowCount = sqlsrv_num_rows($sqlConn);
	
	if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$seq = $row['custfacseq'];
			$type = $row['custcredittype'];
			$need = $row['custcreditneed'];
			$angsuran = $row['mkknewangsuran'];
			$nominal = number_format($row['custcreditplafond']);
			$sukubunga = $row['sukubungayangdiberikan'];
			$periode = $row['custcreditlong'];
			$jangkawaktu1 = $row['mpk_jangka_waktu1'];
			$jangkawaktu2 = $row['mpk_jangka_waktu2'];
			
			$need = $row['custcreditneed'];

			$tsql2 = "SELECT custfullname from tbl_customermasterperson where custnomid = '$custnomid'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);
			
			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
				{
					$custname = $row2['custfullname'];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );

			$tsql2 = "SELECT * from tbl_creditneed where credit_need_code = '$need'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);
			
			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
				{
					$need = $row2['credit_need_name'];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );
			
			$type = $row['custcredittype'];
			$tsql2 = "SELECT * from tbl_kodeproduk where produk_loan_type = '$type'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);
			
			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
				{
					$type = $row2['produk_type_description'];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );

			$tsql2 = "SELECT TOP 1 nominal, cast(datepayment as varchar) from tbl_collect_payment where custnomid = '$custnomid' order by datepayment desc";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);

			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				{
					$collectnom = $row2[0];
					$collectdate = $row2[1];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );

			
			echo '<table width="700px" align="center" style="" class="input" border="1px" cellspacing="0px">';
			echo '<tr>';
				echo '<td>';
				echo 'Custnomid';
				echo '</td>';
				echo '<td>';
				echo $custnomid . " - " . $custname;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Jenis';
				echo '</td>';
				echo '<td>';
				echo $need;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Tipe';
				echo '</td>';
				echo '<td>';
				echo $type;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Plafond';
				echo '</td>';
				echo '<td>';
				echo $nominal;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Suku Bunga';
				echo '</td>';
				echo '<td>';
				echo $sukubunga."%";
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Periode';
				echo '</td>';
				echo '<td>';
				echo $periode." Bulan ($jangkawaktu1 s/d $jangkawaktu2)" ;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Angsuran';
				echo '</td>';
				echo '<td>';
				echo number_format($angsuran);
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Terakhir Bayar';
				echo '</td>';
				echo '<td>';
				echo $collectnom . " ($collectdate)";
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td colspan="2">';
				echo '<input type="button" id="buttonbayar'.$seq.'" name="buttonbayar'.$seq.'" style="width:200px;" value="Bayar" class="blue" onclick="bayar(\''.$seq.'\', \''.$angsuran.'\')"/>';
				echo '</td>';
			echo '</tr>';
			echo '</table>';
			echo '</br>';
			echo '<B>HISTORY PEMBAYARAN</B>';
			echo '<table width="700px" align="center" style="" class="input" border="1px" cellspacing="0px">';
			echo '<tr>';
				echo '<td>';
				echo 'TANGGAL';
				echo '</td>';
				echo '<td>';
				echo 'NOMINAL';
				echo '</td>';
			echo '</tr>';
      
      $urut = 0;
			$tsql2 = "SELECT nominal, cast(datepayment as varchar) from tbl_collect_payment where custnomid = '$custnomid' order by datepayment desc";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);			

			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				{
					$urut++;
			echo '<tr>';
				echo '<td>';
				echo 'Pembayaran ke-' . $urut . " pada " . $row2[1];
				echo '</td>';
				echo '<td>';
				echo number_format($row2[0]);
				echo '</td>';
			echo '</tr>';
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );

			echo '</table>';
			echo '</br>';
			echo '</br>';
		}
	}
	
	
}
else if($nan == "BAYAR")
{
	$custnomid = $_POST['custnomid'];
	$seq = $_POST['seq'];
	$angsuran = $_POST['angsuran'];
	
	$tsql = "INSERT INTO tbl_collect_payment (custnomid, seq, nominal, datepayment, statussms) VALUES ('$custnomid', '$seq', $angsuran, GETDATE(), 'N')";
	$b = sqlsrv_query($conn, $tsql);
	if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	
	$tsql = "SELECT * from tbl_customerfacility2 b where custnomid = '$custnomid' AND b.custfacseq NOT IN (SELECT c.seq from tbl_collect_payment c where c.custnomid = b.custnomid)";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	$rowCount = sqlsrv_num_rows($sqlConn);
	
	if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$seq = $row['custfacseq'];
			$type = $row['custcredittype'];
			$need = $row['custcreditneed'];
			$angsuran = $row['mkknewangsuran'];
			$nominal = $row['custcreditplafond'];
			$sukubunga = $row['sukubungayangdiberikan'];
			$periode = $row['custcreditlong'];
			
			$need = $row['custcreditneed'];
			$tsql2 = "SELECT * from tbl_creditneed where credit_need_code = '$need'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);
			
			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
				{
					$need = $row2['credit_need_name'];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );
			
			$type = $row['custcredittype'];
			$tsql2 = "SELECT * from tbl_kodeproduk where produk_loan_type = '$type'";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn2 = sqlsrv_query($conn, $tsql2, $params, $cursorType);
			$rowCount = sqlsrv_num_rows($sqlConn2);
			
			if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			
			if(sqlsrv_has_rows($sqlConn2))
			{
				while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
				{
					$type = $row2['produk_type_description'];
				}
			}
			sqlsrv_free_stmt( $sqlConn2 );
			
			echo '<table width="700px" align="center" style="" class="input" border="1px" cellspacing="0px">';
			echo '<tr>';
				echo '<td>';
				echo 'Custnomid';
				echo '</td>';
				echo '<td>';
				echo $custnomid;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Jenis';
				echo '</td>';
				echo '<td>';
				echo $need;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Tipe';
				echo '</td>';
				echo '<td>';
				echo $type;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Angsuran';
				echo '</td>';
				echo '<td>';
				echo $angsuran;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Plafond';
				echo '</td>';
				echo '<td>';
				echo $nominal;
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Suku Bunga';
				echo '</td>';
				echo '<td>';
				echo $sukubunga."%";
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td>';
				echo 'Periode';
				echo '</td>';
				echo '<td>';
				echo $periode." Bulan";
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<td colspan="2">';
				echo '<input type="button" id="buttonbayar'.$seq.'" name="buttonbayar'.$seq.'" style="width:200px;" value="Bayar" class="blue" onclick="bayar(\''.$seq.'\')"/>';
				echo '</td>';
			echo '</tr>';
			echo '</table>';
			echo '</br>';
			echo '</br>';
		}
	}
}
?>