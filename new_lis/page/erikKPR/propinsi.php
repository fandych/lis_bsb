<?php
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");

	if (isset($_GET['term'])){

	    $return_arr = array();

		$strsql = "SELECT _name FROM param_propinsi WHERE _name LIKE '%".$_GET['term']."%'";
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				$return_arr[] = $rows['_name'];
			}
		}

		echo json_encode($return_arr);
	}
?>