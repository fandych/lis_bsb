<?
	ini_set("display_errors", 0);
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");
	
	$now= new DateTime;
	date_default_timezone_set('Asia/Jakarta');
	$date= date('Y-m-d',time());

	$custcurcode='IDR';
	$strsql="select 'custcurcode'=case when isnull(custcurcode,0)='0' then 'IDR' else custcurcode end 
	from Tbl_CustomerMasterPerson2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcurcode=$rows['custcurcode'];
		}
	}
	$namadebit="&nbsp;";
	$branch_name="&nbsp;";
	$custapldate="&nbsp;";
	$strsql="select b.branch_name,a.custapldate,
			'namadebit'= case when custsex='0' then custbusname else custfullname end
			from Tbl_CustomerMasterPerson2 a
			join Tbl_Branch b on a.custbranchcode = b.branch_code
			where a.custnomid='$custnomid'
			";
			//echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$branch_name=$rows['branch_name'];
			$namadebit=$rows['namadebit'];
			$custapldate=$rows['custapldate'];
		}
	}

	require ("perhitungan.php");

?>
	
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" >
		function save()
		{
			document.getElementById("frm").action = "do_necara.php";
			submitform = window.confirm("<?=$confmsg;?>");
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
		</script>
	</head>
	<body>
		<table align="center" style="width:1200px;" border="0" >
			<tr>
				<td>
					<table class="tbl100" style="border:1px solid black;background-color:#FFC;">
						<tr>
							<td style="font-size:12pt; text-align:center; font-weight:bold;">LABA RUGI DAN NERACA</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>
								<table class="tbl100">
									<tr>
										<td style="width:300px;">No. Applikasi</td>
										<td>:</td>
										<td><?echo $custnomid?></td>
									</tr>
									<tr>
										<td>Cabang</td>
										<td>:</td>
										<td><?echo $branch_name?></td>
									</tr>
									<tr>
										<td>Nama Calon Debitur</td>
										<td>:</td>
										<td><?echo $namadebit?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
					<table class="tbl100" style="border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;background-color:#FFC;">
						<tr>
							<td>&nbsp;</td>
							<td valign="top" style="width:48%">
								<table class="tbl100" border="0">
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
											<div style="border:1px solid  black;background-color:gray;" >
												<div style="font-size:10pt; text-align:center; font-weight:bold;">LABA RUGI PROFORMA</div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $namadebit ?></div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $date?></div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div style="border:1px solid  black;">
												<div>&nbsp;</div>
												<table class="tbl100" border="0">
													<tr>
														<td style="width:60%">Omset Usaha/Sales</td>
														<td style="width:10%">IDR</td>
														<td align="right"><?echo number_format($proforma_omset)?></td>
													</tr>
													<tr>
														<td>(Harga Pokok Penjualan)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_hpp)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Margin Usaha</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_margin)?></td>
													</tr>
													<tr>
														<td>(Biaya Operasional Usaha)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_biayaoperasional)?></td>
													</tr>
													<tr>
														<td>(Angsuran pinjaman yang dimiliki saat ini)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($Neraca_hutangjkpendek)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Laba Usaha Sebelum Pajak </td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_labasebelumpajak)?></td>
													</tr>
													<tr>
														<td>(Pajak 10%)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_pajak)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Laba Bersih Usaha</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_lababersih)?></td>
													</tr>
													<tr>
														<td>Biaya Rumah Tangga</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_rumahtangga)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Pendapatan Bersih</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proforma_pendapatanbersih)?></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
							<td valign="top" style="width:48%">
								<table class="tbl100" border="0">
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
											<div style="border:1px solid  black;background-color:gray;" >
												<div style="font-size:10pt; text-align:center; font-weight:bold;">NERACA PROFORMA</div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $namadebit ?></div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $date?></div>
											</div>
										</td>
									</tr>
									<tr>
										<td valign="top">
											<div style="border:1px solid  black;">
												<div>&nbsp;</div>
												<table class="tbl100" border="0">
													<tr>
														<td style="width:26%">Kas dan Bank</td>
														<td style="width:5%">IDR</td>
														<td style="width:18%; text-align:right;"><?echo number_format($Neraca_kasdanbank)?></td>
														<td>&nbsp;</td>
														<td style="width:26%">Hutang Bank Jk. Pendek</td>
														<td style="width:5%">IDR</td>
														<td style="width:18%; text-align:right;"><?echo number_format($Neraca_hutangjkpendek)?></td>
													</tr>													
													<tr>
														<td>Piutang Usaha</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_piutang)?></td>
														<td>&nbsp;</td>
														<td>Hutang Usaha</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_hutang)?></td>
													</tr>
													<tr>
														<td>Persediaan</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_persediaan)?></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
													</tr>
													<tr style="background-color:gray;">
														<td>Total Aktiva Lancar</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalaktivalancar)?></td>
														<td>&nbsp;</td>
														<td>Total Hutang Lancar</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalhutanglancar)?></td>														
													</tr>
													<tr>
														<td>Total Aktiva Tetap</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalaktivatetap)?></td>
														<td>&nbsp;</td>
														<td>Hutang Jk. Panjang</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_hutangjkpanjang)?></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
														<td>&nbsp;</td>
														<td>Total Hutang</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalhutang)?></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
														<td>&nbsp;</td>
														<td>Modal (Laba Ditahan)</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_labaditahan)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Total Aktiva</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalaktiva)?></td>
														<td>&nbsp;</td>
														<td>Total Hutang dan Modal</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($Neraca_totalhutangdanmodal)?></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
					<table class="tbl100" style="border-left:1px solid black;border-bottom:1px solid black;border-right:1px solid black;background-color:#FFC;">
						<tr>
							<td>&nbsp;</td>
							<td valign="top" style="width:48%">
								<table class="tbl100" border="0">
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
											<div style="border:1px solid  black;background-color:gray;" >
												<div style="font-size:10pt; text-align:center; font-weight:bold;">LABA RUGI PROFORMA PROYEKSI</div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $namadebit ?></div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $date?></div>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div style="border:1px solid  black;">
												<div>&nbsp;</div>
												<table class="tbl100" border="0">
													<tr>
														<td style="width:60%">Omset Usaha/Sales</td>
														<td style="width:10%">IDR</td>
														<td align="right"><?echo number_format($proyeksi_proforma_omset)?></td>
													</tr>
													<tr>
														<td>(Harga Pokok Penjualan)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_hpp)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Margin Usaha</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_marginusaha)?></td>
													</tr>
													<tr>
														<td>(Biaya Operasional Usaha)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_biayaoperasional)?></td>
													</tr>
													<tr>
														<td>(Angsuran pinjaman yang dimiliki saat ini)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_angsuran)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Laba Usaha Sebelum Pajak </td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_labasebelumpajak)?></td>
													</tr>
													<tr>
														<td>(Pajak 10%)</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_pajak)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Laba Bersih Usaha</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_lababersih)?></td>
													</tr>
													<tr>
														<td>Biaya Rumah Tangga</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_rumahtangga)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Pendapatan Bersih</td>
														<td><?echo $custcurcode?></td>
														<td align="right"><?echo number_format($proyeksi_proforma_pendapatanbersih)?></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
							<td valign="top" style="width:48%">
								<table class="tbl100" border="0">
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
											<div style="border:1px solid  black;background-color:gray;" >
												<div style="font-size:10pt; text-align:center; font-weight:bold;">NERACA PROFORMA PROYEKSI</div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $namadebit ?></div>
												<div style="font-size:10pt; text-align:center; font-weight:bold;"><?echo $date?></div>
											</div>
										</td>
									</tr>
									<tr>
										<td valign="top">
											<div style="border:1px solid  black;">
												<div>&nbsp;</div>
												<table class="tbl100" border="0">
													<tr>
														<td style="width:26%">Kas dan Bank</td>
														<td style="width:5%">IDR</td>
														<td style="width:18%; text-align:right;"><?echo number_format($proyeksi_neraca_kasdanbank)?></td>
														<td>&nbsp;</td>
														<td style="width:26%">Hutang Bank Jk. Pendek</td>
														<td style="width:5%">IDR</td>
														<td style="width:18%; text-align:right;"><?echo number_format($proyeksi_neraca_hutangjkpendek)?></td>
													</tr>													
													<tr>
														<td>Piutang Usaha</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_piutang)?></td>
														<td>&nbsp;</td>
														<td>Hutang Usaha</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_hutang)?></td>
													</tr>
													<tr>
														<td>Persediaan</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_persediaan)?></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
													</tr>
													<tr style="background-color:gray;">
														<td>Total Aktiva Lancar</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalaktivalancar)?></td>
														<td>&nbsp;</td>
														<td>Total Hutang Lancar</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalhutanglancar)?></td>														
													</tr>
													<tr>
														<td>Total Aktiva Tetap</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalaktivatetap)?></td>
														<td>&nbsp;</td>
														<td>Hutang Jk. Panjang</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_hutangjkpanjang)?></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
														<td>&nbsp;</td>
														<td>Total Hutang</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalhutang)?></td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td style="text-align:right;">&nbsp;</td>
														<td>&nbsp;</td>
														<td>Modal (Laba Ditahan)</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_labaditahan)?></td>
													</tr>
													<tr style="background-color:gray;">
														<td>Total Aktiva</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalaktiva)?></td>
														<td>&nbsp;</td>
														<td>Total Hutang dan Modal</td>
														<td><?echo $custcurcode?></td>
														<td style="text-align:right;"><?echo number_format($proyeksi_neraca_totalhutangdanmodal)?></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<form id="frm" name="frm" action="do_necara.php" method="post">
		<? 
		if($userid != "")
		{
		require ("../../requirepage/hiddenfield.php");
		
		?>
		<div style="text-align:center;">
		<input type="button" name="btnsubmit" id="btnsubmit" value="Save" class="buttonsaveflow" onclick="save()"/>
		</div>
		<?
		}
		?>
		</form>
		<?require("../../requirepage/btnprint.php");?>
	</body>
</html>