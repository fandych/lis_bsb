<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");

//ini_set("display_error",0);

include ("new_drawdown_response.php");

//DECLARE VARIABLES
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
$userwfid=$_REQUEST['userwfid'];
//$userpermission=$_REQUEST['userpermission'];
//$buttonaction=$_REQUEST['buttonaction'];
//$custnomid=$_REQUEST['custnomid'];


/*
echo "userid ".$userid;
echo "<br>";
echo "userpwd ".$userpwd;
echo "<br>";
echo "userbranch ".$userbranch;
echo "<br>";
echo "userregion ".$userregion;
echo "<br>";
echo "userwfid ".$userwfid;
echo "<br>";
echo "userpermission ".$userpermission;
echo "<br>";
echo "buttonaction ".$buttonaction;
echo "<br>";
echo "custnomid ".$custnomid;
*/

//SECURITY PROFILE AND WORKFLOW SETTINGS

if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	echo "<br>";
	echo "general security";
	//header("location:restricted.php");
}

// SECURITY
   $tsql = "SELECT COUNT(*) FROM Tbl_SE_User
   					WHERE user_id='$userid'
   					AND user_pwd='$userpwd'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $thecount = $row[0];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   if ($thecount == 0)
   {
	   echo "<br>";
	   echo "security user password";
   	   //header("location:restricted.php");
   }

   $tsql = "SELECT COUNT(*) FROM Tbl_SE_UserProgram
   					WHERE user_id='$userid'
   					AND program_code='$userwfid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $thecount = $row[0];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   if ($thecount == 0)
   {
	   echo "<br>";
	   echo "security user program";
   	   //header("location:restricted.php");
   }

   $tsql = "SELECT * FROM Tbl_SE_UserProgram
   					WHERE user_id='$userid'
   					AND program_code='$userwfid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   $userpinp = "";
   $userpchk = "";
   $userpapr = "";
   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
		$userpinp = substr($row[2],1-1,1);
		$userpchk = substr($row[2],2-1,1);
		$userpapr = substr($row[2],3-1,1);
      }
   }
   sqlsrv_free_stmt( $sqlConn );

// END SECURITY
// PROFILE USER ID (AO / TL / PINCA)
   $tsql = "SELECT user_ao_code, user_level_code, user_child FROM Tbl_SE_User
   					WHERE user_id='$userid'";
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $profileaocode = $row[0];
   		   $profilelevelcode = $row[1];
   		   $profileuserchild = $row[2];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   
//END PROFILE

//SELECT From Tbl_Workflow & PrevFlow
    $tsql = "select * from Tbl_Workflow where wf_id='$userwfid'";
   	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   	$params = array(&$_POST['query']);
   	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if($sqlConn === false)
	{
		die(FormatErrors(sqlsrv_errors()));
	}
	
	if(sqlsrv_has_rows($sqlConn))
	{
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
   		   $wfname = $row[1];
   		   $wftime = $row[3];
   		   $wfscore = $row[4];
   		   $wfaction = $row[5];
   		   $wfflag = $row[7];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
      
//END GET USER

//GET PARAM BRANCH

$tsql = "select * from Tbl_branch";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

if($sqlConn === false)
{
	die(FormatErrors(sqlsrv_errors()));
}

if(sqlsrv_has_rows($sqlConn))
{
  $rowCount = sqlsrv_num_rows($sqlConn);
  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
  {
	   $param_branch[$row[0]]=$row[1];
  }
}
sqlsrv_free_stmt( $sqlConn );

//print_r($param_branch);
//END GET PARAM BRANCH

//FUNCTION LIST DIR
function getDirectoryList ($directory) 
{
$results = array();// create an array to hold directory list
$handler = opendir($directory);// create a handler for the directory
while ($file = readdir($handler)) // open directory and walk through the filenames
{
	if ($file != "." && $file != "..") // if file isn't this directory or its parent, add it to the results
	{
	$results[] = $file;
	}
}
closedir($handler);// tidy up: close the handler
return $results;// done!
}
//END


//GET CUSTNOMID LIST WHICH READY FOR RESPONSE
$directory = "c:/Temp/NicResponse/";

$dir_list = getDirectoryList ($directory);

//print_r($dir_list);

for($i = 0 ; $i < count($dir_list); $i ++)
{
	$fh = fopen($directory.$dir_list[$i],"r"); 	
	$data_read = fread($fh,filesize($directory.$dir_list[$i]));
	$ShowString = $data_read;
	//echo $ShowString;
	$arrTempchar13=explode("\n",$ShowString);
	//echo "<br>";
	//echo strlen($ShowString);
	//echo print_r($arrTempchar13);
	
	for($ar=0;$ar<count($arrTempchar13);$ar++)
	{
		$response=substr($arrTempchar13[$ar],48,1);
		$customer_id=substr($arrTempchar13[$ar],0,13);
		//echo $customer_id;
		//echo "-";
		//echo $response;
		//echo "<br>";			
		
		
		
		$tsql_response = "UPDATE Tbl_Drawdown Set Status_Response = '$response', Date_Response = getdate() where custnomid = '$customer_id'";
		$params_response = array(&$_POST['query']);
		$stmt_response = sqlsrv_prepare( $conn, $tsql_response, $params_response);
		if( $stmt_response )
		{
		} 
		else
		{
			echo "Error in preparing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		
		if( sqlsrv_execute( $stmt_response))
		{
		}
		else
		{
			echo "Error in executing statement.\n";
			die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt_response);
	}
fclose($fh);
}

//MOVE FILE IN NICRESPONSE to NICRESPONSE BACKUP
$dest_directory = "c:/Temp/NicBackup/";

$dir_move = getDirectoryList ($directory);
//print_r($dir_move);
for($d=0;$d<count($dir_move);$d++)
{
	//echo $dir_move[$d];
	copy($directory.$dir_move[$d],$dest_directory.$dir_move[$d]);
	unlink($directory.$dir_move[$d]);
}
//END
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DRAWDOWNRESPONSE</title>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<link href="../../css/d.css" rel="stylesheet" type="text/css" />
<script language="javascript">
function reDrawdown(paramCustnomid)
{
	//alert(paramCustnomid);
	document.formredd.in_redd.value=paramCustnomid;
	sure = window.confirm("Are you sure want to Re-Drawdown "+ paramCustnomid + " ?");	
	if(sure == true)
	{
		document.formredd.submit();	
	}
	else
	{
	}
}
</script>
</head>

<body>
<table width="800" border="0" class="preview" align="center">
<tr><td>
<form name="formredd" action="do_drawdownresponse.php" method="post">
<div align="center">
<table width="800" border="0">
  <tr>
    <td>&nbsp;</td>
    <td><div align="center" class="judul">RESPONSE LIST</div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
      <table width="800" border="1">
        <tr>
          <th>ID</th>
          <th>NAMA CALON DEBITUR</th>
          <th>CABANG</th>
          <th>DETAIL</th>
          <th>STATUS</th>
          <th>DATE</th>
          <th>RE DRAW DOWN</th>
        </tr>
        <?
		$tsql = "select custnomid, status_response, cast(date_response as varchar) from tbl_Drawdown";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
		
		if ( $sqlConn === false)
			die( FormatErrors( sqlsrv_errors() ) );
		
		if(sqlsrv_has_rows($sqlConn))
		{
			$rowCount = sqlsrv_num_rows($sqlConn);
			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
			{
				$tsqlselect = "select * from tbl_customermasterperson2 where custnomid = '$row[0]'";
				$cursorTypeselect = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
				$paramsselect = array(&$_POST['query']);
				$sqlConnselect = sqlsrv_query($conn, $tsqlselect, $paramsselect, $cursorTypeselect);
				
				if($sqlConnselect === false)
				{
					die(FormatErrors(sqlsrv_errors()));
				}
				
				if(sqlsrv_has_rows($sqlConnselect))
				{
					$rowCounselectt = sqlsrv_num_rows($sqlConnselect);
					while( $rowselect = sqlsrv_fetch_array( $sqlConnselect, SQLSRV_FETCH_ASSOC))
					{
						//echo $row['custnomid'];
						//echo "&nbsp;";
						//echo $response_list[$i];
						$valid_custnomid = $rowselect['custnomid'];
						$param_NamaCustomer[$rowselect['custnomid']]=$rowselect['custfullname'];
						$param_CabangCustomer[$rowselect['custnomid']]=$rowselect['custbranchcode'];
					}
				}
				
					?><tr><?
				//echo $row[0];
				//echo "&nbsp;";
				//echo $row[1];
				//echo "&nbsp;";
				//echo $row[2];
				//echo "&nbsp;";
				?>                
                <td>&nbsp;<? echo $row[0];?></td>
                <td>&nbsp;<? echo $param_NamaCustomer[$row[0]];?></td>
                <td>&nbsp;<? echo $param_CabangCustomer[$row[0]];?> - <? echo $param_branch[$param_CabangCustomer[$row[0]]];?></td>
                <td><a href="../Preview/PreviewAll/previewall_n.php?userid=&userpwd=<? echo $userpwd; ?>&userbranch=<? echo $userbranch; ?>&userregion=<? echo $userregion; ?>&custnomid=<? echo $row[0]; ?>&userpermission=I&buttonaction=ICA&userwfid=ALL" target="_blank" >Click Here</a></td>
                <td>&nbsp;
                <?
				if($row[1] == "1")
				{
					//CAIR
					echo "Cair";
				}
				else if($row[1] == "2")
				{
					//PENDING
					echo "Pending";
				}
				else if($row[1] == "0")
				{
					//TIDAK CAIR
					echo "Tidak Cair";
				}
				?>
                </td>
                <td>&nbsp;<? echo $row[2];?></td>   
                <td>&nbsp;
                <?
				if($row[1] == "1")
				{
					//CAIR
					echo "Tidak Perlu";
				}
				else if($row[1] == "2")
				{
					//PENDING
				?>
                <input type="button" name="go" id="go" value="Go" onclick="reDrawdown('<? echo $row[0];?>')"/>
                <?
				}
				else if($row[1] == "0")
				{
					//TIDAK CAIR
				?>
                <input type="button" name="go" id="go" value="Go" onclick="reDrawdown('<? echo $row[0];?>')"/>
                <?
				}
				?>
                </td> 
                </tr>             
                <?


				
			}
		}
		else
		{
			?><tr><td colspan="7"><center>Tidak Ada Data</center></td></tr><?
		}
		sqlsrv_free_stmt( $sqlConn );
		?>
      </table>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">
      <input type="hidden" name="userid"  value='<? echo $userid; ?>'/>
      <input type="hidden" name="userpwd" value='<? echo $userpwd; ?>'/>
      <input type="hidden" name="userbranch" value='<? echo $userbranch; ?>'/>
      <input type="hidden" name="userregion" value='<? echo $userregion; ?>'/>
      <input type="hidden" name="userpermission" value='<? echo $userpermission; ?>' />
      <input type="hidden" name="buttonaction" value='<? echo $buttonaction; ?>'/>
      <input type="hidden" name="userwfid"  value='<? echo $userwfid; ?>'/>
      <input type="hidden" name="in_redd" value="">
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</form>
</td></tr>
</table>
</body>
</html>