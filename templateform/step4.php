<?
session_start();
ob_start();
require_once("sqlsrv.php");
require_once("preference.php");
//check from step2 or step 4 

ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");



$templatecontent = $_SESSION['templatecontent'];
$templatename = $_SESSION['templatename'];



?>

<!DOCTYPE html>
<html>
<head>
<title>Create Tempalte</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />



<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="js/jquery-1.12.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>

<script>
	function onsubmitformnext()
	{
		$("#frm").attr('action', 'tmp.php');
		$("#frm").submit();
	}
	function onsubmitformprev()
	{
		$("#frm").attr('action', 'step3.php');
		$("#frm").submit();
	}
</script>
<style>
	td{padding:5px;}
</style>
</head>
<body>
<form id="frm" name="frm" method="post" >
	<div class="container">
	<div style="text-align:left;font-size:30px;">Template Form <? echo $_SESSION['template']. "  " .$_SESSION['templatename'] ?></div>
	<hr>
	<?php
		$namevar = "";
		$setget2 = "";
		$value2 = "";
		$type2 = "";
		$param2 = "";
		$testarray=array();
		
		$strsql="select * from tmp_templatedetail where _template_code='".$_SESSION['template']."'";
		$getfullcontent="";
		$rss = $condb->_RQ($strsql);
		if(!is_array($rss)){echo $rss;}else{
		for($i=0;$i<count($rss);$i++) {
				//print_r($value);
				$_namevar=$rss[$i]['_namevar'];
				
				array_push($testarray,$_namevar);
				$_setget=$rss[$i]['_setget'];
				$_value=$rss[$i]['_value'];
				$_type=$rss[$i]['_type'];
				$_param=$rss[$i]['_param'];
				
				$setget2 .= $_setget.",";
				$value2 .= $_value.",";
				$type2 .= $_type.",";
				$param2 .= $_param.",";
				$namevar.= $_namevar.",";
				
				$require_value="";
				if($_value=="Mandatory")
				{
					$require_value="required";	
				}
				
				$value_type=strtolower($_type);
				
				//echo $_value."<br/>";
				$value="<span style=\"color:red;\">data tidak ada</span>";
				$value=$_value;
				if($_setget=="Browse to DB")
				{
					$epld=explode('.',$_value);
					$tablename=$epld[0];
					$fieldname=$epld[1];
					
					$datetime = false;
					$strsql="select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME='".$fieldname."'";
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						if($result[0]['DATA_TYPE']== "date" || $result[0]['DATA_TYPE']== "datetime")
						{
							$datetime = true;
						}
					}
					
					$COLUMN_NAME="_name";
					$strsql="select COLUMN_NAME,DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME like '%custnomid%'";
					//echo $strsql;
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						$COLUMN_NAME = $result[0]['COLUMN_NAME'];
					}
					
					$strsql="select ".$fieldname." from ".$tablename." where ".$COLUMN_NAME."='".$_SESSION['template']."'";
					$strsql="select ".$fieldname." from ".$tablename." where ".$COLUMN_NAME."=''";
					
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						for($zz=0;$zz<count($result);$zz++)
						{
							$value=$result[$zz][$fieldname];
							if($datetime==true)
							{
								$value=$result[$zz][$fieldname]->format('Y-m-d');
							}
							else if($_type=="Currency")
							{
								$value=number_format($result[$zz][$fieldname]);
							}
						}
					}	
				}
				//$arryset = array("Textbox","Textarea");
				else
				{
					$value = $_POST[$_namevar];
				}
			
				$lastsentence = multiexplode($testarray,$templatecontent);
				//print_r($lastsentence);
				$getfullcontent.=$lastsentence[$i].$value;
				if($i==(count($rss)-1))
				{
					$getfullcontent.=$lastsentence[count($rss)];
				}
				//echo "<hr>";
				
			
			}
		}
		?>
		<div id="test">
		<?
		echo str_replace("|||",'"',$getfullcontent);
		?>
		</div>
		
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input tv="test"  onclick="onsubmitformnext()" id="btnnext"  style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
			<div>
				<input  onclick="onsubmitformprev()" style="float:left;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="<< Prev">
			</div>
		</div>
	</div>
	<input type="hidden" name="setget2" id="setget2" value="<?=substr($setget2,0,-1)?>">
	<input type="hidden" name="param2" id="param2" value="<?=substr($param2,0,-1)?>">
	<input type="hidden" name="value2" id="value2" value="<?=substr($value2,0,-1)?>">
	<input type="hidden" name="type2" id="type2" value="<?=substr($type2,0,-1)?>">
	<input type="hidden" name="namevar" id="namevar" value="<?=substr($namevar,0,-1)?>">
</form>

</body>
</html>