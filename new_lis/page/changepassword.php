<?
include("../requirepage/session.php");
require_once ("../lib/open_con.php");
require_once ("../lib/formatError.php");
require ("../requirepage/parameter.php");

?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="../js/jquery-latest.min.js" type="text/javascript"></script>
		<script src="../js/menu_ce.js" type="text/javascript"></script>
		<link href="../css/menu_ce.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery-1.7.2.min.js" ></script>
		<link href="../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		
		function changepassword()
		{
			var op = $("#oldpass").val();
			var np = $("#newpass").val();
			var cp = $("#confirmpass").val();
			
			if(op == "")
			{
				alert("Old Password can not be empty!!");
				$("#oldpass").focus();
				return false;
			}
			if(np == "")
			{
				alert("New Password can not be empty!!");
				$("#newpass").focus();
				return false;
			}
			if(cp == "")
			{
				alert("New Password can not be empty!!");
				$("#confirmpass").focus();
				return false;
			}
			if(np != cp)
			{
				alert("The New Password is different from the Confirm Password!!");
				$("#newpass").focus();
				return false;
			}else{
				if(op == np)
				{
					alert("The new password must not be the same as the old password!!");
					$("#newpass").focus();
					return false;
				}else{
					if(np.length < 7)
					{
						alert("New Password must contain at least seven characters");
						$("#newpass").focus();
						return false;
					}else{
						re = /[0-9]/;
						if(!re.test(np))
						{
							alert("New Password must contain at least one number (0-9)");
							$("#newpass").focus();
							return false;
						}
						re = /[a-z]/;
						if(!re.test(np))
						{
							alert("New Password must contain at least one alphabet (a-z)");
							$("#newpass").focus();
							return false;
						}
						re = /[A-Z]/;
						if(!re.test(np))
						{
							alert("New Password must contain at least one uppercase (A-Z)");
							$("#newpass").focus();
							return false;
						}
						var submitform = window.confirm("SAVE ?")
						if (submitform == true)
						{
								document.formchangepass.action = './e_changepassword.php';
								document.formchangepass.submit();
								return true;
						}
					}
				}
			}
		}
		
		</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="position:fixed;">
				<img src="../images/header_lis2.jpg" style="width:100%;"></img>
			</div>
			<div style="border:0px solid black;width:23%;height:545px;margin-top:7%;float:left;position:fixed;">
	<!--Open Menu-->
				<div id="cssmenu">
					<ul>
<?
	$userwfid=$_REQUEST['userwfid'];
	if($userwfid==""){
		$userwfid=$_POST['userwfid'];
	}
	
	$tsql2 = "select program_group, program_code, program_desc from Tbl_SE_Program
			where program_code like '%$userwfid%'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$programgcd = $row2[0];
			$programcd = $row2[1];
			$programname = $row2[2];
			//echo $programname;
		}
	}
	sqlsrv_free_stmt( $sqlConn2 );
	
	$d = date("d");
	$m = date("m");
	$y = date("y");
	$h = date("h");
	$i = date("i");
	$s = date("s");
	$randomtime = $h.$i.$s;
	
	$tsql2 = "select * from Tbl_SE_User U LEFT JOIN Tbl_Branch B ON U.user_branch_code = B.branch_code where user_id='$userid'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$userbranch = $row2[7];
			$userpwd = $row2[9];
			$userregion = $row2[21];
		}
	}
			
	$tsql = "select Tbl_SE_GrpProgram.grp_code, Tbl_SE_GrpProgram.grp_name from Tbl_SE_GrpProgram, Tbl_SE_Program, Tbl_SE_UserProgram
				where Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
				AND Tbl_SE_Program.program_group=Tbl_SE_GrpProgram.grp_code
				AND Tbl_SE_UserProgram.user_id='$userid'
				GROUP BY Tbl_SE_GrpProgram.grp_code,Tbl_SE_GrpProgram.grp_name, Tbl_SE_GrpProgram.grp_urut
				ORDER BY Tbl_SE_GrpProgram.grp_urut,Tbl_SE_GrpProgram.grp_name";
	  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	  $params = array(&$_POST['query']);

	  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	  if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	  if(sqlsrv_has_rows($sqlConn))
	  {
		 $rowCount = sqlsrv_num_rows($sqlConn);
		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		 {
			$tsql2 = "SELECT Tbl_SE_Program.program_code,
						Tbl_SE_Program.program_name,Tbl_SE_Program.program_desc
						FROM Tbl_SE_Program, Tbl_SE_UserProgram
						WHERE Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
						AND Tbl_SE_UserProgram.user_id='$userid'
						AND Tbl_SE_Program.program_group='$row[0]'
						ORDER BY program_urut";
			  $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			  $params2 = array(&$_POST['query']);

			  $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
			  if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			  if(sqlsrv_has_rows($sqlConn2))
			  {
				 $rowCount2 = sqlsrv_num_rows($sqlConn2);
				 
				 if($row['0']=="$programgcd"){
					 echo "<li class='selected'>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }else{
					 echo "<li>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }
				 
				 if($rowCount2 > '15')
				 {
					echo "<ul style='overflow-x:hidden;overflow-y:scroll;white-space:nowrap;height:150px;'>";
				 }else{
					echo "<ul>";
				 }
				 
				 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				 {
					if($row2[2]=="$programname" && $row2[0]=="$programcd")
					{
						echo "<li class='sub_selected'><a href='../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}else{
						echo "<li><a href='../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}
				 }
			  }
			  sqlsrv_free_stmt( $sqlConn2 );
				 echo "</ul>";
				 echo "</li>";
		 }
	  }
	  sqlsrv_free_stmt( $sqlConn );
					
					if($userwfid=="changepassword")
					{
?>
						<li class="selected">
<?
					}else{
?>
						<li>
<?
					}
?>
							<a href="#"><span>SETTING</span></a>
							<ul>
<?
							if($userwfid=="changepassword")
							{
?>
								<li class="sub_selected"><a href='./changepassword.php?userwfid=changepassword'>Change Password</a></li>
<?
							}else{
?>
								<li><a href='./changepassword.php?userwfid=changepassword'>Change Password</a></li>
<?
							}
?>
								<li><a href='./logout.php'>Log Out</a></li>
							</ul>
						</li>
					</ul>
				</div>
<!--Close Menu-->
			<img src="../images/gimmick_logo.png" style="position:absolute;bottom:0;width:60%;margin-left:20%;margin-right:20%;"></img>
		</div>
		<form id="formchangepass" name="formchangepass" method="post" enctype="multipart/form-data">
		<div align=center style="float:right;width:75%;margin-top:7%;margin-right:2%;">
		<table align="center">
			<tr>
				<td colspan="2" align="center" style="font-weight:bold;">Change Password</td>
			</tr>
			<tr>
				<td align="right">Old Password :</td>
				<td align="left"><input type="password" id="oldpass" name="oldpass" style="width:200px;"></td>
			</tr>
			<tr>
				<td align="right">New Password :</td>
				<td align="left"><input type="password" id="newpass" name="newpass" style="width:200px;"></td>
			</tr>
			<tr>
				<td align="right">Confirm Password :</td>
				<td align="left"><input type="password" id="confirmpass" name="confirmpass" style="width:200px;"></td>
			</tr>
			<tr>
				<td align="center">&nbsp;</td>
				<td align="left"><input class="button" type="button" name="btnchange" id="btnchange" value="Change Password" onclick="changepassword();" style="width:150px;"></td>
			</tr>
			<tr>
				<td id="resid">
				</td>
			</tr>
		</table>
		</div>
		</form>
	</body>
</html>