<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");

function konversi($x){
   
  $x = abs($x);
  $angka = array ("","satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
   
  if($x < 12){
   $temp = " ".$angka[$x];
  }else if($x<20){
   $temp = konversi($x - 10)." belas";
  }else if ($x<100){
   $temp = konversi($x/10)." puluh". konversi($x%10);
  }else if($x<200){
   $temp = " seratus".konversi($x-100);
  }else if($x<1000){
   $temp = konversi($x/100)." ratus".konversi($x%100);   
  }else if($x<2000){
   $temp = " seribu".konversi($x-1000);
  }else if($x<1000000){
   $temp = konversi($x/1000)." ribu".konversi($x%1000);   
  }else if($x<1000000000){
   $temp = konversi($x/1000000)." juta".konversi($x%1000000);
  }else if($x<1000000000000){
   $temp = konversi($x/1000000000)." milyar".konversi($x%1000000000);
  }
   
  return $temp;
 }
   
 function tkoma($x){
  $str = stristr($x,".");
  $ex = explode('.',$x);
   
  if(($ex[1]/10) >= 1){
   $a = abs($ex[1]);
  }
  $string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan",   "sembilan","sepuluh", "sebelas");
  $temp = "";
  
  $a2 = $ex[1]/10;
  $pjg = strlen($str);
  $i =1;
     
   
  if($a>=1 && $a< 12){   
   $temp .= " ".$string[$a];
  }else if($a>12 && $a < 20){   
   $temp .= konversi($a - 10)." belas";
  }else if ($a>20 && $a<100){   
   $temp .= konversi($a / 10)." puluh". konversi($a % 10);
  }else{
   if($a2<1){
     
    while ($i<$pjg){     
     $char = substr($str,$i,1);     
     $i++;
     $temp .= " ".$string[$char];
    }
   }
  }  
  return $temp;
 }
  
 function Terbilang($x){
  if($x<0){
   $hasil = "minus ".trim(konversi(x));
  }else{
   $poin = trim(tkoma($x));
   $hasil = trim(konversi($x));
  }
   
if($poin){
   $hasil = $hasil." koma ".$poin;
  }else{
   $hasil = $hasil;
  }
  return $hasil;  
 }

$Format_Generate="";
$strsql="select * from Tbl_Processing";
$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursortype);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
$rowCount = sqlsrv_num_rows($sqlConn);
while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
	$Format_Generate=$row['proc_code'];
	}
}
sqlsrv_free_stmt( $sqlConn );

$denda_nilai_dkpa="";
$sql_denda="Select control_value from ms_control where control_code='DKPA'";
$cursortype_denda = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_denda = array(&$_POST['query']);
$sqlConn_denda = sqlsrv_query($conn, $sql_denda, $params_denda, $cursortype_denda);
if($conn==false)
{
	die(FormatErrors(sqlsrv_errors()));
}
if(sqlsrv_has_rows($sqlConn_denda))
{
	//$rowCount_denda = sqlsrv_num_rows($sqlConn_denda);
	while($row_denda = sqlsrv_fetch_array( $sqlConn_denda, SQLSRV_FETCH_ASSOC))
	{
		$denda_nilai_dkpa=$row_denda['control_value'];
	}
}
sqlsrv_free_stmt( $sqlConn_denda );


$denda_nilai_dpd="";
$sql_denda1="Select control_value from ms_control where control_code='dpd'";
$cursortype_denda1 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_denda1 = array(&$_POST['query']);
$sqlConn_denda1 = sqlsrv_query($conn, $sql_denda1, $params_denda1, $cursortype_denda1);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_denda1))
{
	//$rowCount_denda1 = sqlsrv_num_rows($sqlConn_denda);
	while($row_denda1 = sqlsrv_fetch_array( $sqlConn_denda1, SQLSRV_FETCH_ASSOC))
	{
	$denda_nilai_dpd=$row_denda1['control_value'];
	}
}
sqlsrv_free_stmt( $sqlConn_denda1 );



$region_name="";
$region_code="";
$ao_branch="";
$ao_name="";
$branch_city="";
$branch_address="";
$branch_name="";
$debiturname="";
$marname="";
$hp="";
$address="";
$telp="";
$menyetujui="";
$strsql="select region_code,region_name,c.user_name, 
c.user_branch_code, d.branch_city,d.branch_address,d.branch_name,
'debiturname'= case when custsex='0' then custbusname else custfullname end,
'marname'= case when isnull(custmarname,'')='' then '&nbsp;' else custmarname end,
'menyetujui'= case when custsex='0' then 'Menyetujui' else 
case when custmarcode='1' then 'Mengetahui dan Menyetujui :<br>Suami/Istri' end  end,
'telp'= case when custsex='0' then custbustelp else custtelp end,
'hp'= case when custsex='0' then custbushp else custhp end,
'address'= case when custsex='0' then custbusaddr else custaddrktp end
from Tbl_FSTART a 
join Tbl_Region b on a.txn_region_code = b.region_code 
join Tbl_SE_User c on c.user_id = a.txn_user_id 
join Tbl_Branch d on d. branch_code = a.txn_branch_code 
join Tbl_CustomerMasterPerson2 e on e.custnomid= a.txn_id
where a.txn_id='".$custnomid."'";
$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursortype);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$address=$row['address'];
		$telp=$row['telp'];
		$hp=$row['hp'];
		$menyetujui=$row['menyetujui'];
		$debiturname=$row['debiturname'];
		$marname=$row['marname'];
		$region_name=$row['region_name'];
		$ao_branch=$row['user_branch_code'];
		$ao_name=$row['user_name'];
		$region_code=$row['region_code'];
		$branch_name=$row['branch_name'];
		$branch_address=$row['branch_address'];
		$branch_city=$row['branch_city'];
	}
}
sqlsrv_free_stmt( $sqlConn );
$str=substr($custnomid,3,strlen($custnomid)-3);


$pknumber="";
$pkhari="";
$pktanggal="";
$pcabang="";
$pkbankwakila="";
$pkbankwakilb="";
$pkbanksk1="";
$pkbanktanggalsk1="";
$pkbanksk2="";
$pkbanktanggalsk2="";
$pkdebitur="";
$pkterbilangadministrasi="";
$pkketentuanlain="";
$pknocabang="";
$pkfaxcabang="";
$pkaltaddrdebitur="";
$pkalttelepondebitur="";
$pkfaxdebitur="";
$pkpengadilannegri="";
$pknamajabatan1="";
$pkjabatan1="";
$pknamajabatan2="";
$pkjabatan2="";
$pknametanggal="";
$pknamadebitur="";
$pkmmu="";
$pkseq="";
$pktglstatus="";
$strsql="select * from tbl_newpk where custnomid='".$custnomid."'";
$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursortype);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while($rows = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$pkseq=$rows['pkseq'];
		$pknumber=$rows['pknumber'];
		$pkhari=$rows['pkhari'];
		$pknametanggal=$rows['pknametanggal'];
		$pktanggal=$rows['pktanggal'];
		$pcabang=$rows['pcabang'];
		$pkbankwakila=$rows['pkbankwakila'];
		$pkbankwakilb=$rows['pkbankwakilb'];
		$pkbanksk1=$rows['pkbanksk1'];
		$pkbanktanggalsk1=$rows['pkbanktanggalsk1'];
		$pkbanksk2=$rows['pkbanksk2'];
		$pkbanktanggalsk2=$rows['pkbanktanggalsk2'];
		$pkdebitur=$rows['pkdebitur'];
		$pkterbilangadministrasi=$rows['pkterbilangadministrasi'];
		$pkketentuanlain=$rows['pkketentuanlain'];
		$pknocabang=$rows['pknocabang'];
		$pkfaxcabang=$rows['pkfaxcabang'];
		$pkaltaddrdebitur=$rows['pkaltaddrdebitur'];
		$pknamadebitur=$rows['pknamadebitur'];
		$pkalttelepondebitur=$rows['pkalttelepondebitur'];
		$pkfaxdebitur=$rows['pkfaxdebitur'];
		$pkpengadilannegri=$rows['pkpengadilannegri'];
		$pknamajabatan1=$rows['pknamajabatan1'];
		$pkjabatan1=$rows['pkjabatan1'];
		$pknamajabatan2=$rows['pknamajabatan2'];
		$pkjabatan2=$rows['pkjabatan2'];
		$pkmmu=$rows['pkmmu'];
		$pktglstatus=$rows['pktglstatus'];
	}
}
sqlsrv_free_stmt( $sqlConn );


if($pkaltaddrdebitur=="")
{
	$pkaltaddrdebitur=$address;
}



$hari1="";

if($pkhari=="1")
{
	$hari1='Senin';
}
else if($pkhari=="2")
{
	$hari1='Selasa';
}
else if($pkhari=="3")
{
	$hari1='Rabu';
}
else if($pkhari=="4")
{
	$hari1='Kamis';
}
else if($pkhari=="5")
{
	$hari1='Jumat';
}
else if($pkhari=="6")
{
	$hari1='Sabtu';
}
else if($pkhari=="7")
{
	$hari1='Minggu';
}

$namabank="";
$strsql="SELECT CONTROL_VALUE FROM MS_CONTROL WHERE CONTROL_CODE ='BANKNAME'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
    if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
    {
        $namabank=$rows['CONTROL_VALUE'];
    }
}


?>
<html>
	<head>
		<meta name="author" content="Qiu Kwok Lung">
		<title>Legal - Perjanjian Kredit</title>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<form id="frm" name="frm">
			<center>
				<table style="width:900px; border:0px solid black;" id="tblpreview">
					<tr>
						<td style="width:10%"></td>
						<td style="width:90%" valign="top">
							<table style="width:100%">
				
								<tr>
									<td style="text-align:center;">
										<div>&nbsp;</div>
										<div style="font-weight:bold">PERJANJIAN KREDIT<div>
										<div style="font-weight:bold">NOMOR PERJANJIAN KREDIT : Nomor : <?echo $pkseq?>/<? echo $Format_Generate?>/LEG/<? echo $region_code ?>/<? echo $ao_branch ?>/<? echo date('Y') ?>/<? echo $str; ?><div>
										<div>&nbsp;</div>
									</td>
								</tr>
								<tr>
									<td>
										<table border="0">
											<tr>
												<td colspan="2">
													Perjanjian Kredit ini (selanjutnya disebut Perjanjian) dibuat dan ditandatangani pada hari 
													<? echo $hari1 ?>
													, tanggal 
													<?echo $pknametanggal?>
													oleh dan antara :</div> 
												</td>
											</tr>
											<tr>
												<td valign="top" align="right" style="width:20px;">
													1.
												</td>
												<td valign="top">
													PT. <?=$namabank?>, Tbk., berkedudukan di Jakarta Selatan, suatu bank berbentuk perseroan terbatas yang didirikan menurut dan berdasarkan hukum Republik Indonesia, dengan cabang-cabang antara lain 
													<?echo $pcabang?> 
													yang dalam hal ini diwakili oleh :
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<table class="tbl100" border="0">
														<tr>
															<td valign="top" align="right" style="width:30px;">A.</td>
															<td>
																<?echo nl2br($pkbankwakila);?>
															</td>
														</tr>
														<tr>
															<td valign="top" align="right" style="width:30px;">B.</td>
															<td>
																<?echo nl2br($pkbankwakilb)?>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>
													Masing-masing bertindak dalam jabatannya tersebut selaku kuasa dari Direksi PT <?=$namabank?>, Tbk, berdasarkan Surat Kuasa nomor SK. 
													<?echo $pkbanksk1?> 
													, tanggal 
													<?echo $pkbanktanggalsk1?> 
													dan nomor 
													<?echo $pkbanksk2?>", 
													tanggal 
													<?echo $pkbanktanggalsk2?>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td colspan="2">
													- Untuk selanjutnya disebut �BANK�;
												</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td valign="top" align="right" style="width:20px;">
													2.
												</td>
												<td valign="top">
													<?echo nl2br($pkdebitur);?>
												</td>
											</tr>
											<tr>
												<td valign="top" align="right" style="width:20px;">&nbsp;
												</td>
												<td valign="top">
													<div>Untuk selanjutnya disebut �DEBITUR�.</div>
													<div>BANK dan DEBITUR telah saling setuju untuk membuat, melaksanakan dan mematuhi Perjanjian ini dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut:</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="pagebreak">
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<div style="font-weight:bold">PASAL 1<div>
										<div style="font-weight:bold">FASILITAS KREDIT<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										1.	Fasilitas Kredit yang diberikan BANK kepada DEBITUR  adalah : 
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<table width="100%" border="1" style="border-collapse:collapse; border-color:black;">
											<tr>
												<td align="center" style="border:1px solid black;">Fasilitas</td>
												<td align="center" style="border:1px solid black;">Jenis Fasilitas</td>
												<td align="center" style="border:1px solid black;">Bunga</td>
												<td align="center" style="border:1px solid black;">Tujuan Penggunaan</td>
												<td align="center" style="border:1px solid black;">Plafond</td>
											</tr>
											<?
												$sequence=0;
												$total_plafond=0;
												$sql_mkk="select cf.mkknewbungaflag,cf.mkknewadm,cf.custfacseq,cf.custcreditlong,cf.custcreditplafond,kp.produk_type_description, cn.credit_need_name,cf.sukubungayangdiberikan
												from tbl_customerfacility2 cf, Tbl_KodeProduk kp, Tbl_CreditNeed cn 
												where cf.custcredittype=kp.produk_loan_type
												and cf.custcreditneed=cn.credit_need_code
												and cf.custnomid='$custnomid'";
												$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
												$params_mkk = array(&$_POST['query']);
												$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
												if($conn==false){die(FormatErrors(sqlsrv_errors()));}
												if(sqlsrv_has_rows($sqlConn_mkk))
												{
													$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
													while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
													{
													$sequence+=1;
													$nama_produk=$row_mkk['produk_type_description'];
													$nama_credit=$row_mkk['credit_need_name'];
													$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
													$plafond=$row_mkk['custcreditplafond'];
													$plafond=$row_mkk['custcreditplafond'];
													$total_plafond+=$plafond;
													?>
													<tr>
														<td align="center" style="border:1px solid black;"><? echo $sequence; ?>&nbsp;</td>
														<td align="center" style="border:1px solid black;"><? echo $nama_produk;?></td>
														<td align="center" style="border:1px solid black;"><? echo $sukubungayangdiberikan;?> %</td>
														<td align="center" style="border:1px solid black;"><? echo $nama_credit;?></td>
														<td align="right" style="border:1px solid black;"><? echo numberFormat($plafond);?></td>
													</tr>
													
													<?
													}
												}
											?>
											<tr>
												<td align="center" colspan="4" style="border:1px solid black;">Total</td>
												<td align="right" style="border:1px solid black;"><? echo numberFormat($total_plafond);?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Bunga</td>
											<td valign="top" style="padding-top:8px; width:1px;">:</td>
											<td>
												<table width="100%" border="0">
												<?
														$sequence=0;
														$total_plafond=0;
														$sql_mkk="select cf.mkknewbungaflag,cf.mkknewadm,cf.custfacseq,cf.custcreditlong,cf.custcreditplafond,kp.produk_type_description, cn.credit_need_name,cf.sukubungayangdiberikan,cf.mkk_tujuan_existing,cf.pkterbilangbunga
														from tbl_customerfacility2 cf, Tbl_KodeProduk kp, Tbl_CreditNeed cn 
														where cf.custcredittype=kp.produk_loan_type
														and cf.custcreditneed=cn.credit_need_code
														and cf.custnomid='$custnomid'";
														$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
														$params_mkk = array(&$_POST['query']);
														$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
														if($conn==false){die(FormatErrors(sqlsrv_errors()));}
														if(sqlsrv_has_rows($sqlConn_mkk))
														{
															$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
															while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
															{
															$sequence+=1;
															$nama_produk=$row_mkk['produk_type_description'];
															$nama_credit=$row_mkk['credit_need_name'];
															$pkterbilangbunga=$row_mkk['pkterbilangbunga'];
															$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
															$plafond=$row_mkk['custcreditplafond'];
															$plafond=$row_mkk['custcreditplafond'];
															$custfacseq=$row_mkk['custfacseq'];	
															$total_plafond+=$plafond;
															?>
															<tr>
																<td style="border:0px solid black; width:15%;">Fasilitas <? echo $sequence; ?>&nbsp;</td>
																<td align="left" style="border:0px solid black; width:20%;"><? echo $sukubungayangdiberikan;?> %</td>
																<td>
																	<em>(<?echo ucwords(terbilang($sukubungayangdiberikan))." Persen"?>)</em>
																</td>
															</tr>
															<?
															}
														}
													?>
												</table>
												<div>
													Nb :: %&nbsp;<em>per tahun flat in arrear , metode perhitungan suku bunga  disetarakan dengan perhitungan bunga secara effektif dan
													</em> berlaku  tetap selama Jangka Waktu Fasilitas Kredit dengan tetap memperhatikan ketentuan  dalam pasal 4 Perjanjian <?php echo strtoupper($namabank)?> <? echo $Format_Generate?> ini. <em>
													Jumlah kewajiban bunga</em> sebagaimana diuraikan dalam jadwal angsuran terlampir yang merupakan satu  kesatuan dengan Perjanjian <?php echo strtoupper($namabank)?> <? echo $Format_Generate?> ini; 
												</div>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Jangka Waktu</td>
											<td valign="top" style="padding-top:8px; width:1px;">:</td>
											<td>
												<table width="100%" border="0">
												<?
													$sequence=0;
													$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
													$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
													$params_mkk = array(&$_POST['query']);
													$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
													if($conn==false){die(FormatErrors(sqlsrv_errors()));}
													if(sqlsrv_has_rows($sqlConn_mkk))
													{
														$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
														while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
														{
															$sequence+=1;	
															$custcreditlong=$row_mkk['custcreditlong'];		
															$mpk_jangka_waktu1=$row_mkk['mpk_jangka_waktu1'];	
															$mpk_jangka_waktu2=$row_mkk['mpk_jangka_waktu2'];	
															$custfacseq=$row_mkk['custfacseq'];	
													?>
														<tr>
															<td style="border:0px solid black; width:15%;">Fasilitas <? echo $sequence?></td>
															<td style="border:0px solid black; width:100px;"><? echo $custcreditlong?> Bulan</td>
															<td style="border:0px solid black;">
																Terhitung dari 
																<? echo $mpk_jangka_waktu1?> 
																sampai
																<? echo $mpk_jangka_waktu2?> 
															</td>
														</tr>
													<?
														}
													}
													sqlsrv_free_stmt( $sqlConn_mkk );
												?>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Angsuran</td>
											<td valign="top" style="padding-top:8px; width:1px;">:</td>
											<td>
												<table width="100%" border="0">
												<?
													$sequence=0;
													$sql_mkk="select cast(isnull(mkknewangsuran,0) as int) as 'a',* from tbl_customerfacility2 where custnomid='$custnomid'";
													$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
													$params_mkk = array(&$_POST['query']);
													$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
													if($conn==false){die(FormatErrors(sqlsrv_errors()));}
													if(sqlsrv_has_rows($sqlConn_mkk))
													{
														$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
														while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
														{
															$sequence+=1;	
															$custcreditlong=$row_mkk['custcreditlong'];		
															$custfacseq=$row_mkk['custfacseq'];		
															$mkknewangsuran=$row_mkk['a'];		
															$pkterbilangangsuran=$row_mkk['pkterbilangangsuran'];	
															$mpk_waktu_angsuran1=$row_mkk['mpk_waktu_angsuran1'];	
															$mpk_waktu_angsuran2=$row_mkk['mpk_waktu_angsuran2'];	
															
															if($pktglstatus=="A")
															{
																$statustanggal=
																'
																Sebesar '.numberFormat($mkknewangsuran).'/ bulan, <em>('.ucwords(terbilang($mkknewangsuran)).')</em> dan pembayaran dilakukan setiap bulan selambat-lambatnya pada tanggal
																'.$mpk_waktu_angsuran1.'
																atau tanggal lain yang disetujui oleh BANK) (selanjutnya disebut �Tanggal Angsuran�), yang untuk pertama kali dimulai pada tanggal
																'.$mpk_waktu_angsuran2.'
																sebagaimana diuraikan dalam rincian jadwal angsuran.
																';
															}
															else
															{
																$statustanggal=
																'
																Sebesar '.numberFormat($mkknewangsuran).'/ bulan, dan pembayaran dilakukan setiap bulan selambat-lambatnya pada tanggal
																'.$mpk_waktu_angsuran1.'
																atau tanggal lain yang disetujui oleh BANK (selanjutnya disebut �Tanggal Angsuran�), sebagaimana diuraikan dalam rincian jadwal angsuran.
																';
															}
															echo"
																<tr>
																	<td>".$statustanggal."</td>
																</tr>
															";
														}
													}
													sqlsrv_free_stmt( $sqlConn_mkk );
												?>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Provisi</td>
											<td valign="top" style="padding-top:8px; width:1px;">:</td>
											<td>
												<table width="100%" border="0">
												<?
													$sequence=0;
													$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
													$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
													$params_mkk = array(&$_POST['query']);
													$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
													if($conn==false){die(FormatErrors(sqlsrv_errors()));}
													if(sqlsrv_has_rows($sqlConn_mkk))
													{
														$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
														while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
														{
															$sequence+=1;	
															$custcreditlong=$row_mkk['custcreditlong'];		
															$mpk_jangka_waktu1=$row_mkk['mpk_jangka_waktu1'];	
															$mpk_jangka_waktu2=$row_mkk['mpk_jangka_waktu2'];	
													echo"
														<tr>
															<td>
															1 % (<em>satu persen</em>) flat dari jumlah fasilitas kredit.
															</td>
														</tr>
													";
														}
													}
													sqlsrv_free_stmt( $sqlConn_mkk );
												?>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Biaya Administrasi</td>
											<td valign="top" style="padding-top:8px; width:1px;">:</td>
											<td>
												<table width="100%" border="0">
												<?
													$sequence=0;
													$tadm=0;
													$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
													$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
													$params_mkk = array(&$_POST['query']);
													$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
													if($conn==false){die(FormatErrors(sqlsrv_errors()));}
													if(sqlsrv_has_rows($sqlConn_mkk))
													{
														$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
														while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
														{
															$sequence=$row_mkk['custfacseq'];	
															$mkknewkewajibanbank=$row_mkk['mkknewkewajibanbank'];	
															$mkknewadm=$row_mkk['mkknewadm'];	
															$tadm+=$mkknewadm;
															/*
															<tr>
																<td style="border:0px solid black; width:10%;">Fasilitas <? echo $sequence?></td>
																<td align="right" style="border:0px solid black; width:15%;">Rp. <? echo numberFormat($mkknewadm)?></td>
																<td>	
																	<em>(<input style="width:80%;" type="text" name="" id="" />)</em>
																</td>
															</tr>
															*/
														}
													}
													sqlsrv_free_stmt( $sqlConn_mkk );
													?>
													<tr>
														<td style="border:0px solid black; width:20%;" align="left">Rp. <? echo numberFormat($tadm)?></td>
														<td>	
															<em>(<?echo ucwords(terbilang($tadm))." Rupiah"?>)</em>
														</td>
													</tr>
												</table>
											</td>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<tr>
												<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Denda Keterlambatan Pembayaran Angsuran</td>
												<td valign="top" style="padding-top:8px; width:1px;">:</td>
												<td>
													&nbsp;<? echo $denda_nilai_dkpa?> % per bulan, yang dihitung dari jumlah angsuran yang tertunggak.
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;"border="0">
											<tr>
												<td valign="top" style="padding-top:8px; padding-left:100px; width:150px;">Denda Pembayaran Dipercepat</td>
												<td valign="top" style="padding-top:8px; width:1px;">:</td>
												<td>2 (dua) kali angsuran.</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="pagebreak">
									</td>
								</tr>
								<tr >
									<td style="text-align:center;" class="">
										<div style="font-weight:bold">PASAL 2<div>
										<div style="font-weight:bold">JAMINAN<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										Untuk menjamin pembayaran kembali sebagaimana mestinya semua jumlah uang yang terhutang dan wajib dibayar oleh DEBITUR kepada BANK berdasarkan Perjanjian berikut setiap perubahannya, maka DEBITUR dan / atau pihak ketiga memberikan jaminan (-jaminan)  kepada BANK sebagai berikut  : 
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%">
										<?				
											$seq=0;
											$strsql="select *,b.ap_lisregno from TblCollateralType a 
													join (select distinct(cust_jeniscol) as 'cust_jeniscol',ap_lisregno 
													from tbl_cust_mastercol where flaginsert=1 and flagdelete=0 and ap_lisregno='".$custnomid."') b on  b.cust_jeniscol=a.col_code 
													left join tbl_pk_comment c on c.custnomid= b.ap_lisregno
													and b.cust_jeniscol=c.col_id
													where b.ap_lisregno='".$custnomid."'";
													
											$sqlcon = sqlsrv_query($conn, $strsql);
											if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
											if(sqlsrv_has_rows($sqlcon))
											{
												while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
												{
													$seq+=1;
													echo'
													<tr>
														<td>
															'.$seq.'. '.$rows['col_name'].'</br>
															'.nl2br($rows['pk_coment']).'
														</td>
													</tr>
													';
												}
											}
											?>
										</table>
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<div style="font-weight:bold">PASAL 3<div>
										<div style="font-weight:bold">KONDISI TERTENTU<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										Dalam kondisi tertentu dimana tingkat suku bunga perbankan pada umumnya mengalami kealtkan diluar batas kewajaran, maka BANK atas pertimbangannya sendiri berhak untuk menyesuaikan tingkat suku bunga yang berlaku, perubahan mana akan diberitahukan secara tertulis  kepada DEBITUR, dan pemberitahuan mana mengikat DEBITUR.
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<div style="font-weight:bold">PASAL 4<div>
										<div style="font-weight:bold">KONDISI TERTENTU<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										Debitur dengan ini menyatakan telah membaca, mengerti, memahami, dengan jelas dan menerima baik serta mengikat diri untuk tunduk terhadap seluruh syarat-syarat dan ketentuan umum sebagaimana tertuang dalam Lampiran Perjanjian Kredit (�Perjanjian�), lampiran mana merupakan satu kesatuan yang tidak terpisahkan dengan Perjanjian.
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<div style="font-weight:bold">PASAL 5<div>
										<div style="font-weight:bold">KETENTUAN LAIN-LAIN<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										<?echo nl2br($pkketentuanlain)?>
									</td>
								</tr>
								<tr>
									<td style="text-align:center;">
										<div style="font-weight:bold">PASAL 6<div>
										<div style="font-weight:bold">KORESPONDENSI<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										Semua surat menyurat atau pemberitahuan yang perlu dikirim oleh masing-masing pihak kepada pihak yang lain mengealt atau sehubungan dengan Perjanjian ini harus dilakukan secara langsung dengan surat tercatat atau melalui ekspedisi, faksimili, kawat atau telex ke alamat-alamat dibawah ini : 
									</td>
								</tr>
								<tr>
									<td>
										<table style="width:100%;" border="0" >	
											<tr>
												<td colspan="2">
													1. BANK
												</td>
											</tr>
											<tr>
												<td style="width:250px;">
													<?=strtoupper($namabank)?> KANTOR CABANG
												</td>
												<td>
													 <? echo $branch_name ?>
												</td>
											</tr>
											<tr>
												<td>
													Alamat Cabang
												</td>
												<td>
													 <? echo $branch_address ?>
												</td>
											</tr>
											<tr>
												<td>
													Kota Cabang
												</td>
												<td>
													 <? echo $branch_city ?>
												</td>
											</tr>
											<tr>
												<td>
													No Telepon Cabang
												</td>
												<td>
													<?echo $pknocabang?>
												</td>
											</tr>
											<tr>
												<td>
													No. FAX
												</td>
												<td>
													<?echo $pkfaxcabang?>
												</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2">
													2. Debitur
												</td>
											</tr>
											<tr>
												<td>
													Nama Debitur
												</td>
												<td>
													 <? echo $debiturname ?>
												</td>
											</tr>
											<tr>
												<td>
													Alamat Debitur
												</td>
												<td>
													<?echo nl2br($pkaltaddrdebitur)?>
												</td>
											</tr>
											<tr>
												<td>
													No Telepon
												</td>
												<td>
													<?=$telp?>
												</td>
											</tr>
											<tr>
												<td>
													No Telepon Alternative
												</td>
												<td>
													<?echo $pkalttelepondebitur?>
												</td>
											</tr>
											<tr>
												<td>
													No. HP
												</td>
												<td>
													 <?=$hp?>
												</td>
											</tr>
											<tr>
												<td>
													No. FAX
												</td>
												<td>
													<?echo $pkfaxdebitur?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								
								<tr>
									<td class="pagebreak">
									</td>
								</tr>
								<tr >
									<td style="text-align:center;" class="">
										<div style="font-weight:bold">PASAL 7<div>
										<div style="font-weight:bold">DOMISILI HUKUM<div>
										<div style="font-weight:bold">&nbsp;<div>
									</td>
								</tr>
								<tr>
									<td>
										Kecuali ditetapkan lain dalam Perjanjian, maka kedua belah pihak memilih tempat kedudukan hukum yang tetap dan seumumnya di Kantor Kepaniteraan Pengadilan Negeri
										<?echo $pkpengadilannegri?>
										namun, tidak mengurangi hak dan wewenang BANK untuk memohon pelaksanaan (eksekusi) atau mengajukan tuntutan/gugatan hukum terhadap DEBITUR berdasarkan Ketentuan Umum  ini dimuka pengadilan lain maupun instansi lain yang berwenang dalam wilayah Republik Indonesia.
									</td>
								</tr>
								<tr>
									<td>
										<table align="center" width="800" border="0">
												<tr>
												<td width="200"> BANK</td>
												<td width="200">&nbsp;</td>
												<td align="center" width="200">DEBITUR</td>
												<td align="center" width="200">
												<?echo $menyetujui?>
												</td>
											</tr>
											<tr>
												<td>PT. <?=$namabank?>, Tbk. </td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td height="110">&nbsp;</td>
												<td>&nbsp;</td>
												<td align="center">MATERAI</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td align="center"><?echo $pknamajabatan1?></td>
												<td align="center"><?echo $pknamajabatan2?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td align="center"><?echo $pkjabatan1?></td>
												<td align="center"><?echo $pkjabatan2?></td>
												<td align="center"><? echo $pknamadebitur; ?></td>
												<td align="center"><? echo $marname?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
					<div>&nbsp;</div>
					<?
						if($userid != "")
						{
						require ("../../requirepage/btnview.php");
						require ("../../requirepage/hiddenfield.php");
						}
						require("../../requirepage/btnprint.php");
					?>
					<input type="hidden" name="pkmmu" value=""/>
					<input type="hidden" name="pktglstatus" value="<?echo $pktglstatus?>"/>
					<input type="hidden" name="pknumber" id="pknumber" maxlength="100" value="<? echo $Format_Generate.$region_code.'0'.$ao_branch.$str; ?>"/>
					
			</center>
		</form>
	</body>
</html>