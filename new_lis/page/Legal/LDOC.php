<?php
require ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require ("../../requirepage/parameter.php");
//require ("../../lib/open_con_dm.php");
require ("../../lib/open_con_dm_mysql.php");
require ("../../lib/class.sqlserver.php");
$statusreadonly = "";
if ($userpermission != "I")
{
   $statusreadonly = "readonly=readonly";
}
/*
  $ipdm = "";
  $userdm = "";
	$tsql = "select control_value from ms_control where control_code='IPDM'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if ( $sqlConn === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$row[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}	
*/

  $custcif = "";
	$strsql="SELECT * FROM Tbl_CustomerMasterPerson2
      				 WHERE custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcif = $rows['custaplno'];
		}
	}

    $statusdoc="";
    $kondisistatusdoc = "";
		$strsqlv01="SELECT * FROM Tbl_DocPerson WHERE doc_id = 'PDOC00' or doc_id='PDOC01'";
		$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
		if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlconv01))
		{
			while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
			{
		    $statusdoc .= $rowsv01['doc_id'] . ",";
        $kondisistatusdoc .= "doc_type='" . $rowsv01['doc_id'] . "' or ";
			}
		}

    if ($kondisistatusdoc != "")
    {
	     $kondisistatusdoc = "AND (" . substr($kondisistatusdoc,0,strlen($kondisistatusdoc)-3) . ")";
    } 

  $ipdm = "";
  $userdm = "";
	$strsql="select control_value from ms_control where control_code='IPDM'";
	//echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$rows[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}
$key = $custcif . "_" . $custnomid;
$linkdmV = $ipdm. '/external_view3.php?dmuserid=' . $userdm . '&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=view&key=' . $key . '&dmstatusdoc=' . $statusdoc;

        $tuauserid = "";
        $tuatime = "";
    		$tsql = "select tua_userid,tua_time from Tbl_TemporariUserAkses 
    							where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
   			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			$params = array(&$_POST['query']);
   			$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
				if($sqlConn === false)
				{
					die(FormatErrors(sqlsrv_errors()));
				}
	
				if(sqlsrv_has_rows($sqlConn))
				{
      			$rowCount = sqlsrv_num_rows($sqlConn);
      			while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      			{
        			$tuauserid = $row[0];
        			$tuatime = $row[1];
      			}
   			}
   			sqlsrv_free_stmt( $sqlConn );

				if ($tuauserid == "" || $tuatime == "")
				{
					$tsql = "INSERT INTO Tbl_TemporariUserAkses values('$userwfid',
										'$userid','$custnomid','$userpermission',1,convert(varchar,getdate(),121))";
					$params = array(&$_POST['query']);
					$stmt = sqlsrv_prepare( $conn, $tsql, $params);
					if( $stmt )
					{
					} 
					else
					{
						echo "Error in preparing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
	
					if( sqlsrv_execute( $stmt))
					{
					}
					else
					{
						echo "Error in executing statement.\n";
						die( print_r( sqlsrv_errors(), true));
					}
					sqlsrv_free_stmt( $stmt);
      	  $bisalanjut = "Y";
				}
				else
				{
					if ($tuauserid == $userid)
					{
						$tsql = "UPDATE Tbl_TemporariUserAkses
											set tua_count=tua_count+1,
											tua_action='$userpermission',
											tua_time=convert(varchar,getdate(),121)
											where tua_wfid='$userwfid'
    							AND tua_nomid='$custnomid'";
						$params = array(&$_POST['query']);
						$stmt = sqlsrv_prepare( $conn, $tsql, $params);
						if( $stmt )
						{
						} 
						else
						{
							echo "Error in preparing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
	
						if( sqlsrv_execute( $stmt))
						{
						}
						else
						{
							echo "Error in executing statement.\n";
							die( print_r( sqlsrv_errors(), true));
						}
						sqlsrv_free_stmt( $stmt);
      	  	$bisalanjut = "Y";
					}
					else
					{
						$varmsg = "Application ID <b>$custnomid</b> sudah diambil oleh user <font color=blue><b>$tuauserid</b></font> pada jam <font color=red><b>$tuatime</b></font> <BR>";
						echo $varmsg;
						exit;
					}
				}


   			  $tsql2 = "SELECT *
						        FROM Tbl_WorkflowDoc
						        WHERE wf_id='$userwfid'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

				  $datadoc = "";
          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
							     $datadoc = $row2['wf_doc'];
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
          $arrdoc=explode("|",$datadoc);

      $arrjaminan = array();   
      $jenisjaminan = "";
      $kondisijaminan = "";
      $tsql = "SELECT cust_jeniscol,col_id,group_col FROM Tbl_Cust_MasterCol
      				 WHERE ap_lisregno='$custnomid' and flaginsert='1' and flagdelete='0'"; //echo $tsql;				 
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
         	  $vartemparr = $row[0] . "__" . $row[1] . "__" . $row[2];
      	      array_push($arrjaminan,$vartemparr);
         	  if (substr($row[0],0,1) == "B")
         	  {
         	     $jenisjaminan .= "Building, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	     $doccoltype = "BA1";
         	  }
         	  if (substr($row[0],0,1) == "V")
         	  {
         	     $jenisjaminan .= "Vehicle, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='V'";
         	     $doccoltype = "V01";
         	  }
         	  if (substr($row[0],0,1) == "T")
         	  {
         	     $jenisjaminan .= "Tanah, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	     $doccoltype = "BA1";
         	  }
         	  if (substr($row[0],0,1) == "K")
         	  {
         	     $jenisjaminan .= "Kios, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='T'";
         	     $doccoltype = "BA1";
         	  }
         	  if (substr($row[0],0,1) == "D")
         	  {
         	     $jenisjaminan .= "Deposito, ";
         	     $kondisijaminan .= "or SUBSTRING(doc_segmen,1,1)='D'";
         	     $doccoltype = "D01";
         	  }
			  
			  //echo "kondisi jaminan nih:".$kondisijaminan;
         }
      }
      sqlsrv_free_stmt( $sqlConn );
      array_push($arrjaminan,"PERSONAL__1__1");
      //array_push($arrjaminan,"CORPORATE__1__1");
	  

?>
<HTML>
   <HEAD>
      <META http-equiv=Content-Type content='text/html; charset=iso-8859-1'>
      <META http-equiv='Pragma' content='no-cache'>
      <META content='MSHTML 5.50.4134.100' name=GENERATOR>
      <TITLE>SPIN UPLOAD</TITLE>
      <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
      <link href="../../css/d.css" rel="stylesheet" type="text/css" />
      <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
      <Script Language="JavaScript">
        function toggleExpCol(idGrp, idContainer) {
            idContainer = idContainer ? idContainer + '_' : '';
            var div = document.getElementById(idContainer + 'divGrp' + idGrp);
            var vis = div.style.display = (div.style.display == 'none') ? 'block' : 'none';
        }

			function refreshhtml()
			{
				window.location.reload()
			}
			function uploadDocument(thelink)
			{
	        varwidth = 980;
	        varheight = 640;
	        varx = 0;
	        vary = 0;
          window.open(thelink,'lainnya','scrollbars=yes,width='+varwidth+',height='+varheight+',screenX='+varx+',screenY='+vary+',top='+varx+',left='+vary+',status=yes');
//          document.formsubmit.submit();
			}
	function cekthis()
	{
		document.frm.target = "utama";
		document.frm.act.value = "saveform";
		document.frm.action = "./dospin_legal.php";
           submitform = window.confirm("<? echo $confmsg;?>");
           if (submitform == true)
           {
	            document.frm.submit();
              return true;
           }
           else
           {
              return false;
           }
	}
      </Script>
   </HEAD>
   <BODY link=blue alink=blue vlink=blue>
   <div style="float: right;margin-right: 70px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
      <br />
      <br />
<script language="JavaScript"><!--
name = 'utama';
//--></script>
      <div align=center>
      <TABLE cellPadding=5 width="100%" border=0 class="table">
        <TR>
    	  <TD vAlign=top width=1>
            <TABLE cellSpacing=0 cellPadding=0 width="95%" border=0>
              <TR>
                <TD class=backW vAlign=center>
          	</TD>
              </TR>
            </TABLE>
          </TD>
          <TD align=left valign=top>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <TR>
                <TD class=borderForm align=right bgColor=black>
                      <font style="font-size: 14;" color=#FFFFFF><B>Form type : Verifikasi Dokumen &nbsp</B></font>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                  <TABLE cellSpacing=1 cellPadding=13 width="100%" border=0>
                    <TR>
                      <TD class=backW>
                  	<form name="frm" id="frm" method=post action=./dospin_legal.php>
                  	   <TABLE WIDTH=100% CELLPADDING=1 CELLSPACING=1 border=0>                  	   	
<?
	  $jenis_kelamin = "";
	  $custcif = "";
      $tsql = "SELECT * FROM Tbl_CustomerMasterPerson2
      				 WHERE custnomid='$custnomid'";
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);

      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
         {
			 
			$jenis_kelamin = $row['custsex'];
			$custcif = $row['custaplno'];

			 if(trim($row['custbusname'])!="")
			 {
				 $tampilnama = $row['custbusname'];
			 }
			 else
			 {
				 $tampilnama = $row['custfullname'];
			 }
			 
?>
							          <tr>
							          	 <td width=20% align=left valign=top>
							          	 	<font face=Arial size=2>Application ID / CIF</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=70% align=left valign=top>
							          	 	<font face=Arial size=2><? echo $custnomid; ?> / <? echo $custcif; ?></font>
							          	 	&nbsp &nbsp
							          	 	<a href="javascript:refreshhtml()">refresh</a>
							          	</td>
							          </tr>
							          <tr>
							          	 <td width=20% align=left valign=top>
							          	 	<font face=Arial size=2>Customer Name</font>
							          	</td>
							          	 <td width=10% align=left valign=top>
							          	 	<font face=Arial size=2>:</font>
							          	</td>
							          	 <td width=70% align=left valign=top>
							          	 	<font face=Arial size=2><A HREF=../preview/previewall/previewall_n.php?userpermission=I&buttonaction=ICA&userwfid=ALL target=_blank><? echo $tampilnama; ?></A></font>
							          	</td>
							          </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
?>
                  	   </TABLE>
                             <IFRAME WIDTH=100% HEIGHT=300 MARGINWIDTH=0 MARGINHEIGHT=0 HSPACE=0 VSPACE=0 FRAMEBORDER=0 SCROLLING=no SRC='<? echo $linkdmV ?>'></iframe>
                  	   <BR>
<?
   $hiddenfield = "";
   //echo "naiko";
   for ($ki=0;$ki<count($arrjaminan);$ki++)
   {
      $vartemparr=explode("__",$arrjaminan[$ki]);
	  //echo "vartemp = ".$vartemparr[0];
	  
		if ($vartemparr[0] == "PERSONAL")
			{
         $jenisjaminan = "NON COLATERAL";
         $tsql = "SELECT * FROM Tbl_DocPerson
      				 		WHERE doc_segmen='PERSONAL' OR doc_segmen='FBIC'"; //echo $tsql;
      }
	  else if($vartemparr[0] == "CORPORATE")
	  {
		  
         $jenisjaminan = "NON COLATERAL";
         $tsql = "SELECT * FROM Tbl_DocPerson
      				 		WHERE doc_segmen='CORPORATE'"; //echo $tsql;
	  }
      else
      {
         	  if (substr($vartemparr[0],0,1) == "B")
         	  {
         	     $jenisjaminan = "Building";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='B'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "V")
         	  {
         	     $jenisjaminan = "Vehicle";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='V'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "T")
         	  {
         	     $jenisjaminan = "Tanah";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='T'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "K")
         	  {
         	     $jenisjaminan = "Kios";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='K'";
         	  }
         	  if (substr($vartemparr[0],0,1) == "D")
         	  {
         	     $jenisjaminan = "Deposito";
         	     $kondisijaminan = "AND SUBSTRING(doc_segmen,1,1)='D'";
         	  }
         $tsql = "SELECT * FROM Tbl_DocPerson
         					WHERE doc_id<>''
      				 		$kondisijaminan"; //echo $tsql; exit();
							      }
?>
                  	   <font face=Arial size=2><b>DOKUMEN <? echo $jenisjaminan ?> - <? echo $arrjaminan[$ki] ?></b></font>
                  	   <TABLE WIDTH=100% CELLPADDING=2 CELLSPACING=0 border=1 bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
												   <tr bgcolor=#CCCCCC>
												   	<td width=50% align=center valign=top>
												   		<font face=Arial size=2><b>DOCUMENT</b></font>
												   	</td>
												   	<td width=20% align=center valign=top>
												   		<font face=Arial size=2><b>NOMOR</b></font>
												   	</td>
												   	<td width=20% align=center valign=top>
												   		<font face=Arial size=2><b>EXPIRED</b></font>
												   	</td>
												   	<td class="unprint" width=10% align=center valign=top>
												   		<font face=Arial size=2><b>PREVIEW</b></font>
												   	</td>
												  </tr>
<?
      if ($datadoc != "") 
      {      	
      	$tsql .= " AND (";
         for ($zz=0;$zz<count($arrdoc)-1;$zz++)
         {
      	   $tsql .= " doc_id='$arrdoc[$zz]' or ";
         }
         $vartemp = substr($tsql,0,strlen($tsql)-3) . ")";
         $tsql = $vartemp;
      }

      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);
      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );
      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
      		 $vardocno = "";
      		 $vardocdate = "";
      		 $vardoccolid = "";
      		 $vardocjeniscol = "";	 
					
   			  $tsql2 = "SELECT *
						        FROM Tbl_CKPKLegal
						        WHERE docnomid='$custnomid'
						        AND doccode='$row[0]'
						        AND doc_jeniscol='$vartemparr[0]'
						        AND doc_colid='$vartemparr[1]'
						        AND doc_groupcol='$vartemparr[2]'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
      		   	$vardocno = $row2['docno'];
      		   	$vardocdate = $row2['docdate'];
      		   	$vardoccolid = $row2['doc_colid'];
      		   	$vardocjeniscol = $row2['doc_jeniscol'];
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
   	      $namafield = $row[0] . "__" . $arrjaminan[$ki];
   	      $hiddenfield .= $namafield . ",";
?>
												   <tr>
												   	<td width=50% align=left valign=top>
												   		<font face=Arial size=2><? echo $row[0]; ?> - <? echo $row[1]; ?></font>
												   	</td>
												   	<td width=20% align=center valign=top>
<?
															if ($statusreadonly == "")
															{
?>
												   		   <input type=text name=NO<? echo $namafield; ?> size=20 maxlength=30 style="background-color:#FF0" value='<? echo $vardocno; ?>' <? echo $statusreadonly; ?>>
<?
															}
															else
															{
?>
												   		   <font face=Arial size=2><? echo $vardocno; ?>&nbsp;</font>
<?
															}
?>
												   	</td>
												   	<td width=20% align=center valign=top>
<?
															if ($statusreadonly == "")
															{
?>
												   		   <input type=text id="DATE<? echo $namafield;?>" name="DATE<? echo $namafield;?>" size=15 maxlength=10 style="width: 25mm;" value='<? echo $vardocdate; ?>' <? echo $statusreadonly; ?>>
		   		    							     <a href="javascript:NewCssCal('DATE<? echo $namafield; ?>','yyyymmdd')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
<?
															}
															else
															{
?>
												   		   <font face=Arial size=2><? echo $vardocdate; ?> &nbsp;</font>
<?
															}
?>
												   	</td>
												   	<td class="unprint" width=10% align=center valign=top>
<?
     $statusdoc = $row[0] . ",";
//     $custcif = "CIF001";
		 $key = $custcif . "_" . $custnomid;

     $querytemp = "SELECT COUNT(*)
	   		                    FROM Tbl_Document
	        	                WHERE doc_index3='$custnomid'
	        	                AND doc_type='$row[0]'";
	   $resulttemp=mysql_query($querytemp);
	   $rowtemp = mysql_fetch_row($resulttemp);
	   $countrows=$rowtemp[0];

//     $linkdmI='http://'.$ipdm.'/Lis_mikro/spindm_new/external_upload.php?dmuserid=lismikro&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     $linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&dmuserorganization=PRIVATE&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     if ($countrows <= 0)
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload</A></div>";
     }
     else
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload / View</A></div>";
     }
     echo $uploadgambarI;
?>                    		      
												   	</td>
												  </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
      echo "</TABLE><BR>";

		  if ($vartemparr[0] != "PERSONAL" && $vartemparr[0] != "PERSONAL")
			{
?>
                  	   <font face=Arial size=2><b><font color=red>APPRAISAL</font> <? echo $jenisjaminan ?> - <? echo $arrjaminan[$ki] ?></b></font>
                  	   <TABLE WIDTH=100% CELLPADDING=2 CELLSPACING=0 border=1 bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
												   <tr bgcolor=#CCCCCC>
												   	<td width=50% align=center valign=top>
												   		<font face=Arial size=2><b>DOCUMENT</b></font>
												   	</td>
												   	<td width=20% align=center valign=top>
												   		<font face=Arial size=2><b>NOMOR</b></font>
												   	</td>
												   	<td width=20% align=center valign=top>
												   		<font face=Arial size=2><b>EXPIRED</b></font>
												   	</td>
												   	<td class="unprint" width=10% align=center valign=top>
												   		<font face=Arial size=2><b>PREVIEW</b></font>
												   	</td>
												  </tr>
<?

      $tsql = "SELECT * FROM Tbl_DocCol
         					WHERE doc_id<>''
      				 		AND doc_col_type='$doccoltype'"; //echo $tsql; exit();
      $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $params = array(&$_POST['query']);
      $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
      if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );
      if(sqlsrv_has_rows($sqlConn))
      {
         $rowCount = sqlsrv_num_rows($sqlConn);
         while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
         {
      		 $vardocno = "";
      		 $vardocdate = "";
      		 $vardoccolid = "";
      		 $vardocjeniscol = "";	 
					
   			  $tsql2 = "SELECT *
						        FROM Tbl_CKPKLegal
						        WHERE docnomid='$custnomid'
						        AND doccode='$row[0]'
						        AND doc_jeniscol='$vartemparr[0]'
						        AND doc_colid='$vartemparr[1]'
						        AND doc_groupcol='$vartemparr[2]'";
          $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   			  $params2 = array(&$_POST['query']);

   		    $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

          if ( $sqlConn2 === false)
      			die( FormatErrors( sqlsrv_errors() ) );

   			  if(sqlsrv_has_rows($sqlConn2))
   			  {
      		   $rowCount2 = sqlsrv_num_rows($sqlConn2);
      			 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_ASSOC))
      		   {
      		   	$vardocno = $row2['docno'];
      		   	$vardocdate = $row2['docdate'];
      		   	$vardoccolid = $row2['doc_colid'];
      		   	$vardocjeniscol = $row2['doc_jeniscol'];
      		   }
   				}
   	      sqlsrv_free_stmt( $sqlConn2 );
   	      $namafield = $row[0] . "__" . $arrjaminan[$ki];
   	      $hiddenfield .= $namafield . ",";
?>
												   <tr>
												   	<td width=50% align=left valign=top>
												   		<font face=Arial size=2><? echo $row[0]; ?> - <? echo $row[1]; ?></font>
												   	</td>
												   	<td width=20% align=center valign=top>
<?
															if ($statusreadonly == "")
															{
?>
												   		   <input type=text name=NO<? echo $namafield; ?> size=20 maxlength=30 style="background-color:#FF0" value='<? echo $vardocno; ?>' <? echo $statusreadonly; ?>>
<?
															}
															else
															{
?>
												   		   <font face=Arial size=2><? echo $vardocno; ?>&nbsp;</font>
<?
															}
?>
												   	</td>
												   	<td width=20% align=center valign=top>
<?
															if ($statusreadonly == "")
															{
?>
												   		   <input type=text id="DATE<? echo $namafield;?>" name="DATE<? echo $namafield;?>" size=15 maxlength=10 style="width: 25mm;" value='<? echo $vardocdate; ?>' <? echo $statusreadonly; ?>>
		   		    							     <a href="javascript:NewCssCal('DATE<? echo $namafield; ?>','yyyymmdd')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
<?
															}
															else
															{
?>
												   		   <font face=Arial size=2><? echo $vardocdate; ?> &nbsp;</font>
<?
															}
?>
												   	</td>
												   	<td class="unprint" width=10% align=center valign=top>
<?
     $statusdoc = $row[0] . ",";
//     $custcif = "CIF001";
		 $key = $custcif . "_" . $custnomid;

     $querytemp = "SELECT COUNT(*)
	   		                    FROM Tbl_Document
	        	                WHERE doc_index3='$custnomid'
	        	                AND doc_type='$row[0]'";
	   $resulttemp=mysql_query($querytemp);
	   $rowtemp = mysql_fetch_row($resulttemp);
	   $countrows=$rowtemp[0];

//     $linkdmI='http://'.$ipdm.'/Lis_mikro/spindm_new/external_upload.php?dmuserid=lismikro&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     $linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&dmuserorganization=PRIVATE&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
     if ($countrows <= 0)
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload</A></div>";
     }
     else
     {
        $uploadgambarI = "<div><A HREF=\"javascript:uploadDocument('$linkdmI')\">Upload / View</A></div>";
     }
     echo $uploadgambarI;
?>                    		      
												   	</td>
												  </tr>
<?
         }
      }
      sqlsrv_free_stmt( $sqlConn );
      echo "</TABLE><BR>";
      }

   }
?>                       
<center>
<?
if($userpermission == "I")
{
	?>
	<input type="button" id="btnsave" name="btnsave" value="save" class="buttonsaveflow" onClick="cekthis();"/>
	<?
}
else
{
	require ("../../requirepage/btnview.php");
	require ("../../requirepage/btnprint.php");
}
require ("../../requirepage/hiddenfield.php");
?>
<input type=hidden name=act value='simpandata'>
<input type=hidden name=datadoc value='<? echo $datadoc; ?>'>
<input type=hidden name=hiddenfield value='<? echo $hiddenfield; ?>'>
</center>                     
</form>
<center>
<?
if($userpermission == "I")
{
}
else
{
	//require ("../../requirepage/back_to.php");
}
?>
</center>
                      </TD>
                    </TR>
                    <TR>
                      <TD class=backW></TD>
                    </TR>
                  </TABLE>
                </TD>
              </TR>
              <TR>
                <TD height=15></TD>
              </TR>
              <TR>
                <TD class=borderB>
                </TD>
              </TR>
              <TR>
                <TD height=15>
                   <table width=100% cellpadding=0 cellspacing=0 border=0>
                      <tr>
                         &nbsp
                      <tr>
                      <tr>
                      </tr>
                   </table>
                </TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
     </div>
   </BODY>
</HTML>

