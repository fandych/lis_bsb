<?php


require('./sqlsrv.php');
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");

$custnomid= isset($_POST['custnomid']) ? $_POST['custnomid'] : "";
$nomorpk= isset($_POST['nomorpk']) ? $_POST['nomorpk'] : "";
$tanggalpk= isset($_POST['tanggalpk']) ? $_POST['tanggalpk'] : "";
$namapemimpin_1= isset($_POST['namapemimpin_1']) ? $_POST['namapemimpin_1'] : "";
$jabatanpemimpin_i= isset($_POST['jabatanpemimpin_i']) ? $_POST['jabatanpemimpin_i'] : "";
$p2_jenis= isset($_POST['p2_jenis']) ? $_POST['p2_jenis'] : "";
$p2_luas= isset($_POST['p2_luas']) ? $_POST['p2_luas'] : "";
$p2_block= isset($_POST['p2_block']) ? $_POST['p2_block'] : "";
$p2_nomor= isset($_POST['p2_nomor']) ? $_POST['p2_nomor'] : "";
$p2_lokasi= isset($_POST['p2_lokasi']) ? $_POST['p2_lokasi'] : "";
$p2_penjual= isset($_POST['p2_penjual']) ? $_POST['p2_penjual'] : "";
$p3_tanggalkreditberakhir= isset($_POST['p3_tanggalkreditberakhir']) ? $_POST['p3_tanggalkreditberakhir'] : "";
$p3_tanggalselambatnya= isset($_POST['p3_tanggalselambatnya']) ? $_POST['p3_tanggalselambatnya'] : "";
$p3_dendaketerlambatan= isset($_POST['p3_dendaketerlambatan']) ? $_POST['p3_dendaketerlambatan'] : "";
$p3_biayaadminpersen= isset($_POST['p3_biayaadminpersen']) ? $_POST['p3_biayaadminpersen'] : "";
$p4_tanggalkreditmulai= isset($_POST['p4_tanggalkreditmulai']) ? $_POST['p4_tanggalkreditmulai'] : "";
$p4_rektabungan= isset($_POST['p4_rektabungan']) ? $_POST['p4_rektabungan'] : "";
$p4_atasnama= isset($_POST['p4_atasnama']) ? $_POST['p4_atasnama'] : "";
$p4_rekpinjaman= isset($_POST['p4_rekpinjaman']) ? $_POST['p4_rekpinjaman'] : "";
$p15_pengadilanengeri= isset($_POST['p15_pengadilanengeri']) ? $_POST['p15_pengadilanengeri'] : "";
$p15_badanpiutang= isset($_POST['p15_badanpiutang']) ? $_POST['p15_badanpiutang'] : "";
$txt_point1= isset($_POST['txt_point1']) ? $_POST['txt_point1'] : "";
$p17_tarifkpr= isset($_POST['p17_tarifkpr']) ? $_POST['p17_tarifkpr'] : "";
$p17_tarifkpr = str_replace(",","",$p17_tarifkpr);

$p4_tanggalmulaibayar = date('Y-m-d', strtotime("+1 months", strtotime($tanggalpk)));



$strsql = "select * from PK_KGS where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);
if(count($rs)=="0")
{
$tsql = 'INSERT into PK_KGS (
  CUSTNOMID,
  NOMORPK,
  TANGGALPK,
  NAMAPEMIMPIN_1,
  JABATANPEMIMPIN_i,
  P2_JENIS,
  P2_LUAS,
  P2_BLOCK,
  P2_NOMOR,
  P2_LOKASI,
  P2_PENJUAL,
  P3_TANGGALKREDITBERAKHIR,
  P3_TANGGALSELAMBATNYA,
  P3_DENDAKETERLAMBATAN,
  P3_BIAYAADMIN,
  P4_TANGGALKREDITMULAI,
  P15_PENGADILANENGERI,
  P15_BADANPIUTANG,
  TEXT1,
  P4_REKTABUNGAN,
  P4_ATASNAMA,
  P4_REKPINJAMAN,
  P17_TARIFKPR,
  P4_TANGGALMULAIBAYAR
        )
        VALUES(
          \''.$custnomid.'\',
          \''.$nomorpk.'\',
          \''.$tanggalpk.'\',
          \''.$namapemimpin_1.'\',
          \''.$jabatanpemimpin_i.'\',
          \''.$p2_jenis.'\',
          \''.$p2_luas.'\',
          \''.$p2_block.'\',
          \''.$p2_nomor.'\',
          \''.$p2_lokasi.'\',
          \''.$p2_penjual.'\',
          \''.$p3_tanggalkreditberakhir.'\',
          \''.$p3_tanggalselambatnya.'\',
          \''.$p3_dendaketerlambatan.'\',
          \''.$p3_biayaadminpersen.'\',
          \''.$p4_tanggalkreditmulai.'\',
          \''.$p15_pengadilanengeri.'\',
          \''.$p15_badanpiutang.'\',
          \''.$txt_point1.'\',
          \''.$p4_rektabungan.'\',
          \''.$p4_atasnama.'\',
          \''.$p4_rekpinjaman.'\',
          \''.$p17_tarifkpr.'\',
		  \''.$p4_tanggalmulaibayar.'\'
        )';
}
else
{
$tsql = 'UPDATE PK_KGS set

CUSTNOMID=\''.$custnomid.'\',
NOMORPK=\''.$nomorpk.'\',
TANGGALPK=\''.$tanggalpk.'\',
NAMAPEMIMPIN_1=\''.$namapemimpin_1.'\',
JABATANPEMIMPIN_i=\''.$jabatanpemimpin_i.'\',
P2_JENIS=\''.$p2_jenis.'\',
P2_LUAS=\''.$p2_luas.'\',
P2_BLOCK=\''.$p2_block.'\',
P2_NOMOR=\''.$p2_nomor.'\',
P2_LOKASI=\''.$p2_lokasi.'\',
P2_PENJUAL=\''.$p2_penjual.'\',
P3_TANGGALKREDITBERAKHIR=\''.$p3_tanggalkreditberakhir.'\',
P3_TANGGALSELAMBATNYA=\''.$p3_tanggalselambatnya.'\',
P3_DENDAKETERLAMBATAN=\''.$p3_dendaketerlambatan.'\',
P3_BIAYAADMIN=\''.$p3_biayaadminpersen.'\',
P4_TANGGALKREDITMULAI=\''.$p4_tanggalkreditmulai.'\',
P15_PENGADILANENGERI=\''.$p15_pengadilanengeri.'\',
P15_BADANPIUTANG=\''.$p15_badanpiutang.'\',
TEXT1=\''.$txt_point1.'\',
P4_REKTABUNGAN=\''.$p4_rektabungan.'\',
P4_ATASNAMA=\''.$p4_atasnama.'\',
P4_REKPINJAMAN=\''.$p4_rekpinjaman.'\',
P17_TARIFKPR=\''.$p17_tarifkpr.'\',
P4_TANGGALMULAIBAYAR=\''.$p4_tanggalmulaibayar.'\'
        where
        CUSTNOMID=\''.$custnomid.'\'';
}
$db->_EQ($tsql);


require ("../../requirepage/do_saveflow.php");
header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");


?>
