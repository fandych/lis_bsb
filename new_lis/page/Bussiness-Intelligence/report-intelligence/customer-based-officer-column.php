<?php
include("../../../requirepage/session.php");
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");
require_once("../../../lib/class.sqlserver.php");
$sql = new SQLSRV();
$sql->connect();


$paramSyntax = "SELECT DISTINCT(txn_user_id) AS officer FROM tbl_FFAK ";
$sql->executeQuery($paramSyntax);
$paramResult = $sql->lastResults;
$paramCount = count($paramResult);

if($sql->debug == true){
/*echo $paramSyntax;
echo "<pre>";
print_r($paramResult);
echo "</pre>";*/
}



//GETDATA
if(isset($_POST['offopt'])){
	
	//echo "IF<br />";
	//print_r($_POST['offopt']);
	$getOption = $_POST['offopt'];
}
else{
	
	$getOption = array();
	for($x=0;$x<$paramCount;$x++){
		$paramCode = $paramResult[$x]['officer'];
		//$paramName = $paramResult[$x]['name'];
		array_push($getOption,$paramCode);
	}
	//echo "ELSE<br />";
	//print_r($getOption);
}

$totalOption = count($getOption);


$yeslike .= "WHERE ";
for($x=0;$x<$totalOption;$x++){
	$like = $getOption[$x];
	$yeslike .= "txn_user_id = '$like' OR ";
}
$yeslike = substr($yeslike,0,-3);

$officerSyntax = "SELECT DISTINCT(txn_user_id)AS officer FROM tbl_FFAK $yeslike";
$sql->executeQuery($officerSyntax);
$officerResult = $sql->lastResults;
$officerCount = count($officerResult);

if($sql->debug == true){
/*echo $officerSyntax;
echo "<pre>";
print_r($officerResult);
echo "</pre>";*/
}

$arrayTemp = array();
for($x=0;$x<$officerCount;$x++){
	$color = randomColorOne();
	$officerCode = $officerResult[$x]['officer'];
	//$officerName = $officerResult[$x]['name'];
	$dataSyntax = "SELECT COUNT(*) AS total, '$officerCode' AS officer,'$color' AS color FROM tbl_FFAK WHERE txn_user_id = '$officerCode'";
	$sql->executeQuery($dataSyntax);
	$dataResult = $sql->lastResults;
	$dataCount = count($dataResult);
	
	array_push($arrayTemp,$dataResult[0]);
	
	if($sql->debug == true){
	/*echo $dataSyntax;
	echo "<pre>";
	print_r($dataResult);
	echo "</pre>";*/
	}
}

//echo "<pre>";
//print_r($arrayTemp);
//echo "</pre>";

$totalData = count($arrayTemp);
$json =  json_encode($arrayTemp);

function randomColorOne() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 


function randomColorTwo() { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
    shuffle($possibilities);
    $color = "#";
    for($i=1;$i<=6;$i++){
        $color .= $possibilities[rand(0,14)];
    }
    return $color;
} 


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Total Customer Report</title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>
<script src="../amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
var chart;

var chartData = <?=$json;?>;

AmCharts.ready(function () {
	// SERIAL CHART
	chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "officer";
	// the following two lines makes chart 3D
	chart.depth3D = 20;
	chart.angle = 30;

	// AXES
	// category
	var categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 90;
	categoryAxis.dashLength = 5;
	categoryAxis.gridPosition = "start";

	// value
	var valueAxis = new AmCharts.ValueAxis();
	valueAxis.title = "Total Customer Based on Officer";
	valueAxis.dashLength = 5;
	chart.addValueAxis(valueAxis);

	// GRAPH
	var graph = new AmCharts.AmGraph();
	graph.valueField = "total";
	graph.colorField = "color";
	graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	graph.type = "column";
	graph.lineAlpha = 0;
	graph.fillAlphas = 1;
	chart.addGraph(graph);

	// CURSOR
	var chartCursor = new AmCharts.ChartCursor();
	chartCursor.cursorAlpha = 0;
	chartCursor.zoomable = false;
	chartCursor.categoryBalloonEnabled = false;
	chart.addChartCursor(chartCursor);

	chart.creditsPosition = "top-right";

	// WRITE
	chart.write("chartdiv");
});
</script>
<script>
function refreshData()
{
	$("#formreport").submit();
}
</script>
</head>
<body style="padding:0px;margin:0px;">
<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
<div id="chartdiv" style="width:<?=($totalData*55);?>px; height:450px;"></div>
<div id="parameter">
<h1 style="margin:0; margin-top:10px; padding:0; padding-left:25px; padding-bottom:10px; font-family:sans-serif;">Option</h1>
<div style="background:#444; color:#fafafa; padding:10px;">
<form name="formreport" id="formreport" method="post" action="#">
<table>

<tr>
<?php
for($x=0;$x<$paramCount;$x++){
$paramCode = $paramResult[$x]['officer'];
//$paramName = $paramResult[$x]['name'];
if(in_array($paramCode,$getOption)){$checked="checked=\"checked\"";}else{$checked="";}
if ($x % 5 === 0) { ?></tr><tr><? }?>
<td><input type="checkbox" name="offopt[]" onchange="refreshData();" id="<?=$paramCode;?>" value="<?=$paramCode;?>" class="css-checkbox" <?=$checked;?> /><label for="<?=$paramCode;?>" class="css-label">&nbsp;<?=strtoupper($paramCode);?>&nbsp;</label></td>
<? }?>
</tr>
</table>
</form>
</div>
<p style="padding-left:25px;">Property of <a href="http://spin-international.com">SPIN-International</a></p>
</div>
</body>
</html>