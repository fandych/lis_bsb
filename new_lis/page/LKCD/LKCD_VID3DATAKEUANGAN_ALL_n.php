<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../requirepage/parameter.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Expires" CONTENT="0">
		<meta http-equiv="Cache-Control" CONTENT="no-cache">
		<meta http-equiv="Pragma" CONTENT="no-cache">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LKCD 3</title>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			function validation()
			{
				var FormName="formentry";	
				var StatusAllowSubmit=true;
				var elem = document.getElementById(FormName).elements;
				for(var i = 0; i < elem.length; i++)
				{
					if(elem[i].style.backgroundColor=="#ff0")
					{
						
						if(elem[i].value == "")
						{
							alert(elem[i].nai + "HARUS DIISI");
							elem[i].focus();
							StatusAllowSubmit=false				
							break;
						}
					}
				}
				
				if(StatusAllowSubmit == true)
				{			
					document.getElementById(FormName).action = "DO_LKCD_VID3DATAKEUANGAN_ALL_n.php";
					submitform = window.confirm("Save?")
					if (submitform == true)
					{
						document.getElementById(FormName).submit();
						return true;
					}
					else
					{
						return false;
					} 
				}
			}
		</script>
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">
			<div>
				<table border="0" align="center" style="border:1px solid black; width:900px;">
					<tr>
						<td colspan="2" align="center">INFROMSI KEUANGAN (IDR)</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">MODAL KERJA</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where (a.gl_code like '%AT1%' or  a.gl_code like '%AL1%' or a.gl_code like '%HT0%')
											and group_bs_is = 'bs'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">KAS DAN BANK</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where (a.gl_code like '%KB0%')
											and c.group_bs_is = 'BS'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">AKTIFA TETAP</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where a.gl_code like '%AT0%' or a.gl_code like '%AT2%'
											and c.group_bs_is = 'BS'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												echo  numberFormat($value)."   RUPIAH";
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2" align="center">INCOME STATEMENT</td>
								</tr>
								<tr>
									<td colspan="2">BIAYA OPERASIONAL USAHA</td>
								</tr>
								<?
								$strsql = " select a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo b on a.gl_code = b.gl_code
											where (a.gl_code like '%BY0%')";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
												<?echo $rows['gl_name']?>
											</td>
											<td>
												<? echo  numberFormat($value)."   RUPIAH"; ?>
											</td>
										</tr>
									<?
									}
								}
								?>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
				<table border="0" align="center" style="border:1px solid black; width:900px;">
					<tr>
						<td colspan="2" align="center">INFROMSI KEUANGAN (IDR)</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">MODAL KERJA</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo2 b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where (a.gl_code like '%AT1%' or  a.gl_code like '%AL1%' or a.gl_code like '%HT0%')
											and group_bs_is = 'bs'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">KAS DAN BANK</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo2 b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where (a.gl_code like '%KB0%')
											and c.group_bs_is = 'BS'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2">AKTIFA TETAP</td>
								</tr>
								<?
								$strsql = " select a.gl_db_cr,a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo2 b on a.gl_code = b.gl_code
											join GL_Group c on c.group_code = left(b.gl_code,2)
											where a.gl_code like '%AT0%' or a.gl_code like '%AT2%'
											and c.group_bs_is = 'BS'";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										$db_cr=$rows['gl_db_cr'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
											<?
												if ($db_cr=="db")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="db")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
												<?
												if ($db_cr=="cr")
												{
												echo $rows['gl_name'];
												}
												else{ echo '&nbsp';}
											?>
											</td>
											<td>
											<?
												if ($db_cr=="cr")
												{
												?>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH&nbsp;
												<?
												}
												else{ echo '&nbsp';}
											?>
											</td>
										</tr>
									<?
									}
								}
								?>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2" valign="top" style="width:100%;">
							<table border="0" style="width:100%;">
								<tr>
									<td colspan="2" align="center">INCOME STATEMENT</td>
								</tr>
								<tr>
									<td colspan="2">BIAYA OPERASIONAL USAHA</td>
								</tr>
								<?
								$strsql = " select a.gl_code,a.gl_name,b.gl_amount_cr,b.gl_amount_db from GL_Master a
											join GL_TxnSaldo2 b on a.gl_code = b.gl_code
											where (a.gl_code like '%BY%')";
								$sqlcon = sqlsrv_query($conn, $strsql);
								if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon))
								{
									while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
									{
										$cr=$rows['gl_amount_cr'];
										$db=$rows['gl_amount_db'];
										if($db=="0")
										{
											$value=$cr;
										}
										else if($cr=="0")
										{
											$value=$db;
										}
									?>
										<tr>
											<td style="width:200px;">
												<?echo $rows['gl_name']?>
											</td>
											<td>
												<input value="<?echo  numberFormat($value);?>" nai="<?echo $rows['gl_name']?> " maxlength="18" style="background:#ff0; width:150px;" onkeyup="currency(this.id)" type="text" name="<?echo $rows['gl_code']?>" id="<?echo $rows['gl_code']?>" />   RUPIAH
											</td>
										</tr>
									<?
									}
								}
								?>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="validation()" /></td>
					</tr>
				</table>
				<?
					require ("../../requirepage/hiddenfield.php");
				?>
			</div>
		</form>
	</body>
</html>
