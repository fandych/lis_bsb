<?
require_once ("../../lib/class.sqlserver.php");
require_once('../../requirepage/parameter.php');
$db = new SQLSRV();
$db->connect();

$tsql = "SELECT * FROM tbl_customermasterperson2 WHERE custnomid = '$custnomid'";
$b = sqlsrv_query($conn, $tsql);
if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($b))
{ 
	if($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
	{
		$custfullname = $rowType['custfullname'];
		$custhp = $rowType['custhp'];
	}
}

if($jeniscol=="V01"){
	$bagianA = "Kendaraan";
	
	$cust_info = "select * from tbl_COL_Vehicle where col_id = '$col_id'";
	$db->executeQuery($cust_info);
	$cust_info = $db->lastResults;
	foreach($cust_info[0] as $key=>$value)
	{
		$$key = $value;
	}
}else{
	$bagianA = "Bangunan rumah tinggal yang berdiri di atas tanah";
	
	$cust_info = "select * from tbl_COL_Building where col_id = '$col_id'";
	$db->executeQuery($cust_info);
	$cust_info = $db->lastResults;
	foreach($cust_info[0] as $key=>$value)
	{
		$$key = $value;
	}

	$cust_info = "select * from tbl_COL_Land where col_id = '$col_id'";
	$db->executeQuery($cust_info);
	$cust_info = $db->lastResults;
	foreach($cust_info[0] as $key=>$value)
	{
		$$key = $value;
	}
}

$cust_info = "select * from tbl_col_kjpp where col_id = '$col_id'";
$db->executeQuery($cust_info);
$cust_info = $db->lastResults;
foreach($cust_info[0] as $key=>$value)
{
	$$key = $value;
}
?>

<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  <div align="center" style="border:1px solid black;" id="printableArea">
 <table style="width: 95%; height: 216px; margin-right: auto; margin-left: auto;">
<tbody>
<tr style="height: 184.68px;">
<td style="width: 53px; height: 184.68px;">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Nomor</span></p>
<p><span style="font-size: 10pt;">Lampiran</span></p>
<p><span style="font-size: 10pt;">Perihal</span></p>
</td>
<td style="width: 13.43px; height: 184.68px;">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">:</span></p>
<p><span style="font-size: 10pt;">:</span></p>
<p><span style="font-size: 10pt;">:</span></p>
</td>
<td style="width: 534.56px; height: 184.68px;">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;"><?=$kjpp_nomor;?></span></p>
<p><span style="font-size: 10pt;">1 (Satu) Set</span></p>
<p><span style="font-size: 10pt;"><strong>Permintaan Penilaian Aset Calon Debitur</strong></span></p>
</td>
<td style="width: 425px; height: 184.68px;">
<p><span style="font-size: 10pt;"><?=$kjpp_tanggal;?></span></p>
<p><span style="font-size: 10pt;">&nbsp;</span></p>
<p><span style="font-size: 10pt;">Kepada Yth,</span></p>
<p><span style="font-size: 10pt;"><?=$kjpp_yth;?></span></p>
<p><span style="font-size: 10pt;">di-</span></p>
<p><span style="font-size: 10pt;"><?=$kjpp_tempat;?></strong></span></p>
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>
<table style="height: 29px; margin-right: auto; margin-left: auto;" width="867">
<tbody>
<tr>
<td style="width: 857px;"><span style="font-size: 10pt;">Dengan Hormat,</span></td>
</tr>
</tbody>
</table>
<table style="width: 871px; height: 77px; margin-right: auto; margin-left: auto;">
<tbody>
<tr>
<td style="width: 861px;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Sehubungan dengan permohonan Kredit Griya Sejahtera An.<?=$custfullname;?> (No.HP:<?=$custhp;?>), maka dengan ini kami sampaikan hal-hal sebagai berikut:</span> </span></td>
</tr>
</tbody>
</table>
<table style="width: 866px; height: 12px; margin-right: auto; margin-top:-20px; margin-left: auto;">
<tbody>
<tr>
<td style="width: 22.86px;"><span style="font-size: 10pt;">1.</span></td>
<td style="width: 830.13px;"><span style="font-size: 10pt;">Mohon bantuan Saudara untuk dapat melakukan penilaian aset pemohon dengan rincian sebagai berikut:</span></td>
</tr>
</tbody>
</table>
<?if($jeniscol=="V01" ){
?>
<table style="width: 803px; height: 23px; margin-right: auto; margin-left: auto;">
<tbody>
<tr>
<td style="width: 34.3px;"><span style="font-size: 10pt;">&nbsp;a.</span></td>
<td style="width: 752.7px;"><span style="font-size: 10pt;"><?=$bagianA;?> </span></td>
</tr>
</tbody>
</table>
<table style="width: 722px; height: 62px; margin-right: auto; margin-left: auto;">
<tbody>
<tr style="height: 11.73px;">
<td style="width: 95.19px; height: 11.73px;">
<p><span style="font-size: 10pt;">No polisi</span></p>
</td>
<td style="width: 611.8px; height: 11.73px;"><span style="font-size: 10pt;">: <?=$col_nopol;?></span></td>
</tr>
<tr style="height: 13px;">
<td style="width: 95.19px; height: 13px;">
<p><span style="font-size: 10pt;">No STNK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
</td>
<td style="width: 611.8px; height: 13px;"><span style="font-size: 10pt;">: <?=$col_stnk_no;?></span></td>
</tr>
<tr style="height: 13px;">
<td style="width: 95.19px; height: 13px;">
<p><span style="font-size: 10pt;">No Faktur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
</td>
<td style="width: 611.8px; height: 13px;"><span style="font-size: 10pt;">: <?=$col_fakturno;?></span></td>
</tr>
<tr style="height: 13px;">
<td style="width: 95.19px; height: 13px;">
<p><span style="font-size: 10pt;">No BPKB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
</td>
<td style="width: 611.8px; height: 13px;"><span style="font-size: 10pt;">: <?=$col_bpkbno;?></span></td>
</tr>
</tbody>
</table>
<?	
}else{
?>
<table style="width: 803px; height: 23px; margin-right: auto; margin-left: auto;">
<tbody>
<tr>
<td style="width: 34.3px;"><span style="font-size: 10pt;">&nbsp;a.</span></td>
<td style="width: 752.7px;"><span style="font-size: 10pt;"><?=$bagianA;?> </span></td>
</tr>
</tbody>
</table>
<table style="width: 722px; height: 62px; margin-right: auto; margin-left: auto;">
<tbody>
<tr style="height: 11.73px;">
<td style="width: 95.19px; height: 11.73px;">
<p><span style="font-size: 10pt;">No Sertifikat</span></p>
</td>
<td style="width: 611.8px; height: 11.73px;"><span style="font-size: 10pt;">: <?=$col_certtype;?> No.<?=$col_certno;?> Tgl.<?=$col_certdate->format('d-m-Y');?></span></td>
</tr>
<tr style="height: 13px;">
<td style="width: 95.19px; height: 13px;">
<p><span style="font-size: 10pt;">No SU&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
</td>
<td style="width: 611.8px; height: 13px;"><span style="font-size: 10pt;">: <?=$col_gssuno;?> Tgl.<?=$col_gssutgl->format('d-m-Y');?></span></td>
</tr>
<tr style="height: 13px;">
<td style="width: 95.19px; height: 13px;">
<p><span style="font-size: 10pt;">No IMB&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
</td>
<td style="width: 611.8px; height: 13px;"><span style="font-size: 10pt;">: <?=$col_imbno;?> Tgl.<?=$col_imbdate->format('d-m-Y');?></span></td>
</tr>
</tbody>
</table>
<?}?>
<table style="width: 870px; height: 24px; margin-right: auto; margin-left: auto;">
<tbody>
<tr style="height: 13.76px;">
<td style="width: 23.33px; height: 13.76px;"><span style="font-size: 10pt;">2.</span></td>
<td style="width: 834.66px; height: 13.76px;"><span style="font-size: 10pt;">Selanjutnya, hasil penilaian aset dimaksud agar dapat disampaikan kepada <strong>Satuan Kredit Konsumen</strong> pada kesempatan pertama.<br /></span></td>
</tr>
</tbody>
</table>
<table style="height: 42px; margin-right: auto; margin-left: auto;" width="871">
<tbody>
<tr>
<td style="width: 861px;"><span style="font-size: 10pt;">Demikian kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table style="height: 42px; margin-right: auto; margin-left: auto;" width="873">
<tbody>
<tr>
<td style="width: 863px;"><span style="font-size: 10pt;">PT Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung</span></td>
</tr>
<tr>
<td style="width: 863px;"><span style="font-size: 10pt;">Satuan Kredit Konsumen,</span></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table style="height: 43px; margin-right: auto; margin-left: auto;" width="869">
<tbody>
<tr>
<td style="width: 859px;"><span style="font-size: 10pt;"><strong><u><?=$kjpp_pemimpin;?></u></strong></span></td>
</tr>
<tr>
<td style="width: 859px;"><span style="font-size: 10pt;">Pemimpin</span></td>
</tr>
</tbody>
</table>
<input type="hidden" id="custnomid" name="custnomid" value="<?=$custnomid;?>"/>
<input type="hidden" id="col_id" name="col_id" value="<?=$col_id;?>"/>

</div>

<div align="center"><p><input type="button" class="blue" onclick="printDiv('printableArea')" value="PRINT" /></p></div>
  </body>
</html>

<script type="text/javascript">
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
