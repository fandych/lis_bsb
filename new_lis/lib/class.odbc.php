<?php

class DB_ODBC
{
	var $count;
	var $result;
	var $connection;


	function connect($host,$uid,$pwd)
	{
		$this->connection = odbc_connect($host,$uid,$pwd);

		if (!($this->connection))
		{
			$this->declare_msg("Invalid Connection to Database",odbc_errormsg());
		}
	}

	function queryNoResult($syntax)
	{
		$result = odbc_exec($this->connection, $syntax);

		if($result === false)
		{
			$this->declare_msg("Invalid Query :", $syntax, odbc_errormsg());
		}
	}

	function queryWithResult($syntax)
	{
		$result = odbc_exec($this->connection, $syntax);

		if($result === false)
		{
			$this->declare_msg("Invalid Query :", $syntax, odbc_errormsg());
		}

		$this->count = odbc_num_rows($result);
		$this->result = array();

		if(odbc_num_rows($result) > 0)
		{
			while($row = odbc_fetch_array($result))
			{
				$this->result[] = $row;
			}
		}
	}

	function getAllTables()
	{

	}

	function declare_msg($cap,$msg,$desc)
	{
		echo "<style>";
		echo "body";
		echo "{";
		echo "padding:0px;";
		echo "margin:0px;";
		echo "}";
		echo "</style>";
		echo "<div style=\"width:100%;background-color:#0099FF\"><h3><font style=\"color:#FFFFFF\">$cap</h3></font></div>";
		echo "<div>";
		echo "<pre>";
		print_r($msg);
		echo "</pre>";
		echo "<pre>";
		print_r($desc);
		echo "</pre>";
		echo "</div>";
		exit();
	}

}


?>
