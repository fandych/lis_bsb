<?php
ob_start();
session_start();

$server = $_SERVER['DOCUMENT_ROOT'];

$viewstatus= isset($_GET['view']) ? $_GET['view'] : "";

require_once($server."/LIS_BSB/sqlsrv.security.php");
$db_security = new DB_SECURITY();
$db_security->connect();

require_once($server."/LIS_BSB/new_lis/lib/sqlsrv.lis.php");
$db_lis = new DB_LIS();
$db_lis->connect();

$get_idle_param = "SELECT * FROM MS_CONTROL WHERE CONTROL_CODE = 'IDLE'";
$db_lis->executeQuery($get_idle_param);
$get_idle_param = $db_lis->lastResults;

$minutes = $get_idle_param[0]['CONTROL_VALUE'];

//echo $_SESSION['90272dda245ae1fb3cf197e91a8689dc'];exit;

if(isset($_SESSION['e91e6348157868de9dd8b25c81aebfb9']))
{
	if ($_SESSION['90272dda245ae1fb3cf197e91a8689dc'] + $minutes * 60 < time())
	{
		// session timed out
		$errmsg = "Your%20session%20has%20expired.";

		$update_stat = "UPDATE se_user SET user_signin = '0'
		WHERE LOWER(user_id) = '$userid' AND user_pwd = '$userpwd'
		";
		$db_security->executeNonQuery($update_stat);

		session_destroy();
		header("location:/LIS_BSB/index.php?errmsg=$errmsg");
	}
	else
	{
		// session ok

		$userid = $_SESSION['e8701ad48ba05a91604e480dd60899a3'];
        $userbranch = $_SESSION['3feb759615ff626be3c1e36521f2df28'];
        $userregion = $_SESSION['36a449e73c5d9e5e32437c8b4d65c361'];
        $userpwd = $_SESSION['20b5ef967d3a4e8b21c2d5659cbcb144'];

		$time = time();

		$_SESSION['90272dda245ae1fb3cf197e91a8689dc'] = $time;

		$update_time = "UPDATE se_user SET user_signin_time = '$time'
	    WHERE LOWER(user_id) = '$userid' AND user_pwd = '$userpwd'
	    ";
		$db_security->executeNonQuery($update_time);
	}
}
else
{
	$errmsg = "Please%20login%20to%20access%20this%20page.";
	session_destroy();
	header("location:/LIS_BSB/index.php?errmsg=$errmsg");
}

$get_info_user = "SELECT a.user_name, b.branch_name, c.level_name FROM Tbl_SE_User a JOIN Tbl_Branch b ON a.user_branch_code = b.branch_code JOIN Tbl_SE_Level c ON a.user_level_code = c.level_code WHERE user_id = '$userid'";
$db_lis->executeQuery($get_info_user);
$get_info_user = $db_lis->lastResults;

$username = $get_info_user[0]['user_name'];
$branchname = $get_info_user[0]['branch_name'];
$levelname = $get_info_user[0]['level_name'];

//echo "Ini view status :: ".$_SESSION['BROWSER'];

if($viewstatus!="preview" && $_SESSION['BROWSER'] == "BROWSER")
{
	include($server."/LIS_BSB_PRODUKTIF/new_lis/requirepage/footer.php");
}

?>
