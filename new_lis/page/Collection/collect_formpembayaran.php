<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	/*$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	*/
	
	date_default_timezone_set('Asia/Jakarta');
	$date = date('d/m/Y', time());
	$datemonthbefore = "";
	
	$tsql = "SELECT replace(convert(NVARCHAR, DATEADD(MONTH, -1, GETDATE()), 103), ' ', '/') as now";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	$rowCount = sqlsrv_num_rows($sqlConn);
	
	if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($sqlConn))
	{
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$datemonthbefore = $row['now'];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FORM PEMBAYARAN</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="formentry" name="formentry" method="post">
<table width="1000px" align="center" style="border:1px solid black;" class="input" border="0">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;font-size:20px;line-height:60px;">Form Pembayaran</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" align="center">
		Masukkan Custnomid : <input type="text" id="custnomid" name="custnomid" style="width:200px;"/>
	</td>
</tr>
<tr>
	<td colspan="2" align="center" style="line-height:60px;">
		<input type="button" id="buttonsubmit" name="buttonsubmit" style="width:200px;" value="Submit" class="blue" onclick="save()"/>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="sugananako" align="center"></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
</form>

<script type="text/javascript">
	function save() {
		var nan="SUBMIT";
		var custnomid = $("#custnomid").val();
		
		//alert (id);
		/*var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		*/
		$.ajax({
			type: "POST",
			url: "collect_ajax.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	//alert(response);
				$("#sugananako").html(response);
			}
		});
	}
	
	function bayar(seq, angsuran) {
		var nan="BAYAR";
		var custnomid = $("#custnomid").val();
		var seq = seq;
		var angsuran = angsuran;
		
		//alert (custnomid);
		/*var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		*/
		
		var StatusAllowSubmit=true;
		if(StatusAllowSubmit == true)
		{	
			submitform = window.confirm("<? echo $confmsg;?>")
			if (submitform == true)
			{	
				$.ajax({
					type: "POST",
					url: "collect_ajax.php",
					data: "nan="+nan+"&custnomid="+custnomid+"&seq="+seq+"&angsuran="+angsuran+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{	//alert(response);
						$("#sugananako").html(response);
					}
				});
			}
		}
	}
</script>
</body>
</html> 