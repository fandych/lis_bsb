<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");
require ("../../requirepage/security.php");

//echo "<pre>";
//print_r($_REQUEST);
//echo "</pre>";

if($userpermission == "I")
{
    $button_show = "Input";
}
else if ($userpermission == "C")
{
    $button_show = "Check";
}
else if ($userpermission == "A")
{
    $button_show = "Approve";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>FORM NOT AVAILABLE</title>
    </head>
    <body>
        <div align="center">
        <hr>
        <h3>FORM NOT AVAILABLE</h3>
        <hr>

            <form method="post" action="process_no_form.php">
                <?
                require ("../../requirepage/hiddenfield.php");
                ?>
                <input type="submit" id="btnsave" name="btnsave" value="<?=$button_show;?>" class="buttonsaveflow" />
            </form>
        </div>


    </body>
</html>
