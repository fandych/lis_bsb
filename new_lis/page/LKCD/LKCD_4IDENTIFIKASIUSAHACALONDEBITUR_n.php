<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$usaha_sektorindustri = "";
	$usaha_jenisusaha = "";
	$usaha_lamausahamonth = "";
	$usaha_lamausahayear = "";
	$usaha_kepemilikanusaha = "";
	$usaha_jumlahpenjualanweek = "";
	$usaha_jumlahpenjualanmonth = "";
	$usaha_jumlahpenjualanyear = "";
	$usaha_ratajumlahpembeli = "";
	$usaha_ratapembelian = "";
	$usaha_kenaikanjumlahpembeli = "";
	$usaha_kenaikanticketsize = "";
	$usaha_hargapokokpenjualanrupiah = "";
	$usaha_hargapokokpenjualanpersen = "";
	$usaha_persentasemarginusaha = "";
	$usaha_biayagajipegawai = "";
	$usaha_gajiratapegawai = "";
	$usaha_jumlahkaryawan = "";
	$usaha_biayasewa = "";
	$usaha_biayatelpon = "";
	$usaha_biayasampah = "";
	$usaha_biayaoperasionallain = "";
	$usaha_biayarumahtangga = "";
	
	$tsql = "select * from Tbl_LKCDUsaha where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$usaha_sektorindustri = $row["usaha_sektorindustri"];
			$usaha_jenisusaha = $row["usaha_jenisusaha"];
			$usaha_lamausahamonth = $row["usaha_lamausahamonth"];
			$usaha_lamausahayear = $row["usaha_lamausahayear"];
			$usaha_kepemilikanusaha = $row["usaha_kepemilikanusaha"];
			$usaha_jumlahpenjualanweek = $row["usaha_jumlahpenjualanweek"];
			$usaha_jumlahpenjualanmonth = $row["usaha_jumlahpenjualanmonth"];
			$usaha_jumlahpenjualanyear = $row["usaha_jumlahpenjualanyear"];
			$usaha_ratajumlahpembeli = $row["usaha_ratajumlahpembeli"];
			$usaha_ratapembelian = $row["usaha_ratapembelian"];
			$usaha_kenaikanjumlahpembeli = $row["usaha_kenaikanjumlahpembeli"];
			$usaha_kenaikanticketsize = $row["usaha_kenaikanticketsize"];
			$usaha_hargapokokpenjualanrupiah = $row["usaha_hargapokokpenjualanrupiah"];
			$usaha_hargapokokpenjualanpersen = $row["usaha_hargapokokpenjualanpersen"];
			$usaha_persentasemarginusaha = $row["usaha_persentasemarginusaha"];
			$usaha_biayagajipegawai = $row["usaha_biayagajipegawai"];
			$usaha_gajiratapegawai = $row["usaha_gajiratapegawai"];
			$usaha_jumlahkaryawan = $row["usaha_jumlahkaryawan"];
			$usaha_biayasewa = $row["usaha_biayasewa"];
			$usaha_biayatelpon = $row["usaha_biayatelpon"];
			$usaha_biayasampah = $row["usaha_biayasampah"];
			$usaha_biayaoperasionallain = $row["usaha_biayaoperasionallain"];
			$usaha_biayarumahtangga = $row["usaha_biayarumahtangga"];
		}
	}
	
	$custsex = "";
	
	$tsql = "select * from tbl_customermasterperson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$custsex = $row["custsex"];
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
	
	function yuria(id)
	{
		var rupiah = document.getElementById(id).value;
		
		if(rupiah == "" || rupiah == "0")
		{
			document.all.formentry.usaha_hargapokokpenjualanrupiah.style.background = '#ff0';
			document.all.formentry.usaha_hargapokokpenjualanpersen.style.background = '#ff0';
		}
		else 
		{
			document.all.formentry.usaha_hargapokokpenjualanrupiah.style.background = '#ff0';
			document.all.formentry.usaha_hargapokokpenjualanpersen.style.background = '#fff';
		}
	}
	
	function yuria2(id)
	{
		var persen = document.getElementById(id).value;
		
		if(persen == "" || persen == "0")
		{
			document.all.formentry.usaha_hargapokokpenjualanrupiah.style.background = '#ff0';
			document.all.formentry.usaha_hargapokokpenjualanpersen.style.background = '#ff0';
		}
		else 
		{
			document.all.formentry.usaha_hargapokokpenjualanrupiah.style.background = '#fff';
			document.all.formentry.usaha_hargapokokpenjualanpersen.style.background = '#ff0';
		}
	}
</script>
</head>
<body>
<form id="formentry" name="formentry" method="post">
<table width="100%" align="center" style="border:1px solid black;">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">IDENTIFIKASI DATA-DATA USAHA CALON DEBITUR</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<?
	$tsql = "SELECT * FROM Tbl_SektorIndustri";
	$b = sqlsrv_query($conn, $tsql);
	if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($b))
	{ 
?>

<tr>
	<td width="40%">Segmentasi</td>
	<td width="60%" style="font-size:10px;">
		<select id="usaha_sektorindustri" name="usaha_sektorindustri"  nai="Segmentasi " class="harus" style="background:#FF0;">
			<option value="" selected="selected">- Pilih -</option>
			<?
				While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
				{
					$varselected = "";
					if($usaha_sektorindustri == $rowType["sektor_code"])
					{
						$varselected = "selected";
					}
			?>	
					<option value="<? echo $rowType["sektor_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["sektor_nama"];?></option>
			<?
				}
			?>
		</select>
	</td>
</tr>
<?
	}
?>
<tr>
	<td>Jenis Usaha</td>
	<td><input type="text" id="usaha_jenisusaha" style="width:200px;background:#FF0;" nai="Jenis Usaha " value="<? echo $usaha_jenisusaha;?>"  maxlength="50"/></td>
</tr>
<tr>
	<td>Lama Usaha</td>
	<td>
		<input type="text" id="usaha_lamausahamonth" style="width:50px;background:#FF0" nai="Lama Usaha Bulan "value="<? echo $usaha_lamausahamonth;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> BULAN
		<input type="text" id="usaha_lamausahayear" style="width:50px;background:#FF0" nai="Lama Usaha Tahun "value="<? echo $usaha_lamausahayear;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> TAHUN
	</td>
</tr>
<?
	if($custsex == "0")
	{
?>
	<?
		$tsql = "SELECT * FROM TblStatusRumah Where status_code like 'B%'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
	?>

	<tr>
		<td width="35%">Segmentasi</td>
		<td width="65%" style="font-size:10px;">
			<select id="usaha_kepemilikanusaha" name="usaha_kepemilikanusaha"  nai="Segmentasi " class="harus" style="background:#FF0;">
				<option value="" selected="selected">- Pilih -</option>
				<?
					While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$varselected = "";
						if($usaha_kepemilikanusaha == $rowType["status_code"])
						{
							$varselected = "selected";
						}
				?>	
						<option value="<? echo $rowType["status_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["status_name"];?></option>
				<?
					}
				?>
			</select>
		</td>
	</tr>
	<?
		}
	
	}
	else if ($custsex == "1" || $custsex == "2")
	{
?>
<?
		$tsql = "SELECT * FROM TblStatusRumah Where status_code like 'P%'";
		$b = sqlsrv_query($conn, $tsql);
		if ( $b === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($b))
		{ 
	?>

	<tr>
		<td width="35%">Segmentasi</td>
		<td width="65%" style="font-size:10px;">
			<select id="usaha_kepemilikanusaha" name="usaha_kepemilikanusaha"  nai="Segmentasi " class="harus" style="background:#FF0;">
				<option value="" selected="selected">- Pilih -</option>
				<?
					While($rowType = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC))
					{
						$varselected = "";
						if($usaha_kepemilikanusaha == $rowType["status_code"])
						{
							$varselected = "selected";
						}
				?>	
						<option value="<? echo $rowType["status_code"]; ?>" <? echo $varselected; ?>><? echo $rowType["status_name"];?></option>
				<?
					}
				?>
			</select>
		</td>
	</tr>
<?
		}
	}
?>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>IDENTIFIKASI DATA - DATA KEUANGAN CALON DEBITUR</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Identifikasi Omset penjualan usaha</strong></td>
</tr>
<tr>
	<td colspan="2">Jumlah hari kerja usaha/ jumlah hari penjualan (isi salah satu):</td>
</tr>
<tr>
	<td>a. Jumlah penjualan dalam 1 minggu</td>
	<td><input type="text" id="usaha_jumlahpenjualanweek" style="width:200px;background:#FF0;" nai="Jumlah penjualan dalam 1 minggu " value="<? echo $usaha_jumlahpenjualanweek;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> Kali dalam 1 minggu</td>
</tr>
<tr>
	<td>b. Jumlah penjualan dalam 1 bulan</td>
	<td><input type="text" id="usaha_jumlahpenjualanmonth" style="width:200px;background:#FF0;" nai="Jumlah penjualan dalam 1 bulan " value="<? echo $usaha_jumlahpenjualanmonth;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> Kali dalam 1 bulan</td>
</tr>
<tr>
	<td>c. Jumlah penjualan dalam 1 tahun</td>
	<td><input type="text" id="usaha_jumlahpenjualanyear" style="width:200px;background:#FF0;" nai="Jumlah penjualan dalam 1 tahun " value="<? echo $usaha_jumlahpenjualanyear;?>"  maxlength="10" onKeyPress="return isNumberKey(event)"/> Kali dalam 1 tahun</td>
</tr>
<tr>
	<td>Rata-rata jumlah pembeli/pemesanan per-penjualan</td>
	<td><input type="text" id="usaha_ratajumlahpembeli" style="width:200px;background:#FF0;" nai="Rata - rata jumlah pembeli/pemesanan per-penjualan " value="<? echo $usaha_ratajumlahpembeli;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"/> Customer</td>
</tr>
<tr>
	<td>Rata-rata pembelian/belanja per-customer/Ticket size</td>
	<td><input type="text" id="usaha_ratapembelian" style="width:200px;background:#FF0;" nai="Rata - rata pembelian/belanja per-customer/Ticket size " value="<? echo $usaha_ratapembelian;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_ratapembelian')"/> Rupiah</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Estimasi Proyeksi Omset penjualan usaha</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Kenaikan jumlah pembeli/pemesan</td>
	<td><input type="text" id="usaha_kenaikanjumlahpembeli" style="width:200px;background:#FF0;" nai="Kenaikan jumlah pembeli/pemesan " value="<? echo $usaha_kenaikanjumlahpembeli;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"/> %</td>
</tr>
<tr>
	<td>Kenaikan Ticket size</td>
	<td><input type="text" id="usaha_kenaikanticketsize" style="width:200px;background:#FF0;" nai="Kenaikan Ticket size " value="<? echo $usaha_kenaikanticketsize;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"/> %</td>
</tr>
<tr>
	<td>Harga Pokok Penjualan (isi salah satu dlm Rupiah atau persentase)</td>
	<td><input type="text" id="usaha_hargapokokpenjualanrupiah" style="width:200px;background:#FF0;" nai="Harga Pokok Penjualan " value="<? echo $usaha_hargapokokpenjualanrupiah;?>"  maxlength="20" onBlur="yuria('usaha_hargapokokpenjualanrupiah')" onKeyPress="return isNumberKey(event)" onKeyUp="outputMoney('usaha_hargapokokpenjualanrupiah')"/> Rupiah perbulan</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input type="text" id="usaha_hargapokokpenjualanpersen" style="width:200px;background:#FF0;" nai="Harga Pokok Penjualan " value="<? echo $usaha_hargapokokpenjualanpersen;?>"  maxlength="20" onBlur="yuria2('usaha_hargapokokpenjualanpersen')" onKeyPress="return isNumberKey(event)" /> %</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td><strong>Persentase Margin Usaha</strong></td>
	<td><input type="text" id="usaha_persentasemarginusaha" style="width:200px;background:#FF0;" nai="Persentase Margin Usaha " value="<? echo $usaha_persentasemarginusaha;?>"  maxlength="20" onKeyPress="return isNumberKey(event)" /> %</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Biaya Operasional Usaha</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Biaya Gaji Pegawai:</td>
	<td><input type="text" id="usaha_biayagajipegawai" style="width:200px;" nai="Biaya Gaji Pegawai " value="<? echo $usaha_biayagajipegawai;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayagajipegawai')"/> perbulan</td>
</tr>
<tr>
	<td>Gaji rata-rata per-pegawai</td>
	<td><input type="text" id="usaha_gajiratapegawai" style="width:200px;background:#FF0;" nai="Gaji rata-rata per-pegawai " value="<? echo $usaha_gajiratapegawai;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_gajiratapegawai')"/> perbulan</td>
</tr>
<tr>
	<td>Jumlah Karyawan</td>
	<td><input type="text" id="usaha_jumlahkaryawan" style="width:200px;background:#FF0;" nai="Jumlah Karyawan " value="<? echo $usaha_jumlahkaryawan;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  /></td>
</tr>
<tr>
	<td>Biaya Sewa/Kontrak</td>
	<td><input type="text" id="usaha_biayasewa" style="width:200px;background:#FF0;" nai="Biaya Sewa/Kontrak " value="<? echo $usaha_biayasewa;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayasewa')"/> perbulan</td>
</tr>
<tr>
	<td>Biaya Telepon, Listrik, Air</td>
	<td><input type="text" id="usaha_biayatelpon" style="width:200px;background:#FF0;" nai="Biaya Telepon, Listrik, Air " value="<? echo $usaha_biayatelpon;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayatelpon')"/> perbulan</td>
</tr>
<tr>
	<td>Biaya Sampah, Keamanan, Transportasi</td>
	<td><input type="text" id="usaha_biayasampah" style="width:200px;background:#FF0;" nai="Biaya Sampah, Keamanan, Transportasi " value="<? echo $usaha_biayasampah;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayasampah')"/> perbulan</td>
</tr>
<tr>
	<td>Biaya Operasional Lainnya</td>
	<td><input type="text" id="usaha_biayaoperasionallain" style="width:200px;background:#FF0;" nai="Biaya Operasional Lainnya " value="<? echo $usaha_biayaoperasionallain;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayaoperasionallain')"/> perbulan</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Biaya Lainnya</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Biaya rumah tangga</td>
	<td><input type="text" id="usaha_biayarumahtangga" style="width:200px;background:#FF0;" nai="Biaya rumah tangga " value="<? echo $usaha_biayarumahtangga;?>"  maxlength="20" onKeyPress="return isNumberKey(event)"  onKeyUp="outputMoney('usaha_biayarumahtangga')"/> perbulan</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" /></td>
</tr>
	<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
	<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
	<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
	<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
	<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
	<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
	<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
</table>
</form>
<script type="text/javascript">
	function kumi() {
		var nan="SAVE4";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var usaha_sektorindustri = $("#usaha_sektorindustri").val();
		var usaha_jenisusaha = $("#usaha_jenisusaha").val();
		var usaha_lamausahamonth = $("#usaha_lamausahamonth").val();
		var usaha_lamausahayear = $("#usaha_lamausahayear").val();
		var usaha_kepemilikanusaha = $("#usaha_kepemilikanusaha").val();
		var usaha_jumlahpenjualanweek = $("#usaha_jumlahpenjualanweek").val();
		var usaha_jumlahpenjualanmonth = $("#usaha_jumlahpenjualanmonth").val();
		var usaha_jumlahpenjualanyear = $("#usaha_jumlahpenjualanyear").val();
		var usaha_ratajumlahpembeli = $("#usaha_ratajumlahpembeli").val();
		var usaha_ratapembelian = $("#usaha_ratapembelian").val();
		var usaha_kenaikanjumlahpembeli = $("#usaha_kenaikanjumlahpembeli").val();
		var usaha_kenaikanticketsize = $("#usaha_kenaikanticketsize").val();
		var usaha_hargapokokpenjualanrupiah = $("#usaha_hargapokokpenjualanrupiah").val();
		var usaha_hargapokokpenjualanpersen = $("#usaha_hargapokokpenjualanpersen").val();
		var usaha_persentasemarginusaha = $("#usaha_persentasemarginusaha").val();
		var usaha_biayagajipegawai = $("#usaha_biayagajipegawai").val();
		var usaha_gajiratapegawai = $("#usaha_gajiratapegawai").val();
		var usaha_jumlahkaryawan = $("#usaha_jumlahkaryawan").val();
		var usaha_biayasewa = $("#usaha_biayasewa").val();
		var usaha_biayatelpon = $("#usaha_biayatelpon").val();
		var usaha_biayasampah = $("#usaha_biayasampah").val();
		var usaha_biayaoperasionallain = $("#usaha_biayaoperasionallain").val();
		var usaha_biayarumahtangga = $("#usaha_biayarumahtangga").val();
			
			
		var FormName="formentry";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_ajax_n.php",
				data: "nan="+nan+"&usaha_sektorindustri="+usaha_sektorindustri+"&usaha_jenisusaha="+usaha_jenisusaha+"&usaha_lamausahamonth="+usaha_lamausahamonth+"&usaha_lamausahayear="+usaha_lamausahayear+"&usaha_kepemilikanusaha="+usaha_kepemilikanusaha+"&usaha_jumlahpenjualanweek="+usaha_jumlahpenjualanweek+"&usaha_jumlahpenjualanmonth="+usaha_jumlahpenjualanmonth+"&usaha_jumlahpenjualanyear="+usaha_jumlahpenjualanyear+"&usaha_ratajumlahpembeli="+usaha_ratajumlahpembeli+"&usaha_ratapembelian="+usaha_ratapembelian+"&usaha_kenaikanjumlahpembeli="+usaha_kenaikanjumlahpembeli+"&usaha_kenaikanticketsize="+usaha_kenaikanticketsize+"&usaha_hargapokokpenjualanrupiah="+usaha_hargapokokpenjualanrupiah+"&usaha_hargapokokpenjualanpersen="+usaha_hargapokokpenjualanpersen+"&usaha_persentasemarginusaha="+usaha_persentasemarginusaha+"&usaha_biayagajipegawai="+usaha_biayagajipegawai+"&usaha_gajiratapegawai="+usaha_gajiratapegawai+"&usaha_jumlahkaryawan="+usaha_jumlahkaryawan+"&usaha_biayasewa="+usaha_biayasewa+"&usaha_biayatelpon="+usaha_biayatelpon+"&usaha_biayasampah="+usaha_biayasampah+"&usaha_biayaoperasionallain="+usaha_biayaoperasionallain+"&usaha_biayarumahtangga="+usaha_biayarumahtangga+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
				}
			});
		}
	
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 