<?php
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
$NaiSummarySameID[0]="";
$varcustnomid = $_REQUEST['hide_nomid'];
//echo $varcustnomid;

if(isset($_REQUEST['RadioGroupCustomerType']))
{
	$varCustomerType=$_REQUEST['RadioGroupCustomerType'];
	//echo $varCustomerType;
}
else
{
}

$i=0;
//CHECK DUPLICATE FROM CUSTOMERMASTERPERSON
if(isset($_REQUEST['Againts_0']))
{
	$varAgaints_0=$_REQUEST['Againts_0'];
	//echo $varAgaints_0;

	if($varCustomerType == "company")
	{
		$tsql = "select * from tbl_CustomerMasterPerson where custsex = '0'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
		
		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}
		
		if(sqlsrv_has_rows($sqlConn))
		{
		  $rowCount = sqlsrv_num_rows($sqlConn);
		  
		  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		  {
			  $duplicateNOMID[$i]=$row['custnomid'];
			  $duplicateNames[$i]=$row['custfullname'];
			  $duplicateKTP[$i]=$row['custktpno'];
			  $duplicateNPWP[$i]=$row['custnpwpno'];
			  $duplicateMotherName[$i]=$row['custmothername'];
			  $duplicateDOB[$i]=$row['custboddate'];
			  $i++;
		  }
	   }
	   sqlsrv_free_stmt( $sqlConn );
	}
	else if ($varCustomerType == "personal")
	{
		$tsql = "select * from tbl_CustomerMasterPerson where custsex <> '0'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
		
		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}
		
		if(sqlsrv_has_rows($sqlConn))
		{
		  $rowCount = sqlsrv_num_rows($sqlConn);
		  
		  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		  {
			  $duplicateNOMID[$i]=$row['custnomid'];
			  $duplicateNames[$i]=$row['custfullname'];
			  $duplicateKTP[$i]=$row['custktpno'];
			  $duplicateNPWP[$i]=$row['custnpwpno'];
			  $duplicateMotherName[$i]=$row['custmothername'];
			  $duplicateDOB[$i]=$row['custboddate'];
			  $i++;
		  }
	   }
	   sqlsrv_free_stmt( $sqlConn );
	}	
}
else
{
	$varAgaints_0="null";
	//echo $varAgaints_0;
}

//print_r($duplicateNames);
//END CHECK DUPLICATE FROM CUSTOMERMASTERPERSON

$duplicateCount=$i;
//echo $duplicateCount;

//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";

$b=0;
//CHECK DUPLICATE FROM BLACKLIST
if(isset($_REQUEST['Againts_1']))
{
	$varAgaints_1=$_REQUEST['Againts_1'];
	//echo $varAgaints_1;


    $tsql = "select * from tbl_CustomerBlacklist";
   	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   	$params = array(&$_POST['query']);
   	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if($sqlConn === false)
	{
		die(FormatErrors(sqlsrv_errors()));
	}
	
	if(sqlsrv_has_rows($sqlConn))
	{
      $rowCount = sqlsrv_num_rows($sqlConn);
	  
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
      {
		  $duplicateBlackNOMID[$b]=$row['custnomid'];
		  $duplicateBlackNames[$b]=$row['custfullname'];
		  $duplicateBlackKTP[$b]=$row['custktpno'];
		  $duplicateBlackNPWP[$b]=$row['custnpwpno'];
		  $duplicateBlackMotherName[$b]=$row['custmothername'];
		  $duplicateBlackDOB[$b]=$row['custboddate'];
		  $b++;
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   	
}
else
{
	$varAgaints_1="null";
	//echo $varAgaints_1;
}

//END CHECK DUPLICATE FROM BLACKLIST
$duplicateBlackCount=$b;
//echo $duplicateBlackCount;


//VALIDATE DUPLICATE
    $tsql = "select * from tbl_CustomerMasterPerson where custnomid='$varcustnomid'";
   	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   	$params = array(&$_POST['query']);
   	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
	if($sqlConn === false)
	{
		die(FormatErrors(sqlsrv_errors()));
	}
	
	if(sqlsrv_has_rows($sqlConn))
	{
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
      {
		  $NowNOMID=$row['custnomid'];
		  $NowNames=$row['custfullname'];
		  $NowKTP=$row['custktpno'];
		  $NowNPWP=$row['custnpwpno'];
		  $NowMotherName=$row['custmothername'];
		  $NowDOB=$row['custboddate'];
		  $NowCustomerType=$row['custsex'];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
//END VALIDATE DUPLICATE

$NaiCheckNames=0;
$NaiCheckKTP=0;
$NaiCheckNPWP=0;
$NaiCheckMotherName=0;
$NaiCheckDOB=0;

$NaiBlackNames=0;
$NaiBlackKTP=0;
$NaiBlackNPWP=0;
$NaiBlackMotherName=0;
$NaiBlackDOB=0;

$t=0;
for($x=0;$x<$duplicateCount;$x++)
{
	//echo "#".$duplicateNPWP[$x]."#"." = "."#".$NowNPWP."#";
	//echo "<br>";
	
	if($duplicateNPWP[$x] == $NowNPWP && $NowNPWP != "")
	{
		$NaiCheckNPWP = $NaiCheckNPWP + 1;
		$NaiSummarySameID[$t]=$duplicateNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateNames[$x] == $NowNames)
	{
		//echo "aw1<br>";
		$NaiCheckNames = $NaiCheckNames + 1;
		$NaiSummarySameID[$t]=$duplicateNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateKTP[$x] == $NowKTP)
	{
		//echo "aw2<br>";
		$NaiCheckKTP = $NaiCheckKTP + 1;
		$NaiSummarySameID[$t]=$duplicateNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateDOB[$x] == $NowDOB)
	{
		//echo "aw3<br>";
		$NaiCheckDOB = $NaiCheckDOB + 1;
		$NaiSummarySameID[$t]=$duplicateNOMID[$x];
		$t = $t + 1;
	}	
}

//echo "EXIST NAMA ".$NaiCheckNames."<br>";
//echo "EXIST KTP  ".$NaiCheckKTP."<br>";
//echo "EXIST NPWP ".$NaiCheckNPWP."<br>";
//echo "EXIST MAMA ".$NaiCheckMotherName."<br>";
//echo "EXIST TGL  ".$NaiCheckDOB."<br>";

for($x=0;$x<$duplicateBlackCount;$x++)
{
	//echo "#".$duplicateNPWP[$x]."#"." = "."#".$NowNPWP."#";
	//echo "<br>";
	
	if($duplicateBlackNPWP[$x] == $NowNPWP && $NowNPWP != "")
	{
		$NaiBlackNPWP = $NaiBlackNPWP + 1;
		$NaiSummarySameID[$t]=$duplicateBlackNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateBlackNames[$x] == $NowNames)
	{
		//echo "aw1<br>";
		$NaiBlackNames = $NaiBlackNames + 1;
		$NaiSummarySameID[$t]=$duplicateBlackNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateBlackKTP[$x] == $NowKTP)
	{
		//echo "aw2<br>";
		$NaiBlackKTP = $NaiBlackKTP + 1;
		$NaiSummarySameID[$t]=$duplicateBlackNOMID[$x];
		$t = $t + 1;
	}
	
	if($duplicateBlackDOB[$x] == $NowDOB)
	{
		//echo "aw3<br>";
		$NaiBlackDOB = $NaiBlackDOB + 1;
		$NaiSummarySameID[$t]=$duplicateBlackNOMID[$x];
		$t = $t + 1;
	}	
}

//echo "BLACK NAMA ".$NaiBlackNames."<br>";
//echo "BLACK KTP  ".$NaiBlackKTP."<br>";
//echo "BLACK NPWP ".$NaiBlackNPWP."<br>";
//echo "BLACK MAMA ".$NaiBlackMotherName."<br>";
//echo "BLACK TGL  ".$NaiBlackDOB."<br>";

$statusRelationshipBank = 0;
if($NaiCheckDOB > 1 || $NaiCheckKTP > 1 || $NaiCheckMotherName > 1 || $NaiCheckNames > 1 || $NaiCheckNPWP > 1 )
{
	$statusRelationshipBank = 1;
}
else
{
	$statusRelationshipBank = 0;
}

$statusBlacklist = 0;
if($NaiBlackDOB > 0 || $NaiBlackKTP > 0 || $NaiBlackMotherName > 0 || $NaiBlackNames > 0 || $NaiBlackNPWP > 0 )
{
	$statusBlacklist = 1;
}
else
{
	$statusBlacklist = 0;
}


$o = 0;
$arrCount = count($NaiSummarySameID);
//print_r($NaiSummarySameID);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DUPLICATEANDBLACKLIST</title>
</head>

<body>
<script>
window.name = "naikorasu";
</script>
<div align="center">
<table width="800" border="0">
<tr>
<td width="45%">&nbsp;</td>
<td width="1%">&nbsp;</td>
<td width="44%">&nbsp;</td>
</tr>
<tr>
<td colspan="3"><div align="center">CUSTOMER DUPLICATE </div></td>
</tr>
<tr>
<td colspan="3"><div align="center">AND</div></td>
</tr>
<tr>
<td colspan="3"><div align="center">BLACKLIST CHECKING</div></td>
</tr>
<tr>
<td colspan="3"><div align="center"></div></td>
</tr>
<tr>
<td colspan="3"><div style="background-color:#0FF" align="center">Informasi Pemohon</div></td>
</tr>
<tr>
  <td><div align="right">Nama Lengkap</div></td>
  <td><div align="center">&nbsp;:&nbsp;</div></td>
  <td><div align="left">&nbsp;<?= $NowNames; ?></div></td>
</tr>
<tr>
  <td><div align="right">Tanggal Lahir</div></td>
  <td><div align="center">&nbsp;:&nbsp;</div></td>
  <td><div align="left">&nbsp;<?= $NowDOB; ?></div></td>
</tr>
<tr>
  <td><div align="right">KTP</div></td>
  <td><div align="center">&nbsp;:&nbsp;</div></td>
  <td><div align="left">&nbsp;<?= $NowKTP; ?></div></td>
</tr>
<tr>
  <td><div align="right">NPWP</div></td>
  <td><div align="center">&nbsp;:&nbsp;</div></td>
  <td><div align="left">&nbsp;<?= $NowNPWP; ?></div></td>
</tr>
<tr>
<td><div align="right">Nama Ibu Kandung</div></td>
<td><div align="center">&nbsp;:&nbsp;</div></td>
<td><div align="left">&nbsp;<?= $NowMotherName ?></div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td colspan="3"><div style="background-color:#0FF" align="center">Result </div></td>
</tr>
<tr>
<td colspan="3">
<div align="center">
<table width="50%" border="0">
<tr>
<td>
<div align="center">
<? 
if($statusBlacklist == 0 && $statusRelationshipBank == 0)
{
	$statusSummary = "1";
	echo "Not Blacklist by Internal List and no relationship with Bank";
}
else if($statusBlacklist == 1 && $statusRelationshipBank == 0)
{
	$statusSummary = "2";
	echo "Blacklist by Internal List but no relationship with Bank";
}
else if($statusBlacklist == 0 && $statusRelationshipBank == 1)
{
	$statusSummary = "3";
	echo "Not Blacklist by Internal List but have relationship with Bank";
}
else if($statusBlacklist == 1 && $statusRelationshipBank == 1)
{
	$statusSummary = "4";
	echo "Blacklist by Internal List and have relationship with Bank";
}
?>
</div>
</td>
</tr>
</table>
</div>
</td>
</tr>
<tr>
<td colspan="3">
<table border="1" cellpadding="5" cellspacing="0" width="100%">
<tr>
<th style="background-color:#CCC" valign="top"><div align="center">NO<br>APPLIKASI</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">NAMA</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">TANGGAL<br>LAHIR</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">KTP</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">NPWP</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">NAMA IBU<br>KANDUNG</div></th>
<th style="background-color:#CCC" valign="top"><div align="center">STATUS</div></th> 
<!--<th style="background-color:#CCC" valign="top"><div align="center">DATA*)</div></th>-->
</tr>
<?
$prevID="";
$totalArray=count($NaiSummarySameID);

if($totalArray == 0)
{
}
else
{				
	for($x=0;$x<$totalArray;$x++)
	{
		//echo $NaiSummarySameID[$x];
		
		if($prevID == $NaiSummarySameID[$x])
		{
		}
		else
		{
			$CheckCustNomId = $NaiSummarySameID[$x];
			//echo $CheckCustNomId."<br>";			
			
			if($statusRelationshipBank == 0)
			{
			}
			else
			{
				if($CheckCustNomId == $varcustnomid)
				{
				}
				else
				{
				
					$tsql = "
					select *,
					'dupstat' = case when cap.txn_action = 'A' then 'APPROVE' when fak.txn_action = 'A' then 'PROGRESS' when fak.txn_action = 'R' then 'REJECT' when fak.txn_action = 'J' then 'REJECT' else 'NO STATUS' end
					from tbl_CustomerMasterPerson cmp 
					left join tbl_FFAK fak on fak.txn_id = cmp.custnomid
					left join tbl_FCAP cap on cap.txn_id = cmp.custnomid
					where cmp.custnomid='$CheckCustNomId'
					";
					//echo $tsql."<br>";
					$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
					$params = array(&$_POST['query']);
					$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
					
					if($sqlConn === false)
					{
						die(FormatErrors(sqlsrv_errors()));
					}
					
					if(sqlsrv_has_rows($sqlConn))
					{
					  $rowCount = sqlsrv_num_rows($sqlConn);
					  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
					  {
						  $NowNOMID=$row['custnomid'];
						  $NowNames=$row['custfullname'];
						  $NowKTP=$row['custktpno'];
						  $NowNPWP=$row['custnpwpno'];
						  $NowMotherName=$row['custmothername'];
						  $NowDOB=$row['custboddate'];
						  $NowCustomerType=$row['custsex'];	
						  $NowStatusCustomer=$row['dupstat'];
						  ?>
						  <tr>
						  <td><div align="center">&nbsp;<?=$NowNOMID; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowNames; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowDOB; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowKTP; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowNPWP; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowMotherName; ?></div></td>
						  <td><div align="center">&nbsp;<?=$NowStatusCustomer; ?></div></td>
						  <!--<td>
						  <div align="center">&nbsp;
						  <input type="checkbox" name="<?= "data_".$NowNOMID; ?>" value="<?= "data_".$NowNOMID; ?>" onclick="alert('Under Development');">
						  </div>
						  </td>-->   
						  </tr>              
						  <?
					  }
					}
					sqlsrv_free_stmt( $sqlConn );
				}
			}
			
			$tsql = "
			select *,
			'blkstat' = case when cap.txn_action = 'A' then 'APPROVE' when fak.txn_action = 'A' then 'PROGRESS' when fak.txn_action = 'R' then 'REJECT' when fak.txn_action = 'J' then 'REJECT' else 'NO STATUS' end
			from tbl_CustomerBlacklist cmp 
			left join tbl_FFAK fak on fak.txn_id = cmp.custnomid
			left join tbl_FCAP cap on cap.txn_id = cmp.custnomid
			where cmp.custnomid='$CheckCustNomId'
			";
			//echo $tsql."<br>";
			$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params = array(&$_POST['query']);
			$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
			
			if($sqlConn === false)
			{
				die(FormatErrors(sqlsrv_errors()));
			}
			
			if(sqlsrv_has_rows($sqlConn))
			{
			  $rowCount = sqlsrv_num_rows($sqlConn);
			  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			  {
				  $NowNOMID=$row['custnomid'];
				  $NowNames=$row['custfullname'];
				  $NowKTP=$row['custktpno'];
				  $NowNPWP=$row['custnpwpno'];
				  $NowMotherName=$row['custmothername'];
				  $NowDOB=$row['custboddate'];
				  $NowCustomerType=$row['custsex'];	
				  $NowStatusCustomer=$row['blkstat'];
				  ?>
                  <tr>
                  <td><div align="center">&nbsp;<?= $NowNOMID; ?></div></td>
                  <td><div align="center">&nbsp;<?= $NowNames; ?></div></td>
                  <td><div align="center">&nbsp;<?= $NowDOB; ?></div></td>
                  <td><div align="center">&nbsp;<?= $NowKTP; ?></div></td>
                  <td><div align="center">&nbsp;<?= $NowNPWP; ?></div></td>
                  <td><div align="center">&nbsp;<?= $NowMotherName; ?></div></td>
                  <td><div align="center">&nbsp;<?=$NowStatusCustomer; ?></div></td>
                  <!--<td>
                  <div align="center">&nbsp;
                  <input type="checkbox" name="<?= "data_".$NowNOMID; ?>" value="<?= "data_".$NowNOMID; ?>" onclick="alert('Under Development');">
                  </div>
                  </td>-->
                  </tr>
                  <?
			  }
			}
			sqlsrv_free_stmt( $sqlConn );
			
			$prevID = $NaiSummarySameID[$x];
		}
	}
}

?>
</table>
</td>
</tr>
<tr>
  <td colspan="3">&nbsp;</td>
</tr>
<tr>
  <td colspan="3"><!--*) Klik pada kolom data jika data debitur sama dan akan digunakan--></td>
</tr>
<tr>
  <td colspan="3">&nbsp;</td>
</tr>
<tr>
  <td colspan="3"><div align="center"><input type="submit" value="Back" onclick="window.open('form_duplicate_and_blacklist_checking.php?custnomid=<?= $varcustnomid; ?>','naikorasu')"/></div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
</table>
</div>
</body>
</html>
<?
require ("../../lib/close_con.php");
?>