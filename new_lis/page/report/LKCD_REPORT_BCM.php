<?
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	$userid=$_REQUEST['userid'];
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		
			var rand_no1 = Math.random();
			var rand_no2 = Math.random();
			var rand_no = rand_no1 * rand_no2;
			function statuscabang() 
			{
				var cabang = $("#cabang").val();
				$.ajax	
				({
					type: "GET",
					url: "VIEW_STATUS_FAK.php",
					data: "flag=1&cabang="+cabang+"&rand_no="+rand_no+"",
					success: function(response)
					{
						//alert(response);
						$("#repson").html(response);
					}
				});
			}
			
			function validation()
			{
				var cabang = $("#cabang").val();
				var fromdate = $("#fromdate").val();
				var todate = $("#ftodate").val();
				var ao = $("#ao").val();
				//alert(ao);
				var StatusAllowSubmit=true;
				
				if (cabang=="")
				{
					alert("Tipe report Harus di isi");
					$("#cabang").focus();
					StatusAllowSubmit=false;
				}
				else if (ao=="")
				{
					alert("AO Harus di isi");
					$("#ap").focus();
					StatusAllowSubmit=false;
				}
				else if (fromdate=="")
				{
					alert("FORM DATE Harus di isi");
					$("#fromdate").focus();
					StatusAllowSubmit=false;
				}
				else if (todate=="")
				{
					alert("TO DATE Harus di isi");
					$("#todate").focus();
					StatusAllowSubmit=false;
				}
				
				
				if(StatusAllowSubmit == true)
				{			
					document.getElementById("formentry").action = "LKCD_REPORT_BCM_VIEW.php";			
					document.getElementById("formentry").submit();
				}
			}
		</script>
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">
			<table align="center" border="1" style="width:400px; border:1px solid black;">
				<tr>
					<td colspan="2" style="font-size:16;font-weight:bold; text-align:center;">REPORT LKCD BCM</td>
				</tr>
				<tr>
					<td colspan="2" style="font-size:16;font-weight:bold; text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:30%;">Type Report</td>
					<td>
						<select nai="Cabang " id="cabang" name="cabang" style="background:#ff0;" >
							<option value=""> -- Pilih cabang -- </option>
							<option value="1"> -- Daily Report -- </option>
							<option value=""> -- Monthly Report -- </option>
						</select>
					</td>
				</tr>
				<tr>
					<td style="width:30%;">ao</td>
					<td>
					<div id="repson">
						<table width=100%>
							<td>
								<select nai="ao " id="ao" name="ao" style="background:#ff0;" >
									<option value=""> -- Pilih AO -- </option>
									<?
									$strsql="select * from Tbl_AO a 
											join Tbl_SE_User b on a.ao_code = b.user_ao_code
											and a.ao_branch_code = b.user_branch_code";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											echo'<option value="'.$rows['ao_code'].'">'.$rows['ao_name'].'</option>';
										}
									}
									?>
								</select>
							</td>
						</table>
					</div>
					</td>
					
				</tr>
				<tr>
					<td>Dari Tanggal</td>
					<td><input style="background:#ff0;" type="text" id="fromdate" name="fromdate" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');"/></td>
				</tr>
				<tr>
					<td>Sampai Tanggal</td>
					<td><input style="background:#ff0;" type="text" id="todate" name="todate" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');"/></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;"><input type="hidden" name="userid" id="userid" value="<?echo $userid;?>" /></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;"><input type="button" id="btnsubmit" name="btnsubmit" class="button" value="submit"onclick="validation()"/><input type="hidden" id="flag" name="flag" value="2"></td>
				</tr>
			</table>
		</form>
	</body>
</html>