<?php
# PREPARE DATA FOR SCORING
include("prepare_data.php");

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SCORING</title>
    </head>
    <body>
        <hr>
        <div align="center">
            <table>
                <tr>
                    <td>No Appl</td>
                    <td>:</td>
                    <td><?=$custnomid;?></td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>:</td>
                    <td><?=$custfullname;?></td>
                </tr>
            </table>
        </div>
        <hr>
        <iframe src="./score.php?custnomid=<?=$custnomid;?>" width="100%" height="450px" frameborder="0"></iframe>

        <div align="center">
            <table>
                <tr>
                    <td>
                        <form action="process_data.php" method="post">
                            <input type="hidden" name="custproccode" value="<?=$custproccode;?>">
                            <input type="hidden" name="custnomid" value="<?=$custnomid;?>">
                            <button id="" name="" value="">PROCESS</button>
                        </form>
                    </td>
                    <td>
                        <form action="reset_data.php" method="post">
                            <input type="hidden" name="custproccode" value="<?=$custproccode;?>">
                            <input type="hidden" name="custnomid" value="<?=$custnomid;?>">
                            
							<button id="" name="" value="">RESET</button>
							
                        </form>
                    </td>
                </tr>
            </table>
        </div>

    </body>
</html>
