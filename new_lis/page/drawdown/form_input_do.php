<?
require("../../lib/sqlsrv.lis.php");
require("../../lib/open_con.php");

$db_lis = new DB_LIS();
$db_lis->connect();

foreach($_REQUEST as $key=>$value)
{
    $$key = str_replace(",", "", $value);
}

//echo $custnomid;

$tsql = "DELETE FROM DRAWDOWN WHERE custnomid = '$custnomid'";
$db_lis->executeNonQuery($tsql);

$tsql = "INSERT INTO DRAWDOWN
            ([custnomid]
            ,[cif]
            ,[no_pk]
            ,[no_urut_pk]
            ,[kode_produk_pinjaman]
            ,[cabang_dinas]
            ,[dinas]
            ,[sub_dinas]
            ,[sub_sub_dinas]
            ,[marketing_officer]
            ,[kolektif_officer]
            ,[frekwensi_pembayaran]
            ,[grace_period]
            ,[jumlah_kontrak]
            ,[sifat_kredit]
            ,[kode_lokasi]
            ,[orientasi_penggunaan]
            ,[kategori_pengukuran]
            ,[kategori_debitur]
            ,[kategori_portofolio]
            ,[jenis_kredit]
            ,[jenis_suku_bunga]
            ,[jenis_garansi]
            ,[jaminan]
            ,[sektor_ekonomi]
            ,[pupn]
            ,[golongan_penjamin]
            ,[jenis_pinjaman]
            ,[sandi_realisasi]
            ,[golongan_debitur]
            ,[hubungan_dengan_bank]
            ,[status_debitur]
            ,[lembaga_pemeringkat]
            ,[negara_pihak_pemohon]
            ,[tipe_kredit_khusus]
            ,[jenis_asuransi]
            ,[jenis_pengikat]
            ,[pengadilan_negeri]
            ,[denda_overdraft]
            ,[uang_muka]
            ,[asr_kbkr_krgn]
            ,[pengikatan_jmn]
            ,[bupln]
            ,[status_drawdown]
            ,[date_drawdown])
      VALUES
            ('$custnomid'
            ,'$cif'
            ,'$no_pk'
            ,'$no_urut_pk'
            ,'$kode_produk_pinjaman'
            ,'$cabang_dinas'
            ,'$dinas'
            ,'$sub_dinas'
            ,'$sub_sub_dinas'
            ,'$marketing_officer'
            ,'$kolektif_officer'
            ,'$frekwensi_pembayaran'
            ,'$grace_period'
            ,'$jumlah_kontrak'
            ,'$sifat_kredit'
            ,'$kode_lokasi'
            ,'$orientasi_penggunaan'
            ,'$kategori_pengukuran'
            ,'$kategori_debitur'
            ,'$kategori_portofolio'
            ,'$jenis_kredit'
            ,'$jenis_suku_bunga'
            ,'$jenis_garansi'
            ,'$jaminan'
            ,'$sektor_ekonomi'
            ,'$pupn'
            ,'$golongan_penjamin'
            ,'$jenis_pinjaman'
            ,'$sandi_realisasi'
            ,'$golongan_debitur'
            ,'$hubungan_dengan_bank'
            ,'$status_debitur'
            ,'$lembaga_pemeringkat'
            ,'$negara_pihak_pemohon'
            ,'$tipe_kredit_khusus'
            ,'$jenis_asuransi'
            ,'$jenis_pengikat'
            ,'$pengadilan_negeri'
            ,'$denda_overdraft'
            ,'$uang_muka'
            ,'$asr_kbkr_krgn'
            ,'$pengikatan_jmn'
            ,'$bupln'
            ,'0'
            ,GETDATE())
";
echo nl2br($tsql,true)."<br><br>";
$db_lis->executeNonQuery($tsql);

require ("../../requirepage/do_saveflow.php");

header("location:../flow.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission");

?>
