<?php
	
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");

if(isset($_GET['status']))
{
	$status=$_GET['status'];
}
else
{
	$status=0;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>REPORT BI CHECKING</title>
<script src="../../js/jquery-latest.min.js" type="text/javascript"></script>
<script src="../../js/menu_ce.js" type="text/javascript"></script>
<link href="../../css/menu_ce.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../lib/tab-view.css" />
<script type="text/javascript" src="../../lib/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../lib/slide_down.js"></script>
<script type="text/javascript" src="../../lib/full_function.js"></script>
<link href="../../lib/crw.css" rel="stylesheet" type="text/css" />
<Script language="JAVASCRIPT">
 function goSave()
{
	var FormName="formsubmit";
	var elem = document.getElementById(FormName).elements;
	
	for(var i = 0; i < elem.length; i++)
	{
		//elem[i].value = elem[i].value.toUpperCase();
	}
	
	var StatusAllowSubmit=true;
	var elem = document.getElementById(FormName).elements;
	for(var i = 0; i < elem.length; i++)
	{
		if(elem[i].style.backgroundColor=="#ff0")
		{
			if(elem[i].value == "")
			{
				alert(elem[i].nai + " field Must be filled");	
				StatusAllowSubmit=false				
				break;
			}
		}
	}
	
	if(StatusAllowSubmit == true)
	{			
		document.formsubmit.target = "utama";
		document.formsubmit.action = "doreportbichecking.php";
		
		document.formsubmit.submit();
		return true;
	}
}
        
</Script>

	<style type="text/css" media="print">
		.NonPrintable
		{
		  display: none;
		}
	</style>
	
	<style type="text/css" media="print">
		.break{
		page-break-before: always;
		}
	</style> 
</head>
<BODY>
<script language="Javascript">
				name="utama";
</script>
<div style="position:fixed;">
				<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
			</div>
			<div style="border:0px solid black;width:23%;height:545px;margin-top:7%;float:left;position:fixed;">
<!--Open Menu-->
				<div id="cssmenu">
					<ul>
<?
	$d = date("d");
	$m = date("m");
	$y = date("y");
	$h = date("h");
	$i = date("i");
	$s = date("s");
	$randomtime = $h.$i.$s;
	
	$tsql2 = "select * from Tbl_SE_User U LEFT JOIN Tbl_Branch B ON U.user_branch_code = B.branch_code where user_id='$userid'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$userbranch = $row2[7];
			$userpwd = $row2[9];
			$userregion = $row2[21];
		}
	}
			
	$tsql = "select Tbl_SE_GrpProgram.grp_code, Tbl_SE_GrpProgram.grp_name from Tbl_SE_GrpProgram, Tbl_SE_Program, Tbl_SE_UserProgram
				where Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
				AND Tbl_SE_Program.program_group=Tbl_SE_GrpProgram.grp_code
				AND Tbl_SE_UserProgram.user_id='$userid'
				GROUP BY Tbl_SE_GrpProgram.grp_code,Tbl_SE_GrpProgram.grp_name, Tbl_SE_GrpProgram.grp_urut
				ORDER BY Tbl_SE_GrpProgram.grp_urut,Tbl_SE_GrpProgram.grp_name";
	  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	  $params = array(&$_POST['query']);

	  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	  if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	  if(sqlsrv_has_rows($sqlConn))
	  {
		 $rowCount = sqlsrv_num_rows($sqlConn);
		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		 {
			$tsql2 = "SELECT Tbl_SE_Program.program_code,
						Tbl_SE_Program.program_name,Tbl_SE_Program.program_desc
						FROM Tbl_SE_Program, Tbl_SE_UserProgram
						WHERE Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
						AND Tbl_SE_UserProgram.user_id='$userid'
						AND Tbl_SE_Program.program_group='$row[0]'
						ORDER BY program_urut";
			  $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			  $params2 = array(&$_POST['query']);

			  $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
			  if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			  if(sqlsrv_has_rows($sqlConn2))
			  {
				 $rowCount2 = sqlsrv_num_rows($sqlConn2);
				 
				 echo "<li>";
				 echo "<a href='#'><span>".$row['1']."</span></a>";
				 if($rowCount2 > '15')
				 {
					echo "<ul style='overflow-x:hidden;overflow-y:scroll;white-space:nowrap;height:150px;'>";
				 }else{
					echo "<ul>";
				 }
				 
				 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				 {
					echo "<li><a href='http://127.0.0.1/lis/new_lis/page/".$row2[2].".php?r=".$randomtime."&userid=".$userid."&userpwd=".$userpwd."&userbranch=".$userbranch."&userregion=".$userregion."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
				 }
			  }
			  sqlsrv_free_stmt( $sqlConn2 );
				 echo "</ul>";
				 echo "</li>";
		 }
	  }
	  sqlsrv_free_stmt( $sqlConn );
?>
					</ul>
				</div>
<!--Close Menu-->
				<img src="../../../images/gimmick_logo.png" style="position:absolute;bottom:0;width:60%;margin-left:20%;margin-right:20%;"></img>
			</div>
			<div align=center style="float:right;width:75%;margin-top:7%;margin-right:2%;">
<?
	if($status == 0)
	{
?>
<div align="center">
	<Table width="800" border="1">
	<tr>
		<td>
		<form name="formsubmit" action="doreportbichecking.php" method=post>
			<table width=60% border=0 align=center>
			<tr>
				<td align=center colspan=2><strong>REPORT BI CHECKING</strong></td>
			</tr>
			<tr>
				<td align=center colspan=2>&nbsp;</td>
			</tr>
			<tr>
				<td align=center colspan=2>&nbsp;</td>
			</tr>
			<tr>
				<td width=50% align=left valign=top>
					<font face=Arial size=2>Dari tanggal </font>
				</td>
				<td width=50% align=left valign=top>
					<input type=text id=tgl1 name=tgl1 readonly=readonly nai="DARI TANGGAL" style="background:#FF0"> </input>
					<a href="javascript:NewCssCal('tgl1','ddMMyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
					<font face=Arial size=2 color=blue></font>
				</td>
			</tr>
			<tr>
				<td width=40% align=left valign=top>
					<font face=Arial size=2>Sampai tanggal</font>
				</td>
				<td width=60% align=left valign=top>
					<input type=text id=tgl2 name=tgl2 readonly=readonly nai="SAMPAI TANGGAL" style="background:#FF0"> </input>
					<a href="javascript:NewCssCal('tgl2','ddMMyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
					<font face=Arial size=2 color=blue></font>
				</td>
			</tr>
			<tr>
				<td align=center colspan=2>&nbsp;</td>
			</tr>
			<tr>
				<td align=center colspan=2>&nbsp;</td>
			</tr>
			<tr>
				<td align=center colspan=2><input type=button value="Search" onclick=goSave()></td>
				<input type="hidden" name=status value=1>
			</tr>
		</form>	
		</td>
	</tr>
	</Table>
</div>
<?
	}
	else if($status == 1)
	{
	
		$tgl1 = $_REQUEST['tgl1'];
		$tgl2 = $_REQUEST['tgl2'];
?>

<Table width =800 border='0' align=center>
	<tr >
		<td>
		<form name="formsubmit" action="doreportbichecking.php" method=post>
		
			<table width="60%" border="0" align="center" class="NonPrintable">
				<tr>
					<td align=center colspan=2><strong>REPORT BI CHECKING</strong></td>
				</tr>
				<tr>
					<td align=center colspan=2>&nbsp;</td>
				</tr>
				<tr>
					<td align=center colspan=2>&nbsp;</td>
				</tr>
				<tr>
					<td width=50% align=left valign=top>
						<font face=Arial size=2>Dari tanggal </font>
					</td>
					<td width=50% align=left valign=top>
						<input type=text id="tgl1" name="tgl1" readonly=readonly nai="DARI TANGGAL" style="background:#FF0" value="<? echo $tgl1; ?>" />
						<a href="javascript:NewCssCal('tgl1','ddMMyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
						<font face=Arial size=2 color=blue></font>
					</td>
				</tr>
				<tr>
					<td width=40% align=left valign=top>
						<font face=Arial size=2>Sampai tanggal</font>
					</td>
					<td width=60% align=left valign=top>
						<input type=text id="tgl2" name="tgl2" readonly=readonly nai="SAMPAI TANGGAL" style="background:#FF0" value="<? echo $tgl2; ?>" />
						<a href="javascript:NewCssCal('tgl2','ddMMyyyy')"><img src="../../images/calendar.gif" border="0" height=16 alt="Pick a date" id="img2"/></A>
						<font face=Arial size=2 color=blue></font>
					</td>
				</tr>
				<tr>
					<td align=center colspan=2>&nbsp;</td>
				</tr>
				<tr>
					<td align=center colspan=2>&nbsp;</td>
				</tr>
				<tr>
					<td align=center colspan=2><input type=button value="Search" onclick=goSave()></td>
					<input type=hidden name=status value=1>
				</tr>
			</table>
		</form>	
		</td>
	</tr>
	
	<tr>
		<td>

				<div align=center valign=top> <strong>DAFTAR BI CHECKING</strong></div>
				
				</br></br>

				<table cellspacing=10 width=80% border=0 align=center valign=top >
					<tr>
						<td width = 10% ><strong>No.</strong></td>
						<td width = 30% align=center><strong>Tanggal</strong></td>
						<td width = 30% align=center><strong>Jumlah Order</strong></td>
						<td width = 30% align=center><strong>Jumlah Respon</strong></td>
					</tr>
				
				<?
					
					$i = 0;
					//$hari1 = substr($tgl1, 0, 2) - 1;
					//$tgl1 = $hari1.substr($tgl1, 2, 8);
					//$hari2 = substr($tgl2, 0, 2) + 1;
					//$tgl2 = $hari2.substr($tgl2, 2, 8);
					
					$tsql = "SELECT CONVERT(DATE, TXN_TIME, 105) as b, COUNT(*) as JUMLAH FROM TBL_FIBC WHERE TXN_ACTION = 'A' AND CONVERT(DATE, TXN_TIME, 105) BETWEEN CONVERT(DATE, '$tgl1', 105) AND CONVERT(DATE, '$tgl2', 105) GROUP BY CONVERT(DATE, TXN_TIME, 105) ORDER BY b ASC";

					$aside = sqlsrv_query($conn, $tsql);

					if ($aside === false)
						die( FormatErrors( sqlsrv_errors() ) );

					if(sqlsrv_has_rows($aside))
					{  
						while($rowside = sqlsrv_fetch_array($aside, SQLSRV_FETCH_ASSOC))
						{ 
							$i++;
				?>
							
					<tr>
						<td width = 10%><? echo $i?>.</td>
						<td width = 30% align=center><? echo $rowside['b']->format('D , d/M/Y');?></td>
						<td width = 30% align=center><a href="detailbichecking.php?tgl=<? echo $rowside['b']->format('d/n/Y');?>" target=blank><? echo $rowside['JUMLAH'];?></a></td>
						<? 
						$tanggal = $rowside['b']->format('d/n/Y');
						$sql = "Select count(*) as jml from tbl_FBIR where CONVERT(DATE,txn_time,105) = CONVERT(DATE, '$tanggal', 105)";
						$rs = sqlsrv_query($conn,$sql);
						if ($rs === false)
						die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($rs))
						{  
						while($rowrs = sqlsrv_fetch_array($rs, SQLSRV_FETCH_ASSOC))
						{ ?>
							<td width = 30% align=center><a href="detailbirespon.php?tgl=<? echo $rowside['b']->format('d/n/Y');?>" target=blank><? echo $rowrs['jml'];?></td>
						<?}}?>
					</tr>

				<?
					}
				}
				?>
				</table>
				
		</td>
	</tr>
	</Table>

<?
	} // tutup else if status = 1
?>
</div>
</BODY>
</html>
<?
   	require("../../lib/close_con.php");
?>