<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");

$btnname=$_REQUEST['btn'];
$custnomid=$_REQUEST['custnomid'];
if ($btnname=="btnakteadd")
{
	$noakte=$_REQUEST['noakte'];
	$tglakte=$_REQUEST['tglakte'];
	$nomenkeh=$_REQUEST['nomenkeh'];
	$tglmenkeh=$_REQUEST['tglmenkeh'];
	
	$noberita=$_REQUEST['noberita']==""?$noberita="NULL":$noberita="'".$_REQUEST['noberita']."'";
	$notambahanberita=$_REQUEST['notambahanberita']==""?$notambahanberita="NULL":$notambahanberita="'".$_REQUEST['notambahanberita']."'";
	$tglberita=$_REQUEST['tglberita']==""?$tglberita="NULL":$tglberita="'".$_REQUEST['tglberita']."'";
	
	//$noberita=$_REQUEST['noberita'];
	//$notambahanberita=$_REQUEST['notambahanberita'];
	//$tglberita=$_REQUEST['tglberita'];
	
	
	$seq=0;
	$strsql = "select cast(isnull(max(seq),0)as int)+1 as seq 
				from Tbl_DtlAktePerubahanAnalisaYuridis where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}
	
	$strsql = " INSERT INTO Tbl_DtlAktePerubahanAnalisaYuridis
				(custnomid,seq,noakte,tglakte,nomenkeh,tglmenkeh,noberita,notambahanberita,tglberita)
				VALUES 
				('$custnomid','$seq','$noakte','$tglakte','$nomenkeh','$tglmenkeh',$noberita,$notambahanberita,$tglberita)
				";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_akte.php");
}
else if($btnname=="btnaktedel")
{
	$seq=$_REQUEST['seq'];
	$strsql = "delete from Tbl_DtlAktePerubahanAnalisaYuridis where seq='".$seq."' and custnomid='".$custnomid."'";
	//echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_akte.php");
}
else if($btnname=="btndocadd")
{
	$namadokumen=$_REQUEST['namadokumen'];
	$seq=0;
	$strsql = "select cast(isnull(max(seq),0)as int)+1 as seq 
				from Tbl_DtlDokumenAnalisaYuridis where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}

	$strsql = " INSERT INTO Tbl_DtlDokumenAnalisaYuridis
			(custnomid,seq,namadokumen)
			VALUES ('$custnomid','$seq','$namadokumen')";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_doc.php");
}
else if($btnname=="btndocdel")
{
	$seq=$_REQUEST['seq'];
	$strsql = "delete from Tbl_DtlDokumenAnalisaYuridis where seq='".$seq."' and custnomid='".$custnomid."'";
	//echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_doc.php");
}
else if($btnname=="btnspadd")
{
	$noaktesusunanpengurus=$_REQUEST['noaktesusunanpengurus'];
	$tglaktasusunanpengurus=$_REQUEST['tglaktasusunanpengurus'];
	$namanotarissusunanpengurus=$_REQUEST['namanotarissusunanpengurus'];
	$jabatan=$_REQUEST['jabatan'];
	$nama=$_REQUEST['nama'];
	$noktp=$_REQUEST['noktp'];
	$berlakusampai=$_REQUEST['berlakusampai'];
	
	
	//$noberita=$_REQUEST['noberita'];
	//$notambahanberita=$_REQUEST['notambahanberita'];
	//$tglberita=$_REQUEST['tglberita'];
	
	
	$seq=0;
	$strsql = "select cast(isnull(max(seq),0)as int)+1 as seq 
				from Tbl_DtlSusunanPengurusAnalisaYuridis where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}
	
	$strsql = " INSERT INTO Tbl_DtlSusunanPengurusAnalisaYuridis
				(custnomid,seq,jabatan,nama,noktp,berlakusampai)
				VALUES 
				('$custnomid','$seq','$jabatan','$nama','$noktp','$berlakusampai')
				";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_sp.php");
}
else if($btnname=="btnspdel")
{
	$seq=$_REQUEST['seq'];
	$noaktesusunanpengurus=$_REQUEST['noaktesusunanpengurus'];
	$tglaktasusunanpengurus=$_REQUEST['tglaktasusunanpengurus'];
	$namanotarissusunanpengurus=$_REQUEST['namanotarissusunanpengurus'];
	$strsql = "delete from Tbl_DtlSusunanPengurusAnalisaYuridis where seq='".$seq."' and custnomid='".$custnomid."'";
	echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_sp.php");
}
else if($btnname=="btnkpadd")
{
	$noaktekepemilikansaham=$_REQUEST['noaktekepemilikansaham'];
	$tglaktapemilikansaham=$_REQUEST['tglaktapemilikansaham'];
	$namanotaripemilikansaham=$_REQUEST['namanotaripemilikansaham'];
	$nama=$_REQUEST['nama'];
	$jumlahsaham=str_replace(",","",$_REQUEST['jumlahsaham']);
	$nominalsaham=$_REQUEST['nominalsaham'];
	$persentase=$_REQUEST['persentase'];
	
	
	//$noberita=$_REQUEST['noberita'];
	//$notambahanberita=$_REQUEST['notambahanberita'];
	//$tglberita=$_REQUEST['tglberita'];
	
	
	$seq=0;
	$strsql = "select cast(isnull(max(seq),0)as int)+1 as seq 
				from Tbl_DtlKepemilikanSahamAnalisaYuridis where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}
	
	$strsql = " INSERT INTO Tbl_DtlKepemilikanSahamAnalisaYuridis
				(custnomid,seq,nama,jumlahsaham,nominalsaham,persentase)
				VALUES 
				('$custnomid','$seq','$nama','$jumlahsaham','$nominalsaham','$persentase')
				";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_kp.php");
}
else if($btnname=="btnkpdel")
{
	$seq=$_REQUEST['seq'];
	$noaktekepemilikansaham=$_REQUEST['noaktekepemilikansaham'];
	$tglaktapemilikansaham=$_REQUEST['tglaktapemilikansaham'];
	$namanotaripemilikansaham=$_REQUEST['namanotaripemilikansaham'];
	$strsql = "delete from Tbl_DtlKepemilikanSahamAnalisaYuridis where seq='".$seq."' and custnomid='".$custnomid."'";
	echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require("return_ajax_kp.php");
}
else if($btnname=="btnsave")
{
	require ("../../requirepage/parameter.php");
	
	$rowuserlo=0;
	$strsql="select count(*) as 'v' from tbl_se_user where  user_id='".$userid."' and user_level_code='300'";
	echo $strsql."</br>";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$rowuserlo=$rows['v'];
		}
	}
	
	$rowuserslo=0;
	$strsql="select count(*) as 'v' from tbl_se_user where  user_id='".$userid."' and user_level_code='310'";
	echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$rowuserslo=$rows['v'];
		}
	}
	
	$tmpstrsql="";
	if($rowuserlo!="0")
	{
		$tmpstrsql.=" userlo='".$userid."', ";
	}
	
	if($rowuserslo!="0")
	{
		$tmpstrsql.=" userslo='".$userid."', ";
	}
	
	$nomenkeh=$_REQUEST['nomenkeh'];
	$tglmenkeh=$_REQUEST['tglmenkeh'];
	$noberitanegara=$_REQUEST['noberitanegara'];
	$notambahanberitanegara=$_REQUEST['notambahanberitanegara'];
	$tglberitanegara=$_REQUEST['tglberitanegara'];
	$tglsiup=$_REQUEST['tglsiup'];
	$tgltdp=$_REQUEST['tgltdp'];
	$tglberlakutdp=$_REQUEST['tglberlakutdp'];
	$noskdu=$_REQUEST['noskdu'];
	$tglskdu=$_REQUEST['tglskdu'];
	$tglberlakuskdu=$_REQUEST['tglberlakuskdu'];
	$noaktejabatanpengurus=$_REQUEST['noaktejabatanpengurus'];
	$tglaktajabatanpengurus=$_REQUEST['tglaktajabatanpengurus'];
	$namanotarisaktajabatanpengurus=$_REQUEST['namanotarisaktajabatanpengurus'];
	$notarisdiaktajabatanpengurus=$_REQUEST['notarisdiaktajabatanpengurus'];
	$pasalaktajabatanpengurus=$_REQUEST['pasalaktajabatanpengurus'];
	$noaktekewenanganpengurus=$_REQUEST['noaktekewenanganpengurus'];
	$tglkewenanganpengurus=$_REQUEST['tglkewenanganpengurus'];
	$namanotariskewenanganpengurus=$_REQUEST['namanotariskewenanganpengurus'];
	$notarisdikewenanganpengurus=$_REQUEST['notarisdikewenanganpengurus'];
	$pasalkewenanganpengurus=$_REQUEST['pasalkewenanganpengurus'];
	$noaktesusunanpengurus=$_REQUEST['noaktesusunanpengurus'];
	$tglaktasusunanpengurus=$_REQUEST['tglaktasusunanpengurus'];
	$namanotarissusunanpengurus=$_REQUEST['namanotarissusunanpengurus'];
	$noaktekepemilikansaham=$_REQUEST['noaktekepemilikansaham'];
	$tglaktapemilikansaham=$_REQUEST['tglaktapemilikansaham'];
	$namanotaripemilikansaham=$_REQUEST['namanotaripemilikansaham'];
	$jaminannote=$_REQUEST['jaminannote'];
	$lainlainnote=$_REQUEST['lainlainnote'];
	$kesimpulannote=$_REQUEST['kesimpulannote'];
	
	$strsql="
	update Tbl_AnalisaYuridis set
	$tmpstrsql
	nomenkeh='$nomenkeh',
	tglmenkeh='$tglmenkeh',
	noberitanegara='$noberitanegara',
	notambahanberitanegara='$notambahanberitanegara',
	tglberitanegara='$tglberitanegara',
	tglsiup='$tglsiup',
	tgltdp='$tgltdp',
	tglberlakutdp='$tglberlakutdp',
	noskdu='$noskdu',
	tglskdu='$tglskdu',
	tglberlakuskdu='$tglberlakuskdu',
	noaktejabatanpengurus='$noaktejabatanpengurus',
	tglaktajabatanpengurus='$tglaktajabatanpengurus',
	namanotarisaktajabatanpengurus='$namanotarisaktajabatanpengurus',
	notarisdiaktajabatanpengurus='$notarisdiaktajabatanpengurus',
	pasalaktajabatanpengurus='$pasalaktajabatanpengurus',
	noaktekewenanganpengurus='$noaktekewenanganpengurus',
	tglkewenanganpengurus='$tglkewenanganpengurus',
	namanotariskewenanganpengurus='$namanotariskewenanganpengurus',
	notarisdikewenanganpengurus='$notarisdikewenanganpengurus',
	pasalkewenanganpengurus='$pasalkewenanganpengurus',
	noaktesusunanpengurus='$noaktesusunanpengurus',
	tglaktasusunanpengurus='$tglaktasusunanpengurus',
	namanotarissusunanpengurus='$namanotarissusunanpengurus',
	noaktekepemilikansaham='$noaktekepemilikansaham',
	tglaktapemilikansaham='$tglaktapemilikansaham',
	namanotaripemilikansaham='$namanotaripemilikansaham',
	jaminannote='$jaminannote',
	lainlainnote='$lainlainnote',
	kesimpulannote='$kesimpulannote'
	where custnomid='$custnomid'";
	echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);	
	require("../../requirepage/do_saveflow.php");
	header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}
?>