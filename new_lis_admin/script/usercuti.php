<?php
	require_once ("../lib/class.sqlserver.php");
	
	$db_lis = new SQLSRV();
	$db_lis->connect();

	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>LIS PARAM</title>
		<LINK media=screen href="../style/menu.css" rel=stylesheet>
		<script src='./javabits.js' language='Javascript'></script>
		<script language="javascript" src="../js/jquery-1.7.2.min.js"></script>
		<script language="javascript" src="../js/datetimepicker_css.js"></script>
		<script language="javascript" src="../js/full_function.js"></script>
	</head>
	<body style="margin:0;">
		<form name="formsec" method="post" autocomplete="off">
			<table align="center" width="1000" border="0">
				<tr style="margin-bottom:px;">
					<td align="center">
						<img src="../images/Header Mega (L).png" width=100%>
					</td>
				</tr>
				<tr>
					<td>
					   <A HREF="javascript:changeMenu('../../menu.php')"><h5>Back To Main</h5></A>
					</td>
				</tr>
				<tr>
					<td align="center">
						<h3>MANAGE USER CUTI</h3>
					</td>
				</tr>
				<tr>
					<td align="center">
						Search UID / UName : &nbsp
						<input type="text" name="txt_userid" id="txt_userid" size="50" />
						<input type="button" onClick="dataLoad()" value="Search" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<div id="content_body">
						</div>
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="userid" value="<? echo $userid; ?>" />
			<input type="hidden" name="userpwd" value="<? echo $userpwd; ?>" />
			<input type="hidden" name="theid" id="theid" />
		</form>
		
		<script type="text/javascript">
			$('#txt_userid').keypress(function(e) {
			    if (e.keyCode == 13) {
			        e.preventDefault();
			        dataLoad();
			    }
			});

			function dataLoad() {
				var a = "CDL";
				var thesearch = document.getElementById("txt_userid").value;
				$.ajax({
					type: "POST",
					url: "inc_ajax.php",
					data: "a="+a+"&s="+thesearch+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{	
						document.getElementById("content_body").innerHTML = response;
					}
				});			
			}

			function viewDetail(theid) {
				var a = "CDL1";
				var thesearch = theid;
				$.ajax({
					type: "POST",
					url: "inc_ajax.php",
					data: "a="+a+"&s="+thesearch+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{	
						document.getElementById("content_body").innerHTML = response;
					}
				});			
			}

			function execProcess(theid){
				var status = document.getElementById("s_status").value;

				document.formsec.theid.value = theid;
				document.formsec.action = "./dousercuti.php";
				document.formsec.submit();
			}
		</script>
	</body>
</html>