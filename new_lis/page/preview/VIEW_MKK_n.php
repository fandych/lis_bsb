<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$tanggalkunjungan = "";
	$cabang = "";
	$namanasabah = "";
	$custsex = "";
	
	$tsql = "select * from Tbl_customermasterperson2 where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
			$custsex = $row["custsex"];
			
			if($custsex == "0")
			{
				$namanasabah = $row["custbusname"];
			}
			else
			{
				$namanasabah = $row["custfullname"];
			}
		}
	}
	
	$mkk_kebutuhan = "";
	$mkk_character = "";
	$mkk_condition = "";
	$mkk_capacity = "";
	$mkk_capital = "";
	$mkk_collateral = "";
	$rekomen = "";
	
	$tsql = "select * from Tbl_MKKAnalisa5c where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$mkk_kebutuhan = $row['analisa5c_kebutuhan'];
			$mkk_character = $row['analisa5c_character'];
			$mkk_condition = $row['analisa5c_condition'];
			$mkk_capacity = $row['analisa5c_capacity'];
			$mkk_capital = $row['analisa5c_capital'];
			$mkk_collateral = $row['analisa5c_collateral'];
			$rekomen = $row['analisa5c_rekomendasi'];
		}
	}
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MKK</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="950px" align="center" style="border:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">MEMO KEPUTUSAN KREDIT</td>
</tr>
<tr>
	<td width="25%">Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td width="25%">Nama Nasabah</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $namanasabah;?></div></td>
</tr>
<tr>
	<td width="25%">No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<tr>
	<td width="25%">Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td> 
</tr>
		<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
		<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
		<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
		<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
		<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
		<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
		<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
		<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
	
</table>
<table width="950px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">SUMMARY HASIL ANALISA 5C</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="formentry" name="formentry" method="post">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center"><strong>No.</strong></td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"><strong>Hasil Analisa</strong></td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"><strong>Hasil</strong></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><strong>Keterangan</strong></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">1</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Kebutuhan</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_kebutuhan;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">2</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Character</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_character;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">3</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Condition</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_condition;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">4</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Capacity</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_capacity;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">5</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Capital</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_capital;?></td>
			</tr>
			<tr>
				<td width="15%" style="border:1px solid black;width:30px;" align="center">6</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center">Collateral</td>
				<td width="15%" style="border:1px solid black;width:150px;" align="center"></td>
				<td width="15%" style="border:1px solid black;width:400px;" align="center"><? echo $mkk_collateral;?></td>
			</tr>
			<tr>
				<td colspan="4" align="center"><!--<input type="button" value="SIMPAN" style="width:120px;background-color:blue;color:white;" onclick="yuria()" />--></td>
			</tr>
			<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
			<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
			<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
			<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
			<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
			<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
			<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
			<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="950px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS BARU KREDIT USAHA KECIL</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;
				
				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{  
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{	
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];		

						$biayaadmbaru = $plafondbaru * 0.05;
				
						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}
						
						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";
					
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{  
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}
						
						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{  
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}
						
						$angsuranbaru = ($plafondbaru+($plafondbaru*$sukubungabaru*($jangkawaktubaru/12)))/$jangkawaktubaru;
						
						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $plafondbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru;?> %</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $biayaadmbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $angsuranbaru;?></td>
			</tr>
			
			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo $totalplafond;?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo $totalangsuran;?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="950px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">TOTAL FASILITAS DIBANK MEGA (BARU + EXIST)</td>
</tr>
<tr>
	<td colspan="2" style="">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Plafond (RP)</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Suku Bunga</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya Adm.</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Tujuan</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jk Wkt</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Provisi</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Angsuran</strong></td>
			</tr>
			<?
				$jenisfasilitasbaru = "";
				$plafondbaru = "";
				$sukubungabaru = "";
				$biayaadmbaru = 0;
				$tujuanbaru = "";
				$jangkawaktubaru = "";
				$provisibaru = "1.00% FLAT";
				$angsuranbaru = "";
				$totalplafond = 0;
				$totalangsuran = 0;
				
				$tsql = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{  
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{	
						$jenisfasilitasbaru = $row["custcredittype"];
						$plafondbaru = $row["custcreditplafond"];
						$sukubungabaru = $row["sukubungayangdiberikan"];
						$tujuanbaru = $row["custcreditneed"];
						$jangkawaktubaru = $row["custcreditlong"];		

						$biayaadmbaru = $plafondbaru * 0.05;
				
						if($biayaadmbaru < 100000)
						{
							$biayaadmbaru = 100000;
						}
						
						$tsqlproduk = "select produk_type_description from Tbl_KodeProduk where produk_loan_type = '$jenisfasilitasbaru'";
					
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{  
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasbaru = $rowproduk["produk_type_description"];
							}
						}
						
						$tsqlproduk = "select credit_need_name from Tbl_CreditNeed where credit_need_code = '$tujuanbaru'";
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{  
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$tujuanbaru = $rowproduk["credit_need_name"];
							}
						}
						
						$angsuranbaru = ($plafondbaru+($plafondbaru*$sukubungabaru*($jangkawaktubaru/12)))/$jangkawaktubaru;
						
						$totalplafond = $totalplafond + $plafondbaru;
						$totalangsuran = $totalangsuran + $angsuranbaru;

			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $plafondbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungabaru;?> %</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $biayaadmbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanbaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktubaru;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisibaru;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $angsuranbaru;?></td>
			</tr>
			
			<?
					}
				}
			?>
			<?
				$jenisfasilitasexist = "";
				$plafondexist = "";
				$sukubungaexist = "";
				$biayaadmexist = "-";
				$tujuanexist = "";
				$jangkawaktuexist = "";
				$provisiexist = "1.00% FLAT";
				$angsuranexist = "";
				
				$tsql = "select * from Tbl_LKCDFasilitasPinjamanBankmega2 where custnomid = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{  
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{	
						$jenisfasilitasexist = $row["FasilitasPinjamanBankMega_jenisfasilitas"];
						$plafondexist = $row["FasilitasPinjamanBankMega_plafondawal"];
						$sukubungaexist = $row["FasilitasPinjamanBankMega_rate"];
						$jangkawaktuexist = $row["FasilitasPinjamanBankMega_tenor"];	
						$angsuranexist = $row["FasilitasPinjamanBankMega_angsuran"];

						$totalplafond = $totalplafond + $plafondexist;
						$totalangsuran = $totalangsuran + $angsuranexist;
						
						if($jenisfasilitasexist == "1" || $jenisfasilitasexist == "2" || $jenisfasilitasexist == "3" || $jenisfasilitasexist == "6")
						{
							$tujuanexist = "Modal Kerja";
						}
						else if ($jenisfasilitasexist == "4" || $jenisfasilitasexist == "5")
						{
							$tujuanexist = "Investasi";
						}
						else
						{
							$tujuanexist = "Konsumtif";
						}
						
						$tsqlproduk = "select JenisFasilitasBankMega_nama from Tbl_LKCDJenisFasilitasBankMega where JenisFasilitasBankMega_code = '$jenisfasilitasexist'";
					
						$aproduk = sqlsrv_query($conn, $tsqlproduk);

						  if ( $aproduk === false)
						  die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($aproduk))
						{  
							if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
							{
								$jenisfasilitasexist = $rowproduk["JenisFasilitasBankMega_nama"];
							}
						}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisfasilitasexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $plafondexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $sukubungaexist;?> %</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $biayaadmexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tujuanexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jangkawaktuexist;?> bulan</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $provisiexist;?></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $angsuranexist;?></td>
			</tr>
			<?
					}
				}
			?>
			<tr>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Total Fasilitas</strong></td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo $totalplafond;?></strong></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong><? echo $totalangsuran;?></strong></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<table width="950px" align="center" style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;background-color:gray;">REKOMENDASI KREDIT OFFICER</td>
</tr>
<tr>
	<td colspan="2" style="">
		<form id="rekomendasi" name="rekomendasi">
		<table width="96%" align="center" style="border:0px solid black;" class="preview2">
		<tr>
			<td>Catatan :</td>
		</tr>
		<?
				$notes = "";
				
				$tsql = "select * from tbl_FMKK where txn_id = '$custnomid'";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{  
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$notes = $row["txn_notes"];
					}
				}
			
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:30px;height:30px;" align="left"><? echo $notes; ?></td>
		</tr>
		<tr>
			<td align="right">
				<div width="15%" style="border:1px solid black;width:200px;" align="center"><? echo $rekomen ?></div>
			</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
			<?
				$kodejabatan = "";
				$namajabatan = "";
				$notejabatan = "";
				
				$tsql = "SELECT * FROM TBL_SE_LEVEL WHERE LEVEL_CODE  LIKE '1%' AND LEVEL_CODE NOT IN ('110', '150', '160', '170') ORDER BY LEVEL_CODE DESC";
				$a = sqlsrv_query($conn, $tsql);
				if ( $a === false)
				die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($a))
				{  
					while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
					{
						$namajabatan = $row["level_name"];
						$kodejabatan = $row["level_code"];
						
						$tsql2 = "SELECT * FROM tbl_Plafond_Checklevel WHERE txn_id = '$custnomid' AND txn_user_level = '$kodejabatan'";
						$a2 = sqlsrv_query($conn, $tsql2);
						if ( $a2 === false)
						die( FormatErrors( sqlsrv_errors() ) );

						if(sqlsrv_has_rows($a2))
						{  
							if($row2 = sqlsrv_fetch_array($a2, SQLSRV_FETCH_ASSOC))
							{
								$notejabatan = $row2["txn_notes"];
							}
						}
						else
						{
							$notejabatan = "";
						}
							
			?>
				<table width="100%" align="center" style="border:1px solid black;" class="preview2" cellspacing="0" >
				<tr>
					<td width="15%" colspan="2" style="border:1px solid black;width:100px;background-color:gray;" align="center"><strong>PERSETUJUAN <? echo $namajabatan;?></strong></td>
				</tr>
				<tr>
					<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TTD</strong><br><br><br><br><br><br><br><? echo $namajabatan;?></td>
					<td width="15%" style="border:1px solid black;width:200px;" align="center" valign="top"><strong>SETUJU / TIDAK SETUJU</strong><br>Catatan :<br><br><? echo $notejabatan;?></td>
				</tr>
				</table><br>
			<?
							
					}
				}
			?>
			</td>
		</tr>
		<tr>
			<td align="center">&nbsp;</td>
		</tr>
		<?
			$statuspermission = "";
			$tsql = "SELECT * FROM Tbl_FMKK where txn_id = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);
			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$statuspermission = $row["txn_action"];
				}
			}
			
			if($statuspermission == "I")
			{
		?>
		<tr>
			<td align="center"><input type="button" value="APPROVE" style="width:120px;background-color:blue;color:white;" onclick="yuria();" /></td>
		</tr>
		<?
			}
			else if($statuspermission == "A")
			{
		?>
		<!--<tr>
			<td align="center" style="color:red;font-size:20px"><i>Telah Diapprove</i></td>
		</tr>-->
		<?
			}
		?>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2" style="">&nbsp;</td>
</tr>
</table>
<script type="text/javascript">
	function yuria() {
		var nan="VIEWMKK";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		$.ajax({
			type: "POST",
			url: "MKK_ajax_n.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				alert('Berhasil Diapprove');
				window.location.reload();
			}

		})
	}
	
</script>
</body>
</html> 