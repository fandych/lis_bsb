<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");
	require_once ("../../requirepage/parameter.php");
	
	$view = $_GET['view'];
	$taginput = "";
	$tagselect = "";
	$warna = "class=\"input\"";
	$button = "type=\"button\"";
	$tampil = "1";
	
	if($view == "preview")
	{
		$taginput = "readonly=\"readonly\"";
		$tagselect = "disabled=\"disabled\"";
		$warna = "class=\"preview2\"";
		$button = "type=\"hidden\"";
		$tampil = "0";
	}
	
	$custcurcode="IDR";
	$strsql = "select * From tbl_customermasterperson where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcurcode=$rows['custcurcode'];
		}
	}

	
	
	$custsex="";
	$strsql="select * from Tbl_CustomerMasterPerson where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custsex=$rows['custsex'];
		}
	}
	if ($custsex=="0")
	{
		$tmpwhere="where status_code like '%B%'";
	}
	else
	{
		$tmpwhere="where status_code like '%P%'";
	}
	
	
	$newlkcdsektorindustri="";
	$newlkcdjenisusaha="";
	$newlkcdlamausahayear=0;
	$newlkcdlamausahamonth=0;
	$newlkcdkepemilikan="";
	$newlkcdjumlahharikerjausaha=0;
	$newlkcdjenisjumlahharikerjausaha=0;
	$newlkcdratajumlahpembeli=0;
	$newlkcdratapembelian=0;
	$newlkcdkenaikanjumlahpembeli=0;
	$newlkcdkenaikantiketsize=0;
	$newlkcdjenishpp=0;
	$newlkcdhpp=0;
	$newlkcdmaginusaha=0;
	$newlkcdgajirataperpegawai=0;
	$newlkcdjumlahkaryawan=0;
	$newlkcdbiayasewa=0;
	$newlkcdbiayatelepon=0;
	$newlkcdsampah=0;
	$newlkcdoperasionallainnya=0;
	$newlkcdbiayarumahtangga=0;
	$strsql="select * from Tbl_LKCDDataKeuangan2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$newlkcdsektorindustri=$rows['lkcdsektorindustri'];
			$newlkcdjenisusaha=$rows['lkcdjenisusaha'];
			$newlkcdlamausahayear=$rows['lkcdlamausahayear'];
			$newlkcdlamausahamonth=$rows['lkcdlamausahamonth'];
			$newlkcdkepemilikan=$rows['lkcdkepemilikan'];
			$newlkcdjumlahharikerjausaha=$rows['lkcdjumlahharikerjausaha'];
			$newlkcdjenisjumlahharikerjausaha=$rows['lkcdjenisjumlahharikerjausaha'];
			$newlkcdratajumlahpembeli=$rows['lkcdratajumlahpembeli'];
			$newlkcdratapembelian=$rows['lkcdratapembelian'];
			$newlkcdkenaikanjumlahpembeli=$rows['lkcdkenaikanjumlahpembeli'];
			$newlkcdkenaikantiketsize=$rows['lkcdkenaikantiketsize'];
			$newlkcdjenishpp=$rows['lkcdjenishpp'];
			$newlkcdhpp=$rows['lkcdhpp'];
			$newlkcdmaginusaha=$rows['lkcdmaginusaha'];
			$newlkcdgajirataperpegawai=$rows['lkcdgajirataperpegawai'];
			$newlkcdjumlahkaryawan=$rows['lkcdjumlahkaryawan'];
			$newlkcdbiayasewa=$rows['lkcdbiayasewa'];
			$newlkcdbiayatelepon=$rows['lkcdbiayatelepon'];
			$newlkcdsampah=$rows['lkcdsampah'];
			$newlkcdoperasionallainnya=$rows['lkcdoperasionallainnya'];
			$newlkcdbiayarumahtangga=$rows['lkcdbiayarumahtangga'];
		}
	}
	
	$lkcdsektorindustri="";
	$lkcdjenisusaha="";
	$lkcdlamausahayear=0;
	$lkcdlamausahamonth=0;
	$lkcdkepemilikan="";
	$lkcdjumlahharikerjausaha=0;
	$lkcdjenisjumlahharikerjausaha=0;
	$lkcdratajumlahpembeli=0;
	$lkcdratapembelian=0;
	$lkcdkenaikanjumlahpembeli=0;
	$lkcdkenaikantiketsize=0;
	$lkcdjenishpp=0;
	$lkcdhpp=0;
	$lkcdmaginusaha=0;
	$lkcdgajirataperpegawai=0;
	$lkcdjumlahkaryawan=0;
	$lkcdbiayasewa=0;
	$lkcdbiayatelepon=0;
	$lkcdsampah=0;
	$lkcdoperasionallainnya=0;
	$lkcdbiayarumahtangga=0;
	$strsql="select * from Tbl_LKCDDataKeuangan where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$lkcdsektorindustri=$rows['lkcdsektorindustri'];
			$lkcdjenisusaha=$rows['lkcdjenisusaha'];
			$lkcdlamausahayear=$rows['lkcdlamausahayear'];
			$lkcdlamausahamonth=$rows['lkcdlamausahamonth'];
			$lkcdkepemilikan=$rows['lkcdkepemilikan'];
			$lkcdjumlahharikerjausaha=$rows['lkcdjumlahharikerjausaha'];
			$lkcdjenisjumlahharikerjausaha=$rows['lkcdjenisjumlahharikerjausaha'];
			$lkcdratajumlahpembeli=$rows['lkcdratajumlahpembeli'];
			$lkcdratapembelian=$rows['lkcdratapembelian'];
			$lkcdkenaikanjumlahpembeli=$rows['lkcdkenaikanjumlahpembeli'];
			$lkcdkenaikantiketsize=$rows['lkcdkenaikantiketsize'];
			$lkcdjenishpp=$rows['lkcdjenishpp'];
			$lkcdhpp=$rows['lkcdhpp'];
			$lkcdmaginusaha=$rows['lkcdmaginusaha'];
			$lkcdgajirataperpegawai=$rows['lkcdgajirataperpegawai'];
			$lkcdjumlahkaryawan=$rows['lkcdjumlahkaryawan'];
			$lkcdbiayasewa=$rows['lkcdbiayasewa'];
			$lkcdbiayatelepon=$rows['lkcdbiayatelepon'];
			$lkcdsampah=$rows['lkcdsampah'];
			$lkcdoperasionallainnya=$rows['lkcdoperasionallainnya'];
			$lkcdbiayarumahtangga=$rows['lkcdbiayarumahtangga'];
		}
	}
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		
			function totalgajipegawai()
			{
				var tmpjmlkaryawan = $("#lkcdjumlahkaryawan").val();
				var tmpjmlkaryawan = tmpjmlkaryawan.replace(/,/g,"");
				var tmpgajikaryawan = $("#lkcdgajirataperpegawai").val();
				var tmpgajikaryawan = tmpgajikaryawan.replace(/,/g,"");
				
				if(tmpjmlkaryawan=="" || tmpjmlkaryawan=="0")
				{
					var jmlkaryawan=0;
				}
				else
				{
					var jmlkaryawan=parseFloat(tmpjmlkaryawan);;
				}
				
				
				if(tmpgajikaryawan=="" || tmpgajikaryawan=="0")
				{
					var gajikaryawan=0;
				}
				else
				{
					var gajikaryawan=parseFloat(tmpgajikaryawan);;
				}
				
				//alert(tmpgajikaryawan+"    "+ tmpgajikaryawan+"     ")
				var ttlgajikaryawan=parseFloat(jmlkaryawan)*parseFloat(gajikaryawan);
				//alert(tmpgajikaryawan+"    "+ tmpgajikaryawan+"     "+ttlgajikaryawan)
				document.getElementById("lblgaji").value=ttlgajikaryawan;
			}
			
		function validation()
		{
			var shr=$("#lkcdhpp").val();
			var jenishpp=$("#lkcdjenishpp").val();
			var FormName="formentry";	
			var StatusAllowSubmit=true;
			var elem = document.getElementById(FormName).elements;
			for(var i = 0; i < elem.length; i++)
			{
				if(elem[i].style.backgroundColor=="#ff0")
				{
					
					if(elem[i].value == "")
					{
						alert(elem[i].alt + " HARUS DIISI");
						elem[i].focus();
						StatusAllowSubmit=false				
						break;
					}
				}
			}
			if(jenishpp=="2")
			{
				var Chars ="0123456789.";
				var hitungkoma = 0;
				for (var i = 0; i < eval("document.getElementById('lkcdhpp').value.length"); i++)
				{
					if (Chars.indexOf(eval("document.getElementById('lkcdhpp').value.charAt(i)")) == -1)
					{
						alert("Harus Berupa Angka atau .");
						$("#lkcdhpp").focus();
						StatusAllowSubmit=false;
						return false;
					}
					if (eval("document.getElementById('lkcdhpp').value.substring(i,i+1)") == ".")
					{
						hitungkoma++;
					}
				}
				if (hitungkoma > 1)
				{
					alert("Koma 1 saja");
					$("#lkcdhpp").focus();
					StatusAllowSubmit=false;
					return false;
				}
				varlen = eval("document.getElementById('lkcdhpp').value.length");
				
				if (eval("document.getElementById('lkcdhpp').value.substring(varlen-1)") == ".")
				{
					alert("Koma jangan Di belakang");
					$("#lkcdhpp").focus();
					StatusAllowSubmit=false;
					return false;
				}
				else if(shr >99)
				{
					alert("Tidak boleh di atas 100 %");
					$("#lkcdhpp").focus();
					StatusAllowSubmit=false;
					return false;
				}	
			}
			
			
			var Chars ="0123456789.";
			var hitungkoma = 0;
			for (var i = 0; i < eval("document.getElementById('lkcdkenaikanjumlahpembeli').value.length"); i++)
			{
				if (Chars.indexOf(eval("document.getElementById('lkcdkenaikanjumlahpembeli').value.charAt(i)")) == -1)
				{
					alert("Harus Berupa Angka atau .");
					$("#lkcdkenaikanjumlahpembeli").focus();
					StatusAllowSubmit=false;
					return false;
				}
				if (eval("document.getElementById('lkcdkenaikanjumlahpembeli').value.substring(i,i+1)") == ".")
				{
					hitungkoma++;
				}
			}
			if (hitungkoma > 1)
			{
				alert("Koma 1 saja");
				$("#lkcdkenaikanjumlahpembeli").focus();
				StatusAllowSubmit=false;
				return false;
			}
			varlen = eval("document.getElementById('lkcdkenaikanjumlahpembeli').value.length");
			
			if (eval("document.getElementById('lkcdkenaikanjumlahpembeli').value.substring(varlen-1)") == ".")
			{
				alert("Koma jangan Di belakang");
				$("#lkcdkenaikanjumlahpembeli").focus();
				StatusAllowSubmit=false;
				return false;
			}
			else if(shr >99)
			{
				alert("Tidak boleh di atas 100 %");
				$("#lkcdkenaikanjumlahpembeli").focus();
				StatusAllowSubmit=false;
				return false;
			}
			
			var Chars ="0123456789.";
			var hitungkoma = 0;
			for (var i = 0; i < eval("document.getElementById('lkcdkenaikantiketsize').value.length"); i++)
			{
				if (Chars.indexOf(eval("document.getElementById('lkcdkenaikantiketsize').value.charAt(i)")) == -1)
				{
					alert("Harus Berupa Angka atau .");
					$("#lkcdkenaikantiketsize").focus();
					StatusAllowSubmit=false;
					return false;
				}
				if (eval("document.getElementById('lkcdkenaikantiketsize').value.substring(i,i+1)") == ".")
				{
					hitungkoma++;
				}
			}
			if (hitungkoma > 1)
			{
				alert("Koma 1 saja");
				$("#lkcdkenaikantiketsize").focus();
				StatusAllowSubmit=false;
				return false;
			}
			varlen = eval("document.getElementById('lkcdkenaikantiketsize').value.length");
			
			if (eval("document.getElementById('lkcdkenaikantiketsize').value.substring(varlen-1)") == ".")
			{
				alert("Koma jangan Di belakang");
				$("#lkcdkenaikantiketsize").focus();
				StatusAllowSubmit=false;
				return false;
			}
			else if(shr >99)
			{
				alert("Tidak boleh di atas 100 %");
				$("#lkcdkenaikantiketsize").focus();
				StatusAllowSubmit=false;
				return false;
			}
			
			var Chars ="0123456789.";
			var hitungkoma = 0;
			for (var i = 0; i < eval("document.getElementById('lkcdmaginusaha').value.length"); i++)
			{
				if (Chars.indexOf(eval("document.getElementById('lkcdmaginusaha').value.charAt(i)")) == -1)
				{
					alert("Harus Berupa Angka atau .");
					$("#lkcdmaginusaha").focus();
					StatusAllowSubmit=false;
					return false;
				}
				if (eval("document.getElementById('lkcdmaginusaha').value.substring(i,i+1)") == ".")
				{
					hitungkoma++;
				}
			}
			if (hitungkoma > 1)
			{
				alert("Koma 1 saja");
				$("#lkcdmaginusaha").focus();
				StatusAllowSubmit=false;
				return false;
			}
			varlen = eval("document.getElementById('lkcdmaginusaha').value.length");
			
			if (eval("document.getElementById('lkcdmaginusaha').value.substring(varlen-1)") == ".")
			{
				alert("Koma jangan Di belakang");
				$("#lkcdmaginusaha").focus();
				StatusAllowSubmit=false;
				return false;
			}
			else if(shr >99)
			{
				alert("Tidak boleh di atas 100 %");
				$("#lkcdmaginusaha").focus();
				StatusAllowSubmit=false;
				return false;
			}
			
			if(StatusAllowSubmit == true)
			{			
				document.getElementById(FormName).action = "DO_LKCD_VID4IDENTIFIKASIUSAHACALONDEBITUR_E.php";
				submitform = window.confirm("<? echo $confmsg;?>")
				if (submitform == true)
				{
					document.getElementById(FormName).submit();
					return true;
				}
				else
				{
					return false;
				} 
			}
		}
		</script>
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">	
			<table align="center" style="border:1px solid black; width:1000px;" border="0" <? echo $warna;?>>
				<tr>
					<td colspan="5" style="font-size:12pt;font-weight:bold;">
						DATA USAHA
						<div>&nbsp;</div>
					</td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight:bold;">Segmentasi</td>
				</tr>
				<tr>
					<td>Sektor Industri</td>
					<td>
							<?
							$strsql = "select * From Tbl_SektorIndustri where sektor_code='$lkcdsektorindustri'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['sektor_nama'];
								
								}
							}
							?>
					</td>
					<td colspan="3">
						<select <? echo $tagselect; ?>alt="Sektor Industri " id="lkcdsektorindustri" name="lkcdsektorindustri" style="background:#ff0; width:200px;">
							<?
							$strsql = "select * From Tbl_SektorIndustri";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newlkcdsektorindustri==$rows['sektor_code'])
									{
										echo '<option value="'.$rows['sektor_code'].'" selected="selected">'.$rows['sektor_nama'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['sektor_code'].'">'.$rows['sektor_nama'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Jenis Usaha</td>
					<td><?echo $lkcdjenisusaha; ?></td>
					<td colspan="3"><input alt="Jenis Usaha" style="background-color:#FF0;width:200px;" type="text" <? echo $taginput; ?> name="lkcdjenisusaha" id="lkcdjenisusaha" value="<?echo $newlkcdjenisusaha; ?>" maxlength="50"></td>
				</tr>
				<tr>
					<td>Lama usaha</td>
					<td>
						<? echo $lkcdlamausahayear; ?>  Tahun
						<? echo $lkcdlamausahamonth; ?>  Bulan
					</td>
					<td colspan="3">
						<input alt="Lama usaha Year " type="text" <? echo $taginput; ?> id="lkcdlamausahayear" name="lkcdlamausahayear" value="<? echo $newlkcdlamausahayear; ?>" onkeypress="return isNumberKey(event);" style="background:#ff0; width:50px;" maxlength="3" />  Tahun
						<input alt="Lama usaha Bulan " type="text" <? echo $taginput; ?> id="lkcdlamausahamonth" name="lkcdlamausahamonth" value="<? echo $newlkcdlamausahamonth; ?>" onkeypress="return isNumberKey(event);" style="background:#ff0; width:50px;" maxlength="3" />  Bulan
					</td>
				</tr>
				<tr>
					<td>Kepemilikan</td>
					<td>
							<?
							$strsql = "select * From tblstatusrumah where status_code='$lkcdkepemilikan'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['status_name'];
								}
							}
							?>
					</td>
					<td colspan="3">
						<select <? echo $tagselect; ?>alt="Kepemilikan " id="lkcdkepemilikan" name="lkcdkepemilikan" style="background:#ff0; width:200px;">
							<?
							$strsql = "select * From tblstatusrumah $tmpwhere";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newlkcdkepemilikan==$rows['status_code'])
									{
										echo '<option value="'.$rows['status_code'].'" selected="selected">'.$rows['status_name'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['status_code'].'">'.$rows['status_name'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="width:45%;">&nbsp;</td>
					<td style="width:20%;">&nbsp;</td>
					<td style="width:5%;">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight:bold;">IDENTIFIKASI DATA - DATA KEUANGAN CALON DEBITUR</td>
				</tr>
				<tr>
					<td>Jumlah hari kerja usaha/ jumlah hari penjualan</td>
					<td>
						<?echo $lkcdjumlahharikerjausaha?> kali dalam 1 
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							$opt3="";
							if ($lkcdjenisjumlahharikerjausaha=="1")
							{
								echo 'Minggu';
							}
							else if($lkcdjenisjumlahharikerjausaha=="2")
							{
								echo 'Bulan';
							}
							else if($lkcdjenisjumlahharikerjausaha=="3")
							{
								echo 'Year';
							}
						?>
					</td>
					<td colspan="3">
						<input alt="Jumlah hari kerja usaha/ jumlah hari penjualan " type="text" <? echo $taginput; ?> style=" background:#ff0;width:50px;" id="lkcdjumlahharikerjausaha" name="lkcdjumlahharikerjausaha" maxlength="5" value="<?echo $lkcdjumlahharikerjausaha?>" onkeypress="return isNumberKey(event);" />kali dalam 1 
						<select <? echo $tagselect; ?>id="lkcdjenisjumlahharikerjausaha" name="lkcdjenisjumlahharikerjausaha" style="background:#ff0;width:75px;">
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							$opt3="";
							if ($newlkcdjenisjumlahharikerjausaha=="1")
							{
								$opt1=$selected;
							}
							else if($newlkcdjenisjumlahharikerjausaha=="2")
							{
								$opt2=$selected;
							}
							else if($newlkcdjenisjumlahharikerjausaha=="3")
							{
								$opt3=$selected;
							}
						?>
							<option <?echo $opt1?> value="1">Minggu</option>
							<option <?echo $opt2?> value="2">Bulan</option>
							<option <?echo $opt3?> value="3">Year</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Rata-rata jumlah pembeli/pemesanan per-penjualan</td>
					<td><?echo $lkcdratajumlahpembeli?>  CUSTOMER</td>
					<td colspan="3"><input alt="Rata-rata jumlah pembeli/pemesanan per-penjualan " type="text" <? echo $taginput; ?> style=" background:#ff0;width:50px;" id="lkcdratajumlahpembeli" name="lkcdratajumlahpembeli" value="<?echo $newlkcdratajumlahpembeli?>" onkeypress="return isNumberKey(event);" maxlength="4" />  CUSTOMER</td>
				</tr>
				<tr>
					<td>Rata-rata pembelian/belanja per-customer/Ticket size</td>
					<td><?echo $custcurcode?> <?echo numberFormat($lkcdratapembelian)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Rata-rata pembelian/belanja per-customer/Ticket size " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdratapembelian" name="lkcdratapembelian" value="<?echo numberFormat($newlkcdratapembelian)?>" maxlength="15" onkeypress="return isNumberKey(event);" onkeyup="currency(this.id)" /></td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight:bold;">
						<div>&nbsp;<div>
						Estimasi Proyeksi Omset penjualan usaha
					</td>
				</tr>
				<tr>
					<td>Kenaikan jumlah pembeli/pemesan</td>
					<td><?echo $lkcdkenaikanjumlahpembeli.'&nbsp;%'?></td>
					<td colspan="3"><input alt="Kenaikan jumlah pembeli/pemesan " type="text" <? echo $taginput; ?> style=" background:#ff0;width:50px;" id="lkcdkenaikanjumlahpembeli" name="lkcdkenaikanjumlahpembeli" value="<?echo $newlkcdkenaikanjumlahpembeli?>" maxlength="5" />&nbsp;%</td>
				</tr>
				<tr>
					<td>Kenaikan Ticket size</td>
					<td><?echo $lkcdkenaikanjumlahpembeli.'&nbsp;%'?></td>
					<td colspan="3"><input alt="Kenaikan Ticket size " type="text" <? echo $taginput; ?> style=" background:#ff0;width:50px;" id="lkcdkenaikantiketsize" name="lkcdkenaikantiketsize" value="<?echo $newlkcdkenaikantiketsize?>" maxlength="5" />&nbsp;%</td>
				</tr>
				<tr>
					<td>Harga pokok penjualan</td>
					<td>
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							if ($lkcdjenishpp=="1")
							{
								echo $custcurcode;
							}
							else if($lkcdjenishpp=="2")
							{
								echo '%';
							}
						
						echo '&nbsp;&nbsp;&nbsp;'.numberFormat($lkcdhpp)?>
					</td>
					<td>
					<select <? echo $tagselect; ?>alt="Pilihan Jumlah hari kerja usaha/ jumlah hari penjualan " id="lkcdjenishpp" name="lkcdjenishpp" style="background:#ff0;width:50px;">
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							if ($lkcdjenishpp=="1")
							{
								$opt1=$selected;
							}
							else if($lkcdjenishpp=="2")
							{
								$opt2=$selected;
							}
						?>
							<option <?echo $opt1?> value="1"><?echo $custcurcode?></option>
							<option <?echo $opt2?> value="2">%</option>
						</select>
					</td>
					<td colspan="2">
						<input alt="Harga pokok penjualan " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdhpp" name="lkcdhpp" value="<? echo numberFormat($newlkcdhpp)?>" onkeyup="currency(this.id)" maxlength="15" />
					</td>
				</tr>
				<tr>
					<td style="font-weight:bold;">
						Persentase Margin Usaha
					<td><?echo $lkcdmaginusaha.'&nbsp;%'?></td>
					<td colspan="3"><input alt="Persentase Margin Usaha " type="text" <? echo $taginput; ?> style=" background:#ff0;width:50px;" id="lkcdmaginusaha" name="lkcdmaginusaha" value="<? echo numberFormat($newlkcdmaginusaha)?>" maxlength="5" />&nbsp;%</td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight:bold;">
						<div>&nbsp;<div>
						Biaya Operasional Usaha
					</td>
				</tr>
				<tr>
					<td>Biaya Gaji Pegawai:</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><? echo numberFormat($lkcdgajirataperpegawai*$lkcdjumlahkaryawan)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2">
						<input  type="text" <? echo $taginput; ?> id="lblgaji" name="lblgaji" value="<? echo numberFormat($newlkcdgajirataperpegawai*$newlkcdjumlahkaryawan)?>" disabled="disabled" style="width:200px; background:#fff;"/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp; Gaji rata-rata per-pegawai</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdgajirataperpegawai)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Gaji rata-rata per-pegawai " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdgajirataperpegawai" name="lkcdgajirataperpegawai" value="<?echo numberFormat($newlkcdgajirataperpegawai)?>" onkeyup="currency(this.id)" maxlength="15" onblur="totalgajipegawai()"/></td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;&nbsp; Jumlah Karyawan</td>
					<td><?echo 'orang&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdjumlahkaryawan)?></td>
					<td><?echo 'orang'?></td>
					<td colspan="2"><input alt="Jumlah Karyawan " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdjumlahkaryawan" name="lkcdjumlahkaryawan" value="<? echo numberFormat($newlkcdjumlahkaryawan)?>" onkeyup="currency(this.id)" maxlength="15" onblur="totalgajipegawai()"/></td>
				</tr>
				<tr>
					<td>Biaya Sewa/Kontrak</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdbiayasewa)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Biaya Sewa/Kontrak " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdbiayasewa" name="lkcdbiayasewa" value="<?echo numberFormat($newlkcdbiayasewa)?>" onkeyup="currency(this.id)" maxlength="15" /></td>
				</tr>
				<tr>
					<td>Biaya Telepon, Listrik, Air</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdbiayatelepon)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Biaya Sewa/Kontrak " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdbiayatelepon" name="lkcdbiayatelepon" value="<?echo numberFormat($newlkcdbiayatelepon)?>" onkeyup="currency(this.id)" maxlength="15" /></td>
				</tr>
				<tr>
					<td>Biaya Sampah, Keamanan, Transportasi</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdsampah)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Biaya Sampah, Keamanan, Transportasi " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdsampah" name="lkcdsampah" value="<?echo numberFormat($newlkcdsampah)?>" onkeyup="currency(this.id)" maxlength="15" /></td>
				</tr>
				<tr>
					<td>Biaya Operasional Lainnya</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdoperasionallainnya)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Biaya Operasional Lainnya " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdoperasionallainnya" name="lkcdoperasionallainnya" value="<?echo numberFormat($newlkcdoperasionallainnya)?>" onkeyup="currency(this.id)" maxlength="15" /></td>
				</tr>
				<tr>
					<td colspan="5" style="font-weight:bold;">
					<div>&nbsp;<div>
						Biaya Lainnya
					</td>
				</tr>
				<tr>
					<td>Biaya rumah tangga</td>
					<td><?echo $custcurcode.'&nbsp;&nbsp;&nbsp;'?><?echo numberFormat($lkcdbiayarumahtangga)?></td>
					<td><?echo $custcurcode?></td>
					<td colspan="2"><input alt="Biaya rumah tangga " type="text" <? echo $taginput; ?> style=" background:#ff0;width:200px;" id="lkcdbiayarumahtangga" name="lkcdbiayarumahtangga" value="<?echo numberFormat($newlkcdbiayarumahtangga)?>" onkeyup="currency(this.id)" maxlength="15" /></td>
				</tr>
			</table>
			<div>&nbsp;
			
				<?require ("../../requirepage/hiddenfield.php");?>
			</div>
			<div style="text-align:center;">
				<input <? echo $button; ?> id="btnsave" name="btnsave" value="save" class="button" style="width:300px;" onclick="validation()"/>
			</div>
		</form>
	</body>
</html>