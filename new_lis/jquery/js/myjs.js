
function validateEmail(thisid)
{
	var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
	var result;
	if ($("#"+thisid).val() != "")
	{
		if (reg.test($("#"+thisid).val()))
		{
			result="1";
		}
		else
		{
			result="2";
		}
	}
	return result;
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	return true;
}

