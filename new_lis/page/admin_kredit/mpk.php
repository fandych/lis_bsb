<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require ("../../requirepage/parameter.php");
require("../../query/cmp_query.php");

//echo $userpermission;

$total_plafond1=0;
$sql_mkk="select cf.custfacseq,cf.custcreditlong,cf.custcreditplafond,kp.produk_type_description, cn.credit_need_name
		from tbl_customerfacility2 cf, Tbl_KodeProduk kp, Tbl_CreditNeed cn
		where cf.custcredittype=cn.credit_need_code
		and cf.custcreditneed=kp.produk_loan_type
		and cf.custnomid='$custnomid'";
$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_mkk = array(&$_POST['query']);
$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_mkk))
{
	$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
	while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
	{
	$plafond=$row_mkk['custcreditplafond'];
	$total_plafond1+=$plafond;
	}
}
sqlsrv_free_stmt( $sqlConn_mkk );

//echo $total_plafond1;

$bunga=0;
$sql_mkk="
select * from TBL_MASTER_BUNGA 
where BUNGA_MIN < $total_plafond1
and BUNGA_MAX > $total_plafond1";
$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_mkk = array(&$_POST['query']);
$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_mkk))
{
	$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
	while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
	{
	$bunga=$row_mkk['BUNGA_PESEN'];
	}
}
sqlsrv_free_stmt( $sqlConn_mkk );

if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	header("location:restricted.php");
}


// SECURITY
	$tsql = "SELECT COUNT(*) FROM Tbl_SE_User 
			WHERE user_id='$userid'
			AND user_pwd='$userpwd'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false){die( FormatErrors( sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$thecount = $row[0];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	if ($thecount == 0)
	{
		header("location:restricted.php");
	}

	$tsql = "SELECT * FROM Tbl_SE_UserProgram
			WHERE user_id='$userid'
			AND program_code='$userwfid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false){die( FormatErrors( sqlsrv_errors()));}
	$userpinp = "";
	$userpchk = "";
	$userpapr = "";
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$userpinp = substr($row[2],1-1,1);
			$userpchk = substr($row[2],2-1,1);
			$userpapr = substr($row[2],3-1,1);
		}
	}
	sqlsrv_free_stmt( $sqlConn );


// END SECURITY

// PROFILE USER ID (AO / TL / PINCA)
$tsql = "SELECT user_ao_code, user_level_code, user_child FROM Tbl_SE_User
		WHERE user_id='$userid'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
if ( $sqlConn === false){die( FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
$rowCount = sqlsrv_num_rows($sqlConn);
while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	{
			$profileaocode = $row[0];
			$profilelevelcode = $row[1];
			$profileuserchild = $row[2];
	}
}
sqlsrv_free_stmt( $sqlConn );



$cmpfullname="";
$custcredittype1="";
$custcredittype2="";
$custbranchcode="";
$custcreditplafond1="";
$custcreditplafond2="";
$custcreditlong1="";
$custcreditlong2="";
$select_cmp="select custfullname,custcredittype1,custcredittype2,custbranchcode,
			custcreditplafond1,custcreditplafond2,custcreditlong1,custcreditlong2
			from Tbl_CustomerMasterPerson2 where custnomid='$custnomid'";
$cursortype_cmp = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cmp = array(&$_POST['query']);
$sqlConn_cmp = sqlsrv_query($conn, $select_cmp, $params_cmp, $cursortype_cmp);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_cmp))
{
	$rowCount_cmp = sqlsrv_num_rows($sqlConn_cmp);
	while($row_cmp = sqlsrv_fetch_array( $sqlConn_cmp, SQLSRV_FETCH_ASSOC))
		{
			$cmpfullname=$row_cmp['custfullname'];
			$custcredittype1=$row_cmp['custcredittype1'];
			$custcredittype2=$row_cmp['custcredittype2'];
			$custbranchcode=$row_cmp['custbranchcode'];
			$custcreditplafond1=$row_cmp['custcreditplafond1'];
			$custcreditplafond2=$row_cmp['custcreditplafond2'];
			$custcreditlong1=$row_cmp['custcreditlong1'];
			$custcreditlong2=$row_cmp['custcreditlong2'];
			//$custcredittype2=$row_cmp['custcredittype2'];
			//$custcredittype1=$row_cmp['custcredittype1'];
		}
}
sqlsrv_free_stmt( $sqlConn_cmp );




//GET PLAFOND 
$sql_getplafond = "SELECT * FROM tbl_customerfacility2 WHERE custnomid = '$custnomid'";
$cursortype_getplafond = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_getplafond = array(&$_POST['query']);
$sqlConn_getplafond = sqlsrv_query($conn, $sql_getplafond, $params_getplafond, $cursortype_getplafond);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_getplafond))
{
	$rowCount_getplafond = sqlsrv_num_rows($sqlConn_getplafond);
	$x = 1;
	while($row_getplafond = sqlsrv_fetch_array( $sqlConn_getplafond, SQLSRV_FETCH_BOTH))
	{
		$custcreditplafond = "custcreditplafond".$x;
		//$cmp_cn = "cmp_cn".$x;
		$custcreditlong = "custcreditlong".$x;
		$custcredittype = "custcredittype".$x;
		
		//echo $cmp_cp;
		$$custcreditplafond=round($row_getplafond['custcreditplafond']);
		//$$cmp_cn=round($row_getplafond['custcreditneed']);
		$$custcreditlong=round($row_getplafond['custcreditlong']);
		$$custcredittype=round($row_getplafond['custcredittype']);
		
	
		$x++;
	}
}
sqlsrv_free_stmt( $sqlConn_getplafond );

$custproccode = "";
$currCode = "";
$custaplno="";
//GET CURRENCY
   $tsql = "SELECT * FROM Tbl_CustomerMasterPerson2 WHERE custnomid='$custnomid'";//echo $tsql;
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
      {		   
	  		$custproccode = $row['custproccode'];
	  		$custaplno = $row['custaplno'];
			$currCode = $row['custcurcode'];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   
if($currCode == "")
{
	$currCode = "IDR";
}  



$branch_name="";
$sql_branch="select * from Tbl_Branch where branch_code='$custbranchcode' ";
$cursortype_branch = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_branch = array(&$_POST['query']);
$sqlConn_branch = sqlsrv_query($conn, $sql_branch, $params_branch, $cursortype_branch);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_branch))
	{
		$rowCount_branch = sqlsrv_num_rows($sqlConn_branch);
		while($row_branch = sqlsrv_fetch_array( $sqlConn_branch, SQLSRV_FETCH_ASSOC))
			{
			$branch_name=$row_branch['branch_name'];
			}						
	}
sqlsrv_free_stmt( $sqlConn_branch );



$mkk_no_reg="";
$mkkexistingoutstanding1=0;
$mkkexistingoutstanding2=0;
$mkkexistingbungaflag1="";
$mkkexistingbungaflag2="";
$mkkexistingprovisi1="";
$mkkexistingprovisi2="";
$mkknewbungaflag1="";
$mkknewbungaflag2="";
$mkknewprovisi1="";
$mkknewprovisi2="";

/*
$select_mkk="select mkk_no_reg,mkkexistingoutstanding1,mkkexistingoutstanding2,
			mkkexistingbungaflag1,mkkexistingbungaflag2,mkkexistingprovisi1,
			mkkexistingprovisi2,mkknewbungaflag1,mkknewbungaflag2,
			mkknewprovisi1,mkknewprovisi2
			from Tbl_CustomerMKK where mkknomid='$custnomid'";
$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_mkk = array(&$_POST['query']);
$sqlConn_mkk = sqlsrv_query($conn, $select_mkk, $params_mkk, $cursortype_mkk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_mkk))
{
	$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
	while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
		{
			$mkk_no_reg=$row_mkk['mkk_no_reg'];
			$mkkexistingoutstanding1=$row_mkk['mkkexistingoutstanding1'];
			$mkkexistingoutstanding2=$row_mkk['mkkexistingoutstanding2'];
			$mkkexistingbungaflag1=$row_mkk['mkkexistingbungaflag1'];
			$mkkexistingbungaflag2=$row_mkk['mkkexistingbungaflag2'];
			$mkkexistingprovisi1=$row_mkk['mkkexistingprovisi1'];
			$mkkexistingprovisi2=$row_mkk['mkkexistingprovisi2'];
			$mkknewbungaflag1=$row_mkk['mkknewbungaflag1'];
			$mkknewbungaflag2=$row_mkk['mkknewbungaflag2'];
			$mkknewprovisi1=$row_mkk['mkknewprovisi1'];
			$mkknewprovisi2=$row_mkk['mkknewprovisi2'];
		}
}
sqlsrv_free_stmt( $sqlConn_mkk );
*/


$produk_currency_type="";
$produk_type_description="";
$select_kode_produk="select produk_currency_type,produk_type_description from tbl_kodeproduk where produk_loan_type='$custcredittype1'";
$cursortype_kode_produk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_kode_produk = array(&$_POST['query']);
$sqlConn_kode_produk = sqlsrv_query($conn, $select_kode_produk, $params_kode_produk, $cursortype_kode_produk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_kode_produk))
{
	$rowCount_kode_produk = sqlsrv_num_rows($sqlConn_kode_produk);
	while($row_kode_produk = sqlsrv_fetch_array( $sqlConn_kode_produk, SQLSRV_FETCH_ASSOC))
		{
			$produk_currency_type=$row_kode_produk['produk_currency_type'];
			$produk_type_description=$row_kode_produk['produk_type_description'];
		}
}
sqlsrv_free_stmt( $sqlConn_kode_produk );

$pk_no="";
$select_pk="select pknumber from tbl_newpk where custnomid='$custnomid'";//echo $select_pk;
$cursortype2_pk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_pk = array(&$_POST['query']);
$sqlConn_pk = sqlsrv_query($conn, $select_pk, $params_pk, $cursortype2_pk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_pk))
{
	$rowCount_pk = sqlsrv_num_rows($sqlConn_pk);
	if($row_pk = sqlsrv_fetch_array( $sqlConn_pk, SQLSRV_FETCH_ASSOC))
		{
			$pk_no=$row_pk['pknumber'];
		}
}
sqlsrv_free_stmt( $sqlConn_pk );

$produk_currency_type2="";
$produk_type_description2="";
$select_kode_produk="select produk_currency_type,produk_type_description from tbl_kodeproduk where produk_loan_type='$custcredittype2'";
$cursortype2_kode_produk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_kode_produk = array(&$_POST['query']);
$sqlConn_kode_produk = sqlsrv_query($conn, $select_kode_produk, $params_kode_produk, $cursortype2_kode_produk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_kode_produk))
{
	$rowCount_kode_produk = sqlsrv_num_rows($sqlConn_kode_produk);
	while($row_kode_produk = sqlsrv_fetch_array( $sqlConn_kode_produk, SQLSRV_FETCH_ASSOC))
		{
			$produk_currency_type2=$row_kode_produk['produk_currency_type'];
			$produk_type_description2=$row_kode_produk['produk_type_description'];
		}
}
sqlsrv_free_stmt( $sqlConn_kode_produk );




	$mpk_kepada="";
	$mpk_dari="";
	$mpk_tgl="";
	$mpk_tgl_lfk="";
	$mpk_komite_kredit="";
	$mpk_no_lkk="";
	$mpk_nominal_pencairan1=0;
	$mpk_tgl_valuta_pencairan1="";
	$mpk_keterangan1="";
	$mpk_nominal_pencairan2=0;
	$mpk_tgl_valuta_pencairan2="";
	$mpk_keterangan2="";
	$mpk_tgl_notaris="";
	$mpk_nama_notaris="";
	$mpk_provisi_rpk=0;
	$mpk_biaya_admin_kredit=0;
	$mpk_biaya_notaris=0;
	$mpk_total_biaya=0;
	$mpk_atas_nama_bm="";
	$mpk_no_rek_bm="";
	$mpk_tgl_valuta_bm="";
	$mpk_nominal_bm1=0;
	$mpk_ttun_no_bm="";
	$mpk_jangka_waktu1="";
	$mpk_jangka_waktu2="";
	$mpk_nominal_bm2=0;
	$mpk_dana="";
	$mpk_nama_bank="";
	$mpk_atas_nama="";
	$mpk_no_rek="";
	$mpk_tgl_valuta="";
	$mpk_nominal=0;
	$mpk_ppkspk="";
	$mpk_bagian_marketing="";
	$mpk_administrasi_kredit="";
	$mpk_loan_processing="";
	
	$mpk_x_angsuran="0";
	$mpk_asuransi_kredit="0";
	$mpk_asuransi_kebakaran="0";
	
	$mpk_debet_atasnama="";
	$mpk_debet_norek="";

	$mpk_last_branch="";
	$mpk_last_remarks="";

	$mpk_no_suratpermohonan="0";

	$sql_mpk="select * from tbl_master_mpk where custnomid='$custnomid' ";
	$cursortype_mpk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_mpk= array(&$_POST['query']);
	$sqlConn_mpk = sqlsrv_query($conn, $sql_mpk, $params_mpk, $cursortype_mpk);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_mpk))
	{
		$rowCount_mpk = sqlsrv_num_rows($sqlConn_mpk);
		while($row_mpk = sqlsrv_fetch_array( $sqlConn_mpk, SQLSRV_FETCH_ASSOC))
			{
			$mpk_kepada=$row_mpk['mpk_kepada'];				
			$mpk_dari=$row_mpk['mpk_dari'];				
			$mpk_tgl=$row_mpk['mpk_tgl'];				
			$mpk_tgl_lfk=$row_mpk['mpk_tgl_lfk'];				
			$mpk_komite_kredit=$row_mpk['mpk_komite_kredit'];				
			$mpk_no_lkk=$row_mpk['mpk_no_lkk'];				
			$mpk_nominal_pencairan1=$row_mpk['mpk_nominal_pencairan1'];			
			$mpk_tgl_valuta_pencairan1=$row_mpk['mpk_tgl_valuta_pencairan1'];			
			$mpk_keterangan1=$row_mpk['mpk_keterangan1'];				
			$mpk_nominal_pencairan2=$row_mpk['mpk_nominal_pencairan2'];				
			$mpk_tgl_valuta_pencairan2=$row_mpk['mpk_tgl_valuta_pencairan2'];				
			$mpk_keterangan2=$row_mpk['mpk_keterangan2'];				
			$mpk_tgl_notaris=$row_mpk['mpk_tgl_notaris'];				
			$mpk_nama_notaris=$row_mpk['mpk_nama_notaris'];				
			$mpk_provisi_rpk=$row_mpk['mpk_provisi_rpk'];				
			$mpk_biaya_admin_kredit=$row_mpk['mpk_biaya_admin_kredit'];			
			$mpk_biaya_notaris=$row_mpk['mpk_biaya_notaris'];				
			$mpk_total_biaya=$row_mpk['mpk_total_biaya'];				
			$mpk_atas_nama_bm=$row_mpk['mpk_atas_nama_bm'];				
			$mpk_no_rek_bm=$row_mpk['mpk_no_rek_bm'];				
			$mpk_tgl_valuta_bm=$row_mpk['mpk_tgl_valuta_bm'];				
			$mpk_nominal_bm1=$row_mpk['mpk_nominal_bm1'];				
			$mpk_ttun_no_bm=$row_mpk['mpk_ttun_no_bm'];				
			$mpk_jangka_waktu1=$row_mpk['mpk_jangka_waktu1'];				
			$mpk_jangka_waktu2=$row_mpk['mpk_jangka_waktu2'];				
			$mpk_nominal_bm2=$row_mpk['mpk_nominal_bm2'];				
			//$mpk_dana=$row_mpk['mpk_dana'];				
			//$mpk_nama_bank=$row_mpk['mpk_nama_bank'];				
			//$mpk_atas_nama=$row_mpk['mpk_atas_nama'];				
			//$mpk_no_rek=$row_mpk['mpk_no_rek'];				
			//$mpk_tgl_valuta=$row_mpk['mpk_tgl_valuta'];				
			//$mpk_nominal=$row_mpk['mpk_nominal'];				
			$mpk_ppkspk=$row_mpk['mpk_ppkspk'];				
			$mpk_bagian_marketing=$row_mpk['mpk_bagian_marketing'];			
			$mpk_administrasi_kredit=$row_mpk['mpk_administrasi_kredit'];			
			$mpk_loan_processing=$row_mpk['mpk_loan_processing'];
			
			$mpk_x_angsuran=$row_mpk['mpk_x_angsuran'];
			$mpk_asuransi_kebakaran=$row_mpk['mpk_asuransi_kebakaran'];
			$mpk_asuransi_kredit=$row_mpk['mpk_asuransi_kredit'];
			
			$mpk_debet_atasnama=$row_mpk['mpk_debet_atasnama'];
			$mpk_debet_norek=$row_mpk['mpk_debet_norek'];
			
			//$mpk_last_branch=$row_mpk['mpk_last_branch'];
			//$mpk_last_remarks=$row_mpk['mpk_last_remarks'];
						
			$mpk_no_suratpermohonan=$row_mpk['mpk_no_suratpermohonan'];
			}						
	}
	sqlsrv_free_stmt( $sqlConn_mpk );
	
	$in_cif="";
	
	
	$sql_mkk="select * from  tbl_mkkanalisa5c where custnomid='$custnomid'";
	$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_mkk= array(&$_POST['query']);
	$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_mkk))
	{
		$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
		while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
		{

		$in_cif=$row_mkk['analisa5c_cif'];

		}						
	}
	sqlsrv_free_stmt( $sqlConn_mkk );
	
	$bankname="";
	$sql_bn="select * from MS_CONTROL WHERE CONTROL_CODE = 'BANKNAME'";
	$cursortype_bn = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_bn= array(&$_POST['query']);
	$sqlConn_bn = sqlsrv_query($conn, $sql_bn, $params_bn, $cursortype_bn);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_bn))
	{
		$rowCount_bn = sqlsrv_num_rows($sqlConn_bn);
		while($row_bn = sqlsrv_fetch_array( $sqlConn_bn, SQLSRV_FETCH_ASSOC))
		{

			$bankname=$row_bn['CONTROL_VALUE'];

		}						
	}
	sqlsrv_free_stmt( $sqlConn_bn );
	
	
	
	//echo $mpk_atas_nama_bm;
?>


<html>
<head>
</head>
<title>MPK</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function outputMoney(theid)
{	
	//alert(theid);
	varreplace = replace(eval("document.mpk." + theid + ".value"),',','');
	//alert("woi");
	eval("document.mpk." + theid + ".value =  varreplace")
	number = eval("document.mpk." + theid + ".value");
	newoutput = outputDollars(Math.floor(number-0) + '');
	eval("document.mpk." + theid + ".value =  newoutput") 
}

function lala(theid,htheid)
{	
	//alert(theid);
	varreplace = replace(eval("document.mpk." + theid + ".value"),',','');
	//alert("woi");
	eval("document.mpk." + theid + ".value =  varreplace")
	number = eval("document.mpk." + theid + ".value");
	newoutput = outputDollars(Math.floor(number-0) + '');
	eval("document.mpk." + theid + ".value =  newoutput")
	
	
	
	var tmpvalue = eval("document.mpk."+theid+".value");
	//alert(tmpvalue);
	//alert(document.mpk.npkb.value)
	var inChar = ",";
	var outChar = "";
	var tmpinchar = tmpvalue.split(inChar); 
	tmpoutchar= tmpinchar.join(outChar);
	//alert(tmpoutchar);
	document.getElementById(htheid).value=tmpoutchar;



	

	
}

function hitangsuran()
{
	<?
		$sql = "SELECT * FROM TBL_CUSTOMERFACILITY2 WHERE CUSTNOMID = '$custnomid'";//echo $sql;
		$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
		if($conn==false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}
		if(sqlsrv_has_rows($sqlConn))
		{
			$rowCount = sqlsrv_num_rows($sqlConn);
			while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
			{
				$sequence = $row["custfacseq"];
				?> 
					var angsuran<? echo $sequence;?> = document.mpk.h_mkk_angsuran<? echo $sequence;?>.value;
					var xangsuran<? echo $sequence;?> = document.mpk.mpk_x_angsuran<? echo $sequence;?>.value;
					
					//alert (angsuran + " X " + xangsuran );
					
					document.mpk.mpk_jml_angsuran<? echo $sequence;?>.value = angsuran<? echo $sequence;?> * xangsuran<? echo $sequence;?>;
					outputMoney('mpk_jml_angsuran<? echo $sequence;?>');
					
					document.mpk.h_mpk_jml_angsuran<? echo $sequence;?>.value = angsuran<? echo $sequence;?> * xangsuran<? echo $sequence;?>;
				<?
			}
		}
		sqlsrv_free_stmt( $sqlConn );
	?>
	
	
}

function totalcoi()
{
	var angsuran = 0;
	var provisi = 0;
	var biayaadmin = 0;
	<?
		$sql = "SELECT * FROM TBL_CUSTOMERFACILITY2 WHERE CUSTNOMID = '$custnomid'";//echo $sql;
		$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
		if($conn==false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}
		if(sqlsrv_has_rows($sqlConn))
		{
			$rowCount = sqlsrv_num_rows($sqlConn);
			while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
			{
				$sequence = $row["custfacseq"];
				?> 
					//alert(eval(document.mpk.h_mkk_angsuran<? echo $sequence;?>.value));
					angsuran += eval(document.mpk.h_mpk_jml_angsuran<? echo $sequence;?>.value);
					provisi += eval(document.mpk.h_mpk_provisi_rpk<? echo $sequence;?>.value);
					biayaadmin += eval(document.mpk.h_mpk_biaya_admin_kredit<? echo $sequence;?>.value);
				<?
			}
		}
		sqlsrv_free_stmt( $sqlConn );
	?>
	kb2=provisi;
	//alert(kb2);
	kb3=biayaadmin;
	//alert(kb3);
	kb1=eval(document.mpk.h_mpk_biaya_notaris.value);
	//alert(kb1);
	kb4=angsuran;
	//alert(kb4);
	kb5=eval(document.mpk.h_mpk_asuransi_kredit.value);
	//alert(kb5);
	kb6=eval(document.mpk.h_mpk_asuransi_kebakaran.value);
	//alert(kb6);

	total=parseFloat(kb1+kb2+kb3+kb4+kb5+kb6);
	//alert(total);
	document.mpk.mpk_total_biaya.value=total;
	outputMoney('mpk_total_biaya');
	
}

function outputMoney(theid)
{	
	//alert(theid);
	varreplace = replace(eval("document.mpk." + theid + ".value"),',','');
	//alert("woi");
	eval("document.mpk." + theid + ".value =  varreplace")
	number = eval("document.mpk." + theid + ".value");
	newoutput = outputDollars(Math.floor(number-0) + '');
	eval("document.mpk." + theid + ".value =  newoutput") 
}
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	return true;
}

function outputDollars(number)
{
	if (number.length <= 3)
		return (number == '' ? '0' : number);
	else
	{
		var mod = number.length%3;
		var output = (mod == 0 ? '' : (number.substring(0,mod)));
		for (i=0 ; i < Math.floor(number.length/3) ; i++)
		{
			if ((mod ==0) && (i ==0))
			output+= number.substring(mod+3*i,mod+3*i+3);
			else
			output+= ',' + number.substring(mod+3*i,mod+3*i+3);
		}
		return (output);
	}
}

function replace(string,text,by)
{
   var strLength = string.length, txtLength = text.length;
   if ((strLength == 0) || (txtLength == 0)) return string;

   var i = string.indexOf(text);
   if ((!i) && (text != string.substring(0,txtLength))) return string;
   if (i == -1) return string;

   var newstr = string.substring(0,i) + by;

   if (i+txtLength < strLength)
	  newstr += replace(string.substring(i+txtLength,strLength),text,by);

   return newstr;
}



function rekening_sama()
{
a=document.mpk.mpk_rekening_.value;
document.mpk.mpk_no_rek_bm.value=a;
}
function goSave()
{
	
	var FormName="mpk";
	var elem = document.getElementById(FormName).elements;
	for(var i = 0; i < elem.length; i++)
	{
		if(elem[i].name == "mpk_dana")
		{
		}
		else
		{
			elem[i].value = elem[i].value.toUpperCase();
		}
	}
	
	var StatusAllowSubmit=true;
	var elem = document.getElementById(FormName).elements;
	for(var i = 0; i < elem.length; i++)
	{
		if(elem[i].style.backgroundColor=="#ff0")
		{
			if(elem[i].value == "")
			{
				alert(elem[i].nai + " field Must be filled");	
				StatusAllowSubmit=false				
				break;
			}
		}
		 
		if(elem[i].type == "textarea" || elem[i].type == "text")
		{
			//alert(elem[i].value);
			varinvalidcharone = elem[i].value.indexOf('\'');	
			varinvalidchartwo = elem[i].value.indexOf('\"');
			if (varinvalidchartwo != -1 || varinvalidcharone != -1)
			{
				alert(elem[i].nai + " TIDAK BOLEH MENGGUNAKAN TANDA \' atau \"");
				elem[i].focus();
				StatusAllowSubmit=false	
				return false;
			}
			
		}
	
	}
	if(StatusAllowSubmit == true)
	{			
		document.mpk.target = "utama";
		document.mpk.action = "do_mpk.php";
		submitform = window.confirm("<?=$confmsg?>?")
		if (submitform == true)
		{
			document.mpk.submit();
			return true;
		}
		else
		{
			return false;
		} 
	}
}
</script>
<body>
<script language="JavaScript"><!--
name = 'utama';
//--></script> 
<form name="mpk" id="mpk" action="do_mpk.php" method="post">
	<table align="center" class="tblform" style="border:1px solid black; width:1024px;">
		<tr> 
			<td><input nai="" type="hidden" name=userid  value='<? echo $userid; ?>'>
			  <input nai="" type="hidden" name=userpwd value='<? echo $userpwd; ?>'>
				<input nai="" type="hidden" name=userbranch value='<? echo $userbranch; ?>'>
				<input nai="" type="hidden" name=userregion value='<? echo $userregion; ?>'>
				<input nai="" type="hidden" name=buttonaction value='<? echo $buttonaction; ?>'>
				<input nai="" type="hidden" name=userwfid  value='<? echo $userwfid; ?>'>
				<input nai="" type="hidden" name=custnomid  value='<? echo $custnomid; ?>'>
				<table width="100%" align="center" style="margin-top: 30px;">
					<tr>
						<td align="center" style="font-size: 18px">MEMO PENCAIRAN KREDIT</td>
					</tr>
				</table>
				</br>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr> 
						<td valign="top" width="50%">
							<table width="100%">
								<tr>
									<td style="padding-left: 10px;">Kepada</td>
									<td>
										<input value="<?php echo $mpk_kepada ?>" maxlength="50" nai="KEPADA" style="width: 300px;background-color: #FF0;" type="text" name="mpk_kepada" id="mpk_kepada"/>
									</td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Dari</td>
									<td>
										<input value="<?php echo $mpk_dari?>" maxlength="50" nai="DARI" style="width: 300px;background-color: #FF0;" type="text" name="mpk_dari" id="mpk_dari"/>
									</td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Nomor</td>
									<td><?php echo $custnomid;?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Tanggal</td>
									<td>
										<input value="<?php echo $mpk_tgl?>" nai="TANGGAL" style="width: 70px;background-color: #FF0;" readonly type="text" name="mpk_tgl" id="mpk_tgl"/><a href="javascript:NewCssCal('mpk_tgl','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a> 
									</td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">CIF</td>
									<td>
										<input type="text" name="in_cif" id="in_cif" maxlength="7" nai="CIF" value="<? echo $custaplno;?>"  style="background-color:#FF0;width: 300px;"/>
										<a href="javascript:alert('function under development')">search cif</a></td>
								</tr>
							</table>
						</td>
						<td width="50%">
							<table width="100%">
								<tr>
									<td style="padding-left: 10px;">Debitur</td>
									<td><? if($cmpsex!="0") {echo $cmpfullname;}else{echo $cmpnamausaha;} ?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Rekening</td>
									<td><input nai="REKENING" maxlength="20" onKeyPress="return isNumberKey(event)" style="background-color: #FF0;width: 300px;" onBlur="rekening_sama()" nama="mpk_rekening_" id="mpk_rekening_" value="<?php echo $mpk_no_rek_bm?>"/></td>
								</tr>
                                <?
									if($custproccode == "KUK")
									{
										
										?>
                                        <tr>
									<td style="padding-left: 10px;">&nbsp;</td>
									<td><input value="01/01/1900" nai="TANGGAL. / NO. LFK" readonly type="hidden" name="mpk_tgl_lfk" id="mpk_tgl_lfk"/></td>
								</tr>
                                        <?
									}
									else
									{
								?>
								<tr>
									<td style="padding-left: 10px;">Tgl./No.LFK</td>
									<td><input value="<?php echo $mpk_tgl_lfk?>" nai="TANGGAL. / NO. LFK" style="width: 70px;background-color: #FF0;" readonly type="text" name="mpk_tgl_lfk" id="mpk_tgl_lfk"/><a href="javascript:NewCssCal('mpk_tgl_lfk','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a></td>
								</tr>
                                <?
									}
								?>
								<tr>
									<td style="padding-left: 10px;">Komite Kredit</td>
									<td>
<?php
$komite="";
$sql = "SELECT (SELECT level_name FROM Tbl_SE_Level WHERE level_code = user_level_code) as KOMITE FROM Tbl_FCAP LEFT JOIN Tbl_SE_User ON txn_user_id = user_id WHERE txn_id = '$custnomid'";//echo $sql;
$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
if($conn==false)
{
	die(FormatErrors(sqlsrv_errors()));
}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	if($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
	{
		$komite = $row['KOMITE'];
	}
}
sqlsrv_free_stmt( $sqlConn );
?>
<input value="<?=$komite;?>" maxlength="50" nai="KOMITE KREDIT" style="width: 300px;background-color: #FF0;" type="text" name="mpk_komite_kredit" id="mpk_komite_kredit"/></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;"><!--No. LKK!--></td>
									<td><input onKeyPress="return isNumberKey(event)" value="000" maxlength="50" NAI="No. LKK" type="hidden" name="mpk_no_lkk" id="mpk_no_lkk"/></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
<td colspan="2">Berdasarkan Surat Permohonan Debitur No. <input type="text" id="mpk_no_suratpermohonan" name="mpk_no_suratpermohonan" value="<?=$mpk_no_suratpermohonan;?>" nai="NO SURAT PERMOHONAN" maxlength="20" onKeyPress="return isNumberKey(event)" style="background-color: #FF0;width: 300px;" ></td>
					</tr>
				</table>
				<!--  -->
				<table width="100%" align="center" style="margin-top: 30px;">
					<tr>
						<td  style="font-size: 18px">FASILITAS YANG DICAIRKAN</td>
					</tr>
				</table>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td width="9%" align="center">Fasilitas</td>
						<td width="4%" align="center">Mata Uang</td>
						<td width="13%" align="center">Plafond</td>
						<td width="13%" align="center">Outstanding</td>
						<td width="12%" align="center">Nominal Pencairan</td>
						<td width="10%" align="center">Tgl. Valuta Pencairan</td>
						<td width="7%" align="center">Bunga % pa</td>
						<td width="8%" align="center">Provisi / Komisi </td>
						<td width="5%" align="center">Jangka Waktu</td>
						<td width="20%" align="center">Keterangan</td>
					</tr>
					<?
					$mkknewangsuran = 0;
					
					$total_plafond1=0;
					$plafond=0;
					$ttl_admin=0;
					$perkalian_plafond_provisi=0;
					$ttl_perkalian_plafond_provisi=0;
					$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
							cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.sukubungayangdiberikan, cf.custcreditlong,
							cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran
							from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
							where cf.custnomid='$custnomid'
							and cf.custcredittype=kp.produk_loan_type"; //echo $sql_mkk;
					$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
					$params_mkk = array(&$_POST['query']);
					$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
					if($conn==false){die(FormatErrors(sqlsrv_errors()));}
					if(sqlsrv_has_rows($sqlConn_mkk))
					{
						$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
						while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
						{
						$sequence=$row_mkk['custfacseq'];
						$nama_produk=$row_mkk['produk_type_description'];
						$produk_currency_type=$row_mkk['produk_currency_type'];
						$plafond=$row_mkk['custcreditplafond'];
						$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
						$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
						$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
						$jangkawaktu=$row_mkk['custcreditlong'];
						$mkknewprovisi=$row_mkk['mkknewprovisi'];
						$mpk_keterangan=$row_mkk['mpk_keterangan'];
						$mkknewadm=$row_mkk['mkknewadm'];
						
						$mkknewangsuran=$row_mkk['mkknewangsuran'];//echo $mkknewangsuran;
						
						$perkalian_plafond_provisi=$plafond*$mkknewprovisi/100;
						$total_plafond1+=$plafond;
						$ttl_perkalian_plafond_provisi+=$perkalian_plafond_provisi;
						$ttl_admin+=$mkknewadm;
					?>
					<tr>
						<td><?php echo $nama_produk ?></td>
						<td align="center"><?php echo $produk_currency_type ?></td>
						<td align="right"><?php  echo $currCode.' '.number_format($plafond); ?></td>
						<td align="right">&nbsp;<?php echo $currCode.' '.number_format($plafond);?></td>
						<td align="right"><?php echo $currCode.' '.number_format($plafond) ?></td>
						<td><input nai="<? echo 'TANGGAL VALUTA'.$sequence; ?>" value="<? echo $mpk_tgl_valuta_pencairan; ?>" style="background-color: #FF0;width: 70px;" readonly  name="<? echo 'mpk_tgl_valuta_pencairan'.$sequence; ?>" id="<? echo 'mpk_tgl_valuta_pencairan'.$sequence; ?>"/>
						<a href="javascript:NewCssCal('<? echo 'mpk_tgl_valuta_pencairan'.$sequence; ?>','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a></td>
						<td align="center"><? echo $sukubungayangdiberikan." %" ?></td>
						<td align="center"><? echo $mkknewprovisi;?> %</td>
						<td align="center"><?php echo $jangkawaktu.' Bln'; ?></td>
						<td><input value="<?php echo $mpk_keterangan?>" nai="<? echo 'KETERANGAN'.$sequence; ?>" maxlength="50" style="width: 200px;" name="<? echo 'mpk_keterangan'.$sequence; ?>" id="<? echo 'mpk_keterangan'.$sequence; ?>" /></td>
					</tr>
					<?
						}
					}
					sqlsrv_free_stmt( $sqlConn_mkk );
					?>
					<tr>
						<td>Total</td>
						<td>&nbsp;</td>
						<td align="right">&nbsp;<?php echo $currCode.' '.number_format($total_plafond1); ?></td>
						<td align="right">&nbsp;<?php echo $currCode.' '.number_format($total_plafond1);?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10">Konfirmasi Teasury:</td>
					</tr>
					<tr>
						<td colspan="10">
							<table width="100%">
								<tr>
									<td width="20%"><b/>KETERANGAN</td>
									<td width="1%">:</td>
									<td width="79%">Perjanjian Kredit telah di bawah tanda tangan pada tanggal <input value="<?php echo  $mpk_tgl_notaris?>" nai="TANGGAL NOTARIS" style="width: 70px;background-color: #FF0;" readonly name="mpk_tgl_notaris" id="mpk_tgl_notaris"/><a href="javascript:NewCssCal('mpk_tgl_notaris','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a> 
									oleh Notaris <input value="<?php echo $mpk_nama_notaris?>" maxlength="20" style="background-color: #FF0;" name="mpk_nama_notaris" id="mpk_nama_notaris"/> No, PK : <?php echo $pk_no;?></td>
								</tr>
								<tr>
									<td><b/>Pembebanan Biaya</td>
									<td colspan="2">:</td>
								</tr>
								<?
									$mkknewangsuran = 0;
									
									$total_plafond1=0;
									$plafond=0;
									$ttl_admin=0;
									$perkalian_plafond_provisi=0;
									$ttl_perkalian_plafond_provisi=0;
									$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
											cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.sukubungayangdiberikan, cf.custcreditlong,
											cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran
											from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
											where cf.custnomid='$custnomid'
											and cf.custcredittype=kp.produk_loan_type"; //echo $sql_mkk;
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];
											$nama_produk=$row_mkk['produk_type_description'];
											$produk_currency_type=$row_mkk['produk_currency_type'];
											$plafond=$row_mkk['custcreditplafond'];
											$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
											$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
											$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
											$jangkawaktu=$row_mkk['custcreditlong'];
											$mkknewprovisi=$row_mkk['mkknewprovisi'];
											$mpk_keterangan=$row_mkk['mpk_keterangan'];
											$mkknewadm=$row_mkk['mkknewadm'];
											
											$provisirpk = ($plafond * $mkknewprovisi)/100;
											
								?>
								<tr>
									<td>Provisi RPK <? echo $nama_produk;?></td>
									<td>:</td>
									<td><? echo $currCode.' '.number_format($provisirpk)?>
										<input value="<?php echo $provisirpk?>" type="hidden" name="h_mpk_provisi_rpk<? echo $sequence?>" id="h_mpk_provisi_rpk<? echo $sequence?>"/>
									</td>
								</tr>
								<tr>
									<td>Biaya Admin Kredit</td>
									<td>:</td>
									<td>
										<? echo $currCode.' '.number_format($mkknewadm)?>
										<input value="<?php echo $mkknewadm?>" type="hidden" name="h_mpk_biaya_admin_kredit<? echo $sequence?>" id="h_mpk_biaya_admin_kredit<? echo $sequence?>"/>
									</td>
									
								</tr>
								<?
										}
									}
								?>
								<tr>
									<td>Biaya Notaris</td>
									<td>:</td>
									<td>
<input onKeyPress="return isNumberKey(event)" onBlur="lala('mpk_biaya_notaris','h_mpk_biaya_notaris');totalcoi();"  value="<?php echo number_format($mpk_biaya_notaris)?>" maxlength="12" nai="BIAYA NOTARIS" style="text-align:right;width: 200px;background-color: #FF0;" name="mpk_biaya_notaris" id="mpk_biaya_notaris"/>
<input value="<?php echo $mpk_biaya_notaris?>" type="hidden" name="h_mpk_biaya_notaris" id="h_mpk_biaya_notaris"/>
									</td>
								</tr>
								<?
									$mkknewangsuran = 0;
									$mpk_x_angsuran = 1;
									$total_plafond1=0;
									$plafond=0;
									$ttl_admin=0;
									$perkalian_plafond_provisi=0;
									$ttl_perkalian_plafond_provisi=0;
									$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
											cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.sukubungayangdiberikan, cf.custcreditlong,
											cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran, cf.mpk_blokir
											from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
											where cf.custnomid='$custnomid'
											and cf.custcredittype=kp.produk_loan_type"; //echo $sql_mkk;
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];
											$nama_produk=$row_mkk['produk_type_description'];
											$produk_currency_type=$row_mkk['produk_currency_type'];
											$plafond=$row_mkk['custcreditplafond'];
											$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
											$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
											$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
											$jangkawaktu=$row_mkk['custcreditlong'];
											$mkknewprovisi=$row_mkk['mkknewprovisi'];
											$mpk_keterangan=$row_mkk['mpk_keterangan'];
											$mkknewadm=$row_mkk['mkknewadm'];
											$mkknewangsuran=$row_mkk['mkknewangsuran'];
											$mpk_x_angsuran = $row_mkk['mpk_blokir'];
											
											if($mpk_x_angsuran == "")
											{
												$mpk_x_angsuran = 0;
											}
											
								?>
								<tr>
								  <td>Blokir :<input  onKeyPress="return isNumberKey(event)" onBlur="hitangsuran();totalcoi();" type="text" size="1" id="mpk_x_angsuran<? echo $sequence;?>" name="mpk_x_angsuran<? echo $sequence;?>"  value="<?= $mpk_x_angsuran;?>" maxlength="3"/>
                                  X&nbsp;Angsuran</td>
								  <td></td>
								  <td>
                                  <input value="<?= $mkknewangsuran;?>" type="hidden" name="h_mkk_angsuran<? echo $sequence;?>" id="h_mkk_angsuran<? echo $sequence;?>"/>
                                  <input type="text" style="text-align:right;width: 200px;background-color: #FF0;" id="mpk_jml_angsuran<? echo $sequence;?>" name="mpk_jml_angsuran<? echo $sequence;?>" readonly value="<? echo number_format($mpk_x_angsuran * $mkknewangsuran);?>"/>
                                  <input value="<? echo $mpk_x_angsuran * $mkknewangsuran;?>" type="hidden" name="h_mpk_jml_angsuran<? echo $sequence;?>" id="h_mpk_jml_angsuran<? echo $sequence;?>"/>
                                  </td>
								</tr>
							  <?
										}
									}
							  ?>
								<tr>
								  <td>Asuransi Jiwa Kredit</td>
								  <td>:</td>
								  <td><input onKeyPress="return isNumberKey(event)" onBlur="lala('mpk_asuransi_kredit','h_mpk_asuransi_kredit');totalcoi();" type="text" id="mpk_asuransi_kredit" name="mpk_asuransi_kredit" maxlength="12" nai="ASURANSI KREDIT" style="text-align:right;width: 200px;background-color: #FF0;" value="<?=number_format($mpk_asuransi_kredit);?>"/>&nbsp;
                                  <input value="<?=$mpk_asuransi_kredit;?>" type="hidden" name="h_mpk_asuransi_kredit" id="h_mpk_asuransi_kredit"/>
                                  </td>
							  </tr>
								<tr>
								  <td>Asuransi Jiwa Kebakaran</td>
								  <td>:</td>
								  <td><input onKeyPress="return isNumberKey(event)" onBlur="lala('mpk_asuransi_kebakaran','h_mpk_asuransi_kebakaran');totalcoi();" type="text" id="mpk_asuransi_kebakaran" name="mpk_asuransi_kebakaran" maxlength="12" nai="ASURANSI KEBAKARAN" style="text-align:right;width: 200px;background-color: #FF0;" value="<?=number_format($mpk_asuransi_kebakaran);?>"/>&nbsp;
                                  <input value="<?=$mpk_asuransi_kebakaran;?>" type="hidden" name="h_mpk_asuransi_kebakaran" id="h_mpk_asuransi_kebakaran"/>
                                  </td>
							  </tr>
								<tr>
									<td>Total Biaya</td>
									<td>:</td>
									<td>
										<input readonly onKeyPress="return isNumberKey(event)"   value="<?php echo number_format($mpk_total_biaya)?>" maxlength="20" nai="TOTAL BIAYA" style="width: 200px; text-align:right;background-color: #00FFFF;" name="mpk_total_biaya" id="mpk_total_biaya"/>
										
									</td>
								</tr>
								<tr>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
							  </tr>
								<tr>
								  <td colspan="3">Seluruh biaya mohon di debet dari rekening Debitur</td>
							  </tr>
								<tr>
								  <td>Atas Nama</td>
								  <td>:</td>
								  <td><input type="text" id="mpk_debet_atasnama" name="mpk_debet_atasnama" nai="DEBET ATAS NAMA" style="text-align:left;width: 200px;background-color: #FF0;" value="<?=$mpk_debet_atasnama;?>" />&nbsp;</td>
						      </tr>
								<tr>
								  <td>No. Rekening</td>
								  <td>:</td>
								  <td><input type="text" id="mpk_debet_norek" name="mpk_debet_norek" nai="DEBET NO REKENING" style="text-align:left;width: 200px;background-color: #FF0;" value="<?=$mpk_debet_norek;?>" />&nbsp;</td>
						      </tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3">Pencairan tersebut agar di- kreditkan kepada Rekening  <?php echo $bankname?> :</td>
								</tr>
								<tr>
									<td>Cabang</td>
									<td>:</td>
									<td><?php echo $branch_name;?></td>
								</tr>
								<tr>
									<td>Atas Nama</td>
									<td>:</td>
									<td><input value="<?php echo $mpk_atas_nama_bm; ?>" maxlength="20" nai="ATAS NAMA BANK SUMSELBABEL" style="width: 200px;background-color: #FF0;" name="mpk_atas_nama_bm" id="mpk_atas_nama_bm"/></td>
								</tr>
								<tr>
									<td>No. Rekening</td>
									<td>:</td>
									<td><input readonly onKeyPress="return isNumberKey(event)" value="<?php echo $mpk_no_rek_bm?>" maxlength="20" nai="No. REKENING BANK SUMSELBABEL" style="text-align:right;width: 200px;background-color: #FF0;" name="mpk_no_rek_bm" id="mpk_no_rek_bm"/></td>
								</tr>
								<tr>
									<td>Tanggal Valuta</td>
									<td>:</td>
									<td><input value="<?php echo $mpk_tgl_valuta_bm?>" nai="TANGGAL VALUTA BANK SUMSELBABEL" style="text-align:right;width: 70px;background-color: #FF0;" readonly type="text" name="mpk_tgl_valuta_bm" id="mpk_tgl_valuta_bm"/><a href="javascript:NewCssCal('mpk_tgl_valuta_bm','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a></td>
								</tr>
								<tr>
									<td>Nominal</td>
									<td>:</td>
									<td><input onKeyPress="return isNumberKey(event)" onBlur="outputMoney('mpk_nominal_bm1')" value="<?php echo number_format($mpk_nominal_bm1);?>" maxlength="12" nai="NOMINAL BANK SUMSELBABEL" style="width: 200px;text-align:right;background-color: #FF0;" name="mpk_nominal_bm1" id="mpk_nominal_bm1"/></td>
								</tr>
								<tr>
									<td>TTUN</td>
									<td>&nbsp;</td>
									<td> 
									<input onKeyPress="return isNumberKey(event)" value="<?php echo $mpk_ttun_no_bm?>" maxlength="20" nai="TTUN / PROMES NO. BANK SUMSELBABEL" style="text-align:right;width: 200px;background-color: #FF0;" name="mpk_ttun_no_bm" id="mpk_ttun_no_bm"/>
									<input value="<?php echo $mpk_jangka_waktu1?>" nai="TANGGAL TTUN JANGKA WAKTU1 BANK SUMSELBABEL" style="width: 70px;background-color: #FF0;" readonly name="mpk_jangka_waktu1" id="mpk_jangka_waktu1"/><a href="javascript:NewCssCal('mpk_jangka_waktu1','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a>
									s/d
									<input value="<?php echo $mpk_jangka_waktu2?>" nai="TANGGAL TTUN JANGKA WAKTU2 BANK SUMSELBABEL" style="width: 70px;background-color: #FF0;" readonly name="mpk_jangka_waktu2" id="mpk_jangka_waktu2"/><a href="javascript:NewCssCal('mpk_jangka_waktu2','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a>
									, Nominal Rp <input onKeyPress="return isNumberKey(event)" onBlur="outputMoney('mpk_nominal_bm2')"  value="<?php echo  number_format($mpk_nominal_bm2)?>" maxlength="12" nai="NOMINAL BANK SUMSELBABEL" style="text-align:right;width: 200px;background-color: #FF0;" name="mpk_nominal_bm2" id="mpk_nominal_bm2"/>
 									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
								<td colspan="3">
								<div id="DANA" style="border:1px solid black;">
								<table id="DANATABLE" name="DANATABLE">
									<tr>
										<td colspan="7"><strong>Untuk Selanjutnya Dana di - </strong></td>
									</tr>
									<tr>
										<td>Tipe</td>
										<td>Nama Bank</td>
										<td>Cabang</td>
										<td>Atas Nama</td>
										<td>Nomor Rekening</td>
										<td>Tanggal Valuta</td>
										<td>Nominal</td>
										<td>Remarks</td>
									</tr>
									<tr>
										<td>
											<select id="mpk_dana" name="mpk_dana">
										<?
											$arrValue = array("-","Pindah Buku","Transfer","RTGS");
											$arrShow = array("-PILIH-","Pindah Buku","Transfer","RTGS");

											$selected = "";

											for($i=0;$i<count($arrValue);$i++)
											{
												if($mpk_dana == $arrValue[$i])
												{
													$selected = "selected";
												}
												else
												{
													$selected = "";
												}
												echo "<option $selected value=\"$arrValue[$i]\">$arrShow[$i]</option>";
											}

										?>

											</select>
										</td>
										<td><input value="<?php echo $mpk_nama_bank?>" maxlength="50" nai="NAMA BANK" style="width: 120px;" name="mpk_nama_bank" id="mpk_nama_bank"/></td>
										<td><input type="text" id="mpk_last_branch" name="mpk_last_branch" style="width: 100px;" value="<?=$mpk_last_branch;?>" maxlength="50"/></td>
										<td><input value="<?php echo $mpk_atas_nama?>" maxlength="50" nai="ATAS NAMA" style="width: 100px;" name="mpk_atas_nama" id="mpk_atas_nama"/></td>
										<td><input onKeyPress="return isNumberKey(event)" value="<?php echo $mpk_no_rek?>" maxlength="20" nai="NOMOR REKENING" style="width: 120px" name="mpk_no_rek" id="mpk_no_rek"/></td>
										<td><input value="<?php echo $mpk_tgl_valuta?>" nai="TANGGAL VALUTA" style="width: 70px;" readonly name="mpk_tgl_valuta" id="mpk_tgl_valuta"/><a href="javascript:NewCssCal('mpk_tgl_valuta','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a></td>
										<td><input onkeypress="return isNumberKey(event)" onKeyUp="outputMoney('mpk_nominal')" value="<?php echo number_format($mpk_nominal)?>" maxlength="20" nai="NOMINAL" style="width: " name="mpk_nominal" id="mpk_nominal"/></td>
										<td><input type="text" id="mpk_last_remarks" name="mpk_last_remarks" style="width: 100px;" value="<?=$mpk_last_remarks;?>" maxlength="50"/></td>
										<td><input type="button" value="SAVE" onClick="SAVEDANA()" style="background-color:blue;color:white;"/></td>
									</tr>
									<?
										
										$mpk_seq="";
										$mpk_dana="";
										$mpk_nama_bank="";
										$mpk_last_branch="";
										$mpk_atas_nama="";
										$mpk_no_rek="";
										$mpk_tgl_valuta="";
										$mpk_nominal=0;
										$mpk_last_remarks="";
										$mpk_flag="";
										
										$sql_mkk="select * from Tbl_MPKDANA where custnomid = '$custnomid' order by mpk_seq"; //echo $sql_mkk;
										$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
										$params_mkk = array(&$_POST['query']);
										$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
										if($conn==false){die(FormatErrors(sqlsrv_errors()));}
										if(sqlsrv_has_rows($sqlConn_mkk))
										{
											$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
											while($rows = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
											{
												$mpk_seq=$rows["mpk_seq"];
												$mpk_dana=$rows["mpk_dana"];
												$mpk_nama_bank=$rows["mpk_nama_bank"];
												$mpk_last_branch=$rows["mpk_last_branch"];
												$mpk_atas_nama=$rows["mpk_atas_nama"];
												$mpk_no_rek=$rows["mpk_no_rek"];
												$mpk_tgl_valuta=$rows["mpk_tgl_valuta"];
												$mpk_nominal=$rows["mpk_nominal"];
												$mpk_last_remarks=$rows["mpk_last_remarks"];
												$mpk_flag=$rows["mpk_flag"];
									?>
									<tr>
										<td>
											<select id="mpk_dana<? echo $mpk_seq;?>" name="mpk_dana<? echo $mpk_seq;?>">
											<?
												$arrValue = array("-","Pindah Buku","Transfer","RTGS");
												$arrShow = array("-PILIH-","Pindah Buku","Transfer","RTGS");

												$selected = "";

												for($i=0;$i<count($arrValue);$i++)
												{
													if($mpk_dana == $arrValue[$i])
													{
														$selected = "selected";
													}
													else
													{
														$selected = "";
													}
													
													echo "<option $selected value=\"$arrValue[$i]\">$arrShow[$i]</option>";
												}

											?>

											</select>
										</td>
										<td><input value="<?php echo $mpk_nama_bank?>" maxlength="50" nai="NAMA BANK" style="width: 120px;" name="mpk_nama_bank<? echo $mpk_seq;?>" id="mpk_nama_bank<? echo $mpk_seq;?>"/></td>
										<td><input type="text" id="mpk_last_branch<? echo $mpk_seq;?>" name="mpk_last_branch<? echo $mpk_seq;?>" style="width: 100px;" value="<?=$mpk_last_branch;?>" maxlength="50"/></td>
										<td><input value="<?php echo $mpk_atas_nama?>" maxlength="50" nai="ATAS NAMA" style="width: 100px;" name="mpk_atas_nama<? echo $mpk_seq;?>" id="mpk_atas_nama<? echo $mpk_seq;?>"/></td>
										<td><input onKeyPress="return isNumberKey(event)" value="<?php echo $mpk_no_rek?>" maxlength="20" nai="NOMOR REKENING" style="width: 120px" name="mpk_no_rek<? echo $mpk_seq;?>" id="mpk_no_rek<? echo $mpk_seq;?>"/></td>
										<td><input value="<?php echo $mpk_tgl_valuta?>" nai="TANGGAL VALUTA" style="width: 70px;" readonly name="mpk_tgl_valuta<? echo $mpk_seq;?>" id="mpk_tgl_valuta<? echo $mpk_seq;?>"/><a href="javascript:NewCssCal('mpk_tgl_valuta<? echo $mpk_seq;?>','ddmmyyyy')"><img src="../../images/calendar.gif" style="text-decoration:none; border:0;" ></img></a></td>
										<td><input onkeypress="return isNumberKey(event)" onKeyUp="outputMoney('mpk_nominal<? echo $mpk_seq;?>')" value="<?php echo number_format($mpk_nominal)?>" maxlength="20" nai="NOMINAL" style="width: " name="mpk_nominal<? echo $mpk_seq;?>" id="mpk_nominal<? echo $mpk_seq;?>"/></td>
										<td><input type="text" id="mpk_last_remarks<? echo $mpk_seq;?>" name="mpk_last_remarks<? echo $mpk_seq;?>" style="width: 100px;" value="<?=$mpk_last_remarks;?>" maxlength="50"/></td>
										<td><input type="button" value="EDIT" onClick="EDITDANA(<? echo $mpk_seq;?>)" style="background-color:red;color:white;font-size:10px;"/><input type="button" value="DELETE" onClick="DELETEDANA(<? echo $mpk_seq;?>)" style="background-color:red;color:white;font-size:10px;"/></td>
									</tr>
									<?
											}
										}	
									?>
								</table>
								</div>
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>								
<script type="text/javascript">
	function SAVEDANA(){
		var nan="SAVEDANA";

		var custnomid = $("#custnomid").val();
		var mpk_seq = $("#mpk_seq").val();
		var mpk_dana = $("#mpk_dana").val();
		var mpk_nama_bank = $("#mpk_nama_bank").val();
		var mpk_last_branch = $("#mpk_last_branch").val();
		var mpk_atas_nama = $("#mpk_atas_nama").val();
		var mpk_no_rek = $("#mpk_no_rek").val();
		var mpk_tgl_valuta = $("#mpk_tgl_valuta").val();
		var mpk_nominal = $("#mpk_nominal").val();
		var mpk_last_remarks = $("#mpk_last_remarks").val();
		var mpk_flag = "";
		
		
		$.ajax({
			type: "POST",
			url: "ajaxDANA.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&mpk_dana="+mpk_dana+"&mpk_nama_bank="+mpk_nama_bank+"&mpk_last_branch="+mpk_last_branch+"&mpk_atas_nama="+mpk_atas_nama+"&mpk_no_rek="+mpk_no_rek+"&mpk_tgl_valuta="+mpk_tgl_valuta+"&mpk_nominal="+mpk_nominal+"&mpk_last_remarks="+mpk_last_remarks+"&mpk_flag="+mpk_flag+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				$("#DANA").html(response);
				//alert('Berhasil disimpan');
			}
		});
	}
	
	function EDITDANA(id){
		var nan="EDITDANA";
		
		var custnomid = $("#custnomid").val();
		var mpk_seq = id;
		var mpk_dana = $("#mpk_dana"+id).val();
		var mpk_nama_bank = $("#mpk_nama_bank"+id).val();
		var mpk_last_branch = $("#mpk_last_branch"+id).val();
		var mpk_atas_nama = $("#mpk_atas_nama"+id).val();
		var mpk_no_rek = $("#mpk_no_rek"+id).val();
		var mpk_tgl_valuta = $("#mpk_tgl_valuta"+id).val();
		var mpk_nominal = $("#mpk_nominal"+id).val();
		var mpk_last_remarks = $("#mpk_last_remarks"+id).val();
		var mpk_flag = "";
		
		$.ajax({
			type: "POST",
			url: "ajaxDANA.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&mpk_seq="+mpk_seq+"&mpk_dana="+mpk_dana+"&mpk_nama_bank="+mpk_nama_bank+"&mpk_last_branch="+mpk_last_branch+"&mpk_atas_nama="+mpk_atas_nama+"&mpk_no_rek="+mpk_no_rek+"&mpk_tgl_valuta="+mpk_tgl_valuta+"&mpk_nominal="+mpk_nominal+"&mpk_last_remarks="+mpk_last_remarks+"&mpk_flag="+mpk_flag+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				$("#DANA").html(response);
				//alert('Berhasil disimpan');
			}
		});
	}
	
	function DELETEDANA(id){
		var nan="DELETEDANA";
		
		var custnomid = $("#custnomid").val();
		var mpk_seq = id;
		var mpk_dana = $("#mpk_dana"+id).val();
		var mpk_nama_bank = $("#mpk_nama_bank"+id).val();
		var mpk_last_branch = $("#mpk_last_branch"+id).val();
		var mpk_atas_nama = $("#mpk_atas_nama"+id).val();
		var mpk_no_rek = $("#mpk_no_rek"+id).val();
		var mpk_tgl_valuta = $("#mpk_tgl_valuta"+id).val();
		var mpk_nominal = $("#mpk_nominal"+id).val();
		var mpk_last_remarks = $("#mpk_last_remarks"+id).val();
		var mpk_flag = "";
		
		$.ajax({
			type: "POST",
			url: "ajaxDANA.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&mpk_seq="+mpk_seq+"&mpk_dana="+mpk_dana+"&mpk_nama_bank="+mpk_nama_bank+"&mpk_last_branch="+mpk_last_branch+"&mpk_atas_nama="+mpk_atas_nama+"&mpk_no_rek="+mpk_no_rek+"&mpk_tgl_valuta="+mpk_tgl_valuta+"&mpk_nominal="+mpk_nominal+"&mpk_last_remarks="+mpk_last_remarks+"&mpk_flag="+mpk_flag+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				$("#DANA").html(response);
				//alert('Berhasil disimpan');
			}
		});
	}
	
</script>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td> 
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td></b>Persyaratan Pencairan Kredit sesuai Perjanjian Kredit :</td>
					</tr>
					<tr>
						<td><textarea nai="PERSYARATAN PENCAIRAN KREDIT" style="width: 100%;height: 100px;background-color: #FF0;" name="mpk_ppkspk" id="mpk_ppkspk"><?php echo $mpk_ppkspk?></textarea> </td>
					</tr>
				</table>
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
	<?
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='MPK'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='CKPK'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOVE'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOOP'
    
    $getCheckerMPK="";
    $getApprovalMPK="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='MPK'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerMPK=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalMPK=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
    
	$getCheckerCKPK="";
    $getApprovalCKPK="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='CKPK'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerCKPK=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalCKPK=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
	
	$getCheckerLOVE="";
    $getApprovalLOVE="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOVE'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerLOVE=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalLOVE=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
	
	
	$getCheckerLOOP="";
    $getApprovalLOOP="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOOP'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerLOOP=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalLOOP=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
    ?>
            
            
            
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td align="center" width="25%">Diusulkan oleh : Bagian Marketing</td>
						<td align="center" width="25%">Diperiksa oleh : Acceptance</td>
						<td align="center" width="25%">Diperiksa Oleh : Loan Verification</td>
                        <td align="center" width="25%">Dilaksanakan oleh : Loan Transaction</td>
					</tr>
					<tr>
                    <td colspan="4">
                    <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
                    <tr>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    </tr>
                    <tr height="100px">
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerMPK;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalMPK;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerCKPK;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalCKPK;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerLOVE;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalLOVE;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerLOOP;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalLOOP;?>&nbsp;</td>
                    </tr>
                    </table>
                    </td>
                    <!--
						<td><textarea nai="BAGIAN MARKETING" style="width: 100%;height: 100px;background-color: #FF0;" name="mpk_bagian_marketing" id="mpk_bagian_marketing"><?php echo $mpk_bagian_marketing?></textarea> </td>
						<td><textarea nai="ADMINISTRASI KREDIT" style="width: 100%;height: 100px;background-color: #FF0;" name="mpk_administrasi_kredit" id="mpk_administrasi_kredit"><?php echo $mpk_administrasi_kredit?></textarea> </td>
						<td><textarea nai="LOAN PROCESSING" style="width: 100%;height: 100px;background-color: #FF0;" name="mpk_loan_processing" id="mpk_loan_processing"><?php echo $mpk_loan_processing?></textarea> </td>
					!-->
                    </tr>
				</table>
			</td>
		</tr>
	</table>
    <input type="hidden" name="mpk_bagian_marketing" value="NULL">
    <input type="hidden" name="mpk_administrasi_kredit" value="NULL">
    <input type="hidden" name="mpk_loan_processing" value="NULL">
	<div style="text-align:center;">
		<div>&nbsp;</div>
		<div>
		
		<input nai="" type=hidden name=userpermission  value='I'>
		<input nai="" onClick="goSave()" class="button" name="save" style="width:300px;" value="Save" type="button" />

		</div>
	</div>
</form>
</body>
</html>