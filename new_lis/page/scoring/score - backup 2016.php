<?php
require ("../../lib/config.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");
//require ("../../requirepage/security.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>Phone Verification Information</title>
  <link rel="stylesheet" href="../../css/BJstyle.css" type="text/css">
</head>
<body>
<div id="content">
  <table width="100%" cellpadding="2" cellspacing="1" id="table-data" border="0px">
    <tr>
      <td colspan="9" align="center" style="background-color:#000080; color:#FFFFFF;">
        <b>SCORING</b>
      </td>
    </tr>
    <?
			$jmlscore = 0;
			$i = 1;
			$sql2    = "SELECT * FROM DW_SourceDetail WHERE sd_id = '$custnomid'"; 
			$result2 = sqlsrv_query($conn, $sql2);
			if ( $result2 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($result2))
			{
				while($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC))
				{
					$jmlscore = $jmlscore + $row2['sd_score'];
					$sd_field = $row2['sd_field'];
					
          $sd_class = $sd_field + 1;
          /* ganjil atau genap */
          if ($sd_class % 2 == 0) {
            $class_name = 'even'; /* class_name = 'even' jika genap */
          } else {
            $class_name = 'odd'; /* class_name = 'odd' jika ganjil */
          } 
		?>
    <tr class="<?php echo $class_name; ?>">
      <td width="5%" align="center"><?=$i;?></td>
			<td width="40%" align="left"><b><?=$row2['sd_pm_id']?></b></td>
			<td width="2%" class="titikDua">:</td>
			<td><?=$row2['sd_score']?></td>
		</tr>
    <?	  $i++;
				}
			}
		?>
    <tr>
  		<td colspan="4" align="center" style="font-size:14px; background-color:rgb(233,237,236);">
        <b>TOTAL SCORE</b>
      </td>
  	</tr>
  	<tr>
  		<td colspan="4" align="center" style="font-size:14px; color:#058E19; background-color:#FAFDEC;">
        <b><?=$jmlscore;?></b>
      </td>
  	</tr>
  	<tr>
  	  <?php
          $sqlscoring = "SELECT * FROM param_gscoring WHERE rangestart <= '$jmlscore' AND rangeend >= '$jmlscore' ";
          $resscoring = sqlsrv_query($conn, $sqlscoring);
          if ($resscoring === false)die( FormatErrors( sqlsrv_errors() ) );
          if(sqlsrv_has_rows($resscoring))
    			{
    				while($row = sqlsrv_fetch_array($resscoring, SQLSRV_FETCH_ASSOC))
    				{
            	$rangestart   = $row['rangestart'];
            	$rangeend     = $row['rangeend'];
            	$scoring      = $row['scoring'];
            	if ($scoring > 0)
            	{
            		 $scoring = $scoring * 100;
            		 $varscoring = number_format($scoring);
            	}
            	else
            	{
            		 $varscoring = $scoring;
            	}
        ?>
  		<td colspan="4" align="center" style="font-size:12px; background-color:rgb(233,237,236);">
        No. Aplikasi ini mendapatkan limit = <b>Rp. <? echo $varscoring;?></b>
      </td>
        <?php
            }
          }
        ?>
  	</tr>
  </table>
</div>
</body>
</html>