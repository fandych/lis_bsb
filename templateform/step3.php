<?
session_start();
ob_start();
require_once("sqlsrv.php");
require_once("preference.php");

ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");

//check from step2 or step 4
$setget="";
if(isset($_POST['setget']))
{
	$setget = $_SESSION['setget'] = $_POST['setget'];
}
else 
{
	if(isset($_SESSION['setget']))
	{
		$setget = $_SESSION['setget'];
	}
}

$value="";
if(isset($_POST['value']))
{
	$value = $_SESSION['value'] = $_POST['value'];
}
else 
{
	if(isset($_SESSION['value']))
	{
		$value = $_SESSION['value'];
	}
}

$type="";
if(isset($_POST['type']))
{
	$type = $_SESSION['type'] = $_POST['type'];
}
else 
{
	if(isset($_SESSION['type']))
	{
		$type = $_SESSION['type'];
	}
}

$param="";
if(isset($_POST['param']))
{
	$param = $_SESSION['param'] = $_POST['param'];
}
else 
{
	if(isset($_SESSION['param']))
	{
		$param = $_SESSION['param'];
	}
}


$templatecontent = $_SESSION['templatecontent'];
$templatename = $_SESSION['templatename'];

// save ke dalam temporaray database
$expldevarspin = explode(",",$_POST['namevar']);

$deletedb="delete from  tmp_templatedetail where _template_code='".$_SESSION['template']."'";
$condb->_EQ($deletedb);

$setget2="";
$value2="";
$type2="";
$param2="";
foreach( $expldevarspin as $key => $n )
{
	$setget2 .= $setget[$key].",";
	$value2 .= $value[$key].",";
	$type2 .= $type[$key].",";
	$param2 .= $param[$key].",";
	$inserttodb="
	INSERT INTO tmp_templatedetail(_idx,_template_code,_namevar,_setget,_value,_type,_param)
    VALUES ('".$_SESSION['template'].$key."','".$_SESSION['template']."','".$n."','".$setget[$key]."','".$value[$key]."','".$type[$key]."','".$param[$key]."')";
	//echo $inserttodb;
	$condb->_EQ($inserttodb);
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Create Tempalte</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />



<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="js/jquery-1.12.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>

<script>
	function onsubmitformnext()
	{
		$("#frm").attr('action', 'step4.php');
		$("#frm").submit();
	}
	function onsubmitformprev()
	{
		$("#frm").attr('action', 'step2.php');
		$("#frm").submit();
	}
	function checkerik(id)
	{
		if(fc(id)!=0)
		{
			onsubmitformnext()
		}
	}
</script>
<style>
	td{padding:5px;}
</style>
</head>
<body>
<form id="frm" name="frm" method="post" >
	<div class="container">
	<div style="text-align:left;font-size:30px;line-height:45px;">Form <? echo $_SESSION['template'] ?> <? echo $_SESSION['templatename'] ?>
		<div style="float:right;line-height:45px;">
			<a href="tmp.php?a=3&b=<?=$_SESSION['template']?>" class="btn btn-warning btn-md" role="button"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
		</div>
	</div>
	<hr>
	<?php
		
		$testarray=array();
		$getfullcontent="";
		$strsql="select * from tmp_templatedetail where _template_code='".$_SESSION['template']."'";
		$rss = $condb->_RQ($strsql);
		if(!is_array($rss)){echo $rss;}else{
		for($i=0;$i<count($rss);$i++) {
				//print_r($value);
				$_namevar=$rss[$i]['_namevar'];
				array_push($testarray,$_namevar);
				$_setget=$rss[$i]['_setget'];
				$_value=$rss[$i]['_value'];
				$_type=$rss[$i]['_type'];
				$_param=$rss[$i]['_param'];
				
				
				
				
				$require_value="";
				$require_style="";
				if($_value=="Mandatory")
				{
					$require_value="required";
					$require_style="background:#FFFF00;";
				}
				
				$value_type=strtolower($_type);
				
				//echo $_value."<br/>";
				$value="<span style=\"color:red;\">data tidak ada</span>";
				$value=$_value;
				if($_setget=="Browse to DB")
				{
					$epld=explode('.',$_value);
					$tablename=$epld[0];
					$fieldname=$epld[1];
					
					$datetime = false;
					$strsql="select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME='".$fieldname."'";
					//echo $strsql;
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						if($result[0]['DATA_TYPE']== "date" || $result[0]['DATA_TYPE']== "datetime")
						{
							$datetime = true;
						}
					}
					
					$COLUMN_NAME="_idx";
					$strsql="select COLUMN_NAME,DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME like '%custnomid%'";
					//echo $strsql;
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						$COLUMN_NAME = $result[0]['COLUMN_NAME'];
					}
					
					$strsql="select ".$fieldname." from ".$tablename." where ".$COLUMN_NAME."='".$_SESSION['template']."'";
					$strsql="select ".$fieldname." from ".$tablename." where ".$COLUMN_NAME."=''";
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){/*echo $result;*/}else
					{
						for($zz=0;$zz<count($result);$zz++)
						{
							$value=$result[$zz][$fieldname];
							if($datetime==true)
							{
								$value=$result[$zz][$fieldname]->format('Y-m-d');
							}
							else if($_type=="Currency")
							{
								$value=number_format($result[$zz][$fieldname]);
							}
						}
					}	
				}
				//$arryset = array("Textbox","Textarea");
				else if($_setget=="Textbox")
				{
					$value="<input style=\"color:black;".$require_style."\" erikmsg=\"Textbox ".$_namevar."\" ".$require_value." ".$value_type." type=\"text\" id=\"".$_namevar."\" name=\"".$_namevar."\">";
				}
				else if($_setget=="Textarea")
				{
					$value="</br><textarea style=\"color:black;".$require_style."\" erikmsg=\"Textbox ".$_namevar."\" ".$require_value." ".$value_type." rows=\"10\" cols=\"100\" id=\"".$_namevar."\" name=\"".$_namevar."\"></textarea>";
				}
				else if($_setget=="Combobox_Y_N")
				{
					$value="<select style=\"color:black;".$require_style."\" erikmsg=\"Combobox ".$_namevar."\" ".$require_value." "." id=\"".$_namevar."\" name=\"".$_namevar."\"><option value='Ada'>Ada</option><option value='Tidak Ada'>Tidak Ada</option></select>";
				}

				$lastsentence = multiexplode($testarray,$templatecontent);
				//print_r($lastsentence);
				$getfullcontent.=$lastsentence[$i].$value;
				if($i==(count($rss)-1))
				{
					$getfullcontent.=$lastsentence[count($rss)];
				}
				//echo "<hr>";
			
			}
		}
		?>
		<div id="test">
		<?
		//echo str_replace("�","",str_replace("|||",'"',$getfullcontent));
		echo str_replace("|||",'"',$getfullcontent);
		
		?>
		</div>
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input tv="test" onclick="checkerik(this.id);" id="btnnext"  style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
			<div>
				<input  onclick="onsubmitformprev()" style="float:left;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="<< Prev">
			</div>
		</div>
	</div>
</form>

</body>
</html>