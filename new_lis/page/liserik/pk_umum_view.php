<?php
require('./sqlsrv.php');
require('../../requirepage/parameter.php');
require('./terbilang.php');

date_default_timezone_set('Asia/jakarta');

$confmsg = "Are You Sure ??";

$dayList = array(
	'Sun' => 'Minggu',
	'Mon' => 'Senin',
	'Tue' => 'Selasa',
	'Wed' => 'Rabu',
	'Thu' => 'Kamis',
	'Fri' => 'Jumat',
	'Sat' => 'Sabtu'
);

$mountList = array(
	'1' => 'Januari',
	'2' => 'Februari',
	'3' => 'Maret',
	'4' => 'April',
	'5' => 'Mei',
	'6' => 'Juni',
	'7' => 'Juli',
	'8' => 'Agustus',
	'9' => 'September',
	'10' => 'Oktober',
	'11' => 'November',
	'12' => 'Desember'
);

function param_pekerjaan($pek_id){
	global $db;
	
	$v_kerjaan_name = "";
	$strsql='SELECT attribute from param_pekerjaan where code=\''.$pek_id.'\'';
	$rs = $db->_RQ($strsql);
	for($i=0;$i<count($rs);$i++)
	{
	  $v_kerjaan_name = $rs[$i]['attribute'];
	}
	
	return $v_kerjaan_name;
	
}

$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";

require("../../lib/sqlsrv.lis.php");

$db_lis = new DB_LIS();
$db_lis->connect();

//	START CUSTOMER MASTER PERSON

$v_custtarid = '';
$v_custfullname = '';
$v_custaddr = '';
$v_custrt = '';
$v_custrw = '';
$v_custkel = '';
$v_custkec = '';
$v_custcity = '';
$v_custprop = '';
$v_custktpno = '';

$strsql='SELECT * from Tbl_CustomerMasterPerson2 where custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$v_custtarid = $rs[$i]['custtarid'];
		$v_custfullname = $rs[$i]['custfullname'];
		$v_custaddr = $rs[$i]['custaddr'];
		$v_custrt = $rs[$i]['custrt'];
		$v_custrw = $rs[$i]['custrw'];
		$v_custkel = $rs[$i]['custkel'];
		$v_custkec = $rs[$i]['custkec'];
		$v_custcity = $rs[$i]['custcity'];
		$v_custprop = $rs[$i]['custprop'];
		$v_custktpno = $rs[$i]['custktpno'];
	}
}

//	END CUSTOMER MASTER PERSON

// 	START INFO KERJA

if($v_custtarid == "1")
{
	$info_kerja = "SELECT _pekerjaan, _dinasname as _office FROM tbl_KGB_infokerja2 WHERE _custnomid = '$custnomid'";
}
else if($v_custtarid == "2")
{
	$info_kerja = "SELECT _jenispekerjaan as _pekerjaan, _namaperusahan as _office FROM tbl_KPR_infokerja2 WHERE _custnomid = '$custnomid'";
}

$db_lis->executeQuery($info_kerja);
$info_kerja = $db_lis->lastResults;

$v_pekerjaan = param_pekerjaan($info_kerja[0]['_pekerjaan']);
$v_office = $info_kerja[0]['_office'];

//	END INFO KERJA

// START INFO PASANGAN

$v_pasangan_nomorktp = ' - ';
$v_pasangan_namadepan = ' - ';
$v_pasangan_namatengah = ' - ';
$v_pasangan_namabelakang = ' - ';
$v_pasangan_pekerjaan = ' - ';

$strsql = 'SELECT _pasangan_nomorktp,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang,_pasangan_pekerjaan_id
			FROM pl_custpasangan2 WHERE _custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$v_pasangan_nomorktp = $rs[$i]['_pasangan_nomorktp'];
		$v_pasangan_namadepan = $rs[$i]['_pasangan_namadepan'];
		$v_pasangan_namatengah = $rs[$i]['_pasangan_namatengah'];
		$v_pasangan_namabelakang = $rs[$i]['_pasangan_namabelakang'];
		$v_pasangan_pekerjaan = param_pekerjaan($rs[$i]['_pasangan_pekerjaan_id']);
	}
}

//END INFO PASANGAN

//START ANALYST APPROVAL

$index_id = '';
$source = '';
$cr_option = '';
$fac_seq = '';
$plafond = '0';
$tenor = '0';
$installment = '0';
$interest = '0';
$provisi = '0';
$opinion = '';
$description = '';
$strsql='SELECT *  FROM CR_ANALYST_APPROVAL WHERE custnomid=\''.$custnomid.'\'';
$custnpwpno='';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$index_id= $rs[$i]['index_id'];
		$custnomid= $rs[$i]['custnomid'];
		$source= $rs[$i]['source'];
		$cr_option= $rs[$i]['cr_option'];
		$fac_seq= $rs[$i]['fac_seq'];
		$plafond= $rs[$i]['plafond'];
		$tenor= $rs[$i]['tenor'];
		$installment= $rs[$i]['installment'];
		$interest= $rs[$i]['interest'];
		$provisi= $rs[$i]['provisi'];
		$opinion= $rs[$i]['opinion'];
		$description= $rs[$i]['description'];
	}
}

//END ANALYST APPROVAL

//START INFO COLLATERAL

$cust_jeniscol='';
$col_id='';

$strsql='SELECT cust_jeniscol,col_id FROM Tbl_Cust_MasterCol WHERE ap_lisregno=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$cust_jeniscol= $rs[$i]['cust_jeniscol'];
		$col_id= $rs[$i]['col_id'];
	}
}

$jml_col = count($rs);

$v_coltype = "";
$v_col_imbluas = "";
$v_col_certluas = "";
$v_col_addr1 = "";
$v_col_devname = "";
$v_col_certtype = "";
$v_col_certno = "";
$v_col_certdate = "";
$v_col_certatasnama = "";

if($cust_jeniscol == "BA1")
{
	$v_coltype = "Rumah Tinggal";
	
	$strsql='SELECT * FROM tbl_COL_Building WHERE ap_lisregno=\''.$custnomid.'\' AND col_id=\''.$col_id.'\'';
	$rs = $db->_RQ($strsql);
	if(is_array($rs))
	{
		for($i=0;$i<count($rs);$i++)
		{
			$v_col_imbluas= $rs[$i]['col_imbluas'];
			$v_col_devname= $rs[$i]['col_devname'];
		}
	}
	
	$strsql='SELECT *, CONVERT(VARCHAR,col_certdate,111) as col_certdate FROM tbl_COL_Land WHERE ap_lisregno=\''.$custnomid.'\' AND col_id=\''.$col_id.'\'';
	$rs = $db->_RQ($strsql);
	if(is_array($rs))
	{
		for($i=0;$i<count($rs);$i++)
		{
			$v_col_certluas= $rs[$i]['col_certluas'];
			$v_col_addr1= $rs[$i]['col_addr1'];
			$v_col_certtype = $rs[$i]['col_certtype'];
			$v_col_certno = $rs[$i]['col_certno'];
			$v_col_certdate = $rs[$i]['col_certdate'];
			$v_col_certatasnama = $rs[$i]['col_certatasnama'];
		}
	}
}

//END INFO COLLATERAL

//START CR DATA

$harga_jual = '';
$uang_muka = '';
$rencana_kredit = '';
$persentase_setoran_uang_muka = '';
$persentase_pembiayaan = '';
$max_pembiayaan = '';
$ket_identitas = '';
$ket_penghasilan = '';
$ket_kredit = '';
$kesimpulan = '';
$data_identitas = '';
$data_pekerjaan = '';
$data_penghasilan = '';
$data_syarat_kredit = '';
$ket_bunga = '';
$ket_angsuran = '';
$pengikatan_jaminan = '';
$biaya_adm = '0';
$syarat_ttd_pk = '';
$syarat_realisasi = '';
$lain_lain = '';
$custnpwpno='';
$strsql='SELECT *  FROM CR_DATA WHERE custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$custnomid= $rs[$i]['custnomid'];
		$harga_jual= $rs[$i]['harga_jual'];
		$uang_muka= $rs[$i]['uang_muka'];
		$rencana_kredit= $rs[$i]['rencana_kredit'];
		$persentase_setoran_uang_muka= $rs[$i]['persentase_setoran_uang_muka'];
		$persentase_pembiayaan= $rs[$i]['persentase_pembiayaan'];
		$max_pembiayaan= $rs[$i]['max_pembiayaan'];
		$ket_identitas= $rs[$i]['ket_identitas'];
		$ket_penghasilan= $rs[$i]['ket_penghasilan'];
		$ket_kredit= $rs[$i]['ket_kredit'];
		$kesimpulan= $rs[$i]['kesimpulan'];
		$data_identitas= $rs[$i]['data_identitas'];
		$data_pekerjaan= $rs[$i]['data_pekerjaan'];
		$data_penghasilan= $rs[$i]['data_penghasilan'];
		$data_syarat_kredit= $rs[$i]['data_syarat_kredit'];
		$ket_bunga= $rs[$i]['ket_bunga'];
		$ket_angsuran= $rs[$i]['ket_angsuran'];
		$pengikatan_jaminan= $rs[$i]['pengikatan_jaminan'];
		$biaya_adm= $rs[$i]['biaya_adm'];
		$syarat_ttd_pk= $rs[$i]['syarat_ttd_pk'];
		$syarat_realisasi= $rs[$i]['syarat_realisasi'];
		$lain_lain= $rs[$i]['lain_lain'];
	}
}

//END CR DATA

// START LEGAL OPINION INFO
	
$UNTUKPEMBELIAN = '';
$strsql="SELECT * FROM LEGAL_OPINION WHERE custnomid = '$custnomid'";
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
		$UNTUKPEMBELIAN= $rs[$i]['UNTUKPEMBELIAN'];
	}
}
	
// END LEGAL OPINION INFO

// START BRANCH INFO

$branch_name='';
$branch_region_code='';
$branch_address='';
$branch_city='';
$strsql='SELECT branch_name,branch_region_code,branch_address,branch_city from Tbl_Branch where branch_code=\''.$userbranch.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
	$branch_name= $rs[$i]['branch_name'];
	$branch_region_code= $rs[$i]['branch_region_code'];
	$branch_address= $rs[$i]['branch_address'];
	$branch_city= $rs[$i]['branch_city'];
}

// END BRANCH INFO

$_cond_code = '';
$_type = '';
$_merk = '';
$_model = '';
$_jns_kendaraan = '';
$_thnpembuatan = '';
$_silinder_isi = '';
$_silinder_wrn = '';
$_norangka = '';
$_nomesin = '';
$_bpkb_tgl = '';
$_stnk_exp = '';
$_faktur_tgl = '';
$_bahanbakar = '';
$_bpkb_nama = '';
$_bpkb_addr1 = '';
$_bpkb_addr2 = '';
$_bpkb_addr3 = '';
$_perlengkapan = '';
$_notes = '';
$_opini = '';
$_officer_code = '';
$_tanggal_kunjungan = '';
$strsql='SELECT * FROM appraisal_vhc WHERE _collateral_id=\''.$col_id.'\'';
$rs = $apr->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_cond_code= $rs[$i]['_cond_code'];
  $_type= $rs[$i]['_type'];
  $_merk= $rs[$i]['_merk'];
  $_model= $rs[$i]['_model'];
  $_jns_kendaraan= $rs[$i]['_jns_kendaraan'];
  $_thnpembuatan= $rs[$i]['_thnpembuatan'];
  $_silinder_isi= $rs[$i]['_silinder_isi'];
  $_silinder_wrn= $rs[$i]['_silinder_wrn'];
  $_norangka= $rs[$i]['_norangka'];
  $_nomesin= $rs[$i]['_nomesin'];
  $_bpkb_tgl= $rs[$i]['_bpkb_tgl'];
  $_stnk_exp= $rs[$i]['_stnk_exp'];
  $_faktur_tgl= $rs[$i]['_faktur_tgl'];
  $_bahanbakar= $rs[$i]['_bahanbakar'];
  $_bpkb_nama= $rs[$i]['_bpkb_nama'];
  $_bpkb_addr1= $rs[$i]['_bpkb_addr1'];
  $_bpkb_addr2= $rs[$i]['_bpkb_addr2'];
  $_bpkb_addr3= $rs[$i]['_bpkb_addr3'];
  $_perlengkapan= $rs[$i]['_perlengkapan'];
  $_notes= $rs[$i]['_notes'];
  $_opini= $rs[$i]['_opini'];
  $_officer_code= $rs[$i]['_officer_code'];
  $_tanggal_kunjungan= $rs[$i]['_tanggal_kunjungan'];
}


$ap_lisregno = '';

$col_addr1 = '';
$col_addr2 = '';
$col_addr3 = '';
$col_kodepos = '';
$col_nopol = '';
$col_stnk_no = '';
$col_stnkexp = '';
$col_fakturno = '';
$col_fakturtgl = '';
$col_bpkbno = '';
$col_bpkbtgl = '';
$col_condition = '';
$col_type = '';
$col_model = '';
$col_merk = '';
$col_tahunpembuatan = '';
$col_jeniskendaran = '';
$col_silinder = '';
$col_warna = '';
$col_norangka = '';
$col_nomesin = '';
$col_bpkbnama = '';
$col_bpkbaddr1 = '';
$col_bpkbaddr2 = '';
$col_bpkbaddr3 = '';
$col_perlengkapan = '';
$col_desc = '';
$col_nilaiwajar = '';
$col_nilailikuidasi = '';
$col_safemargin = '';
$col_appraiser = '';
$col_appraisdate = '';
$col_mk_code = '';
$nilai_likuidasi2 = '';
$strsql='SELECT * from tbl_COL_Vehicle where col_id=\''.$col_id.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

$ap_lisregno= $rs[$i]['ap_lisregno'];
$col_id= $rs[$i]['col_id'];
$col_addr1= $rs[$i]['col_addr1'];
$col_addr2= $rs[$i]['col_addr2'];
$col_addr3= $rs[$i]['col_addr3'];
$col_kodepos= $rs[$i]['col_kodepos'];
$col_nopol= $rs[$i]['col_nopol'];
$col_stnk_no= $rs[$i]['col_stnk_no'];
$col_stnkexp= $rs[$i]['col_stnkexp'];
$col_fakturno= $rs[$i]['col_fakturno'];
$col_fakturtgl= $rs[$i]['col_fakturtgl'];
$col_bpkbno= $rs[$i]['col_bpkbno'];
$col_bpkbtgl= $rs[$i]['col_bpkbtgl'];
$col_condition= $rs[$i]['col_condition'];
$col_type= $rs[$i]['col_type'];
$col_model= $rs[$i]['col_model'];
$col_merk= $rs[$i]['col_merk'];
$col_tahunpembuatan= $rs[$i]['col_tahunpembuatan'];
$col_jeniskendaran= $rs[$i]['col_jeniskendaran'];
$col_silinder= $rs[$i]['col_silinder'];
$col_warna= $rs[$i]['col_warna'];
$col_norangka= $rs[$i]['col_norangka'];
$col_nomesin= $rs[$i]['col_nomesin'];
$col_bpkbnama= $rs[$i]['col_bpkbnama'];
$col_bpkbaddr1= $rs[$i]['col_bpkbaddr1'];
$col_bpkbaddr2= $rs[$i]['col_bpkbaddr2'];
$col_bpkbaddr3= $rs[$i]['col_bpkbaddr3'];
$col_perlengkapan= $rs[$i]['col_perlengkapan'];
$col_desc= $rs[$i]['col_desc'];
$col_nilaiwajar= $rs[$i]['col_nilaiwajar'];
$col_nilailikuidasi= $rs[$i]['col_nilailikuidasi'];
$col_safemargin= $rs[$i]['col_safemargin'];
$col_appraiser= $rs[$i]['col_appraiser'];
$col_appraisdate= $rs[$i]['col_appraisdate'];
$col_mk_code= $rs[$i]['col_MK_CODE'];
$nilai_likuidasi2= $rs[$i]['nilai_likuidasi2'];
}


$nomorpk = '/PK.Ang.KGS/SKO/'.date('Y');
$tanggalpk = date('Y-m-d');
$namapemimpin_1 = '';
$jabatanpemimpin_i = '';
$text1 = '';
$p2_jenis = 'Rumah Tinggal';
$p2_luas = $v_col_imbluas." / ".$v_col_certluas." M2";
$p2_block = '';
$p2_nomor = '';
$p2_lokasi = '';
$p2_penjual = $v_col_devname;
$p3_tanggalkreditberakhir = date('Y-m-d');
$p3_tanggalselambatnya = date('Y-m-d');
$p3_dendaketerlambatan = '';
$p3_biayaadmin = '';
$p4_tanggalkreditmulai = date('Y-m-d');
$p4_rektabungan = '';
$p4_atasnama = '';
$p4_rekpinjaman = '';
$p15_pengadilanengeri = '';
$p15_badanpiutang = '';

$strsql = "select * from PK_KGS where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);
if(is_array($rs))
{
	for($i=0;$i<count($rs);$i++)
	{
	  $custnomid= $rs[$i]['CUSTNOMID'];
	  $nomorpk= $rs[$i]['NOMORPK'];
	  $namapemimpin_1= $rs[$i]['NAMAPEMIMPIN_1'];
	  $jabatanpemimpin_i= $rs[$i]['JABATANPEMIMPIN_i'];
	  $text1= $rs[$i]['TEXT1'];
	  $tanggalpk= $rs[$i]['TANGGALPK']->format('d-m-Y');
	  $p2_jenis= $rs[$i]['P2_JENIS'];
	  $p2_luas= $rs[$i]['P2_LUAS'];
	  $p2_block= $rs[$i]['P2_BLOCK'];
	  $p2_nomor= $rs[$i]['P2_NOMOR'];
	  $p2_lokasi= $rs[$i]['P2_LOKASI'];
	  $p2_penjual= $rs[$i]['P2_PENJUAL'];
	  $p3_tanggalkreditberakhir= $rs[$i]['P3_TANGGALKREDITBERAKHIR']->format('d-m-Y');
	  $p3_tanggalselambatnya= $rs[$i]['P3_TANGGALSELAMBATNYA']->format('d-m-Y');
	  $p3_dendaketerlambatan= $rs[$i]['P3_DENDAKETERLAMBATAN'];
	  $p4_tanggalkreditmulai= $rs[$i]['P4_TANGGALKREDITMULAI']->format('d-m-Y');
	  $p4_rekpinjaman= $rs[$i]['P4_REKPINJAMAN'];
	  $p15_pengadilanengeri= $rs[$i]['P15_PENGADILANENGERI'];
	  $p15_badanpiutang= $rs[$i]['P15_BADANPIUTANG'];
	}
}





$_type_jaminan = '';
$_lokasi_rumah = '';
$_jumlah_lantai = '';
$_luas_bangunan = '';
$_panjang_bangunan = '';
$_lebar_bangunan = '';
$_arah_bangunan = '';
$_umur_bangunan = '';
$_tahun_dibangun = '';
$_tahun_renovasi = '';
$_luas_tanah = '';
$_panjang_tanah = '';
$_lebar_tanah = '';
$_sisi_utara = '';
$_sisi_timur = '';
$_sisi_selatan = '';
$_sisi_barat = '';
$_latitude = '';
$_longitude = '';
$_notes = '';
$_opini = '';
$_officer_code = '';
$_tanggal_kunjungan = '';
$strsql='SELECT * FROM appraisal_tnb WHERE _collateral_id=\''.$col_id.'\'';
$rs = $apr->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

  $_collateral_id= $rs[$i]['_collateral_id'];
  $_type_jaminan= $rs[$i]['_type_jaminan'];
  $_lokasi_rumah= $rs[$i]['_lokasi_rumah'];
  $_jumlah_lantai= $rs[$i]['_jumlah_lantai'];
  $_luas_bangunan= $rs[$i]['_luas_bangunan'];
  $_panjang_bangunan= $rs[$i]['_panjang_bangunan'];
  $_lebar_bangunan= $rs[$i]['_lebar_bangunan'];
  $_arah_bangunan= $rs[$i]['_arah_bangunan'];
  $_umur_bangunan= $rs[$i]['_umur_bangunan'];
  $_tahun_dibangun= $rs[$i]['_tahun_dibangun'];
  $_tahun_renovasi= $rs[$i]['_tahun_renovasi'];
  $_luas_tanah= $rs[$i]['_luas_tanah'];
  $_panjang_tanah= $rs[$i]['_panjang_tanah'];
  $_lebar_tanah= $rs[$i]['_lebar_tanah'];
  $_sisi_utara= $rs[$i]['_sisi_utara'];
  $_sisi_timur= $rs[$i]['_sisi_timur'];
  $_sisi_selatan= $rs[$i]['_sisi_selatan'];
  $_sisi_barat= $rs[$i]['_sisi_barat'];
  $_latitude= $rs[$i]['_latitude'];
  $_longitude= $rs[$i]['_longitude'];
  $_notes= $rs[$i]['_notes'];
  $_opini= $rs[$i]['_opini'];
  $_officer_code= $rs[$i]['_officer_code'];
  $_tanggal_kunjungan= $rs[$i]['_tanggal_kunjungan'];
}


?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Perjanjian Kredit</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
    <script type="text/javascript" src="../../js/full_function.js" ></script>
    <script type="text/javascript" src="../../js/accounting.js" ></script>

    <script type="text/javascript">


    function submitform()
    {
		if($('#nomorpk').val()==""){
        	$('#nomorpk').focus();
            alert('Nomor PK Harus Diisi');
        }else if($('#namapemimpin_1').val()==""){
        	$('#namapemimpin_1').focus();
            alert('Nama Pemimpin Harus Diisi');
        }
        else if($('#jabatanpemimpin_i').val()==""){
        	$('#jabatanpemimpin_i').focus();
            alert('Jabatan Pemimpin Harus Diisi');
        }
        else if($('#txt_point1').val()==""){
        	$('#txt_point1').focus();
            alert('Text Point 1 Harus Diisi');
        }
        else if($('#p2_jenis').val()==""){
        	$('#p2_jenis').focus();
            alert('Jenis Bangunan Harus Diisi');
        }
        else if($('#p2_luas').val()==""){
        	$('#p2_luas').focus();
            alert('Luas Bangunan Harus Diisi');
        }
        else if($('#p2_block').val()==""){
        	$('#p2_block').focus();
            alert('Blok Harus Diisi');
        }
        else if($('#p2_nomor').val()==""){
        	$('#p2_nomor').focus();
            alert('Nomor Bangunan Harus Diisi');
        }
        else if($('#p2_lokasi').val()==""){
        	$('#p2_lokasi').focus();
            alert('Lokasi Harus Diisi');
        }
        else if($('#p2_penjual').val()==""){
        	$('#p2_penjual').focus();
            alert('Penjual Harus Diisi');
        }
        else if($('#p3_tanggalkreditberakhir').val()==""){
        	$('#p3_tanggalkreditberakhir').focus();
            alert('Tanggal kredit Berakrhir Harus Diisi');
        }
        else if($('#p3_dendaketerlambatan').val()==""){
        	$('#p3_dendaketerlambatan').focus();
            alert('Denda Keterlambatan Harus Diisi');
        }
        else if($('#p3_biayaadminpersen').val()==""){
        	$('#p3_biayaadminpersen').focus();
            alert('Biaya Admin Harus Diisi');
        }
        else if($('#p4_tanggalkreditmulai').val()==""){
        	$('#p4_tanggalkreditmulai').focus();
            alert('Tanggal Kredit Dimulai Harus Diisi');
        }
        else if($('#p4_rektabungan').val()==""){
        	$('#p4_rektabungan').focus();
            alert('Rekening Tabungan Harus Diisi');
        }
        else if($('#p4_atasnama').val()==""){
        	$('#p4_atasnama').focus();
            alert('Atas Nama Rekening Tabungan Harus Diisi');
        }
        else if($('#p4_rekpinjaman').val()==""){
        	$('#p4_rekpinjaman').focus();
            alert('Rekening Pinjaman Harus Diisi');
        }
        else if($('#p15_pengadilanengeri').val()==""){
        	$('#p15_pengadilanengeri').focus();
            alert('Pengadilan Negri Harus Diisi');
        }
        else if($('#p15_badanpiutang').val()==""){
        	$('#p15_badanpiutang').focus();
            alert('Badan Piutang Harus Diisi');
        }
      else
      {
        $('#frm').submit();
      }
    }
    </script>

    <style media="screen">
      ol li {padding: 5px 0px;}
      .pihak {text-align:center;font-weight: bold;}
      #tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}
    </style>
	
</head>
<body>
    <form id="frm" action="pk_umum_exec.php" method="post">
    <?
      if($userid != "" && $userwfid!="")
      {
    ?>
      <div style="float: right;margin-right: 220px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
    <?
      }
    ?>
      <br />
      <br />
		<table id="tableform" border="0" align="center">
			<tr>
				<td style="text-align:justify">
					<p style="text-align:center;">
						<b>
							PERJANJIAN KREDIT
							<br/>
						</b>
					</p>
					<p style="text-align:center;">
						Nomor :
						<?echo $nomorpk;?>
					</p>
					<p>
						Pada hari ini <?php echo $dayList[date("D",strtotime($tanggalpk))];?> tanggal <?php echo ucwords(Terbilang(date("j",strtotime($tanggalpk))))?> bulan <?php echo $mountList[date("n",strtotime($tanggalpk))];?> tahun <?php echo ucwords(Terbilang(date("Y",strtotime($tanggalpk))))?> ( <?php echo date("d-m-Y",strtotime($tanggalpk));?> )
						yang  bertanda tangan dibawah ini :
					</p>
					
					<ol style="margin-left:-25px;" type="I">
						<li>
							<? echo $namapemimpin_1; ?>, selaku 
							<? echo $jabatanpemimpin_i; ?> 
							<?php echo $text1;?>
							, untuk selanjutnya disebut dengan :
							<p class="pihak">BANK</p>
						</li>
						<li>
							<table style="margin-top:-20px;">
								<tr>
									<td style="width:200px;">N a m a</td>
									<td>: </td>
									<td><?php echo $v_custfullname;?></td>
								</tr>
								<tr>
									<td valign="top">A l a m a t</td>
									<td valign="top">: </td>
									<td><?php echo $v_custaddr." RT. ".$v_custrt." RW. ".$v_custrw.", Kel. ".$v_custkel.", Kec. ".$v_custkec.", ".$v_custcity.", ".$v_custprop;?></td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>: </td>
									<td><?echo $v_pekerjaan?> - <?echo $v_office?></td>
								</tr>
								<tr>
									<td>Kartu Tanda Penduduk</td>
									<td>: </td>
									<td><?php echo $v_custktpno;?></td>
								</tr>
							</table>
							
							<p>
								yang dalam hal ini bertindak untuk dan atas nama dirinya sendiri dan untuk melakukan tindakan 
								hukum dalam Perjanjian Kredit ini dengan memperoleh persetujuan dari istrinya / suaminya*  yaitu
								<?php echo $v_pasangan_namadepan.' '.$v_pasangan_namatengah.' '.$v_pasangan_namabelakang;  ?> 
								pekerjaan <?php echo $v_pasangan_pekerjaan;?> bertempat tinggal di <?php echo $v_custaddr." RT. ".$v_custrt." RW. ".$v_custrw.", Kel. ".$v_custkel.", Kec. ".$v_custkec.", ".$v_custcity.", ".$v_custprop;?> 
								pemegang KTP Nomor <?php echo $v_pasangan_nomorktp;?> yang turut hadir dan menandatangani 
								Perjanjian Kredit ini, untuk selanjutnya disebut :
							</p>
							<p class="pihak">
								DEBITUR
							</p>
						</li>
					</ol>
					
					<p>
						BANK dan DEBITUR telah saling setuju dan sepakat untuk dan dengan ini membuat serta menetapkan Perjanjian Kredit ini berikut semua lampiran, 
						perubahan dan atau penambahan dan atau pembaharuannya kemudian, apabila ada, untuk selanjutnya disebut Perjanjian Kredit, untuk dipatuhi dan dilaksanakan oleh para pihak tersebut, dengan syarat-syarat dan ketentuan sebagai berikut : 
					</p>
					<p style="text-align:center;">
						<b>
							PASAL 1
							<br/>
							MAKSIMUM KREDIT
						</b>
					</p>
					<p>
						BANK memberikan fasilitas kredit KGS kepada DEBITUR dan DEBITUR menyatakan mengaku dan 
						menerima fasilitas kredit tersebut dari BANK dengan maksimum kredit sebesar Rp. <?php echo number_format($plafond);?>,- (<?php echo ucwords(Terbilang($plafond));?> Rupiah).
					</p>
					<p style="text-align:center;">
						<b>
						  PASAL 2
						  <br/>
						  TUJUAN PENGGUNAAN KREDIT
						</b>
					</p>
					<p>
						Fasilitas  kredit yang diberikan  BANK kepada  DEBITUR sebagaimana tersebut PASAL 1 Perjanjian Kredit ini digunakan 
						DEBITUR untuk Pembelian  <?php echo $jml_col;?> (<?php echo ucwords(Terbilang($jml_col));?> ) unit rumah berikut tanahnya guna dimiliki dan dihuni sendiri oleh DEBITUR dengan data sebagai berikut :
						
						<table>
							<tr>
								<td>- Jenis / Type</td>
								<td>: </td>
								<td>
									<? echo $p2_jenis; ?>
								</td>
							</tr>
							<tr>
								<td style="width:200px;">- Luas rumah / tanah</td>
								<td>: </td>
								<td>
									<? echo $p2_luas; ?>
								</td>
							</tr>
							<tr>
								<td>- Blok / JALAN</td>
								<td>: </td>
								<td>
									<? echo $p2_block; ?>
								</td>
							</tr>
							<tr>
								<td>- Nomor</td>
								<td>: </td>
								<td>
									<? echo $p2_nomor; ?>
								</td>
							</tr>
							<tr>
								<td>- Lokasi</td>
								<td>: </td>
								<td>
									<? echo $p2_lokasi; ?>
								</td>
							</tr>
							<tr>
								<td>- Perusahaan Pengelola</td>
								<td>: </td>
								<td>
									<? echo $p2_penjual; ?>
								</td>
							</tr>
						</table>
					</p>
					<p style="text-align:center;">
						<b>
							PASAL 3
							<br/>
							KETENTUAN KREDIT
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Jangka waktu kredit selama <?php echo $tenor;?> (<?php echo ucwords(Terbilang($tenor));?>) bulan terhitung sejak 
							ditandatanganinya Perjanjian Kredit ini sampai dengan tanggal 
							<? echo $p3_tanggalkreditberakhir; ?>
							sehingga DEBITUR wajib melunasi seluruh hutang pokok, bunga, denda kredit dan biaya-biaya lainnya kepada BANK selambat-lambatnya 
							pada tanggal <? echo $p3_tanggalselambatnya; ?>.
						</li>
						<li>
							Sehubungan dengan pemberian fasilitas kredit tersebut DEBITUR dengan ini berjanji dan mengikatkan diri untuk membayar kepada BANK:
							<ol type="a">
								<li>
									Bunga sebesar <?echo $interest?>% per tahun, sehingga angsuran 
									pokok dan bunga setiap bulan sebesar Rp. <?php echo number_format($installment);?>,- (<?php echo ucwords(Terbilang($installment));?> Rupiah).
								</li>
								<li>
									Provisi untuk selama jangka waktu kredit sebesar  <?echo number_format($provisi,2);?>% 
									dari maksimal kredit sebagaimana dimaksud dalam pasal 1 Perjanjian Kredit ini dan dibayar DEBITUR 
									pada saat Perjanjian Kredit ini ditandatangani.
								</li>
								<li>
									Biaya Administrasi sebesar Rp. <?echo number_format($biaya_adm)?>,- (<?php echo ucwords(Terbilang($biaya_adm));?> Rupiah) yang dibayar pada saat Perjanjian Kredit ini ditandatangani.
								</li>
							</ol>
						</li>
						<li>
							Apabila DEBITUR tidak membayar angsuran kredit sesuai dengan tanggal jatuh tempo angsuran/yang telah ditetapkan, 
							maka DEBITUR wajib membayar denda keterlambatan angsuran sebesar 
							<? echo $p3_dendaketerlambatan; ?> % 
							dari jumlah bunga  yang telambat dibayar tersebut.
						</li>
						<li>
							Apabila DEBITUR melunasi kredit lebih awal dari jangka waktu yang telah diperjanjikan sebagaimana tersebut ayat 1 pasal ini , 
							maka DEBITUR akan dikenakan biaya pelunasan sesuai dengan ketentuan intern yang berlaku pada Bank.
						</li>
						<li>
							Debitur dapat mengajukan fasilitas penurunan plafond dan/atau jangka waktu kredit jika diperlukan oleh DEBITUR.
						</li>
						<li>
							DEBITUR menyatakan setuju bahwa untuk dapat memperoleh fasilitas penurunan plafond dan/atau jangka waktu sebagaimana dimaksud pada ayat 5 Pasal ini, maka DEBITUR wajib memenuhi ketentuan sebagai berikut :
							
							<ol type="a">
								<li>
									Rekening wajib dalam kondisi lancar (Kolektabilitas 1).
								</li>
								<li>
									DEBITUR wajib mengajukan surat permohonan penurunan plafond dan/atau jangka waktu.
								</li>
								<li>
									Jangka waktu KGS minimal sudah berjalan selama 24 bulan.
								</li>
								<li>
									DEBITUR tidak diperkenankan menambah plafond dan/atau menambah jangka waktu pinjaman.
								</li>
								<li>
									Jangka waktu kredit setelah dilakukan setoran penurunan plafond adalah maksimal sebesar sisa jangka waktu awal.
								</li>
								<li>
									Syarat dan prosedur pengajuan fasilitas penurunan plafond dan/atau jangka waktu sesuai dengan ketentuan yang berlaku pada BANK.
								</li>
							</ol>
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 4
							<br/>
							CARA PEMBAYARAN ANGSURAN KREDIT
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							DEBITUR  diwajibkan  melakukan  pembayaran  angsuran  kredit setiap bulan  sebanyak 
							<?php echo $tenor;?> (<?php echo ucwords(Terbilang($tenor));?>) kali angsuran atau selama 
							<?php echo $tenor;?> (<?php echo ucwords(Terbilang($tenor));?>) bulan yang harus dibayar setiap 
							tanggal <?php echo ucwords(Terbilang(date('j', strtotime($p4_tanggalkreditmulai))))." ( ".date('j', strtotime($p4_tanggalkreditmulai))." )";?> setiap bulan dan untuk angsuran pertama dimulai pada tanggal 
							<? echo $p4_tanggalkreditmulai; ?>
							sebagaimana jadwal angsuran terlampir berikut perubahan-perubahannya yang merupakan satu kesatuan serta bagian yang 
							tidak terpisahkan dari Perjanjian Kredit ini.
						</li>
						<li>
							Jumlah angsuran  yang wajib dibayar oleh DEBITUR adalah pokok beserta bunga yang dibayar secara bersama-sama sebesar 
							Rp. <?php echo number_format($installment);?>,- (<?php echo ucwords(Terbilang($installment));?> Rupiah). 
						</li>
						<li>
							Untuk pelaksanaan pembayaran angsuran kredit pada ayat 1 pasal ini dilakukan dengan cara memotong langsung 
							Gaji/Penghasilan DEBITUR melalui Bendaharawan Gaji pada Instansi/Dinas  setiap bulan sesuai dengan Surat Kuasa untuk 
							memotong Gaji dari DEBITUR kepada Bendaharawan atau dapat disetor secara tunai ke rekening tabungan atau pinjaman DEBITUR pada BANK,
							atau dengan cara lain yang telah diperjanjikan dan disepakati oleh BANK dan DEBITUR pada BANK untuk selanjutnya akan di auto 
							transfer dan atau didebet ke rekening pinjaman atas nama pemohon A.C 
							<? echo $p4_rekpinjaman; ?>.
						</li>
						<li>
							Apabila karena sesuatu hal yang menyebabkan penghentian  pembayaran  gaji/ penghasilan  dari usaha DEBITUR, 
							maka terhadap sisa kredit/ seluruh  kewajiban  DEBITUR ( termasuk bunga, denda, dan biaya lainnya) pada BANK, 
							wajib dilunasi oleh ahli waris DEBITUR.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 5
							<br/>
							ASURANSI DAN BEBAN BIAYA
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							DEBITUR menyetujui dan mewajibkan serta mengikatkan diri, dengan ditandatanganinya Perjanjian Kredit ini sekaligus memberikan kuasa kepada BANK yang tidak dapat ditarik kembali, untuk dan atas nama DEBITUR, BANK menutup asuransi jiwa DEBITUR dan asuransi agunan DEBITUR pada  perusahaan  asuransi yang telah ditetapkan oleh BANK, dengan ketentuan jenis resiko, nilai pertanggungan, jangka waktu pertanggungan yang ditentukan oleh BANK dan didalam perjanjian  asuransi/ polis ditetapkan klausul sedemikian rupa untuk kepentingan BANK,sehingga jika  terjadi  pembayaran ganti rugi/ klaim, BANK berhak untuk memperhitungkan hasil pembayaran klaim tersebut untuk pelunasan seluruh kewajiban DEBITUR pada BANK.
						</li>
						<li>
							Premi asuransi tersebut pada ayat 1  pasal  ini   dibayar  lunas oleh DEBITUR atau dipotong dari jumlah kredit yang akan diterima DEBITUR berdasarkan Perjanjian Kredit ini.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 6
							<br/>
							AGUNAN KREDIT
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Agunan pokok kredit adalah penghasilan DEBITUR baik tetap maupun tidak tetap.
						</li>
						<li>
							Harta kekayaan DEBITUR, baik yang bergerak maupun yang tidak bergerak, baik yang menjadi agunan atau sebagai agunan tambahan yang ditunjuk DEBITUR atas kredit ini, dapat menjadi sumber pelunasan kredit DEBITUR.
						</li>
						<li>
							Untuk menjamin pembayaran kembali angsuran kredit dengan tertib dan sebagaimana mestinya oleh DEBITUR yang karena sebab apapun 
							juga terhutang dan wajib dibayar oleh DEBITUR kepada BANK berdasarkan Perjanjian Kredit ini, 
							maka DEBITUR menyerahkan agunan tambahan berupa :
							<br/>
							<?echo $UNTUKPEMBELIAN.', '.$v_col_addr1.' dengan Luas Bangunan/Tanah '.$v_col_imbluas.'/'.$v_col_certluas.' M2 '.$v_col_certtype.' No. '.$v_col_certno.' Tgl. '.$v_col_certdate.', An. '.$v_col_certatasnama.' (Akan dibalik nama atas nama pemohon).';?>
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 7
							<br/>
							PENYELENGGARAAN REKENING PINJAMAN
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Sebagai pelaksanaan Perjanjian Kredit ini, BANK membuka rekening tersendiri atas nama DEBITUR yang dinamakan rekening pinjaman dan rekening atau catatan-catatan lainnya sesuai dengan cara dan ketentuan yang berlaku pada BANK dan merupakan satu kesatuan yang tidak dapat dipisahkan dari Perjanjian Kredit ini.
						</li>
						<li>
							Penyelenggaran rekening pinjaman dan rekening atau catatan-catatan lainnya tersebut ayat 1 pasal ini dilakukan oleh BANK pada Kantor Cabangnya yaitu Cabang <?php echo $branch_name;?>.
						</li>
						<li>
							Untuk Keperluan administrasi, BANK mewajibkan DEBITUR membuka Rekening Tabungan pada BANK dan menabung dalam rangka pemupukan angsuran kredit serta memelihara saldo tabungan setiap bulannya sampai dengan kredit lunas minimal sebesar 1 (satu) kali angsuran.
						</li>
						<li>
							Sesuai dengan cara dan ketentuan yang berlaku pada BANK, BANK akan membuat catatan baik dalam rekening DEBITUR maupun dalam catatan-catatan lainnya mengenai jumlah-jumlah yang sewaktu-waktu dipinjamkan kepada DEBITUR dan yang terhutang berdasarkan Perjanjian Kredit ini.
						</li>
						<li>
							Dalam setiap tuntutan hukum atau perkara yang timbul dari atau diakibatkan oleh Perjanjian Kredit ini dokumen agunan, semua catatan yang dibuat menurut ayat 4 pasal ini harus merupakan bukti nyata dan sempurna dari jumlah-jumlah yang telah ditarik oleh DEBITUR dan setiap pembayaran yang telah diterima oleh BANK, serta suatu keterangan tertulis dari BANK sehubungan dengan jumlah terhutang harus merupakan bukti nyata dan sempurna perihal jumlah terhutang berdasarkan Perjanjian Kredit ini dan dokumen agunan.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 8
							<br/>
							KUASA-KUASA
						</b>
					</p>
					<p>
						BANK berhak dan dengan ini diberi kuasa oleh DEBITUR, kuasa mana merupakan bagian yang tidak terpisahkan dari Perjanjian Kredit ini, dan oleh karenanya kuasa ini tidak akan berakhir karena sebab-sebab yang ditentukan oleh pasal 1813 KUH Perdata, serta dengan telah ditandatanganinya Perjanjian Kredit ini tidak diperlukannya lagi adanya suatu surat kuasa khusus, untuk sewaktu-waktu tanpa persetujuan terlebih dahulu oleh DEBITUR untuk :
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Bertindak untuk dan atas nama DEBITUR melakukan pembayaran pada waktu yang dianggap baik oleh BANK terhadap sejumlah uang yang
							diperoleh DEBITUR dari kredit tersebut pada pasal 1 Perjanjian Kredit ini kepada 
							<?php echo $v_col_devname;?> (Developer) atau penjual rumah berikut tanah yang dibeli oleh DEBITUR tersebut pada 
							pasal 2 Perjanjian Kredit ini.
						</li>
						<li>
							Melaksanakan tindakan-tindakan yang dianggap perlu diantaranya menjual dimuka umum atau dibawah tangan barang-barang yang diserahkan sebagai agunan pada pasal 6 Perjanjian Kredit ini, apabila DEBITUR dari sebab apapun juga tidak memenuhi salah satu atau lebih kewajiban yang timbul dari Perjanjian Kredit ini. Hasil penjualan tersebut diperhitungkan pertama kali untuk  pelunasan kewajiban DEBITUR pada BANK, dan apabila ternyata hasil penjualan tersebut tidak mencukupi pelunasan kewajiban DEBITUR pada BANK, maka DEBITUR berkewajiban menyerahkan barang agunan lain kepada BANK sampai DEBITUR melunasi seluruh kewajibannya.
						</li>
						<li>
							Melakukan eksekusi melalui penjualan agunan di bawah tangan yang dilakukan tanpa melalui proses pelelangan umum dengan tujuan mempercepat penjualan objek Hak Tanggungan dan agar tercapai harga penjualan tertinggi apabila penjualan melalui pelelangan umum diperkirakan tidak akan menghasilkan harga tertinggi. Hal ini Debitur dengan serta merta membebankan kepada BANK sebagi pihak yang menerima kuasa untuk melakukan kuasa menjual penjualan di muka umum maupun di bawah tangan.
						</li>
						<li>
							Membebani rekening giro, tabungan  dan atau rekening pinjaman DEBITUR yang ada pada BANK untuk pembayaran hutang pokok, bunga kredit, bunga tunggakan, denda, premi asuransi, biaya pengikatan barang jaminan dan biaya lainnya yang timbul karena dan untuk pelaksanaan Perjanjian Kredit ini.
						</li>
						<li>
							Kuasa-kuasa yang diberikan DEBITUR kepada BANK tersebut ayat 1 dan 2 pasal ini diberikan dengan Hak Substitusi dan merupakan bagian yang tidak dapat dipisahkan dari Perjanjian Kredit ini sehingga tanpa adanya kuasa-kuasa tersebut Perjanjian Kredit ini tidak akan dibuat.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 9
							<br/>
							PENGAWASAN DAN PEMERIKSAAN BARANG JAMINAN
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Selama DEBITUR belum melunasi seluruh kreditnya yang timbul dari Perjanjian Kredit ini, maka BANK berhak setiap saat yang dianggap layak oleh BANK, melakukan pemeriksaan dan meminta keterangan-keterangan setempat yang diperlukan.
						</li>
						<li>
							DEBITUR menyetujui dan mewajibkan serta mengikatkan diri untuk memberikan keterangan-keterangan secara benar atas pertanyaan-pertanyaan pihak BANK dalam rangka pengawasan dan pemeriksaaan barang agunan ini.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 10
							<br/>
							HAK-HAK BANK UNTUK MENOLAK PENARIKAN KREDIT
							<br/>
							DAN MENGAKHIRI JANGKA WAKTU KREDIT SERTA PENAGIHAN SEKETIKA SELURUH KEWAJIBAN
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Tanpa memperhatikan ketentuan mengenai angsuran bulanan dan jangka waktu kredit ini, BANK berhak dan dapat untuk menolak/ menghentikan penarikan kredit dan atau untuk seketika menagih pelunasan sekaligus atas seluruh sisa kredit DEBITUR kepada BANK yang timbul dari Perjanjian Kredit ini, dan DEBITUR wajib membayarnya dengan seketika dan sekaligus lunas untuk seluruh sisa kredit yang ditagih oleh BANK, dalam hal terjadi salah satu atau beberapa keadaan di bawah ini, yaitu:
							
							<ol type="a">
								<li>
									DEBITUR menunggak pembayaran angsuran kredit sebanyak 3 kali angsuran baik secara berturut-turut maupun tidak berturut-turut.
								</li>
								<li>
									DEBITUR tidak mungkin lagi atau tidak mempunyai dasar hukum untuk memenuhi sesuatu ketentuan atau kewajiban berdasarkan Perjanjian Kredit ini antara lain: diberhentikan dari Kantor/ Instansi yang bersangkutan, dijatuhi hukuman Pidana, mendapat cacat badan sehingga oleh karenanya belum/ tidak dapat dipekerjakan lagi, dipindahkan ke kota/ daerah lain atau ke luar negeri.
								</li>
								<li>
									Perusahaan tempat DEBITUR bekerja atau DEBITUR telah dinyatakan pailit atau tidak mampu membayar atau telah dikeluarkan perintah oleh pejabat yang berwenang untuk menunjuk wali atau kuratornya.
								</li>
								<li>
									Usaha DEBITUR mengalami kepailitan.
								</li>
								<li>
									DEBITUR membuat atau menyebabkan atau menyetujui dilakukan atau membiarkan dilakukan suatu tindakan yang membahayakan atau dapat membahayakan, mengurangi atau meniadakan agunan yang diberikan untuk kredit.
								</li>
								<li>
									Harta-harta DEBITUR yang diberikan sebagai jaminan kredit telah musnah.
								</li>
								<li>
									Setiap keterangan yang diberikan, hal-hal yang disampaikan atau agunan yang dibuat oleh DEBITUR kepada BANK terbukti palsu atau menyesatkan dalam segala segi atau DEBITUR lalai atau gagal untuk memberikan keterangan yang benar atau sesungguhnya kepada BANK.
								</li>
								<li>
									DEBITUR gagal dalam memenuhi atau DEBITUR bertindak bertentangan dengan sesuatu peraturan Pemerintah atau Daerah, Undang-undang atau Peraturan-peraturan yang mempunyai akibat penting terhadap atau mempengaruhi hubungan kerjanya dengan Kantor tempat bekerja.
								</li>
								<li>
									Setiap sebab atau kejadian lainnya yang telah terjadi atau mungkin akan terjadi sehingga menjadi layak bagi BANK untuk melakukan penagihan seketika mengenai seluruh (sisa) kredit guna melindungi kepentingan-kepentingannya, satu dan lainnya semata-mata menurut penetapan/pertimbangan BANK.
								</li>
								<li>
									DEBITUR tidak atau belum mempergunakan / menarik kredit setelah lewat 1 (satu) bulan sejak ditandatanganinya Perjanjian Kredit ini dan untuk hal ini DEBITUR tidak berhak untuk meminta kembali atas provisi, biaya administrasi dan biaya lain yang telah disetor kepada BANK. 
								</li>
							</ol>
						</li>
						<li>
							DEBITUR mengikatkan diri kepada BANK dan menyatakan dengan sesungguhnya bahwa apabila DEBITUR oleh sebab apapun juga tidak dapat membayar angsuran kredit sesuai dengan jumlah dan jadwal angsuran sebagaimana dilampirkan pada dan merupakan satu kesatuan yang tidak dapat dipisahkan dari Perjanjian Kredit ini, sebanyak 3 (tiga) kali angsuran berturut turut maupun, maka BANK dapat dan berhak untuk melakukan penyitaan atas rumah  seperti yang tercantum dalam pasal 2 Perjanjian Kredit ini.
						</li>
						<li>
							Apabila setelah mendapat peringatan dari BANK, DEBITUR tidak dapat melunasi seluruh sisa kewajiban pembayarannya yang seketika ditagih oleh BANK karena terjadinya hal-hal yang disebutkan di dalam ayat 1 pasal ini, maka BANK berhak memerintahkan kepada DEBITUR untuk mengosongkan rumah  yang dibiayai dengan fasilitas kredit ini, DEBITUR mengikatkan diri untuk melaksanakan pengosongan  rumah  tersebut pasal 2 Perjanjian Kredit ini, selambat-lambatnya dalam jangka waktu 30 hari dihitung mulai tanggal perintah BANK untuk itu, tanpa syarat dan ganti rugi apapun juga.
						</li>
						<li>
							Apabila DEBITUR ternyata tidak mengosongkan rumah  tersebut ayat 3 pasal ini dalam jangka waktu yang ditentukan dalam ayat 2 pasal ini, maka BANK berhak untuk meminta bantuan pihak berwenang guna mengeluarkan DEBITUR dan mengosongkan rumah  tersebut.
						</li>
						<li>
							DEBITUR dengan ini menyatakan melepaskan haknya untuk meminta bantuan dari Instansi manapun mengenai pengosongan rumah  tersebut, apabila haknya untuk itu memang ada.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 11
							<br/>
							PELAKSANAAN EKSEKUSI BARANG AGUNAN
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Apabila berdasarkan pasal 10 Perjanjian Kredit ini, BANK menggunakan haknya untuk menagih pelunasan sekaligus atas utang DEBITUR, dan DEBITUR tidak dapat memenuhi kewajibannya membayar pelunasan tersebut walaupun telah mendapat peringatan-peringatan dari BANK, maka BANK berhak untuk setiap saat melaksanakan hak eksekusinya atas agunan yang dipegangnya, menurut cara dan dengan harga yang dianggap baik oleh BANK dalam batas-batas yang diberikan oleh undang-undang serta peraturan hukum lainnya.
						</li>
						<li>
							Hasil eksekusi dan/ atau penjualan barang agunan tersebut dalam ayai 1 pasal ini pertama-tama akan digunakan untuk melunasi sisa utang DEBITUR kepada BANK, termasuk semua biaya yang dikeluarkan BANK guna melaksanakan eksekusi barang jaminan, dan apabila masih ada sisanya jumlah sisa tersebut akan dibayarkan kembali kepada DEBITUR.
						</li>
						<li>
							Apabila dari hasil penjualan atau eksekusi barang jaminan kredit sebagaimana tersebut pada ayat 2 pasal ini, jumlahnya belum mencukupi untuk melunasi seluruh utang DEBITUR kepada BANK, maka sesuai dengan ketentuan peraturan yang berlaku BANK berhak untuk mengambil pelunasan atas sisa utang tersebut dari penjualan barang-barang lain milik DEBITUR, yang ditunjuk oleh DEBITUR sebagai agunan tambahan atas kredit ini.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 12
							<br/>
							ALAMAT PIHAK-PIHAK
						</b>
					</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							Seluruh pembayaran hutang atau setiap bagian dari hutang DEBITUR dan surat menyurat harus dilakukan pada Kantor BANK yang telah ditentukan pada jam-jam kerja dari Kantor yang bersangkutan.
						</li>
						<li>
							Semua surat menyurat dan pernyataan-pernyataan tertulis yang timbul dari dan berakar pada Perjanjian Kredit ini dianggap telah diserahkan dan diterima apabila dikirimkan kepada : 
							
							<ul>
								<li>
									Pihak BANK dengan alamat    : <?php echo $branch_name." ".$branch_address;?>
								</li>
								<li>
									Pihak DEBITUR dengan alamat : <?php echo $v_custaddr?>
								</li>
							</ul>
						</li>
						<li>
							Kedua belah pihak masing-masing akan memberitahukan secara tertulis pada kesempatan pertama secepatnya setiap terjadi perubahan alamat, DEBITUR pindah/tidak lagi menghuni rumah yang bersangkutan dan sebagainya.
						</li>
					</ol>

					<p style="text-align:center;">
						<b>
							PASAL 13
							<br/>
							DOMISILI
						</b>
					</p>
					<p>
						Tentang Perjanjian Kredit ini dan segala akibatnya serta pelaksanaannya kedua belah pihak memilih tempat kedudukan hukum 
						(domisili) yang tetap dan umum di Kantor Kepaniteraan Pengadilan Negeri 
						<? echo $p15_pengadilanengeri; ?>
						dan/atau Badan Urusan Piutang dan Lelang Negara (BUPLN) di 
						<? echo $p15_badanpiutang; ?>
						dengan tidak mengurangi hak dan wewenangnya BANK untuk memenuhi pelaksanaan/eksekusi atau mengajukan tuntutan hukum terhadap 
						DEBITUR berdasarkan Perjanjian Kredit ini melalui atau dihadapan Pengadilan-pengadilan lainnya di manapun juga di dalam 
						Wilayah Republik Indonesia.
					</p>

					<p style="text-align:center;">
						<b>
							PASAL 14
							<br/>
							PASAL TAMBAHAN
						</b>
					</p>
					<p>Atas pemberian kredit ini berlaku pula ketentuan sebagai berikut :</p>
					
					<ol style="margin-left:-25px;" type="1">
						<li>
							DEBITUR sebelum kredit ini lunas tidak diperkenankan tanpa persetujuan tertulis dari BANK untuk menerima fasilitas kredit dari BANK lain atau lembaga keuangan lain yang sumber pengembalian kreditnya berasal dari gaji/penghasilannya.
						</li>
						<li>
							Segala sesuatu yang belum cukup diatur dalam perjanjian kredit ini oleh BANK diatur dalam surat menyurat dan kertas-kertas lain merupakan bagian yang tidak dapat dipisahkan dari Perjanjian kredit ini.
						</li>
					</ol>

					<p>
						Demikian Perjanjian Kredit ini dibuat dan ditanda tangani oleh kedua belah pihak yang telah disesuaikan dengan ketentuan peraturan perundang-undangan termasuk ketentuan peraturan Otoritas Jasa Keuangan (OJK) yang dibuat dalam rangkap 2 (dua) masing-masing bermaterai cukup dan mempunyai kekuatan pembuktian yang sama.
					</p>

					<table style="width:100%;">
						<tr>
							<td style="width:350px;text-align:left;">
								PT Bank Pembangunan Daerah Sumatera Selatan 
								<br/>
								dan Bangka Belitung 
								<br/>
								Satuan Kredit Konsumen
							</td>
							<td style="text-align:center;">DEBITUR</td>
							<td style="width:200px;text-align:center;">
								MENYETUJUI
							</td>
						</tr>
						<tr>
						<td colspan="3" style="height:100px"></td>
						</tr>
						<tr>
						<td style="text-align:LEFT;">
							<span class="namapinca"><u><?php echo $namapemimpin_1;?></u></span><br/>
							<span class="Jabatan"><?php echo $jabatanpemimpin_i;?></span>
						</td>
						<td valign="top" style="text-align:center;"><u><?php echo $v_custfullname;?></u></td>
						<td style="text-align:center;">
							<u><?php echo $v_pasangan_namadepan.' '.$v_pasangan_namatengah.' '.$v_pasangan_namabelakang;  ?></u>
							</br>
							<span>Suami/Isteri</span>
						</td>
						</tr>
					</table>

				</td>
			</tr>
			<tr>
          <td colspan="3" style="text-align:center;">
            <?php
            if($userid != "" && $userwfid!="")
            {
            require ("../../requirepage/btnview.php");
            require ("../../requirepage/hiddenfield.php");
            }
            require("../../requirepage/btnprint.php");
            ?>

          </td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">
      <? require ("../../requirepage/hiddenfield.php"); ?>
		
    </form>
</body>
</html>
