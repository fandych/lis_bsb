<?php 
session_start();
ob_start();
require_once("sqlsrv.php");
$contenttextarea = "";
$_SESSION['template']="1";
if(isset($_POST['hidval']) && trim($_POST['hidval'])!="")
{
	$contenttextarea = str_replace('|||','"',$_POST['hidval']);
}
else
{
	$strsql="select * from tr_template where _idx='".$_SESSION['template']."'";
	//echo $strsql;
	$rs = $condb->_RQ($strsql);
	for( $i=0; $i<count($rs); $i++)
	{	
		$contenttextarea=$rs[$i]['_body'];
		//echo $tablename;
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Classic theme development file</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<script src="js/jquery-1.12.3.min.js"></script>
<script src="js/tinymce/tinymce.dev.js"></script>
<script src="js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="js/tinymce/plugins/spellchecker/plugin.dev.js"></script>
<script>
	tinymce.init({
		selector: "textarea#elm1",
		theme: "modern",
		plugins: [
			"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons template textcolor paste textcolor colorpicker codesample"
		],
		external_plugins: {
			//"moxiemanager": "/moxiemanager-php/plugin.js"
		},
		content_css: "css/development.css",
		add_unload_trigger: false,
		autosave_ask_before_unload: false,

		toolbar1: "styleselect formatselect fontselect fontsizeselect forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | ",
		toolbar2: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media help code | insertdatetime preview ",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | insertfile insertimage codesample",
		menubar: false,
		toolbar_items_size: 'small',

		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		templates: [
			//{title: 'My template 1', description: 'Some fancy template 1', content: 'My html'},
			//{title: 'My template 2', description: 'Some fancy template 2', url: 'development.html'}
		],

        spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}
	});
	
	function onsubmitformnext()
	{
		//document.getElementById("frm").submit();
		document.getElementById("hidval").value=tinymce.get('elm1').getContent();
		document.getElementById("frm").action = "step3.php";
		document.getElementById("frm").submit();
	}

	function onsubmitformprev()
	{
		//document.getElementById("frm").submit();
		document.getElementById("frm").action = "step1.php";
		document.getElementById("frm").submit();
	}
</script>
</head>
<body>
<form id="frm" name="frm" method="post" action="index2.php">
	<div>
		<textarea id="elm1" name="elm1" rows="15" cols="80" style="width: 80%">
			<?=$contenttextarea?>
		</textarea>
		<div style="text-align:right;margin: 10px 0;height:30px;">
			<div>
				<input onclick="onsubmitformnext()" style="float:right;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Next >>">
			</div>
			<div>
				<input onclick="onsubmitformprev()" style="float:left;border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="<< Prev">
			</div>
		</div>
		
		<input type="hidden" name="hidval" id="hidval">
	</div>
</form>

</body>
</html>
