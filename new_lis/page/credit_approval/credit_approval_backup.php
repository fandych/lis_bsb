<?
error_reporting("E_ALL || ~E_WARNING");
require ("../../lib/class.sqlserver.php");
require_once('../../requirepage/parameter.php');

$db = new SQLSRV();
$db->connect();

$buttonaction="ICA";
echo "<pre>";
//print_r($_REQUEST);
//print_r($_SESSION);
echo "</pre>";

$source = "SELECT * FROM tbl_FSTART WHERE txn_id = '$custnomid'";
$db->executeQuery($source);
$source = $db->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";

$branch = $source[0]['txn_branch_code'];

$branch_info = "SELECT * FROM tbl_branch WHERE branch_code = '$branch'";
$db->executeQuery($branch_info);
$branch_info = $db->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";



$analyst_data = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
$db->executeQuery($analyst_data);
$analyst_data = $db->lastResults;

foreach($analyst_data[0] as $key=>$value)
{
    $$key = $value;
}

$amount_analyst = $analyst_data[0]['plafond'];
$rate_analyst = $analyst_data[0]['interest'] / 100 / 12;
$term_analyst = $analyst_data[0]['tenor'];

$emi_analyst = $amount_analyst * $rate_analyst * (pow(1 + $rate_analyst, $term_analyst) / (pow(1 + $rate_analyst, $term_analyst) - 1));


$committe_data = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'RKOM'";
$db->executeQuery($committe_data);
$committe_data = $db->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";

if(count($committe_data) == 0) {
    //echo "no data";
    $check_custom = "";
    $check_maksimal = "";
    $check_request = "";
    $check_analyst = "";
    $check_recomend = "checked";
}
else {
    //echo "data found";
    foreach($committe_data[0] as $key=>$value)
    {
        $$key = $value;
    }

    $check_custom = "";
    $check_maksimal = "";
    $check_request = "";
    $check_analyst = "";
    $check_recomend = "";

    $amount_custom = 0;
    $term_custom = 0;
    $provisi_custom = 0;
    $rate_show = 0;
    $rate_custom = 0;

    switch ($cr_option) {
        case 'plafond_custom':
            $check_custom = "checked";
            $amount_custom = $plafond;
            $term_custom = $tenor;
            $provisi_custom = $provisi;
            $rate_show = $interest;
            $rate_custom = $rate_show / 100 / 12;


            break;
        case 'plafond_maksimal':
            $check_maksimal = "checked";
            break;
        case 'plafond_request':
            $check_request = "checked";
            break;
        case 'plafond_analyst':
            $check_analyst = "checked";
            break;
        case 'plafond_recomend':
            $check_recomend = "checked";
            break;

        default:
            $check_custom = "";
            $check_maksimal = "";
            $check_request = "";
            $check_analyst = "";
            $check_recomend = "";
            break;
    }
}


$loan_info = "SELECT * FROM tbl_CustomerFacility WHERE custnomid = '$custnomid'";
$db->executeQuery($loan_info);
$loan_info = $db->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";

$rate = ($loan_info[0]['sukubungayangdiberikan'] / 100 ) / 12;
$term = $loan_info[0]['custcreditlong'];

$amount_request = $loan_info[0]['custcreditplafond'];
$emi_request = $amount_request * $rate * (pow(1 + $rate, $term) / (pow(1 + $rate, $term) - 1));

echo "<pre>";
//print_r(number_format($emi));
echo "</pre>";

$customer_info = "SELECT * FROM tbl_CustomerMasterPerson2 WHERE custnomid = '$custnomid'";
$db->executeQuery($customer_info);
$customer_info = $db->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";


$group = $customer_info[0]['custtarid'];

$header = "select * from KEU_HEADER WHERE _group = '$group' order by _sequence";
$db->executeQuery($header);
$result_header = $db->lastResults;

//echo "<pre>";
//print_r($result_header);
//echo "</pre>";

$def_year = date('Y');
$def_month = date('m');

$lkcd_source = "LKCD";
$akre_source = "AKRE";

$emi_calculate = 0;

?>

<!DOCTYPE html>
<html>
   <head>
       <meta charset="utf-8">

       <title>Credit Approval</title>

       <script type="text/javascript" src="../../js/full_function.js" ></script>
       <script type="text/javascript" src="../../js/accounting_neraca.js" ></script>


       <style>

       *  {
           margin: 0px;
           padding: 0px;
           font-size: 12px;
       }


       body {
           font-family: "Trebuchet MS", Helvetica, sans-serif;
       }

       .workspace {
           margin-top: 10px;
           margin-bottom: 100px;
           margin-left: 25px;
           margin-right: 25px;
       }

       .table-data {
       }

       .information-area {
           width: 75%;
           margin-top: 25px;
           margin-bottom: 25px;
       }

       .display-area {
           display: inline-block;
       }

       .sign-area {
           display: inline-block;
       }

       .approve-area {
           display: inline-block;
       }

       .display-box {
           color: #666666;
           font-weight: lighter;
           float: left;
           margin-top: 1px;
           margin-left: 30px;
           margin-right: 30px;
           padding-top: 5px;
           padding-left: 20px;
           padding-right: 20px;
           padding-bottom: 10px;
           width: 170px;
           position: relative;
           border-radius: 5px 5px 5px 5px;
           -moz-border-radius: 5px 5px 5px 5px;
           -webkit-border-radius: 5px 5px 5px 5px;
           border: 0px solid #cccccc;
       }

       .sign-box {
           color: #666666;
           font-weight: lighter;
           float: left;
           margin-top: 5px;
           margin-left: 30px;
           margin-right: 30px;
           padding-top: 15px;
           padding-left: 20px;
           padding-right: 20px;
           padding-bottom: 10px;
           width: 170px;
           position: relative;
           border-radius: 5px 5px 5px 5px;
           -moz-border-radius: 5px 5px 5px 5px;
           -webkit-border-radius: 5px 5px 5px 5px;
           border: 0px solid #cccccc;
       }

       .credit-box {
           color: #666666;
           font-weight: lighter;
           float: left;
           margin-top: 5px;
           margin-left: 30px;
           margin-right: 30px;
           padding-top: 15px;
           padding-left: 20px;
           padding-right: 20px;
           position: relative;
           border-radius: 5px 5px 5px 5px;
           -moz-border-radius: 5px 5px 5px 5px;
           -webkit-border-radius: 5px 5px 5px 5px;
           border: 0px solid #cccccc;
       }

       .save-button {

           border-radius: 5px 5px 5px 5px;
           -moz-border-radius: 5px 5px 5px 5px;
           -webkit-border-radius: 5px 5px 5px 5px;

           padding-top: 10px;
           padding-bottom: 10px;
           padding-left: 50px;
           padding-right: 50px;
       }

       .neraca td {
         text-align:center;
       }


       .buttonsaveflow {
           background-color:blue;
           color:#FFF;
           cursor:pointer;
           text-decoration:none;
           cursor:pointer;
           width:300px;
           -moz-border-radius:4px;
           -webkit-border-radius:4px;
           -khtml-border-radius:4px;
           border-radius:4px;

       }

       .buttonneg{
           background-color:red;
           color:#FFF;
           cursor:pointer;
           text-decoration:none;
           cursor:pointer;
           -moz-border-radius:4px;
           -webkit-border-radius:4px;
           -khtml-border-radius:4px;
           border-radius:4px;
           width:50px;
       }
       </style>

   </head>
   <body>
       <div class="workspace" align="center">
           <h1 style="font-size:18px;">CREDIT APPROVAL</h1>
           <br>
           <hr>

           <div class="information-area" align="left">
               <font style="font-weight:bold">PERMOHONAN KREDIT</font>
               <hr>
               <br>

               <table class="table-data" cellspacing="0" cellpadding="5">
                   <tr>
                       <td width="30%" align="left">&nbsp;</td>
                       <td width="0%">&nbsp;</td>
                       <td width="10%">&nbsp;</td>
                       <td width="60%" align="left">&nbsp;</td>
                   </tr>
                   <tr>
                       <td align="left">Informasi Nasabah</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><a href="../preview/PreviewALL/PreviewALL_n.php?custnomid=<?=$custnomid;?>"><?=$custnomid;?></a></td>
                   </tr>
                   <tr>
                       <td align="left">Plafond yang diajukan nasabah</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($amount_request);?></td>
                   </tr>
                   <tr>
                       <td align="left">Tenor</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($term);?> Bulan</td>
                   </tr>
                   <tr>
                       <td align="left">Suku Bunga</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                   </tr>
                   <tr>
                       <td align="left">Angsuran</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($emi_request);?></td>
                   </tr>
               </table>

           </div>

           <div class="information-area" align="left">
               <font style="font-weight:bold">KALKULASI KEBUTUHAN KREDIT BERDASARKAN INFORMASI DEBITUR</font>
               <hr>
               <br>
               <div align="left">


               <?
               /////////////////////////////////////////NERACA////////////////////////////////////////////////////////////////////////////////
               ?>

               <?
               $def_source = "LKCD";
               $group = $customer_info[0]['custtarid'];

               $header = "SELECT * FROM KEU_HEADER WHERE _group = '$group'";
               $db->executeQuery($header);
               $result_header = $db->lastResults;

               $count_data = "SELECT DISTINCT _year, _month, _source FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _source = '$def_source'";
               //echo $count_data;
               $db->executeQuery($count_data);
               $result_count_data= $db->lastResults;

               echo "<pre>";
               //print_r($db->lastResults);
               echo "</pre>";
               ?>
               <table border="1" cellpadding="5" cellspacing="0">

               <tr>
                   <td width="300px" colspan="2">&nbsp;</td>
                   <?
                   for($z=0;$z<count($result_count_data);$z++)
                   {
                       $year = $result_count_data[$z]['_year'];
                       $month = $result_count_data[$z]['_month'];
                       $source = $def_source;

                       $monthNum  = $month;
                       $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                       $monthName = $dateObj->format('F');

                       ?>
                       <td>
                           <div align="center">
                               <font style="font-size:12px">
                                   <?=$year;?> / <?=$monthName;?>
                               </font>
                           </div>
                       </td>
                       <?
                   }
                   ?>
               </tr>

               <?
               for($x=0;$x<count($result_header);$x++)
               {
                   $header_code = $result_header[$x]['_code'];
                   $header_show = $result_header[$x]['_name'];

                   ?>
                   <tr>
                       <td colspan="<?=count($result_count_data)+2?>" style="text-align:left"><font style="font-weight:bold;font-size:16px"><?=strtoupper($header_show);?></font></td>
                   </tr>

                   <?

                   $detail = "select * from KEU_DETAIL where _code_header = '$header_code' order by _sequence";
                   $db->executeQuery($detail);
                   $result_detail = $db->lastResults;


                   for($y=0;$y<count($result_detail);$y++)
                   {
                       $detail_code = $result_detail[$y]['_code_detail'];
                       $detail_type = $result_detail[$y]['_type'];

                       if($detail_type == "VALUE")
                       {
                           //VALUE

                           $detail_show = $result_detail[$y]['_name'];
                           ?>
                           <tr>
                               <td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
                               <td>:</td>

                               <?
                               for($z=0;$z<count($result_count_data);$z++)
                               {
                                   $year = $result_count_data[$z]['_year'];
                                   $month = $result_count_data[$z]['_month'];
                                   $source = $def_source;

                                   $get_value = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$detail_code' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                   $db->executeQuery($get_value);
                                   $result_value = $db->lastResults;
                                   $value = $result_value[0]['_value'];

                                   ?>
                                   <td width="100px" style="text-align:right;font-size:11px;"><div align="right" sty><?=number_format($value,2);?></div></td>
                                   <?
                               }
                               ?>

                           </tr>
                           <?

                       }
                       else
                       {
                           //FORMULA
                           $detail_show = "<b>".$result_detail[$y]['_name']."</b>";

                           ?>
                           <tr>
                               <td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
                               <td>:</td>
                               <?
                               for($z=0;$z<count($result_count_data);$z++)
                               {
                                   $year = $result_count_data[$z]['_year'];
                                   $month = $result_count_data[$z]['_month'];
                                   $source = $def_source;

                                   $formula_sql = "select * from KEU_FORMULA where _code_detail = '$detail_code'";
                                   $db->executeQuery($formula_sql);
                                   $result_formula = $db->lastResults;

                                   $total_formula = 0;

                                   if(isset($result_formula[0]['_formula']))
                                   {
                                       $formula = $result_formula[0]['_formula'];

                                       $chars = str_split($formula);

                                       $t=0;
                                       //echo $formula."<br>";
                                       $formula = "";
                                       $save = "";

                                       $temp = "";

                                       foreach($chars as $char)
                                       {
                                           $t++;

                                           if($char == "+" || $char == "-" || $char == "*" ||  $char == "/")
                                           {
                                               if($temp == "(" || $temp == ")")
                                               {
                                                   $formula .= $char;
                                               }
                                               else
                                               {
                                                   $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                                   $db->executeQuery($check);
                                                   $result_check = $db->lastResults;
                                                   $another = $result_check[0]['_value'];

                                                   $formula .= $another . $char;
                                               }

                                               $save = "";
                                           }
                                           else if($char == "(")
                                           {
                                               $formula .= $char;

                                               $save = "";
                                               $temp = "(";

                                           }
                                           else if($char == ")")
                                           {
                                               $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                               $db->executeQuery($check);
                                               $result_check = $db->lastResults;

                                               if(count($result_check) > 0)
                                               {
                                                   $another = $result_check[0]['_value'];
                                               }
                                               else {
                                                   $another = "";
                                               }

                                               $formula .= $another . $char;

                                               $save = "";
                                               $temp = ")";
                                           }
                                           else
                                           {
                                               $save .= $char;

                                               if($t == count($chars))
                                               {
                                                   $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                                   $db->executeQuery($check);
                                                   $result_check = $db->lastResults;
                                                   $another = $result_check[0]['_value'];

                                                   $formula .= $another;
                                               }

                                               $temp = $save;
                                           }
                                       }

                                       //echo $formula."<br>";

                                       eval( '$total_formula = ('.$formula.');');
                                   }
                                   else
                                   {
                                       $formula = "";
                                   }

                                   if($detail_code == "KMGS")
                                   {
                                       $emi_debitur = $total_formula;
                                   }

                                   ?>
                                   <td style="text-align:right;font-weight:bold;font-size:12px;"><?=number_format($total_formula,2);?></td>
                                   <?
                               }
                               ?>
                           </tr>
                           <?

                       }


                   }

               }
               ?>
               <tr>
                   <td colspan="2">&nbsp;</td>
                   <?
                   for($z=0;$z<count($result_count_data);$z++)
                   {
                       $year = $result_count_data[$z]['_year'];
                       $month = $result_count_data[$z]['_month'];
                       $source = $def_source;
                       ?>
                       <td>&nbsp;</td>
                       <?
                   }
                   ?>
               </tr>
               </table>


               <?
               /////////////////////////////////////////END OF NERACA////////////////////////////////////////////////////////////////////////////////


               $amount_debitur = $emi_debitur / ($rate * (pow(1 + $rate, $term) / (pow(1 + $rate, $term) - 1)));

               //echo $amount;
               ?>




               </div>
           </div>

           <div class="information-area" align="left">
               <font style="font-weight:bold">PERHITUNGAN KREDIT BERDASARKAN INFORMASI DEBITUR</font>
               <hr>

               <table class="table-data" cellspacing="0" cellpadding="5">
                   <tr>
                       <td width="30%" align="left">&nbsp;</td>
                       <td width="0%">&nbsp;</td>
                       <td width="10%">&nbsp;</td>
                       <td width="60%" align="left">&nbsp;</td>
                   </tr>
                   <tr>
                       <td align="left">Plafond Maksimal</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($amount_debitur);?></td>
                   </tr>
                   <tr>
                       <td align="left">Tenor</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($term);?> Bulan</td>
                   </tr>
                   <tr>
                       <td align="left">Suku Bunga</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                   </tr>
                   <tr>
                       <td align="left">Angsuran</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($emi_debitur);?></td>
                   </tr>
               </table>

           </div>

           <div class="information-area" align="left">

               <font style="font-weight:bold">KALKULASI KEBUTUHAN KREDIT BERDASARKAN ANALYST</font>
               <hr>
               <br>

               <form method="post" action="">
                   <?
                   $count_data = "SELECT DISTINCT _year, _month, _source FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _source = '$akre_source'";
                   //echo $count_data;
                   $db->executeQuery($count_data);
                   $result_count_data= $db->lastResults;

                   echo "<pre>";
                   //print_r($db->lastResults);
                   echo "</pre>";
                   ?>
                   <table border="1" cellpadding="5" cellspacing="0">

                   <tr>
                       <td width="300px" colspan="2">&nbsp;</td>
                       <?
                       for($z=0;$z<count($result_count_data);$z++)
                       {
                           $year = $result_count_data[$z]['_year'];
                           $month = $result_count_data[$z]['_month'];
                           $source = $akre_source;

                           $monthNum  = $month;
                           $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                           $monthName = $dateObj->format('F');

                           ?>
                           <td>
                               <div align="center">
                                   <font style="font-size:12px">
                                       <?=$year;?> / <?=$monthName;?>
                                   </font>
                               </div>
                           </td>
                           <?
                       }
                       ?>

                   </tr>

                   <?
                   for($x=0;$x<count($result_header);$x++)
                   {
                       $header_code = $result_header[$x]['_code'];
                       $header_show = $result_header[$x]['_name'];

                       ?>
                       <tr>
                           <td colspan="<?=count($result_count_data)+2?>" style="text-align:left"><font style="font-weight:bold;font-size:16px"><?=strtoupper($header_show);?></font></td>
                       </tr>

                       <?

                       $detail = "select * from KEU_DETAIL where _code_header = '$header_code' order by _sequence";
                       $db->executeQuery($detail);
                       $result_detail = $db->lastResults;


                       for($y=0;$y<count($result_detail);$y++)
                       {
                           $detail_code = $result_detail[$y]['_code_detail'];
                           $detail_type = $result_detail[$y]['_type'];

                           if($detail_type == "VALUE")
                           {
                               //VALUE

                               $detail_show = $result_detail[$y]['_name'];
                               ?>
                               <tr>
                                   <td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
                                   <td>:</td>

                                   <?
                                   for($z=0;$z<count($result_count_data);$z++)
                                   {
                                       $year = $result_count_data[$z]['_year'];
                                       $month = $result_count_data[$z]['_month'];
                                       $source = $akre_source;

                                       $get_value = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$detail_code' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                       $db->executeQuery($get_value);
                                       $result_value = $db->lastResults;
                                       $value = $result_value[0]['_value'];

                                       ?>
                                       <td width="100px" style="text-align:right;font-size:11px;"><div align="right" sty><?=number_format($value,2);?></div></td>
                                       <?
                                   }
                                   ?>

                               </tr>
                               <?

                           }
                           else
                           {
                               //FORMULA
                               $detail_show = "<b>".$result_detail[$y]['_name']."</b>";

                               ?>
                               <tr>
                                   <td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
                                   <td>:</td>
                                   <?
                                   for($z=0;$z<count($result_count_data);$z++)
                                   {
                                       $year = $result_count_data[$z]['_year'];
                                       $month = $result_count_data[$z]['_month'];
                                       $source = $akre_source;

                                       $formula_sql = "select * from KEU_FORMULA where _code_detail = '$detail_code'";
                                       $db->executeQuery($formula_sql);
                                       $result_formula = $db->lastResults;

                                       $total_formula = 0;

                                       if(isset($result_formula[0]['_formula']))
                                       {
                                           $formula = $result_formula[0]['_formula'];

                                           $chars = str_split($formula);

                                           $t=0;
                                           //echo $formula."<br>";
                                           $formula = "";
                                           $save = "";

                                           $temp = "";

                                           foreach($chars as $char)
                                           {
                                               $t++;

                                               if($char == "+" || $char == "-" || $char == "*" ||  $char == "/")
                                               {
                                                   if($temp == "(" || $temp == ")")
                                                   {
                                                       $formula .= $char;
                                                   }
                                                   else
                                                   {
                                                       $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                                       $db->executeQuery($check);
                                                       $result_check = $db->lastResults;
                                                       $another = $result_check[0]['_value'];

                                                       $formula .= $another . $char;
                                                   }

                                                   $save = "";
                                               }
                                               else if($char == "(")
                                               {
                                                   $formula .= $char;

                                                   $save = "";
                                                   $temp = "(";

                                               }
                                               else if($char == ")")
                                               {
                                                   $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                                   $db->executeQuery($check);
                                                   $result_check = $db->lastResults;

                                                   if(count($result_check) > 0)
                                                   {
                                                       $another = $result_check[0]['_value'];
                                                   }
                                                   else {
                                                       $another = "";
                                                   }

                                                   $formula .= $another . $char;

                                                   $save = "";
                                                   $temp = ")";
                                               }
                                               else
                                               {
                                                   $save .= $char;

                                                   if($t == count($chars))
                                                   {
                                                       $check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                                       $db->executeQuery($check);
                                                       $result_check = $db->lastResults;
                                                       $another = $result_check[0]['_value'];

                                                       $formula .= $another;
                                                   }

                                                   $temp = $save;
                                               }
                                           }

                                           //echo $formula."<br>";

                                           eval( '$total_formula = ('.$formula.');');
                                       }
                                       else
                                       {
                                           $formula = "";
                                       }

                                       if($detail_code == "KMGS")
                                       {
                                           $emi_calculate = $total_formula;
                                       }

                                       ?>
                                       <td style="text-align:right;font-weight:bold;font-size:12px;"><?=number_format($total_formula,2);?></td>
                                       <?
                                   }
                                   ?>
                               </tr>
                               <?

                           }


                       }

                   }
                   ?>
                   <tr>
                       <td colspan="2">&nbsp;</td>
                       <?
                       for($z=0;$z<count($result_count_data);$z++)
                       {
                           $year = $result_count_data[$z]['_year'];
                           $month = $result_count_data[$z]['_month'];
                           $source = $akre_source;
                           ?>
                           <td align="center">&nbsp;</td>
                           <?
                       }
                       ?>
                   </tr>
                   </table>
                   <input type="hidden" name="input_source" value="<?=$akre_source;?>">
                   <input type="hidden" name="custnomid" value="<?=$custnomid;?>">
               </form>



               <?
               /////////////////////////////////////////END OF NERACA////////////////////////////////////////////////////////////////////////////////


               $amount_calculate = $emi_calculate / ($rate * (pow(1 + $rate, $term) / (pow(1 + $rate, $term) - 1)));

               //echo $amount;
               ?>

               <br/>

               <font style="color:#ff3600">
                   <?
                   if($emi_calculate < $emi_debitur)
                   {
                       ?>
                       <i>Hasil perhitungan Analyst <b>lebih kecil</b> dari hasil perhitungan Debitur</i>
                       <?
                   }
                   else if ($emi_calculate > $emi_debitur) {
                       ?>
                       <i>Hasil perhitungan Analsyt <b>lebih besar</b> dari hasil perhitungan Debitur</i>
                       <?
                   }
                   ?>
               </font>

           </div>

           <div class="information-area" align="left">
               <font style="font-weight:bold">PERHITUNGAN KREDIT BERDASARKAN ANALYST</font>
               <hr>

               <table class="table-data" cellspacing="0" cellpadding="5">
                   <tr>
                       <td width="30%" align="left">&nbsp;</td>
                       <td width="0%">&nbsp;</td>
                       <td width="10%">&nbsp;</td>
                       <td width="60%" align="left">&nbsp;</td>
                   </tr>
                   <tr>
                       <td align="left">Plafond Rekomendasi</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($amount_calculate);?></td>
                   </tr>
                   <tr>
                       <td align="left">Tenor</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($term);?> Bulan</td>
                   </tr>
                   <tr>
                       <td align="left">Suku Bunga</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                   </tr>
                   <tr>
                       <td align="left">Angsuran</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($emi_calculate);?></td>
                   </tr>
               </table>

           </div>


           <div class="information-area" align="left">
               <font style="font-weight:bold">KETERANGAN ANALYST</font>
               <hr>
               <br>
               <?=$description;?>
               <br/>
               <br/>
           </div>


           <div class="information-area" align="left">
               <font style="font-weight:bold">PENENTUAN KREDIT BERDASARKAN ANALYST</font>
               <hr>

               <table class="table-data" cellspacing="0" cellpadding="5">
                   <tr>
                       <td width="30%" align="left">&nbsp;</td>
                       <td width="0%">&nbsp;</td>
                       <td width="10%">&nbsp;</td>
                       <td width="60%" align="left">&nbsp;</td>
                   </tr>
                   <tr>
                       <td align="left">Plafond Rekomendasi</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($amount_analyst);?></td>
                   </tr>
                   <tr>
                       <td align="left">Tenor</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($term_analyst);?> Bulan</td>
                   </tr>
                   <tr>
                       <td align="left">Suku Bunga</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left"><?=number_format($rate_analyst * 12 * 100,2);?> %</td>
                   </tr>
                   <tr>
                       <td align="left">Angsuran</td>
                       <td>:</td>
                       <td>&nbsp;</td>
                       <td align="left">Rp <?=number_format($emi_analyst);?></td>
                   </tr>
               </table>

           </div>

           <div class="information-area" align="left">
               <font style="font-weight:bold">OPINI ANALYST</font>
               <hr>
               <br/>
               <?=$opinion;?>
               <br/>
               <br/>
           </div>


           <?
           ///////////////////////////////// CUSTOM KREDIT
           //$emi_request = $amount_request * $rate * (pow(1 + $rate, $term) / (pow(1 + $rate, $term) - 1));
           $emi_custom = $amount_custom * $rate_custom * (pow(1 + $rate_custom, $term_custom) / (pow(1 + $rate_custom, $term_custom) - 1));
           //echo $amount;
           ?>




           <form method="post" action="credit_approval_process.php">

           <div class="information-area" align="left">
               <font style="font-weight:bold">PENENTUAN KREDIT RAPAT KOMITE</font>
               <hr>

               <div class="display-area">

                   <div class="credit-box">
                       <table class="table-data" cellspacing="0" cellpadding="5" id="data_custom">
                           <tr>
                               <td width="30%" align="left">&nbsp;</td>
                               <td width="0%">&nbsp;</td>
                               <td width="10%">&nbsp;</td>
                               <td width="60%" align="left">&nbsp;</td>
                           </tr>
                           <tr>
                               <td colspan="4"><font style="font-weight:bold">Custom</font></td>
                           </tr>
                           <tr>
                               <td colspan="4">&nbsp;</td>
                           </tr>
                           <tr>
                               <td align="left">Plafond</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp&nbsp;
                                   <input type="text" style="text-align:right;width:120px;" id="amount_custom" name="amount_custom" onfocus="custom_selected();" onblur="currency(this.id);" value="<?=number_format($amount_custom);?>">
                                   &nbsp;
                                   <button style="width:40px;" name="action" type="submit" value="edit_plafond">save</button>
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Tenor</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <input type="text" style="text-align:right;width:120px;" id="tenor_custom" name="tenor_custom" onfocus="custom_selected();" value="<?=number_format($term_custom);?>">
                                   Bulan
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Suku Bunga</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <input type="text" style="text-align:right;width:120px;" id="rate_custom" name="rate_custom" onfocus="custom_selected();" onblur="currency(this.id);" value="<?=number_format($rate_show,2);?>">
                                   %
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Provisi</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   <input type="text" style="text-align:right;width:120px;" id="provisi_custom" name="provisi_custom" onfocus="custom_selected();" onblur="currency(this.id);" value="<?=number_format($provisi_custom,2);?>">
                                   %
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Angsuran</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($emi_custom);?>
                                   <input type="hidden" name="emi_custom" value="<?=$emi_custom;?>">
                               </td>
                           </tr>
                       </table>
                       <br>
                       <div style="padding-left:100px;">
                           <input id="plafond_custom" class="option-radio" type="radio" <?=$check_custom;?> name="keputusan_kredit" value="plafond_custom">
                       </div>

                   </div>

                   <div class="credit-box">
                       <table class="table-data" cellspacing="0" cellpadding="5" id="data_request">
                           <tr>
                               <td width="30%" align="left">&nbsp;</td>
                               <td width="0%">&nbsp;</td>
                               <td width="10%">&nbsp;</td>
                               <td width="60%" align="left">&nbsp;</td>
                           </tr>
                           <tr>
                               <td colspan="4"><font style="font-weight:bold">Permohonan</font></td>
                           </tr>
                           <tr>
                               <td colspan="4">&nbsp;</td>
                           </tr>
                           <tr>
                               <td align="left">Plafond</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($amount_request);?>
                                   <input type="hidden" name="amount_request" value="<?=$amount_request;?>">
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Tenor</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($term);?> Bulan</td>
                               <input type="hidden" name="tenor_request" value="<?=$term;?>">
                           </tr>
                           <tr>
                               <td align="left">Suku Bunga</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                               <input type="hidden" name="rate_request" value="<?=number_format($rate * 12 * 100,2);?>">
                           </tr>
                           <tr>
                               <td align="left">Angsuran</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($emi_request);?>
                                   <input type="hidden" name="emi_request" value="<?=$emi_request;?>">
                               </td>
                           </tr>
                       </table>
                       <br>
                       <div style="padding-left:100px;">
                           <input id="plafond_request" class="option-radio" type="radio" <?=$check_request;?> name="keputusan_kredit" value="plafond_request">
                       </div>
                   </div>

                   <div class="credit-box">
                       <table class="table-data" cellspacing="0" cellpadding="5" id="data_maksimal">
                           <tr>
                               <td width="30%" align="left">&nbsp;</td>
                               <td width="0%">&nbsp;</td>
                               <td width="10%">&nbsp;</td>
                               <td width="60%" align="left">&nbsp;</td>
                           </tr>
                           <tr>
                               <td colspan="4"><font style="font-weight:bold">Maksimal</font></td>
                           </tr>
                           <tr>
                               <td colspan="4">&nbsp;</td>
                           </tr>
                           <tr>
                               <td align="left">Plafond</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($amount_debitur);?>
                                   <input type="hidden" name="amount_maksimal" value="<?=$amount_debitur;?>">
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Tenor</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($term);?> Bulan</td>
                               <input type="hidden" name="tenor_maksimal" value="<?=$term;?>">
                           </tr>
                           <tr>
                               <td align="left">Suku Bunga</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                               <input type="hidden" name="rate_maksimal" value="<?=number_format($rate * 12 * 100,2);?>">
                           </tr>
                           <tr>
                               <td align="left">Angsuran</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($emi_debitur);?>
                                   <input type="hidden" name="emi_maksimal" value="<?=$emi_debitur;?>">
                               </td>
                           </tr>
                       </table>
                       <br>
                       <div style="padding-left:100px;">
                           <input id="plafond_maksimal" class="option-radio" type="radio" <?=$check_maksimal;?> name="keputusan_kredit" value="plafond_maksimal">
                       </div>
                   </div>

                   <div class="credit-box">
                       <table class="table-data" cellspacing="0" cellpadding="5" id="data_calculate">
                           <tr>
                               <td width="30%" align="left">&nbsp;</td>
                               <td width="0%">&nbsp;</td>
                               <td width="10%">&nbsp;</td>
                               <td width="60%" align="left">&nbsp;</td>
                           </tr>
                           <tr>
                               <td colspan="4"><font style="font-weight:bold">Perhitungan Analyst</font></td>
                           </tr>
                           <tr>
                               <td colspan="4">&nbsp;</td>
                           </tr>
                           <tr>
                               <td align="left">Plafond</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($amount_calculate);?>
                                   <input type="hidden" name="amount_calculate" value="<?=$amount_calculate;?>">
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Tenor</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($term);?> Bulan</td>
                               <input type="hidden" name="tenor_calculate" value="<?=$term;?>">
                           </tr>
                           <tr>
                               <td align="left">Suku Bunga</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($rate * 12 * 100,2);?> %</td>
                               <input type="hidden" name="rate_calculate" value="<?=number_format($rate * 12 * 100,2);?>">
                           </tr>
                           <tr>
                               <td align="left">Angsuran</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($emi_calculate);?>
                                   <input type="hidden" name="emi_calculate" value="<?=$emi_calculate;?>">
                               </td>
                           </tr>
                       </table>
                       <br>
                       <div style="padding-left:100px;">
                           <input id="plafond_calculate" class="option-radio" type="radio" <?=$check_calculate;?> name="keputusan_kredit" value="plafond_calculate">
                       </div>
                   </div>


                   <div class="credit-box">
                       <table class="table-data" cellspacing="0" cellpadding="5" id="data_recomend">
                           <tr>
                               <td width="30%" align="left">&nbsp;</td>
                               <td width="0%">&nbsp;</td>
                               <td width="10%">&nbsp;</td>
                               <td width="60%" align="left">&nbsp;</td>
                           </tr>
                           <tr>
                               <td colspan="4"><font style="font-weight:bold">Rekomendasi Analyst</font></td>
                           </tr>
                           <tr>
                               <td colspan="4">&nbsp;</td>
                           </tr>
                           <tr>
                               <td align="left">Plafond</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($amount_analyst);?>
                                   <input type="hidden" name="amount_recomend" value="<?=$amount_analyst;?>">
                               </td>
                           </tr>
                           <tr>
                               <td align="left">Tenor</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($term_analyst);?> Bulan</td>
                               <input type="hidden" name="tenor_recomend" value="<?=$term_analyst;?>">
                           </tr>
                           <tr>
                               <td align="left">Suku Bunga</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left"><?=number_format($rate_analyst * 12 * 100,2);?> %</td>
                               <input type="hidden" name="rate_recomend" value="<?=number_format($rate_analyst * 12 * 100,2);?>">
                           </tr>
                           <tr>
                               <td align="left">Angsuran</td>
                               <td>:</td>
                               <td>&nbsp;</td>
                               <td align="left">
                                   Rp <?=number_format($emi_analyst);?>
                                   <input type="hidden" name="emi_recomend" value="<?=$emi_analyst;?>">
                               </td>
                           </tr>
                       </table>
                       <br>
                       <div style="padding-left:100px;">
                           <input id="plafond_recomend" class="option-radio" type="radio" <?=$check_recomend;?> name="keputusan_kredit" value="plafond_recomend">
                       </div>
                   </div>

               </div>
           </div>

           <div class="information-area" align="left">


                   <input type="hidden" id="in_opinion" name="in_opinion" value="<?=$opinion;?>">
                   <input type="hidden" id="in_keterangan" name="in_keterangan" value="<?=$description;?>">
                   <div align="center">
                   <?
                   require_once("../../requirepage/hiddenfield.php");

                   if($userid != "")
                   {
                       echo "<br><button class=\"buttonsaveflow\" name=\"action\" value=\"submit\" style />SUBMIT</button>";
                       echo "<button class=\"buttonneg\" style=\"width:300px;\" name=\"action\" value=\"reject\" />REJECT</button>";
                       echo "<br><textarea id=\"recommendNotes\" name=\"recommendNotes\" cols=\"50\" row=\"5\" ></textarea>";
                       require ("../../requirepage/hiddenfield.php");
                   }
                   ?>
                   </div>
           </div>

           </form>

       </div>
       <script>
       function copy(source,target)
       {
           var val_source = document.getElementById(source).value;
           document.getElementById(target).value = val_source;
       }

       function custom_selected()
       {
           document.getElementById('plafond_custom').checked = true;
       }
       </script>
   </body>
</html>
