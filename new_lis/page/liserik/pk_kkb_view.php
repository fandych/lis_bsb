<?php
require('./sqlsrv.php');
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");



date_default_timezone_set('Asia/jakarta');

$arrayhari =array('Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
$tahun = date('Y');
$bulan = date('m');
$tanggal = date('d');
$hari = $arrayhari[date('w')];

$format='Y-m-d';

//hardcode
//echo $userbranch;
$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";


$attribute='';
$strsql='select * from (
        select _custnomid,_pekerjaan,b.attribute from tbl_KPR_infokerja a
        join param_pekerjaan b on a._pekerjaan = b.code
        	union
        select _custnomid,_pekerjaan,b.attribute from tbl_KGB_infokerja a
        join param_pekerjaan b on a._pekerjaan = b.code)
        tblxx where _custnomid =\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $attribute= $rs[$i]['attribute'];

}

$custcreditplafond='';
$custcreditlong='';
$strsql='SELECT custcreditplafond,custcreditlong  from tbl_CustomerFacility where custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custcreditplafond= $rs[$i]['custcreditplafond'];
  $custcreditlong= $rs[$i]['custcreditlong'];
}




$index_id = '';
$source = '';
$cr_option = '';
$fac_seq = '';
$plafond = '';
$tenor = '';
$installment = '';
$interest = '';
$provisi = '';
$opinion = '';
$description = '';
$strsql='SELECT *  FROM CR_ANALYST_APPROVAL WHERE custnomid=\''.$custnomid.'\'';
$custnpwpno='';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $index_id= $rs[$i]['index_id'];
  $custnomid= $rs[$i]['custnomid'];
  $source= $rs[$i]['source'];
  $cr_option= $rs[$i]['cr_option'];
  $fac_seq= $rs[$i]['fac_seq'];
  $plafond= $rs[$i]['plafond'];
  $tenor= $rs[$i]['tenor'];
  $installment= $rs[$i]['installment'];
  $interest= $rs[$i]['interest'];
  $provisi= $rs[$i]['provisi'];
  $opinion= $rs[$i]['opinion'];
  $description= $rs[$i]['description'];
}



$harga_jual = '';
$uang_muka = '';
$rencana_kredit = '';
$persentase_setoran_uang_muka = '';
$persentase_pembiayaan = '';
$max_pembiayaan = '';
$ket_identitas = '';
$ket_penghasilan = '';
$ket_kredit = '';
$kesimpulan = '';
$data_identitas = '';
$data_pekerjaan = '';
$data_penghasilan = '';
$data_syarat_kredit = '';
$ket_bunga = '';
$ket_angsuran = '';
$pengikatan_jaminan = '';
$biaya_adm = '';
$syarat_ttd_pk = '';
$syarat_realisasi = '';
$lain_lain = '';
$custnpwpno='';
$strsql='SELECT *  FROM CR_DATA WHERE custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custnomid= $rs[$i]['custnomid'];
  $harga_jual= $rs[$i]['harga_jual'];
  $uang_muka= $rs[$i]['uang_muka'];
  $rencana_kredit= $rs[$i]['rencana_kredit'];
  $persentase_setoran_uang_muka= $rs[$i]['persentase_setoran_uang_muka'];
  $persentase_pembiayaan= $rs[$i]['persentase_pembiayaan'];
  $max_pembiayaan= $rs[$i]['max_pembiayaan'];
  $ket_identitas= $rs[$i]['ket_identitas'];
  $ket_penghasilan= $rs[$i]['ket_penghasilan'];
  $ket_kredit= $rs[$i]['ket_kredit'];
  $kesimpulan= $rs[$i]['kesimpulan'];
  $data_identitas= $rs[$i]['data_identitas'];
  $data_pekerjaan= $rs[$i]['data_pekerjaan'];
  $data_penghasilan= $rs[$i]['data_penghasilan'];
  $data_syarat_kredit= $rs[$i]['data_syarat_kredit'];
  $ket_bunga= $rs[$i]['ket_bunga'];
  $ket_angsuran= $rs[$i]['ket_angsuran'];
  $pengikatan_jaminan= $rs[$i]['pengikatan_jaminan'];
  $biaya_adm= $rs[$i]['biaya_adm'];
  $syarat_ttd_pk= $rs[$i]['syarat_ttd_pk'];
  $syarat_realisasi= $rs[$i]['syarat_realisasi'];
  $lain_lain= $rs[$i]['lain_lain'];
}


$custktpno='';
$custaddr='';
$custfullname='';
$strsql='SELECT custktpno,custaddr,custfullname  from Tbl_CustomerMasterPerson where custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custktpno= $rs[$i]['custktpno'];
  $custaddr= $rs[$i]['custaddr'];
  $custfullname= $rs[$i]['custfullname'];
}

$_pasangan_nomorktp='';
$_pasangan_namadepan='';
$_pasangan_namatengah='';
$_pasangan_namabelakang='';
$strsql = 'SELECT _pasangan_nomorktp,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang from pl_custpasangan where _custnomid=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_pasangan_nomorktp= $rs[$i]['_pasangan_nomorktp'];
  $_pasangan_namadepan= $rs[$i]['_pasangan_namadepan'];
  $_pasangan_namatengah= $rs[$i]['_pasangan_namatengah'];
  $_pasangan_namabelakang= $rs[$i]['_pasangan_namabelakang'];
}



$branch_name='';
$branch_region_code='';
$branch_address='';
$branch_city='';
$strsql='SELECT branch_name,branch_region_code,branch_address,branch_city from Tbl_Branch where branch_code=\''.$userbranch.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $branch_name= $rs[$i]['branch_name'];
  $branch_region_code= $rs[$i]['branch_region_code'];
  $branch_address= $rs[$i]['branch_address'];
  $branch_city= $rs[$i]['branch_city'];
}


$cust_jeniscol='';
$col_id='';
$strsql='SELECT cust_jeniscol,col_id FROM Tbl_Cust_MasterCol WHERE ap_lisregno=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $cust_jeniscol= $rs[$i]['cust_jeniscol'];
  $col_id= $rs[$i]['col_id'];
}





$_cond_code = '';
$_type = '';
$_merk = '';
$_model = '';
$_jns_kendaraan = '';
$_thnpembuatan = '';
$_silinder_isi = '';
$_silinder_wrn = '';
$_norangka = '';
$_nomesin = '';
$_bpkb_tgl = '';
$_stnk_exp = '';
$_faktur_tgl = '';
$_bahanbakar = '';
$_bpkb_nama = '';
$_bpkb_addr1 = '';
$_bpkb_addr2 = '';
$_bpkb_addr3 = '';
$_perlengkapan = '';
$_notes = '';
$_opini = '';
$_officer_code = '';
$_tanggal_kunjungan = '';
$strsql='SELECT * FROM appraisal_vhc WHERE _collateral_id=\''.$col_id.'\'';
$rs = $apr->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $_cond_code= $rs[$i]['_cond_code'];
  $_type= $rs[$i]['_type'];
  $_merk= $rs[$i]['_merk'];
  $_model= $rs[$i]['_model'];
  $_jns_kendaraan= $rs[$i]['_jns_kendaraan'];
  $_thnpembuatan= $rs[$i]['_thnpembuatan'];
  $_silinder_isi= $rs[$i]['_silinder_isi'];
  $_silinder_wrn= $rs[$i]['_silinder_wrn'];
  $_norangka= $rs[$i]['_norangka'];
  $_nomesin= $rs[$i]['_nomesin'];
  $_bpkb_tgl= $rs[$i]['_bpkb_tgl'];
  $_stnk_exp= $rs[$i]['_stnk_exp'];
  $_faktur_tgl= $rs[$i]['_faktur_tgl'];
  $_bahanbakar= $rs[$i]['_bahanbakar'];
  $_bpkb_nama= $rs[$i]['_bpkb_nama'];
  $_bpkb_addr1= $rs[$i]['_bpkb_addr1'];
  $_bpkb_addr2= $rs[$i]['_bpkb_addr2'];
  $_bpkb_addr3= $rs[$i]['_bpkb_addr3'];
  $_perlengkapan= $rs[$i]['_perlengkapan'];
  $_notes= $rs[$i]['_notes'];
  $_opini= $rs[$i]['_opini'];
  $_officer_code= $rs[$i]['_officer_code'];
  $_tanggal_kunjungan= $rs[$i]['_tanggal_kunjungan'];
}


$ap_lisregno = '';

$col_addr1 = '';
$col_addr2 = '';
$col_addr3 = '';
$col_kodepos = '';
$col_nopol = '';
$col_stnk_no = '';
$col_stnkexp = '';
$col_fakturno = '';
$col_fakturtgl = '';
$col_bpkbno = '';
$col_bpkbtgl = '';
$col_condition = '';
$col_type = '';
$col_model = '';
$col_merk = '';
$col_tahunpembuatan = '';
$col_jeniskendaran = '';
$col_silinder = '';
$col_warna = '';
$col_norangka = '';
$col_nomesin = '';
$col_bpkbnama = '';
$col_bpkbaddr1 = '';
$col_bpkbaddr2 = '';
$col_bpkbaddr3 = '';
$col_perlengkapan = '';
$col_desc = '';
$col_nilaiwajar = '';
$col_nilailikuidasi = '';
$col_safemargin = '';
$col_appraiser = '';
$col_appraisdate = '';
$col_mk_code = '';
$nilai_likuidasi2 = '';
$strsql='SELECT * from tbl_COL_Vehicle where col_id=\''.$col_id.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

$ap_lisregno= $rs[$i]['ap_lisregno'];
$col_id= $rs[$i]['col_id'];
$col_addr1= $rs[$i]['col_addr1'];
$col_addr2= $rs[$i]['col_addr2'];
$col_addr3= $rs[$i]['col_addr3'];
$col_kodepos= $rs[$i]['col_kodepos'];
$col_nopol= $rs[$i]['col_nopol'];
$col_stnk_no= $rs[$i]['col_stnk_no'];
$col_stnkexp= $rs[$i]['col_stnkexp'];
$col_fakturno= $rs[$i]['col_fakturno'];
$col_fakturtgl= $rs[$i]['col_fakturtgl'];
$col_bpkbno= $rs[$i]['col_bpkbno'];
$col_bpkbtgl= $rs[$i]['col_bpkbtgl'];
$col_condition= $rs[$i]['col_condition'];
$col_type= $rs[$i]['col_type'];
$col_model= $rs[$i]['col_model'];
$col_merk= $rs[$i]['col_merk'];
$col_tahunpembuatan= $rs[$i]['col_tahunpembuatan'];
$col_jeniskendaran= $rs[$i]['col_jeniskendaran'];
$col_silinder= $rs[$i]['col_silinder'];
$col_warna= $rs[$i]['col_warna'];
$col_norangka= $rs[$i]['col_norangka'];
$col_nomesin= $rs[$i]['col_nomesin'];
$col_bpkbnama= $rs[$i]['col_bpkbnama'];
$col_bpkbaddr1= $rs[$i]['col_bpkbaddr1'];
$col_bpkbaddr2= $rs[$i]['col_bpkbaddr2'];
$col_bpkbaddr3= $rs[$i]['col_bpkbaddr3'];
$col_perlengkapan= $rs[$i]['col_perlengkapan'];
$col_desc= $rs[$i]['col_desc'];
$col_nilaiwajar= $rs[$i]['col_nilaiwajar'];
$col_nilailikuidasi= $rs[$i]['col_nilailikuidasi'];
$col_safemargin= $rs[$i]['col_safemargin'];
$col_appraiser= $rs[$i]['col_appraiser'];
$col_appraisdate= $rs[$i]['col_appraisdate'];
$col_mk_code= $rs[$i]['col_MK_CODE'];
$nilai_likuidasi2= $rs[$i]['nilai_likuidasi2'];
}
//select * from tbl_COL_Vehicle where col_id



$nomorpk = '';
$tanggalpk = '';
$namapemimpin_1 = '';
$jabatanpimpinan = '';
$akta_1 = '';
$tanggalakta_1 = '';
$p2_penjual = '';
$p3_tanggal = date('Y-m-d');
$p3_tanggalselambatnya = date('Y-m-d');
$p3_3keterlambatanangsuran = '';
$p4_tanggalkreditmulai = date('Y-m-d');
$p4_tanggalpertamakredit = date('Y-m-d');
$p4_pemotongangaji = '';

$strsql='SELECT * from PK_KKB where CUSTNOMID=\''.$custnomid.'\'';
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
	$custnomid= $rs[$i]['CUSTNOMID'];
	$nomorpk= $rs[$i]['NOMORPK'];
	$tanggalpk= $rs[$i]['TANGGALPK']->format('Y-m-d');;
	$namapemimpin_1= $rs[$i]['NAMAPEMIMPIN_1'];
	$jabatanpimpinan= $rs[$i]['JABATANPIMPINAN'];
	$akta_1= $rs[$i]['AKTA_1'];
	$tanggalakta_1= $rs[$i]['TANGGALAKTA_1']->format('Y-m-d');;
	$p2_penjual= $rs[$i]['P2_PENJUAL'];
	$p3_tanggal= $rs[$i]['P3_TANGGAL']->format('Y-m-d');;
	$p3_tanggalselambatnya= $rs[$i]['P3_TANGGALSELAMBATNYA']->format('Y-m-d');;
	$p3_3keterlambatanangsuran= $rs[$i]['P3_3KETERLAMBATANANGSURAN'];
	$p4_tanggalkreditmulai= $rs[$i]['P4_TANGGALKREDITMULAI']->format('Y-m-d');;
	$p4_tanggalpertamakredit= $rs[$i]['P4_TANGGALPERTAMAKREDIT']->format('Y-m-d');;
	$p4_pemotongangaji= $rs[$i]['P4_PEMOTONGANGAJI'];
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Perjanjian Kredit</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
    <script type="text/javascript" src="../../js/full_function.js" ></script>
    <script type="text/javascript" src="../../js/accounting.js" ></script>

    <script type="text/javascript">
    function namapt(thisid)
    {
      $('.namapt').html($('#'+thisid).val());
    }

    function lokasi(thisid)
    {
      $('.lokasi').html($('#'+thisid).val());
    }


    </script>

    <style media="screen">
      ol li {padding: 5px 0px;}
      .pihak {text-align:center;font-weight: bold;}
      #tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}
    </style>
  </head>
  <body>


    <form id="frm" action="pk_kkb_exec.php" method="post">
    <?
      if($userid != "" && $userwfid!="")
      {
    ?>
      <div style="float: right;margin-right: 220px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
    <?
      }
    ?>
      <br />
      <br />
      <table id="tableform" border="0" align="center">
        <tr>
          <td style="text-align:justify">
            <p style="text-align:center;">
              <b>
                PERJANJIAN KREDIT<br/>
              </b>
            </p>
            <p style="text-align:center;">



              Nomor : <?
			  $s = $tanggalpk.' 11:16:12 AM';
              $dt = new DateTime($s);

              $tanggal = $dt->format('d');
              $bulan = $dt->format('m');
              $tahun = $dt->format('Y');
              $time = $dt->format('H:i:s');
			  
			  echo $nomorpk?>

            </p>
            <p>Pada hari ini tanggal tanggal <?php echo $tanggal; ?> bulan <?php echo $bulan; ?> tahun <?php echo $tahun.' ('.$tanggalpk.')'; ?> yang  bertanda tangan dibawah ini :</p>

            <ol style="margin-left:-25px;" type="I">
              <li>
                  <? echo $namapemimpin_1; ?>
                  , <? echo $jabatanpimpinan; ?> Cabang, PT.  Bank  Pembangunan  Daerah  Sumatera  Selatan  dan   Bangka   Belitung,  berkedudukan   di
                  <?php echo $branch_city;?> dan beralamat di jalan
                  <?php echo $branch_address;?> dalam hal ini sah bertindak
                  dalam  jabatannya  tersebut  berdasarkan  Akta  Kuasa Direksi Nomor.
                  <? echo $akta_1; ?>
                  tanggal
                    <? echo $tanggalakta_1; ?>

                  dari  dan  selaku  demikian  sah  bertindak  untuk  dan  atas nama  PT. Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung  yang berkedudukan dan berkantor pusat di Palembang Jalan Gubernur H. Ahmad Bastari No. 07 Jakabaring, yang didirikan berdasarkan Akta Pendirian No. 20 tanggal 25 November 2000 yang dibuat oleh Dr. Justin Aritonang, Sarjana Hukum, Notaris  di Palembang  dan diumumkan dalam Berita  Negara Republik Indonesia  Nomor 938  tahun 2001,  Tambahan  Berita  Negara RI  tanggal  9 Februari 2001 Nomor 12, berikut dengan perubahan-perubahannya terakhir berdasarkan Akta Nomor 29  tanggal  17 Januari 2017 yang dibuat dihadapan Ny. Elmadiantini, SH.SpN  Notaris  di Palembang  dan telah mendapatkan pengesahan dari  Menteri Hukum  dan  Hak Asasi Manusia  RI No. AHU-AH.01.03-0035434 tahun 2017 tanggal 26 Januari 2017, selanjutnya disebut :
                  <p class="pihak">BANK</p>
              </li>
              <li>
                  <table style="margin-top:-20px;">
                    <tr>
                      <td style="width:200px;">N a m a</td>
                      <td>: </td>
                      <td><?php echo $custfullname;?></td>
                    </tr>
                    <tr>
                      <td>A l a m a t</td>
                      <td>: </td>
                      <td><?php echo $custaddr;?></td>
                    </tr>
                    <tr>
                      <td>Pekerjaan</td>
                      <td>: </td>
                      <td><?php echo $attribute;?></td>
                    </tr>
                    <tr>
                      <td>Kartu Tanda Penduduk</td>
                      <td>: </td>
                      <td><?php echo $custktpno;?></td>
                    </tr>
                  </table>
                <p>yang dalam hal ini bertindak untuk dan atas nama dirinya sendiri dan untuk melakukan tindakan hukum dalam Perjanjian Kredit ini dengan
                  memperoleh persetujuan dari istrinya / suaminya * yaitu <?php echo $_pasangan_namadepan.' '.$_pasangan_namatengah.' '.$_pasangan_namabelakang;  ?>
                  pekerjaan bertempat tinggal sama dengan istrinya / suaminya * pemegang
                  KTP Nomor <?php echo $_pasangan_nomorktp?> yang turut hadir dan menandatangani Perjanjian Kredit ini, untuk selanjutnya disebut:
                </p>
                <p class="pihak">DEBITUR</p>
                <p>BANK dan DEBITUR telah saling setuju dan sepakat untuk dan dengan ini membuat serta menetapkan Perjanjian Kredit ini berikut semua lampiran, perubahan dan atau penambahan dan atau pembaharuannya kemudian, apabila ada, untuk selanjutnya disebut Perjanjian Kredit, untuk dipatuhi dan dilaksanakan oleh para pihak tersebut, dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut : </p>
              </li>
            </ol>



            <p style="text-align:center;"><b>
                  PASAL 1<br/>
                  MAKSIMUM KREDIT
            </b></p>
            <p>BANK memberikan fasilitas kredit Kendaraan Bermotor kepada DEBITUR dan DEBITUR menyatakan mengaku dan menerima fasilitas kredit tersebut dari BANK dengan maksimum kredit sebesar
              Rp <?php echo number_format($plafond);?>.</p>


            <p style="text-align:center;"><b>
                  PASAL 2<br/>
                  TUJUAN PENGGUNAAN KREDIT
            </b></p>
            <p>Fasilitas  kredit yang diberikan  BANK kepada  DEBITUR sebagaimana tersebut PASAL 1 Perjanjian Kredit ini digunakan
              DEBITUR untuk membiayai pembelian 1 (1)  unit Kendaraan Bermotor Roda Empat berikut tanahnya oleh DEBITUR dengan data sebagai berikut :-
            </p>

            <?
            echo '
            <table style="page-break-after: always;">
              <tr>
                <td coslpan="3" valign="top">KENDARAAN</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">Merek</td>
                <td valign="top">:</td>
                <td valign="top">'.$_merk.'</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">Tahun Pembuatan</td>
                <td valign="top">:</td>
                <td valign="top">'.$_model.'</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">No Rangka</td>
                <td valign="top">:</td>
                <td valign="top">'.$_norangka.'</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">No Mesin</td>
                <td valign="top">:</td>
                <td valign="top">'.$_nomesin.'</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">Warna </td>
                <td valign="top">:</td>
                <td valign="top">'.$_silinder_wrn.'</td>
              </tr>
              <tr>
                <td valign="top" style="width:300px;">Penjual</td>
                <td valign="top">:</td>
                <td valign="top">'.$p2_penjual.'</td>
              </tr>
            </table>';
            ?>


            <p style="text-align:center;"><b>
                  PASAL 3<br/>
                  KETENTUAN KREDIT
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>Jangka waktu kredit selama <?php echo $custcreditlong;?> bulan terhitung sejak ditandatanganinya
                Perjanjian Kredit ini sampai dengan tanggal
                <? echo $p3_tanggal; ?>
                sehingga DEBITUR wajib melunasi seluruh hutang pokok, bunga,
                denda kredit dan biaya-biaya lainnya kepada BANK selambat-lambatnya pada tanggal <? echo $p3_tanggalselambatnya; ?>
              </li>
              <li>Sehubungan dengan pemberian fasilitas kredit tersebut DEBITUR dengan ini berjanji dan mengikatkan diri untuk membayar kepada BANK:
                <ol type="a">
                  <li>Bunga sebesar <?echo $ket_bunga?>  per tahun, sehingga angsuran pokok dan bunga setiap bulan sebesar
                    Rp. <? $persenbunga = is_numeric(str_replace('.','',$ket_bunga)) ? $ket_bunga : 1;
                          $nominalpersen = $persenbunga*$plafond/100;
                          echo number_format($nominalpersen);
                    ?></li>
                  <li>Provisi untuk selama jangka waktu kredit sebesar <?echo $provisi?> %
                    dari maksimum kredit sebagaimana yang dimaksud dalam PASAL 1 Perjanjian Kredit ini dan
                    dibayar DEBITUR pada saat Perjanjian Kredit ini ditandatangani.
                  </li>
                  <li>Biaya Administrasi sebesar Rp. <?echo number_format($biaya_adm);?> yang dibayar pada saat Perjanjian Kredit ini ditandatangani. </li>
                </ol>
              </li>
              <li>Apabila DEBITUR tidak membayar angsuran kredit sesuai dengan tanggal jatuh tempo angsuran/yang telah ditetapkan,
                maka DEBITUR wajib membayar denda keterlambatan angsuran sebesar
                <? echo $p3_3keterlambatanangsuran; ?>
                 % per bulan
                dari jumlah angsuran (pokok dan atau bunga)  yang telambat dibayar tersebut.
              </li>
              <li>
                Apabila DEBITUR melunasi kredit lebih awal dari jangka waktu yang telah diperjanjikan sebagaimana tersebut ayat 1 pasal ini ,
                 maka DEBITUR akan dikenakan biaya pelunasan sesuai dengan ketentuan yang berlaku pada Bank.
              </li>
              <li>BANK berhak sewaktu-waktu untuk merubah  tingkat suku bunga kredit pada ayat 2 (a) PASAL ini,
                 maupun yang ditetapkan BANK dikemudian hari sesuai dengan keadaan tanpa perlu pemberitahuan terlebih dahulu
                 dan tanpa perlu mendapatkan persetujuan dari DEBITUR
              </li>
            </ol>
            <p style="text-align:center;"><b>
                  PASAL 4<br/>
                  CARA PEMBAYARAN ANGSURAN KREDIT
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>DEBITUR  diwajibkan  melakukan  pembayaran  dalam  bentuk  angsuran  kredit setiap bulan
                sebanyak <?php echo $tenor; ?> kali angsuran atau selama
                <?php echo $tenor; ?> bulan yang harus dibayar
                setiap tanggal <? echo substr($p4_tanggalkreditmulai,-2); ?> setiap bulan dan untuk angsuran pertama
                dimulai pada tanggal
                <? echo $p4_tanggalpertamakredit; ?>
                 sebagaimana jadwal angsuran terlampir berikut perubahan-perubahannya
                yang merupakan satu kesatuan serta bagian yang tidak terpisahkan dari Perjanjian Kredit ini.
              </li>
              <li>Jumlah angsuran  yang wajib dibayar oleh DEBITUR adalah pokok
                beserta bunga yang dibayar secara bersama-sama sebesar
                sebesar Rp. <?php echo number_format($installment); ?> selama jangka waktu kredit
              </li>
              <li>Untuk pelaksanaan pembayaran angsuran kredit pada ayat 1 pasal ini dilakukan dengan cara memotong
                langsung Gaji/Penghasilan DEBITUR melalui Bendaharawan Gaji pada Instansi/Dinas  setiap bulan sesuai dengan Surat Kuasa
                untuk memotong Gaji dari DEBITUR kepada Bendaharawan, atau dengan cara lain yang
                telah diperjanjikan dan disepakati oleh BANK dan DEBITUR (Untuk DEBITUR berpenghasilan tetap).</li>
              <li>
                Pemotongan gaji DEBITUR sebagai angsuran kredit akan dilakukan oleh BANK pada tanggal
                pembayaran gaji/ penghasilan DEBITUR setiap bulan dan untuk  yang  pertama dimulai
                pada tanggal <? echo $p4_tanggalpertamakredit; ?>
              </li>
              <li>
                Apabila karena sesuatu hal yang menyebabkan penghentian  pembayaran  gaji/ penghasilan  DEBITUR, maka terhadap sisa kredit/ seluruh  kewajiban  DEBITUR ( termasuk bunga, denda, dan biaya lainnya) pada BANK, wajib dilunasi oleh ahli waris DEBITUR
              </li>
            </ol>



            <p style="text-align:center;"><b>
                  PASAL 5<br/>
                  ASURANSI DAN BEBAN BIAYA
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>DEBITUR menyetujui dan mewajibkan serta mengikatkan diri, dengan ditandatanganinya Perjanjian Kredit ini sekaligus memberikan kuasa kepada BANK yang tidak dapat ditarik kembali, untuk dan atas nama DEBITUR, BANK menutup asuransi jiwa DEBITUR dan asuransi barang/ jaminan DEBITUR pada  perusahaan  asuransi yang telah ditetapkan oleh BANK, dengan ketentuan jenis resiko, nilai pertanggungan, jangka waktu pertanggungan yang ditentukan oleh BANK dan didalam perjanjian  asuransi/ polis ditetapkan klausul sedemikian rupa untuk kepentingan BANK,sehingga jika  terjadi  pembayaran ganti rugi/ klaim, BANK berhak untuk memperhitungkan hasil pembayaran klaim tersebut untuk pelunasan seluruh kewajiban DEBITUR pada BANK. </li>
              <li>Premi asuransi tersebut pada ayat 1  pasal  ini  harus  telah  dibayar  lunas oleh DEBITUR atau dipotong dari jumlah kredit yang akan diterima DEBITUR berdasarkan Perjanjian Kredit ini. </li>
            </ol>


            <p style="text-align:center;"><b>
                  PASAL 6<br/>
                  JAMINAN KREDIT
            </b></p>
            <ol style="margin-left:-25px;page-break-after: always;" type="1">
              <li>Segala harta kekayaan DEBITUR, baik yang bergerak maupun yang tidak bergerak, baik yang sudah ada maupun yang akan ada di kemudian hari , merupakan jaminan bagi pelunasan jumlah kredit DEBITUR yang timbul karena Perjanjian Kredit ini.</li>
              <li>Guna lebih menjamin pembayaran kembali angsuran kredit dengan tertib dan sebagaimana mestinya oleh DEBITUR yang karena sebab apapun juga terhutang dan wajib dibayar oleh DEBITUR kepada BANK berdasarkan Perjanjian Kredit ini, maka DEBITUR menyerahkan jaminan berupa :                 <br/>
                <?
				echo '1 (satu) Unit Kendaraan dengan merek '.$_merk.' tipe '.$_type.' warna '.$col_warna.' dengan no rangka '.$_norangka.'
						dan no mesin '.$_nomesin.' No. faktur '.$col_fakturno.' tanggal faktur '.$_faktur_tgl;
						/*
                echo '
                <table>
                  <tr>
                    <td coslpan="3" valign="top">KENDARAAN</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">Merek</td>
                    <td valign="top">:</td>
                    <td valign="top">'.$_merk.'</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">Tahun Pembuatan</td>
                    <td valign="top">:</td>
                    <td valign="top">'.$_model.'</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">No Rangka</td>
                    <td valign="top">:</td>
                    <td valign="top">'.$_norangka.'</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">No Mesin</td>
                    <td valign="top">:</td>
                    <td valign="top">'.$_nomesin.'</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">Warna </td>
                    <td valign="top">:</td>
                    <td valign="top">'.$col_warna.'</td>
                  </tr>
                  <tr>
                    <td valign="top" style="width:300px;">Penjual</td>
                    <td valign="top">:</td>
                    <td valign="top">&nbsp;</td>
                  </tr>
                </table>';
				*/
                ?>
              </li>
            </ol>
            <p style="text-align:center;"><b>
                  PASAL 7<br/>
                  PENYELENGGARAAN REKENING PINJAMAN
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>
                Sebagai pelaksanaan Perjanjian Kredit ini, BANK membuka rekening tersendiri atas nama DEBITUR yang dinamakan rekening pinjaman dan rekening atau catatan-catatan lainnya sesuai dengan cara dan ketentuan yang berlaku pada BANK dan merupakan satu kesatuan yang tidak dapat dipisahkan dari Perjanjian Kredit ini.
              </li>
              <li>
                Penyelenggaran rekening pinjaman dan rekening atau catatan-catatan lainnya tersebut ayat 1 pasal ini dilakukan oleh
                BANK pada Kantor Cabangnya yaitu Cabang <?php echo $branch_name?>
              </li>
              <li>Untuk Keperluan administrasi, BANKmewajibkan DEBITUR membuka Rekening Tabungan pada BANK
                yaitu di Kantor Cabang  <?php echo $branch_name?>. dan menabung dalam rangka pemupukan angsuran kredit serta memelihara
                saldo tabungan setiap bulannya sampai dengan kredit lunas minimal sebesar 1 (satu) kali angsuran
                menurut ayat 2 pasal 4 Perjanjian Kredit ini.</li>
              <li>Seusai dengan cara dan ketentuan yang berlaku pada BANK, BANK akan membuat catatan baik dalam rekening DEBITUR maupun dalam catatan-catatan lainnya mengenai jumlah-jumlah yang sewaktu-waktu dipinjamkan kepada DEBITUR dan yang terhutang berdasarkan Perjanjian Kredit ini.</li>
              <li>Dalam setiap tuntutan hukum atau perkara yang timbul dari atau diakibatkan oleh Perjanjian Kredit ini dokumen jaminan, semua catatan yang dibuat menurut ayat 4 pasal ini harus merupakan bukti nyata dan sempurna dari jumlah-jumlah yang telah ditarik oleh DEBITUR dan setiap pembayaran yang telah diterima oleh BANK, serta suatu keterangan tertulis dari BANKsehubungan dengan jumlah terhutang harus merupakan bukti nyata dan sempurna perihal jumlah terhutang berdasarkan Perjanjian Kredit ini dan dokumen jaminan.</li>
            </ol>
            <p style="text-align:center;"><b>
                  PASAL 8<br/>
                  KUASA-KUASA
            </b></p>
            <p>BANK berhak dan dengan ini diberi kuasa oleh DEBITUR, kuasa mana merupakan bagian yang tidak terpisahkan dari Perjanjian Kredit ini, dan oleh karenanya kuasa ini tidak akan berakhir karena sebab-sebab yang ditentukan oleh pasal 1813 KUH Perdata, serta dengan telah ditandatanganinya Perjanjian Kredit ini tidak diperlukannya lagi adanya suatu surat kuasa khusus, untuk sewaktu-waktu tanpa persetujuan terlebih dahulu oleh DEBITUR untuk :</p>
            <ol style="margin-left:-25px;" type="1">
                <li>Bertindak untuk dan atas nama DEBITUR melakukan pembayaran pada waktu yang dianggap baik oleh BANK terhadap sejumlah uang yang diperoleh DEBITUR dari kredit tersebut pada pasal 1 Perjanjian Kredit ini kepada Dealer atau penjual kendaraan bermotor yang dibeli oleh DEBITUR tersebut pada pasal 2 Perjanjian Kredit ini</li>
                <li>Melaksanakan tindakan-tindakan yang dianggap perlu diantaranya menjual dimuka umum atau dibawah tangan barang-barang yang diserahkan sebagai jaminan pada pasal 6 Perjanjian Kredit ini, apabila DEBITUR dari sebab apapun juga tidak memenuhi salah satu atau lebih kewajiban yang timbul dari Perjanjian Kredit ini. Hasil penjualan tersebut diperhitungkan pertama kali untuk  pelunasan kewajiban DEBITUR pada BANK, dan apabila ternyata hasil penjualan tersebut tidak mencukupi pelunasan kewajiban DEBITUR pada BANK, maka DEBITUR berkewajiban menyerahkan barang jaminan lain kepada BANK sampai DEBITUR melunasi seluruh kewajibannya.</li>
                <li>Melakukan eksekusi melalui penjualan agunan di bawah tangan yang dilakukan tanpa melalui proses pelelangan umum dengan tujuan mempercepat penjualan obyek Hak Tanggungan/FEO dan agar tercapai harga penjualan tertinggi apabila penjualan melalui pelelangan umum diperkirakan tidak akan menghasilkan harga tertinggi. Hal ini Debitur dengan serta merta membebankan kepada Bank sebagai pihak yang menerima kuasa untuk melakukan kuasa menjual di muka umum maupun di bawah tangan.</li>
                <li>Membebani rekening giro, tabungan  dan atau rekening pinjaman DEBITUR yang ada pada BANK untuk pembayaran hutang pokok, bunga kredit, bunga tunggakan, denda, premi asuransi, biaya pengikatan barang jaminan dan biaya lainnya yang timbul karena dan untuk pelaksanaan Perjanjian Kredit ini.</li>
                <li>Kuasa-kuasa yang diberikan DEBITUR kepada BANKtersebut ayat 1 dan 2 pasal ini diberikan dengan Hak Substitusi dan merupakan bagian yang tidak dapat dipisahkan dari Perjanjian Kredit ini sehingga tanpa adanya kuasa-kuasa tersebut Perjanjian Kredit ini tidak akan dibuat.</li>
            </ol>
            <p style="text-align:center;"><b>
                  PASAL 9<br/>
                  PENGAWASAN DAN PEMERIKSAAN BARANG JAMINAN
            </b></p>
            <ol style="margin-left:-25px;page-break-after: always;" type="1">
              <li>Selama DEBITUR belum melunasi seluruh kreditnya yang timbul dari Perjanjian Kredit ini, maka BANK berhak setiap saat yang dianggap layak oleh BANK, melakukan pemeriksaan dan meminta keterangan-keterangan setempat yang diperlukan. </li>
              <li>DEBITUR menyetujui dan mewajibkan serta mengikatkan diri untuk memberikan keterangan-keterangan secara benar atas pertanyaan-pertanyaan pihak BANKdalam rangka pengawasan dan pemeriksaaan barang jaminan ini. </li>
            </ol>


            <p style="text-align:center;"><b>
                  PASAL 10<br/>
                  HAK-HAK BANKUNTUK MENOLAK PENARIKAN KREDIT <br/>
                  DAN MENGAKHIRI JANGKA WAKTU KREDIT SERTA PENAGIHAN SEKETIKA SELURUH KEWAJIBAN
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>Tanpa memperhatikan ketentuan mengenai angsuran bulanan dan jangka waktu kredit ini, BANKberhak dan dapat untuk menolak/ menghentikan penarikan kredit dan atau untuk seketika menagih pelunasan sekaligus atas seluruh sisa kredit DEBITUR kepada BANKyang timbul dari Perjanjian Kredit ini, dan DEBITUR wajib membayarnya dengan seketika dan sekaligus lunas untuk seluruh sisa kredit yang ditagih oleh BANK, dalam hal terjadi salah satu atau beberapa keadaan di bawah ini, yaitu:
                <ol type="a">
                  <li>DEBITUR menunggak pembayaran angsuran kredit sebanyak 3 kali angsuran baik secara berturut-turut maupun tidak berturut-turut. </li>
                  <li>DEBITUR tidak mungkin lagi atau tidak mempunyai dasar hukum untuk memenuhi sesuatu ketentuan atau kewajiban berdasarkan Perjanjian Kredit ini antara lain: diberhentikan dari Kantor/ Instansi yang bersangkutan, dijatuhi hukuman Pidana, mendapat cacat badan sehingga oleh karenanya belum/ tidak dapat dipekerjakan lagi, dipindahkan ke kota/ daerah lain atau ke luar negeri. </li>
                  <li>Perusahaan tempat DEBITUR bekerja atau DEBITUR telah dinyatakan pailit atau tidak mampu membayar atau telah dikeluarkan perintah oleh pejabat yang berwenang untuk menunjuk wali atau kuratornya. </li>
                  <li>DEBITUR membuat atau menyebabkan atau menyetujui dilakukan atau membiarkan dilakukan suatu tindakan yang membahayakan atau dapat membahayakan, mengurangi atau meniadakan jaminan yang diberikan untuk kredit. </li>
                  <li>Harta-harta DEBITUR yang diberikan sebagai jaminan kredit telah musnah. </li>
                  <li>Setiap keterangan yang diberikan, hal-hal yang disampaikan atau jaminan yang dibuat oleh DEBITUR kepada BANKterbukti palsu atau menyesatkan dalam segala segi atau DEBITUR lalai atau gagal untuk memberikan keterangan yang benar atau sesungguhnya kepada BANK.  </li>
                  <li>DEBITUR gagal dalam memenuhi atau DEBITUR bertindak bertentangan dengan sesuatu peraturan Pemerintah atau Daerah, Undang-undang atau Peraturan-peraturan yang mempunyai akibat penting terhadap atau mempengaruhi hubungan kerjanya dengan Kantor tempat bekerja. </li>
                  <li>Setiap sebab atau kejadian lainnya yang telah terjadi atau mungkin akan terjadi sehingga menjadi layak bagi BANKuntuk melakukan penagihan seketika mengenai seluruh (sisa) kredit guna melindungi kepentingan-kepentingannya, satu dan lainnya semata-mata menurut penetapan/pertimbangan BANK.</li>
                  <li>DEBITUR tidak atau belum mempergunakan / menarik kredit setelah lewat 1 (satu) bulan sejak ditandatanganinya Perjanjian Kredit ini dan untuk hal ini DEBITUR tidak berhak untuk meminta kembali atas provisi, biaya administrasi dan biaya lain yang telah disetor kepada BANK.</li>
                </ol>
              </li>
              <li>DEBITUR mengikatkan diri kepada BANKdan menyatakan dengan sesungguhnya bahwa apabila DEBITUR oleh sebab apapun juga tidak dapat membayar angsuran kredit sesuai dengan jumlah dan jadwal angsuran sebagaimana dilampirkan pada dan merupakan satu kesatuan yang tidak dapat dipisahkan dari Perjanjian Kredit ini, sebanyak 3 (tiga) kali angsuran berturut turut maupun, maka BANKdapat dan berhak untuk melakukan penyitaan atas rumah  seperti yang tercantum dalam pasal 2 Perjanjian Kredit ini.</li>
              <li>Apabila setelah mendapat peringatan dari BANK, DEBITUR tidak dapat melunasi seluruh sisa kewajiban pembayarannya yang seketika ditagih oleh BANKkarena terjadinya hal-hal yang disebutkan di dalam ayat 1 pasal ini, maka BANKberhak memerintahkan kepada DEBITUR untuk mengosongkan rumah  yang dibiayai dengan fasilitas kredit ini, DEBITUR mengikatkan diri untuk melaksanakan pengosongan  rumah  tersebut pasal 2 Perjanjian Kredit ini, selambat-lambatnya dalam jangka waktu 30 hari dihitung mulai tanggal perintah BANKuntuk itu, tanpa syarat dan ganti rugi apapun juga.</li>
              <li>Apabila DEBITUR ternyata tidak mengosongkan rumah  tersebut ayat 3 pasal ini dalam jangka waktu yang ditentukan dalam ayat 2 pasal ini, maka BANKberhak untuk meminta bantuan pihak berwenang guna mengeluarkan DEBITUR dan mengosongkan rumah  tersebut.</li>
              <li>DEBITUR dengan ini menyatakan melepaskan haknya untuk meminta bantuan dari Instansi manapun mengenai pengosongan rumah  tersebut,  apabila haknya untuk itu memang ada.</li>
            </ol>

            <p style="text-align:center;"><b>
                  PASAL 11<br/>
                  PELAKSANAAN EKSEKUSI BARANG JAMINAN
            </b></p>
            <ol style="margin-left:-25px;page-break-after: always;" type="1">
              <li>Apabila berdasarkan pasal 10 Perjanjian Kredit ini, BANK menggunakan haknya untuk menagih pelunasan sekaligus atas utang DEBITUR, dan DEBITUR tidak dapat memenuhi kewajibannya membayar pelunasan tersebut walaupun telah mendapat peringatan-peringatan dari BANK, maka BANK berhak untuk setiap saat melaksanakan hak eksekusinya atas agunan yang dipegangnya, menurut cara dan dengan harga yang dianggap baik oleh BANK dalam batas-batas yang diberikan oleh undang-undang serta peraturan hukum lainnya.</li>
              <li>Hasil eksekusi dan atau penjualan barang agunan tersebut dalam ayat 1 pasal ini pertama-tama akan digunakan untuk melunasi sisa utang DEBITUR kepada BANK, termasuk semua biaya yang dikeluarkan BANK guna melaksanakan eksekusi barang agunan, dan apabila masih ada sisanya jumlah sisa tersebut akan dibayarkan kembali kepada DEBITUR.</li>
              <li>Apabila dari hasil penjualan atau eksekusi barang agunan kredit sebagaimana tersebut pada ayat 2 pasal ini jumlahnya belum mencukupi untuk melunasi seluruh utang DEBITUR kepada BANK, maka sesuai dengan ketentuan peraturan yang berlaku BANK berhak untuk mengambil pelunasan atas sisa utang tersebut dari penjualan barang-barang lain milik DEBITUR, yang ditunjuk oleh DEBITUR sebagai agunan tambahan atas kredit ini.</li>
            </ol>


            <p style="text-align:center;"><b>
                  PASAL 12<br/>
                  ALAMAT PIHAK-PIHAK
            </b></p>
            <ol style="margin-left:-25px;" type="1">
              <li>Seluruh pembayaran hutang atau setiap bagian dari hutang DEBITUR dan surat menyurat harus dilakukan pada Kantor BANKyang telah ditentukan pada jam-jam kerja dari Kantor yang bersangkutan.</li>
              <li>Semua surat menyurat dan pernyataan-pernyataan tertulis yang timbul dari dan berakar pada Perjanjian Kredit ini dianggap telah diserahkan dan diterima apabila dikirimkan kepada :
                <ul>
                  <li>
                    Pihak BANK dengan alamat    : <?php echo $branch_address?>
                  </li>
                  <li>
                    Pihak DEBITUR dengan alamat : <?php echo $custaddr?>
                  </li>
                </ul>
              </li>
              <li>Kedua belah pihak masing-masing akan memberitahukan secara tertulis pada kesempatan pertama secepatnya setiap terjadi perubahan alamat, DEBITUR pindah/tidak lagi menghuni rumah yang bersangkutan dan sebagainya.</li>
            </ol>


            <p style="text-align:center;"><b>
                  PASAL 13<br/>
                  DOMISILI
            </b></p>
            <p>
                Tentang Perjanjian Kredit ini dan segala akibatnya serta pelaksanaannya kedua belah pihak memilih tempat kedudukan hukum (domisili) yang tetap dan umum di Kantor Kepaniteraan Pengadilan Negeri Palembang dan/atau Badan Urusan Piutang dan Lelang Negara (BUPLN) di Sumatera Selatan dengan tidak mengurangi hak dan wewenangnya BANK untuk memenuhi pelaksanaan/eksekusi atau menajukan tuntutan hukum terhadap DEBITUR berdasarkan Perjanjian Kredit ini melalui atau dihadapan Pengadilan-pengadilan lainnya di manapun juga di dalam Wilayah Republik Indonesia
            </p>


            <p style="text-align:center;"><b>
                  PASAL 14<br/>
                  PASAL TAMBAHAN
            </b></p>

            <p>Atas pemberian kredit ini berlaku pula ketentuan sebagai berikut :</p>
            <ol style="margin-left:-25px;" type="1">
              <li>DEBITUR sebelum kredit  ini  lunas  tidak  diperkenankan  tanpa  persetujuan tertulis dari BANK untuk menerima fasilitas  kredit  dari  BANK lain  atau  lembaga keuangan lain yang sumber pengembalian kreditnya berasal dari gaji/penghasilannya </li>
              <li>Segala sesuatu yang belum cukup diatur dalam Perjanjian Kredit ini oleh BANK diatur dalam surat menyurat dan kertas-kertas lain merupakan bagian yang tidak dapat dipisahkan dari Perjanjian Kredit ini.</li>
            </ol>

            <p>Demikian Perjanjian Kredit ini dibuat dan ditandatangani oleh kedua belah pihak yang dibuat dalam rangkap 2 (dua) masing-masing bermaterai cukup dan mempunyai kekuatan pembuktian yang sama. </p>



            <table style="width:100%;">
              <tr>
                <td style="width:300px;text-align:left;">
                  PT Bank Pembangunan Daerah <br/>
                  Mengetahui/menyetujui<br/>
                  Sumatera Selatan dan Bangka Belitung <br/>
                  Cabang <?php echo $branch_name?>
                </td>
                <td>&nbsp;</td>
                <td style="width:200px;text-align:center;">DEBITUR</td>
              </tr>
              <tr>
                <td colspan="3" style="height:100px"></td>
              </tr>
              <tr>
                <td style="text-align:center;">
                  <?php echo $namapemimpin_1?><br/>
                  <?php echo $jabatanpimpinan?>
                </td>
                <td>&nbsp;</td>
                <td style="text-align:center;">MENYETUJUI SUAMI/ISTERI</td>
              </tr>
            </table>

          </td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:center;">
            <?php
            if($userid != "" && $userwfid!="")
            {
            require ("../../requirepage/btnview.php");
            require ("../../requirepage/hiddenfield.php");
            }
            require("../../requirepage/btnprint.php");
            ?>

          </td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">
      <? require ("../../requirepage/hiddenfield.php"); ?>
    </form>
  </body>
</html>
