<?
	require ("../../lib/open_conCOLL.php");
	require ("../../lib/formatError.php");
	//require ("../../requirepage/parameter.php");
	//require ("../../requirepage/security.php");
    
    $start=date("Y-m-d");
    $end=date("Y-m-d");
    if(isset($_REQUEST['start']) && isset($_REQUEST['end']))
    {
        $start=$_REQUEST['start'];
        $end=$_REQUEST['end'];
    }
    
?>
<html>
	<head>
		<title>APPRAISAL</title>
		
		<script type="text/javascript" src="../../bootstrap/dist/js/jquery-1.11.3.js" ></script>
		<script type="text/javascript" src="../../bootstrap/dist/js/bootstrap.min.js" ></script>
        
        <script type="text/javascript" src="../../js/datatable.js"></script>
        <script type="text/javascript" src="../../js/bootstraptable.js"></script>
        <script type="text/javascript" src="../../js/bootstrap-datepicker.min.js"></script>
        
		<link href="../../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
        <link rel="stylesheet" href="../../bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
        
		<link href="../../css/table.css" rel="stylesheet" type="text/css" />
        
		<script type="text/javascript">
        $(document).ready(function() {
				$('#example').DataTable();
                $('.erikbuat').attr("readonly","readonly")
                $('.erikbuat').datepicker({format: 'yyyy-mm-dd',todayBtn: "linked"});
			} );
            
            function getreport()
            {
                start = parseInt($("#start").val().replace(/\-/g, ""));
                end = parseInt($("#end").val().replace(/\-/g, ""));
                
                if(end<=start)
                {
                    alert("Tanggal akhir terlalu kecil");    
                }
                else
                {
                    $("#formcheker").submit();
                }
            }
        </script>
        <style>
            .erikbuat
            {
                border:1px solid #F0F0F0;
                line-height:30px;
                height:30px;
                padding-left:5px;
                border-radius:5px;
                background:#EBEBEB;
            }
            .erikbuat:active
            {
               border:1px solid #66AFE9; 
            }
        </style>
    </head>        
	<body>
        <form id="formcheker" method="get" action="" enctype="multipart/form-data">
            <table align="center" border="0" style ="width:1000px; border-color:black;border-collapse:collapse;">
                <tr>
                    <td style="padding:20px 15 20 15px">
                    <!---->
                        
                            <h2>Collection Report
                            
                            <div style="float:right;font-size:10pt;">
                            <input type="text" readonly="readonly" class="erikbuat" id="start" name="start" value="<?=$start?>"/> To 
                            <input type="text" readonly="readonly" class="erikbuat" id="end" name="end" value="<?=$end?>"/>
                            <input type="button" onclick="getreport();" class="btn btn-info" value="submit"/>
                            </div>
                            </h2>
                        <hr>
                        <?
                        $start=date("Y-m-d");
                        $end=date("Y-m-d");
                        if(isset($_REQUEST['start']) && isset($_REQUEST['end']))
                        {
                            //$_REQUEST['start']
                            
                        ?>
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Tagihan</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Nominal</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nama</th>
                                    <th>Tagihan</th>
                                    <th>Tanggal Bayar</th>
                                    <th>Nominal</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                
 
                                $start=$_REQUEST['start'];
                                $end=$stop_date = date('Y-m-d', strtotime($_REQUEST['end'] . ' +1 day'));
                                $tsql = "select c._cust_name,a._payment_tagihanke,convert(varchar,a._payment_tglbayar,105) as 'tglbayar',a._payment_nominal
                                        from collect_custpayment a
                                        join collect_custtxn b on a._custnomid = b._custnomid
                                        and a._payment_tagihanke = b._txn_tagihanke
                                        join collect_custmaster c on a._custnomid= c._custnomid
                                        where a._payment_tglbayar BETWEEN '".$start."' AND '".$end."'
                                        order by c._cust_name,a._payment_tagihanke
                                        ";
                                $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                                $params = array(&$_POST['query']);
                                $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
                                $rowimage = sqlsrv_num_rows($sqlConn);
                                
                                if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
                                if(sqlsrv_has_rows($sqlConn))
                                {
                                    
                                    while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
                                    {
                                        echo "<tr>";
                                        echo "<td>".$row['_cust_name']."</td>";
                                        echo "<td>".$row['_payment_tagihanke']."</td>";
                                        echo "<td>".$row['tglbayar']."</td>";
                                        echo "<td>".number_format($row['_payment_nominal'])."</td>";
                                        echo "</tr>";
                                    }
                                    
                                }
                                ?>
                            </tbody>
                        </table>
                        <?
                        }
                        ?>
                    <!---->
                    </td>
                </tr>
            </table>
            <?
                require ("../../requirepage/hiddenfield.php");
            ?>
            <input type="hidden" name="_collateral_id" id="_collateral_id" maxlength="50" value="<?=$col_id;?>"   />
        </form>
	</body>
</html>