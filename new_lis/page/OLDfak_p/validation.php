<?
echo'
if	(duty=="")
{
	alert("Silahkan isi tanggungan.");
	$("#duty").focus();
}
else if (address=="")
{
	alert("Silahkan isi alamat.");
	$("#address").focus();
}
else if(rt=="")
{
	alert("Silahkan isi RT.");
	$("#rt").focus();
}
else if(rw=="")
{
	alert("Silahkan isi Rw.");
	$("#rw").focus();
}
else if(kec=="")
{
	alert("Silahkan isi kecamatan.");
	$("#kec").focus();
}
else if(kel=="")
{
	alert("Silahkan isi kelurahan.");
	$("#kel").focus();
}
else if(city=="")
{
	alert("Silahkan isi kota.");
	$("#city").focus();
}
else if(zip=="")
{
	alert("Silahkan isi kode pos.");
	$("#zip").focus();
}
else if (fixphone=="")
{
	alert("Silahkan isi telepon rumah.");
	$("#fixphone").focus();
}
else if (hp=="")
{
	alert("Silahkan isi nomor hp.");
	$("#hp").focus();
}
else if	(homestatus=="")
{
	alert("Silahkan pilih status tempat tinggal saat ini.");
	$("#homestatus").focus();
}
else if	(homeyearlong=="")
{
	alert("Silahkan isi lama tahun menempati rumah.");
	$("#homeyearlong").focus();
}
else if	(homemonthlong=="")
{
	alert("Silahkan isi lama bulan menempati rumah.");
	$("#homemonthlong").focus();
}
else if(getflag=="2")
{	
	if (ktpaddress=="")
	{
		alert("Silahkan isi alamat sesuai KTP.");
		$("#ktpaddress").focus();
	}
	else if(ktprt=="")
	{
		alert("Silahkan isi RT sesuai KTP.");
		$("#ktprt").focus();
	}
	else if(ktprw=="")
	{
		alert("Silahkan isi Rw sesuai KTP.");
		$("#ktprw").focus();
	}
	else if(ktpkec=="")
	{
		alert("Silahkan isi kecamatan sesuai KTP.");
		$("#ktpkec").focus();
	}
	else if(ktpkel=="")
	{
		alert("Silahkan isi kelurahan sesuai KTP.");
		$("#ktpkel").focus();
	}
	else if(ktpcity=="")
	{
		alert("Silahkan isi kota sesuai KTP.");
		$("#ktpcity").focus();
	}
	else if(ktpzip=="")
	{
		alert("Silahkan isi kode pos sesuai KTP.");
		$("#ktpzip").focus();
	}
	else if(bussname=="")
	{
		alert("Silahkan isi nama usaha / toko.");
		$("#bussname").focus();
	}
	else if(bussaddress=="")
	{
		alert("Silahkan isi alamat usaha / toko.");
		$("#bussaddress").focus();
	}
	else if(busspfixphone=="")
	{
		alert("Silahkan isi nomor telepon usaha / toko.");
		$("#busspfixphone").focus();
	}
	else if(custbushp=="")
	{
		alert("Silahkan isi nomor hp usaha / toko.");
		$("#custbushp").focus();
	}
	else if(bussnpwp=="")
	{
		alert("Silahkan isi NPWP usha / toko.");
		$("#bussnpwp").focus();
	}
	else if(busssiup=="")
	{
		alert("Silahkan isi SIUP usha / toko.");
		$("#busssiup").focus();
	}
	else if(busstdp=="")
	{
		alert("Silahkan isi TDP usha / toko.");
		$("#busstdp").focus();
	}
	else if(custcreditstatus=="")
	{
		alert("Silahkan pilih Permohonan Kredit.");
		$("#custcreditstatus").focus();
	}
	else if(statuscredit=="")
	{
		alert("Silahkan pilih jenis pemohonan kredit.");
		$("#statuscredit").focus();
	}
	else if(rowcount=="5")
	{
		alert("Silahkan Tambah fasilitas.");
		$("#creditneed").focus();
	}
	else if(totalplafond<parseFloat(min_plafond))
	{
		alert("Silahkan Tambah fasilitas karena total plafond belum sampai "+money_min_plafond+".");
		$("#creditneed").focus();
	}
	else if(totalplafond > max_plafond)
	{
		alert("Silahkan Menguragi jumlah fasilitas karena telah melebihi "+money_max_plafond+".");
		$("#creditneed").focus();
	}
	else if(lifeinsurance=="")
	{
		alert("Silahkan pilih asuransi jiwa.");
		$("#lifeinsurance").focus();
	}
	else if(lifeinsurance=="JK99")
	{
		if(otherlifeinsurance=="")
		{				
			alert("Silahkan isi asuransi jiwa lainnya.");
			$("#otherlifeinsurance").focus();
		}
		else
		{
			if(lostinsurance=="")
			{
				alert("Silahkan pilih asuransi kerugian.");
				$("#lostinsurance").focus();
			}
			else if(lostinsurance=="JK99")
			{	
				if(otherlostinsurance=="")
				{				
					alert("Silahkan isi asuransi kerugian ke lainnya.");
					$("#otherlostinsurance").focus();
				}
				else if(lkcddate=="")
				{				
					alert("Silahkan pilih tanggal kunjungan.");
					$("#lkcddate").focus();
				}
				else if (introduce=="")
				{
					alert("Silahkan pilih perkenalan.");
					$("#introduce").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else if(lostinsurance!="JK99" || lostinsurance!="")
			{
				if(lkcddate=="")
				{				
					alert("Silahkan pilih tanggal kunjungan.");
					$("#lkcddate").focus();
				}
				else if (introduce=="")
				{
					alert("Silahkan pilih perkenalan.");
					$("#introduce").focus();
				}
				else if (recomen=="")
				{
					alert("Silahkan pilih rekomendasi.");
					$("#recomen").focus();
				}
				else
				{
					document.getElementById("frm").action = "fak_ajax.php";
					submitform = window.confirm("'.$confmsg.'")
					if (submitform == true)
					{
						document.getElementById("frm").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
	}
	else if(lifeinsurance!="JK99" || lifeinsurance!="")
	{
		if(lostinsurance=="")
		{
			alert("Silahkan pilih asuransi kerugian.");
			$("#lostinsurance").focus();
		}
		else if(lostinsurance=="JK99")
		{	
			if(otherlostinsurance=="")
			{				
				alert("Silahkan isi asuransi kerugian ke lainnya.");
				$("#otherlostinsurance").focus();
			}
			else if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if(lostinsurance!="JK99" || lostinsurance!="")
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
}
else if(bussname=="")
{
	alert("Silahkan isi nama usaha / toko.");
	$("#bussname").focus();
}
else if(bussaddress=="")
{
	alert("Silahkan isi alamat usaha / toko.");
	$("#bussaddress").focus();
}
else if(busspfixphone=="")
{
	alert("Silahkan isi nomor telepon usaha / toko.");
	$("#busspfixphone").focus();
}
else if(custbushp=="")
{
	alert("Silahkan isi nomor hp usaha / toko.");
	$("#custbushp").focus();
}
else if(bussnpwp=="")
{
	alert("Silahkan isi NPWP usha / toko.");
	$("#bussnpwp").focus();
}
else if(busssiup=="")
{
	alert("Silahkan isi SIUP usha / toko.");
	$("#busssiup").focus();
}
else if(busstdp=="")
{
	alert("Silahkan isi TDP usha / toko.");
	$("#busstdp").focus();
}
else if(custcreditstatus=="")
{
	alert("Silahkan pilih Permohonan Kredit.");
	$("#custcreditstatus").focus();
}
else if(statuscredit=="")
{
	alert("Silahkan pilih jenis pemohonan kredit.");
	$("#statuscredit").focus();
}
else if(rowcount==5)
{
	alert("Silahkan Tambah fasilitas.");
	$("#creditneed").focus();
}
else if(totalplafond<parseFloat(min_plafond))
{
	alert("Silahkan Tambah fasilitas karena total plafond belum sampai "+money_min_plafond+".");
	$("#creditneed").focus();
}
else if(totalplafond > max_plafond)
{
	alert("Silahkan Menguragi jumlah fasilitas karena telah melebihi "+money_max_plafond+".");
	$("#creditneed").focus();
}
else if(lifeinsurance=="")
{
	alert("Silahkan pilih asuransi jiwa.");
	$("#lifeinsurance").focus();
}
else if(lifeinsurance=="JK99")
{
	if(otherlifeinsurance=="")
	{				
		alert("Silahkan isi asuransi jiwa lainnya.");
		$("#otherlifeinsurance").focus();
	}
	else
	{
		if(lostinsurance=="")
		{
			alert("Silahkan pilih asuransi kerugian.");
			$("#lostinsurance").focus();
		}
		else if(lostinsurance=="JK99")
		{	
			if(otherlostinsurance=="")
			{				
				alert("Silahkan isi asuransi kerugian ke lainnya.");
				$("#otherlostinsurance").focus();
			}
			else if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if(lostinsurance!="JK99" || lostinsurance!="")
		{
			if(lkcddate=="")
			{				
				alert("Silahkan pilih tanggal kunjungan.");
				$("#lkcddate").focus();
			}
			else if (introduce=="")
			{
				alert("Silahkan pilih perkenalan.");
				$("#introduce").focus();
			}
			else if (recomen=="")
			{
				alert("Silahkan pilih rekomendasi.");
				$("#recomen").focus();
			}
			else
			{
				document.getElementById("frm").action = "fak_ajax.php";
				submitform = window.confirm("'.$confmsg.'")
				if (submitform == true)
				{
					document.getElementById("frm").submit();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
}
else if(lifeinsurance!="JK99" || lifeinsurance!="")
{
	if(lostinsurance=="")
	{
		alert("Silahkan pilih asuransi kerugian.");
		$("#lostinsurance").focus();
	}
	else if(lostinsurance=="JK99")
	{	
		if(otherlostinsurance=="")
		{				
			alert("Silahkan isi asuransi kerugian ke lainnya.");
			$("#otherlostinsurance").focus();
		}
		else if(lkcddate=="")
		{				
			alert("Silahkan pilih tanggal kunjungan.");
			$("#lkcddate").focus();
		}
		else if (introduce=="")
		{
			alert("Silahkan pilih perkenalan.");
			$("#introduce").focus();
		}
		else if (recomen=="")
		{
			alert("Silahkan pilih rekomendasi.");
			$("#recomen").focus();
		}
		else
		{
			document.getElementById("frm").action = "fak_ajax.php";
			submitform = window.confirm("'.$confmsg.'")
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else if(lostinsurance!="JK99" || lostinsurance!="")
	{
		if(lkcddate=="")
		{				
			alert("Silahkan pilih tanggal kunjungan.");
			$("#lkcddate").focus();
		}
		else if (introduce=="")
		{
			alert("Silahkan pilih perkenalan.");
			$("#introduce").focus();
		}
		else if (recomen=="")
		{
			alert("Silahkan pilih rekomendasi.");
			$("#recomen").focus();
		}
		else
		{
			document.getElementById("frm").action = "fak_ajax.php";
			submitform = window.confirm("'.$confmsg.'")
			if (submitform == true)
			{
				document.getElementById("frm").submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}

';

?>