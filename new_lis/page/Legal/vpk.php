<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?
require ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require ("../../lib/open_con_apr.php");
require ("../../requirepage/parameter.php");


$tsql = "select r.region_name from Tbl_SE_User s, Tbl_Region r,Tbl_Branch b
where b.branch_code=s.user_branch_code
and r.region_code=b.branch_region_code
and s.user_id='$userid'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlConn)){
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$region_name = $row['region_name'];
	}
}
sqlsrv_free_stmt( $sqlConn );

//DECLARE VARIABLES
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
$userwfid=$_REQUEST['userwfid'];
$custnomid=$_REQUEST['custnomid'];
//echo $userwfid;
//echo $custnomid;
require('../../query/cmp_query.php');

$datenow=date("Y-m-d");
//echo $datenow;

$str=substr($custnomid,3,strlen($custnomid)-3);
$strpp=substr($custnomid,0,3);


/*
echo "userid ".$userid;
echo "<br>";
echo "userpwd ".$userpwd;
echo "<br>";
echo "userbranch ".$userbranch;
echo "<br>";
echo "userregion ".$userregion;
echo "<br>";
echo "userwfid ".$userwfid;
echo "<br>";
echo "userpermission ".$userpermission;
echo "<br>";
echo "buttonaction ".$buttonaction;
echo "<br>";
echo "custnomid ".$custnomid;
*/

//SECURITY PROFILE AND WORKFLOW SETTINGS

if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	header("location:restricted.php");
	//echo $userid."</br>".$userwfid."</br>".$userbranch."</br>".$userregion."</br>".$userpwd."</br>".$custnomid;
}

// SECURITY
	$tsql = "SELECT COUNT(*) FROM Tbl_SE_User WHERE user_id='$userid' AND user_pwd='$userpwd'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlConn)){
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$thecount = $row[0];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	if ($thecount == 0)
	{
		header("location:restricted.php");
		//echo $userid."</br>".$userwfid."</br>".$userbranch."</br>".$userregion."</br>".$userpwd."</br>".$custnomid;
	}

	$tsql = "SELECT COUNT(*) FROM Tbl_SE_UserProgram WHERE user_id='$userid' AND program_code='$userwfid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$thecount = $row[0];
		}
	}
	sqlsrv_free_stmt($sqlConn);
	if ($thecount == 0)
	{
		header("location:restricted.php");
		//echo $userid."</br>".$userwfid."</br>".$userbranch."</br>".$userregion."</br>".$userpwd."</br>".$custnomid;
	}

	$tsql = "SELECT * FROM Tbl_SE_UserProgram WHERE user_id='$userid' AND program_code='$userwfid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
	$userpinp = "";
	$userpchk = "";
	$userpapr = "";
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$userpinp = substr($row[2],1-1,1);
			$userpchk = substr($row[2],2-1,1);
			$userpapr = substr($row[2],3-1,1);
		}
	}
	sqlsrv_free_stmt( $sqlConn );

// END SECURITY

	// PROFILE USER ID (AO / TL / PINCA)
	$tsql = "SELECT user_ao_code, user_level_code, user_child FROM Tbl_SE_User WHERE user_id='$userid'";
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$profileaocode = $row[0];
			$profilelevelcode = $row[1];
			$profileuserchild = $row[2];
		}	
	}
	sqlsrv_free_stmt( $sqlConn );
	// END PROFILE
//SELECT From Tbl_Workflow & PrevFlow
$wfname="PERJANJIAN KREDIT";
//END GET WFNAME

	

	
	
	
$nomor_custmarcode="";
$cmp_jabatan_code="";
$cmp_cust_sex="";
$cmp_ktp_no="";
$cmp_ao="";
$cmp_branch="";
$cmp_fname="";
$cmp_mar="";
$cmp_ct1="";
$cmp_ct2="";
$cmp_cp1="";
$cmp_cp2="";
$cmp_cn1="";
$cmp_rt="";
$cmp_rw="";
$cmp_kelurahan="";
$cmp_kecamatan="";
$cmp_kota="";
$cmp_no_telp="";
$cmp_addr="";
$cmp_no_hp="";
$cmp_propinsi="";
$cmp_name_menyetujui="";

$sql_cmp="select 'debiturname'=case when custsex='0' then custbusname else  custfullname end,* from Tbl_CustomerMasterPerson2 where custnomid='$custnomid' ";//echo $sql_cmp;
$cursortype_cmp = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cmp = array(&$_POST['query']);
$sqlConn_cmp = sqlsrv_query($conn, $sql_cmp, $params_cmp, $cursortype_cmp);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_cmp))
{
	$rowCount_cmp = sqlsrv_num_rows($sqlConn_cmp);
	while($row_cmp = sqlsrv_fetch_array( $sqlConn_cmp, SQLSRV_FETCH_ASSOC))
	{
		$cmp_ao=$row_cmp['custaocode'];
		$cmp_branch=$row_cmp['custbranchcode'];
		$cmp_fname=$row_cmp['debiturname'];
		$cmp_mar=$row_cmp['custmarname'];
		$cmp_ct1=$row_cmp['custcredittype1'];
		$cmp_ct2=$row_cmp['custcredittype2'];
		$cmp_cp1=$row_cmp['custcreditplafond1'];
		$cmp_cp2=$row_cmp['custcreditplafond2'];
		$cmp_cn1=$row_cmp['custcreditneed1'];
		$custcreditlong1=$row_cmp['custcreditlong1'];
		$cmp_ktp_no=$row_cmp['custktpno'];
		$cmp_cust_sex=$row_cmp['custsex'];
		$nomor_custmarcode=$row_cmp['custmarcode'];
		$cmp_addr=$row_cmp['custaddr'];
		$cmp_rt=$row_cmp['custrt'];
		$cmp_rw=$row_cmp['custrw'];
		$cmp_kelurahan=$row_cmp['custkel'];
		$cmp_kecamatan=$row_cmp['custkec'];
		$cmp_kota=$row_cmp['custcity'];
		$cmp_no_telp=$row_cmp['custtelp'];
		$cmp_no_hp=$row_cmp['custhp'];
		$cmp_jabatan_code=$row_cmp['custjabatancode'];
		$cmp_propinsi=$row_cmp['custprop'];
		$cmp_name_menyetujui=$row_cmp['custmarname'];
	
		
		
		//echo '</br>'.$cmp_ao;
			$sql_ao="Select * from tbl_ao where ao_code='$cmp_ao'";
			$cursortype_ao = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_ao = array(&$_POST['query']);
			$sqlConn_ao = sqlsrv_query($conn, $sql_ao, $params_ao, $cursortype_ao);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_ao))
			{
				$rowCount_ao = sqlsrv_num_rows($sqlConn_ao);
				while($row_ao = sqlsrv_fetch_array( $sqlConn_ao, SQLSRV_FETCH_ASSOC))
				{
				$ao_name=$row_ao['ao_name'];
				$ao_branch=$row_ao['ao_branch_code'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_ao );
		//echo $ao_branch;
		
		$branch_region_code="";
		$sql_branch="Select * from tbl_branch where branch_code='$ao_branch'";
			$cursortype_branch = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_branch = array(&$_POST['query']);
			$sqlConn_branch = sqlsrv_query($conn, $sql_branch, $params_branch, $cursortype_branch);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_branch))
			{
				$rowCount_branch = sqlsrv_num_rows($sqlConn_branch);
				while($row_branch = sqlsrv_fetch_array( $sqlConn_branch, SQLSRV_FETCH_ASSOC))
				{
				$branch_region_code=$row_branch['branch_region_code'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_branch );
		
		
		
			
			$denda_nilai_dkpa="";
			$sql_denda="Select control_value from ms_control where control_code='DKPA'";
			$cursortype_denda = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_denda = array(&$_POST['query']);
			$sqlConn_denda = sqlsrv_query($conn, $sql_denda, $params_denda, $cursortype_denda);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_denda))
			{
				//$rowCount_denda = sqlsrv_num_rows($sqlConn_denda);
				while($row_denda = sqlsrv_fetch_array( $sqlConn_denda, SQLSRV_FETCH_ASSOC))
				{
				$denda_nilai_dkpa=$row_denda['control_value'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_denda );
		
			
			$denda_nilai_dpd="";
			$sql_denda1="Select control_value from ms_control where control_code='dpd'";
			$cursortype_denda1 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_denda1 = array(&$_POST['query']);
			$sqlConn_denda1 = sqlsrv_query($conn, $sql_denda1, $params_denda1, $cursortype_denda1);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_denda1))
			{
				//$rowCount_denda1 = sqlsrv_num_rows($sqlConn_denda);
				while($row_denda1 = sqlsrv_fetch_array( $sqlConn_denda1, SQLSRV_FETCH_ASSOC))
				{
				$denda_nilai_dpd=$row_denda1['control_value'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_denda1 );
			
		
			$branch_city="";
			$branch_name="";
			$branch_addr="";
			$sql_branch="Select * from tbl_branch where branch_code='$cmp_branch'";
			$cursortype_branch = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_branch = array(&$_POST['query']);
			$sqlConn_branch = sqlsrv_query($conn, $sql_branch, $params_branch, $cursortype_branch);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_branch))
			{
				$rowCount_branch = sqlsrv_num_rows($sqlConn_branch);
				while($row_branch = sqlsrv_fetch_array( $sqlConn_branch, SQLSRV_FETCH_ASSOC))
				{
				$branch_name=$row_branch['branch_name'];
				$branch_addr=$row_branch['branch_address'];
				$branch_city=$row_branch['branch_city'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_branch );
		
		}						
	}
	sqlsrv_free_stmt( $sqlConn_cmp );
	
	
$user_name="";
$sql_se_user="select * from tbl_se_user where user_level_code ='150' and user_branch_code='$ao_branch'";
$cursortype_se_user = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_se_user = array(&$_POST['query']);
$sqlConn_se_user = sqlsrv_query($conn, $sql_se_user, $params_se_user, $cursortype_se_user);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_se_user))
{
	$rowCount_se_user = sqlsrv_num_rows($sqlConn_se_user);
	while($row_se_user = sqlsrv_fetch_array( $sqlConn_se_user, SQLSRV_FETCH_ASSOC))
	{
	$user_name=$row_se_user['user_name'];
	}
}
sqlsrv_free_stmt( $sqlConn_se_user );
	
	
	
	$sql_jabatan="Select * from TblJabatan where jabatan_code='$cmp_jabatan_code'";
			$cursortype_jabatan = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_jabatan = array(&$_POST['query']);
			$sqlConn_jabatan = sqlsrv_query($conn, $sql_jabatan, $params_jabatan, $cursortype_jabatan);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_jabatan))
			{
				$rowCount_jabatan = sqlsrv_num_rows($sqlConn_jabatan);
				while($row_jabatan = sqlsrv_fetch_array( $sqlConn_jabatan, SQLSRV_FETCH_ASSOC))
				{
				$jabatan_name=$row_jabatan['jabatan_name'];
				//$jabatan_branch=$row_jabatan['jabatan_branch_code'];
				}
			}
			sqlsrv_free_stmt( $sqlConn_jabatan );
	
	
	
	
	

	//echo $cmp_ct1;

			$produk_type_description="";
		$sql_kodeproduk="select * from Tbl_KodeProduk where produk_loan_type='$cmp_ct1'";
		//echo $sql_kodeproduk;
			$cursortype_kodeproduk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			$params_kodeproduk = array(&$_POST['query']);
			$sqlConn_kodeproduk = sqlsrv_query($conn, $sql_kodeproduk, $params_kodeproduk, $cursortype_kodeproduk);
			if($conn==false){die(FormatErrors(sqlsrv_errors()));}
			if(sqlsrv_has_rows($sqlConn_kodeproduk))
			{
				$rowCount_kodeproduk = sqlsrv_num_rows($sqlConn_kodeproduk);
				while($row_kodeproduk = sqlsrv_fetch_array( $sqlConn_kodeproduk, SQLSRV_FETCH_ASSOC))
				{
				$produk_type_description=$row_kodeproduk['produk_type_description'];

				}
			}
			sqlsrv_free_stmt( $sqlConn_kodeproduk );




			
		$pk_custnomid="";
$pk_no="";
$pk_tgl1="";
$pk_wakil_a="";
$pk_wakil_b="";
$pk_no_sk="";
$pk_tgl2="";
$pk_nomor="";
$pk_tgl3="";
$pk_legalisasi="";
$pk_notaris="";
$pk_kota="";
$pk_alamat="";
$pk_rt="";
$pk_rw="";
$pk_kelurahan="";
$pk_kecamatan="";
$pk_ktp="";
$pk_tgl_materai="";
$pk_tgl_jk1="";
$pk_tgl_jk2="";
$pk_tgl_a1="";
$pk_tgl_a2="";
$pk_dkpa="";
$pk_dpd="";
$pk_narasi1pasal2="";
$pk_narasi2pasal2="";
$pk_narasi3pasal2="";
$pk_narasipasal5="";
$pk_bank_telp="";
$pk_bank_fax="";
$pk_debitur_fax="";
$pk_jabatan="";
$pk_nama_jabatan="";
$alamat_pk="";
$pk_jabatan1="";
$pk_nama_jabatan1="";
$nama_pengadilan="";
$pk_telepon="";
$tujuan_mmu="";
$inPKTemplate="";
$sql_pk="select * from tbl_pk where pk_custnomid='$custnomid'";
$cursortype_pk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_pk = array(&$_POST['query']);
$sqlConn_pk = sqlsrv_query($conn, $sql_pk, $params_pk, $cursortype_pk);
if($conn==false)
{
die(FormatErrors(sqlsrv_errors()));
}
if(sqlsrv_has_rows($sqlConn_pk))
{
	$rowCount_pk = sqlsrv_num_rows($sqlConn_pk);
	while($row_pk = sqlsrv_fetch_array( $sqlConn_pk, SQLSRV_FETCH_ASSOC))
	{
		$pk_custnomid=$row_pk['pk_custnomid'];
$pk_no=$row_pk['pk_no'];
$pk_tgl1=$row_pk['pk_tgl1'];
$pk_wakil_a=$row_pk['pk_wakil_a'];
$pk_wakil_b=$row_pk['pk_wakil_b'];
$pk_no_sk=$row_pk['pk_no_sk'];
$pk_tgl2=$row_pk['pk_tgl2'];
$pk_nomor=$row_pk['pk_nomor'];
$pk_tgl3=$row_pk['pk_tgl3'];
$pk_legalisasi=$row_pk['pk_legalisasi'];
$pk_notaris=$row_pk['pk_notaris'];
$pk_kota=$row_pk['pk_kota'];
$pk_alamat=$row_pk['pk_alamat'];
$pk_rt=$row_pk['pk_rt'];
$pk_rw=$row_pk['pk_rw'];
$pk_kelurahan=$row_pk['pk_kelurahan'];
$pk_kecamatan=$row_pk['pk_kecamatan'];
$pk_ktp=$row_pk['pk_ktp'];
$pk_tgl_materai=$row_pk['pk_tgl_materai'];
$pk_tgl_jk1=$row_pk['pk_tgl_jk1'];
$pk_tgl_jk2=$row_pk['pk_tgl_jk2'];
$pk_tgl_a1=$row_pk['pk_tgl_a1'];
$pk_tgl_a2=$row_pk['pk_tgl_a2'];
$pk_dkpa=$row_pk['pk_dkpa'];
$pk_dpd=$row_pk['pk_dpd'];
$pk_narasi1pasal2=$row_pk['pk_narasi1pasal2'];
$pk_narasi2pasal2=$row_pk['pk_narasi2pasal2'];
$pk_narasi3pasal2=$row_pk['pk_narasi3pasal2'];
$pk_narasipasal5=$row_pk['pk_narasipasal5'];
$pk_bank_telp=$row_pk['pk_bank_telp'];
$pk_bank_fax=$row_pk['pk_bank_fax'];
$pk_debitur_fax=$row_pk['pk_debitur_fax'];
$pk_jabatan=$row_pk['pk_jabatan'];
$pk_nama_jabatan=$row_pk['pk_nama_jabatan'];
$alamat_pk=$row_pk['alamat_pk'];
$pk_jabatan1=$row_pk['pk_jabatan1'];
$pk_nama_jabatan1=$row_pk['pk_nama_jabatan1'];
$nama_pengadilan=$row_pk['nama_pengadilan'];
$pk_telepon=$row_pk['pk_telepon'];
$tujuan_mmu=$row_pk['pk_tujuan_mmu'];
$inPKTemplate = $row_pk['inPKTemplate']; 
$pk_alamat_cabang = $row_pk['pk_alamat_cabang']; ;

	}
}
sqlsrv_free_stmt( $sqlConn_pk );


	


$pkTemplate = $inPKTemplate;

//echo "PK TEMPLATE =".$pkTemplate."=";

if($pkTemplate == "A")
{
	//echo "A";
	$templatePK = ", kecuali pembayaran angsuran terakhir harus dibayarkan pada tanggal jatuh tempo fasilitas kredit";
}
else if($pkTemplate == "B")
{
	//echo "B";
	$templatePK = ", yang untuk pertama kali dimulai pada tanggal ";
}
else
{
}






$sql_cn="select * from Tbl_CreditNeed where credit_need_code='$cmp_cn1' ";
$cursortype_cn = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_cn = array(&$_POST['query']);
$sqlConn_cn = sqlsrv_query($conn, $sql_cn, $params_cn, $cursortype_cn);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_cn))
	{
		$rowCount_cn = sqlsrv_num_rows($sqlConn_cn);
		while($row_cn = sqlsrv_fetch_array( $sqlConn_cn, SQLSRV_FETCH_ASSOC))
			{
			$cnname1=$row_cn['credit_need_name'];
			}						
	}
sqlsrv_free_stmt( $sqlConn_cn );




?>





































<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PK</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js" ></script>
<script type="text/javascript" src="../../js/accounting.js" ></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
</head>

<body>
<script language="JavaScript"><!--
name = 'utama';
//--></script>
   <form name="pkerik" method="post">
   
   
<input nai="" type=hidden name=userid  value='<? echo $userid; ?>'>
<input nai="" type=hidden name=userpwd value='<? echo $userpwd; ?>'>
<input nai="" type=hidden name=userbranch value='<? echo $userbranch; ?>'>
<input nai="" type=hidden name=userregion value='<? echo $userregion; ?>'>
<input nai="" type=hidden name=buttonaction value='<? echo $buttonaction; ?>'>
<input nai="" type=hidden name=userwfid  value='<? echo $userwfid; ?>'>
<input nai="" type=hidden name=custnomid  value='<? echo $custnomid; ?>'> 
   
<?
if($tujuan_mmu == "NOMMU")
{
	$titleMMU = "";
}
else
{

	$titleMMU = "-MMU";
}
?> 

<table width="800" border="0" align="center" class="preview">
	<tr>
		<td colspan="3" align="center">
		PERJANJIAN KREDIT</br>
		FASILITAS PEMBIAYAAN MEGA USAHA KECIL MENENGAH</br>
		("MEGA UKM<? echo $titleMMU;?>")</br>
		Nomor : UKM/LEG/<? echo $branch_region_code ?>/<? echo $ao_branch ?>/<? echo date('Y') ?>/<? echo $str; ?>
		<input type="hidden" name="pk_no" id="pk_no"  value="UKM/LEG/<? echo $branch_region_code ?>/<? echo $ao_branch ?>/<? echo date('Y') ?>/<? echo $str; ?>"/></td>
		<input type="hidden" name="pk_tgl_input" id="pk_tgl_input"  value="<?echo $datenow ?>"/></td>
		<input type="hidden" name="pk_no_host" id="pk_no_host"  value="<? echo $strpp.$branch_region_code.'0'.$ao_branch.$str; ?>"/>
		</td>
	</tr>
	<tr>
		<td style="text-align:justify;" valign="top" colspan="3">
			Perjanjian Kredit MEGA UKM ini (selanjutnya disebut Perjanjian MEGA UKM<? echo $titleMMU;?>) dibuat dan ditandatangani pada tanggal  <? echo $pk_tgl1 ?> oleh dan antara :
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="2%" valign="top">1</td>
		<td style="text-align:justify;" colspan="2">
		PT. Bank Mega, Tbk., berkedudukan di <? echo $region_name;?>, suatu bank berbentuk perseroan terbatas yang didirikan menurut dan berdasarkan hukum Republik Indonesia, dengan cabang-cabang antara lain beralamat di 
		<? echo $branch_addr;?> <? echo $pk_alamat_cabang;?> yang dalam hal ini diwakili oleh :
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td style="text-align:justify;" colspan="2">
			<table width="100%">
				<tr>
					<td valign="top" width="2%">A.</td>
					<td valign="top"><? echo $pk_wakil_a;?></td>
				</tr>
				<tr>
					<td valign="top" width="2%">B.</td>
					<td valign="top"><? echo $pk_wakil_b?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" style="text-align:justify;" colspan="3">
			Masing-masing bertindak dalam jabatannya tersebut selaku kuasa dari Direksi PT Bank Mega, Tbk, berdasarkan Surat Kuasa nomor SK <? echo $pk_no_sk;?> , 
			tanggal <? echo $pk_tgl2?> dan nomor <? echo $pk_nomor?> , tanggal <? echo $pk_tgl3?>;</br>
			- Untuk selanjutnya disebut "BANK";
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="2%" valign="top">2</td>
		<td style="text-align:justify;" colspan="2">
		<?
		if($cmp_cust_sex!="0")
		{
			if ($nomor_custmarcode=="1")
			{
				echo $cmp_fname.", bertempat tinggal di";?><? echo " ".$pk_kota; ?> <? echo ",";?> <? echo " ".$pk_alamat ?> <? echo ", Rukun Tetangga"; ?><? echo " ".$pk_rt ?><? echo ", Rukun Warga"?><? echo " ".$pk_rw ?> <? echo ", Desa/Kelurahan ";?> <? echo " ".$pk_kelurahan ?> <? echo ",";?><? echo " ".$pk_kecamatan ?><? echo ", Kota ".$cmp_kota.", Provinsi ".$cmp_propinsi.", pemegang Kartu Tanda Penduduk dengan Nomor Induk Kependudukan : ";?><? echo $pk_ktp ?>.<br><? echo "Dalam hal ini bertindak untuk diri dan untuk melakukan tindakan hukun dalam Perjanjian Kredit ini telah mendapat persertujuan dari suaminya/istrinya yaitu ".$cmp_name_menyetujui.", bertempat tinggal sama dengan suaminya/ istrinya tersebut diatas, pemegang Kartu Tanda Pendukuk Nomor ".$cmp_ktp_no." yang turut hadir dan menandatangani Perjanjian Kredit ini / Sebagaimana ternyata dari Surat Persetujuan yang dibuat di bawah tangan bermaterai cukup tertanggal ";?><? echo $pk_tgl_materai ?><? echo  " dan dilegalisasi dengan nomor ";?><? echo $pk_legalisasi ?><? echo ", notaris di"; ?> <? echo $pk_notaris?><? ;
			}
			else
			{
				echo $cmp_fname.", bertempat tinggal di";?><? echo " ".$pk_kota; ?> <? echo ",";?> <? echo " ".$pk_alamat ?> <? echo ", Rukun Tetangga"; ?><? echo " ".$pk_rt ?><? echo ", Rukun Warga"?><? echo " ".$pk_rw ?> <? echo ", Desa/Kelurahan ";?> <? echo " ".$pk_kelurahan ?> <? echo ",";?><? echo " ".$pk_kecamatan ?><? echo ", Kota ".$cmp_kota.", Provinsi ".$cmp_propinsi.", pemegang Kartu Tanda Penduduk dengan Nomor Induk Kependudukan : ";?><? echo $pk_ktp ?>.<br><? echo "dalam hal ini bertindak  untuk diri dan untuk melakukan tindakan hukum dalam Perjanjian Kredit ini tidak diperlukan persetujuan dari siapapun karena saat ini tidak terikat suatu perkawinan menurut ketentuan hukum manapun; sebagaimana ternyata dari Suat Pernyataan di buat dibawah tangan bermaterai cukup tertanggal"?><? echo $pk_tgl_materai ?><? ;
			}
		}
		else
		{
			echo $cmp_fname.", bertempat tinggal di";?><? echo " ".$pk_kota; ?> <? echo ",";?> <? echo " ".$pk_alamat ?> <? echo ", Rukun Tetangga"; ?><? echo " ".$pk_rt ?><? echo ", Rukun Warga"?><? echo " ".$pk_rw ?> <? echo ", Desa/Kelurahan ";?> <? echo " ".$pk_kelurahan ?> <? echo ",";?><? echo " ".$pk_kecamatan ?><? echo ", Kota ".$cmp_kota.", Provinsi ".$cmp_propinsi.", pemegang Kartu Tanda Penduduk dengan Nomor Induk Kependudukan : ";?><? echo $pk_ktp ?><? echo" dan untuk melakukan tindakan hukum dalam Perjanjian ini tidak memerlukan persetujuan dari pihak manapun sebagaimana ternyata dalam surat pernyataan tanggal "?> <? echo $pk_tgl_materai ?> <? echo"yang dibuat secara dibawah tangan dan bermeterai cukup.";	
		}
		 ?>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">- Untuk selanjutnya disebut "DEBITUR".</td>
	</tr>
	<tr>
		<td style="text-align:justify;" colspan="3">
		BANK dan DEBITUR telah saling setuju untuk membuat, melaksanakan dan mematuhi Perjanjian MEGA UKM ini dengan
		syarat-syarat dan ketentuan-ketentuan sebagai berikut:
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="center">PASAL 1</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%">
				<td width="4%" align="right" valign="top">1.</td>
				<td valign="top">Fasilitas Kredit yang diberikan BANK kepada DEBITUR  adalah : </td>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%">
				<td width="6%" align="right" valign="top">&nbsp;</td>
				<td valign="top">
					<table width="100%" BORDER="1">
						<tr>
							<td align="center">&nbsp;</td>
							<td align="center">Jenis Fasilitas</td>
							<td align="center">Tujuan Penggunaan</td>
							<td align="center">Plafond</td>
						</tr>
						<?
						$totalmkknewadm=0;
						$mkknewadm=0;
						$total_plafond1=0;
						$plafond=0;
						$tkewajiban=0;
						$sql_mkk="select cf.mkknewbungaflag,cf.mkknewadm,cf.custfacseq,cf.custcreditlong,cf.custcreditplafond,kp.produk_type_description, cn.credit_need_name
								from tbl_customerfacility2 cf, Tbl_KodeProduk kp, Tbl_CreditNeed cn 
								where cf.custcredittype=kp.produk_loan_type
								and cf.custcreditneed=cn.credit_need_code
								and cf.custnomid='$custnomid'";
								echo $sql_mkk;
						$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
						$params_mkk = array(&$_POST['query']);
						$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
						if($conn==false){die(FormatErrors(sqlsrv_errors()));}
						if(sqlsrv_has_rows($sqlConn_mkk))
						{
							$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
							while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
							{
							$sequence=$row_mkk['custfacseq'];
							$mkknewbungaflag=$row_mkk['mkknewbungaflag'];
							$mkknewadm=$row_mkk['mkknewadm'];
							$jangkawaktu=$row_mkk['custcreditlong'];
							$plafond=$row_mkk['custcreditplafond'];
							$nama_produk=$row_mkk['produk_type_description'];
							$nama_credit=$row_mkk['credit_need_name'];
							
							$total_plafond1+=$plafond;
							$totalmkknewadm+=$mkknewadm;
							$jangkatahun=0;
							$jangkatahun=$jangkawaktu/12;
							$tplafond_bunga=0;
							$tplafond_bunga_jangkatahun=0;
							$tplafond_bunga_jangkatahun_totalplafond=0;
							
							$tplafond_bunga_jangkatahun_totalplafond_jangkawaktu=0;
							$tplafond_bunga=$plafond*$mkknewbungaflag/100;
							$tplafond_bunga_jangkatahun=$tplafond_bunga*$jangkatahun;
							$tplafond_bunga_jangkatahun_totalplafond=$tplafond_bunga_jangkatahun+$plafond;
							$tplafond_bunga_jangkatahun_totalplafond_jangkawaktu=$tplafond_bunga_jangkatahun_totalplafond/$jangkawaktu;
							$tkewajiban+=$tplafond_bunga_jangkatahun_totalplafond_jangkawaktu;			
							//echo $tplafond_bunga_jangkatahun_totalplafond_jangkawaktu;
							
						?>
						<tr>
							<td><? echo "fasilitas ".$sequence; ?>&nbsp;</td>
							<td align="center"><? echo $nama_produk;?></td>
							<td align="center"><? echo $nama_credit;?></td>
							<td align="right"><? echo numberFormat($plafond);?></td>
						</tr>
						<?
							}
						}
						
						?>
                        
						<tr>
							<td>total</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td align="right"><? echo numberFormat($total_plafond1);?></td>
						</tr>
					</table>
				</td>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%">
				<tr>
					<td width="6%" align="right" valign="top">&nbsp;</td>
					<td valign="top">
						<table width="100%">
							<tr>
								<td valign="top" width="20%">Suku Bunga</td>
								<td valign="top" align="center" width="4%">:</td>
								<td style="text-align:justify;">
									<?echo $mkknewbungaflag;?> %&nbsp;<em>per tahun flat in arrear , metode perhitungan suku bunga  disetarakan dengan perhitungan bunga secara effektif dan
									</em> berlaku  tetap selama Jangka Waktu Fasilitas Kredit dengan tetap memperhatikan ketentuan  dalam pasal 4 Perjanjian MEGA UKM ini. <em>
									Jumlah kewajiban bunga</em> sebagaimana diuraikan dalam jadwal angsuran terlampir yang merupakan satu  kesatuan dengan Perjanjian MEGA UKM ini; 
								</td>
							</tr>
							<tr>
								<td valign="top" >Jangka Waktu</td>
								<td valign="top" align="center">:</td>
								<td style="text-align:justify;">
									<table width="100%" border="1">
									<?
										$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
										$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
										$params_mkk = array(&$_POST['query']);
										$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
										if($conn==false){die(FormatErrors(sqlsrv_errors()));}
										if(sqlsrv_has_rows($sqlConn_mkk))
										{
											$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
											while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
											{
												$sequence=$row_mkk['custfacseq'];	
												$custcreditlong=$row_mkk['custcreditlong'];	
												$mpk_jangka_waktu1=$row_mkk['mpk_jangka_waktu1'];	
												$mpk_jangka_waktu2=$row_mkk['mpk_jangka_waktu2'];
									?>
										<tr>
											<td width="75px">fasilitas<?echo $sequence?></td>
											<td width="50px"><?echo $custcreditlong?> Bln</td>
											<td>Terhitung dari <?echo $mpk_jangka_waktu1;?> sampai <?echo $mpk_jangka_waktu2?></td>
										</tr>
									<?
											}
										}
										sqlsrv_free_stmt( $sqlConn_mkk );
									?>
									</table>
									
								</td>
							</tr>
							<tr>
								<td valign="top" >Angsuran</td>
								<td valign="top" align="center">:</td>
								<td style="text-align:justify;">
									<table width="100%" border="1">
									<?
										$tkewajiban=0;
										$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
										$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
										$params_mkk = array(&$_POST['query']);
										$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
										if($conn==false){die(FormatErrors(sqlsrv_errors()));}
										if(sqlsrv_has_rows($sqlConn_mkk))
										{
											$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
											while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
											{
												$sequence=$row_mkk['custfacseq'];
												$mkknewangsuran=$row_mkk['mkknewangsuran'];	
												$mpk_waktu_angsuran1=$row_mkk['mpk_waktu_angsuran1'];	
												$mpk_waktu_angsuran2=$row_mkk['mpk_waktu_angsuran2'];
												$tkewajiban+=$mkknewangsuran;
											?>
											<tr>
												<td valign="top" width="75px">Fasilitas<?echo $sequence?></td>
												<td valign="top" style="width:120px;" align="right">Rp. <? echo numberFormat($mkknewangsuran)?></td>
												<td>
													Pembayaran di lakukan setiap bulan selambat- lambatnya tanggal <? echo $mpk_waktu_angsuran1?>
													<? echo $templatePK;?> <? echo $mpk_waktu_angsuran2?>
												</td>
											</tr>
											<?
											}
											?>
											<tr>
												<td>total</td>
												<td align="right">Rp. <?echo numberFormat($tkewajiban)?></td>
												<td>&nbsp;</td>
											</tr>
											<?
										}
										sqlsrv_free_stmt( $sqlConn_mkk );
									?>
									</table>
                                       
									<?
                                    if($tujuan_mmu == "NOMMU")
                                    {
                                        $kartuKreditMMU = "";
                                    }
                                    else
                                    {
                                    
                                        $kartuKreditMMU = "</br>
									Kartu Kredit Mega Mitra Usaha : </br>
									Pembayaran Kartu Kredit Mega Mitra usaha di lakukan setiap bulannya minimal sebesar 10%(sepuluh persen)
									dari Total Tagihan atau sebesar Rp. 50.000,- (lima puluh ribu), mana yang lebih tinggi.
									<em>
									billing cycle Kartu Kredit Mega Mitra Usaha adalah tanggal 23 (dua puluh tiga) setiap bulannya,
									dan pembayarab wajib di lakukan maksimal 12 (lima belas) hari sebtlah tanggal billing cycle.
									</em>";
                                    }
                                    ?> 
									<? echo $kartuKreditMMU;?>
								</td>
							</tr>
							<tr>
								<td valign="top" >Biaya Provisi</td>
								<td valign="top" align="center">:</td>
								<td style="text-align:justify;">
									<table width="150px" border="1">
									<?
									$tkewajiban=0;
									$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];	
											$mkknewkewajibanbank=$row_mkk['mkknewkewajibanbank'];	
											$mkknewprovisi=$row_mkk['mkknewprovisi'];	
											$tkewajiban+=$mkknewkewajibanbank;
										?>
										<tr>
											<td width="75">Fasilitas<?echo $sequence?></td>
											<td align="center"><?echo $mkknewprovisi?> %</td>
										</tr>
										<?
										}
										?>
										
										<?
									}
									sqlsrv_free_stmt( $sqlConn_mkk );
									?>
									</table>
									flat dari jumlah fasilitas kredit.
								</td>
							</tr>
							<tr>
								<td valign="top" >Biaya Administrasi</td>
								<td valign="top" align="center">:</td>
								<td style="text-align:justify;">
									<table width="200px" border="1">
									<?
									$tadm=0;
									$sql_mkk="select * from tbl_customerfacility2 where custnomid='$custnomid'";
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];	
											$mkknewkewajibanbank=$row_mkk['mkknewkewajibanbank'];	
											$mkknewadm=$row_mkk['mkknewadm'];	
											$tadm+=$mkknewadm;
										?>
										<tr>
											<td width="75">Fasilitas<?echo $sequence?></td>
											<td align="right">Rp. <?echo numberFormat($mkknewadm)?></td>
										</tr>
										<?
										}
										?>
											<tr>
												<td>total</td>
												<td align="right">Rp. <?echo numberFormat($tadm)?></td>
											</tr>
											<?
									}
									sqlsrv_free_stmt( $sqlConn_mkk );
									?>
									</table>
                                    
                                    <?
                                    if($tujuan_mmu == "NOMMU")
                                    {

                                    }
                                    else
                                    {

									$sql_mkk="select * from ms_mmu";
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$kartu_utama=$row_mkk['mmu_utama'];
											$kartu_tambahan=$row_mkk['mmu_tambahan'];
											$kartu_denda=$row_mkk['mmu_denda'];
										
										}
									}
									sqlsrv_free_stmt( $sqlConn_mkk );
									?>
									Kartu Kredit Mega Mitra Usaha:</br>
									&nbsp;&nbsp;-Kartu Utama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	: Rp. <? echo numberFormat($kartu_utama);?>,-</br>
									&nbsp;&nbsp;-Kartu Tambahan&nbsp;&nbsp;: Rp. <? echo numberFormat($kartu_tambahan);?>,-</br>
									<em>
										Untuk Kartu Utama, Bebas Iuran tahunan untuk 1 (satu) tahun pertama dan bebes iuran tahunan
										untuk tahun selajutnya, apabila pemakaian cash advance tunai dan transfer selama 1 (satu) tahun 
										sebelumnya adalah rata-rata Rp. 2,500,000.-(dua juta lima ratus ribu Rupiah) per bulan atau akumulasi Rp.
										30,000,000.- (tiga puluh juta Rupiah) per tahun
									</em>
                                    <?
									}
									?>
								</td>
							</tr>
							<tr>
								<td valign="top" >Denda Keterlambatan Pembayaran Angsuran</td>
								<td valign="top" align="center">:</td>
								<td valign="top"  style="text-align:justify;">
									1. <? echo $denda_nilai_dkpa?> % per bulan, yang dihitung dari jumlah angsuran yang tertunggak.</br>
                                    <?
                                    if($tujuan_mmu == "NOMMU")
                                    {

                                    }
                                    else
                                    {
									?>
									2. Untuk kartu Kredit Mega Mitra Usaha, sebesar Rp. <?echo $kartu_denda?>,- </br>
									&nbsp;&nbsp;&nbsp;&nbsp;(tujuh puluh lima ribu rupiah)
                                    <?
									}
									?>
								</td>
							</tr>
							<tr>
								<td valign="top" >Denda Pembayaran Dipercepat</td>
								<td valign="top" align="center">:</td>
								<td style="text-align:justify;">
									2 kali angsuran
								</td>
							</tr>
						</table>
					</td>	
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
		<div>PASAL 2</div>
		<div>JAMINAN</div>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;" align="center">
		Untuk menjamin pembayaran kembali sebagaimana mestinya semua jumlah uang yang terhutang dan wajib dibayar oleh DEBITUR kepada BANK berdasarkan Perjanjian MEGA UKM<? echo $titleMMU;?> berikut setiap perubahannya, maka DEBITUR dan / atau pihak ketiga memberikan jaminan (-jaminan)  kepada BANK sebagai berikut  : 
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<table  width="100%">
				<tr>
					<td width="100%">
						<table width="100%" border ="1">
						<?
						
						$add="";
						$col_certtype="";
						$col_certno="";
						$col_certatasnama="";
						$col_name="";
						$col_land_wajar_bld="";
						$col_land_likuidasi_bld="";
						$totalcol_land_likuidasi_bld=0;
						$totalcol_land_wajar_bld=0;
						$seq=0;
						$seqa=0;
						$sql_col_land="select cmc.flagdelete,cmc.col_id,cmc.ap_lisregno,ct.col_code,ct.col_name, cl.col_certtype,cl.col_certno,cl.col_certatasnama,
										cb.col_nilaitotalfisik,cb.col_nilailikuidasifisik,cb.col_addr1
										from tbl_COL_Building cb,tbl_COL_Land cl,Tbl_Cust_MasterCol cmc,TblCollateralType ct
										where cmc.ap_lisregno=cl.ap_lisregno
										and cl.ap_lisregno=cb.ap_lisregno
										and ct.col_code=cmc.cust_jeniscol
										and cl.col_id=cb.col_id
										and cl.col_id=cmc.col_id
										and cmc.flagdelete='0'
										and cl.ap_lisregno='$custnomid'
										and cmc.cust_jeniscol='BA1'";
						$cursortype_col_land = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
						$params_col_land = array(&$_POST['query']);
						$sqlConn_col_land = sqlsrv_query($conn, $sql_col_land, $params_col_land, $cursortype_col_land);
						if($conn==false){die(FormatErrors(sqlsrv_errors()));}
						if(sqlsrv_has_rows($sqlConn_col_land))
						{
							$rowCount_col_land = sqlsrv_num_rows($sqlConn_col_land);
							while($row_col_land = sqlsrv_fetch_array( $sqlConn_col_land, SQLSRV_FETCH_ASSOC))
							{
								$add=$row_col_land['col_addr1'];
								$col_id=$row_col_land['col_id'];
								$col_name=$row_col_land['col_name'];
								$col_certtype=$row_col_land['col_certtype'];
								$col_certno=$row_col_land['col_certno'];
								$col_certatasnama=$row_col_land['col_certatasnama'];
								$col_land_wajar_bld=$row_col_land['col_nilaitotalfisik'];
								$col_land_likuidasi_bld=$row_col_land['col_nilailikuidasifisik'];
								
								$totalcol_land_likuidasi_bld+=$col_land_likuidasi_bld;
								$totalcol_land_wajar_bld+=$col_land_wajar_bld;
								$seq+=1;
								$seqa+=1;
								
								
								
								if($rowCount_col_land != 0)
								{
								$pk_coment="";
								$query_pk_comment="select * from tbl_pk_comment
											where col_id='$col_id' 
											and custnomid='$custnomid'";
								$sql_cursor = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
								$params = array(&$_POST['query']);
								$sqlConn =sqlsrv_query($conn, $query_pk_comment, $params, $sql_cursor);
								if($conn==false){die(FormatErrors(sqlsrv_errors()));}
								if(sqlsrv_has_rows($sqlConn))
								{
									$rowcount = sqlsrv_num_rows($sqlConn);
									while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
									{
									$pk_coment=$row['pk_coment'];
						
									}						
								}
								sqlsrv_free_stmt( $sqlConn ); 
							?>
							<tr>
									<td width="5%"><div align="center">No</div></td>
									<td width="25%"><div align="center">Jenis Jaminan </div></td>
									<td width="40%"><div align="center">Alamat Jaminan</div></td>
									<td width="15%"><div align="center">Bukti Kepemilikan</div></td>
									<td width="15%"><div align="center">Atas Nama</div></td>
							<tr>
							<tr>
								<td><?echo $seq;?></td>
								<td><?echo $col_name;?></td>
								<td><?echo $add;?></td>
								<td><?echo $col_certtype;?> <?echo $col_certno;?>&nbsp;</td>
								<td><?echo $col_certatasnama;  ?></td>
							</tr>
							<tr>
								<td align="left" colspan="5" align="top">
								<?echo $pk_coment?>
								</td>
							</tr>
							<tr>
								<td colspan="5" align="top">&nbsp;
								
								</td>
							</tr>
							<?
								}
							}						
						}
						sqlsrv_free_stmt( $sqlConn_col_land );      
						
						$add="";
						$col_certtype="";
						$col_certno="";
						$col_certatasnama="";
						$col_name="";
						$col_land_wajar_tnk="";
						$col_land_likuidasi_tnk="";
						$totalcol_land_wajar_tnk=0;
						$totalcol_land_likuidasi_tnk=0;
						$seqa=0;
						$sql_col_land="select cmc.col_id,cmc.ap_lisregno,ct.col_code,ct.col_name,cv.col_nilaiwajar,cv.col_nilailikuidasi,
										cv.col_addr1,cv.col_certtype,cv.col_certno,cv.col_certatasnama,cv.COL_NILAIWAJAR,cv.COL_NILAILIKUIDASI
										from tbl_COL_Land cv,Tbl_Cust_MasterCol cmc,TblCollateralType ct
										where cv.col_id=cmc.col_id
										and ct.col_apr_code like '%TNK%'
										and cmc.ap_lisregno=cv.ap_lisregno
										and cmc.col_id like '%TNK%'
										and cmc.flagdelete='0'
										and cv.ap_lisregno='$custnomid'";
						$cursortype_col_land = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
						$params_col_land = array(&$_POST['query']);
						$sqlConn_col_land = sqlsrv_query($conn, $sql_col_land, $params_col_land, $cursortype_col_land);
						if($conn==false){die(FormatErrors(sqlsrv_errors()));}
						if(sqlsrv_has_rows($sqlConn_col_land))
						{
							$rowCount_col_land = sqlsrv_num_rows($sqlConn_col_land);
							while($row_col_land = sqlsrv_fetch_array( $sqlConn_col_land, SQLSRV_FETCH_ASSOC))
							{
								$add=$row_col_land['col_addr1'];
								$col_name=$row_col_land['col_name'];
								$col_certtype=$row_col_land['col_certtype'];
								$col_id=$row_col_land['col_id'];
								$col_certno=$row_col_land['col_certno'];
								$col_certatasnama=$row_col_land['col_certatasnama'];
								$col_land_wajar_tnk=$row_col_land['COL_NILAIWAJAR'];
								$col_land_likuidasi_tnk=$row_col_land['COL_NILAILIKUIDASI'];
								$totalcol_land_wajar_tnk+=$col_land_wajar_tnk;
								$totalcol_land_likuidasi_tnk+=$col_land_likuidasi_tnk;
								$seq+=1;
								$seqa+=1;
								if($rowCount_col_land != 0)
								{
								$pk_coment="";
								$query_pk_comment="select * from tbl_pk_comment
											where col_id='$col_id' 
											and custnomid='$custnomid'";
								$sql_cursor = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
								$params = array(&$_POST['query']);
								$sqlConn =sqlsrv_query($conn, $query_pk_comment, $params, $sql_cursor);
								if($conn==false){die(FormatErrors(sqlsrv_errors()));}
								if(sqlsrv_has_rows($sqlConn))
								{
									$rowcount = sqlsrv_num_rows($sqlConn);
									while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
									{
									$pk_coment=$row['pk_coment'];
						
									}						
								}
								sqlsrv_free_stmt( $sqlConn ); 
								
						?>
							<tr>
								<td width="5%"><div align="center">No</div></td>
								<td width="25%"><div align="center">Jenis Jaminan </div></td>
								<td width="40%"><div align="center">Alamat Jaminan</div></td>
								<td width="15%"><div align="center">Bukti Kepemilikan</div></td>
								<td width="15%"><div align="center">Atas Nama</div></td>
							<tr>
							<tr>
								<td><?echo $seq;?></td>
								<td><?echo $col_name;?></td>
								<td><?echo $add;?></td>
								<td><?echo $col_certtype;?> <?echo $col_certno;?>&nbsp;</td>
								<td><?echo $col_certatasnama;  ?></td>
							</tr>
							<tr>
								<td align="left" colspan="5" align="top">
								<?echo $pk_coment?>
								</td>
							</tr>
							<tr>
								<td colspan="5" align="top">&nbsp;
								
								</td>
							</tr>
						<?
								}
							}						
						}
						sqlsrv_free_stmt( $sqlConn_col_land );
						
						
					
					
						$add_ken="";
						$col_certatasnamaken="";
						$col_valueken="";
						$totalcol_valueken=0;
						$col_name="";
						$col_nilailikuidasiken="";
						$totalcol_nilailikuidasiken=0;
						$seqa=0;
						$sql_col_vehicle="select cmc.col_id,cmc.ap_lisregno,ct.col_code,ct.col_name,cv.col_nilaiwajar,cv.col_nilailikuidasi,
									cv.col_addr1,cv.col_bpkbnama
									from tbl_COL_Vehicle cv,Tbl_Cust_MasterCol cmc,TblCollateralType ct
									where cv.col_id=cmc.col_id
									and ct.col_apr_code like '%vhc%'
									and cmc.ap_lisregno=cv.ap_lisregno
									and cmc.col_id like '%vhc%'
									and cmc.flagdelete='0'
									and cv.ap_lisregno='$custnomid'";
						$cursortype_col_vehicle = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
						$params_col_vehicle = array(&$_POST['query']);
						$sqlConn_col_vehicle = sqlsrv_query($conn, $sql_col_vehicle, $params_col_vehicle, $cursortype_col_vehicle);
						if($conn==false){die(FormatErrors(sqlsrv_errors()));}
						if(sqlsrv_has_rows($sqlConn_col_vehicle))
						{
							$rowCount_col_vehicle = sqlsrv_num_rows($sqlConn_col_vehicle);
							while($row_col_vehicle = sqlsrv_fetch_array( $sqlConn_col_vehicle, SQLSRV_FETCH_ASSOC))
							{
							$add_ken=$row_col_vehicle['col_addr1'];
							$col_id=$row_col_vehicle['col_id'];
							$col_name=$row_col_vehicle['col_name'];
							$col_certatasnamaken=$row_col_vehicle['col_bpkbnama'];
							$col_valueken=$row_col_vehicle['col_nilaiwajar'];
							$col_nilailikuidasiken=$row_col_vehicle['col_nilailikuidasi'];
							$totalcol_valueken+=$col_valueken;
							$totalcol_nilailikuidasiken+=$col_nilailikuidasiken;
							$seqa+=1;
							$seq+=1;
							if($rowCount_col_vehicle!=0)
								{
								
								$pk_coment="";
								$query_pk_comment="select * from tbl_pk_comment
											where col_id='$col_id' 
											and custnomid='$custnomid'";
								$sql_cursor = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
								$params = array(&$_POST['query']);
								$sqlConn =sqlsrv_query($conn, $query_pk_comment, $params, $sql_cursor);
								if($conn==false){die(FormatErrors(sqlsrv_errors()));}
								if(sqlsrv_has_rows($sqlConn))
								{
									$rowcount = sqlsrv_num_rows($sqlConn);
									while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
									{
									$pk_coment=$row['pk_coment'];
									}						
								}
								sqlsrv_free_stmt( $sqlConn ); 
								
						?>
						<tr>
							<td width="5%"><div align="center">No</div></td>
							<td width="25%"><div align="center">Jenis Jaminan </div></td>
							<td width="40%"><div align="center">Alamat Jaminan</div></td>
							<td width="15%"><div align="center">Bukti Kepemilikan</div></td>
							<td width="15%"><div align="center">Atas Nama</div></td>
						<tr>
						<tr>
							<td><?echo $seq;?></td>
							<td><?echo $col_name?></td>
							<td><? echo $add_ken?></td>
							<td>BPKB</td>
							<td><?echo $col_certatasnamaken?></td>
						</tr>
						<tr>
							<td align="left" colspan="5" align="top">
							<?echo $pk_coment?>
							</td>
						</tr>
						<tr>
							<td colspan="5" align="top">&nbsp;
							
							</td>
						</tr>
						<?
								}
							}						
						}
						sqlsrv_free_stmt( $sqlConn_col_vehicle );   
					
					
						$cash_alamat1="";
						$col_name="";
						$cash_noaccount="";
						$cash_nilai="";
						$totalcash_nilai="";
						$col_name="";
						$cash_atasnama="";
						$seqa=0;
						$sql_col_vehicle="select ct.col_name,cc.cash_alamat1,cc.cash_noaccount,cc.cash_nilai,cc.cash_atasnama,cmc.flagdelete,cmc.col_id,cmc.ap_lisregno 
										from Tbl_Cust_MasterCol cmc, tbl_COL_Cash cc,TblCollateralType ct
										where cc.col_id = cmc.col_id
										and cmc.col_id like '%D01%'
										and cmc.flagdelete like '0'
										and cmc.ap_lisregno=cc.ap_lisregno
										and cmc.cust_jeniscol=ct.col_apr_code
										and cc.ap_lisregno='$custnomid'";
						$cursortype_col_vehicle = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
						$params_col_vehicle = array(&$_POST['query']);
						$sqlConn_col_vehicle = sqlsrv_query($conn, $sql_col_vehicle, $params_col_vehicle, $cursortype_col_vehicle);
						if($conn==false){die(FormatErrors(sqlsrv_errors()));}
						if(sqlsrv_has_rows($sqlConn_col_vehicle))
						{
							$rowCount_col_vehicle = sqlsrv_num_rows($sqlConn_col_vehicle);
							while($row_col_vehicle = sqlsrv_fetch_array( $sqlConn_col_vehicle, SQLSRV_FETCH_ASSOC))
							{
							$cash_alamat1=$row_col_vehicle['cash_alamat1'];
							$col_id=$row_col_vehicle['col_id'];
							$col_name=$row_col_vehicle['col_name'];
							$cash_noaccount=$row_col_vehicle['cash_noaccount'];
							$cash_nilai=$row_col_vehicle['cash_nilai'];
							$totalcash_nilai+=$cash_nilai;
							$seq+=1;
							$seqa+=1;
							$cash_atasnama=$row_col_vehicle['cash_atasnama'];
							if($rowCount_col_vehicle!=0)
								{
								$pk_coment="";
								$query_pk_comment="select * from tbl_pk_comment
											where col_id='$col_id' 
											and custnomid='$custnomid'";
								$sql_cursor = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
								$params = array(&$_POST['query']);
								$sqlConn =sqlsrv_query($conn, $query_pk_comment, $params, $sql_cursor);
								if($conn==false){die(FormatErrors(sqlsrv_errors()));}
								if(sqlsrv_has_rows($sqlConn))
								{
									$rowcount = sqlsrv_num_rows($sqlConn);
									while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
									{
									$pk_coment=$row['pk_coment'];
						
									}						
								}
								sqlsrv_free_stmt( $sqlConn ); 
							?>
						<tr>
							<td width="5%"><div align="center">No</div></td>
							<td width="25%"><div align="center">Jenis Jaminan </div></td>
							<td width="40%"><div align="center">Alamat Jaminan</div></td>
							<td width="15%"><div align="center">Bukti Kepemilikan</div></td>
							<td width="15%"><div align="center">Atas Nama</div></td>
						<tr>
						<tr>
							<td><?echo $seq;?></td>
							<td><?echo $col_name?></td>
							<td><? echo $cash_alamat1?></td>
							<td><?echo $cash_noaccount?></td>
							<td><?echo $cash_atasnama?></td>
						</tr>
						<tr>
							<td align="left" colspan="5" align="top">
							<? echo $pk_coment;?>
							</td>
						</tr>
						<tr>
							<td colspan="5" align="top">&nbsp;
							
							</td>
						</tr>
						<?
								}
							}						
						}
						sqlsrv_free_stmt( $sqlConn_col_vehicle );   
						
						
							$cash_alamat1="";
							$col_name="";
							$cash_noaccount="";
							$cash_nilai_tab="";
							$totalcash_nilai_tab="";
							$col_name="";
							$cash_atasnama="";
							$seqa=0;
							$sql_col_vehicle="select ct.col_name,cc.cash_alamat1,cc.cash_noaccount,cc.cash_nilai,cc.cash_atasnama,cmc.flagdelete,cmc.col_id,cmc.ap_lisregno 
												from Tbl_Cust_MasterCol cmc, tbl_COL_Cash cc,TblCollateralType ct
												where cc.col_id = cmc.col_id
												and cmc.col_id like '%TAB%'
												and cmc.flagdelete like '0'
												and cmc.ap_lisregno=cc.ap_lisregno
												and cmc.cust_jeniscol=ct.col_apr_code
												and cc.ap_lisregno='$custnomid'";
							$cursortype_col_vehicle = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
							$params_col_vehicle = array(&$_POST['query']);
							$sqlConn_col_vehicle = sqlsrv_query($conn, $sql_col_vehicle, $params_col_vehicle, $cursortype_col_vehicle);
							if($conn==false){die(FormatErrors(sqlsrv_errors()));}
							if(sqlsrv_has_rows($sqlConn_col_vehicle))
							{
								$rowCount_col_vehicle = sqlsrv_num_rows($sqlConn_col_vehicle);
								while($row_col_vehicle = sqlsrv_fetch_array( $sqlConn_col_vehicle, SQLSRV_FETCH_ASSOC))
								{
								$cash_alamat1=$row_col_vehicle['cash_alamat1'];
								$col_id=$row_col_vehicle['col_id'];
								$col_name=$row_col_vehicle['col_name'];
								$cash_noaccount=$row_col_vehicle['cash_noaccount'];
								$cash_nilai_tab=$row_col_vehicle['cash_nilai'];
								$totalcash_nilai_tab+=$cash_nilai_tab;
								$cash_atasnama=$row_col_vehicle['cash_atasnama'];
								$seq+=1;
								$seqa+=1;
								if($rowCount_col_vehicle!=0)
									{
									$pk_coment="";
								$query_pk_comment="select * from tbl_pk_comment
											where col_id='$col_id' 
											and custnomid='$custnomid'";
								$sql_cursor = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
								$params = array(&$_POST['query']);
								$sqlConn =sqlsrv_query($conn, $query_pk_comment, $params, $sql_cursor);
								if($conn==false){die(FormatErrors(sqlsrv_errors()));}
								if(sqlsrv_has_rows($sqlConn))
								{
									$rowcount = sqlsrv_num_rows($sqlConn);
									while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
									{
									$pk_coment=$row['pk_coment'];
						
									}						
								}
								sqlsrv_free_stmt( $sqlConn ); 
								
							?>
							<tr>
								<td width="5%"><div align="center">No</div></td>
								<td width="25%"><div align="center">Jenis Jaminan </div></td>
								<td width="40%"><div align="center">Alamat Jaminan</div></td>
								<td width="15%"><div align="center">Bukti Kepemilikan</div></td>
								<td width="15%"><div align="center">Atas Nama</div></td>
							<tr>
							<tr>
								<td><?echo $seq;?></td>
								<td><?echo $col_name?></td>
								<td><? echo $cash_alamat1?></td>
								<td><?echo $cash_noaccount?></td>
								<td><?echo $cash_atasnama?></td>
							</tr>
							<tr>
								<td align="left" colspan="5" align="top">
								<?echo $pk_coment?>
								</td>
							</tr>
							<tr>
								<td colspan="5" align="top">&nbsp;
								
								</td>
							</tr>
							<?
									}
								}						
							}
							sqlsrv_free_stmt( $sqlConn_col_vehicle ); 						
					
							?>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="center">PASAL 3</div>
			<div align="center">KONDISI TERTENTU</div> 
		<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">
		Dalam kondisi tertentu dimana tingkat suku bunga perbankan pada umumnya mengalami kenaikan diluar batas kewajaran, maka BANK atas pertimbangannya sendiri berhak untuk menyesuaikan tingkat suku bunga yang berlaku, perubahan mana akan diberitahukan secara tertulis kepada DEBITUR, dan pemberitahuan mana mengikat DEBITUR.
		<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="center">PASAL 4</div>
			<div align="center">PERSETUJUAN TUNDUK PADA</div> 
			<div align="center">SYARAT DAN KETENTUAN UMUM MEGA USAHA KECIL MENENGAH (SKU MEGA UKM)</div> 
		<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">
		Debitur dengan ini menyatakan telah membaca, mengerti, memahami, dengan jelas dan menerima baik serta mengikat diri untuk tunduk terhadap seluruh syarat-syarat dan ketentuan umum sebagaimana tertunag dalam Lampiran Perjanjian Kredit Fasilitas Pembiayaan Mega Usaha Kecil Menengah, lampiran mana merupakan satu kesatuan yang tidak terpisahkan dengan Perjanjian MEGA UKM.
		<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="center">PASAL 5</div>
			<div align="center">KETENTUAN LAIN-LAIN</div> 
		<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;"><? echo $pk_narasipasal5?><td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="center">PASAL 6</div>
			<div align="center">KORESPONDENSI </div> 
		<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">
			Semua surat menyurat atau pemberitahuan yang perlu dikirim oleh masing-masing pihak kepada pihak yang lain mengenai atau sehubungan dengan Perjanjian MEGA UKM ini harus dilakukan secara langsung dengan surat tercatat atau melalui ekspedisi facsimile, kawat atau telex ke alamat-alamat dibawah ini :
		<td>
	</tr>
	<tr>
		<td colspan="3">1. BANK<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;" align="center">
			<table width="100%">
				<tr>
					<td colspan="2" valign="top">BANK MEGA KANTOR CABANG <? echo $branch_name ?></td>
				</tr>
				<tr>
					<td colspan="2" valign="top"><? echo $branch_addr;?></td>
				</tr>
				<tr>
					<td colspan="2" valign="top"><? echo $branch_city ?></td>
				</tr>
				<tr>
					<td width="20%">No Telepon</td>
					<td ><? echo $pk_bank_telp ?></td>
				</tr>
				<tr>
					<td>No FAX</td>
					<td><? echo $pk_bank_fax ?></td>
				</tr>
			</table>
		</td>	
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">2. Debitur<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;" align="center">
			<table width="100%">
				<tr>
					<td colspan="2" valign="top"><? if($cmpsex!="0") {echo $cmpfullname;}else{echo $cmpnamausaha;} ?></td>
				</tr>
				<tr>
					<td	colspan="1"><?echo $alamat_pk?>
					</td>
				</tr>
				<tr>
					<td colspan="2" valign="top"><? echo $cmp_kota ?></td>
				</tr>
				<tr>
					<td width="20%">No Telepon 1</td>
					<td><? echo $cmp_no_telp ?></td>
				</tr>
				<tr>
					<td width="20%">No Telepon 2</td>
					<td><? echo $pk_telepon ?></td>
				</tr>
				<tr>
					<td width="10%">No HP</td>
					<td ><? echo $cmp_no_hp?></td>
				</tr>
				<tr>
					<td width="10%">NO FAX</td>
					<td ><? echo $pk_debitur_fax ?></td>
				</tr>
			</table>
		</td>	
	</tr>
	<tr>
		<td colspan="3">Setiap perubahan dari data dan  alamat tersebut di atas wajib diberitahukan secara tertulis oleh pihak yang  mengalami perubahan kepada pihak lainnya.<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">
			<div align="center">PASAL 7</div>
			<div align="center">DOMISILI HUKUM</div> 
		<td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">
		Kecuali ditetapkan lain dalam Perjanjian MEGA UKM<? echo $titleMMU;?>, maka kedua belah pihak memilih tempat kedudukan hukum yang tetap dan seumumnya di <?echo $nama_pengadilan?> , namun, tidak mengurangi hak dan wewenang BANK untuk memohon pelaksanaan (eksekusi) atau mengajukan tuntutan/gugatan hukum terhadap DEBITUR berdasarkan Ketentuan Umum ini dimuka pengadilan lain dalam wilayah Republik Indonesia
		<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">Demikian setelah ketentuan-ketentuan ini dibaca dan dipelajari dengan seksama oleh DEBITUR dan isinya telah dimengerti oleh DEBITUR dan dengan penuh kesadaran dan tanggung jawab, tanpa ada unsur paksaan dan tekanan dari pihak manapun menandatangani Perjanjian MEGA UKM ini pada tanggal dan tahun sebagaimana tersebut diatas<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;<td>
	</tr>
	<tr>
		<td colspan="3">
			<table align="center" width="800" border="0">
					<tr>
					<td width="200" align="center"> BANK</td>
					<td width="200">&nbsp;</td>
					<td align="center" width="200">DEBITUR</td>
					<td align="center" width="200">Menyetujui : </td>
				</tr>
				<tr>
					<td align="center">PT. Bank Mega, Tbk. </td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="center">MATERAI</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><?echo $pk_nama_jabatan?></td>
					<td align="center"><? echo $pk_nama_jabatan1 ?>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><?echo $pk_jabatan?></td>
					<td align="center"><?echo $pk_jabatan1?></td>
					<td align="center"><? echo $cmp_fname; ?></td>
					<td align="center"><? 	if($cmp_mar==null){echo "&nbsp;";}else {echo $cmp_mar;}?></td>
				</tr>
			</table>
		<td>
	</tr>
</table>
</form>
<form id="frm" name="frm" method="post">
<div style="text-align:center; border:0px solid black;">
	<?
		require ("../../requirepage/btnview.php");
		require ("../../requirepage/hiddenfield.php");
	?>	
</div>
</form>
</body>
</html>

<? require('../../lib/close_con.php'); ?>