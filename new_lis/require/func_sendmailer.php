<?php
function sendmail($to, $alias_to, $info = array())
{
	require_once("/PHPMailer/class.phpmailer.php");
	require_once("/PHPMailer/class.smtp.php");

	$mail = new PHPMailer();

	$mail->IsSMTP();
	$mail->Host = "smtp.starnet.net.id";
	$mail->SMTPAuth = false;
	$mail->Username = "elos@banksumselbabel.com";
	$mail->Password = "AAAAAAAAAAAAAAAA";

	$mail->Port = 25;

	//$mail->SMTPSecure = "tls";
	//$mail->Port = 587;
	//$mail->SMTPSecure = "ssl";
	//$mail->Port = 465;

	$mail->SMTPDebug = 0;


	$mail->From = "elos@banksumselbabel.com";
	$mail->FromName = "ELOS BANK SUMSEL BABEL";
	$mail->AddAddress($to, $alias_to);
	$mail->AddReplyTo("elos@banksumselbabel.com", "ELOS BANK SUMSEL BABEL");

	$mail->IsHTML(true);

	$message = '<html><body>';
	$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	$message .= "<tr style='background: #eee;'><td width='20%'><strong>Nama Cabang:</strong> </td><td width='80%'>" . $info['branch'] . "</td></tr>";
	$message .= "<tr><td><strong>No Aplikasi:</strong> </td><td>" . $info['app_id'] . "</td></tr>";
	$message .= "<tr><td><strong>Nama Pemohon:</strong> </td><td>" . $info['name'] . "</td></tr>";
	$message .= "<tr><td><strong>Jenis Kredit:</strong> </td><td>" . $info['type'] . "</td></tr>";
	$message .= "<tr><td><strong>Produk Kredit:</strong> </td><td>" . $info['product'] . "</td></tr>";
	$message .= "<tr><td><strong>User Pengirim:</strong> </td><td>" . $info['sender_id'] . "</td></tr>";
	$message .= "<tr><td><strong>Flow Sebelumnya:</strong> </td><td>" . $info['prev_flow'] . "</td></tr>";
	$message .= "<tr><td><strong>Flow Saat Ini:</strong> </td><td>" . $info['flow'] . " - " . $info['stat'] . "</td></tr>";
	$message .= "<tr><td><strong>URL to ELOS:</strong> </td><td><a href='http://172.17.115.10/lis_bsb' target='blank'>ELOS KONSUMTIF</a></td></tr>";
	$message .= '</body></html>';

	$mail->Subject = "NOTIFICATION : " . $info['flow'] . " " . $info['stat'] . " " . $info['app_id'] . " " . $info['name'];

	$mail->Body    = $message;

	$return['DAT'] = date('Y-m-d H:i:s');
	$return['UID'] = md5(microtime());
	$return['EML'] = $to;

	if(!$mail->Send())
	{
		$return['STS'] = "FAILED";
		$return['MSG'] = "MESSAGE COULD NOT BE SENT";
		$return['ERR'] = $mail->ErrorInfo;
	   
	}
	else 
	{
		$return['STS'] = "SUCCESS";
		$return['MSG'] = "MESSAGE HAS BEEN SENT";
		$return['ERR'] = "NULL";
	}

	$date = date('Y-m-d H:i:s');
	$file = "../../LOG/MAIL" . date('Ymd') . ".LOG";
		
	file_put_contents($file, $date . "\t\t" . $return['UID'] . "\t\t" . $to ."\t\t" . $return['STS'] . "\t\t" . $return['MSG'] . "\t\t" . $return['ERR'] . "\r\n" , FILE_APPEND);

	return $return;

}


?>