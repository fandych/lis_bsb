<?

	// SAVE FLOW
	include ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_REQUEST['userid'];
	$userpwd=$_REQUEST['userpwd'];
	$userbranch=$_REQUEST['userbranch'];
	$userregion=$_REQUEST['userregion'];
	$userwfid=$_REQUEST['userwfid'];
	$userpermission=$_REQUEST['userpermission'];
	$custnomid=$_REQUEST['custnomid'];

	  $originalbranch = $userbranch;
	  $originalregion = $userregion;
	  if ($userwfid != "NOM")
	  {
			$tsql = "SELECT txn_branch_code,txn_region_code FROM Tbl_FSTART
							WHERE txn_id='$custnomid'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
		
			if($sqlConn === false)
			{
				die(FormatErrors(sqlsrv_errors()));
			}
		
			if(sqlsrv_has_rows($sqlConn))
			{
		  $rowCount = sqlsrv_num_rows($sqlConn);
		  while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		  {
				$originalbranch = $row[0];
				$originalregion = $row[1];
		  }
		}
		sqlsrv_free_stmt( $sqlConn );
	  }

	   if ($userpermission == "I")
	   {
		  $wfaction = "";
		  $tsql = "SELECT wf_action as b FROM Tbl_Workflow
							 WHERE wf_id='$userwfid'";
		  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		  $params = array(&$_POST['query']);

		  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		  if ( $sqlConn === false)
			 die( FormatErrors( sqlsrv_errors() ) );

		  if(sqlsrv_has_rows($sqlConn))
		  {
			 $rowCount = sqlsrv_num_rows($sqlConn);
			 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			 {
				$wfaction = $row['b'];
			 }
		  }
		  sqlsrv_free_stmt( $sqlConn );

				if ($wfaction == "I")
				{
					  $userpermission = "A";
			  }
				if ($wfaction == "IA")
				{
					  $userpermission = "C";
			  }

		  $tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid
						WHERE txn_id='$custnomid'
						AND txn_action='$userpermission'";
		  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		  $params = array(&$_POST['query']);

		  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		  if ( $sqlConn === false)
			 die( FormatErrors( sqlsrv_errors() ) );

		  if(sqlsrv_has_rows($sqlConn))
		  {
			 $rowCount = sqlsrv_num_rows($sqlConn);
			 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			 {
				$rowcount = $row['b'];
			 }
		  }
		  sqlsrv_free_stmt( $sqlConn );
	   
		  if ($rowcount <= 0)
		  {
			 $tsql = "INSERT INTO Tbl_F$userwfid
				   VALUES('$custnomid','$userpermission',getdate(),'$userid','',
						 '$originalbranch','$originalregion')";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);

			 $tsql = "INSERT INTO Tbl_Txn_History
				   VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid',
						 '$originalbranch','$originalregion')";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);
		  }
	   }

	   if ($userpermission == "C")
	   {
		  $wfaction = "";
		  $tsql = "SELECT wf_action as b FROM Tbl_Workflow
							 WHERE wf_id='$userwfid'";
		  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		  $params = array(&$_POST['query']);

		  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		  if ( $sqlConn === false)
			 die( FormatErrors( sqlsrv_errors() ) );

		  if(sqlsrv_has_rows($sqlConn))
		  {
			 $rowCount = sqlsrv_num_rows($sqlConn);
			 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			 {
				$wfaction = $row['b'];
			 }
		  }
		  sqlsrv_free_stmt( $sqlConn );

				if ($wfaction == "IC")
				{
					  $userpermission = "A";
			  }

		  $tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid
						WHERE txn_id='$custnomid'
						AND txn_action='I'";
		  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		  $params = array(&$_POST['query']);

		  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		  if ( $sqlConn === false)
			 die( FormatErrors( sqlsrv_errors() ) );

		  if(sqlsrv_has_rows($sqlConn))
		  {
			 $rowCount = sqlsrv_num_rows($sqlConn);
			 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			 {
				$rowcount = $row['b'];
			 }
		  }
		  sqlsrv_free_stmt( $sqlConn );
	   
		  if ($rowcount > 0)
		  {
			 $tsql = "UPDATE Tbl_F$userwfid
								set txn_action='$userpermission',
								txn_time=getdate(),
								txn_user_id='$userid'
					  WHERE txn_id='$custnomid'";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);

			 $tsql = "INSERT INTO Tbl_Txn_History
				   VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid',
						 '$originalbranch','$originalregion')";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);
		  }
	   }

	   if ($userpermission == "A")
	   {
		  $approvepermission=$_POST['approvepermission'];
	// APPROVE
		  $tsql = "SELECT COUNT(*) as b FROM Tbl_F$userwfid
						WHERE txn_id='$custnomid'
						AND (txn_action='C' or txn_action='I')";
		  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		  $params = array(&$_POST['query']);

		  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		  if ( $sqlConn === false)
			 die( FormatErrors( sqlsrv_errors() ) );

		  if(sqlsrv_has_rows($sqlConn))
		  {
			 $rowCount = sqlsrv_num_rows($sqlConn);
			 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
			 {
				$rowcount = $row['b'];
			 }
		  }
		  sqlsrv_free_stmt( $sqlConn );
	   
		  if ($rowcount > 0)
		  {
			 $tsql = "UPDATE Tbl_F$userwfid
								set txn_action='$userpermission',
								txn_time=getdate(),
								txn_user_id='$userid'
					  WHERE txn_id='$custnomid'";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);

			 $tsql = "INSERT INTO Tbl_Txn_History
				   VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid',
						 '$originalbranch','$originalregion')";

			 $params = array(&$_POST['query']);

			 $stmt = sqlsrv_prepare( $conn, $tsql, $params);
			 if( $stmt )
			 {
			 } 
			 else
			 {
				echo "Error in preparing statement.\n";
				die( print_r( sqlsrv_errors(), true));
			 }

			 if( sqlsrv_execute( $stmt))
			 {
			 }
			 else
			 {
			   echo "Error in executing statement.\n";
			   die( print_r( sqlsrv_errors(), true));
			 }
			 sqlsrv_free_stmt( $stmt);
		  }
		}
		Header("Location:flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
	
	require ("../../lib/close_con.php");
	
?>

