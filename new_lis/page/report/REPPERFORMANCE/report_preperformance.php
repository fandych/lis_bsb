<?php
	
	include ("../../../lib/formatError.php");
	require ("../../../lib/open_con.php");
	//require ("../../../lib/open_con_apr.php");
	error_reporting(0);
	
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VIEW PERFORMACE</title>
<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../../js/full_function.js"></script>
<link href="../../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../../js/jquery-1.7.2.min.js"></script>

<style type="text/css" media="print">
	.NonPrintable
	{
	  display: none;
	}
</style>

<style type="text/css" media="print">
	.break{
	page-break-before: always;
	}
</style>

<script type="text/javascript" >	
	function reg(){
		var nan="REG";
		var value=$("#reg").val();
		
		$.ajax({
			type: "POST",
			url: "ajaxPerf.php",
			data: "nan="+nan+"&value="+value+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				var pecahString = response.split("|");
				$("#kec1").html(pecahString[0]);
				$("#kec2").html(pecahString[1]);
				$("#kec3").html(pecahString[2]);
				$("#but").html(pecahString[3]);
			}
		});
	
	}
	
	function sub()
	{
		var value=$("#brc").val();
		
		if(value=="")
		{
			alert("Please fill the branch");
		}
		else
		{
			window.location.href="./report_performance.php?userid=<?=$userid?>&userpwd=<?=$userpwd?>&userbranch="+value+"&userregion=<?=$userregion?>";
		}
	}
</script>
</head>

<BODY>
<script language="Javascript">
				name="utama";
</script>

<div align=center valign=top> <strong>REPORT PERFORMANCE</strong></div>
				
</br></br>

<table width=800px border=0 style="border:0px solid black;" cellpadding="5" cellspacing="0" align=center valign=top >

<tr>
	<td class="text" width="25%">
		Region
	</td>
	<td class="text" width="5%">
		:
	</td>
	<td class="text">
		<select id="reg" name="reg" class="value" onchange="reg()">
		<?
			echo"<option value='' selected='selected'>- Select -</option>";
			$kodekabupaten = "";
			$namakabupaten = "";
			$tsql = "SELECT * FROM Tbl_Region";
			$a = sqlsrv_query($conn, $tsql);
			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$kodekabupaten = $row['region_code'];
					$namakabupaten = $row['region_name'];
					echo "<option value='$kodekabupaten' >$namakabupaten</option>";
					echo "<br>";
				}
			}
		?>
		</select>
	</td>
</tr>
<tr>
	<td id="kec1" class="text" width="25%">
		
	</td>
	<td id="kec2" class="text" width="5%">
		
	</td>
	<td id="kec3" class="text">
		
	</td>
</tr>

<tr>
	<td colspan="3" align="center" id="but"></td>
</tr>

</BODY>
</html>
<?
   	require("../../../lib/close_con.php");
?>