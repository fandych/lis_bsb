<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");
	require ("../../requirepage/parameter.php");

	$custsex="";
	$strsql="select * from Tbl_CustomerMasterPerson2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custsex=$rows['custsex'];
		}
	}
	if ($custsex=="0")
	{
		$tmpwhere="where status_code like '%B%'";
	}
	else
	{
		$tmpwhere="where status_code like '%P%'";
	}
	
	$usaha_tujuanmodalkerja="";
	$usaha_lamapersediaan="";
	$usaha_lamapiutang="";
	$usaha_lamahutang="";
	$usaha_sektorindustri="";
	$usaha_jenisusaha="";
	$usaha_lamausahamonth="";
	$usaha_lamausahayear="";
	$usaha_kepemilikanusaha="";
	$usaha_jumlahpenjualan="";
	$usaha_carabayarpiutang="";
	$usaha_jumlahpenjualandate="";
	$usaha_ratajumlahpembeli="";
	$usaha_ratapembelian="";
	$usaha_kenaikanjumlahpembeli="";
	$usaha_kenaikanticketsize="";
	$usaha_carabayarhutang="";
	$usaha_hargapokokpenjualanpersen="";
	$usaha_persentasemarginusaha="";
	$usaha_jumlahkaryawan="";
	$strsql="select * from Tbl_LKCDUsaha where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
	
		$usaha_tujuanmodalkerja=$rows['usaha_tujuanmodalkerja'];
		$usaha_lamapersediaan=$rows['usaha_lamapersediaan'];
		$usaha_lamapiutang=$rows['usaha_lamapiutang'];
		$usaha_lamahutang=$rows['usaha_lamahutang'];
		$usaha_sektorindustri=$rows['usaha_sektorindustri'];
		$usaha_jenisusaha=$rows['usaha_jenisusaha'];
		$usaha_carabayarhutang=$rows['usaha_carabayarhutang'];
		$usaha_lamausahamonth=$rows['usaha_lamausahamonth'];
		$usaha_lamausahayear=$rows['usaha_lamausahayear'];
		$usaha_kepemilikanusaha=$rows['usaha_kepemilikanusaha'];
		$usaha_carabayarpiutang=$rows['usaha_carabayarpiutang'];
		$usaha_jumlahpenjualan=$rows['usaha_jumlahpenjualan'];
		$usaha_jumlahpenjualandate=$rows['usaha_jumlahpenjualandate'];
		$usaha_ratajumlahpembeli=$rows['usaha_ratajumlahpembeli'];
		$usaha_ratapembelian=$rows['usaha_ratapembelian'];
		$usaha_kenaikanjumlahpembeli=$rows['usaha_kenaikanjumlahpembeli'];
		$usaha_kenaikanticketsize=$rows['usaha_kenaikanticketsize'];
		$usaha_hargapokokpenjualanpersen=$rows['usaha_hargapokokpenjualanpersen'];
		$usaha_persentasemarginusaha=$rows['usaha_persentasemarginusaha'];
		$usaha_jumlahkaryawan=$rows['usaha_jumlahkaryawan'];
		}
	}
	
	$newusaha_tujuanmodalkerja="";
	$newusaha_lamapersediaan="";
	$newusaha_lamapiutang="";
	$newusaha_lamahutang="";
	$newusaha_sektorindustri="";
	$newusaha_jenisusaha="";
	$newusaha_lamausahamonth="";
	$newusaha_lamausahayear="";
	$newusaha_carabayarpiutang="";
	$newusaha_kepemilikanusaha="";
	$newusaha_jumlahpenjualan="";
	$newusaha_jumlahpenjualandate="";
	$newusaha_ratajumlahpembeli="";
	$newusaha_ratapembelian="";
	$newusaha_kenaikanjumlahpembeli="";
	$newusaha_kenaikanticketsize="";
	$newusaha_carabayarhutang="";
	$newusaha_hargapokokpenjualanpersen="";
	$newusaha_persentasemarginusaha="";
	$newusaha_jumlahkaryawan="";
	$strsql="select * from Tbl_LKCDUsaha2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
		$newusaha_tujuanmodalkerja=$rows['usaha_tujuanmodalkerja'];
		$newusaha_lamapersediaan=$rows['usaha_lamapersediaan'];
		$newusaha_lamapiutang=$rows['usaha_lamapiutang'];
		$newusaha_lamahutang=$rows['usaha_lamahutang'];
		$newusaha_carabayarpiutang=$rows['usaha_carabayarpiutang'];
		$newusaha_sektorindustri=$rows['usaha_sektorindustri'];
		$newusaha_carabayarhutang=$rows['usaha_carabayarhutang'];
		$newusaha_jenisusaha=$rows['usaha_jenisusaha'];
		$newusaha_lamausahamonth=$rows['usaha_lamausahamonth'];
		$newusaha_lamausahayear=$rows['usaha_lamausahayear'];
		$newusaha_kepemilikanusaha=$rows['usaha_kepemilikanusaha'];
		$newusaha_jumlahpenjualan=$rows['usaha_jumlahpenjualan'];
		$newusaha_jumlahpenjualandate=$rows['usaha_jumlahpenjualandate'];
		$newusaha_ratajumlahpembeli=$rows['usaha_ratajumlahpembeli'];
		$newusaha_ratapembelian=$rows['usaha_ratapembelian'];
		$newusaha_kenaikanjumlahpembeli=$rows['usaha_kenaikanjumlahpembeli'];
		$newusaha_kenaikanticketsize=$rows['usaha_kenaikanticketsize'];
		$newusaha_hargapokokpenjualanpersen=$rows['usaha_hargapokokpenjualanpersen'];
		$newusaha_persentasemarginusaha=$rows['usaha_persentasemarginusaha'];
		$newusaha_jumlahkaryawan=$rows['usaha_jumlahkaryawan'];
		}
	}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Expires" CONTENT="0">
	<meta http-equiv="Cache-Control" CONTENT="no-cache">
	<meta http-equiv="Pragma" CONTENT="no-cache">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>LKCD 1</title>
	<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
	<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
	<script type="text/javascript" src="../../js/full_function.js" ></script>
	<script type="text/javascript" src="../../js/accounting.js" ></script>
	<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	<script>
		function validation()
		{
			var FormName="formentry";	
			var StatusAllowSubmit=true;
			var elem = document.getElementById(FormName).elements;
			for(var i = 0; i < elem.length; i++)
			{
				if(elem[i].style.backgroundColor=="#ff0")
				{
					
					if(elem[i].value == "")
					{
						alert(elem[i].nai + "HARUS DIISI");
						elem[i].focus();
						StatusAllowSubmit=false				
						break;
					}
				}
			}
			
			if(StatusAllowSubmit == true)
			{			
				document.getElementById(FormName).action = "DO_LKCD_VID3DATAKEUANGANLAINNYA_INFORMASI_PENDUKUNG_n.php";
				submitform = window.confirm("Save?")
				if (submitform == true)
				{
					document.getElementById(FormName).submit();
					return true;
				}
				else
				{
					return false;
				} 
			}
		}
	</script>
	</head>
	<body>
		<form id="formentry" name="formentry" method="post">
			<div style="font-weight:bold; font-size:16px; text-align:center;">INFORMASI PENDUKUNG</div>
			<div>&nbsp;</div>
			<table border="1" style="width:900px;" align="center">
				<tr>
					<td style="width:400px;">Tujuan Penggunaan Modal Kerja</td>
					<td>
					<?
							$strsql = "select * From tbl_tujuanlkcd where code='$usaha_sektorindustri'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['name'];
								}
							}
					?>
					</td>
					<td>
						<select id="usaha_tujuanmodalkerja" nai="Tujuan Penggunaan Modal Kerja " name="usaha_tujuanmodalkerja" style="background:#ff0; width:200px;">
							<option value=""> --  Pilih tujuan -- </option>
							<?
							$strsql = "select * From tbl_tujuanlkcd";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newusaha_tujuanmodalkerja==$rows['code'])
									{
										echo '<option value="'.$rows['code'].'" selected="selected">'.$rows['name'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['code'].'">'.$rows['name'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Lama persediaan</td>
					<td><?echo $usaha_lamapersediaan ?>  Hari</td>
					<td><input nai="Lama persediaan "type="text" style="background:#ff0; width:100px;" id="usaha_lamapersediaan" name="usaha_lamapersediaan" value="<?echo $newusaha_lamapersediaan ?>" onkeypress="return isNumberKey(event);" maxlength="5" />  Hari</td>
				</tr>
				<tr>
					<td>Cara Penjualan ke pembeli/buyer</td>
					<td>
						<?
						$strsql = "select * From TblTermPayment where term_code='$usaha_carabayarpiutang'";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								echo $rows['term_name'];
							}
						}
						?>
					</td>
					<td>
						<select nai="Cara Penjualan ke pembeli/buyer " id="usaha_carabayarpiutang" name="usaha_carabayarpiutang" style="background:#ff0; width:200px;">
							<option value=""> --  Pilih Pembayaran -- </option>
							<?
							$strsql = "select * From TblTermPayment";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newusaha_carabayarpiutang==$rows['term_code'])
									{
										echo '<option value="'.$rows['term_code'].'" selected="selected">'.$rows['term_name'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['term_code'].'">'.$rows['term_name'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Lama piutang usaha</td>
					<td><?echo $usaha_lamapiutang ?>  Hari</td>
					<td><input nai="Lama piutang usaha " type="text" style="background:#ff0; width:100px;" id="usaha_lamapiutang" name="usaha_lamapiutang" value="<?echo $newusaha_lamapiutang ?>" onkeypress="return isNumberKey(event);" maxlength="5" />  Hari</td>
				</tr>
				<tr>
					<td>Cara Penjualan ke pemasok/supplier</td>
					<td>
						<?
						$strsql = "select * From TblTermPayment where term_code='$usaha_carabayarhutang'";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								echo $rows['term_name'];
							}
						}
						?>
					</td>
					<td>
						<select nai="Cara Penjualan ke pemasok/supplier " id="usaha_carabayarhutang" name="usaha_carabayarhutang" style="background:#ff0; width:200px;">
							<option value=""> --  Pilih Pembayaran -- </option>
							<?
							$aaa="";
							$strsql = "select * From TblTermPayment";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newusaha_carabayarhutang==$rows['term_code'])
									{
										echo '<option value="'.$rows['term_code'].'" selected="selected">'.$rows['term_name'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['term_code'].'">'.$rows['term_name'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Lama hutang usaha</td>
					<td><?echo $usaha_lamahutang ?>  Hari</td>
					<td><input nai="Lama hutang usaha " type="text" style="background:#ff0; width:100px;" id="usaha_lamahutang" name="usaha_lamahutang" value="<?echo $newusaha_lamahutang ?>" onkeypress="return isNumberKey(event);" maxlength="5" />   Hari</td>
				</tr>
				<tr>
					<td>Sektor Industri</td>
					<td>
					<?
							$strsql = "select * from Tbl_SektorIndustri where sektor_code ='$usaha_sektorindustri'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['sektor_nama'];
								}
							}
							?>
					</td>
					<td>
						<select nai="Sektor Industri " id="usaha_sektorindustri" name="usaha_sektorindustri" style="background:#ff0; width:200px;">
							<option value=""> --  Pilih sektor industri -- </option>
							<?
							$strsql = "select * From Tbl_SektorIndustri";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newusaha_sektorindustri==$rows['sektor_code'])
									{
										echo '<option value="'.$rows['sektor_code'].'" selected="selected">'.$rows['sektor_nama'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['sektor_code'].'">'.$rows['sektor_nama'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Jenis usaha</td>
					<td><?echo $newusaha_jenisusaha?></td>
					<td><input nai="Jenis usaha " type="text" id="usaha_jenisusaha" name="usaha_jenisusaha" value="<? echo $newusaha_jenisusaha ?>" maxlength="20" style="background:#ff0; width:200px;" /></td>
				</tr>
				<tr>
					<td>Lama usaha</td>
					<td>
						<?echo $newusaha_lamausahamonth?>  Bulan
						<?echo $newusaha_lamausahayear ?>  Tahun
					</td>
					<td>
					<input nai="Lama usaha Bulan " type="text" id="usaha_lamausahamonth" name="usaha_lamausahamonth" value="<? echo $newusaha_lamausahamonth ?>" onkeypress="return isNumberKey(event);" style="background:#ff0; width:50px;" maxlength="3" />  Bulan
					<input nai="Lama usaha Year " type="text" id="usaha_lamausahayear" name="usaha_lamausahayear" value="<? echo $newusaha_lamausahayear ?>" onkeypress="return isNumberKey(event);" style="background:#ff0; width:50px;" maxlength="3" />  Tahun
					</td>
				</tr>
				<tr>
					<td>Kepemilikan</td>
					<td>
						<?
						$strsql = "select * From tblstatusrumah where status_code ='$usaha_kepemilikanusaha'";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								echo $rows['status_name'];
							}
						}
						?>
					</td>
					<td>
						<select nai="Kepemilikan " id="usaha_kepemilikanusaha" name="usaha_kepemilikanusaha" style="background:#ff0; width:200px;">
							<option value=""> --  Pilih Status Kepemilikan -- </option>
							<?
							$strsql = "select * From tblstatusrumah $tmpwhere";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									if($newusaha_kepemilikanusaha==$rows['status_code'])
									{
										echo '<option value="'.$rows['status_code'].'" selected="selected">'.$rows['status_name'].'</option>';
									}
									else
									{
										echo '<option value="'.$rows['status_code'].'">'.$rows['status_name'].'</option>';
									}
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Jumlah hari kerja usaha/ jumlah hari penjualan</td>
					<td>
						<? 
						echo $usaha_jumlahpenjualan;
						$selected="";
						if ($usaha_jumlahpenjualandate=="1")
						{
							$selected="Hari";
						}
						else if($usaha_jumlahpenjualandate=="2")
						{
							$selected="Bulan";
						}
						else if($usaha_jumlahpenjualandate=="3")
						{
							$selected="Year";
						}
						
						echo '&nbsp;'.$selected;
						?>
						
					</td>
					<td>
						<input nai="Jumlah hari kerja usaha/ jumlah hari penjualan " type="text" style=" background:#ff0;width:50px;" id="usaha_jumlahpenjualan" name="usaha_jumlahpenjualan" maxlength="5" value="<?echo $newusaha_jumlahpenjualan?>" onkeypress="return isNumberKey(event);" />
						<select nai="Pilihan Jumlah hari kerja usaha/ jumlah hari penjualan " id="usaha_jumlahpenjualandate" name="usaha_jumlahpenjualandate" style="background:#ff0;width:100px;">
						<?
							$selected='selected="selected"';
							$opt1="";
							$opt2="";
							$opt3="";
							if ($newusaha_jumlahpenjualandate=="1")
							{
								$opt1=$selected;
							}
							else if($newusaha_jumlahpenjualandate=="2")
							{
								$opt2=$selected;
							}
							else if($newusaha_jumlahpenjualandate=="3")
							{
								$opt3=$selected;
							}
						?>
							<option value=""> -- Pilih -- </option>
							<option <?echo $opt1?> value="1">Hari</option>
							<option <?echo $opt2?> value="2">Bulan</option>
							<option <?echo $opt3?> value="3">Year</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Rata-rata jumlah pembeli/pemesanan per-penjualan</td>
					<td><?echo $usaha_ratajumlahpembeli?> CUSTOMER</td>
					<td><input nai="Rata-rata jumlah pembeli/pemesanan per-penjualan " type="text" style=" background:#ff0;width:50px;" id="usaha_ratajumlahpembeli" name="usaha_ratajumlahpembeli" value="<?echo $newusaha_ratajumlahpembeli?>" onkeypress="return isNumberKey(event);" maxlength="4" />  CUSTOMER</td>
				</tr>
				<tr>
					<td>Rata-rata pembelian/belanja per-customer/Ticket size</td>
					<td><?echo $usaha_ratapembelian?> RUPIAH</td>
					<td><input nai="Rata-rata pembelian/belanja per-customer/Ticket size " type="text" style=" background:#ff0;width:200px;" id="usaha_ratapembelian" name="usaha_ratapembelian" value="<?echo $newusaha_ratapembelian?>" maxlength="15" onkeypress="return isNumberKey(event);" onkeyup="currency(this.id)" />  RUPIAH</td>
				</tr>
				<tr>
					<td>Kenaikan jumlah pembeli/pemesan</td>
					<td><?echo $usaha_kenaikanjumlahpembeli?>  %</td>
					<td><input nai="Kenaikan jumlah pembeli/pemesan " type="text" style=" background:#ff0;width:50px;" id="usaha_kenaikanjumlahpembeli" name="usaha_kenaikanjumlahpembeli" value="<?echo $newusaha_kenaikanjumlahpembeli?>" onkeypress="return isNumberKey(event);" maxlength="4" />  %</td>
				</tr>
				<tr>
					<td>Kenaikan Ticket size</td>
					<td>
						<?echo $usaha_kenaikanticketsize?> %
					</td>
					<td><input nai="Kenaikan Ticket size " type="text" style=" background:#ff0;width:50px;" id="usaha_kenaikanticketsize" name="usaha_kenaikanticketsize" value="<?echo $newusaha_kenaikanticketsize?>" onkeypress="return isNumberKey(event);" maxlength="4" />  %</td>
				</tr>
				<tr>
					<td>Hpp</td>
					<td><?echo $usaha_hargapokokpenjualanpersen?>  %</td>
					<td><input nai="Hpp " type="text" style=" background:#ff0;width:50px;" id="usaha_hargapokokpenjualanpersen" name="usaha_hargapokokpenjualanpersen" value="<? echo $newusaha_hargapokokpenjualanpersen?>" onkeypress="return isNumberKey(event);" maxlength="4" />  %</td>
				</tr>
				<tr>
					<td>Persentase Margin Usaha</td>
					<td><?echo $usaha_persentasemarginusaha?>  %</td>
					<td><input nai="Persentase Margin Usaha " type="text" style=" background:#ff0;width:50px;" id="usaha_persentasemarginusaha" name="usaha_persentasemarginusaha" value="<? echo $newusaha_persentasemarginusaha?>" onkeypress="return isNumberKey(event);" maxlength="4" />  %</td>
				</tr>
				<tr>
					<td>Jumlah Karyawan</td>
					<td><?echo $usaha_jumlahkaryawan?> orang</td>
					<td><input nai="Jumlah Karyawan " type="text" style=" background:#ff0;width:100px;" id="usaha_jumlahkaryawan" name="usaha_jumlahkaryawan" value="<? echo $newusaha_jumlahkaryawan?>" onkeypress="return isNumberKey(event);" maxlength="8" />  Orang</td>
				</tr>
			</table>
			<div>&nbsp;</div>
			<div class="divcenter">
				<?
					require ("../../requirepage/hiddenfield.php");
				?>
					<input type="hidden" id="btn" name="btn" value="btnsave" />
					<input type="hidden" id="btn" name="btn" value="btnsave" /><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="validation()" />
					
			</div>
		</form>
	</body>
</html>