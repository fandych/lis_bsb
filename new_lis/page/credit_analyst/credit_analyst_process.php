<?
require ("../../lib/class.sqlserver.php");
require_once('../../requirepage/parameter.php');
require_once("../../lib/functionPerhitunganAngsuran.php");

$db = new SQLSRV();
$db->connect();

echo "<pre>";
print_r($_REQUEST);
echo "</pre>";

$seq = 0;

foreach($_REQUEST as $key=>$value)
{
    $$key = $value;
}

if($in_max_pembiayaan == "") { $in_max_pembiayaan = 0; }
if($in_rencana_kredit == "") { $in_rencana_kredit = 0; }
if($in_harga_jual == "") { $in_harga_jual = 0; }
if($in_uang_muka == "") { $in_uang_muka = 0; }


if($action == "edit_plafond")
{
    $rand = md5(microtime());
    $plafond = str_replace(",","",$amount_custom);
    $tenor = $tenor_custom;
    $rate = $rate_custom;
    $provisi = $provisi_custom;

	$get_jebu = "SELECT jenisbunga FROM tbl_customerFacility2 WHERE custnomid = '$custnomid'";
    $db->executeQuery($get_jebu);
    $get_jebu = $db->lastResults;
	$jeBu = $get_jebu[0]['jenisbunga'];
	
	echo $plafond;
	echo $tenor;
	echo $rate;
	
    $installment = perhitunganAngsuran($plafond, $tenor, $rate, $jeBu);
	
    $rate = $rate_custom;

    $check_plafond = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
    echo $check_plafond."<br>";
    $db->executeQuery($check_plafond);
    $check_plafond = $db->lastResults;

    if(count($check_plafond) == 0)
    {
        $tsql_plafond = "INSERT INTO CR_ANALYST_APPROVAL (index_id,custnomid,source,cr_option,fac_seq,plafond,tenor,interest,provisi,installment,opinion,description) VALUES ('$rand','$custnomid','AKRE','$keputusan_kredit','$seq','$plafond','$tenor','$rate','$provisi','$installment','$in_opinion','$in_keterangan')";
    }
    else
    {
        $tsql_plafond = "UPDATE CR_ANALYST_APPROVAL SET plafond = '$plafond', installment = '$installment', tenor = '$tenor', interest = '$rate', cr_option = '$keputusan_kredit', provisi = '$provisi', opinion = '$in_opinion', description = '$in_keterangan' WHERE custnomid = '$custnomid' AND source = 'AKRE'";
    }

    echo $tsql_plafond."<br>";
    $db->executeNonQuery($tsql_plafond);

    $facility = "UPDATE tbl_CustomerFacility2 SET custcreditplafond = '$plafond', mkknewangsuran = '$installment' WHERE custnomid='$custnomid' AND custfacseq='$seq'";
    echo $facility."<br>";
    $db->executeNonQuery($facility);

    $tsql_history = "INSERT INTO [dbo].[CAP_PLAFOND_HISTORY]
    ([index_id]
    ,[custnomid]
    ,[facility_seq]
    ,[facility_plafond]
    ,[facility_angsuran]
    ,[facility_need]
    ,[facility_tenor]
    ,[datetime]
    ,[user_id])
    VALUES
    ('$rand'
    ,'$custnomid'
    ,'$seq'
    ,'$plafond'
    ,'$installment'
    ,'0'
    ,'$tenor'
    ,GETDATE()
    ,'$userid'
    )
    ";
    echo $tsql_history."<br>";
    $db->executeNonQuery($tsql_history);

    header("location:credit_analyst.php?userwfid=$userwfid&userpermission=$userpermission&custnomid=$custnomid&buttonaction=ICA");

}
else if ($action == "delete")
{

    $action = $_REQUEST['delete_data'];
    $action_arr = explode("_",$action);
    $get_delete_year = $action_arr[0];
    $get_delete_month = $action_arr[1];
    $get_delete_source = $action_arr[2];

    $delete = "DELETE FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _year = '$get_delete_year' AND _month = '$get_delete_month' AND _source = '$get_delete_source'";
    $db->executeQuery($delete);

    header("location:credit_analyst.php?userwfid=$userwfid&userpermission=$userpermission&custnomid=$custnomid&buttonaction=I");
}
else if($action == "save_max") {

    $delete_advis_data = "DELETE FROM CR_DATA WHERE custnomid = '$custnomid'";
    echo $delete_advis_data."<br>";
    $db->executeNonQuery($delete_advis_data);

	$insert_advis_data = "
	INSERT INTO [dbo].[CR_DATA]
           ([custnomid]
           ,[harga_jual]
           ,[uang_muka]
           ,[rencana_kredit]
           ,[persentase_setoran_uang_muka]
           ,[persentase_pembiayaan]
           ,[max_pembiayaan]
           ,[ket_identitas]
           ,[ket_penghasilan]
           ,[ket_kredit]
           ,[kesimpulan]
           ,[data_identitas]
           ,[data_pekerjaan]
           ,[data_penghasilan]
           ,[data_syarat_kredit]
           ,[ket_bunga]
           ,[ket_angsuran]
           ,[pengikatan_jaminan]
           ,[biaya_adm]
           ,[syarat_ttd_pk]
           ,[syarat_realisasi]
           ,[user_advis]
           ,[lain_lain])
     VALUES
			('$custnomid'
			,'$in_harga_jual'
			,'$in_uang_muka'
			,'$in_rencana_kredit'
			,'$in_percent_setoran_uang_muka'
			,'$in_percent'
			,'$in_max_pembiayaan'
			,'$in_keterangan_identitas'
			,'$in_keterangan_penghasilan'
			,'$opinion'
			,'$in_kesimpulan_data'
			,'$in_data_identitas'
			,'$in_data_pekerjaan'
			,'$in_data_penghasilan'
			,'$in_data_syarat_kredit'
			,''
			,''
			,''
			,'0'
			,''
			,''
            ,'$user_advis'
			,''
			)
			";

    echo $insert_advis_data."<br>";
    $db->executeNonQuery($insert_advis_data);

    header("location:credit_analyst.php?userwfid=$userwfid&userpermission=$userpermission&custnomid=$custnomid&buttonaction=I");

}
else if($action == "submit") {

    $source = "SELECT * FROM tbl_FSTART WHERE txn_id = '$custnomid'";
    $db->executeQuery($source);
    $source = $db->lastResults;

    echo "<pre>";
    //print_r($db->lastResults);
    echo "</pre>";

    $originalbranch = $source[0]['txn_branch_code'];
    $originalregion = $source[0]['txn_region_code'];

    switch($keputusan_kredit)
    {
        case "plafond_maksimal":
            echo $amount_maksimal;
            echo "<br>";
            echo $emi_maksimal;

            $rand = md5(microtime());
            $plafond = str_replace(",","",$amount_maksimal);
            $installment = str_replace(",","",$emi_maksimal);
            $tenor = $tenor_maksimal;
            $rate = $rate_maksimal;
            $provisi = 1;
            $seq = 0;

            $check_plafond = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            echo $check_plafond."<br>";
            $db->executeQuery($check_plafond);
            $check_plafond = $db->lastResults;

            if(count($check_plafond) == 0)
            {
                $tsql_plafond = "INSERT INTO CR_ANALYST_APPROVAL (index_id,custnomid,source,cr_option,fac_seq,plafond,tenor,interest,provisi,installment,opinion,description) VALUES ('$rand','$custnomid','AKRE','$keputusan_kredit','$seq','$plafond','$tenor','$rate','$provisi','$installment','$in_opinion','$in_keterangan')";
            }
            else
            {
                $tsql_plafond = "UPDATE CR_ANALYST_APPROVAL SET plafond = '$plafond', installment = '$installment', tenor = '$tenor', interest = '$rate', cr_option = '$keputusan_kredit', provisi = '$provisi', opinion = '$in_opinion', description = '$in_keterangan' WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            }

            echo $tsql_plafond."<br>";
            $db->executeNonQuery($tsql_plafond);

            $facility = "UPDATE tbl_CustomerFacility2 SET custcreditplafond = '$plafond', mkknewangsuran = '$installment' WHERE custnomid='$custnomid' AND custfacseq='$seq'";
            echo $facility."<br>";
            $db->executeNonQuery($facility);

            $tsql_history = "INSERT INTO [dbo].[CAP_PLAFOND_HISTORY]
            ([index_id]
            ,[custnomid]
            ,[facility_seq]
            ,[facility_plafond]
            ,[facility_angsuran]
            ,[facility_need]
            ,[facility_tenor]
            ,[datetime]
            ,[user_id])
            VALUES
            ('$rand'
            ,'$custnomid'
            ,'$seq'
            ,'$plafond'
            ,'$installment'
            ,'0'
            ,'$tenor'
            ,GETDATE()
            ,'$userid'
            )
            ";
            echo $tsql_history."<br>";
            $db->executeNonQuery($tsql_history);

        break;

        case "plafond_pengusulan":
            echo $amount_request;
            echo "<br>";
            echo $emi_request;

            $rand = md5(microtime());
            $plafond = str_replace(",","",$amount_request);
            $installment = str_replace(",","",$emi_request);
            $tenor = $tenor_request;
            $rate = $rate_request;
            $provisi = 1;
            $seq = 0;

            $check_plafond = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            echo $check_plafond."<br>";
            $db->executeQuery($check_plafond);
            $check_plafond = $db->lastResults;

            if(count($check_plafond) == 0)
            {
                $tsql_plafond = "INSERT INTO CR_ANALYST_APPROVAL (index_id,custnomid,source,cr_option,fac_seq,plafond,tenor,interest,provisi,installment,opinion,description) VALUES ('$rand','$custnomid','AKRE','$keputusan_kredit','$seq','$plafond','$tenor','$rate','$provisi','$installment','$in_opinion','$in_keterangan')";
            }
            else
            {
                $tsql_plafond = "UPDATE CR_ANALYST_APPROVAL SET plafond = '$plafond', installment = '$installment', tenor = '$tenor', interest = '$rate', cr_option = '$keputusan_kredit', provisi = '$provisi', opinion = '$in_opinion', description = '$in_keterangan' WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            }

            echo $tsql_plafond."<br>";
            $db->executeNonQuery($tsql_plafond);

            $facility = "UPDATE tbl_CustomerFacility2 SET custcreditplafond = '$plafond', mkknewangsuran = '$installment' WHERE custnomid='$custnomid' AND custfacseq='$seq'";
            echo $facility."<br>";
            $db->executeNonQuery($facility);

            $tsql_history = "INSERT INTO [dbo].[CAP_PLAFOND_HISTORY]
            ([index_id]
            ,[custnomid]
            ,[facility_seq]
            ,[facility_plafond]
            ,[facility_angsuran]
            ,[facility_need]
            ,[facility_tenor]
            ,[datetime]
            ,[user_id])
            VALUES
            ('$rand'
            ,'$custnomid'
            ,'$seq'
            ,'$plafond'
            ,'$installment'
            ,'0'
            ,'$tenor'
            ,GETDATE()
            ,'$userid'
            )
            ";
            echo $tsql_history."<br>";
            $db->executeNonQuery($tsql_history);

        break;


        case "plafond_recomend":
            echo $amount_recomend;
            echo "<br>";
            echo $emi_request;

            $rand = md5(microtime());
            $plafond = str_replace(",","",$amount_recomend);
            $installment = str_replace(",","",$emi_recomend);
            $tenor = $tenor_recomend;
            $rate = $rate_recomend;
            $provisi = 1;
            $seq = 0;

            $check_plafond = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            echo $check_plafond."<br>";
            $db->executeQuery($check_plafond);
            $check_plafond = $db->lastResults;

            if(count($check_plafond) == 0)
            {
                $tsql_plafond = "INSERT INTO CR_ANALYST_APPROVAL (index_id,custnomid,source,cr_option,fac_seq,plafond,tenor,interest,provisi,installment,opinion,description) VALUES ('$rand','$custnomid','AKRE','$keputusan_kredit','$seq','$plafond','$tenor','$rate','$provisi','$installment','$in_opinion','$in_keterangan')";
            }
            else
            {
                $tsql_plafond = "UPDATE CR_ANALYST_APPROVAL SET plafond = '$plafond', installment = '$installment', tenor = '$tenor', interest = '$rate', cr_option = '$keputusan_kredit', provisi = '$provisi', opinion = '$in_opinion', description = '$in_keterangan' WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            }

            echo $tsql_plafond."<br>";
            $db->executeNonQuery($tsql_plafond);

            $facility = "UPDATE tbl_CustomerFacility2 SET custcreditplafond = '$plafond', mkknewangsuran = '$installment' WHERE custnomid='$custnomid' AND custfacseq='$seq'";
            echo $facility."<br>";
            $db->executeNonQuery($facility);

            $tsql_history = "INSERT INTO [dbo].[CAP_PLAFOND_HISTORY]
            ([index_id]
            ,[custnomid]
            ,[facility_seq]
            ,[facility_plafond]
            ,[facility_angsuran]
            ,[facility_need]
            ,[facility_tenor]
            ,[datetime]
            ,[user_id])
            VALUES
            ('$rand'
            ,'$custnomid'
            ,'$seq'
            ,'$plafond'
            ,'$installment'
            ,'0'
            ,'$tenor'
            ,GETDATE()
            ,'$userid'
            )
            ";
            echo $tsql_history."<br>";
            $db->executeNonQuery($tsql_history);

        break;

        case "plafond_custom":
            echo $amount_custom;
            echo "<br>";
            echo $emi_custom;

            $rand = md5(microtime());

            $plafond = str_replace(",","",$amount_custom);
            $installment = str_replace(",","",$emi_custom);
            $tenor = $tenor_custom;
            $rate = $rate_custom;
            $provisi = $provisi_custom;

            $seq = 0;

            $check_plafond = "SELECT * FROM CR_ANALYST_APPROVAL WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            echo $check_plafond."<br>";
            $db->executeQuery($check_plafond);
            $check_plafond = $db->lastResults;

            if(count($check_plafond) == 0)
            {
                $tsql_plafond = "INSERT INTO CR_ANALYST_APPROVAL (index_id,custnomid,source,cr_option,fac_seq,plafond,tenor,interest,provisi,installment,opinion,description) VALUES ('$rand','$custnomid','AKRE','$keputusan_kredit','$seq','$plafond','$tenor','$rate','$provisi','$installment','$in_opinion','$in_keterangan')";
            }
            else
            {
                $tsql_plafond = "UPDATE CR_ANALYST_APPROVAL SET plafond = '$plafond', installment = '$installment', tenor = '$tenor', interest = '$rate', cr_option = '$keputusan_kredit', provisi = '$provisi', opinion = '$in_opinion', description = '$in_keterangan' WHERE custnomid = '$custnomid' AND source = 'AKRE'";
            }

            echo $tsql_plafond."<br>";
            $db->executeNonQuery($tsql_plafond);

            $facility = "UPDATE tbl_CustomerFacility2 SET custcreditplafond = '$plafond', mkknewangsuran = '$installment' WHERE custnomid='$custnomid' AND custfacseq='$seq'";
            echo $facility."<br>";
            $db->executeNonQuery($facility);

            $tsql_history = "INSERT INTO [dbo].[CAP_PLAFOND_HISTORY]
            ([index_id]
            ,[custnomid]
            ,[facility_seq]
            ,[facility_plafond]
            ,[facility_angsuran]
            ,[facility_need]
            ,[facility_tenor]
            ,[datetime]
            ,[user_id])
            VALUES
            ('$rand'
            ,'$custnomid'
            ,'$seq'
            ,'$plafond'
            ,'$installment'
            ,'0'
            ,'$tenor'
            ,GETDATE()
            ,'$userid'
            )
            ";
            echo $tsql_history."<br>";
            $db->executeNonQuery($tsql_history);

        break;

        default :
        break;

    }

    $delete_advis_data = "DELETE FROM CR_DATA WHERE custnomid = '$custnomid'";
    echo $delete_advis_data."<br>";
    $db->executeNonQuery($delete_advis_data);

	$insert_advis_data = "
	INSERT INTO [dbo].[CR_DATA]
           ([custnomid]
           ,[harga_jual]
           ,[uang_muka]
           ,[rencana_kredit]
           ,[persentase_setoran_uang_muka]
           ,[persentase_pembiayaan]
           ,[max_pembiayaan]
           ,[ket_identitas]
           ,[ket_penghasilan]
           ,[ket_kredit]
           ,[kesimpulan]
           ,[data_identitas]
           ,[data_pekerjaan]
           ,[data_penghasilan]
           ,[data_syarat_kredit]
           ,[ket_bunga]
           ,[ket_angsuran]
           ,[pengikatan_jaminan]
           ,[biaya_adm]
           ,[syarat_ttd_pk]
           ,[syarat_realisasi]
           ,[user_advis]
           ,[lain_lain])
     VALUES
			('$custnomid'
			,'$in_harga_jual'
			,'$in_uang_muka'
			,'$in_rencana_kredit'
			,'$in_percent_setoran_uang_muka'
			,'$in_percent'
			,'$in_max_pembiayaan'
			,'$in_keterangan_identitas'
			,'$in_keterangan_penghasilan'
			,'$opinion'
			,'$in_kesimpulan_data'
			,'$in_data_identitas'
			,'$in_data_pekerjaan'
			,'$in_data_penghasilan'
			,'$in_data_syarat_kredit'
			,''
			,''
			,''
			,'0'
			,''
			,''
            ,'$user_advis'
			,''
			)
			";

    echo $insert_advis_data."<br>";
    $db->executeNonQuery($insert_advis_data);


	/*
    $update_advis_data = "UPDATE CR_DATA
    SET data_identitas = '$in_data_identitas'
    ,data_pekerjaan = '$in_data_pekerjaan'
    ,data_penghasilan = '$in_data_penghasilan'
    ,data_syarat_kredit = '$in_data_syarat_kredit'
    ,ket_identitas = '$in_keterangan_identitas'
    ,ket_penghasilan = '$in_keterangan_penghasilan'
    ,ket_kredit = '$in_keterangan_kredit'
    ,kesimpulan = '$in_kesimpulan_data'
    WHERE custnomid = '$custnomid'";
    echo $update_advis_data."<br>";
    $db->executeNonQuery($update_advis_data);
	*/


    if($userpermission == "I") {
        $txn_action = "I";
    }
    else {
        $txn_action = "A";
    }


    $notes = $_REQUEST['recommendNotes'];

    $delete = "DELETE FROM Tbl_F$userwfid WHERE txn_id = '$custnomid'";
    $db->executeNonQuery($delete);

    $approve = "INSERT INTO Tbl_F$userwfid ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code])
    VALUES('$custnomid','$txn_action',getdate(),'$userid','$notes', '$originalbranch','$originalregion')";
    $db->executeNonQuery($approve);

    $tsql = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$txn_action',getdate(),'$userid','$notes','$userwfid', '$originalbranch','$originalregion')";
    $db->executeNonQuery($tsql);

    header("location:../flow.php?userwfid=$userwfid");
}
?>
