<?php
include("../../../requirepage/session.php");
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");
require_once("../../../lib/class.sqlserver.php");
$sql = new SQLSRV();
$sql->connect();
$currentDB = $sql->dbName;
$notlike = "AND TABLE_NAME NOT LIKE 'tbl_flow%' ";
$notlike .= "AND TABLE_NAME NOT LIKE 'tbl_fak%' ";
$notlike .= "AND TABLE_NAME NOT LIKE 'tbl_format%' ";

$paramSyntax = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_CATALOG = '$currentDB' AND TABLE_NAME LIKE 'tbl_F%' $notlike";
$sql->executeQuery($paramSyntax);
$paramResult = $sql->lastResults;
$paramCount = count($paramResult);

if($sql->debug == true){
/*echo $paramSyntax;
echo "<pre>";
print_r($paramResult);
echo "</pre>";*/
}

//GETDATA
if(isset($_POST['wfopt'])){
	
	//echo "IF<br />";
	//print_r($_POST['wfopt']);
	$getOption = $_POST['wfopt'];
}
else{
	
	$getOption = array();
	for($x=0;$x<$paramCount;$x++){
		$paramCode = substr($paramResult[$x]['TABLE_NAME'],5,10);
		array_push($getOption,$paramCode);
	}
	//echo "ELSE<br />";
	//print_r($getOption);
}
//echo "asd<br/>";
$totalOption = count($getOption);

$tmp="";
for($x=0;$x<$paramCount;$x++){
	$tmp .="'".$paramResult[$x]['TABLE_NAME']."',";
}

$yeslike = substr($tmp,0,-1);
$wfSyntax = "SELECT TABLE_NAME FROM information_schema.tables where TABLE_NAME in($yeslike)";
$sql->executeQuery($wfSyntax);
$wfResult = $sql->lastResults;
$wfCount = count($wfResult);

if($sql->debug == true){
/*echo $wfSyntax;
echo "<pre>";
print_r($wfResult);
echo "</pre>";*/
}

$arrayTemp = array();

for($x=0;$x<$wfCount;$x++){
	$table = $wfResult[$x]['TABLE_NAME'];
	$flow = substr($paramResult[$x]['TABLE_NAME'],5,10);
	$color = randomColorOne();
	$dataSyntax = "SELECT '$flow' as flow,COUNT(*) AS data,'$color' AS color FROM $table";
	$sql->executeQuery($dataSyntax);
	$dataResult = $sql->lastResults;
	$dataCount = count($dataResult);
	
	array_push($arrayTemp,$dataResult[0]);
	
	if($sql->debug == true){
	/*echo $dataSyntax;
	echo "<pre>";
	print_r($dataResult);
	echo "</pre>";*/
	}
}

//echo "<pre>";
//print_r($arrayTemp);
//echo "</pre>";

$totalData = count($arrayTemp);
$json =  json_encode($arrayTemp);



function randomColorOne() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 


function randomColorTwo() { 
    $possibilities = array(1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F" );
    shuffle($possibilities);
    $color = "#";
    for($i=1;$i<=6;$i++){
        $color .= $possibilities[rand(0,14)];
    }
    return $color;
} 


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Workflow Report</title>
<link rel="stylesheet" href="../css/style.css" type="text/css">
<script src="../js/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="../amcharts/amcharts.js" type="text/javascript"></script>
<script src="../amcharts/serial.js" type="text/javascript"></script>
<script type="text/javascript">
var chart;

var chartData = <?=$json;?>;

AmCharts.ready(function () {
	// SERIAL CHART
	chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "flow";
	// the following two lines makes chart 3D
	chart.depth3D = 20;
	chart.angle = 30;

	// AXES
	// category
	var categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 90;
	categoryAxis.dashLength = 5;
	categoryAxis.gridPosition = "start";

	// value
	var valueAxis = new AmCharts.ValueAxis();
	valueAxis.title = "Data Workflow";
	valueAxis.dashLength = 5;
	chart.addValueAxis(valueAxis);

	// GRAPH
	var graph = new AmCharts.AmGraph();
	graph.valueField = "data";
	graph.colorField = "color";
	graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
	graph.type = "column";
	graph.lineAlpha = 0;
	graph.fillAlphas = 1;
	chart.addGraph(graph);

	// CURSOR
	var chartCursor = new AmCharts.ChartCursor();
	chartCursor.cursorAlpha = 0;
	chartCursor.zoomable = false;
	chartCursor.categoryBalloonEnabled = false;
	chart.addChartCursor(chartCursor);

	chart.creditsPosition = "top-right";

	// WRITE
	chart.write("chartdiv");
});
</script>
<script>
function refreshData()
{
	$("#formreport").submit();
}
</script>
</head>
<body style="padding:0px;margin:0px;">
<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
<div id="chartdiv" style="width:<?=($totalData*75)+100;?>px; height:450px;"></div>
<div id="parameter">
<h1 style="margin:0; margin-top:10px; padding:0; padding-left:25px; padding-bottom:10px; font-family:sans-serif;">Option</h1>
<div style="background:#444; color:#fafafa; padding:10px;">
<form name="formreport" id="formreport" method="post" action="#">
<table>
<tr>
<?php
for($x=0;$x<$paramCount;$x++){
$paramCode = substr($paramResult[$x]['TABLE_NAME'],5,10);
if(in_array($paramCode,$getOption)){$checked="checked=\"checked\"";}else{$checked="";}
if ($x % 5 === 0) { ?></tr><tr><? }?>
<td><input type="checkbox" name="wfopt[]" onchange="refreshData();" id="<?=$paramCode;?>" value="<?=$paramCode;?>" class="css-checkbox" <?=$checked;?> /><label for="<?=$paramCode;?>" class="css-label">&nbsp;<?=strtoupper($paramCode);?>&nbsp;</label></td>
<? }?>
</tr>
</table>
</form>
</div>
<p style="padding-left:25px;">Property of <a href="http://spin-international.com">SPIN-International</a></p>
</div>
</body>
</html>