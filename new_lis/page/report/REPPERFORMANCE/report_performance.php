<?php
	
	include ("../../../lib/formatError.php");
	require ("../../../lib/open_con.php");
	//require ("../../../lib/open_con_apr.php");
	error_reporting(0);
	
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VIEW PERFORMANCE</title>
<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../../js/full_function.js"></script>
<link href="../../../css/crw.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="print">
	.NonPrintable
	{
	  display: none;
	}
</style>

<style type="text/css" media="print">
	.break{
	page-break-before: always;
	}
</style>
</head>

<BODY>
<script language="Javascript">
				name="utama";
</script>

<div align=center valign=top> <strong>REPORT PERFORMANCE DI CABANG <?=$userbranch?></strong></div>
				
</br></br>

<table width=800px border=0 style="border:1px solid black;" cellpadding="0" cellspacing="0" align=center valign=top >
			
<tr>
	<td style="border:1px solid black;" width=3% align=center><strong>NO.</strong></td>
	<td style="border:1px solid black;" width=20% align=center><strong>NAMA</strong></td>
	<td style="border:1px solid black;" width=20% align=center><strong>JABATAN</strong></td>
	<td style="border:1px solid black;" width=15% align=center><strong>PERFORMANCE</strong></td>
</tr>

<?
	$count=0;
	$tsql = "select * from Tbl_SE_User where user_branch_code = '$userbranch'";		
	$ase = sqlsrv_query($conn, $tsql);
	if ($ase === false)
		die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($ase))
	{  
		while($rowse = sqlsrv_fetch_array($ase, SQLSRV_FETCH_ASSOC))
		{ 
			$userfor = $rowse['user_id'];
			
			$tsqlmp = "select (select level_name from Tbl_SE_Level where level_code = user_level_code) as Jabatan, user_name, (SELECT sum(sla_sum) FROM Tbl_SLAHead WHERE sla_userid=USER_ID) as sum, (SELECT sum(sla_count) FROM Tbl_SLAHead WHERE sla_userid=USER_ID) as count from Tbl_SE_User where user_id = '$userfor'";
			$amp = sqlsrv_query($conn, $tsqlmp);
			if ($amp === false)
				die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($amp))
			{  
				if($rowmp = sqlsrv_fetch_array($amp, SQLSRV_FETCH_ASSOC))
				{ 
					//$debitur = $rowmp['JABATAN'];
					//echo $rowmp['Jabatan'];
					
					
?>				
					
<tr>
	<td style="border:1px solid black;" width=3% align=center><?=$count+1;?></td>
	<td style="border:1px solid black;" width=20% align=center><?=$rowmp['user_name'];?></td>
	<td style="border:1px solid black;" width=20% align=center><?=$rowmp['Jabatan']?></td>
	<td style="border:1px solid black;" width=15% align=center><?=round(($rowmp['sum'] / $rowmp['count']) * 100,2);?> %</td>
</tr>
					
<?
				}
			}
			$count++;
		}
	}
?>

</table>

</BODY>
</html>
<?
   	require("../../../lib/close_con.php");
?>