<?
require('../../lib/open_con.php');
require ("../../lib/class.sqlserver.php");
require('../../lib/formatError.php');
require_once('../../requirepage/parameter.php');


$db = new SQLSRV();
$db->connect();

$customer_info = "SELECT * FROM tbl_CustomerMasterPerson2 WHERE custnomid = '$custnomid'";
$db->executeQuery($customer_info);
$customer_info = $db->lastResults;
$custproccode = $customer_info[0]['custproccode'];

$customer_info = "SELECT * FROM Tbl_Processing WHERE proc_code = '$custproccode'";
$db->executeQuery($customer_info);
$customer_info = $db->lastResults;
$proc_name = $customer_info[0]['proc_name'];

$cust_info = "select * from CR_ANALYST_APPROVAL where custnomid = '$custnomid' and source='$userwfid'";
$db->executeQuery($cust_info);
$cust_info = $db->lastResults;
foreach($cust_info[0] as $key=>$value)
{
    $$key = $value;
}

$cust_info = "select * from tbl_CustomerFacility2 where custnomid = '$custnomid'";
$db->executeQuery($cust_info);
$cust_info = $db->lastResults;
foreach($cust_info[0] as $key=>$value)
{
    $$key = $value;
}

$cust_info = "select * from tbl_COL_Building where col_id in (select col_id from Tbl_Cust_MasterCol where ap_lisregno = '$custnomid' and flaginsert = '1' and flagdelete = '0' and cust_jeniscol = 'BA1')";
$db->executeQuery($cust_info);
$cust_info = $db->lastResults;
foreach($cust_info[0] as $key=>$value)
{
    $$key = $value;
}

$cust_info = "select * from tbl_COL_Land where col_id in (select col_id from Tbl_Cust_MasterCol where ap_lisregno = '$custnomid' and flaginsert = '1' and flagdelete = '0' and cust_jeniscol = 'BA1')";
$db->executeQuery($cust_info);
$cust_info = $db->lastResults;
foreach($cust_info[0] as $key=>$value)
{
    $$key = $value;
}

$flow_info = "SELECT * FROM tbl_FAKRE WHERE txn_id = '$custnomid'";
$db->executeQuery($flow_info);
$flow_info = $db->lastResults;
foreach($flow_info[0] as $key=>$value)
{
    $$key = $value;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <title>Struktur Fasilitas Kredit</title>

        <script type="text/javascript" src="../../js/full_function.js" ></script>
        <script type="text/javascript" src="../../js/accounting_neraca.js" ></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>


        <style>

        *  {
            margin: 0px;
            padding: 0px;
            font-size: 12px;
        }


        body {
            font-family: "Trebuchet MS", Helvetica, sans-serif;
        }

        .workspace {
            margin-top: 10px;
            margin-bottom: 100px;
            margin-left: 25px;
            margin-right: 25px;
        }

        .table-data {
        }

        .information-area {
            width: 75%;
            margin-top: 25px;
            margin-bottom: 25px;
        }

        .display-area {
            display: inline-block;
        }

        .sign-area {
            display: inline-block;
        }

        .approve-area {
            display: inline-block;
        }

        .display-box {
            color: #666666;
            font-weight: lighter;
            float: left;
            margin-top: 1px;
            margin-left: 30px;
            margin-right: 30px;
            padding-top: 5px;
            padding-left: 20px;
            padding-right: 20px;
            padding-bottom: 10px;
            width: 170px;
            position: relative;
            border-radius: 5px 5px 5px 5px;
            -moz-border-radius: 5px 5px 5px 5px;
            -webkit-border-radius: 5px 5px 5px 5px;
            border: 0px solid #cccccc;
        }

        .sign-box {
            color: #666666;
            font-weight: lighter;
            float: left;
            margin-top: 5px;
            margin-left: 30px;
            margin-right: 30px;
            padding-top: 15px;
            padding-left: 20px;
            padding-right: 20px;
            padding-bottom: 10px;
            width: 170px;
            position: relative;
            border-radius: 5px 5px 5px 5px;
            -moz-border-radius: 5px 5px 5px 5px;
            -webkit-border-radius: 5px 5px 5px 5px;
            border: 0px solid #cccccc;
        }

        .credit-box {
            color: #666666;
            font-weight: lighter;
            float: left;
            margin-top: 5px;
            margin-left: 30px;
            margin-right: 30px;
            padding-top: 15px;
            padding-left: 20px;
            padding-right: 20px;
            position: relative;
            border-radius: 5px 5px 5px 5px;
            -moz-border-radius: 5px 5px 5px 5px;
            -webkit-border-radius: 5px 5px 5px 5px;
            border: 0px solid #cccccc;
        }

        .neraca td {
          text-align:center;
        }



        </style>
		<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="workspace" align="center">
            <h1 style="font-size:18px;">STRUKTUR FASILITAS KREDIT</h1>
            <br>
            <hr>

			<table style="width: 1053.83px;">
			<tbody>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">a. Jenis Kredit</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><?=$proc_name;?> (<?=$custproccode;?>)</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">b. Maksimum kredit</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;">Rp <?=number_format($plafond);?></td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">c. Penggunaan Kredit</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><?=$custcredituse;?></td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">d. Tarif Bunga</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><?=$interest;?> % </td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">e. Provisi</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><?=$provisi;?> % </td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">f. Biaya administrasi</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;">Rp <?=number_format($mkknewadm);?></td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">g. Jangka waktu</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><?=$tenor;?> Bulan</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">h. Angsuran Perbulan</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;"><b>Rp <?=number_format($installment);?>,- fix sampai dengan jatuh tempo</b></td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;" valign="top">i. Jaminan</td>
			<td style="width: 10px; height: 15px; text-align: justify;" valign="top">:</td>
			<td style="width: 1689.83px; height: 15px; text-align: justify;">
			<?if($custproccode=="KGS"){?>
			<?=$col_addr1;?> dengan Luas Bangunan/Tanah <?=$col_imbluas;?>/<?=$col_certluas;?>M2 <?=$col_certtype;?> No. <?=$col_certno;?> TgI. <?=$col_certdate->format('d-m-Y');?>, An. <?=$col_certatasnama;?> (Akan dibalik nama atas nama pemohon).
			<?}else{}?>
			</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;">j. Pengikatan Jaminan</td>
			<td style="width: 10px; height: 15px;">:</td>
			<td style="width: 1689.83px; height: 15px;">APHT sebesar Rp. 110.000.000,-</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 226px; height: 15px;" valign="top">k. Asuransi</td>
			<td style="width: 10px; height: 15px;" valign="top">:</td>
			<td style="width: 1689.83px; height: 15px;">Askrindo Bundling (Pertahun)</br> 1. Asuransi Jiwa </br> 2. Asuransi Kredit </br> 3. Asuransi Kebakaran dengan Banker's Clause</td>
			</tr>
			</tbody>
			</table>
			<table style="width: 1054px;">
			<tbody>
			<tr style="height: 15px;">
			<td style="width: 2591px; height: 15px;" colspan="2">l. Syarat Penandatanganan PK :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;" valign="top">1.</td>
			<td style="width: 2549.45px; height: 15px; text-align: justify;">Penandatangan Perjanjian Kredit dilakukan setelah terdapat persetujuan dari Kementerian Pekerjaan Umum dan Perumahan Rakyat atas permohonan Fasilitas Likuiditas Pembiayaan Perumahan.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;">2.</td>
			<td style="width: 2549.45px; height: 15px; text-align: justify;">Telah menandatangani Surat Penegasan Pernyataan Persetujuan Kredit (SP3K) dan telah mengembalikan tembusan SP3K tersebut kepada Bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;">3.</td>
			<td style="width: 2549.45px; height: 15px; text-align: justify;">Menunjukkan kepada Bank Sumsel Babel asli dokumen-dokumen sebagai berikut:</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;">&nbsp;</td>
			<td style="width: 2549.45px; height: 15px; vertical-align: top;">a. KTP/SIM/Paspor debitur dan suami/istri (apabila telah menikah) berikut kartu keluarga. <br />b. Akta Nikah/Cerai/Surat Kematian (bagi yang telah menikah/cerai). <br />c. Akta Perjanjian Kawin (apabila dalam pernikahan debitur terdapat perjanjian pemisahan harta). <br />d. Buku tabungan Bank Sumsel Babel atau bukti pembukaan rekening giro.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">4.</td>
			<td style="width: 2549.45px; height: 15px;" valign="top">Menyerahkan kepada Bank Sumsel Babel asli dokumen-dokumen sebagai berikut:</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">&nbsp;</td>
			<td style="width: 2549.45px; height: 15px;" valign="top">a. Copy kuintansi dan bukti setoran pelunasan uang muka (down payment) yangtelah divalidasi bank. <br />b. Copy brosur dan site plan. <br />c. Surat kuasa memotong gaji dan surat pernyataan kesediaan <br />e. Surat Pernyataan yang memuat. keterangan mengenai fasilitas kredit pembiayaan properti yang sudah diterima maupun yang sedang dalam proses pengajuan pada bank lain. <br />f. Surat pernyataan yang berisi apabila debitur berhenti, maka fasilitas kredit wajib untuk dilunasi. Jika debitur berpindah tugas masih dalam wilayah Sumsel Babel maka debitur wajib memberitahukan kepada Bank Sumsel Babel paling lambat 2 hari setelah dikeluarkan surat pindah tugas serta dilengkapi dengan surat penyataan yang diketahui oleh atasan baru. <br />g. Surat pernyataan yang diketahui oleh atasan debitur bahwa apabila debitur menunggak maka bersedia surat peringatan dapat ditembuskan kepada atasan debitur. <br />h. Surat pernyataan dari developer bahwa apabila terjadi wanprestasi maka dana buyback wajib telah tersedia pada giro developer di Bank Sumsel Babel maksimal 7 (tujuh) hari sebelum dilakukan transaksi buyback, yang waktunya ditentukan oleh pihak bank. <br />i. Kuasa memblokir rekening giro atas nama PT Golden Artha Sriwijaya</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">5.</td>
			<td style="width: 2549.45px; height: 15px;" valign="top">Agunan yang diserahkan ke Bank Sumsel Babel dapat dilakukan pengikatan secara notariel oleh notaris rekanan bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">6.</td>
			<td style="width: 2549.45px; height: 15px;" valign="top">Penandatanganan kredit dilakukan bersama suami/istri dan dihadapan pejabat berwenang.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;" colspan="2">m. Syarat Realisasi Kredit :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">&nbsp;1.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah menandatangani Perjanjian Kredit berikut lampirannya.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">2.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah melunasi biaya-biaya yang terkait dengan pembukaan kredit.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">3.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah melaksanakan proses balik nama dan pengikatan agunan berupa SKMHT/APHT dari notaris rekanan bank</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">4.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah menyerahkan covernote pengurusan pemecahan, proses balik nama, penyerahan sertifikat dan pengikatan agunan berupa SKMHT/APHT dari notaris rekanan bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">5.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah menyerahkan IMB/IPB asli atau induk yang telah dilegalisir dari instansi terkait.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">6.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah menyerahkan surat pernyataan debitur bahwa telah menerima rumah dalam keadaan selesai 100% dan siap huni.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">7.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Petugas bank melakukan kunjungan setempat dan membuat laporan kunjungan guna memastikan rumah telah selesai.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">8.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Telah menyerahkan akte notariil perjanjian pembelian kembali (buyback) dari developer PT Golden Artha Sriwijaya.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">9.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">Pencairan tersebut dengan tahapan pencairan sebagai berikut :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: right;">&nbsp;</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top; text-align: left;">
			<p>a. Sebesar 90% (sembilan puluh persen) dari nominal KPR setelah berita acara serah terima bangunan dan disetor ke rekening giro atau rekening pinjaman developer</p>
			<p>b. Sebesar 10% (sepuluh persen) dari nominal KPR dan disetor ke rekening titipan sebagai dana retensi dan dicairkan ke rekening giro atau rekening pinjaman developer (jika debitur) dengan kondisi :</p>
			<p>&nbsp;&nbsp; - Sebesar 5% (lima persen) setelah penerimaan SHM/SHGB an debitur, IMB, A3B, SKMHT/APHT dan polis asuransi kebakaran (tahun pertama)</p>
			<p>&nbsp;&nbsp; - Sebesar 5% (lima persen) setelah masa pemeliharaan 60 (enam puluh hari) kalender sejak serah terima bangunan termasuk pembangunan sarana jalan dalam komplek perumahan</p>
			</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px;" colspan="2">n. Lain-lain :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">&nbsp;1.</td>
			<td style="width: 41.55px; height: 15px; vertical-align: top;">Guna menindaklanjuti UU Anti Korupsi serta dalam rangka penerapan good corporate governance, pemohon menyatakan tanpa dibatalkan, dicabut kembali Pemimpin Divisi, karyawan/karyawati ..PT Bank Sumsel Babel antara lain tetapi tidak terbatas pada pemberian dalam bentuk fasilitas dan/atau dapat menyebabkan keuntungan pribadi dan/atau kelompoknya yang diduga dan/atau dapat diduga secara langsung atau tidak langsung berkaitan dengan fasilitas yang diberikan oleh PT Bank Sumsel Babel kepada debitur.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;">2.</td>
			<td style="width: 41.55px; height: 15px;">Penutupan asuransi penjaminan kredit, asuransi jiwa dan asuransi kerugian serta pengikatan agunan pada perusahaan asuransi I notaris yang ditunjuk Bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right;">3.</td>
			<td style="width: 41.55px; height: 15px;">Biaya asuransi menjadi beban bank dan biaya pengikatan beban calon debitur.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">4.</td>
			<td style="width: 41.55px; height: 15px;">Selama pinjaman belum lunas, debitur diharapakan untuk menyampaikan pemberitahuan terlebih dahulu kepada Bank Sumsel Babel apabila menerima fasilitas kredit baru balk dari Bank lain maupun lembaga keuangan lainnya.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">5.</td>
			<td style="width: 41.55px; height: 15px;">Calon debitur diwajibkan membuka tabungan dan menabung sebagai pemupukan angsuran kredit dan memelihara setiap bulannya sampai dengan kredit lunas dengan saldo tabungan beku sebesar 1 kali angsuran kredit.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">6.</td>
			<td style="width: 41.55px; height: 15px;">Apabila terjadi tunggakan sebanyak 1 (satu) kali angsuran pokok dan bunga, maka Bank berhak :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">&nbsp;</td>
			<td style="width: 41.55px; height: 15px;">
			<p>a Melakukan penagihan langsung kepada debitur dengan memberikan surat peringatan.</p>
			<p>b. Apabila tidak ada penyelesaian dari debitur, selama 3 (tiga) bulan berturut &mdash; turut maka Bank berhak memerintahkan debitur untuk menyerahkan dan mengosongkan rumah yang dibiayai dan menjadi jaminan kredit selambat &mdash; lambatnya 7 (tujuh) hari kalender sejak tanggal surat perintah.</p>
			<p>c. Apabila debitur tidak menyerahkan dan mengosongkan rumah dalam Batas waktu yang dimaksud diatas, Bank berhak segera dan seketika mengosongkan rumah tersebut dan dapat dimintakan bantuan kepada pihak berwenang.</p>
			<p>d. Eksekusi barang jaminan dapat dilakukan oleh Bank atau melalui instansi yang berwenang.</p>
			</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">7.</td>
			<td style="width: 41.55px; height: 15px;">Sewaktu-waktu pihak Bank Sumsel Babel :</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">&nbsp;</td>
			<td style="width: 41.55px; height: 15px;">a. Dapat merubah tingkat suku bunga kredit, denda dan biaya-biaya lainnya sesuai dengan ketentuan yang berlaku pada bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">&nbsp;</td>
			<td style="width: 41.55px; height: 15px;">b. Dapat mendebet rekening pinjaman dan atau giro serta rekening simpanan lainnya atas nama pemohon di Bank Sumsel Babel yang akan dipergunakan untuk pembayaran angsuran pokok, bunga, denda, dan biaya-biaya yang timbul dikemudian hari yang berkaitan dengan fasilitas kredit yang berjaian.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">8.</td>
			<td style="width: 41.55px; height: 15px;">Provisi, biaya administrasi serta biaya-biaya lainnya sehubungan dengan pemberian kredit ini, yang telah disetor tidak dapat ditarik kembali dengan alasan apapun kecuali ditentukan lain oleh bank.</td>
			</tr>
			<tr style="height: 15px;">
			<td style="width: 41.55px; height: 15px; text-align: right; vertical-align: top;">9.</td>
			<td style="width: 41.55px; height: 15px;">Syarat lainnya sesuai dengan ketentuan pemberian kredit yang berlaku di Bank Sumsel Babel.</td>
			</tr>
			</tbody>
			</table>
			<p>&nbsp;</p>
			<p style="text-align: center;">Disiapkan oleh</p>
			<p style="text-align: center;">Tanggal <?=strtoupper($txn_time->format('d M Y'));?></p>
			<table style="width: 1058px;">
			<tbody>
			<tr>
			<td style="width: 553.817px; text-align: center;">Unit Bisnis</td>
			<td style="width: 508.183px; text-align: center;">Unit Resiko Kredit</td>
			</tr>
			<tr>
			<td style="width: 553.817px; text-align: center;">&nbsp;</td>
			<td style="width: 508.183px; text-align: center;">&nbsp;</td>
			</tr>
			<tr>
			<td style="width: 553.817px; text-align: center;"><span style="text-decoration: underline;"><strong><?=strtoupper($txn_user_id);?></strong></span></td>
			<td style="width: 508.183px; text-align: center;"><span style="text-decoration: underline;"><strong><?=strtoupper($txn_user_id);?></strong></span></td>
			</tr>
			<tr>
			<td style="width: 553.817px; text-align: center;">Analis Kredit</td>
			<td style="width: 508.183px; text-align: center;">Analis Resiko Kredit</td>
			</tr>
			</tbody>
			</table>
			<p>&nbsp;</p>

		</div>
		<form id="frm" name="frm" action="do_saveflowview.php" method="post">
		<?require("../../requirepage/hiddenfield.php");?>
		<?
		if($userid != ""&&$userwfid!="")
		{
		?>
		<div>&nbsp;</div>
		<div style="text-align:center;margin:15px;">
		<?require('../../requirepage/btnview.php');

		require("../../requirepage/btnprint.php");
		}
		?>

		</div>
		</form>
	</body>
</html>
