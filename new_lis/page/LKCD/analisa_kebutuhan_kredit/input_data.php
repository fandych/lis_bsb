<?php
error_reporting(E_ALL & ~E_NOTICE);
//error_reporting(0);
require("../../../lib/sqlsrv.lis.php");
require("../../../lib/class.sqlserver.php");
$db_lis = new DB_LIS();
$db_lis->connect();

$custnomid = $_REQUEST['custnomid'];

$def_year = date('Y');
$def_month = date('m');
$def_source = "LKCD";

$customer_info = "SELECT * FROM tbl_CustomerMasterPerson2 WHERE custnomid = '$custnomid'";
$db_lis->executeQuery($customer_info);
$customer_info = $db_lis->lastResults;

echo "<pre>";
//print_r($db->lastResults);
echo "</pre>";

$group = $customer_info[0]['custtarid'];

$header = "select * from KEU_HEADER WHERE _group = '$group' order by _sequence";
$db_lis->executeQuery($header);
$result_header = $db_lis->lastResults;


//echo "<pre>";
//print_r($result_header);
//echo "</pre>";

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>INPUT REPAYMENT CAPACITY</title>


    <style>
    body {
    	padding:0px;
    	margin:0px;
    	font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    td {
    	text-align:center;
    }

    input:focus {
        background-color:#FFFFFF;
    }
    </style>


    <script type="text/javascript" src="../../../js/full_function.js" ></script>
    <script type="text/javascript" src="../../../js/accounting_neraca.js" ></script>
</head>
<body>
    <div align="center">
        <div id="content">
            <form method="post" action="input_data_process.php">
                <?
                $count_data = "SELECT DISTINCT _year, _month, _source FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _source = '$def_source'";
                //echo $count_data;
                $db_lis->executeQuery($count_data);
                $result_count_data= $db_lis->lastResults;

                echo "<pre>";
                //print_r($db_lis->lastResults);
                echo "</pre>";
                ?>
                <table border="1" cellpadding="5" cellspacing="0">

    			<tr>
    				<td colspan="<?=count($result_count_data)+3?>">INPUT REPAYMENT CAPACITY</td>
    			</tr>
    			<tr>
    				<td colspan="<?=count($result_count_data)+3?>">&nbsp;</td>
    			</tr>

    			<tr>
    				<td width="300px" colspan="2">&nbsp;</td>
                    <?
                    for($z=0;$z<count($result_count_data);$z++)
                    {
                        $year = $result_count_data[$z]['_year'];
                        $month = $result_count_data[$z]['_month'];
                        $source = $def_source;

                        $monthNum  = $month;
                        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                        $monthName = $dateObj->format('F');

                        ?>
                        <td>
                            <div align="center">
                                <font style="font-size:12px">
                                    <?=$year;?> / <?=$monthName;?>
                                </font>
                            </div>
                        </td>
                        <?
                    }
                    ?>
    				<td width="150px">
                        <input type="text" style="text-align:center;width:25%;background-color:#E5F2FF" id="input_year" name="input_year" maxlength="4" onkeypress="return event.charCode >= 48 && event.charCode <= 57" value="<?=$def_year;?>"/>

                        <select id="input_month" name="input_month" >
                            <?
                            for($m=1;$m<=12;$m++)
                            {
                                $monthNum  = $m;
                                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                $monthName = $dateObj->format('F');

                                if($m == $def_month) {
                                    $selected = "selected";
                                }
                                else {
                                    $selected = "";
                                }

                                ?>
                                <option value="<?=$m;?>" <?=$selected;?>><?=$monthName;?></option>
                                <?
                            }
                            ?>
                        </select>
                    </td>
    			</tr>

                <?
    			for($x=0;$x<count($result_header);$x++)
    			{
    				$header_code = $result_header[$x]['_code'];
    				$header_show = $result_header[$x]['_name'];

        			?>
        			<tr>
        				<td colspan="<?=count($result_count_data)+3?>" style="text-align:left"><font style="font-weight:bold;font-size:16px"><?=strtoupper($header_show);?></font></td>
        			</tr>

        			<?

    				$detail = "select * from KEU_DETAIL where _code_header = '$header_code' order by _sequence";
    				$db_lis->executeQuery($detail);
    				$result_detail = $db_lis->lastResults;

                    $array_temp_value_code = array();


    				for($y=0;$y<count($result_detail);$y++)
    				{
    					$detail_code = $result_detail[$y]['_code_detail'];
    					$detail_type = $result_detail[$y]['_type'];

                        if($detail_type == "VALUE")
                        {
                            //VALUE

                            array_push($array_temp_value_code, $detail_code);

    					    $detail_show = $result_detail[$y]['_name'];
                            ?>
                            <tr>
                                <td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
                                <td>:</td>

                                <?
                                for($z=0;$z<count($result_count_data);$z++)
                                {
                                    $year = $result_count_data[$z]['_year'];
                                    $month = $result_count_data[$z]['_month'];
                                    $source = $def_source;

                                    $get_value = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$detail_code' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
            						$db_lis->executeQuery($get_value);
            						$result_value = $db_lis->lastResults;
            						$value = $result_value[0]['_value'];

                                    ?>
                                    <td width="100px" style="text-align:right;font-size:11px;"><div align="right" sty><?=number_format($value,2);?></div></td>
                                    <?
                                }
                                ?>

                                <td><input type="text" style="text-align:right;width:95%;background-color:#E5F2FF" id="<?=$detail_code;?>" name="<?=$detail_code;?>" onblur="currency(this.id);" value="0"/></td>
                            </tr>
                            <?

                        }
                        else if ($detail_type == "FORMULA")
                        {
                            //FORMULA
    						$detail_show = "<b>".$result_detail[$y]['_name']."</b>";

    						?>
    						<tr>
    							<td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
    							<td>:</td>
                                <?
                                for($z=0;$z<count($result_count_data);$z++)
                                {
                                    $year = $result_count_data[$z]['_year'];
                                    $month = $result_count_data[$z]['_month'];
                                    $source = $def_source;

                                    $formula_sql = "select * from KEU_FORMULA where _code_detail = '$detail_code'";
            						$db_lis->executeQuery($formula_sql);
            						$result_formula = $db_lis->lastResults;

            						$total_formula = 0;

            						if(isset($result_formula[0]['_formula']))
            						{
            							$formula = $result_formula[0]['_formula'];

            							$chars = str_split($formula);

            							$t=0;
            							//echo $formula."<br>";
            							$formula = "";
            							$save = "";

            							$temp = "";

            							foreach($chars as $char)
            							{
            								$t++;

            								if($char == "+" || $char == "-" || $char == "*" ||  $char == "/")
            								{
            									if($temp == "(" || $temp == ")")
            									{
            										$formula .= $char;
            									}
            									else
            									{
            										$check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
            										$db_lis->executeQuery($check);
            										$result_check = $db_lis->lastResults;
            										$another = $result_check[0]['_value'];

            										$formula .= $another . $char;
            									}

            									$save = "";
            								}
            								else if($char == "(")
            								{
            									$formula .= $char;

            									$save = "";
            									$temp = "(";

            								}
            								else if($char == ")")
            								{
            									$check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
            									$db_lis->executeQuery($check);
            									$result_check = $db_lis->lastResults;

                                                if(count($result_check) > 0)
                								{
                									$another = $result_check[0]['_value'];
                								}
                								else {
                									$another = "";
                								}

            									$formula .= $another . $char;

            									$save = "";
            									$temp = ")";
            								}
            								else
            								{
            									$save .= $char;

            									if($t == count($chars))
            									{
            										$check = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$save' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
            										$db_lis->executeQuery($check);
            										$result_check = $db_lis->lastResults;
            										$another = $result_check[0]['_value'];

            										$formula .= $another;
            									}

            									$temp = $save;
            								}
            							}

            							//echo $formula."<br>";

            							eval( '$total_formula = ('.$formula.');');
            						}
            						else
            						{
            							$formula = "";
            						}

                                    ?>
        							<td style="text-align:right;font-weight:bold;font-size:12px;"><?=number_format($total_formula,2);?></td>
                                    <?
                                }
                                $formula_sql = "select * from KEU_FORMULA where _code_detail = '$detail_code'";
                                $db_lis->executeQuery($formula_sql);
                                $result_formula = $db_lis->lastResults;
                                if(isset($result_formula[0]['_formula'])) {
                                    $eval_formula = $result_formula[0]['_formula'];
                                }
                                else {
                                    $eval_formula = " UNIDENTIFIED ";
                                }
                                ?>
                                <td style="text-align:right;font-weight:bold;font-size:12px;"><span id="<?=$detail_code;?>_result"></span>&nbsp;<button type="button" onclick="<?=$detail_code;?>_calculate();">calculate</button></td>
                                <script>
                                function <?=$detail_code;?>_calculate()
                                {
                                    <?
                                    foreach($array_temp_value_code as $key => $value)
                                    {
                                        ?>
                                        var <?=$value;?> = parseFloat(document.getElementById('<?=$value;?>').value.replaceAll(',',''));
                                        <?
                                    }
                                    echo "var result = eval(\"" .  $eval_formula . "\");";
                                    ?>
                                    console.log(result);
                                    document.getElementById('<?=$detail_code;?>_result').innerHTML = format_currency(result,'');
                                }

                                </script>
    						</tr>
    						<?

    					}
						else if($detail_type == "SOURCE")
                        {
                            //SOURCE

                            array_push($array_temp_value_code, $detail_code);

    						$detail_show = "<b>".$result_detail[$y]['_name']."</b>";

                            $source_sql = "select * from KEU_SOURCE where _code_detail = '$detail_code'";
                            $db_lis->executeQuery($source_sql);
                            $result_source = $db_lis->lastResults;

                            if(isset($result_source[0]['_source'])) {
                                $source = $result_source[0]['_source'];

                                $arr_source = explode(".",$source);

                                $src_table = $arr_source[0];
                                $src_field = $arr_source[1];
                                $src_key = $arr_source[2];
                                $src_row = $arr_source[3];
                                $src_fm = $arr_source[4];


                                if($src_row == "MULTIROW")
                                {
                                    if($src_fm == "SUM")
                                    {
                                        $fm = "SELECT SUM(CAST($src_field AS INT)) AS RESULT FROM $src_table WHERE $src_key = '$custnomid';";

                                    }
                                    else if ($src_fm == "AVERAGE") {

                                        $fm = "SELECT AVG(CAST($src_field AS INT)) AS RESULT FROM $src_table WHERE $src_key = '$custnomid';";
                                    }
                                    else {
                                        $fm = "SELECT 'PARAMETER FORMULA NOT SET' AS RESULT";
                                    }
                                }
                                else if($src_row == "ONEROW") {

                                    $fm = "SELECT CAST($src_field AS INT) AS RESULT FROM $src_table WHERE $src_key = '$custnomid';";
                                }
                                else {
                                    $fm = "SELECT 'PARAMETER ROW NOT SET' AS RESULT";
                                }

                                try {
									
                                    $db_lis->executeQuery($fm);
                                    $result_fm= $db_lis->lastResults;
                                    $result_source = $result_fm[0]['RESULT'];

                                } catch (Exception $e) {

                                    $result_source = "QUERY DATA FAILED";
                                }

                            }
                            else {
                                $source = " UNIDENTIFIED ";

                                $result_source = "PARAMETER DATA NOT SET";
                            }

    						?>
    						<tr>
    							<td style="text-align:left;font-size:11px"><?=strtoupper($detail_show);?></td>
    							<td>:</td>

                                <?
                                for($z=0;$z<count($result_count_data);$z++)
                                {
                                    $year = $result_count_data[$z]['_year'];
                                    $month = $result_count_data[$z]['_month'];
                                    $source = $def_source;

                                    $get_value = "SELECT * FROM KEU_TRANS WHERE _custnomid = '$custnomid' AND _code_detail = '$detail_code' AND _year = '$year' AND _month = '$month' AND _source = '$source'";
                                    $db_lis->executeQuery($get_value);
                                    $result_value = $db_lis->lastResults;
                                    $value = $result_value[0]['_value'];

                                    ?>
                                    <td width="100px" style="text-align:right;font-size:11px;"><div align="right" sty><?=number_format($value,2);?></div></td>
                                    <?
                                }
                                ?>

                                <td style="text-align:right;font-weight:bold;font-size:12px;">
                                    <input type="text" style="text-align:right;width:95%;background-color:#E5F2FF" id="<?=$detail_code;?>" name="<?=$detail_code;?>" readonly value="<?=number_format($result_source);?>"/>
                                </td>

    						</tr>
    						<?

                        }


                    }

                }
                ?>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <?
                    for($z=0;$z<count($result_count_data);$z++)
                    {
                        $year = $result_count_data[$z]['_year'];
                        $month = $result_count_data[$z]['_month'];
                        $source = $def_source;
                        ?>
                        <td><button type="submit" name="action" value="delete_<?=$year;?>_<?=$month;?>_<?=$source;?>" class="blue">X</button></td>
                        <?
                    }
                    ?>
                    <td><button type="submit" name="action" value="save" class="blue">save</button></td>
                </tr>
                </table>
        		<input type="hidden" name="input_source" value="<?=$def_source;?>">
        		<input type="hidden" name="custnomid" value="<?=$custnomid;?>">
            </form>
        </div>
    </div>
    <script>

   function format_currency(n, currency) {
       return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
   }

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
    </script>
</body>
</html>
