<?php

class SQLSRV
{
    //varibale global for get return query ( _RQ ) or execute query ( _EQ )
	var $_RS; // RETURN strsql
	var $_RR; // RETURN rows
	var $_REMOTE;
	var $_SHOWERROR;
	var $_RESULT;

    //data Remote
    var $_SERVER;
    var $_USERDB;

    //debug and language
    var $_DEBUGQUERY    = true;
    var $_LANGUAGE      = "0";//0=return null 1=LOCAL; 2=ENGLISH

    //debug from api server

    function _ON($server,$user,$password,$usedb)
	{
        //$multidb => for using more than 1 db in 1 server

        $this->_REMOTE = sqlsrv_connect( $server, array( "Database"=>$usedb, "UID"=>$user, "PWD"=>$password ));
		if (!$this->_REMOTE)
		{
            echo "
            <div style=\"font-weight:bold;color:red;font-size:12pt;border:0px solid black;padding:10px;display:inline-block;\">
            Wrong Parameter Connection To Database</div>";

		}
        else
        {
            $this->_SERVER= $server;
            $this->_USEDB= $usedb;
        }

	}

    function _EQ($strsql)
	{

		$stmt = sqlsrv_prepare( $this->_REMOTE, $strsql);


		if (!$stmt || !sqlsrv_execute( $stmt))
		{
            if($this->_DEBUGQUERY)
            {
                $this->_RESULT = $this->_EM($strsql);
				echo $this->_RESULT;
            }
		}
        else
        {
            echo $this->_LANGUAGE();
        }
	}

    function _RQ($strsql)
	{
		$cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);

		$result = sqlsrv_query($this->_REMOTE, $strsql, $params, $cursortype);

		if (!$result)
		{
			if($this->_DEBUGQUERY)
            {
               $this->_RESULT = $this->_EM($strsql);
            }
		}
		else
		{
			$this->_RS = array();
			$ResultRow = sqlsrv_num_rows($result);

			while ($rows = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
			{
				$this->_RS[] = $rows;
			}
			$this->_RR = $ResultRow;

            if($this->_RR > 0)
            {
                $this->_RESULT = $this->_RS;
            }
            else
            {
                $this->_LANGUAGE();
            }
			sqlsrv_free_stmt($result);
		}
        return $this->_RESULT;
	}


	function _FE( $errors )
	{
        return $errors[0]['message']."<br/>";
	}

	function _EM($strsql){
		return "<div style=\"font-weight:bold;color:red;font-size:12pt;border:0px solid black;padding:10px;display:inline-block;\">
                Could not successfully run query on server ".$this->_SERVER." at database ".$this->_USERDB.": </br>"
                .$strsql."</br>".$this->_FE(sqlsrv_errors())."</div>";
	}

	function _LANGUAGE(){
		if($this->_LANGUAGE=="0")
		{
			$this->_RESULT = "";
		}
		else if($this->_LANGUAGE=="1")
		{
			$this->_RESULT = "Tidak Ada Data";
		}
		else if($this->_LANGUAGE=="2")
		{
			$this->_RESULT = "No Data";
		}
	}
}


function get1data($condb,$strsql)
{
    $return ="";
	if($strsql!="")
	{
		$getlastsubstring = strpos($strsql, "from")-6;
		$getarrayname = trim(substr($strsql,6,$getlastsubstring));
		$rs = $condb->_RQ($strsql);
		if(!is_array($rs)){$return = "";}else{$return=$rs[0][$getarrayname];}
	}
    return $return;
}


$db = "DB_LIS_BSB";

$condb = new SQLSRV();
$condb->_ON("127.0.0.1","user","user",$db);


/*
$rs = $condb->_RQ("select _name,_idx from tr_template");
if(!is_array($rs)){echo $rs;}else
{
	foreach($rs as $key => $value)
	{
		$namavariable = $rs[$key]['_idx'];
	}
}

or

$rs = $condb->_RQ("select _name,_idx from tr_template");
if(!is_array($rs)){echo $rs;}else
{
	for($i=0;$i<count($rs);$i++)
	{

		$namavariable = $rs[$i]['_idx'];
	}
}

*/
