<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");

$nan=$_POST['nan'];

if($nan == "MKK")
{
	$id = $_POST['id'];
	$custnomid = $_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$buttonaction = $_POST['buttonaction'];
	$userwfid = "MKK";
	$userpermission = "I";
	
	$mkk_kebutuhan = $_POST['mkk_kebutuhan'];
	$mkk_character = $_POST['mkk_character'];
	$mkk_condition = $_POST['mkk_condition'];
	$mkk_capacity = $_POST['mkk_capacity'];
	$mkk_capital = $_POST['mkk_capital'];
	$mkk_collateral = $_POST['mkk_collateral'];
	
	$tsql = "SELECT * FROM Tbl_MKKAnalisa5c where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($a))
	{  
		$tsqlc = "DELETE FROM Tbl_MKKAnalisa5c WHERE CUSTNOMID = '$custnomid'";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
		
		$tsqlc = "INSERT INTO Tbl_MKKAnalisa5c (custnomid, analisa5c_kebutuhan, analisa5c_character, analisa5c_condition, analisa5c_capacity, analisa5c_capital, analisa5c_collateral) 
		VALUES ('$custnomid', '$mkk_kebutuhan', '$mkk_character', '$mkk_condition', '$mkk_capacity', '$mkk_capital', '$mkk_collateral')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	else
	{
		$tsqlc = "INSERT INTO Tbl_MKKAnalisa5c (custnomid, analisa5c_kebutuhan, analisa5c_character, analisa5c_condition, analisa5c_capacity, analisa5c_capital, analisa5c_collateral) 
		VALUES ('$custnomid', '$mkk_kebutuhan', '$mkk_character', '$mkk_condition', '$mkk_capacity', '$mkk_capital', '$mkk_collateral')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	
	// GET ORIGINAL BRANCH
	$originalregion = $userregion;
	$originalbranch = $userbranch;
	$originalao = $userid;
	if ($userwfid != "START")
	{
		$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
						WHERE txn_id='$custnomid'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}

		if(sqlsrv_has_rows($sqlConn))
		{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$originalbranch = $row[0];
			$originalregion = $row[1];
			$originalao = $row[2];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	}
	
	$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
	$c = sqlsrv_query($conn, $tsqlc);
}
else if($nan == "MKKFLOW")
{
	$custnomid = $_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$buttonaction = $_POST['buttonaction'];
	$userwfid = "MKK";
	$userpermission = "C";
	
	$Notes = $_POST['rekomendasikreditofficer'];
	$selectrekomendasikreditofficer = $_POST['selectrekomendasikreditofficer'];
	
	// GET ORIGINAL BRANCH
  $originalregion = $userregion;
  $originalbranch = $userbranch;
  $originalao = $userid;
  if ($userwfid != "START")
  {
		$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
					 	WHERE txn_id='$custnomid'";
   	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   	$params = array(&$_POST['query']);
   	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	
		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}
	
		if(sqlsrv_has_rows($sqlConn))
		{
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
      {
  			$originalbranch = $row[0];
  			$originalregion = $row[1];
        $originalao = $row[2];
      }
   	}
   	sqlsrv_free_stmt( $sqlConn );
  }
	
	$tsql = "SELECT * FROM Tbl_FMKK where txn_id = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($a))
	{  
		$tsqlc = "DELETE FROM Tbl_FMKK WHERE txn_id = '$custnomid'";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
		
		$tsqlc = "INSERT INTO Tbl_FMKK ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code]) 
		VALUES ('$custnomid','$userpermission',getdate(),'$userid','$Notes', '$originalbranch','$originalregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	else
	{
		$tsqlc = "INSERT INTO Tbl_FMKK ([txn_id],[txn_action],[txn_time],[txn_user_id],[txn_notes],[txn_branch_code],[txn_region_code]) 
		VALUES ('$custnomid','$userpermission',getdate(),'$userid','$Notes', '$originalbranch','$originalregion')";
		$c = sqlsrv_query($conn, $tsqlc);
		if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	}
	
	$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
	$c = sqlsrv_query($conn, $tsqlc);
	
	$tsqlc = "UPDATE Tbl_MKKAnalisa5c SET analisa5c_rekomendasi='$selectrekomendasikreditofficer', analisa5c_notes='$Notes' WHERE CUSTNOMID = '$custnomid'";
	$c = sqlsrv_query($conn, $tsqlc);
	if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
}
else if($nan == "VIEWMKK")
{
	$custnomid = $_POST['custnomid'];
	$userid = $_POST['userid'];
	$userpwd = $_POST['userpwd'];
	$userbranch = $_POST['userbranch'];
	$userregion = $_POST['userregion'];
	$buttonaction = $_POST['buttonaction'];
	$userwfid = "MKK";
	$userpermission = "A";
	
	
	$tsqlc = "UPDATE tbl_FMKK SET txn_action='$userpermission', txn_time=getdate() WHERE txn_id = '$custnomid'";
	$c = sqlsrv_query($conn, $tsqlc);
	if ( $c === false)die( FormatErrors( sqlsrv_errors() ) );
	
	// GET ORIGINAL BRANCH
	$originalregion = $userregion;
	$originalbranch = $userbranch;
	$originalao = $userid;
	if ($userwfid != "START")
	{
		$tsql = "SELECT txn_branch_code,txn_region_code,txn_user_id FROM Tbl_FSTART
						WHERE txn_id='$custnomid'";
		$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
		$params = array(&$_POST['query']);
		$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

		if($sqlConn === false)
		{
			die(FormatErrors(sqlsrv_errors()));
		}

		if(sqlsrv_has_rows($sqlConn))
		{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			$originalbranch = $row[0];
			$originalregion = $row[1];
			$originalao = $row[2];
		}
	}
	sqlsrv_free_stmt( $sqlConn );
	}
	
	$tsqlc = "INSERT INTO Tbl_Txn_History VALUES('$custnomid','$userpermission',getdate(),'$userid','','$userwfid', '$originalbranch','$originalregion')";
	$c = sqlsrv_query($conn, $tsqlc);
	
	include ("../../requirepage/do_saveflow.php");
	
}

?>