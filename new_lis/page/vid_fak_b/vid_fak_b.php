<?
	
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");
	//require ("../../requirepage/security.php");
	
	$taginput = "";
	$tagselect = "";
	$warna = "id=\"tblform\"";
	$button = "type=\"button\"";
	$tampil = "1";
	
	if(isset($_REQUEST['view']))
	{
		if($_REQUEST['view'] == "preview")
		{
			$taginput = "readonly=\"readonly\"";
			$taginput = "disabled=\"disabled\"";
			$tagselect = "disabled=\"disabled\"";
			$warna = "class=\"preview2\"";
			$button = "type=\"hidden\"";
			$tampil = "0";
		}
	}
	
	$custbussiup="";
	$custbussiup="";
	$custapldate="";
	$custproccode="";
	$custfullname="";
	$custsex="";
	$custnpwpno="";
	$custcity="";
	$custprop="";
	$custzipcode="";
	$custfax="";
	$custhomestatus="";
	$custhomeyearlong="";
	$custhomemonthlong="";
	$custbustype="";
	$custbusname="";
	$custkec="";
	$custbusaddr="";
	$custbustelp="";
	$custbusfax="";
	$custbushp="";
	$custbusaktano="";
	$custbusaktadate="";
	$custbusnpwp="";
	$custbussiup="";
	$custbustdp="";
	$custbusyearlong="";
	$custbusmonthlong="";
	$custcreditstatus="";
	$custnomfrom="";
	$custbusnpwp="";
	$custnomperkenalan="";
	$custprostsusahacode="";
	$custprostskredit="";
	$custprointerview="";
	$custpropendapatan="";
	$custkel="";
	$custjabatancode="";
	$custprodate="";
	$flag="";


	$strsql="SELECT * from tbl_customermasterperson where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custapldate=$rows['custapldate'];
			$custproccode=$rows['custproccode'];
			$custfullname=$rows['custfullname'];
			$custsex=$rows['custsex'];
			$custnpwpno=$rows['custnpwpno'];
			$custkel=$rows['custkel'];
			$custkec=$rows['custkec'];
			$custcity=$rows['custcity'];
			$custprop=$rows['custprop'];
			$custzipcode=$rows['custzipcode'];
			$custfax=$rows['custfax'];
			$custjabatancode=$rows['custjabatancode'];
			$custhomestatus=$rows['custhomestatus'];
			$custhomeyearlong=$rows['custhomeyearlong'];
			$custhomemonthlong=$rows['custhomemonthlong'];
			$custbustype=$rows['custbustype'];
			$custbusname=$rows['custbusname'];
			$custbusaddr=$rows['custbusaddr'];
			$custbustelp=$rows['custbustelp'];
			$custbusfax=$rows['custbusfax'];
			$custbushp=$rows['custbushp'];
			$custbusaktano=$rows['custbusaktano'];
			$custbusaktadate=$rows['custbusaktadate'];
			$custbusnpwp=$rows['custbusnpwp'];
			$custbussiup=$rows['custbussiup'];
			$custbustdp=$rows['custbustdp'];
			$custbusyearlong=$rows['custbusyearlong'];
			$custbusmonthlong=$rows['custbusmonthlong'];
			$custcreditstatus=$rows['custcreditstatus'];
			$custnomfrom=$rows['custnomfrom'];
			$custnomperkenalan=$rows['custnomperkenalan'];
			$custprostsusahacode=$rows['custprostsusahacode'];
			$custprostskredit=$rows['custprostskredit'];
			$custprointerview=$rows['custprointerview'];
			$custpropendapatan=$rows['custpropendapatan'];
			$custprodate=$rows['custprodate'];
			$flag=$rows['flag'];
		}
	}
	

	$newcustbussiup="";
	$newcustbussiup="";
	$newcustapldate="";
	$newcustproccode="";
	$newcustfullname="";
	$newcustsex="";
	$newcustnpwpno="";
	$newcustcity="";
	$newcustprop="";
	$newcustzipcode="";
	$newcustfax="";
	$newcusthomestatus="";
	$newcusthomeyearlong="";
	$newcusthomemonthlong="";
	$newcustbustype="";
	$newcustbusname="";
	$newcustbusaddr="";
	$newcustbustelp="";
	$newcustbusfax="";
	$newcustbushp="";
	$newcustkel="";
	$newcustbusaktano="";
	$newcustbusaktadate="";
	$newcustbusnpwp="";
	$newcustbussiup="";
	$newcustbustdp="";
	$newcustbusyearlong="";
	$newcustbusmonthlong="";
	$newcustcreditstatus="";
	$newcustkec="";
	$newcustnomfrom="";
	$newcustnomperkenalan="";
	$newcustprostsusahacode="";
	$newcustprostskredit="";
	$newcustprointerview="";
	$newcustpropendapatan="";
	$newcustjabatancode="";
	$newcustprodate="";
	$newflag="";
	$strsql="SELECT * from tbl_customermasterperson2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$newcustapldate=$rows['custapldate'];
			$newcustproccode=$rows['custproccode'];
			$newcustfullname=$rows['custfullname'];
			$newcustsex=$rows['custsex'];
			$newcustnpwpno=$rows['custnpwpno'];
			$newcustcity=$rows['custcity'];
			$newcustkel=$rows['custkel'];
			$newcustprop=$rows['custprop'];
			$newcustzipcode=$rows['custzipcode'];
			$newcustfax=$rows['custfax'];
			$newcustjabatancode=$rows['custjabatancode'];
			$newcustkec=$rows['custkec'];
			$newcusthomestatus=$rows['custhomestatus'];
			$newcusthomeyearlong=$rows['custhomeyearlong'];
			$newcusthomemonthlong=$rows['custhomemonthlong'];
			$newcustbustype=$rows['custbustype'];
			$newcustbusname=$rows['custbusname'];
			$newcustbusaddr=$rows['custbusaddr'];
			$newcustbustelp=$rows['custbustelp'];
			$newcustbusfax=$rows['custbusfax'];
			$newcustbushp=$rows['custbushp'];
			$newcustbusaktano=$rows['custbusaktano'];
			$newcustbusaktadate=$rows['custbusaktadate'];
			$newcustbusnpwp=$rows['custbusnpwp'];
			$newcustbussiup=$rows['custbussiup'];
			$newcustbustdp=$rows['custbustdp'];
			$newcustbusyearlong=$rows['custbusyearlong'];
			$newcustbusmonthlong=$rows['custbusmonthlong'];
			$newcustcreditstatus=$rows['custcreditstatus'];
			$newcustnomfrom=$rows['custnomfrom'];
			$newcustnomperkenalan=$rows['custnomperkenalan'];
			$newcustprostsusahacode=$rows['custprostsusahacode'];
			$newcustprostskredit=$rows['custprostskredit'];
			$newcustprointerview=$rows['custprointerview'];
			$newcustpropendapatan=$rows['custpropendapatan'];
			$newcustprodate=$rows['custprodate'];
			$flag=$rows['flag'];
		}
	}
	
	
	$lifeinsurance="";
	$otherlifeinsurance="";
	$lostinsurance="";
	$otherlostinsurance="";
	$strsql="select * from Tbl_Customerinsurance where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$lifeinsurance=$rows['code_life'];
			$otherlifeinsurance=$rows['other_life'];
			$lostinsurance=$rows['code_loss'];
			$otherlostinsurance=$rows['other_loss'];
		}
	}
	
	$newlifeinsurance="";
	$newotherlifeinsurance="";
	$newlostinsurance="";
	$newotherlostinsurance="";
	$strsql="select * from Tbl_Customerinsurance2 where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$newlifeinsurance=$rows['code_life'];
			$newotherlifeinsurance=$rows['other_life'];
			$newlostinsurance=$rows['code_loss'];
			$newotherlostinsurance=$rows['other_loss'];
		}
	}
	
	$strsql = "select proc_code,proc_name,proc_active,proc_min_plafond,proc_max_plafond,proc_min_facility 
	from Tbl_Processing where proc_code='$custproccode'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$proc_name = $rows['proc_name'];
			$proc_min_plafond = $rows['proc_min_plafond'];
			$proc_max_plafond = $rows['proc_max_plafond'];
			$proc_min_facility = $rows['proc_min_facility'];
		}
	}
	
	
	$hslcodelife="Tidak Sesuai";
	$hslotherlife="Tidak Sesuai";
	$hslcodeloss="Tidak Sesuai";
	$hslotherloss="Tidak Sesuai";
	$hslflag="Tidak Sesuai";
	
	
	if($newlifeinsurance==$lifeinsurance){ $hslcodelife="Sesuai";}
	if($newotherlifeinsurance==$otherlifeinsurance){ $hslotherlife="Sesuai";}
	if($newlostinsurance==$lostinsurance){ $hslcodeloss="Sesuai";}
	if($newotherlostinsurance==$otherlostinsurance){ $hslotherloss="Sesuai";}

	
	$tdcustnomid="";
	$tdcodelife="";
	$tdotherlife="";
	$tdcodeloss="";
	$tdotherloss="";
	
	
		if(isset($_REQUEST['view']))
	{
		if($_REQUEST['view'] == "preview")
		{
		$tdcodelife="<td style=|width:100px;|>".$hslcodelife."</td>";
		$tdotherlife="<td style=|width:100px;|>".$hslotherlife."</td>";
		$tdcodeloss="<td style=|width:100px;|>".$hslcodeloss."</td>";
		$tdotherloss="<td style=|width:100px;|>".$hslotherloss."</td>";
		$tdflag="<td style=|width:100px;|>".$hslflag."</td>";
			
		}
	}
	
	$strsql = "select proc_code,proc_name,proc_active,proc_min_plafond,proc_max_plafond,proc_min_facility 
	from Tbl_Processing where proc_code='$custproccode'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$proc_name = $rows['proc_name'];
			$proc_min_plafond = $rows['proc_min_plafond'];
			$proc_max_plafond = $rows['proc_max_plafond'];
			$proc_min_facility = $rows['proc_min_facility'];
		}
	}
	
	
	
	$hslcustnomid="Tidak Sesuai";
	$hslcusttarid="Tidak Sesuai";
	$hslcustaplno="Tidak Sesuai";
	$hslcustapldate="Tidak Sesuai";
	$hslcustaocode="Tidak Sesuai";
	$hslcustbranchcode="Tidak Sesuai";
	$hslcustlkcdno="Tidak Sesuai";
	$hslcustproccode="Tidak Sesuai";
	$hslcustcurcode="Tidak Sesuai";
	$hslcustplafond="Tidak Sesuai";
	$hslcustfullname="Tidak Sesuai";
	$hslcustshortname="Tidak Sesuai";
	$hslemail="Tidak Sesuai";
	$hslcustsex="Tidak Sesuai";
	$hslcustjabatancode="Tidak Sesuai";
	$hslcustktpno="Tidak Sesuai";
	$hslcustktpexp="Tidak Sesuai";
	$hslcustboddate="Tidak Sesuai";
	$hslcustbodplace="Tidak Sesuai";
	$hslcustnpwpno="Tidak Sesuai";
	$hslcustmothername="Tidak Sesuai";
	$hslcusteducode="Tidak Sesuai";
	$hslcustmarcode="Tidak Sesuai";
	$hslcustmarname="Tidak Sesuai";
	$hslcustjmltanggungan="Tidak Sesuai";
	$hslcustaddr="Tidak Sesuai";
	$hslcustrt="Tidak Sesuai";
	$hslcustrw="Tidak Sesuai";
	$hslcustkel="Tidak Sesuai";
	$hslcustkec="Tidak Sesuai";
	$hslcustcity="Tidak Sesuai";
	$hslcustprop="Tidak Sesuai";
	$hslcustzipcode="Tidak Sesuai";
	$hslcusttelp="Tidak Sesuai";
	$hslcusthp="Tidak Sesuai";
	$hslcustfax="Tidak Sesuai";
	$hslcusthomestatus="Tidak Sesuai";
	$hslcusthomeyearlong="Tidak Sesuai";
	$hslcusthomemonthlong="Tidak Sesuai";
	$hslcustaddrktp="Tidak Sesuai";
	$hslcustrtktp="Tidak Sesuai";
	$hslcustrwktp="Tidak Sesuai";
	$hslcustkelktp="Tidak Sesuai";
	$hslcustkecktp="Tidak Sesuai";
	$hslcustcityktp="Tidak Sesuai";
	$hslcustpropktp="Tidak Sesuai";
	$hslcustzipcodektp="Tidak Sesuai";
	$hslcustbustype="Tidak Sesuai";
	$hslcustbusname="Tidak Sesuai";
	$hslcustbusaddr="Tidak Sesuai";
	$hslcustbustelp="Tidak Sesuai";
	$hslcustbusfax="Tidak Sesuai";
	$hslcustbushp="Tidak Sesuai";
	$hslcustbusaktano="Tidak Sesuai";
	$hslcustbusaktadate="Tidak Sesuai";
	$hslcustbusnpwp="Tidak Sesuai";
	$hslcustbussiup="Tidak Sesuai";
	$hslcustbustdp="Tidak Sesuai";
	$hslcustbusyearlong="Tidak Sesuai";
	$hslcustbusmonthlong="Tidak Sesuai";
	$hslcustcreditstatus="Tidak Sesuai";
	$hslcustcreditneed1="Tidak Sesuai";
	$hslcustcredittype1="Tidak Sesuai";
	$hslcustcreditplafond1="Tidak Sesuai";
	$hslcustcreditlong1="Tidak Sesuai";
	$hslcustcreditneed2="Tidak Sesuai";
	$hslcustcredittype2="Tidak Sesuai";
	$hslcustcreditplafond2="Tidak Sesuai";
	$hslcustcreditlong2="Tidak Sesuai";
	$hslcustprointerviewnotes="Tidak Sesuai";
	$hslcustowner1jabatancode="Tidak Sesuai";
	$hslcustowner1name="Tidak Sesuai";
	$hslcustowner1saham="Tidak Sesuai";
	$hslcustowner1lamagabung="Tidak Sesuai";
	$hslcustowner1ktp="Tidak Sesuai";
	$hslcustowner1npwp="Tidak Sesuai";
	$hslcustowner2jabatancode="Tidak Sesuai";
	$hslcustowner2name="Tidak Sesuai";
	$hslcustowner2saham="Tidak Sesuai";
	$hslcustowner2lamagabung="Tidak Sesuai";
	$hslcustowner2ktp="Tidak Sesuai";
	$hslcustowner2npwp="Tidak Sesuai";
	$hslcustowner3jabatancode="Tidak Sesuai";
	$hslcustowner3name="Tidak Sesuai";
	$hslcustowner3saham="Tidak Sesuai";
	$hslcustowner3lamagabung="Tidak Sesuai";
	$hslcustowner3ktp="Tidak Sesuai";
	$hslcustowner3npwp="Tidak Sesuai";
	$hslcustnomobfacility="Tidak Sesuai";
	$hslcustnomomsetcode="Tidak Sesuai";
	$hslcustnomlamausahacode="Tidak Sesuai";
	$hslcustnomplafondcode="Tidak Sesuai";
	$hslcustnomcreditfu="Tidak Sesuai";
	$hslcustnomnotes="Tidak Sesuai";
	$hslcustnomfrom="Tidak Sesuai";
	$hslcustnomperkenalan="Tidak Sesuai";
	$hslcustprostsusahacode="Tidak Sesuai";
	$hslcustprostskredit="Tidak Sesuai";
	$hslcustprointerview="Tidak Sesuai";
	$hslcustpropendapatan="Tidak Sesuai";
	$hslcustprodate="Tidak Sesuai";
	$hslflag="Tidak Sesuai";
	
	
	
	if($newcustproccode==$custproccode){ $hslcustproccode="Sesuai";}
	if($newcustjabatancode==$custjabatancode){ $hslcustjabatancode="Sesuai";}
	if($newcustfullname==$custfullname){ $hslcustfullname="Sesuai";}
	if($newcustnpwpno==$custnpwpno){ $hslcustnpwpno="Sesuai";}
	if($newcustkel==$custkel){ $hslcustkel="Sesuai";}
	if($newcustkec==$custkec){ $hslcustkec="Sesuai";}
	if($newcustcity==$custcity){ $hslcustcity="Sesuai";}
	if($newcustprop==$custprop){ $hslcustprop="Sesuai";}
	if($newcustzipcode==$custzipcode){ $hslcustzipcode="Sesuai";}
	if($newcustfax==$custfax){ $hslcustfax="Sesuai";}
	if($newcusthomestatus==$custhomestatus){ $hslcusthomestatus="Sesuai";}
	if($newcusthomeyearlong==$custhomeyearlong){ $hslcusthomeyearlong="Sesuai";}
	if($newcusthomemonthlong==$custhomemonthlong){ $hslcusthomemonthlong="Sesuai";}
	if($newcustbustype==$custbustype){ $hslcustbustype="Sesuai";}
	if($newcustbusname==$custbusname){ $hslcustbusname="Sesuai";}
	if($newcustbusaddr==$custbusaddr){ $hslcustbusaddr="Sesuai";}
	if($newcustbustelp==$custbustelp){ $hslcustbustelp="Sesuai";}
	if($newcustbusfax==$custbusfax){ $hslcustbusfax="Sesuai";}
	if($newcustbushp==$custbushp){ $hslcustbushp="Sesuai";}
	if($newcustbusaktano==$custbusaktano){ $hslcustbusaktano="Sesuai";}
	if($newcustbusaktadate==$custbusaktadate){ $hslcustbusaktadate="Sesuai";}
	if($newcustbusnpwp==$custbusnpwp){ $hslcustbusnpwp="Sesuai";}
	if($newcustbussiup==$custbussiup){ $hslcustbussiup="Sesuai";}
	if($newcustbustdp==$custbustdp){ $hslcustbustdp="Sesuai";}
	if($newcustbusyearlong==$custbusyearlong){ $hslcustbusyearlong="Sesuai";}
	if($newcustbusmonthlong==$custbusmonthlong){ $hslcustbusmonthlong="Sesuai";}
	if($newcustcreditstatus==$custcreditstatus){ $hslcustcreditstatus="Sesuai";}
	if($newcustnomfrom==$custnomfrom){ $hslcustnomfrom="Sesuai";}
	if($newcustnomperkenalan==$custnomperkenalan){ $hslcustnomperkenalan="Sesuai";}
	if($newcustprostsusahacode==$custprostsusahacode){ $hslcustprostsusahacode="Sesuai";}
	if($newcustprostskredit==$custprostskredit){ $hslcustprostskredit="Sesuai";}
	if($newcustprointerview==$custprointerview){ $hslcustprointerview="Sesuai";}
	if($newcustpropendapatan==$custpropendapatan){ $hslcustpropendapatan="Sesuai";}
	if($newcustprodate==$custprodate){ $hslcustprodate="Sesuai";}
	if($newflag==$flag){ $hslflag="Sesuai";}
	
	
	$tdcustnomid="";
	$tdcusttarid="";
	$tdcustaplno="";
	$tdcustapldate="";
	$tdcustaocode="";
	$tdcustbranchcode="";
	$tdcustlkcdno="";
	$tdcustproccode="";
	$tdcustcurcode="";
	$tdcustplafond="";
	$tdcustfullname="";
	$tdcustshortname="";
	$tdemail="";
	$tdcustsex="";
	$tdcustjabatancode="";
	$tdcustktpno="";
	$tdcustktpexp="";
	$tdcustboddate="";
	$tdcustbodplace="";
	$tdcustnpwpno="";
	$tdcustmothername="";
	$tdcusteducode="";
	$tdcustmarcode="";
	$tdcustmarname="";
	$tdcustjmltanggungan="";
	$tdcustaddr="";
	$tdcustrt="";
	$tdcustrw="";
	$tdcustkel="";
	$tdcustkec="";
	$tdcustcity="";
	$tdcustprop="";
	$tdcustzipcode="";
	$tdcusttelp="";
	$tdcusthp="";
	$tdcustfax="";
	$tdcusthomestatus="";
	$tdcusthomeyearlong="";
	$tdcusthomemonthlong="";
	$tdcustaddrktp="";
	$tdcustrtktp="";
	$tdcustrwktp="";
	$tdcustkelktp="";
	$tdcustkecktp="";
	$tdcustcityktp="";
	$tdcustpropktp="";
	$tdcustzipcodektp="";
	$tdcustbustype="";
	$tdcustbusname="";
	$tdcustbusaddr="";
	$tdcustbustelp="";
	$tdcustbusfax="";
	$tdcustbushp="";
	$tdcustbusaktano="";
	$tdcustbusaktadate="";
	$tdcustbusnpwp="";
	$tdcustbussiup="";
	$tdcustbustdp="";
	$tdcustbusyearlong="";
	$tdcustbusmonthlong="";
	$tdcustcreditstatus="";
	$tdcustcreditneed1="";
	$tdcustcredittype1="";
	$tdcustcreditplafond1="";
	$tdcustcreditlong1="";
	$tdcustcreditneed2="";
	$tdcustcredittype2="";
	$tdcustcreditplafond2="";
	$tdcustcreditlong2="";
	$tdcustprointerviewnotes="";
	$tdcustowner1jabatancode="";
	$tdcustowner1name="";
	$tdcustowner1saham="";
	$tdcustowner1lamagabung="";
	$tdcustowner1ktp="";
	$tdcustowner1npwp="";
	$tdcustowner2jabatancode="";
	$tdcustowner2name="";
	$tdcustowner2saham="";
	$tdcustowner2lamagabung="";
	$tdcustowner2ktp="";
	$tdcustowner2npwp="";
	$tdcustowner3jabatancode="";
	$tdcustowner3name="";
	$tdcustowner3saham="";
	$tdcustowner3lamagabung="";
	$tdcustowner3ktp="";
	$tdcustowner3npwp="";
	$tdcustnomobfacility="";
	$tdcustnomomsetcode="";
	$tdcustnomlamausahacode="";
	$tdcustnomplafondcode="";
	$tdcustnomcreditfu="";
	$tdcustnomnotes="";
	$tdcustnomfrom="";
	$tdcustnomperkenalan="";
	$tdcustprostsusahacode="";
	$tdcustprostskredit="";
	$tdcustprointerview="";
	$tdcustpropendapatan="";
	$tdcustprodate="";
	$tdflag="";
	
	
	if(isset($_REQUEST['view']))
	{
		if($_REQUEST['view'] == "preview")
		{
		$tdcustnomid='<td style="width:100px;">'.$hslcustnomid.'</td>';
		$tdcusttarid='<td style="width:100px;">'.$hslcusttarid.'</td>';
		$tdcustaplno='<td style="width:100px;">'.$hslcustaplno.'</td>';
		$tdcustapldate='<td style="width:100px;">'.$hslcustapldate.'</td>';
		$tdcustaocode='<td style="width:100px;">'.$hslcustaocode.'</td>';
		$tdcustbranchcode='<td style="width:100px;">'.$hslcustbranchcode.'</td>';
		$tdcustlkcdno='<td style="width:100px;">'.$hslcustlkcdno.'</td>';
		$tdcustproccode='<td style="width:100px;">'.$hslcustproccode.'</td>';
		$tdcustcurcode='<td style="width:100px;">'.$hslcustcurcode.'</td>';
		$tdcustplafond='<td style="width:100px;">'.$hslcustplafond.'</td>';
		$tdcustfullname='<td style="width:100px;">'.$hslcustfullname.'</td>';
		$tdcustshortname='<td style="width:100px;">'.$hslcustshortname.'</td>';
		$tdemail='<td style="width:100px;">'.$hslemail.'</td>';
		$tdcustsex='<td style="width:100px;">'.$hslcustsex.'</td>';
		$tdcustjabatancode='<td style="width:100px;">'.$hslcustjabatancode.'</td>';
		$tdcustktpno='<td style="width:100px;">'.$hslcustktpno.'</td>';
		$tdcustktpexp='<td style="width:100px;">'.$hslcustktpexp.'</td>';
		$tdcustboddate='<td style="width:100px;">'.$hslcustboddate.'</td>';
		$tdcustbodplace='<td style="width:100px;">'.$hslcustbodplace.'</td>';
		$tdcustnpwpno='<td style="width:100px;">'.$hslcustnpwpno.'</td>';
		$tdcustmothername='<td style="width:100px;">'.$hslcustmothername.'</td>';
		$tdcusteducode='<td style="width:100px;">'.$hslcusteducode.'</td>';
		$tdcustmarcode='<td style="width:100px;">'.$hslcustmarcode.'</td>';
		$tdcustmarname='<td style="width:100px;">'.$hslcustmarname.'</td>';
		$tdcustjmltanggungan='<td style="width:100px;">'.$hslcustjmltanggungan.'</td>';
		$tdcustaddr='<td style="width:100px;">'.$hslcustaddr.'</td>';
		$tdcustrt='<td style="width:100px;">'.$hslcustrt.'</td>';
		$tdcustrw='<td style="width:100px;">'.$hslcustrw.'</td>';
		$tdcustkel='<td style="width:100px;">'.$hslcustkel.'</td>';
		$tdcustkec='<td style="width:100px;">'.$hslcustkec.'</td>';
		$tdcustcity='<td style="width:100px;">'.$hslcustcity.'</td>';
		$tdcustprop='<td style="width:100px;">'.$hslcustprop.'</td>';
		$tdcustzipcode='<td style="width:100px;">'.$hslcustzipcode.'</td>';
		$tdcusttelp='<td style="width:100px;">'.$hslcusttelp.'</td>';
		$tdcusthp='<td style="width:100px;">'.$hslcusthp.'</td>';
		$tdcustfax='<td style="width:100px;">'.$hslcustfax.'</td>';
		$tdcusthomestatus='<td style="width:100px;">'.$hslcusthomestatus.'</td>';
		$tdcusthomeyearlong='<td style="width:100px;">'.$hslcusthomeyearlong.'</td>';
		$tdcusthomemonthlong='<td style="width:100px;">'.$hslcusthomemonthlong.'</td>';
		$tdcustaddrktp='<td style="width:100px;">'.$hslcustaddrktp.'</td>';
		$tdcustrtktp='<td style="width:100px;">'.$hslcustrtktp.'</td>';
		$tdcustrwktp='<td style="width:100px;">'.$hslcustrwktp.'</td>';
		$tdcustkelktp='<td style="width:100px;">'.$hslcustkelktp.'</td>';
		$tdcustkecktp='<td style="width:100px;">'.$hslcustkecktp.'</td>';
		$tdcustcityktp='<td style="width:100px;">'.$hslcustcityktp.'</td>';
		$tdcustpropktp='<td style="width:100px;">'.$hslcustpropktp.'</td>';
		$tdcustzipcodektp='<td style="width:100px;">'.$hslcustzipcodektp.'</td>';
		$tdcustbustype='<td style="width:100px;">'.$hslcustbustype.'</td>';
		$tdcustbusname='<td style="width:100px;">'.$hslcustbusname.'</td>';
		$tdcustbusaddr='<td style="width:100px;">'.$hslcustbusaddr.'</td>';
		$tdcustbustelp='<td style="width:100px;">'.$hslcustbustelp.'</td>';
		$tdcustbusfax='<td style="width:100px;">'.$hslcustbusfax.'</td>';
		$tdcustbushp='<td style="width:100px;">'.$hslcustbushp.'</td>';
		$tdcustbusaktano='<td style="width:100px;">'.$hslcustbusaktano.'</td>';
		$tdcustbusaktadate='<td style="width:100px;">'.$hslcustbusaktadate.'</td>';
		$tdcustbusnpwp='<td style="width:100px;">'.$hslcustbusnpwp.'</td>';
		$tdcustbussiup='<td style="width:100px;">'.$hslcustbussiup.'</td>';
		$tdcustbustdp='<td style="width:100px;">'.$hslcustbustdp.'</td>';
		$tdcustbusyearlong='<td style="width:100px;">'.$hslcustbusyearlong.'</td>';
		$tdcustbusmonthlong='<td style="width:100px;">'.$hslcustbusmonthlong.'</td>';
		$tdcustcreditstatus='<td style="width:100px;">'.$hslcustcreditstatus.'</td>';
		$tdcustcreditneed1='<td style="width:100px;">'.$hslcustcreditneed1.'</td>';
		$tdcustcredittype1='<td style="width:100px;">'.$hslcustcredittype1.'</td>';
		$tdcustcreditplafond1='<td style="width:100px;">'.$hslcustcreditplafond1.'</td>';
		$tdcustcreditlong1='<td style="width:100px;">'.$hslcustcreditlong1.'</td>';
		$tdcustcreditneed2='<td style="width:100px;">'.$hslcustcreditneed2.'</td>';
		$tdcustcredittype2='<td style="width:100px;">'.$hslcustcredittype2.'</td>';
		$tdcustcreditplafond2='<td style="width:100px;">'.$hslcustcreditplafond2.'</td>';
		$tdcustcreditlong2='<td style="width:100px;">'.$hslcustcreditlong2.'</td>';
		$tdcustprointerviewnotes='<td style="width:100px;">'.$hslcustprointerviewnotes.'</td>';
		$tdcustowner1jabatancode='<td style="width:100px;">'.$hslcustowner1jabatancode.'</td>';
		$tdcustowner1name='<td style="width:100px;">'.$hslcustowner1name.'</td>';
		$tdcustowner1saham='<td style="width:100px;">'.$hslcustowner1saham.'</td>';
		$tdcustowner1lamagabung='<td style="width:100px;">'.$hslcustowner1lamagabung.'</td>';
		$tdcustowner1ktp='<td style="width:100px;">'.$hslcustowner1ktp.'</td>';
		$tdcustowner1npwp='<td style="width:100px;">'.$hslcustowner1npwp.'</td>';
		$tdcustowner2jabatancode='<td style="width:100px;">'.$hslcustowner2jabatancode.'</td>';
		$tdcustowner2name='<td style="width:100px;">'.$hslcustowner2name.'</td>';
		$tdcustowner2saham='<td style="width:100px;">'.$hslcustowner2saham.'</td>';
		$tdcustowner2lamagabung='<td style="width:100px;">'.$hslcustowner2lamagabung.'</td>';
		$tdcustowner2ktp='<td style="width:100px;">'.$hslcustowner2ktp.'</td>';
		$tdcustowner2npwp='<td style="width:100px;">'.$hslcustowner2npwp.'</td>';
		$tdcustowner3jabatancode='<td style="width:100px;">'.$hslcustowner3jabatancode.'</td>';
		$tdcustowner3name='<td style="width:100px;">'.$hslcustowner3name.'</td>';
		$tdcustowner3saham='<td style="width:100px;">'.$hslcustowner3saham.'</td>';
		$tdcustowner3lamagabung='<td style="width:100px;">'.$hslcustowner3lamagabung.'</td>';
		$tdcustowner3ktp='<td style="width:100px;">'.$hslcustowner3ktp.'</td>';
		$tdcustowner3npwp='<td style="width:100px;">'.$hslcustowner3npwp.'</td>';
		$tdcustnomobfacility='<td style="width:100px;">'.$hslcustnomobfacility.'</td>';
		$tdcustnomomsetcode='<td style="width:100px;">'.$hslcustnomomsetcode.'</td>';
		$tdcustnomlamausahacode='<td style="width:100px;">'.$hslcustnomlamausahacode.'</td>';
		$tdcustnomplafondcode='<td style="width:100px;">'.$hslcustnomplafondcode.'</td>';
		$tdcustnomcreditfu='<td style="width:100px;">'.$hslcustnomcreditfu.'</td>';
		$tdcustnomnotes='<td style="width:100px;">'.$hslcustnomnotes.'</td>';
		$tdcustnomfrom='<td style="width:100px;">'.$hslcustnomfrom.'</td>';
		$tdcustnomperkenalan='<td style="width:100px;">'.$hslcustnomperkenalan.'</td>';
		$tdcustprostsusahacode='<td style="width:100px;">'.$hslcustprostsusahacode.'</td>';
		$tdcustprostskredit='<td style="width:100px;">'.$hslcustprostskredit.'</td>';
		$tdcustprointerview='<td style="width:100px;">'.$hslcustprointerview.'</td>';
		$tdcustpropendapatan='<td style="width:100px;">'.$hslcustpropendapatan.'</td>';
		$tdcustprodate='<td style="width:100px;">'.$hslcustprodate.'</td>';
		$tdflag='<td style="width:100px;">'.$hslflag.'</td>';
		}
	}
	
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			var min_plafond = <?echo $proc_min_plafond?>;
			var money_min_plafond = "<?echo numberFormat($proc_min_plafond)?>";
			var max_plafond = <?echo $proc_max_plafond?>;
			var money_max_plafond = "<?echo numberFormat($proc_max_plafond)?>";
			var min_facility = <?echo $proc_min_facility?>;
			var money_min_facility = "<?echo numberFormat($proc_min_facility)?>";
			<?
				$strsql="select custfacseq from tbl_CustomerFacility2 where custnomid='$custnomid'";
				$sqlcon = sqlsrv_query($conn, $strsql);
				if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($sqlcon))
				{
					while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
					{
						$seq=$rows['custfacseq'];
						?>
						function checkplafond<?echo $seq?>()
						{	
							var plafond=$("#plafond<?echo $seq?>").val();
							plafond=plafond.replace(/,/g,"");
							if (plafond=="")
							{
								alert("Silahkan pilih isi plafond.");
								$("#plafond<?echo $seq?>").focus();
							}
							else if (plafond==0)
							{
								alert("Plafond tidak boleh 0.");
								$("#plafond<?echo $seq?>").focus();
							}
							else if (plafond<min_facility)
							{
								alert("Plafond minimal "+money_min_facility+".");
								$("#plafond<?echo $seq?>").focus();
								document.getElementById("plafond<?echo $seq?>").value=money_min_facility;
							}
						}
						
						function checkperiod<?echo $seq?>()
						{
							var period=$("#period<?echo $seq?>").val();
							if(period=="")
							{
								alert("Silahkan isi jangka waktu.");
								$("#period<?echo $seq?>").focus();
							}
							else if(period==0)
							{
								alert("Jangka waktu tidak boleh 0.");
								$("#period<?echo $seq?>").focus();
							}
							else if(period<12)
							{
								alert("Jangka waktu minimal 12 bulan.");
								$("#period<?echo $seq?>").focus();
								document.getElementById("period<?echo $seq?>").value="12";
							}
						}
						<?
					}
				}
			?>
			function insurance(getid) 
			{
				var getvalinsurance = $("#"+getid).val();
				
				if (getvalinsurance=="JK99")
				{
					document.getElementById("other_"+getid).style.display="block";
					document.getElementById("other__"+getid).style.display="block";
					document.getElementById("other___"+getid).style.display="block";
					document.getElementById("other"+getid).value="";
				}
				else
				{
					document.getElementById("other_"+getid).style.display="none";
					document.getElementById("other__"+getid).style.display="none";
					document.getElementById("other___"+getid).style.display="none";
					document.getElementById("other"+getid).value="";
				}
			}
			
			
			var rand_no1 = Math.random();
			var rand_no2 = Math.random();
			var rand_no = rand_no1 * rand_no2;
			function btnonclick(thisid)
			{
				
				<?
				$totalplafond="";
				$strsql="select custfacseq from tbl_CustomerFacility2 where custnomid='$custnomid'";
				$sqlcon = sqlsrv_query($conn, $strsql);
				if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
				if(sqlsrv_has_rows($sqlcon))
				{
					while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
					{
						$seq=$rows['custfacseq'];
						
					?>
						var plafond<?echo $seq?> = $("#plafond<?echo $seq?>").val();
						var tmpplafond<?echo $seq?>=plafond<?echo $seq?>.replace(/,/g,"");
						var intplafond<?echo $seq?>=parseFloat(tmpplafond<?echo $seq?>);
					<?
						$totalplafond.=" intplafond".$seq."+";
						
					}
					$totalplafond=substr($totalplafond,0,-1);
				}
				?>
				var totalplafond=<?echo $totalplafond;?>;
				
				
				//data pemohon
				var custbusname=$("#custbusname").val();
				var custbusnpwp=$("#custbusnpwp").val();
				var custfullname=$("#custfullname").val();
				var custnpwpno=$("#custnpwpno").val();
				var custbusaktano=$("#custbusaktano").val();
				var custbusaktadate=$("#custbusaktadate").val();
				var custbussiup=$("#custbussiup").val();
				var custjabatancode=$("#custjabatancode").val();
				var custbustdp=$("#custbustdp").val();
				var custbustelp=$("#custbustelp").val();
				var custbushp=$("#custbushp").val();
				
				
				
				//alamat
				var custbusaddr=$("#custbusaddr").val();
				var custcity=$("#custcity").val();
				var custprop=$("#custprop").val();
				var custkec=$("#custkec").val();
				var custkel=$("#custkel").val();
				var custzipcode=$("#custzipcode").val();
				var custfax=$("#custfax").val();
				var custhomestatus=$("#custhomestatus").val();
				var custhomemonthlong =$("#custhomemonthlong").val();
				var custcreditstatus=$("#custcreditstatus").val();
				var homemonthlong=$("#homemonthlong").val();
				
				
				//insurance
				var lostinsurance=$("#lostinsurance").val();
				var otherlostinsurance=$("#otherlostinsurance").val();
				
				//totalplafond
				var totalplafond=parseFloat($("#totalplafond").val());
				
				
				//data pemohon
				var lkcddate=$("#lkcddate").val();
				var introduce=$("#introduce").val();
				var recomen=$("#recomen").val();
				
				if (custbusname=="")
				{
					alert("Silahkan isi Nama perusahaan.");
					$("#custbusname").focus();
				}
				else if(custbusnpwp=="")
				{
					alert("Silahkan isi NPWP Perusahaan.");
					$("#custbusnpwp").focus();
				}
				else if(custfullname=="")
				{
					alert("Silahkan isi Contact person.");
					$("#custfullname").focus();
				}
				else if(custjabatancode=="")
				{
					alert("Silahkan Pilih Jabatan.");
					$("#custjabatancode").focus();
				}
				else if(custnpwpno=="")
				{
					alert("Silahkan isi NPWP.");
					$("#custnpwpno").focus();
				}
				else if(custbusaktano=="")
				{
					alert("Silahkan isi no akte.");
					$("#custbusaktano").focus();
				}
				else if(custbusaktadate=="")
				{
					alert("Silahkan pilih Tanggal akte.");
					$("#custbusaktadate").focus();
				}
				else if(custbussiup=="")
				{
					alert("Silahkan isi SIUP.");
					$("#custbussiup").focus();
				}
				else if(custbustdp=="")
				{
					alert("Silahkan isi TDP.");
					$("#custbustdp").focus();
				}
				else if(custbustelp=="")
				{
					alert("Silahkan isi no telepon.");
					$("#custbustelp").focus();
				}
				else if(custbushp=="")
				{
					alert("Silahkan isi no hp.");
					$("#custbushp").focus();
				}
				else if (custbusaddr=="")
				{
					alert("Silahkan isi alamat perusahaan.");
					$("#custbusaddr").focus();
				}
				else if	(custcity=="")
				{
					alert("Silahkan isi kota.");
					$("#custcity").focus();
				}
				else if	(custprop=="")
				{
					alert("Silahkan pilih propinsi.");
					$("#custprop").focus();
				}
				else if	(custkec=="")
				{
					alert("Silahkan isi Kecamatan.");
					$("#custkec").focus();
				}
				else if	(custkel=="")
				{
					alert("Silahkan isi Kelurahan.");
					$("#custkel").focus();
				}
				else if	(custzipcode=="")
				{
					alert("Silahkan isi kode pos.");
					$("#custzipcode").focus();
				}
				else if	(custfax=="")
				{
					alert("Silahkan isi  no fax.");
					$("#custfax").focus();
				}
				else if	(custhomestatus=="")
				{
					alert("Silahkan pilih Status kantor.");
					$("#custzipcode").focus();
				}
				else if	(custhomemonthlong=="")
				{
					alert("Silahkan isi lama tahun menempati.");
					$("#custhomemonthlong").focus();
				}
				else if	(homemonthlong=="")
				{
					alert("Silahkan isi lama bulan menempati.");
					$("#homemonthlong").focus();
				}
				else if (totalplafond<min_plafond)
				{
					alert("Plafond yang anda masukkan baru "+totalplafond+ ".")
				}
				else if (totalplafond>max_plafond)
				{
					alert("Plafond yang anda masukkan ( "+totalplafond+ " ) sudah melewati batas maksimal.")
				}
				else if(lostinsurance=="JK99")
				{
					if(otherlostinsurance=="")
					{			
						alert("Silahkan isi asuransi kerugian ke lainnya.");
						$("#otherlostinsurance").focus();
					}
					else 
					{
						submitform = window.confirm("<?=$confmsg;?>");
						if (submitform == true)
						{
							document.getElementById("frmsaveflow").action = "do_vid_fak_b.php";
							document.getElementById("frmsaveflow").submit();
							return true;
						}
						else
						{
							return false;
						}
					}
				}
				else 
				{
					submitform = window.confirm("<?=$confmsg;?>");
					if (submitform == true)
					{
						document.getElementById("frmsaveflow").action = "do_vid_fak_b.php";
						document.getElementById("frmsaveflow").submit();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			
			
			function statusperkawinan()
			{
				var status=$("#custmarcode").val();
				if (status=="2")
				{
					document.getElementById("td_relation").style.display="block";
					document.getElementById("td__relation").style.display="block";
					document.getElementById("td___relation").style.display="block";
					document.getElementById("relation").value="";
				}
				else
				{
					document.getElementById("td_relation").style.display="none";
					document.getElementById("td__relation").style.display="none";
					document.getElementById("td___relation").style.display="none";
					document.getElementById("relation").value="";
				}
			}
			
			function getkodeproduct(thisid)
			{
				var creditneed=$("#"+thisid).val();
				//alert(creditneed);
				var custnomid=$("#custnomid").val();
				var seq=thisid.substring(10,thisid.length);
				//alert(seq);
				//alert("dtl_"+thisid+" creditneed ::::"+creditneed);
				
				$.ajax	
				({
					type: "GET",
					url: "getcode.php",
					data: "btn=changekp&creditneed="+creditneed+"&seq="+seq+"&custnomid="+custnomid+"&random="+ rand_no +"",
					success: function(response)
					{
						//alert(response);
						$("#dtl_"+thisid).html(response);
					}
				});
			}
		</script>
	</head>
	<body>
		<form id="frmsaveflow" name="frmsaveflow" action="do_vid_fak_b.php" method="post">
			<div class="divcenter">
				<table <?echo $warna?> border="0" style ="width:900px; border:1px solid black;">
					<tr>
						<td colspan="3" style="text-align:center;"><h4>VERIFIKASI IDENTITAS DEBITUR</h4></td>
					</tr>
					<tr>
						<td colspan="3"><h4>DATA PEMOHON</h4></td>
					</tr>
					<tr>
						<td style="width:20%">Nama Perusahaaan</td>
						<td style="width:35%"><?echo $custbusname?></td>
						<td style="width:45%">
							<input type="text" <?echo $tagselect;?> id="custbusname" name="custbusname" maxlength="50" value="<?echo $newcustbusname;?>" style="width:100%;"/>
						</td>
						<?=$tdcustbusname?>
					</tr>
					<tr>
						<td>NPWP Perusahaan</td>
						<td><?echo $custbusnpwp;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbusnpwp" name="custbusnpwp" maxlength="20"  value="<?echo $newcustbusnpwp;?>"/>
						</td>
						<?=$tdcustbusnpwp?>
					</tr>
					<tr>
						<td>Contact Person</td>
						<td><?echo $custfullname;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custfullname" name="custfullname" maxlength="50" value="<?echo $newcustfullname;?>" style="width:100%;"/>
						</td>
						<?=$tdcustfullname?>
					</tr>
					<tr>
						<td>Jabatan</td>
						<td>
						<?
							$strsql = "select * from TblJabatan where jabatan_code='$custjabatancode'";
							$sqlcon = sqlsrv_query($conn, $strsql);
							if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
							if(sqlsrv_has_rows($sqlcon))
							{
								while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
								{
									echo $rows['jabatan_name'];
								}
							}
						?>
						</td>
						<td>
							<select <?echo $tagselect;?>id="custjabatancode"  name="custjabatancode" >
									<?
									$strsql = "select * from TblJabatan";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											if ($newcustjabatancode==$rows['jabatan_code'])
											{
												echo '<option value="'.$rows['jabatan_code'].'" selected="selected">'.$rows['jabatan_name'].'</option>';
											}
											else
											{
												echo '<option value="'.$rows['jabatan_code'].'">'.$rows['jabatan_name'].'</option>';
											}
										}
									}
								?>
							</select>
							<?=$tdcustjabatancode?>
						</td>
					</tr>
					<tr>
						<td>NPWP</td>
						<td><?echo $custnpwpno;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custnpwpno" name="custnpwpno" maxlength="20"  value="<?echo $newcustnpwpno;?>"/>
						</td>
						<?=$tdcustnpwpno?>
					</tr>
					<tr>
						<td>Tanggal berdiri</td>
						<td><?echo $custbusaktadate;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbusaktadate" name="custbusaktadate" readonly="readonly" onfocus="NewCssCal(this.id,'YYYYMMDD');" value="<?echo $newcustbusaktadate;?>" />
						</td>
						<?=$tdcustbusaktadate?>
					</tr>
					<tr>
						<td>NO akte</td>
						<td><?echo $custbusaktano;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbusaktano" name="custbusaktano" maxlength="20" value="<?echo $newcustbusaktano;?>"/>
						</td>
						<?=$tdcustbusaktano?>
					</tr>
					<tr>
						<td>SIUP</td>
						<td><?echo $custbussiup;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbussiup" name="custbussiup" maxlength="20" value="<?echo $newcustbussiup;?>"/>
						</td>
						<?=$tdcustbussiup?>
					</tr>
					<tr>
						<td>TDP</td>
						<td><?echo $custbustdp;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbustdp" name="custbustdp" maxlength="20" value="<?echo $newcustbustdp;?>"/>
						</td>
						<?=$tdcustbustdp?>
					</tr>	
					<tr>
						<td>Telepon Kantor</td>
						<td><?echo $custbustelp;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbustelp" name="custbustelp" onkeypress="return isNumberKey(event);" maxlength="20" value="<?echo $newcustbustelp;?>"/>
						</td>
						<?=$tdcustbustelp?>
					</tr>
					<tr>
						<td>No HP</td>
						<td><?echo $custbushp;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custbushp" name="custbushp" onkeypress="return isNumberKey(event);" maxlength="20" value="<?echo $newcustbushp;?>"/>
						</td>
						<?=$tdcustbushp?>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3"><h3>Keterangan Alamat Perusahaan</h3></td>
					</tr>
					<tr>
						<td>
							Alamat Kantor
							<input type="text" <?echo $tagselect;?> style="text-align:center; width:30px;" class="nonmandatory" readonly="readonly" name="tmpaddress" id="tmpaddress" value="100" />
						</td>
						<td><?echo $custbusaddr;?></td>
						<td>
							<textarea id="custbusaddr" name="custbusaddr" <?echo $tagselect;?> rows="3" cols="74" onkeyup="limitTextArea(this.id,'tmpaddress','100')"><?echo $newcustbusaddr;?></textarea>
						</td>
						<?=$tdcustbusaddr?>
					</tr>
					<tr>
						<td>Kota&#47;Kabupaten</td>
						<td><?echo $custcity;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custcity" name="custcity" maxlength="30" value="<?echo $newcustcity;?>"/>
						</td>
						<?=$tdcustcity?>
					</tr>
					<tr>
						<td>Propinsi</td>
						<td><?echo $custprop;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custprop" name="custprop" maxlength="30" value="<?echo $newcustprop;?>"/>
						</td>
						<?=$tdcustprop?>
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td><?echo $custkec;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custkec" name="custkec" maxlength="30" value="<?echo $newcustkec;?>"/>
						</td>
						<?=$tdcustkec?>
					</tr>
					<tr>
						<td>Kelurahan</td>
						<td><?echo $custkel;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custkel" name="custkel" maxlength="30" value="<?echo $newcustkel;?>"/>
						</td>
						<?=$tdcustkel?>
					</tr>
					<tr>
						<td>Kode Pos</td>
						<td><?echo $custzipcode;?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custzipcode" name="custzipcode" maxlength="5" onkeypress="return isNumberKey(event);" value="<?echo $newcustzipcode;?>"/>
						</td>
						<?=$tdcustzipcode?>
					</tr>
					<tr>
						<td>FAX</td>
						<td><? echo $custfax?></td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custfax" name="custfax" onkeypress="return isNumberKey(event);" maxlength="20" value="<?echo $newcustfax;?>"/>
						</td>
						<?=$tdcustfax?>
					</tr>
					<tr>
						<td>Status Tempat tinggal saat ini</td>
						<td>
								<?
									$strsql = "select * from TblStatusRumah where status_code='$custhomestatus'";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											echo $rows['status_name'];
										}
									}
								?>
						</td>
						<td>
							<select <?echo $tagselect;?>id="custhomestatus" name="custhomestatus" >
								<?
									$strsql = "select * from TblStatusRumah where status_code like '%B%'";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											if($newcusthomestatus==$rows['status_code'])
											{
												echo '<option value="'.$rows['status_code'].'" selected="selected">'.$rows['status_name'].'</option>';
											}
											else
											{
												echo '<option value="'.$rows['status_code'].'">'.$rows['status_name'].'</option>';
											}
										}
									}
								?>
							</select>
							<?=$tdcusthomestatus?>
						</td>
					</tr>
					<tr>
						<td>Lama Menempati Rumah</td>
						<td>
							<?echo $custhomeyearlong?> &nbsp;&nbsp;Tahun
						</td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custhomeyearlong" name="custhomeyearlong" style="width:50px;" maxlength="3" onkeypress="return isNumberKey(event);" value="<?echo $newcusthomeyearlong?>"/> &nbsp;&nbsp;Tahun
						<?=$tdcusthomeyearlong?>
						</td>
					</tr>
					<tr>
						<td>Lama Menempati Rumah</td>
						<td>
							<?echo $custhomemonthlong?> &nbsp;&nbsp;Bulan
						</td>
						<td>
							<input type="text" <?echo $tagselect;?> id="custhomemonthlong" name="custhomemonthlong" style="width:50px;" maxlength="3" onkeypress="return isNumberKey(event);"value="<?echo $newcusthomemonthlong?>"/> &nbsp;&nbsp;Bulan
						</td>
						<?=$tdcusthomemonthlong?>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
						<tr>
						<td colspan="3"><h4>DETAIL FASILITAS</h4></td>
					</tr>
					<tr>
						<td>Jenis Pemohonan Kredit</td>
						<td>
							<?
									$strsql = "select * from TblCreditStatus where status_code='$custcreditstatus'";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											echo $rows['status_name'];
										}
									}
								?>
						</td>
						<td>
							<select <?echo $tagselect;?>id="custcreditstatus" name="custcreditstatus" disabled="disabled">
							<?
									$strsql = "select * from TblCreditStatus";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											if($custcreditstatus==$rows['status_code'])
											{
												echo '<option value="'.$rows['status_code'].'" selected="selected">'.$rows['status_name'].'</option>';
											}
											else
											{
												echo '<option value="'.$rows['status_code'].'">'.$rows['status_name'].'</option>';
											}
											
										}
									}
								?>
							</select>
							<?=$tdcustcreditstatus?>
						</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<?
						$totalpalfond=0;
							$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,a.custfacseq as seq,
									c.produk_type_description,a.custcreditplafond,a.custcreditlong,
									a.custcreditneed,a.custcredittype,
									d.custcreditneed as newcustcreditneed,
									d.custcredittype as newcustcredittype,
									d.custcreditplafond as newcustcreditplafond ,d.custcreditlong as newcustcreditlong
									from tbl_CustomerFacility a 
									left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
									left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
									left join tbl_CustomerFacility2 d on d.custnomid = a.custnomid
									and a.custfacseq = d.custfacseq
									where a.custnomid ='".$custnomid."'";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								$seq=$rows['seq'];
								$credit_need_name=$rows['credit_need_name'];
								$produk_type_description=$rows['produk_type_description'];
								$custcreditplafond=$rows['custcreditplafond'];
								$custcreditneed=$rows['custcreditneed'];
								$custcredittype=$rows['custcredittype'];
								$custcreditlong=$rows['custcreditlong'];
								$newcustcreditneed=$rows['newcustcreditneed'];
								$newcustcredittype=$rows['newcustcredittype'];
								$newcustcreditplafond=$rows['newcustcreditplafond'];
								$newcustcreditlong=$rows['newcustcreditlong'];
								$totalpalfond+=$rows['newcustcreditplafond'];
								?>
									<tr>
										<td>Tujuan Pengajuan Kredit</td>
										<td style="width:200px;"><?echo $credit_need_name?></td>
										<td>
											<select <?echo $tagselect;?>id="custcreditneed<?echo $seq?>" name="custcreditneed<?echo $seq?>" style="width:100%;" onchange="getkodeproduct(this.id);">
												<?
													$strsql1 = "select * from tbl_Creditneed";
													$sqlcon1 = sqlsrv_query($conn, $strsql1);
													if ( $sqlcon1 === false)die( FormatErrors( sqlsrv_errors() ) );
													if(sqlsrv_has_rows($sqlcon1))
													{
														while($rows1 = sqlsrv_fetch_array($sqlcon1, SQLSRV_FETCH_ASSOC))
														{
															if ($newcustcreditneed==$rows1['credit_need_code'])
															{
																echo '<option value="'.$rows1['credit_need_code'].'" selected="selected">'.$rows1['credit_need_name'].'</option>';
															}
															else
															{
																echo '<option value="'.$rows1['credit_need_code'].'">'.$rows1['credit_need_name'].'</option>';
															}
															
														}
													}
													if ($newcustcreditneed=="0")
													{
														$tmp="where (produk_type_description like '%term loan%' or produk_type_description like '%Term%' or produk_type_description like '%TL%')";
													}
													else
													{
														$tmp="where (produk_type_description like 'FL%' or produk_type_description like '%FIXED%' or produk_type_description like '%DL%' or produk_type_description like '%PRK%' or produk_type_description like '%demand%')";
													}
												?>
											</select>
										</td>
										<?
											if(isset($_REQUEST['view']))
											{
												if($_REQUEST['view'] == "preview")
												{
													if($custcreditneed==$newcustcreditneed)
													{
														echo '<td>Sesuai</td>';
													}
													else
													{
														echo '<td>Tidak Sesuai</td>';	
													}
												}
											}
										?>
										
									</tr>
									<tr>
										<td>Jenis Fasilitas</td>
										<td><?echo $produk_type_description?></td>
										<td id="dtl_custcreditneed<?echo $seq?>">
											<select <?echo $tagselect;?>id="custcredittype<?echo $seq?>" name="custcredittype<?echo $seq?>" style="width:100%;">
												<?
													$strsql2 = "select * from tbl_kodeproduk $tmp";
													$sqlcon2 = sqlsrv_query($conn, $strsql2);
													if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
													if(sqlsrv_has_rows($sqlcon2))
													{
														while($rows2 = sqlsrv_fetch_array($sqlcon2, SQLSRV_FETCH_ASSOC))
														{
															if($newcustcredittype==$rows2['produk_loan_type'])
															{
																echo '<option value="'.$rows2['produk_loan_type'].'" selected="selected">'.$rows2['produk_type_description'].'</option>';
															}
															else
															{
																echo '<option value="'.$rows2['produk_loan_type'].'">'.$rows2['produk_type_description'].'</option>';
															}
														}
													}
												?>
											</select>
									</td>
									<?
											if(isset($_REQUEST['view']))
											{
												if($_REQUEST['view'] == "preview")
												{
													if($custcredittype==$newcustcredittype)
													{
														echo '<td>Sesuai</td>';
													}
													else
													{
														echo '<td>Tidak Sesuai</td>';	
													}
												}
											}
										?>
									</tr>
									<tr>
										<td>Plafond (IDR)</td>
										<td><?echo numberFormat($custcreditplafond)?></td>
										<td>
											<input type="text" <?echo $tagselect;?> id="plafond<?echo $seq?>" name="plafond<?echo $seq?>"  onkeyup="currency(this.id); " onblur="checkplafond<?echo $seq?>();" maxlength="18" value="<?echo numberFormat($newcustcreditplafond)?>"/>
										</td>
										<?
											if(isset($_REQUEST['view']))
											{
												if($_REQUEST['view'] == "preview")
												{
													if($custcreditplafond==$newcustcreditplafond)
													{
														echo '<td>Sesuai</td>';
													}
													else
													{
														echo '<td>Tidak Sesuai</td>';	
													}
												}
											}
										?>
									</tr>
									<tr>
										<td>Jangka Waktu</td>
										<td><?echo $custcreditlong?></td>
										<td>
											<input type="text" <?echo $tagselect;?> style="width:50px" id="period<?echo $seq?>" name="period<?echo $seq?>" onblur="checkperiod<?echo $seq?>();"  maxlength="3" onkeypress="return isNumberKey(event);" value="<?echo $newcustcreditlong?>"/> &nbsp;&nbsp; Bulan
										</td>
										<?
											if(isset($_REQUEST['view']))
											{
												if($_REQUEST['view'] == "preview")
												{
													if($custcreditlong==$newcustcreditlong)
													{
														echo '<td>Sesuai</td>';
													}
													else
													{
														echo '<td>Tidak Sesuai</td>';	
													}
												}
											}
										?>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
								<?
							}	
							?>
								<input type="hidden" name="totalplafond" id="totalplafond" value="<?echo $totalpalfond?>" />
							<?
						}
					?>
					<tr>
						<td colspan="3"><h3>Asuransi</h3></td>
					</tr>
					<tr>
						<td style="width:20%">Asuransi Kerugian</td>
						<td style="width:35%">
								<?
									$strsql = "select * from tbl_insurance where code like '%$lostinsurance%'";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											echo $rows['name'];
										}
									}
								?>
						</td>
						<td style="width:45%">
							<select <?echo $tagselect;?>id="lostinsurance" name="lostinsurance" onchange="insurance(this.id);">
								<?
									$strsql = "select * from tbl_insurance where code like '%K%'";
									$sqlcon = sqlsrv_query($conn, $strsql);
									if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlcon))
									{
										while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
										{
											if($newlostinsurance==$rows['code'])
											{
												echo '<option value="'.$rows['code'].'" selected="selected">'.$rows['name'].'</option>';
											}
											else
											{
												echo '<option value="'.$rows['code'].'">'.$rows['name'].'</option>';
											}
										}
									}
								?>
							</select>
							<?=$tdcodeloss?>
						</td>
					</tr>
					<tr>
						<td <?if($newlostinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?> id="other_lostinsurance">Jika Lainnya, Sebutkan :</td>
						<td <?if($newlostinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?> id="other__lostinsurance"><?echo $otherlostinsurance?>&nbsp;</td>
						<td <?if($newlostinsurance!="JK99"){echo 'style="display:none;"';}else{echo 'style="display:block;"';}?> id="other___lostinsurance">
							<input type="text" <?echo $tagselect;?> id="otherlostinsurance" name="otherlostinsurance" maxlength="50" value="<?echo $newotherlostinsurance?>"/>
						</td>
						<?if($newlostinsurance=="JK99"){echo $tdotherloss;}?>
					</tr>
				</table>
					<div>&nbsp;</div>	
					<div class="divcenter" style="width:100%">
						<?
						require ("../../requirepage/hiddenfield.php");
						if(!isset($_REQUEST['view']))
						{
							echo '<input type="button" name="btnsave6" id="btnsave6" class="buttonsaveflow"  value="Submit" onclick="btnonclick();" />';
							
						}
						?>
						<input type="hidden" name="btnid" name="btnid" value="6" />
						
					</div>
			</div>
		</form>
	</body>
</html>