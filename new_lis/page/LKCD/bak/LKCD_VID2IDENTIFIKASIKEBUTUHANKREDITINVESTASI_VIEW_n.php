<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<table width="100%" align="center" style="border:1px solid black;" class="preview2">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">IDENTIFIKASI KEBUTUHAN KREDIT INVESTASI</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Jenis Barang yang dibeli</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry1" name="formentry1" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS BARANG</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>MERK</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TYPE</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KUANTITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>HARGA SATUAN (IDR)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$jenisbarang_seq = "";
			$jenisbarang_nama = "";
			$jenisbarang_merk = "";
			$jenisbarang_type = "";
			$jenisbarang_kuantitas = "";
			$jenisbarang_hargasatuan = "";
			$jenisbarang_total = "";
			
			$AllTotalbiaya = 0;
			$AllTotaltemp = 0;
			
			$tsql = "select * from Tbl_LKCDJenisBarang where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$jenisbarang_seq = $row["jenisbarang_seq"];
					$jenisbarang_nama = $row["jenisbarang_nama"];
					$jenisbarang_merk = $row["jenisbarang_merk"];
					$jenisbarang_type = $row["jenisbarang_type"];
					$jenisbarang_kuantitas = $row["jenisbarang_kuantitas"];
					$jenisbarang_hargasatuan = $row["jenisbarang_hargasatuan"];
					$jenisbarang_total = $row["jenisbarang_total"];
				
					$AllTotaltemp = str_replace(",","", $jenisbarang_total);
					$AllTotalbiaya = $AllTotalbiaya + $AllTotaltemp;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_merk?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_type?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($jenisbarang_hargasatuan)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($jenisbarang_total)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</form>
		<form id="formedit1" name="formedit1" method="post">
		
		<?
		
			//Table 2
			$jenisbarang_seq = "";
			$jenisbarang_nama = "";
			$jenisbarang_merk = "";
			$jenisbarang_type = "";
			$jenisbarang_kuantitas = "";
			$jenisbarang_hargasatuan = "";
			$jenisbarang_total = "";
			
			$AllTotalbiaya = 0;
			$AllTotaltemp = 0;
			
			$tsql = "select * from Tbl_LKCDJenisBarang2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$jenisbarang_seq = $row["jenisbarang_seq"];
					$jenisbarang_nama = $row["jenisbarang_nama"];
					$jenisbarang_merk = $row["jenisbarang_merk"];
					$jenisbarang_type = $row["jenisbarang_type"];
					$jenisbarang_kuantitas = $row["jenisbarang_kuantitas"];
					$jenisbarang_hargasatuan = $row["jenisbarang_hargasatuan"];
					$jenisbarang_total = $row["jenisbarang_total"];
				
					$AllTotaltemp = str_replace(",","", $jenisbarang_total);
					$AllTotalbiaya = $AllTotalbiaya + $AllTotaltemp;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_merk?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_type?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $jenisbarang_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($jenisbarang_hargasatuan)?></td>
			<td width="200px"  style="border:1px solid black;width:100px;"  align="center"><? echo  numberFormat($jenisbarang_total)?></td>
			<td width="25%">&nbsp;</td>
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Tempat Usaha yang dibeli</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry2" name="formentry2" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>JENIS TEMPAT USAHA</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>LOKASI</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>LT, LB, dan Lantai</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>KUANTITAS</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>HARGA Pembelian (IDR)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$tempatusaha_seq = "";
			$tempatusaha_nama = "";
			$tempatusaha_lokasi = "";
			$tempatusaha_lbltlantai = "";
			$tempatusaha_kuantitas = "";
			$tempatusaha_hargasatuan = "";
			$tempatusaha_total = "";
			
			$AllTotalbiaya2 = 0;
			$AllTotaltemp2 = 0;
			
			$tsql = "select * from Tbl_LKCDTempatUsaha where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$tempatusaha_seq = $row["tempatusaha_seq"];
					$tempatusaha_nama = $row["tempatusaha_nama"];
					$tempatusaha_lokasi = $row["tempatusaha_lokasi"];
					$tempatusaha_lbltlantai = $row["tempatusaha_lbltlantai"];
					$tempatusaha_kuantitas = $row["tempatusaha_kuantitas"];
					$tempatusaha_hargasatuan = $row["tempatusaha_hargasatuan"];
					$tempatusaha_total = $row["tempatusaha_total"];
				
					$AllTotaltemp2 = str_replace(",","", $tempatusaha_total);
					$AllTotalbiaya2 = $AllTotalbiaya2 + $AllTotaltemp2;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lokasi?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lbltlantai?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_hargasatuan)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_total)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya2)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</form>
		<form id="formedit2" name="formedit2" method="post">
		
		<?
		
			//Table 2
			
			$tempatusaha_seq = "";
			$tempatusaha_nama = "";
			$tempatusaha_lokasi = "";
			$tempatusaha_lbltlantai = "";
			$tempatusaha_kuantitas = "";
			$tempatusaha_hargasatuan = "";
			$tempatusaha_total = "";
			
			$AllTotalbiaya2 = 0;
			$AllTotaltemp2 = 0;
			
			$tsql = "select * from Tbl_LKCDTempatUsaha2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$tempatusaha_seq = $row["tempatusaha_seq"];
					$tempatusaha_nama = $row["tempatusaha_nama"];
					$tempatusaha_lokasi = $row["tempatusaha_lokasi"];
					$tempatusaha_lbltlantai = $row["tempatusaha_lbltlantai"];
					$tempatusaha_kuantitas = $row["tempatusaha_kuantitas"];
					$tempatusaha_hargasatuan = $row["tempatusaha_hargasatuan"];
					$tempatusaha_total = $row["tempatusaha_total"];
				
					$AllTotaltemp2 = str_replace(",","", $tempatusaha_total);
					$AllTotalbiaya2 = $AllTotalbiaya2 + $AllTotaltemp2;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lokasi?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_lbltlantai?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $tempatusaha_kuantitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_hargasatuan)?></td>
			<td width="200px" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($tempatusaha_total)?></td>
			<td width="30%">&nbsp;</td>
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya2)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Rencana Pembangunan/ Renovasi yang akan dilakukan</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry3" name="formentry3" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Jenis bangunan yang akan dibangun/direnovasi</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Luas bangungan/renovasi (m2)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Estimasi waktu pengerjaan</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>Biaya per-m2 (IDR)</strong></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL BIAYA</strong></td>
			<td width="25%">&nbsp;</td>
		</tr>
		<?
			$rencanapembangunan_seq = "";
			$rencanapembangunan_nama = "";
			$rencanapembangunan_luas = "";
			$rencanapembangunan_estimasiwaktu = "";
			$rencanapembangunan_biayaperm2 = "";
			$rencanapembangunan_total = "";
			
			$AllTotalbiaya3 = 0;
			$AllTotaltemp3 = 0;
			
			$tsql = "select * from Tbl_LKCDRencanaPembangunan where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$rencanapembangunan_seq = $row["rencanapembangunan_seq"];
					$rencanapembangunan_nama = $row["rencanapembangunan_nama"];
					$rencanapembangunan_luas = $row["rencanapembangunan_luas"];
					$rencanapembangunan_estimasiwaktu = $row["rencanapembangunan_estimasiwaktu"];
					$rencanapembangunan_biayaperm2 = $row["rencanapembangunan_biayaperm2"];
					$rencanapembangunan_total = $row["rencanapembangunan_total"];
				
					$AllTotaltemp3 = str_replace(",","", $rencanapembangunan_total);
					$AllTotalbiaya3 = $AllTotalbiaya3 + $AllTotaltemp3;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_luas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_estimasiwaktu?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_biayaperm2)?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_total)?></td>
		
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya3)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</form>
		<form id="formedit3" name="formedit3" method="post">
		
		<?
			
			//Table 2
			$rencanapembangunan_seq = "";
			$rencanapembangunan_nama = "";
			$rencanapembangunan_luas = "";
			$rencanapembangunan_estimasiwaktu = "";
			$rencanapembangunan_biayaperm2 = "";
			$rencanapembangunan_total = "";
			
			$AllTotalbiaya3 = 0;
			$AllTotaltemp3 = 0;
			
			$tsql = "select * from Tbl_LKCDRencanaPembangunan2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$rencanapembangunan_seq = $row["rencanapembangunan_seq"];
					$rencanapembangunan_nama = $row["rencanapembangunan_nama"];
					$rencanapembangunan_luas = $row["rencanapembangunan_luas"];
					$rencanapembangunan_estimasiwaktu = $row["rencanapembangunan_estimasiwaktu"];
					$rencanapembangunan_biayaperm2 = $row["rencanapembangunan_biayaperm2"];
					$rencanapembangunan_total = $row["rencanapembangunan_total"];
				
					$AllTotaltemp3 = str_replace(",","", $rencanapembangunan_total);
					$AllTotalbiaya3 = $AllTotalbiaya3 + $AllTotaltemp3;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_nama?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_luas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $rencanapembangunan_estimasiwaktu?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_biayaperm2)?></td>
			<td width="200px"  style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($rencanapembangunan_total)?></td>
			<td width="25%">&nbsp;</td>
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya3)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2"><strong>Kebutuhan Takeover Kredit Investasi</strong></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		<table width="100%" align="center" style="border:0px solid black;">
		<form id="formentry4" name="formentry4" method="post">
		<tr>
			<td width="15%" style="border:1px solid black;width:200px;" align="center"><strong>Jenis Fasilitas Kredit Investasi yang akan di takeover</strong></td>
			<td width="15%" style="border:1px solid black;width:200px;" align="center"><strong>Nama Bank atau institusi</strong></td>
			<td width="15%" style="border:1px solid black;width:200px;" align="center"><strong>Total Outstanding (IDR)</strong></td>
			<td width="25%" colspan="3">&nbsp;</td>
		</tr>
		<?
			$KebutuhanTakeover_seq = "";
			$KebutuhanTakeover_jenisfasilitas = "";
			$KebutuhanTakeover_namabank = "";
			$KebutuhanTakeover_totaloutstanding = "";
			
			$AllTotalbiaya4 = 0;
			$AllTotaltemp4 = 0;
			
			$tsql = "select * from Tbl_LKCDKebutuhanTakeover where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$KebutuhanTakeover_seq = $row["KebutuhanTakeover_seq"];
					$KebutuhanTakeover_jenisfasilitas = $row["KebutuhanTakeover_jenisfasilitas"];
					$KebutuhanTakeover_namabank = $row["KebutuhanTakeover_namabank"];
					$KebutuhanTakeover_totaloutstanding = $row["KebutuhanTakeover_totaloutstanding"];
				
					$AllTotaltemp4 = str_replace(",","", $KebutuhanTakeover_totaloutstanding);
					$AllTotalbiaya4 = $AllTotalbiaya4 + $AllTotaltemp4;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($KebutuhanTakeover_totaloutstanding)?></td>
			
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya4)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</form>
		<form id="formedit4" name="formedit4" method="post">
		
		<?
			//Table 2
			$KebutuhanTakeover_seq = "";
			$KebutuhanTakeover_jenisfasilitas = "";
			$KebutuhanTakeover_namabank = "";
			$KebutuhanTakeover_totaloutstanding = "";
			
			$AllTotalbiaya4 = 0;
			$AllTotaltemp4 = 0;
			
			$tsql = "select * from Tbl_LKCDKebutuhanTakeover2 where custnomid = '$custnomid'";
			$a = sqlsrv_query($conn, $tsql);

			if ( $a === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($a))
			{  
				while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
				{
					$KebutuhanTakeover_seq = $row["KebutuhanTakeover_seq"];
					$KebutuhanTakeover_jenisfasilitas = $row["KebutuhanTakeover_jenisfasilitas"];
					$KebutuhanTakeover_namabank = $row["KebutuhanTakeover_namabank"];
					$KebutuhanTakeover_totaloutstanding = $row["KebutuhanTakeover_totaloutstanding"];
				
					$AllTotaltemp4 = str_replace(",","", $KebutuhanTakeover_totaloutstanding);
					$AllTotalbiaya4 = $AllTotalbiaya4 + $AllTotaltemp4;

		
		?>
		<tr>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_jenisfasilitas?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo $KebutuhanTakeover_namabank?></td>
			<td width="15%" style="border:1px solid black;width:100px;" align="center"><? echo  numberFormat($KebutuhanTakeover_totaloutstanding)?></td>
			<td width="25%">&nbsp;</td>
		</tr>

		<?
				}
			}
		?>
		
		<tr>
			<td width="25%">&nbsp;</td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><strong>TOTAL</strong></td>
			<td width="25%" style="border:1px solid black;width:100px;" align="center"><? echo numberFormat($AllTotalbiaya4)?></td>
			<td width="25%">&nbsp;</td>
		</tr>
		</table>
		</form>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<!--<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="nanako()" /></td>
</tr>-->
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
<script type="text/javascript">
	function nanako(){
		var nan="SAVE2"
		
		var custnomid=$("#custnomid").val();
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				alert('Data berhasil disimpan');
			}
		});
	
	}
	
	function kumi4() {
		var nan="SAVE2D";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var KebutuhanTakeover_jenisfasilitasAdd = $("#KebutuhanTakeover_jenisfasilitasAdd").val();
		var KebutuhanTakeover_namabankAdd = $("#KebutuhanTakeover_namabankAdd").val();
		var KebutuhanTakeover_totaloutstandingAdd = $("#KebutuhanTakeover_totaloutstandingAdd").val();
			
		var FormName="formentry4";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&KebutuhanTakeover_jenisfasilitasAdd="+KebutuhanTakeover_jenisfasilitasAdd+"&KebutuhanTakeover_namabankAdd="+KebutuhanTakeover_namabankAdd+"&KebutuhanTakeover_totaloutstandingAdd="+KebutuhanTakeover_totaloutstandingAdd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena4(id){
		var nan="DEL2D"
		
		var custnomid=$("#custnomid").val();
		var KebutuhanTakeover_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&KebutuhanTakeover_seq="+KebutuhanTakeover_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria4(id) {
		var nan="EDIT2D";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var KebutuhanTakeover_jenisfasilitasAdd = $("#KebutuhanTakeover_jenisfasilitasAdd"+id).val();
		var KebutuhanTakeover_namabankAdd = $("#KebutuhanTakeover_namabankAdd"+id).val();
		var KebutuhanTakeover_totaloutstandingAdd = $("#KebutuhanTakeover_totaloutstandingAdd"+id).val();
			
		var KebutuhanTakeover_seq = id;
		
		var FormName="formedit4";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&KebutuhanTakeover_seq="+KebutuhanTakeover_seq+"&KebutuhanTakeover_jenisfasilitasAdd="+KebutuhanTakeover_jenisfasilitasAdd+"&KebutuhanTakeover_namabankAdd="+KebutuhanTakeover_namabankAdd+"&KebutuhanTakeover_totaloutstandingAdd="+KebutuhanTakeover_totaloutstandingAdd+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function kumi3() {
		var nan="SAVE2C";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var rencanapembangunan_nama = $("#rencanapembangunan_namaAdd").val();
		var rencanapembangunan_luas = $("#rencanapembangunan_luasAdd").val();
		var rencanapembangunan_estimasiwaktu = $("#rencanapembangunan_estimasiwaktuAdd").val();
		var rencanapembangunan_biayaperm2 = $("#rencanapembangunan_biayaperm2Add").val();
			
		var FormName="formentry3";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&rencanapembangunan_nama="+rencanapembangunan_nama+"&rencanapembangunan_luas="+rencanapembangunan_luas+"&rencanapembangunan_estimasiwaktu="+rencanapembangunan_estimasiwaktu+"&rencanapembangunan_biayaperm2="+rencanapembangunan_biayaperm2+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena3(id){
		var nan="DEL2C"
		
		var custnomid=$("#custnomid").val();
		var rencanapembangunan_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&rencanapembangunan_seq="+rencanapembangunan_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria3(id) {
		var nan="EDIT2C";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var rencanapembangunan_nama = $("#rencanapembangunan_namaAdd"+id).val();
		var rencanapembangunan_luas = $("#rencanapembangunan_luasAdd"+id).val();
		var rencanapembangunan_estimasiwaktu = $("#rencanapembangunan_estimasiwaktuAdd"+id).val();
		var rencanapembangunan_biayaperm2 = $("#rencanapembangunan_biayaperm2Add"+id).val();
		
		var rencanapembangunan_seq = id;
			
		var FormName="formedit3";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&rencanapembangunan_seq="+rencanapembangunan_seq+"&rencanapembangunan_nama="+rencanapembangunan_nama+"&rencanapembangunan_luas="+rencanapembangunan_luas+"&rencanapembangunan_estimasiwaktu="+rencanapembangunan_estimasiwaktu+"&rencanapembangunan_biayaperm2="+rencanapembangunan_biayaperm2+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}

	function kumi2() {
		var nan="SAVE2B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var tempatusaha_nama = $("#tempatusaha_namaAdd").val();
		var tempatusaha_lokasi = $("#tempatusaha_lokasiAdd").val();
		var tempatusaha_lbltlantai = $("#tempatusaha_lbltlantaiAdd").val();
		var tempatusaha_kuantitas = $("#tempatusaha_kuantitasAdd").val();
		var tempatusaha_hargasatuan = $("#tempatusaha_hargasatuanAdd").val();
			
		var FormName="formentry2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&tempatusaha_nama="+tempatusaha_nama+"&tempatusaha_lokasi="+tempatusaha_lokasi+"&tempatusaha_lbltlantai="+tempatusaha_lbltlantai+"&tempatusaha_kuantitas="+tempatusaha_kuantitas+"&tempatusaha_hargasatuan="+tempatusaha_hargasatuan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena2(id){
		var nan="DEL2B"
		
		var custnomid=$("#custnomid").val();
		var tempatusaha_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&tempatusaha_seq="+tempatusaha_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria2(id){
		var nan="EDIT2B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var tempatusaha_seq = id;
		var tempatusaha_nama = $("#tempatusaha_namaEdit"+id).val();
		var tempatusaha_lokasi = $("#tempatusaha_lokasiEdit"+id).val();
		var tempatusaha_lbltlantai = $("#tempatusaha_lbltlantaiEdit"+id).val();
		var tempatusaha_kuantitas = $("#tempatusaha_kuantitasEdit"+id).val();
		var tempatusaha_hargasatuan = $("#tempatusaha_hargasatuanEdit"+id).val();
		
		var FormName="formedit2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&tempatusaha_seq="+tempatusaha_seq+"&tempatusaha_nama="+tempatusaha_nama+"&tempatusaha_lokasi="+tempatusaha_lokasi+"&tempatusaha_lbltlantai="+tempatusaha_lbltlantai+"&tempatusaha_kuantitas="+tempatusaha_kuantitas+"&tempatusaha_hargasatuan="+tempatusaha_hargasatuan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
	}
	
	function kumi1() {
		var nan="SAVE2A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var jenisbarang_nama = $("#jenisbarang_namaAdd").val();
		var jenisbarang_merk = $("#jenisbarang_merkAdd").val();
		var jenisbarang_type = $("#jenisbarang_typeAdd").val();
		var jenisbarang_kuantitas = $("#jenisbarang_kuantitasAdd").val();
		var jenisbarang_hargasatuan = $("#jenisbarang_hargasatuanAdd").val();
			
		var FormName="formentry1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&jenisbarang_nama="+jenisbarang_nama+"&jenisbarang_merk="+jenisbarang_merk+"&jenisbarang_type="+jenisbarang_type+"&jenisbarang_kuantitas="+jenisbarang_kuantitas+"&jenisbarang_hargasatuan="+jenisbarang_hargasatuan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena1(id){
		var nan="DEL2A"
		
		var custnomid=$("#custnomid").val();
		var jenisbarang_seq = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&jenisbarang_seq="+jenisbarang_seq+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria1(id) {
		var nan="EDIT2A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var jenisbarang_seq = id;
		var jenisbarang_nama = $("#jenisbarang_namaAdd"+id).val();
		var jenisbarang_merk = $("#jenisbarang_merkAdd"+id).val();
		var jenisbarang_type = $("#jenisbarang_typeAdd"+id).val();
		var jenisbarang_kuantitas = $("#jenisbarang_kuantitasAdd"+id).val();
		var jenisbarang_hargasatuan = $("#jenisbarang_hargasatuanAdd"+id).val();
			
		var FormName="formedit1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&jenisbarang_seq="+jenisbarang_seq+"&jenisbarang_nama="+jenisbarang_nama+"&jenisbarang_merk="+jenisbarang_merk+"&jenisbarang_type="+jenisbarang_type+"&jenisbarang_kuantitas="+jenisbarang_kuantitas+"&jenisbarang_hargasatuan="+jenisbarang_hargasatuan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}

</script>
</body>
</html> 