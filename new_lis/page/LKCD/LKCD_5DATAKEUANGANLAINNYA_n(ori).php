<?	
	require ("../../lib/formatError.php");
	require ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	
	$KeuanganLain_saldotabungan = "";
	$KeuanganLain_deposito = "";
	$KeuanganLain_kaskecil = "";
	$KeuanganLain_lainnyacash = "";
	$KeuanganLain_tanahkosong = "";
	$KeuanganLain_tanahdanbangunan = "";
	$KeuanganLain_kendaraan = "";
	$KeuanganLain_ruko = "";
	$KeuanganLain_mesin = "";
	$KeuanganLain_lainnyanoncash = "";
	
	$tsql = "select * from Tbl_LKCDKeuanganLain where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{  
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$KeuanganLain_saldotabungan = $row["KeuanganLain_saldotabungan"];
			$KeuanganLain_deposito = $row["KeuanganLain_deposito"];
			$KeuanganLain_kaskecil = $row["KeuanganLain_kaskecil"];
			$KeuanganLain_lainnyacash = $row["KeuanganLain_lainnyacash"];
			$KeuanganLain_tanahkosong = $row["KeuanganLain_tanahkosong"];
			$KeuanganLain_tanahdanbangunan = $row["KeuanganLain_tanahdanbangunan"];
			$KeuanganLain_kendaraan = $row["KeuanganLain_kendaraan"];
			$KeuanganLain_ruko = $row["KeuanganLain_ruko"];
			$KeuanganLain_mesin = $row["KeuanganLain_mesin"];
			$KeuanganLain_lainnyanoncash = $row["KeuanganLain_lainnyanoncash"];
		}
	}

	
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<form id="formentry" name="formentry" method="post">
<table width="100%" align="center" style="border:1px solid black;">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">DATA KEUANGAN LAINNYA</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="">Kas dan bank Debitur <span style="color:red;">(Wajib diisi apabila permohonan debitur adalah kredit investasi)</span></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Saldo Tabungan di Bank</td>
	<td><input type="text" id="KeuanganLain_saldotabungan" style="width:200px;background:#FF0;" nai="Saldo Tabungan di Bank " value="<? echo $KeuanganLain_saldotabungan;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_saldotabungan')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Deposito</td>
	<td><input type="text" id="KeuanganLain_deposito" style="width:200px;background:#FF0;" nai="Deposito " value="<? echo $KeuanganLain_deposito;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_deposito')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Kas kecil</td>
	<td><input type="text" id="KeuanganLain_kaskecil" style="width:200px;background:#FF0;" nai="Kas kecil " value="<? echo $KeuanganLain_kaskecil;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_kaskecil')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Lainnya</td>
	<td><input type="text" id="KeuanganLain_lainnyacash" style="width:200px;background:#FF0;" nai="Lainnya " value="<? echo $KeuanganLain_lainnyacash;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_lainnyacash')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" style="">Aktiva Tetap (Fixed Asset)</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Tanah Kosong / Kebun / Sawah</td>
	<td><input type="text" id="KeuanganLain_tanahkosong" style="width:200px;background:#FF0;" nai="Tanah Kosong / Kebun / Sawah " value="<? echo $KeuanganLain_tanahkosong;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_tanahkosong')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Tanah dan Bangunan</td>
	<td><input type="text" id="KeuanganLain_tanahdanbangunan" style="width:200px;background:#FF0;" nai="Tanah dan Bangunan " value="<? echo $KeuanganLain_tanahdanbangunan;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_tanahdanbangunan')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Kendaraan</td>
	<td><input type="text" id="KeuanganLain_kendaraan" style="width:200px;background:#FF0;" nai="Kendaraan " value="<? echo $KeuanganLain_kendaraan;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_kendaraan')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Ruko / Kios / Toko / Los</td>
	<td><input type="text" id="KeuanganLain_ruko" style="width:200px;background:#FF0;" nai="Ruko / Kios / Toko / Los " value="<? echo $KeuanganLain_ruko;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_ruko')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Mesin</td>
	<td><input type="text" id="KeuanganLain_mesin" style="width:200px;background:#FF0;" nai="Mesin " value="<? echo $KeuanganLain_mesin;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_mesin')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>
<tr>
	<td>Lainnya</td>
	<td><input type="text" id="KeuanganLain_lainnyanoncash" style="width:200px;background:#FF0;" nai="Lainnya " value="<? echo $KeuanganLain_lainnyanoncash;?>"  maxlength="20"   onKeyUp="outputMoney('KeuanganLain_lainnyanoncash')" onKeyPress="return isNumberKey(event)"/> RUPIAH</td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input type="button" value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="kumi()" /></td>
</tr>
	<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
	<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
	<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
	<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
	<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
	<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
	<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
	<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>
</form>
<script type="text/javascript">
	function kumi() {
		var nan="SAVE5";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var KeuanganLain_saldotabungan = $("#KeuanganLain_saldotabungan").val();
		var KeuanganLain_deposito = $("#KeuanganLain_deposito").val();
		var KeuanganLain_kaskecil = $("#KeuanganLain_kaskecil").val();
		var KeuanganLain_lainnyacash = $("#KeuanganLain_lainnyacash").val();
		var KeuanganLain_tanahkosong = $("#KeuanganLain_tanahkosong").val();
		var KeuanganLain_tanahdanbangunan = $("#KeuanganLain_tanahdanbangunan").val();
		var KeuanganLain_kendaraan = $("#KeuanganLain_kendaraan").val();
		var KeuanganLain_ruko = $("#KeuanganLain_ruko").val();
		var KeuanganLain_mesin = $("#KeuanganLain_mesin").val();
		var KeuanganLain_lainnyanoncash = $("#KeuanganLain_lainnyanoncash").val();
					
		var FormName="formentry";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_ajax_n.php",
				data: "nan="+nan+"&KeuanganLain_saldotabungan="+KeuanganLain_saldotabungan+"&KeuanganLain_deposito="+KeuanganLain_deposito+"&KeuanganLain_kaskecil="+KeuanganLain_kaskecil+"&KeuanganLain_lainnyacash="+KeuanganLain_lainnyacash+"&KeuanganLain_tanahkosong="+KeuanganLain_tanahkosong+"&KeuanganLain_tanahdanbangunan="+KeuanganLain_tanahdanbangunan+"&KeuanganLain_kendaraan="+KeuanganLain_kendaraan+"&KeuanganLain_ruko="+KeuanganLain_ruko+"&KeuanganLain_mesin="+KeuanganLain_mesin+"&KeuanganLain_lainnyanoncash="+KeuanganLain_lainnyanoncash+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
				}
			});
		}
	
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 