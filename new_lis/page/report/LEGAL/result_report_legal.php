<?php
include("../../../requirepage/session.php");
require ("../../../lib/formatError.php");
require ("../../../lib/open_con.php");

$tgl1=$_REQUEST['tgl1'];
$tgl2=$_REQUEST['tgl2'];
//$region=$_REQUEST['regioncode'];
//$branch=$_REQUEST['branchcode'];
//$status=$_REQUEST['status'];
$admin=$_REQUEST['inadmin'];
//$userid=$_REQUEST['userid'];


if($tgl1=="")
{
	$tglStart = "getdate()";
} 
else 
{
	//$tglStart = "'".date("d/m/Y", strtotime("$tgl1"))."'";
	$tglStart = "'".$tgl1."'";
}

if($tgl1=="")
{
	$tglFinish = "getdate()";
} 
else 
{
	//$tglFinish = "'".date("d/m/Y", strtotime("$tgl2"))."'";
	$tglFinish = "'".$tgl2."'";
}


if($admin == "888")
{
	if($region == "" || $region == "all")
	{
		$KondisiRegion = "";
	} 
	else 
	{
		$KondisiRegion = "and ldoc.txn_region_code='$region'";
	} 
} 
else 
{
	if($region == "" || $region == "all")
	{
		$KondisiRegion = "and ldoc.txn_region_code='$admin'";
	} 
	else 
	{
		$KondisiRegion = "and ldoc.txn_region_code='$region'";
	} 
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="../../../js/jquery-latest.min.js" type="text/javascript"></script>
<script src="../../../js/menu_ce.js" type="text/javascript"></script>
<link href="../../../css/menu_ce.css" type="text/css" rel="stylesheet" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>REPORT LEGAL</title>
</head>

<body>
<div style="position:fixed;">
	<img src="../../../images/header_lis2.jpg" style="width:100%;"></img>
</div>
<div style="border:0px solid black;width:23%;height:545px;margin-top:7%;float:left;position:fixed;">
<!--Open Menu-->
				<div id="cssmenu">
					<ul>
<?
	$userwfid=$_REQUEST['userwfid'];
	
	$tsql2 = "select program_group, program_code, program_desc from Tbl_SE_Program
			where program_code like '%$userwfid%'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$programgcd = $row2[0];
			$programcd = $row2[1];
			$programname = $row2[2];
			//echo $programgcd;
		}
	}
	sqlsrv_free_stmt( $sqlConn2 );
	
	$d = date("d");
	$m = date("m");
	$y = date("y");
	$h = date("h");
	$i = date("i");
	$s = date("s");
	$randomtime = $h.$i.$s;
	
	$tsql2 = "select * from Tbl_SE_User U LEFT JOIN Tbl_Branch B ON U.user_branch_code = B.branch_code where user_id='$userid'";
	$cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params2 = array(&$_POST['query']);

	$sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
	if ( $sqlConn2 === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($sqlConn2))
	{
		$rowCount2 = sqlsrv_num_rows($sqlConn2);
		if( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
		{
			$userbranch = $row2[7];
			$userpwd = $row2[9];
			$userregion = $row2[21];
		}
	}
			
	$tsql = "select Tbl_SE_GrpProgram.grp_code, Tbl_SE_GrpProgram.grp_name from Tbl_SE_GrpProgram, Tbl_SE_Program, Tbl_SE_UserProgram
				where Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
				AND Tbl_SE_Program.program_group=Tbl_SE_GrpProgram.grp_code
				AND Tbl_SE_UserProgram.user_id='$userid'
				GROUP BY Tbl_SE_GrpProgram.grp_code,Tbl_SE_GrpProgram.grp_name, Tbl_SE_GrpProgram.grp_urut
				ORDER BY Tbl_SE_GrpProgram.grp_urut,Tbl_SE_GrpProgram.grp_name";
	  $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	  $params = array(&$_POST['query']);

	  $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	  if ( $sqlConn === false)
	  die( FormatErrors( sqlsrv_errors() ) );
	  if(sqlsrv_has_rows($sqlConn))
	  {
		 $rowCount = sqlsrv_num_rows($sqlConn);
		 while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		 {
			$tsql2 = "SELECT Tbl_SE_Program.program_code,
						Tbl_SE_Program.program_name,Tbl_SE_Program.program_desc
						FROM Tbl_SE_Program, Tbl_SE_UserProgram
						WHERE Tbl_SE_UserProgram.program_code=Tbl_SE_Program.program_code
						AND Tbl_SE_UserProgram.user_id='$userid'
						AND Tbl_SE_Program.program_group='$row[0]'
						ORDER BY program_urut";
			  $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
			  $params2 = array(&$_POST['query']);

			  $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);
			  if ( $sqlConn2 === false)
			  die( FormatErrors( sqlsrv_errors() ) );
			  if(sqlsrv_has_rows($sqlConn2))
			  {
				 $rowCount2 = sqlsrv_num_rows($sqlConn2);
				 
				 if($row['0']=="$programgcd"){
					 echo "<li class='selected'>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }else{
					 echo "<li>";
					 echo "<a href='#'><span>".$row['1']."</span></a>";
				 }
				 
				 if($rowCount2 > '15')
				 {
					echo "<ul style='overflow-x:hidden;overflow-y:scroll;white-space:nowrap;height:150px;'>";
				 }else{
					echo "<ul>";
				 }
				 
				 while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
				 {
					if($row2[2]=="$programname" && $row2[0]=="$programcd")
					{
						echo "<li class='sub_selected'><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}else{
						echo "<li><a href='../../../page/".$row2[2].".php?r=".$randomtime."&userwfid=".$row2[0]."'>".$row2[1]."</a></li>";
					}
				 }
			  }
			  sqlsrv_free_stmt( $sqlConn2 );
				 echo "</ul>";
				 echo "</li>";
		 }
	  }
	  sqlsrv_free_stmt( $sqlConn );
?>
						<li>
							<a href="#"><span>SETTING</span></a>
							<ul>
								<li><a href='../../changepassword.php?userwfid=changepassword'>Change Password</a></li>
								<li><a href='../../logout.php'>Log Out</a></li>
							</ul>
						</li>
					</ul>
				</div>
<!--Close Menu-->
	<img src="../../../images/gimmick_logo.png" style="position:absolute;bottom:0;width:60%;margin-left:20%;margin-right:20%;"></img>
</div>
<div style="float:right;width:75%;margin-top:7%;margin-right:2%;">
<div align="center">
<font>OVERVIEW ALL LEGAL PROCESS</font>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <th rowspan="2" scope="col">No.&nbsp;</th>
    <th rowspan="2" scope="col">NO PROPOSAL&nbsp;</th>
    <th colspan="2" scope="col">ORIGINATOR</th>
    <th colspan="2" scope="col">DEBITUR</th>
    <th rowspan="2" scope="col">TANGGAL MASUK</th>
    <th rowspan="2" scope="col">STAFF LEGAL</th>
    <th rowspan="2" scope="col">ORDER NOTARY&nbsp;</th>
    <th rowspan="2" scope="col">STATUS&nbsp;</th>
    <th rowspan="2" scope="col">TANGGAL PK&nbsp;</th>
    <th rowspan="2" scope="col">ALASAN / KETERANGAN&nbsp;</th>
  </tr>
  <tr>
    <th scope="col">CABANG</th>
    <th scope="col">MO</th>
    <th scope="col">NAMA</th>
    <th scope="col">JENIS</th>
    </tr>
    
<?php
	$tsql = "select 
			cmp.custnomid as APPS_ID,
			(select branch_name from tbl_branch where branch_code = cmp.custbranchcode) as BRANCH,
			(select user_name from Tbl_SE_User where user_id = start.txn_user_id) as MO,
			case when cmp.custsex='0' then cmp.custbusname else cmp.custfullname end as DEBITUR,
			case when cmp.custsex = '0' then 'BADAN USAHA' else 'PERORANGAN' end as TIPE,
			cast(ldoc.txn_time as varchar) as INLEGAL,
			(select user_name from Tbl_SE_User where user_id = ldoc.txn_user_id) as STAFF_LEGAL,
			'NOT IMPLEMENTED' as NOTARY,
			case when isnull(lspk.txn_time,'')='' then 'NOT YET' else 'DONE' end as PKSTAT,
			case when isnull(lspk.txn_time,'')='' then 'NOT YET' else cast(lspk.txn_time as varchar) end as PKTIME,
			case when isnull(lspk.txn_notes,'')='' then '-' else lspk.txn_notes end as REASON
			from Tbl_CustomerMasterPerson2 cmp 
			left join Tbl_FSTART start on cmp.custnomid = start.txn_id
			left join Tbl_FLDOC ldoc on cmp.custnomid = ldoc.txn_id
			left join Tbl_FLSPK lspk on cmp.custnomid = lspk.txn_id
			where isnull(ldoc.txn_time,'') <> '' and ldoc.txn_time between CONVERT(datetime,$tglStart,121) and CONVERT(datetime,$tglFinish,121) $KondisiRegion";
			//echo $tsql;
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);

	$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
	if ( $sqlConn === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlConn))
	{
		$x = 1;
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
		{
			echo "<tr>";
			echo "<td>".$x."</td>";
			echo "<td>".$row[0]."</td>";
			echo "<td>".$row[1]."</td>";
			echo "<td>".$row[2]."</td>";
			echo "<td>".$row[3]."</td>";
			echo "<td>".$row[4]."</td>";
			echo "<td>".$row[5]."</td>";
			echo "<td>".$row[6]."</td>";
			echo "<td>".$row[7]."</td>";
			echo "<td>".$row[8]."</td>";
			echo "<td>".$row[9]."</td>";
			echo "<td>".$row[10]."</td>";
			echo "</tr>";
			$x++;
		}
	}
	sqlsrv_free_stmt( $sqlConn );
?>
</table>
</div>
</div>
</body>
</html>