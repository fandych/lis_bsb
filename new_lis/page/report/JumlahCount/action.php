<?php
include ("class.sqlsrv.php");
$sqlsrv = new SQLSRV();
$sqlsrv->connect();

$query = "select * from Tbl_Branch";
		
$sqlsrv->executeQuery($query);
$result = $sqlsrv->lastResults;
?>
<html>
	<head>
	<script type="text/javascript" src="jquery-1.10.2.js"></script>
	<script type="text/javascript" src="jquery-ui-1.10.4.custom.js"></script>
	<script type="text/javascript">	
		 $(function() {
			$( "#sd" ).datepicker({ dateFormat: 'yy/mm/dd', changeMonth: true, changeYear: true, });
			$( "#dari" ).datepicker({ dateFormat: 'yy/mm/dd', changeMonth: true,changeYear: true, });
		  });
		  
		function valid()
		{

			var dari =document.getElementById("dari").value;
			var sd =document.getElementById("sd").value;
			var branch =document.getElementById("branch").value;
			if(dari=="")
			{
				alert("Mulai tanggal harus di isi");
				
			}
			else if(sd=="")
			{
				alert("Sampai tanggal harus di isi");
				
			}
			else if(branch=="")
			{
				alert("Branch harus di isi");
				
			}
			else
			{
				document.action.submit();
			}

		}
	</script>
	<link href="jquery-ui-1.8rc3.custom.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body,td,tr,table{font-size:16px}
		input,select{background:#00CCFF;color:#000000;border:3px solid blue;}
		td{color:darkblue}
		select{width:200px}
		table{border-collapse:collapse;}
		
	</style>
	
	</head>
	<body>
		<form action="jumlah.php" method="post" name="action" id="action">
		<table style="width:500px;" border="0" align="center">
		  <tr>
				<td style="width:200px;">Mulai tanggal </td>
				<td>:</td>
				<td><input  id="dari" name="dari" type="text" readonly="readonly" style="width:200px"></td>
		  </tr>
		  <tr>
				<td>Sampai tanggal </td>
				<td>:</td>
				<td><input name="sd" id="sd" type="text" readonly="readonly" style="width:200px"></td>
		  </tr>
		  <tr>
				<td>Kode Branch </td>
				<td>:</td>
				<td><select name="branch" id="branch">
					<?
						for($x=0;$x<count($result);$x++)
						{
							echo '<option value="'.$result[$x]['branch_code'].'">'.$result[$x]['branch_code'].' - '.$result[$x]['branch_name'].'</option>';
						}
					?>
				
				</select>
				</td>
		  </tr>
		  <tr>
			  <td colspan="3" align="center"><input type="button" value="Submit" onclick="valid();"></td>
		  </tr>
		</table>
		</form>
	</body>
</html>