<?php
require("../../lib/sqlsrv.lis.php");
require("../../lib/sqlsrv.collection.php");
require("../../lib/sqlsrv.targeting.php");
require("../../lib/class.odbc.php");
ob_start();

$db_lis = new DB_LIS();
$db_lis->connect();

//$db_col = new DB_COLLECTION();
//$db_col->connect();

$db_tar = new DB_TARGETING();
$db_tar->connect();


$custnomid = $_REQUEST['custnomid'];

function PMT($i, $n, $p)
{
	return $i * $p * pow((1 + $i), $n) / (1 - pow((1 + $i), $n));
}


$sql = "select * from Tbl_CustomerMasterPerson2 where custnomid = '$custnomid'";
$db_lis->executeQuery($sql);
$result = $db_lis->lastResults;

$custnomid= $result[0]['custnomid'];
$custtarid= $result[0]['custtarid'];
$custaplno= $result[0]['custaplno'];
$custapldate= $result[0]['custapldate'];
$custaocode= $result[0]['custaocode'];
$custbranchcode= $result[0]['custbranchcode'];
$custlkcdno= $result[0]['custlkcdno'];
$custproccode= $result[0]['custproccode'];
$custcurcode= $result[0]['custcurcode'];
$custplafond= $result[0]['custplafond'];
$custfullname= $result[0]['custfullname'];
$custshortname= $result[0]['custshortname'];
$email= $result[0]['email'];
$custsex= $result[0]['custsex'];
$custjabatancode= $result[0]['custjabatancode'];
$custktpno= $result[0]['custktpno'];
$custktpexp= $result[0]['custktpexp'];
$custboddate= $result[0]['custboddate'];
$custbodplace= $result[0]['custbodplace'];
$custnpwpno= $result[0]['custnpwpno'];
$custmothername= $result[0]['custmothername'];
$custeducode= $result[0]['custeducode'];
$custmarcode= $result[0]['custmarcode'];
$custmarname= $result[0]['custmarname'];
$custjmltanggungan= $result[0]['custjmltanggungan'];
$custaddr= $result[0]['custaddr'];
$custrt= $result[0]['custrt'];
$custrw= $result[0]['custrw'];
$custkel= $result[0]['custkel'];
$custkec= $result[0]['custkec'];
$custcity= $result[0]['custcity'];
$custprop= $result[0]['custprop'];
$custzipcode= $result[0]['custzipcode'];
$custtelp= $result[0]['custtelp'];
$custhp= $result[0]['custhp'];
$custfax= $result[0]['custfax'];
$custhomestatus= $result[0]['custhomestatus'];
$custhomeyearlong= $result[0]['custhomeyearlong'];
$custhomemonthlong= $result[0]['custhomemonthlong'];
$custaddrktp= $result[0]['custaddrktp'];
$custrtktp= $result[0]['custrtktp'];
$custrwktp= $result[0]['custrwktp'];
$custkelktp= $result[0]['custkelktp'];
$custkecktp= $result[0]['custkecktp'];
$custcityktp= $result[0]['custcityktp'];
$custpropktp= $result[0]['custpropktp'];
$custzipcodektp= $result[0]['custzipcodektp'];
$custbustype= $result[0]['custbustype'];
$custbusname= $result[0]['custbusname'];
$custbusaddr= $result[0]['custbusaddr'];
$custbustelp= $result[0]['custbustelp'];
$custbusfax= $result[0]['custbusfax'];
$custbushp= $result[0]['custbushp'];
$custbusaktano= $result[0]['custbusaktano'];
$custbusaktadate= $result[0]['custbusaktadate'];
$custbusnpwp= $result[0]['custbusnpwp'];
$custbussiup= $result[0]['custbussiup'];
$custbustdp= $result[0]['custbustdp'];
$custbusyearlong= $result[0]['custbusyearlong'];
$custbusmonthlong= $result[0]['custbusmonthlong'];
$custcreditstatus= $result[0]['custcreditstatus'];
$custcreditneed1= $result[0]['custcreditneed1'];
$custcredittype1= $result[0]['custcredittype1'];
$custcreditplafond1= $result[0]['custcreditplafond1'];
$custcreditlong1= $result[0]['custcreditlong1'];
$custcreditneed2= $result[0]['custcreditneed2'];
$custcredittype2= $result[0]['custcredittype2'];
$custcreditplafond2= $result[0]['custcreditplafond2'];
$custcreditlong2= $result[0]['custcreditlong2'];
$custprointerviewnotes= $result[0]['custprointerviewnotes'];
$custowner1jabatancode= $result[0]['custowner1jabatancode'];
$custowner1name= $result[0]['custowner1name'];
$custowner1saham= $result[0]['custowner1saham'];
$custowner1lamagabung= $result[0]['custowner1lamagabung'];
$custowner1ktp= $result[0]['custowner1ktp'];
$custowner1npwp= $result[0]['custowner1npwp'];
$custowner2jabatancode= $result[0]['custowner2jabatancode'];
$custowner2name= $result[0]['custowner2name'];
$custowner2saham= $result[0]['custowner2saham'];
$custowner2lamagabung= $result[0]['custowner2lamagabung'];
$custowner2ktp= $result[0]['custowner2ktp'];
$custowner2npwp= $result[0]['custowner2npwp'];
$custowner3jabatancode= $result[0]['custowner3jabatancode'];
$custowner3name= $result[0]['custowner3name'];
$custowner3saham= $result[0]['custowner3saham'];
$custowner3lamagabung= $result[0]['custowner3lamagabung'];
$custowner3ktp= $result[0]['custowner3ktp'];
$custowner3npwp= $result[0]['custowner3npwp'];
$custnomobfacility= $result[0]['custnomobfacility'];
$custnomomsetcode= $result[0]['custnomomsetcode'];
$custnomlamausahacode= $result[0]['custnomlamausahacode'];
$custnomplafondcode= $result[0]['custnomplafondcode'];
$custnomcreditfu= $result[0]['custnomcreditfu'];
$custnomnotes= $result[0]['custnomnotes'];
$custnomfrom= $result[0]['custnomfrom'];
$custnomperkenalan= $result[0]['custnomperkenalan'];
$custprostsusahacode= $result[0]['custprostsusahacode'];
$custprostskredit= $result[0]['custprostskredit'];
$custprointerview= $result[0]['custprointerview'];
$custpropendapatan= $result[0]['custpropendapatan'];
$custprodate= $result[0]['custprodate'];
$flag= $result[0]['flag'];

if($custbusname != "")
{
	$get_name = $custbusname;
}
else
{
	$get_name = $custfullname;
}

$get_zip_state = "SELECT * FROM param_zipcode WHERE zipcode = '$custzipcode'";
$db_lis->executeQuery($get_zip_state);
$get_zip_state = $db_lis->lastResults;

$zip_state = $get_zip_state[0]['propinsi'];

$sql_get_region = "
select * from Tbl_Branch where branch_code = '$custbranchcode'
";
$db_lis->executeQuery($sql_get_region);
$region_result = $db_lis->lastResults;
$region_code = $region_result[0]['branch_region_code'];

$sql = "select * from tbl_master_mpk where custnomid = '$custnomid'";
$db_lis->executeQuery($sql);
$result = $db_lis->lastResults;

$mpk_kepada= $result[0]['mpk_kepada'];
$mpk_dari= $result[0]['mpk_dari'];
$mpk_tgl= $result[0]['mpk_tgl'];
$mpk_tgl_lfk= $result[0]['mpk_tgl_lfk'];
$mpk_komite_kredit= $result[0]['mpk_komite_kredit'];
$mpk_no_lkk= $result[0]['mpk_no_lkk'];
$mpk_nominal_pencairan1= $result[0]['mpk_nominal_pencairan1'];
$mpk_tgl_valuta_pencairan1= $result[0]['mpk_tgl_valuta_pencairan1'];
$mpk_keterangan1= $result[0]['mpk_keterangan1'];
$mpk_nominal_pencairan2= $result[0]['mpk_nominal_pencairan2'];
$mpk_tgl_valuta_pencairan2= $result[0]['mpk_tgl_valuta_pencairan2'];
$mpk_keterangan2= $result[0]['mpk_keterangan2'];
$mpk_tgl_notaris= $result[0]['mpk_tgl_notaris'];
$mpk_nama_notaris= $result[0]['mpk_nama_notaris'];
$mpk_provisi_rpk= $result[0]['mpk_provisi_rpk'];
$mpk_biaya_admin_kredit= $result[0]['mpk_biaya_admin_kredit'];
$mpk_biaya_notaris= $result[0]['mpk_biaya_notaris'];
$mpk_total_biaya= $result[0]['mpk_total_biaya'];
$mpk_atas_nama_bm= $result[0]['mpk_atas_nama_bm'];
$mpk_no_rek_bm= $result[0]['mpk_no_rek_bm'];
$mpk_tgl_valuta_bm= $result[0]['mpk_tgl_valuta_bm'];
$mpk_nominal_bm1= $result[0]['mpk_nominal_bm1'];
$mpk_ttun_no_bm= $result[0]['mpk_ttun_no_bm'];
$mpk_jangka_waktu1= $result[0]['mpk_jangka_waktu1'];
$mpk_jangka_waktu2= $result[0]['mpk_jangka_waktu2'];
$mpk_nominal_bm2= $result[0]['mpk_nominal_bm2'];
$mpk_dana= $result[0]['mpk_dana'];
$mpk_nama_bank= $result[0]['mpk_nama_bank'];
$mpk_atas_nama= $result[0]['mpk_atas_nama'];
$mpk_no_rek= $result[0]['mpk_no_rek'];
$mpk_tgl_valuta= $result[0]['mpk_tgl_valuta'];
$mpk_nominal= $result[0]['mpk_nominal'];
$mpk_ppkspk= $result[0]['mpk_ppkspk'];
$mpk_bagian_marketing= $result[0]['mpk_bagian_marketing'];
$mpk_administrasi_kredit= $result[0]['mpk_administrasi_kredit'];
$mpk_loan_processing= $result[0]['mpk_loan_processing'];
$mpk_custnomid= $result[0]['custnomid'];
$mpk_x_angsuran= $result[0]['mpk_x_angsuran'];
$mpk_asuransi_kredit= $result[0]['mpk_asuransi_kredit'];
$mpk_asuransi_kebakaran= $result[0]['mpk_asuransi_kebakaran'];
$mpk_debet_atasnama= $result[0]['mpk_debet_atasnama'];
$mpk_debet_norek= $result[0]['mpk_debet_norek'];
$mpk_last_branch= $result[0]['mpk_last_branch'];
$mpk_last_remarks= $result[0]['mpk_last_remarks'];
$mpk_no_suratpermohonan= $result[0]['mpk_no_suratpermohonan'];

$sql = "select * from tbl_newpk where custnomid = '$custnomid'";
$db_lis->executeQuery($sql);
$result = $db_lis->lastResults;

$pk_custnomid= $result[0]['custnomid'];
$pkseq= $result[0]['pkseq'];
$pknametanggal= $result[0]['pknametanggal'];
$pknumber= $result[0]['pknumber'];
$pknohost= $result[0]['pknohost'];
$pkdatenow= $result[0]['pkdatenow'];
$pkhari= $result[0]['pkhari'];
$pktanggal= $result[0]['pktanggal'];
$pcabang= $result[0]['pcabang'];
$pkbankwakila= $result[0]['pkbankwakila'];
$pkbankwakilb= $result[0]['pkbankwakilb'];
$pkbanksk1= $result[0]['pkbanksk1'];
$pkbanktanggalsk1= $result[0]['pkbanktanggalsk1'];
$pkbanksk2= $result[0]['pkbanksk2'];
$pkbanktanggalsk2= $result[0]['pkbanktanggalsk2'];
$pkdebitur= $result[0]['pkdebitur'];
$pkterbilangadministrasi= $result[0]['pkterbilangadministrasi'];
$pkketentuanlain= $result[0]['pkketentuanlain'];
$pknocabang= $result[0]['pknocabang'];
$pkfaxcabang= $result[0]['pkfaxcabang'];
$pkaltaddrdebitur= $result[0]['pkaltaddrdebitur'];
$pkalttelepondebitur= $result[0]['pkalttelepondebitur'];
$pkfaxdebitur= $result[0]['pkfaxdebitur'];
$pkpengadilannegri= $result[0]['pkpengadilannegri'];
$pknamajabatan1= $result[0]['pknamajabatan1'];
$pkjabatan1= $result[0]['pkjabatan1'];
$pknamajabatan2= $result[0]['pknamajabatan2'];
$pkjabatan2= $result[0]['pkjabatan2'];
$pkmmu= $result[0]['pkmmu'];
$pktglstatus= $result[0]['pktglstatus'];
$pknamadebitur= $result[0]['pknamadebitur'];

$sql = "select * from Tbl_CustomerFacility2 where custnomid = '$custnomid'";
$db_lis->executeQuery($sql);
$result = $db_lis->lastResults;

//$custnomid = $result[0]['custnomid'];
$custfacseq = $result[0]['custfacseq'];
$custcreditneed = $result[0]['custcreditneed'];
$custcredittype = $result[0]['custcredittype'];
$custcreditplafond = $result[0]['custcreditplafond'];
$custcreditlong = $result[0]['custcreditlong'];
$mkkexistingoutstanding = $result[0]['mkkexistingoutstanding'];
$mkkexistingbungaflag = $result[0]['mkkexistingbungaflag'];
$mkkexistingprovisi = $result[0]['mkkexistingprovisi'];
$mkkexistingkewajibanbank = $result[0]['mkkexistingkewajibanbank'];
$mkkexistingangsuran = $result[0]['mkkexistingangsuran'];
$mkkexistinglnparn = $result[0]['mkkexistinglnparn'];
$mkkexistingjhrate = $result[0]['mkkexistingjhrate'];
$mkkexistingvariancecode = $result[0]['mkkexistingvariancecode'];
$mkkexistingvariancerate = $result[0]['mkkexistingvariancerate'];
$mkknewbungaflag = $result[0]['mkknewbungaflag'];
$mkknewadm = $result[0]['mkknewadm'];
$mkknewprovisi = $result[0]['mkknewprovisi'];
$mkknewkewajibanbank = $result[0]['mkknewkewajibanbank'];
$mkknewangsuran = $result[0]['mkknewangsuran'];
$mkknewlnparn = $result[0]['mkknewlnparn'];
$mkknewjhrate = $result[0]['mkknewjhrate'];
$mkknewvariancecode = $result[0]['mkknewvariancecode'];
$mkknewvariancerate = $result[0]['mkknewvariancerate'];
$mkk_tujuan_existing = $result[0]['mkk_tujuan_existing'];
$mkk_jangka_waktu = $result[0]['mkk_jangka_waktu'];
$mpk_nominal_pencairan = $result[0]['mpk_nominal_pencairan'];
$mpk_tgl_valuta_pencairan = $result[0]['mpk_tgl_valuta_pencairan'];
$mpk_keterangan = $result[0]['mpk_keterangan'];
//$mpk_jangka_waktu1 = $result[0]['mpk_jangka_waktu1'];
//$mpk_jangka_waktu2 = $result[0]['mpk_jangka_waktu2'];
$mpk_waktu_angsuran1 = $result[0]['mpk_waktu_angsuran1'];
$mpk_waktu_angsuran2 = $result[0]['mpk_waktu_angsuran2'];
$sukubungayangdiberikan = $result[0]['sukubungayangdiberikan'];
$pkterbilangbunga = $result[0]['pkterbilangbunga'];
$pkterbilangangsuran = $result[0]['pkterbilangangsuran'];
$mpk_blokir = $result[0]['mpk_blokir'];


//TURN OFF COLLECTION

$plafond = $custcreditplafond;
$periode = $custcreditlong;
$bunga = $sukubungayangdiberikan;

$tgl_awal = new DateTime($mpk_jangka_waktu1);

$resultPMT = round(-PMT($bunga/100/12, $periode, $plafond), 2);

$angsuranpokok = 0;
$saldopokok = $plafond;
$jmlangsuran = 0;
$jmlangsuranpokok = 0;
$jmgangsuranbunga = 0;
$angsuranperbulan = $resultPMT;

for($i = 1; $i <= $periode ; $i++)
{
	//$saldopokok = $saldopokok - $angsuranpokok;

	$angsuranbunga = round($saldopokok*($bunga/12/100), 2);
	$angsuranpokok = round($resultPMT - $angsuranbunga, 2);
	$saldopokok = round($saldopokok - $angsuranpokok, 2);

	$jmlangsuran = $jmlangsuran + $angsuranperbulan;
	$jmlangsuranpokok = $jmlangsuranpokok + $angsuranpokok;
	$jmgangsuranbunga = $jmgangsuranbunga + $angsuranbunga;

}

$sql_cust_master = "
insert into
collect_custmaster
(_custnomid,
_cust_cif,
_cust_lisid,
_cust_listype,
_cust_plafond,
_cust_angsuran,
_cust_bunga,
_cust_tglpinjam,
_cust_tglselesai,
_cust_jangkawaktu,
_cust_region_id,
_cust_branch_id,
_cust_name,
_cust_addr,
_cust_kelurahan,
_cust_kecamatan,
_cust_kota,
_cust_propinsi,
_cust_kodepos,
_cust_telp1,
_cust_telp2,
_cust_telp3,
_cust_hp1,
_cust_hp2,
_cust_hp3,
_cust_email1,
_cust_email2,
_cust_email3,
_cust_orangterdekat1,
_cust_orangterdekat2,
_cust_norek,
_cust_collector_id,
_cust_kolektibilitas_id,
_cust_createtime,
_cust_flag)

values

('$custnomid',
'$custaplno',
'$custcreditneed',
'$custproccode',
'$custcreditplafond',
'$angsuranperbulan',
'$sukubungayangdiberikan',
'$mpk_jangka_waktu1',
'$mpk_jangka_waktu2',
'$custcreditlong',
'$region_code',
'$custbranchcode',
'$custfullname',
'$custaddr',
'$custkel',
'$custkec',
'$custcity',
'$custprop',
'$custzipcode',
'$custtelp',
'-',
'-',
'$custhp',
'-',
'-',
'$email',
'-',
'-',
'-',
'-',
'$mpk_no_rek',
'col_id',
'L',
getdate(),
'0')
";

//echo $sql_cust_master."<br><br>";

//$db_col->executeNonQuery($sql_cust_master);

/*
	echo "'i',
	'tgl_jatuh_tempo',
	'nominal',
	'saldopokok',
	'angsuranpokok',
	'angsuranbunga',
	'saldo_akhir'"."<br>";
	
*/

$angsuranpokok = 0;
$saldopokok = $plafond;
$angsuranperbulan = $resultPMT;
for($i = 1; $i <= $periode ; $i++)
{
	//$saldopokok = $saldopokok - $angsuranpokok;

	$angsuranbunga = round($saldopokok*($bunga/12/100), 0);
	$angsuranpokok = round($resultPMT - $angsuranbunga, 0);

	$saldo_akhir = round($saldopokok - $angsuranpokok,0);

	$month = $tgl_awal->format('m');
	$day = $tgl_awal->format('d');
	$year = $tgl_awal->format('Y');

	$tgl_calc = mktime(0,0,0,$month+$i,$day,$year);
	$tgl_jatuh_tempo = date('Y/m/d',$tgl_calc);

	$sql_cust_txn = "
	insert into
	dbo.collect_custtxn
	(_custnomid,
	_txn_tagihanke,
	_txn_tgljatuhtempo,
	_txn_nominal,
	_txn_saldoawal,
	_txn_pokok,
	_txn_bunga,
	_txn_saldoakhir,
	_txn_denda,
	_txn_collectorid,
	_txn_statusbayar,
	_txn_createtime,
	_txn_flag)

	values

	('$custnomid',
	'$i',
	'$tgl_jatuh_tempo',
	'$angsuranperbulan',
	'$saldopokok',
	'$angsuranpokok',
	'$angsuranbunga',
	'$saldo_akhir',
	'0',
	'col_id',
	'N',
	getdate(),
	'')
	";

	//echo $sql_cust_txn."<br>";
	/*
	echo "'$i',
	'$tgl_jatuh_tempo',
	'$angsuranperbulan',
	'$saldopokok',
	'$angsuranpokok',
	'$angsuranbunga',
	'$saldo_akhir'"."<br>";
	*/

	//$db_col->executeNonQuery($sql_cust_txn);


	$saldopokok = round($saldopokok - $angsuranpokok, 0);
}


//TURN OFF TARGETING

$sql_insert_realisasi = "
insert into targeting_realisasi
(_custnomid
,_cust_cif
,_cust_loantype
,_cust_name
,_cust_creditname
,_cust_plafond
,_cust_angsuran
,_cust_tglpinjam
,_cust_tglselesai
,_cust_creditlong
,_cust_ao_code
,_cust_branch_id
,_cust_region_id
,_custzipcode
,_custstatecode
,_custcreatetime)
values
('$custnomid'
,'$custaplno'
,'$custproccode'
,'$custfullname'
,'$custproccode'
,'$custcreditplafond'
,'$angsuranperbulan'
,'$mpk_jangka_waktu1'
,'$mpk_jangka_waktu2'
,'$custcreditlong'
,'$custaocode'
,'$custbranchcode'
,'$region_code'
,'$custzipcode'
,'$zip_state'
,getdate())
";


// _custzipcode sudah ada _custstatecode diisi dengan zipcode atau yg lain ?



//echo $sql_insert_realisasi."<br>";

//$db_tar->executeNonQuery($sql_insert_realisasi);


//////////////////////////////////////////// DRAWDOWN TO LNSPK //////////////////////////////////////////




$ibm_connection = "
Driver={Client Access ODBC Driver (32-bit)};
System=172.17.115.1;
Uid=ELOSUSR;
Pwd=ELOSUSR;
";

$db_ibm = new DB_ODBC();
$db_ibm->connect($ibm_connection,"ELOSUSR","ELOSUSR");


$user_id = "DEVUSR";
$workstation = "DEVPC01";
$time = substr(time(),0,6);
$custnomid = $_REQUEST['custnomid'];


$sql = "
select
cmp.custnomid as customer_id
,cmp.custaplno as customer_cif_number
,cmp.custfullname as customer_fullname
,cmp.custaddr + CHAR(32) + cmp.custrt + CHAR(32) + cmp.custrw + CHAR(32) + cmp.custkec + CHAR(32) + cmp.custkel + CHAR(32) + cmp.custcity as customer_address
,cmp.custbranchcode as branch_code
,cmp.custaocode as officer_code
,cmp.custproccode as credit_type_code
,cmp.custapldate as application_date
,pk.pkseq as pk_sequence
,pk.pknumber as pk_number
,pk.pktanggal as pk_date
,pk.pkdatenow as pk_datetime
,mpk.mpk_jangka_waktu1 as credit_start
,mpk.mpk_jangka_waktu2 as credit_end
,mpk.mpk_biaya_notaris as cost_notaris
,cf.mkknewprovisi as cost_provisi_percent
,cf.mkknewadm as cost_administration
,cf.custcreditlong as tenor
,cf.custcreditplafond as plafond
,cf.sukubungayangdiberikan as interest
,cf.mpk_tgl_valuta_pencairan as valuta_date
,cf.mpk_waktu_angsuran1 as due_date
,cps._pasangan_namadepan as spouse_fullname
,cps._pasangan_nomorktp as spouse_id_card_number
,cps._pasangan_namaperusahaan as spouse_company_name
,cps._pasangan_penghasilanbersih as spouse_income
,cps._pasangan_pekerjaan_id as spouse_job_name
from
tbl_customermasterperson2 cmp
join tbl_customerfacility2 cf on cmp.custnomid = cf.custnomid
join tbl_master_mpk mpk on cmp.custnomid = mpk.custnomid
join tbl_newpk pk on cmp.custnomid = pk.custnomid
join pl_custpasangan2 cps on cmp.custnomid = cps._custnomid
where cmp.custnomid = '$custnomid'
";
$db_lis->executeQuery($sql);
$result = $db_lis->lastResults;

echo "<pre>";
print_r($result);
echo "</pre>";


$customer_id = $result[0]['customer_id'];
$customer_cif_number = $result[0]['customer_cif_number'];$customer_cif_number = "80407134624000";
$customer_address_number = "80407134750000";
$customer_fullname = $result[0]['customer_fullname'];
$customer_address = $result[0]['customer_address'];
$branch_code = $result[0]['branch_code'];$branch_code = '140';
$officer_code = $result[0]['officer_code'];$officer_code = '00140';
$credit_type_code = $result[0]['credit_type_code'];
$application_date = str_replace("/","",$result[0]['application_date']);
$pk_sequence = $result[0]['pk_sequence'];
$pk_number = $result[0]['pk_number'];
$pk_date = str_replace("/","",$result[0]['pk_date']);
$pk_datetime = str_replace("/","",$result[0]['pk_datetime']);
$credit_start = str_replace("/","",$result[0]['credit_start']);
$credit_end = str_replace("/","",$result[0]['credit_end']);
$next_credit_start = str_replace("/","",$result[0]['credit_start']);
$tenor = $result[0]['tenor'];
$plafond = $result[0]['plafond'];
$interest = $result[0]['interest'] / 100;
$cost_notaris = $result[0]['cost_notaris'];
$cost_provisi_percent = $result[0]['cost_provisi_percent'] / 100;
$cost_provisi = ($cost_provisi_percent / 100) * $plafond;
$cost_administration = $result[0]['cost_administration'];
$valuta_date = $result[0]['valuta_date'];
$due_date = str_replace("/","",$result[0]['due_date']);$due_date = "25";
$spouse_fullname = $result[0]['spouse_fullname'];
$spouse_id_card_number = $result[0]['spouse_id_card_number'];
$spouse_company_name = $result[0]['spouse_company_name'];
//$spouse_job_code = $result[0]['spouse_job_code'];
$spouse_job_name = $result[0]['spouse_job_name'];
$spouse_income = $result[0]['spouse_income'];

include_once("drawdown_to_lnspk.php");

$insert_lnspk = "
INSERT INTO VISIONT.LNSPK
(LNNOPK,LNNOUT,LNJNPK,LNPKST,LNDTPK,LNAPPK,LNNAMK,LNADDK,LNNOOB,LNNOCB,LNNORB,LNNOAC,LNNOPT,LNNORN,LNNONP,LNNORP,LNCDLN,LNCDFC,LNCDPL,LNNOGL,LNNOOG,LNSNAM,LNOOFF,LNCUOF,LNCOOF,LNCDCF,LNFLRA,LNPPGL,LNPPRB,LNPPST,LNSNIR,LNSNBL,LNSNP1,LNSNP2,LNSNP3,LNSNMT,LNBPI1,LNBPI2,LNBPI3,LNBPI4,LNBPI5,LNCDCL,LNCDPP,LNCDBS,LNCDTY,LNCDTA,LNCDTX,LNCDIT,LNCDC1,LNCDC2,LNCDC3,LNCDC4,LNCDC5,LNCDPD,LNCDRD,LNCDSF,LNCDYB,LNCDPM,LNCDCP,LNCDTC,LNCDPF,LNCDIF,LNCDRN,LNCDPS,LNCDAM,LNCDCB,LNCDSP,LNCDIP,LNCDER,LNCD98,LNCDMQ,LNCDRE,LNCDCA,LNCDNL,LNCDRP,LNCDBC,LNCDCO,LNCDST,LNCDEO,LNCDOD,LNCDQU,LNCDJS,LNSWA1,LNSWA2,LNSWA3,LNSWA4,LNNOEX,LNNORV,LNNODY,LNNOCR,LNNOCS,LNNORS,LNNODP,LNCDRC,LNCDCC,LNCDCY,LNCDPC,LNCDPB,LNCDI1,LNCDI2,LNCDI3,LNCDI4,LNCDI5,LNCDLC,LNCDLJ,LNRTTA,LNACTA,LNBLTA,LNCDIR,LNCDYT,LNCDPO,LNRTII,LNNOPC,LNNOP1,LNNOP2,LNNOP3,LNNOP4,LNNODL,LNNOPE,LNNOPM,LNCDSI,LNCDBI,LNCDTT,LNNOPD,LNNOPP,LNNOTP,LNNOFP,LNNOIF,LNIDBP,LNCDP1,LNCDP2,LNCDP3,LNCDP4,LNCDP5,LNNOCT,LNRTNM,LNRTPN,LNRTCA,LNRTI1,LNRTI2,LNRTI3,LNRTI4,LNRTI5,LNRTEF,LNRTFN,LNRTFX,LNRTMI,LNRTMD,LNRTCR,LNRTDL,LNRTOR,LNCDPW,LNDTPJ,LNDTPP,LNDTAN,LNDTXD,LNDTXT,LNDTTF,LNDTXF,LNDTRA,LNDTFP,LNDTNE,LNDTEN,LNDTNT,LNDTPD,LNDTCO,LNDTPO,LNDTNA,LNDTNS,LNDTSL,LNDTBI,LNDTLI,LNDTLL,LNDTLB,LNDTLR,LNDTJA,LNDTCL,LNDTLP,LNDTLD,LNDTND,LNDTMT,LNDTOM,LNDTLV,LNDTNV,LNDTSC,LNDTEC,LNDTCR,LNDTCD,LNDTNR,LNDI1S,LNDI2S,LNDI3S,LNDI4S,LNDI5S,LNDI1E,LNDI2E,LNDI3E,LNDI4E,LNDI5E,LNDTRC,LNDTFC,LNDTIC,LNDTJU,LNDTLF,LNDTPC,LNDLMP,LNDLMI,LNDLMO,LNNOPL,LNNOCN,LNNOMR,LNNOPB,LNDPAC,LNRPAC,LNATCC,LNATIP,LNATCB,LNATAI,LNATLB,LNATJC,LNATTA,LNATI1,LNATI2,LNATI3,LNATI4,LNATI5,LNATE1,LNATE2,LNATE3,LNATE4,LNATF1,LNATF2,LNATF3,LNATF4,LNATF5,LNATG1,LNATG2,LNATG3,LNATG4,LNATG5,LNATTX,LNNOBL,LNAYTA,LNAYTP,LNAY1A,LNAY2A,LNAY3A,LNAY4A,LNAY5A,LNAY1P,LNAY2P,LNAY3P,LNAY4P,LNAY5P,LNDTXM,LNAMOP,LNAMTA,LNALTA,LNAMCM,LNAMTX,LNALAT,LNAMLC,LNAMLJ,LNALPL,LNALPY,LNALPP,LNALAL,LNALAY,LNALAP,LNALJP,LNAJPY,LNALJA,LNAJAY,LNAMMI,LNACI1,LNACI2,LNACI3,LNACI4,LNACI5,LNAAI1,LNAAI2,LNAAI3,LNAAI4,LNAAI5,LNAATA,LNOVAI,LNOVAL,LNOVI1,LNOVI2,LNOVI3,LNOVI4,LNOVI5,LNOVTA,LNOMAI,LNOMAL,LNOMI1,LNOMI2,LNOMI3,LNOMI4,LNOMI5,LNOMTA,LNTMAI,LNTMAL,LNTMI1,LNTMI2,LNTMI3,LNTMI4,LNTMI5,LNTMTA,LNNIYS,LNNLYS,LNATNI,LNATNL,LNAMCB,LNAMAB,LNAMYB,LNBCOF,LNICOF,LNLCOF,LNOPCO,LNOICO,LNPCOT,LNICOT,LNIACO,LNTCCP,LNTCCI,LNTDCP,LNTDCI,LNPLIN,LNPLPR,LNPLOT,LNTDPP,LNTCPP,LNTDPI,LNTCPI,LNTDPO,LNTCPO,LNAITD,LNAIPD,LNAIYS,LNAMAI,LNTDI1,LNTCI1,LNTDI2,LNTCI2,LNTDI3,LNTCI3,LNTDI4,LNTCI4,LNTDI5,LNTCI5,LNTDE1,LNTCE1,LNTDE2,LNTCE2,LNTDE3,LNTCE3,LNTDE4,LNTCE4,LNTDF1,LNTCF1,LNTDF2,LNTCF2,LNTDF3,LNTCF3,LNTDF4,LNTCF4,LNTDF5,LNTCF5,LNTDLC,LNTCLC,LNTDLJ,LNTCLJ,LNRVCR,LNRVDR,LNADI1,LNADI2,LNADI3,LNADI4,LNADI5,LNADTA,LNADAI,LNADLC,LNADLJ,LNTDPR,LNTCPR,LNTDIN,LNTCIN,LNTDTA,LNTCTA,LNCDHC,LNCDF1,LNCDF2,LNCDF3,LNCDF4,LNCDF5,LNCDE1,LNCDE2,LNCDE3,LNCDE4,LNATYS,LNI1YS,LNI2YS,LNI3YS,LNI4YS,LNI5YS,LNLCYS,LNJCYS,LNAMBI,LNAMB1,LNAMB2,LNAMB3,LNAMB4,LNAMB5,LNAMBA,LNAMBL,LNAMBJ,LNAMAL,LNAMIR,LNAMRP,LNAMPP,LNAMLL,LNAMPO,LNCDIA,LNAMIA,LNAMAV,LNPDI1,LNPDI2,LNPDI3,LNPDI4,LNPDI5,LNPDE1,LNPDE2,LNPDE3,LNPDE4,LNPDTA,LNPDF1,LNPDF2,LNPDF3,LNPDF4,LNPDF5,LNPDPR,LNPDIN,LNAMI1,LNAMI2,LNAMI3,LNAMI4,LNAMI5,LNAME1,LNAME2,LNAME3,LNAME4,LNAMF1,LNAMF2,LNAMF3,LNAMF4,LNAMF5,LNE1YS,LNE2YS,LNE3YS,LNE4YS,LNF1YS,LNF2YS,LNF3YS,LNF4YS,LNF5YS,LNG1YS,LNG2YS,LNG3YS,LNG4YS,LNG5YS,LNTXYS,LNOAE1,LNOAE2,LNOAE3,LNOAE4,LNOAF1,LNOAF2,LNOAF3,LNOAF4,LNOAF5,LNOAG1,LNOAG2,LNOAG3,LNOAG4,LNOAG5,LNOATX,LNPDFX,LNAMTC,LNAMPV,LNAMTP,LNAMTD,LNAMPR,LNAMHB,LNAPIA,LNAMA1,LNAMA2,LNAMA3,LNAMA4,LNAOI7,LNAI17,LNAI27,LNAI37,LNAI47,LNAI57,LNAUI7,LNAUD7,LNAOD7,LNAI18,LNAI28,LNAI38,LNAI48,LNAI58,LNAMRC,LNABRC,LNAYRP,LNAYRA,LNAYRE,LNAPYP,LNAPYA,LNAPYE,LNAIPL,LNAIAL,LNAIEL,LNYSRP,LNATRP,LNTDYP,LNTCYP,LNAMPD,LNAMCR,LNAMAD,LNAMBP,LNAMPT,LNADRT,LNACMT,LNADMT,LNALPG,LNALIG,LNALOG,LNAMFX,LNAMIN,LNC2RP,LNNOH1,LNNOH2,LNNOH3,LNNOIN,LNNOMN,LNNOPA,LNN2PB,LNN2P1,LNN2P2,LNN2P3,LNNOR1,LNNOR2,LNRTTO,LNRCAS,LNFXI1,LNFXI2,LNFXI3,LNFXI4,LNFXI5,LNFXE1,LNFXE2,LNFXE3,LNFXE4,LNFXTA,LNFXF1,LNFXF2,LNFXF3,LNFXF4,LNFXF5,LNPD01,LNPD02,LNPD03,LNPD04,LNPD05,LNPD06,LNPD07,LNPD08,LNPD09,LNPD10,LNPD11,LNPD12,LNPD13,LNPD14,LNPD15,LNPD16,LNPD17,LNPD18,LNPD19,LNPD20,LNAO01,LNAO02,LNAO03,LNAO04,LNAO05,LNAO06,LNAO07,LNAO08,LNAO09,LNAO10,LNAO11,LNAO12,LNAO13,LNAO14,LNAO15,LNAO16,LNAO17,LNAO18,LNAO19,LNCDU1,LNCDU2,LNCDU3,LNCDU4,LNCDU5,LNCDU6,LNCDU7,LNCDU8,LNCDU9,LNCDUA,LNMAU1,LNMAU2,LNMAU3,LNMAU4,LNDTU1,LNDTU2,LNDTU3,LNDTU4,LNDTU5,LNDTU6,LNDTU7,LNDTU8,LNAMU1,LNAMU2,LNAMU3,LNAMU4,LNAMU5,LNAMU6,LNDSU1,LNDSU2,LNDSU3,LNDSU4,LNDSU5,LNRTU1,LNRTU2,LNRTU3,LNSTU1,LNSTU2,LNSTU3,LNSTU4,LNSTU5,LNSTU6,LNSTU7,LNNOU1,LNNOU2,LNNOU3,LNNOU4,LNOFU1,LNOFU2,LNBRU1,LNBRU2,LNCLCD,LNAMCL,LNAMVA,LNAMLD,LNMXCD,LNMXAM,LNMXPC,LNAAHL,LNPPNL,LNAPNL,LNCDNR,LNDTHL,LNAMHL,LNDSHL,LNDTST,LNDTEX,LNENUS,LNENWS,LNENTM,LNRTPV,LNCDAA,LNNKUK,LNUSD1,LNUSD2,LNUSD3,LNUSD4,LNUSD5,LNUSD6,LNPDDY,LNPDTM,LNDTQC,LNNSDK,LNNSKK,LNDSDK,LNDSKK,LNNODA,LNCDIS,LNAMLM,LNBI11,LNBI12,LNBI13,LNBI14,LNBI15,LNBI16,LNBI17,LNBI18,LNBI19,LNBI1A,LNBI21,LNBI22,LNBI23,LNBI24,LNBI25,LNBI26,LNBI27,LNBI28,LNBI29,LNBI2A,LNBI31,LNBI32,LNBI33,LNBI34,LNBI35,LNBI36,LNBI37,LNBI61,LNBI62,LNBI63,LNBI64,LNBI65,LNBI66,LNBI67,LNBI68,LNBI69,LNBI6A,LNBID1,LNBID2,LNBID3,LNBID4,LNBID5,LNBIA1,LNBIA2,LNBIA3,LNBIA4,LNBIA5,LNBIS1,LNBIS2,LNBIS3,LNCDBG,LNDYBG,LNNOBG,LNNMBG,LNACBG,LNISBG,LNRNBG,LNCLBG,LNDTBG,LNCDPG,LNMSKJ,LNNMIS,LNISID,LNPKJR,LNINST,LNNIPP,LNCGOL,LNALKR,LNALKT,LNKPRL,LNNMST,LNCOCL,LNJMN1,LNJMN2,LNJMN3,LNJMN4,LNJMN5,LNJMN6,LNJMN7,LNJMN8,LNJMN9,LNJMNA,LNJMNB,LNJMNC,LNJMND,LNJMNE,LNJMNF,LNNOSK,LNDTSK,LNBDNM,LNNIP1,LNADD1,LNKDNM,LNNIP2,LNJBTD,LNADD2,LNUNIT,LNLUAS,LNBLOK,LNNOMO,LNLOKS,LNDEVP,LNMERK,LNTHBT,LNNOCH,LNNOMS,LNWARN,LNDEAL,LNRODA,LNBGTP,LNNOPG,LNDTPG,LNUGR1,LNUGR2,LNUGR3,LNSGR1,LNSGR2,LNSGR3,LNOGR1,LNOGR2,LNOGR3,LNNOPR,LNTGPR,LNNPPK,LNNTPK,LNBIIJ,LNUMRK,LNASKK,LNPRAS,LNPABG,LNASBG,LNSPBR,LNDTBR,LNDSBR,LNAMUM,LNPSBR,LNAYBR,LNSTJM,LNNMPN,LNBULN,LNAKIV,LNAIDC,LNRTOD,LNSTBR,LNIKJM,LNNOAK,LNDTAK,LNNOSP,LNDTSP,LNNBUS,LNTBUS,LNNBPN,LNTBPN,LNNBPM,LNTBPM,LNNBPL,LNTBPL,LNNTPJ,LNTTPJ,LNNTTJ,LNTTTJ,LNNONT,LNTGNT,LNDBAM,LNCRAM,LNDBAC,LNCRAC,LNSIUP,LNSITU,LNINDT,LNINTM,LNINUS,LNINWS,LNLMDT,LNLMTM,LNLMUS,LNLMWS)
VALUES
(
'$LNNOPK',$LNNOUT,'$LNJNPK','$LNPKST',$LNDTPK,'$LNAPPK',$LNNAMK,$LNADDK,$LNNOOB,$LNNOCB,$LNNORB,$LNNOAC,$LNNOPT,$LNNORN,$LNNONP,$LNNORP,'$LNCDLN','$LNCDFC','$LNCDPL',$LNNOGL,$LNNOOG,'$LNSNAM','$LNOOFF','$LNCUOF','$LNCOOF','$LNCDCF','$LNFLRA',$LNPPGL,$LNPPRB,'$LNPPST','$LNSNIR','$LNSNBL','$LNSNP1','$LNSNP2','$LNSNP3','$LNSNMT','$LNBPI1','$LNBPI2','$LNBPI3','$LNBPI4','$LNBPI5','$LNCDCL','$LNCDPP','$LNCDBS','$LNCDTY','$LNCDTA','$LNCDTX','$LNCDIT','$LNCDC1','$LNCDC2','$LNCDC3','$LNCDC4','$LNCDC5','$LNCDPD','$LNCDRD','$LNCDSF','$LNCDYB','$LNCDPM','$LNCDCP','$LNCDTC','$LNCDPF','$LNCDIF','$LNCDRN','$LNCDPS','$LNCDAM','$LNCDCB','$LNCDSP','$LNCDIP','$LNCDER','$LNCD98','$LNCDMQ','$LNCDRE','$LNCDCA','$LNCDNL','$LNCDRP','$LNCDBC','$LNCDCO','$LNCDST','$LNCDEO','$LNCDOD','$LNCDQU','$LNCDJS','$LNSWA1','$LNSWA2','$LNSWA3','$LNSWA4',$LNNOEX,$LNNORV,$LNNODY,$LNNOCR,$LNNOCS,$LNNORS,$LNNODP,'$LNCDRC','$LNCDCC','$LNCDCY','$LNCDPC','$LNCDPB','$LNCDI1','$LNCDI2','$LNCDI3','$LNCDI4','$LNCDI5','$LNCDLC','$LNCDLJ',$LNRTTA,'$LNACTA',$LNBLTA,'$LNCDIR','$LNCDYT','$LNCDPO',$LNRTII,$LNNOPC,$LNNOP1,$LNNOP2,$LNNOP3,$LNNOP4,$LNNODL,$LNNOPE,$LNNOPM,'$LNCDSI','$LNCDBI','$LNCDTT',$LNNOPD,$LNNOPP,$LNNOTP,$LNNOFP,$LNNOIF,$LNIDBP,'$LNCDP1','$LNCDP2','$LNCDP3','$LNCDP4','$LNCDP5',$LNNOCT,$LNRTNM,$LNRTPN,$LNRTCA,$LNRTI1,$LNRTI2,$LNRTI3,$LNRTI4,$LNRTI5,$LNRTEF,$LNRTFN,$LNRTFX,$LNRTMI,$LNRTMD,$LNRTCR,$LNRTDL,$LNRTOR,'$LNCDPW',$LNDTPJ,$LNDTPP,$LNDTAN,$LNDTXD,$LNDTXT,$LNDTTF,$LNDTXF,$LNDTRA,$LNDTFP,$LNDTNE,$LNDTEN,$LNDTNT,$LNDTPD,$LNDTCO,$LNDTPO,$LNDTNA,$LNDTNS,$LNDTSL,$LNDTBI,$LNDTLI,$LNDTLL,$LNDTLB,$LNDTLR,$LNDTJA,$LNDTCL,$LNDTLP,$LNDTLD,$LNDTND,$LNDTMT,$LNDTOM,$LNDTLV,$LNDTNV,$LNDTSC,$LNDTEC,$LNDTCR,$LNDTCD,$LNDTNR,$LNDI1S,$LNDI2S,$LNDI3S,$LNDI4S,$LNDI5S,$LNDI1E,$LNDI2E,$LNDI3E,$LNDI4E,$LNDI5E,$LNDTRC,$LNDTFC,$LNDTIC,$LNDTJU,$LNDTLF,$LNDTPC,$LNDLMP,$LNDLMI,$LNDLMO,$LNNOPL,$LNNOCN,$LNNOMR,$LNNOPB,$LNDPAC,$LNRPAC,$LNATCC,$LNATIP,$LNATCB,$LNATAI,$LNATLB,$LNATJC,$LNATTA,$LNATI1,$LNATI2,$LNATI3,$LNATI4,$LNATI5,$LNATE1,$LNATE2,$LNATE3,$LNATE4,$LNATF1,$LNATF2,$LNATF3,$LNATF4,$LNATF5,$LNATG1,$LNATG2,$LNATG3,$LNATG4,$LNATG5,$LNATTX,$LNNOBL,$LNAYTA,$LNAYTP,$LNAY1A,$LNAY2A,$LNAY3A,$LNAY4A,$LNAY5A,$LNAY1P,$LNAY2P,$LNAY3P,$LNAY4P,$LNAY5P,$LNDTXM,$LNAMOP,$LNAMTA,$LNALTA,$LNAMCM,$LNAMTX,$LNALAT,$LNAMLC,$LNAMLJ,$LNALPL,$LNALPY,$LNALPP,$LNALAL,$LNALAY,$LNALAP,$LNALJP,$LNAJPY,$LNALJA,$LNAJAY,$LNAMMI,$LNACI1,$LNACI2,$LNACI3,$LNACI4,$LNACI5,$LNAAI1,$LNAAI2,$LNAAI3,$LNAAI4,$LNAAI5,$LNAATA,$LNOVAI,$LNOVAL,$LNOVI1,$LNOVI2,$LNOVI3,$LNOVI4,$LNOVI5,$LNOVTA,$LNOMAI,$LNOMAL,$LNOMI1,$LNOMI2,$LNOMI3,$LNOMI4,$LNOMI5,$LNOMTA,$LNTMAI,$LNTMAL,$LNTMI1,$LNTMI2,$LNTMI3,$LNTMI4,$LNTMI5,$LNTMTA,$LNNIYS,$LNNLYS,$LNATNI,$LNATNL,$LNAMCB,$LNAMAB,$LNAMYB,$LNBCOF,$LNICOF,$LNLCOF,$LNOPCO,$LNOICO,$LNPCOT,$LNICOT,$LNIACO,$LNTCCP,$LNTCCI,$LNTDCP,$LNTDCI,$LNPLIN,$LNPLPR,$LNPLOT,$LNTDPP,$LNTCPP,$LNTDPI,$LNTCPI,$LNTDPO,$LNTCPO,$LNAITD,$LNAIPD,$LNAIYS,$LNAMAI,$LNTDI1,$LNTCI1,$LNTDI2,$LNTCI2,$LNTDI3,$LNTCI3,$LNTDI4,$LNTCI4,$LNTDI5,$LNTCI5,$LNTDE1,$LNTCE1,$LNTDE2,$LNTCE2,$LNTDE3,$LNTCE3,$LNTDE4,$LNTCE4,$LNTDF1,$LNTCF1,$LNTDF2,$LNTCF2,$LNTDF3,$LNTCF3,$LNTDF4,$LNTCF4,$LNTDF5,$LNTCF5,$LNTDLC,$LNTCLC,$LNTDLJ,$LNTCLJ,$LNRVCR,$LNRVDR,$LNADI1,$LNADI2,$LNADI3,$LNADI4,$LNADI5,$LNADTA,$LNADAI,$LNADLC,$LNADLJ,$LNTDPR,$LNTCPR,$LNTDIN,$LNTCIN,$LNTDTA,$LNTCTA,'$LNCDHC','$LNCDF1','$LNCDF2','$LNCDF3','$LNCDF4','$LNCDF5','$LNCDE1','$LNCDE2','$LNCDE3','$LNCDE4',$LNATYS,$LNI1YS,$LNI2YS,$LNI3YS,$LNI4YS,$LNI5YS,$LNLCYS,$LNJCYS,$LNAMBI,$LNAMB1,$LNAMB2,$LNAMB3,$LNAMB4,$LNAMB5,$LNAMBA,$LNAMBL,$LNAMBJ,$LNAMAL,$LNAMIR,$LNAMRP,$LNAMPP,$LNAMLL,$LNAMPO,'$LNCDIA',$LNAMIA,$LNAMAV,$LNPDI1,$LNPDI2,$LNPDI3,$LNPDI4,$LNPDI5,$LNPDE1,$LNPDE2,$LNPDE3,$LNPDE4,$LNPDTA,$LNPDF1,$LNPDF2,$LNPDF3,$LNPDF4,$LNPDF5,$LNPDPR,$LNPDIN,$LNAMI1,$LNAMI2,$LNAMI3,$LNAMI4,$LNAMI5,$LNAME1,$LNAME2,$LNAME3,$LNAME4,$LNAMF1,$LNAMF2,$LNAMF3,$LNAMF4,$LNAMF5,$LNE1YS,$LNE2YS,$LNE3YS,$LNE4YS,$LNF1YS,$LNF2YS,$LNF3YS,$LNF4YS,$LNF5YS,$LNG1YS,$LNG2YS,$LNG3YS,$LNG4YS,$LNG5YS,$LNTXYS,$LNOAE1,$LNOAE2,$LNOAE3,$LNOAE4,$LNOAF1,$LNOAF2,$LNOAF3,$LNOAF4,$LNOAF5,$LNOAG1,$LNOAG2,$LNOAG3,$LNOAG4,$LNOAG5,$LNOATX,$LNPDFX,$LNAMTC,$LNAMPV,$LNAMTP,$LNAMTD,$LNAMPR,$LNAMHB,$LNAPIA,$LNAMA1,$LNAMA2,$LNAMA3,$LNAMA4,$LNAOI7,$LNAI17,$LNAI27,$LNAI37,$LNAI47,$LNAI57,$LNAUI7,$LNAUD7,$LNAOD7,$LNAI18,$LNAI28,$LNAI38,$LNAI48,$LNAI58,$LNAMRC,$LNABRC,$LNAYRP,$LNAYRA,$LNAYRE,$LNAPYP,$LNAPYA,$LNAPYE,$LNAIPL,$LNAIAL,$LNAIEL,$LNYSRP,$LNATRP,$LNTDYP,$LNTCYP,$LNAMPD,$LNAMCR,$LNAMAD,$LNAMBP,$LNAMPT,$LNADRT,$LNACMT,$LNADMT,$LNALPG,$LNALIG,$LNALOG,$LNAMFX,$LNAMIN,'$LNC2RP',$LNNOH1,$LNNOH2,$LNNOH3,$LNNOIN,$LNNOMN,$LNNOPA,$LNN2PB,$LNN2P1,$LNN2P2,$LNN2P3,$LNNOR1,$LNNOR2,$LNRTTO,'$LNRCAS',$LNFXI1,$LNFXI2,$LNFXI3,$LNFXI4,$LNFXI5,$LNFXE1,$LNFXE2,$LNFXE3,$LNFXE4,$LNFXTA,$LNFXF1,$LNFXF2,$LNFXF3,$LNFXF4,$LNFXF5,$LNPD01,$LNPD02,$LNPD03,$LNPD04,$LNPD05,$LNPD06,$LNPD07,$LNPD08,$LNPD09,$LNPD10,$LNPD11,$LNPD12,$LNPD13,$LNPD14,$LNPD15,$LNPD16,$LNPD17,$LNPD18,$LNPD19,$LNPD20,'$LNAO01','$LNAO02','$LNAO03','$LNAO04','$LNAO05','$LNAO06','$LNAO07','$LNAO08','$LNAO09','$LNAO10','$LNAO11','$LNAO12','$LNAO13','$LNAO14','$LNAO15','$LNAO16','$LNAO17','$LNAO18','$LNAO19','$LNCDU1','$LNCDU2','$LNCDU3','$LNCDU4','$LNCDU5','$LNCDU6','$LNCDU7','$LNCDU8','$LNCDU9','$LNCDUA','$LNMAU1','$LNMAU2','$LNMAU3','$LNMAU4',$LNDTU1,$LNDTU2,$LNDTU3,$LNDTU4,$LNDTU5,$LNDTU6,$LNDTU7,$LNDTU8,$LNAMU1,$LNAMU2,$LNAMU3,$LNAMU4,$LNAMU5,$LNAMU6,'$LNDSU1','$LNDSU2','$LNDSU3','$LNDSU4','$LNDSU5',$LNRTU1,$LNRTU2,$LNRTU3,'$LNSTU1','$LNSTU2','$LNSTU3','$LNSTU4','$LNSTU5','$LNSTU6','$LNSTU7',$LNNOU1,$LNNOU2,$LNNOU3,$LNNOU4,'$LNOFU1','$LNOFU2',$LNBRU1,$LNBRU2,'$LNCLCD',$LNAMCL,$LNAMVA,$LNAMLD,'$LNMXCD',$LNMXAM,$LNMXPC,'$LNAAHL',$LNPPNL,$LNAPNL,'$LNCDNR',$LNDTHL,$LNAMHL,'$LNDSHL',$LNDTST,$LNDTEX,'$LNENUS','$LNENWS',$LNENTM,$LNRTPV,'$LNCDAA','$LNNKUK','$LNUSD1','$LNUSD2','$LNUSD3','$LNUSD4','$LNUSD5','$LNUSD6',$LNPDDY,$LNPDTM,$LNDTQC,'$LNNSDK','$LNNSKK',$LNDSDK,$LNDSKK,'$LNNODA','$LNCDIS',$LNAMLM,'$LNBI11','$LNBI12','$LNBI13','$LNBI14','$LNBI15','$LNBI16','$LNBI17','$LNBI18','$LNBI19','$LNBI1A','$LNBI21','$LNBI22','$LNBI23','$LNBI24','$LNBI25','$LNBI26','$LNBI27','$LNBI28','$LNBI29','$LNBI2A','$LNBI31','$LNBI32','$LNBI33','$LNBI34','$LNBI35','$LNBI36','$LNBI37','$LNBI61','$LNBI62','$LNBI63','$LNBI64','$LNBI65','$LNBI66','$LNBI67','$LNBI68',$LNBI69,'$LNBI6A',$LNBID1,$LNBID2,$LNBID3,$LNBID4,$LNBID5,$LNBIA1,$LNBIA2,$LNBIA3,$LNBIA4,$LNBIA5,'$LNBIS1','$LNBIS2','$LNBIS3','$LNCDBG',$LNDYBG,$LNNOBG,'$LNNMBG','$LNACBG',$LNISBG,'$LNRNBG','$LNCLBG',$LNDTBG,'$LNCDPG',$LNMSKJ,'$LNNMIS','$LNISID','$LNPKJR','$LNINST','$LNNIPP','$LNCGOL','$LNALKR','$LNALKT','$LNKPRL','$LNNMST','$LNCOCL','$LNJMN1','$LNJMN2','$LNJMN3','$LNJMN4','$LNJMN5','$LNJMN6','$LNJMN7','$LNJMN8','$LNJMN9','$LNJMNA','$LNJMNB','$LNJMNC','$LNJMND','$LNJMNE','$LNJMNF','$LNNOSK',$LNDTSK,'$LNBDNM','$LNNIP1','$LNADD1','$LNKDNM','$LNNIP2','$LNJBTD','$LNADD2',$LNUNIT,'$LNLUAS','$LNBLOK','$LNNOMO','$LNLOKS','$LNDEVP','$LNMERK',$LNTHBT,'$LNNOCH','$LNNOMS','$LNWARN','$LNDEAL',$LNRODA,'$LNBGTP','$LNNOPG',$LNDTPG,'$LNUGR1','$LNUGR2','$LNUGR3','$LNSGR1','$LNSGR2','$LNSGR3','$LNOGR1','$LNOGR2','$LNOGR3','$LNNOPR',$LNTGPR,'$LNNPPK',$LNNTPK,$LNBIIJ,$LNUMRK,$LNASKK,$LNPRAS,'$LNPABG','$LNASBG','$LNSPBR',$LNDTBR,'$LNDSBR',$LNAMUM,$LNPSBR,$LNAYBR,$LNSTJM,'$LNNMPN','$LNBULN',$LNAKIV,$LNAIDC,$LNRTOD,$LNSTBR,'$LNIKJM','$LNNOAK',$LNDTAK,'$LNNOSP',$LNDTSP,'$LNNBUS',$LNTBUS,'$LNNBPN',$LNTBPN,'$LNNBPM',$LNTBPM,'$LNNBPL',$LNTBPL,'$LNNTPJ',$LNTTPJ,'$LNNTTJ',$LNTTTJ,'$LNNONT',$LNTGNT,$LNDBAM,$LNCRAM,$LNDBAC,$LNCRAC,'$LNSIUP','$LNSITU',$LNINDT,$LNINTM,'$LNINUS','$LNINWS',$LNLMDT,$LNLMTM,'$LNLMUS','$LNLMWS'
)";

//echo $insert_lnspk."<br>";

$db_ibm->queryNoResult($insert_lnspk);



//header("location:drawdown_detail.php?custnomid=$custnomid");


?>
