<?
session_start();
ob_start();
require_once("sqlsrv.php");
require_once("preference.php");

ini_set( 'default_charset', 'UTF-8' );
header("Content-Type: text/html;charset=UTF-8");

$templatecode = isset($_GET['a']) ? $_GET['a'] : "";
$custnomid = isset($_GET['b']) ? $_GET['b'] : "";
$print = isset($_GET['c']) ? $_GET['c'] : "";

$templatecontent="";
$templatename="";

$strsql="select _body,_name from tr_template where _idx='".$templatecode."'";	
$rs = $condb->_RQ($strsql);
if(!is_array($rs)){echo $rs;}else
{
	$no=0;
	for($i=0; $i<count($rs);$i++) 
	{
		$templatecontent=str_replace("|||","'",$rs[$i]['_body']);
		$templatename=str_replace("|||","'",$rs[$i]['_name']);
	}
}

$year="";
$strsql="select YEAR(txn_time) as _year,* from Tbl_FSTART where txn_id='".$custnomid."'";
$result = $condb->_RQ($strsql);
if(!is_array($result)){echo $result;}else
{
	for( $i=0; $i<count($result); $i++)
	{
		$year=$result[$i]['_year'];
	}
}
?>


<!DOCTYPE html>
<html>
<head>
<title><?=$templatename?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />



<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="css/d.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css"></link>
<link rel="stylesheet" href="css/bootstrap-datepicker3.min.css"></link>



<script src="js/jquery-1.12.3.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/accounting.min.js"></script>
<script src="js/bootstrap-datepicker.min.js"></script>

<script src="js/d.js"></script>

<script>

	function onsubmitformnext()
	{
		$("#frm").attr('action', 'tmp.php');
		$("#frm").submit();
	}
	function onsubmitformprev()
	{
		$("#frm").attr('action', 'step3.php');
		$("#frm").submit();
	}
	function toprintpage() {
		
		var a = "<?=$templatecode?>";
		var b = "<?=$custnomid?>";
		var c = "<?=$print?>";
		//alert('viewtemplate.php?a='+a+'&b='+b+'&c=1');
		if(c =="")
		{
			window.location.href = 'viewtemplate.php?a='+a+'&b='+b+'&c=1';
		}
	}
	
	var d = "<?=$print?>";
	if(d!="")
	{
	window.print();
	}
	
</script>
<style>
	td{padding:5px;}
</style>
</head>
<body>
<form id="frm" name="frm" method="post" >
	<div class="container">
	<div style="text-align:left;font-size:30px;line-height:45px;border:0px solid black;"><?=$templatecode." ".$templatename?>
	<div style="text-align:right;line-height:45px;border:0px solid red;width:100px;float:right;">
		<?
		if($print =="")
		{
		?>
		<input tv="test" onclick="toprintpage()" id="printbtn"  style="border-radius:5px;border:none;background:#337AB7;color:#ffffff;line-height:20px;padding:5px 10px;" type="button" value="Print">
		<?
		}
		?>
	</div>
	</div>
	<hr>
	<?php
		$namevar = "";
		$setget2 = "";
		$value2 = "";
		$type2 = "";
		$testarray=array();
		
		$strsql="select * from tr_templatedetail where _template_code='".$templatecode."'";
		$getfullcontent="";
		$rss = $condb->_RQ($strsql);
		if(!is_array($rss)){echo $rss;}else{
		for($i=0;$i<count($rss);$i++) 
			{
				//print_r($value);
				$_namevar=$rss[$i]['_namevar'];
				$_idx=$rss[$i]['_idx'];
				array_push($testarray,$_namevar);
				$_setget=$rss[$i]['_setget'];
				$_value=$rss[$i]['_value'];
				$_type=$rss[$i]['_type'];
				$_param=$rss[$i]['_param'];
				
				
				$require_value="";
				if($_value=="Mandatory")
				{
					$require_value="required";	
				}
				
				$value_type=strtolower($_type);
				
				$value="<span style=\"color:red;\">".$_value." data tidak ada</span>";
				
				$value_form="";
				if($year!="")
				{
//					$db = 'DB_LIS_BSB';
					$tr_datatemplateyear = 'tr_datatemplate'.$year;
//					$strsql = "select * from INFORMATION_SCHEMA.TABLES where TABLE_CATALOG = '".$db."' and TABLE_NAME = '".$tr_datatemplateyear."'";
					$strsql = "select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '".$tr_datatemplateyear."'";
					$result = $condb->_RQ($strsql);
					$rowcount = $condb->_RR;
					if($rowcount>0)
					{
					$strsql="select _value from tr_datatemplate".$year."
					where _custnomid ='".$custnomid."' and _template_id='".$_idx."'";
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						$value_form = $result[0]['_value'];
					}
					
					}
				}
				
				
				
				if($_setget=="Browse to DB")
				{
					$epld=explode('.',$_value);
					$tablename=$epld[0];
					$fieldname=$epld[1];
					
					$datetime = false;
					$strsql="select DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME='".$fieldname."'";
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						if($result[0]['DATA_TYPE']== "date" || $result[0]['DATA_TYPE']== "datetime")
						{
							$datetime = true;
						}
					}
					
					$COLUMN_NAME="_name";
					$strsql="select COLUMN_NAME,DATA_TYPE from INFORMATION_SCHEMA.COLUMNS 
					where TABLE_CATALOG ='".$db."' and TABLE_NAME='".$tablename."'
					and COLUMN_NAME like '%custnomid%'";
					//echo $strsql;
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						$COLUMN_NAME = $result[0]['COLUMN_NAME'];
					}
					
					$strsql="select ".$fieldname." from ".$tablename." where ".$COLUMN_NAME."='".$custnomid."'";
					$result = $condb->_RQ($strsql);
					if(!is_array($result)){echo $result;}else
					{
						for($zz=0;$zz<count($result);$zz++)
						{
							
							$value=$result[$zz][$fieldname];
							if($_param!="")
							{
								$paramexp = explode('.',$_param);
								
								$tableparam = $paramexp[0];
								
								$fieldparam = $paramexp[1];
								$whereparam = $paramexp[2];
								
								$strsql2="select ".$fieldparam." from ".$tableparam." where ".$whereparam."='".$value."'";
								
								$result2 = $condb->_RQ($strsql2);
								if(!is_array($result2)){echo $result2;}else
								{
									for($xx=0;$xx<count($result2);$xx++)
									{
										$value=$result2[$xx][$fieldparam];
									}
								}
							}
							
							
							
							
							
							if($datetime==true)
							{
								$value=$result[$zz][$fieldname]->format('Y-m-d');
							}
							else if($_type=="Currency")
							{
								$value=number_format($result[$zz][$fieldname]);
							}
						}
					}
				}
				//$arryset = array("Textbox","Textarea");
				else
				{
					$value = $value_form;
				}
			
				$lastsentence = multiexplode($testarray,$templatecontent);
				//print_r($lastsentence);
				$getfullcontent.=$lastsentence[$i].$value;
				if($i==(count($rss)-1))
				{
					$getfullcontent.=$lastsentence[count($rss)];
				}
				//echo "<hr>";
				
			
			}
		}
		?>
		<div id="test">
		<?
		echo str_replace("|||",'"',$getfullcontent);
		?>
	</div>
	
	<input type="hidden" name="hidval" id="hidval" value="<?=str_replace('"',"|||",$_POST['hidval'])?>">
	<input type="hidden" name="tempname" id="tempname" value="<?=str_replace('"',"|||",$_POST['tempname'])?>">
	<input type="hidden" name="setget2" id="setget2" value="<?=substr($setget2,0,-1)?>">
	<input type="hidden" name="value2" id="value2" value="<?=substr($value2,0,-1)?>">
	<input type="hidden" name="type2" id="type2" value="<?=substr($type2,0,-1)?>">
	<input type="hidden" name="namevar" id="namevar" value="<?=substr($namevar,0,-1)?>">
	
</form>

</body>
</html>