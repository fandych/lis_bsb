<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
$btn=$_REQUEST['btn'];

$custnomid=$_REQUEST['custnomid'];
$strsql="SELECT 'custcurcode'= case when ISNULL(custcurcode,0)='0' then 'IDR' else custcurcode end,custbusnpwp, custproccode from tbl_customermasterperson where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$custcurcode=$rows['custcurcode'];
		$custproccode=$rows['custproccode'];
	}
}

if($custproccode=="KPM")
{
	$custkodepro = "KKB";
}else{
	$custkodepro = "KGS";
}


if ($btn=="btnadd")
{
	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$hargapenawaran=str_replace(",","",$_REQUEST['hargapenawaran']);
	$perkiraanangsuran=str_replace(",","",$_REQUEST['perkiraanangsuran']);
	$period=$_REQUEST['period'];
	$credituse=$_REQUEST['credituse'];
	$sukubunga=$_REQUEST['sukubunga'];
	$jenisbunga=$_REQUEST['jenisbunga'];
	$uangmuka=$hargapenawaran-$plafond;
	$uangmuka=$uangmuka/$hargapenawaran*100;
	$uangmuka=number_format($uangmuka,2);

	$strsql = "select cast(isnull(max(custfacseq),0)as int)+1 as seq
				from tbl_CustomerFacility where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}

	$strsql = " INSERT INTO tbl_CustomerFacility
				(custnomid,custfacseq,custcreditneed,custcredittype,custcreditplafond,custcreditlong,sukubungayangdiberikan,custcredituse,sukubunga,perkiraanangsuran,hargapenawaran,jenisbunga,uang_muka)
				VALUES
				('$custnomid','$seq','$creditneed','$codeproduct','$plafond','$period','$sukubunga','$credituse','$sukubunga','$perkiraanangsuran','$hargapenawaran','$jenisbunga','$uangmuka')
				";
				//echo $strsql;exit;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	require ("return_ajax.php");

}
else if ($btn=="btnD")
{
	$seq=$_REQUEST['seq'];
	$strsql = " delete from tbl_CustomerFacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	require ("return_ajax.php");

}
else if($btn=="btnE")
{
	$seq=$_REQUEST['seq'];
	$strsql = " select custcreditneed,custcreditplafond,custcreditlong,custcredituse,sukubunga,perkiraanangsuran,hargapenawaran,jenisbunga from tbl_customerfacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
			$custcredituse=$rows['custcredituse'];
			$sukubunga=$rows['sukubunga'];
			$perkiraanangsuran=$rows['perkiraanangsuran'];
			$hargapenawaran=$rows['hargapenawaran'];
			$jenisbunga=$rows['jenisbunga'];
		}
	}

	if ($custcreditneed=="1")
	{
		$tmp="where produk_type_description like '%$custkodepro%' and produk_type like '%1%'";
	}
	else
	{
		$tmp="where produk_type_description like '%$custkodepro%' and produk_type like '%0%'";
	}


	$strsql = "select count(*) as countrow from tbl_customerfacility where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$chcekrow=$rows['countrow'];
		}
	}
	$condition="";
	if ($chcekrow!="0")
	{
		$tmpcondition="";
		$strsql = "select custcreditneed from tbl_customerfacility where custnomid='$custnomid'";
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				$tmpcondition.="'".$rows['custcreditneed']."',";
			}
		}
		$condition="where credit_need_code not in (".substr($tmpcondition,0,-1).")";
	}
	?>
	<table class="tbl100">
		<tr>
			<td>Tujuan Pengajuan Kredit</td>
			<td>
				<select id="creditneed" name="creditneed" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
					<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
					<?
						$strsql = "select * from tbl_Creditneed ".$condition;
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{

								echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Fasilitas</td>
			<td style="width:250px;" id="dtl_creditneed">
				<select id="codeproduct" name="codeproduct" style="width:100%;" class="nonmandatory">
					<option value="">-- Pilih Jenis Fasilitas --</option>
					<?
						$strsql = "select * from tbl_kodeproduk";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								if($custcredittype==$rows['produk_loan_type'])
								{
									echo '<option value="'.$rows['produk_loan_type'].'"selected="selected">'.$rows['produk_type_description'].'</option>';
								}
								else
								{
								echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
								}
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Plafond (<?echo $custcurcode?>)</td>
			<td>
				<input type="text" id="plafond" name="plafond" class="nonmandatory" onkeyup="currency(this.id);" maxlength="15"/>
			</td>
		</tr>
		<tr>
			<td>Jangka Waktu</td>
			<td>
				<input type="text" style="width:50px" id="period" name="period" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);"/> &nbsp;&nbsp; Bulan
			</td>
		</tr>
		<tr>
			<td>Suku Bunga</td>
			<td>
				<input type="text" style="width:50px" id="sukubunga" name="sukubunga" class="nonmandatory" onkeyup="perkiraanAngsuran()" maxlength="5" /> &nbsp;&nbsp; %
			</td>
		</tr>
		<tr>
			<td>Jenis Bunga</td>
			<td>
				<select name="jenisbunga" id="jenisbunga" class="nonmandatory" onchange="perkiraanAngsuran()">
					<option value="1">Flat</option>
					<option value="2">Efektif</option>
					<option value="3">Anuitas</option>
					<option value="4">Anuitas Tahunan</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Perkiraan Angsuran</td>
			<td>
				<input type="text" style="width:100%" readonly id="perkiraanangsuran" name="perkiraanangsuran" class="nonmandatory" maxlength="30" onkeyup="currency(this.id);" onkeypress="return isNumberKey(event);"/>
			</td>
		</tr>
		<tr>
			<td>Kegunaan Kredit</td>
			<td>
				<textarea name="credituse" id="credituse" class="nonmandatory" style="width:100%;" rows="3"></textarea>
			</td>
		</tr>
		<tr>
			<td>Harga Rumah / Penawaran</td>
			<td>
				<input type="text" class="nonmandatory" style="width:100%" id="hargapenawaran" name="hargapenawaran" maxlength="30" onkeypress="return isNumberKey(event);" onkeyup="currency(this.id);"/>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" id="btnadd" name="btnadd" value="save" class="button" onclick="adddtl();"/>
			</td>
		</tr>
	</table>
	<?

	$totalpalfond=0;
	$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,a.custcreditneed,a.custcredittype,
				c.produk_type_description,a.custcreditplafond,a.custcreditlong,a.custcredituse,a.sukubunga ,a.perkiraanangsuran,a.hargapenawaran ,a.jenisbunga, a.uang_muka
				from tbl_CustomerFacility a
				left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code
				left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype
				where a.custnomid ='".$custnomid."'";

	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		echo '
			<table class="tbl100" >
			';
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcredittype=$rows['custcredittype'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
			$custcredituse=$rows['custcredituse'];
			$sukubunga=$rows['sukubunga'];
			$perkiraanangsuran=$rows['perkiraanangsuran'];
			$hargapenawaran=$rows['hargapenawaran'];
			$jenisbunga=$rows['jenisbunga'];

			if($jenisbunga == "1")
			{
				$jenisbunga = "Flat";
			}else if($jenisbunga == "2")
			{
				$jenisbunga = "Efektif";
			}else if($jenisbunga == "4")
			{
				$jenisbunga = "Anuitas Tahunan";
			}else{
				$jenisbunga = "Anuitas";
			}

			$totalpalfond+=$rows['custcreditplafond'];
			if($seq!=$rows['custfacseq'])
			{
				echo '
					<tr>
						<td style="width:180px;">Tujuan Pengajuan Kredit</td>
						<td style="width:190px;">'.$rows['credit_need_name'].'</td>
						<td rowspan="2" style="text-align:right;">&nbsp;
							<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jenis Fasilitas</td>
						<td>'.$rows['produk_type_description'].'</td>
					</tr>
					<tr>
						<td>Plafond ('.$custcurcode.')</td>
						<td>'.numberFormat($rows['custcreditplafond']).'</td>
						<td rowspan="2" style="text-align:right;">
							<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td>'.$rows['custcreditlong'].'</td>
					</tr>
					<tr>
						<td>Suku Bunga</td>
						<td>'.$rows['sukubunga'].' %</td>
					</tr>
					<tr>
						<td>Jenis Bunga</td>
						<td>'.$jenisbunga.'</td>
					</tr>
					<tr>
						<td>Perkiraan Angsuran</td>
						<td>'.numberFormat($rows['perkiraanangsuran']).'</td>
					</tr>
					<tr>
						<td>Kegunaan Kredit</td>
						<td style="max-width:190px;">'.$rows['custcredituse'].'</td>
					</tr>
					<tr>
						<td>Harga Rumah / Penawaran</td>
						<td>'.numberFormat($rows['hargapenawaran']).'</td>
					</tr>
					<tr>
						<td>Uang Muka / Self Financing</td>
						<td>'.$rows['uang_muka'].' %</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				';
			}
			else
			{
				?>
				<tr>
					<td style="width:180px;">Tujuan Pengajuan Kredit</td>
					<td style="width:190px;">
						<select id="creditneed<?echo $seq?>" name="creditneed<?echo $seq?>" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
							<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
							<?
								$strsql2 = "select * from tbl_Creditneed where credit_need_code ='".$custcreditneed."'
											union select * from tbl_Creditneed ".$condition;
								$sqlcon2 = sqlsrv_query($conn, $strsql2);
								if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon2))
								{
									while($rows = sqlsrv_fetch_array($sqlcon2, SQLSRV_FETCH_ASSOC))
									{
										if($custcreditneed==$rows['credit_need_code'])
										{
											echo '<option value="'.$rows['credit_need_code'].'" selected="selected">'.$rows['credit_need_name'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
										}
									}
								}
							?>
						</select>
					</td>
					<td rowspan="2" style="text-align:right;">&nbsp;
						<input type="button" id="btnY<?echo $seq?>" name="btnY<?echo $seq?>" class="button" value="YES" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jenis Fasilitas</td>
					<td id="dtl_creditneed<?echo $seq?>" style="width:190px;">
						<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
							<option value="">-- Pilih Jenis Fasilitas --</option>
							<?
								$strsql3 = "select * from tbl_kodeproduk $tmp";
								$sqlcon3 = sqlsrv_query($conn, $strsql3);
								if ( $sqlcon3 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon3))
								{
									while($rows = sqlsrv_fetch_array($sqlcon3, SQLSRV_FETCH_ASSOC))
									{
										if($custcredittype==$rows['produk_loan_type'])
										{
											echo '<option value="'.$rows['produk_loan_type'].'" selected="selected">'.$rows['produk_type_description'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
										}
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Plafond (<?echo $custcurcode?>)</td>
					<td>
						<input type="text" id="plafond<?echo $seq?>" name="plafond<?echo $seq?>" class="nonmandatory" onkeyup="currency(this.id);" maxlength="18" value="<? echo numberFormat($custcreditplafond)?>"/>
					</td>
					<td rowspan="2" style="text-align:right;">
						<input type="button" id="btnN<?echo $seq?>" name="btnN<?echo $seq?>" class="buttonneg" value="NO" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jangka Waktu</td>
					<td>
						<input type="text" style="width:50px" id="period<?echo $seq?>" name="period<?echo $seq?>" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);" value="<?echo $custcreditlong;?>"/> &nbsp;&nbsp; Bulan
					</td>
				</tr>
				<tr>
					<td>Suku Bunga</td>
					<td>
						<input type="text" style="width:50px" id="sukubunga<?echo $seq?>" name="sukubunga<?echo $seq?>" onkeyup="perkiraanAngsuranseq('<?php echo $seq?>')" class="nonmandatory" maxlength="5" value="<? echo numberFormat($sukubunga)?>"/> &nbsp;&nbsp; %
					</td>
				</tr>
				<tr>
					<td>Jenis Bunga</td>
					<td>
						<select name="jenisbunga<?php echo $seq?>" id="jenisbunga<?php echo $seq?>" class="nonmandatory" onchange="perkiraanAngsuranseq('<?php echo $seq?>')">
<?
							if($jenisbunga == "2")
							{
?>
							<option value="1">Flat</option>
							<option value="2" selected>Efektif</option>
							<option value="3">Anuitas</option>
							<option value="4">Anuitas Tahunan</option>
<?
							}else if($jenisbunga == "3")
							{
?>
							<option value="1">Flat</option>
							<option value="2">Efektif</option>
							<option value="3" selected>Anuitas</option>
							<option value="4">Anuitas Tahunan</option>
<?
							}else if($jenisbunga == "4")
							{
?>
							<option value="1">Flat</option>
							<option value="2">Efektif</option>
							<option value="3">Anuitas</option>
							<option value="3" selected>Anuitas Tahunan</option>
<?
							}else{
?>
							<option value="1" selected>Flat</option>
							<option value="2">Efektif</option>
							<option value="3">Anuitas</option>
							<option value="4">Anuitas Tahunan</option>
<?
							}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Perkiraan Angsuran</td>
					<td>
						<input type="text" style="width:100%" readonly id="perkiraanangsuran<?echo $seq?>" name="perkiraanangsuran<?echo $seq?>" class="nonmandatory" onkeyup="currency(this.id);" value="<? echo numberFormat($perkiraanangsuran)?>" maxlength="30" onkeypress="return isNumberKey(event);"/>
					</td>
				</tr>
				<tr>
					<td>Kegunaan Kredit</td>
					<td>
						<textarea name="credituse<?echo $seq?>" id="credituse<?echo $seq?>" class="nonmandatory" style="width:100%;" rows="3"><?echo $custcredituse;?></textarea>
					</td>
				</tr>
				<tr>
					<td>Harga Rumah / Penawaran</td>
					<td>
						<input type="text" class="nonmandatory" style="width:100%" id="hargapenawaran<?echo $seq?>" name="hargapenawaran<?echo $seq?>" maxlength="30" value="<? echo numberFormat($hargapenawaran)?>" onkeyup="currency(this.id);" onkeypress="return isNumberKey(event);"/>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<?
			}
		}
		echo '
			</table>
			<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
		';
	}

}
else if($btn=="btnY")
{
	$seq=$_REQUEST['seq'];

	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$perkiraanangsuran=str_replace(",","",$_REQUEST['perkiraanangsuran']);
	$hargapenawaran=str_replace(",","",$_REQUEST['hargapenawaran']);
	$period=$_REQUEST['period'];
	$credituse=$_REQUEST['credituse'];
	$sukubunga=$_REQUEST['sukubunga'];
	$jenisbunga=$_REQUEST['jenisbunga'];
	$uangmuka=$hargapenawaran-$plafond;
	$uangmuka=$uangmuka/$hargapenawaran*100;
	$uangmuka=number_format($uangmuka,2);

	$strsql = " update tbl_CustomerFacility set
				custcreditneed='".$creditneed."',
				custcredittype='".$codeproduct."',
				custcreditplafond='".$plafond."',
				custcreditlong='".$period."',
				sukubungayangdiberikan='".$sukubunga."',
				custcredituse='".$credituse."',
				sukubunga='".$sukubunga."',
				perkiraanangsuran='".$perkiraanangsuran."',
				hargapenawaran='".$hargapenawaran."',
				jenisbunga='".$jenisbunga."',
				uang_muka='".$uangmuka."'
				where custnomid ='".$custnomid."'
				and custfacseq ='".$seq."'
				";

	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	require ("return_ajax.php");

}
else if($btn=="btnN")
{
	require ("return_ajax.php");
}
else if($btn=="btnsave")
{
	require ("../../requirepage/parameter.php");
	$email=$_REQUEST['email'];
	$custnpwpno=$_REQUEST['custnpwpno'];
	$fullname=$_REQUEST['fullname'];
	$nickname=$_REQUEST['nickname'];
	$gender=$_REQUEST['gender'];
	$ktp=$_REQUEST['ktp'];
	$expiredktp=$_REQUEST['expiredktp'];
	$bop=$_REQUEST['bop'];
	$bod=$_REQUEST['bod'];
	$mothername=$_REQUEST['mothername'];
	$education=$_REQUEST['education'];
	$status=$_REQUEST['status'];
	$relation="";
	if ($status=="2")
	{
		$relation=$_REQUEST['relation'];
	}
	$duty=$_REQUEST['duty'];
	$address=str_replace("\n"," ",$_REQUEST['address']);
	$address=str_replace("\r"," ",$_REQUEST['address']);
	$address=str_replace("\r\n"," ",$_REQUEST['address']);
	$rt=$_REQUEST['rt'];
	$rw=$_REQUEST['rw'];
	$custprop=$_REQUEST['custprop'];
	$kec=$_REQUEST['kec'];
	$kel=$_REQUEST['kel'];
	$flag=$_REQUEST['flag'];
	$city=$_REQUEST['city'];
	$zip=$_REQUEST['zip'];
	$custbushp=$_REQUEST['custbushp'];
	$fixphone=$_REQUEST['fixphone'];
	$hp=$_REQUEST['hp'];
	$homestatus=$_REQUEST['homestatus'];
	$homeyearlong=$_REQUEST['homeyearlong'];
	$homemonthlong=$_REQUEST['homemonthlong'];
	if($flag=="1")
	{
		$ktpaddress=$address;
		$ktprt=$rt;
		$ktprw=$rw;
		$ktpkec=$kec;
		$ktpkel=$kel;
		$ktpcity=$city;
		$ktpzip=$zip;
		$custpropktp =$custprop;
	}
	else
	{
		$ktpaddress=$_REQUEST['ktpaddress'];
		$ktprt=$_REQUEST['ktprt'];
		$ktprw=$_REQUEST['ktprw'];
		$ktpkec=$_REQUEST['ktpkec'];
		$ktpkel=$_REQUEST['ktpkel'];
		$ktpcity=$_REQUEST['ktpcity'];
		$ktpzip=$_REQUEST['ktpzip'];
		$custpropktp=$_REQUEST['custpropktp'];
	}

	$bussname=$_REQUEST['bussname'];

	$bussaddress=str_replace("\n"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r\n"," ",$_REQUEST['bussaddress']);
	$busspfixphone=$_REQUEST['busspfixphone'];
	$bussnpwp=$_REQUEST['bussnpwp'];
	$busssiup=$_REQUEST['siup'];
	$busstdp=$_REQUEST['busstdp'];
	$custcreditstatus=$_REQUEST['custcreditstatus'];
	$lifeinsurance=$_REQUEST['lifeinsurance'];
	$otherlifeinsurance="";
	if($lifeinsurance=="JKC99")
	{
		$otherlifeinsurance=$_REQUEST['otherlifeinsurance'];
	}
	$lostinsurance=$_REQUEST['lostinsurance'];
	$otherlostinsurance="";
	if($lostinsurance=="JKC99")
	{
		$otherlostinsurance=$_REQUEST['otherlostinsurance'];
	}
	$kreditinsurance=$_REQUEST['kreditinsurance'];
	$otherkreditinsurance="";
	if($kreditinsurance=="JKC99")
	{
		$otherkreditinsurance=$_REQUEST['otherkreditinsurance'];
	}
	$introduce=$_REQUEST['introduce'];
	$lkcddate=$_REQUEST['lkcddate'];
	$recomen=$_REQUEST['recomen'];


	//lkcd
	$usaha_jenisusaha=$_REQUEST['usaha_jenisusaha'];
	$usaha_lamausahamonth=$_REQUEST['usaha_lamausahamonth'];
	$usaha_lamausahayear=$_REQUEST['usaha_lamausahayear'];
	
	$_status_pekerjaan = "";
	$_subsidi_pemerintah = "";
	$_fotocopy_spt = "";
	$_jenis_pembelian = "";
	$_nama_mo = "";
	
	$_status_pekerjaan=$_REQUEST['_status_pekerjaan'];
	$_subsidi_pemerintah=$_REQUEST['_subsidi_pemerintah'];
	$_fotocopy_spt=$_REQUEST['_fotocopy_spt'];
	$_jenis_pembelian=$_REQUEST['_jenis_pembelian'];
	$_nama_mo=$_REQUEST['nama_mo'];

	$strsql = "
				UPDATE Tbl_CustomerMasterPerson
				SET custfullname = '$fullname'
				,custnpwpno = '$custnpwpno'
				,custshortname = '$nickname'
				,custsex = '$gender'
				,custktpno = '$ktp'
				,custktpexp = '$expiredktp'
				,email = '$email'
				,custboddate = '$bod'
				,custbodplace = '$bop'
				,custmothername = '$mothername'
				,custeducode = '$education'
				,custmarcode = '$status'
				,custmarname = '$relation'
				,custbushp = '$custbushp'
				,custjmltanggungan = '$duty'
				,custaddr = '$address'
				,custrt = '$rt'
				,custrw = '$rw'
				,custkel = '$kel'
				,custkec = '$kec'
				,custcity = '$city'
				,custzipcode = '$zip'
				,custtelp = '$fixphone'
				,custhp = '$hp'
				,custhomestatus = '$homestatus'
				,custhomeyearlong = '$homeyearlong'
				,custhomemonthlong = '$homemonthlong'
				,custaddrktp = '$ktpaddress'
				,custrtktp = '$ktprt'
				,custrwktp = '$ktprw'
				,custkelktp = '$ktpkel'
				,custkecktp = '$ktpkec'
				,custcityktp = '$ktpcity'
				,custzipcodektp = '$ktpzip'
				,custbusname = '$bussname'
				,custbusaddr = '$bussaddress'
				,custbustelp = '$busspfixphone'
				,custbusnpwp = '$bussnpwp'
				,custbussiup = '$busssiup'
				,custbustdp = '$busstdp'
				,custapldate = '$lkcddate'
				,custnomfrom = 'PC'
				,custnomperkenalan = '$introduce'
				,custpropendapatan = '$recomen'
				,custprop = '$custprop'
				,custpropktp = '$custpropktp'
				where custnomid='".$custnomid."'

				UPDATE Tbl_LKCDUsaha
				SET
				usaha_jenisusaha='$usaha_jenisusaha',
				usaha_lamausahamonth='$usaha_lamausahamonth',
				usaha_lamausahayear='$usaha_lamausahayear'
				where custnomid='".$custnomid."'

				 ";

	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

    $vargabung = 	$_POST['_txtmulaiusaha'] . "~" . $_POST['_sistempenggajian'];


    $strsql="delete from tbl_KPR_infokerja where _custnomid='".$custnomid."'
                INSERT INTO tbl_KPR_infokerja
           (_custnomid
           ,_pekerjaan
           ,_jenispekerjaan
           ,_jabatan
           ,_alamat
           ,_lamabekerja
           ,_penghasilan
           ,_sistempenggajian
           ,_namaperusahan
           ,_jenisperushaan
           ,_linebisnis
           ,_noteleponperusahaan
           ,_lamaberdiriperusahaan
           ,_fax
           ,_kemampuanmembayar
           ,_persentase
           ,_pembayarangaji)
     VALUES
           ('".$custnomid."',
            '".$_POST['_pekerjaan']."',
            '".$_POST['_jenispekerjaan']."',
            '".$_POST['_jabatan']."',
            '".$_POST['_alamat']."',
            '".$_POST['_lamabekerja']."',
            '".$_POST['_penghasilan']."',
            '".$vargabung."',
            '".$_POST['_namaperusahan']."',
            '".$_POST['_jenisperushaan']."',
            '".$_POST['_linebisnis']."',
            '".$_POST['_noteleponperusahaan']."',
            '".$_POST['_lamaberdiriperusahaan']."',
            '".$_POST['_fax']."',
            '".$_POST['_kemampuanmembayar']."',
            '".$_POST['_persentase']."',
            '".$_POST['_pembayarangaji']."')







            delete from tbl_KPR_infokerja2 where _custnomid='".$custnomid."'
                INSERT INTO tbl_KPR_infokerja2
           (_custnomid
           ,_pekerjaan
           ,_jenispekerjaan
           ,_jabatan
           ,_alamat
           ,_lamabekerja
           ,_penghasilan
           ,_sistempenggajian
           ,_namaperusahan
           ,_jenisperushaan
           ,_linebisnis
           ,_noteleponperusahaan
           ,_lamaberdiriperusahaan
           ,_fax
           ,_kemampuanmembayar
           ,_persentase
           ,_pembayarangaji)
     VALUES
           ('".$custnomid."',
            '".$_POST['_pekerjaan']."',
            '".$_POST['_jenispekerjaan']."',
            '".$_POST['_jabatan']."',
            '".$_POST['_alamat']."',
            '".$_POST['_lamabekerja']."',
            '".$_POST['_penghasilan']."',
            '".$vargabung."',
            '".$_POST['_namaperusahan']."',
            '".$_POST['_jenisperushaan']."',
            '".$_POST['_linebisnis']."',
            '".$_POST['_noteleponperusahaan']."',
            '".$_POST['_lamaberdiriperusahaan']."',
            '".$_POST['_fax']."',
            '".$_POST['_kemampuanmembayar']."',
            '".$_POST['_persentase']."',
            '".$_POST['_pembayarangaji']."')


			 delete from tbl_info_referensi where custnomid='".$custnomid."'
                INSERT INTO tbl_info_referensi
           (custnomid
           ,nama
           ,alamat
           ,kodepos
           ,telp
           ,nohp
           ,pekerjaan)
     VALUES
           ('".$custnomid."',
            '".$_POST['txtnama']."',
            '".$_POST['txtalamat']."',
            '".$_POST['txtkodepos']."',
            '".$_POST['txttlp']."',
            '".$_POST['txthp']."',
            '".$_POST['txtpekerjaanref']."')


            delete from survey_task where _custnomid='".$custnomid."'
            INSERT INTO survey_task
           ([_custnomid]
           ,[_custname]
           ,[_custaddress]
           ,[_officercode]
           ,[_flag])
     VALUES
           ('".$custnomid."','".$fullname."','".$address."','indrasan','0')


					 delete from Tbl_Scoring where custnomid='".$custnomid."'
					 INSERT INTO Tbl_Scoring
					([custnomid]
					,[status_pekerjaan]
					,[jenis_pembelian]
					,[subsidi_pemerintah]
					,[fotokopi_spt]
					,[nama_mo])
		VALUES
					('".$custnomid."','".$_status_pekerjaan."','".$_jenis_pembelian."','".$_subsidi_pemerintah."','".$_fotocopy_spt."','".$_nama_mo."')

            ";


	//echo $strsql;
    $params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
















	$apenghasilan = $_POST['txtpenghasilan'];
	if($apenghasilan=="")
	{
		$apenghasilan="0";
	}else{
		$apenghasilan = str_replace(',', '', $apenghasilan);
	}

	$amasaberlaku = $_POST['txtmasaberlaku']==""? "NULL" : "'".$_POST['txtmasaberlaku']."'";
	$atgllahir = $_POST['txttgllahir']==""? "NULL" : "'".$_POST['txttgllahir']."'";
	$amulaikerja = $_POST['txtmulaikerja']==""? "NULL" : "'".$_POST['txtmulaikerja']."'";


	$anmdepan = $_POST['txtnamadepan'];
	$anmtengah = $_POST['txtnamatengah'];
	$anmbelakang = $_POST['txtnamabelakang'];
	$atmptlahir = $_POST['txttmptlahir'];
	$anoktp = $_POST['txtnoktp'];
	$apekerjaan = $_POST['txtpekerjaan'];
	$anmperusahaan = $_POST['txtnmperusahaan'];
	$atlpperusahaan = $_POST['txttlpperusahaan'];



	$strsql = "
    DELETE FROM pl_custpasangan WHERE _custnomid = '$custnomid'
    INSERT INTO pl_custpasangan (_custnomid,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang,_pasangan_tempatlahir,_pasangan_tgllahir,_pasangan_nomorktp,_pasangan_expiredktp,_pasangan_pekerjaan_id,_pasangan_mulaibekerjan,_pasangan_namaperusahaan,_pasangan_telpperusahaan,_pasangan_penghasilanbersih,_pasangan_flag)
    VALUES ('$custnomid','$anmdepan','$anmtengah','$anmbelakang','$atmptlahir',$atgllahir,'$anoktp',$amasaberlaku,'$apekerjaan',$amulaikerja,'$anmperusahaan','$atlpperusahaan','$apenghasilan','')

    DELETE FROM pl_custpasangan2 WHERE _custnomid = '$custnomid'
    INSERT INTO pl_custpasangan2 (_custnomid,_pasangan_namadepan,_pasangan_namatengah,_pasangan_namabelakang,_pasangan_tempatlahir,_pasangan_tgllahir,_pasangan_nomorktp,_pasangan_expiredktp,_pasangan_pekerjaan_id,_pasangan_mulaibekerjan,_pasangan_namaperusahaan,_pasangan_telpperusahaan,_pasangan_penghasilanbersih,_pasangan_flag)
    VALUES ('$custnomid','$anmdepan','$anmtengah','$anmbelakang','$atmptlahir',$atgllahir,'$anoktp',$amasaberlaku,'$apekerjaan',$amulaikerja,'$anmperusahaan','$atlpperusahaan','$apenghasilan','')
                                    ";
	//echo $strsql;
	$stmt = sqlsrv_prepare( $conn, $strsql);
	if(!$stmt)
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	if(!sqlsrv_execute( $stmt))
	{
	echo "Cannot insert table ". $strsql;
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);












    /*
    echo $strsql;
				 echo "</br>";
				 echo "</br>";
				 echo "</br>";
    */
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$checkrow=$rows['b'];
		}
	}




	$asd = $_POST['_pekerjaan'];
	$occupation="";
	$strsql = "select * from param_pekerjaan where code = '$asd'";
	echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$occupation = $rows['attribute'];
		}
	}

	echo $occupation;

	$apenghasilan = $_POST['txtpenghasilan'];
	if($apenghasilan=="")
	{
		$apenghasilan="0";
	}else{
		$apenghasilan = str_replace(',', '', $apenghasilan);
	}

	$amasaberlaku = $_POST['txtmasaberlaku']==""? "NULL" : "'".$_POST['txtmasaberlaku']."'";
	$atgllahir = $_POST['txttgllahir']==""? "NULL" : $_POST['txttgllahir'];
	$amulaikerja = $_POST['txtmulaikerja']==""? "NULL" : "'".$_POST['txtmulaikerja']."'";


	$anmdepan = $_POST['txtnamadepan'];
	$anmtengah = $_POST['txtnamatengah'];
	$anmbelakang = $_POST['txtnamabelakang'];
	$atmptlahir = $_POST['txttmptlahir'];
	$anoktp = $_POST['txtnoktp'];
	$apekerjaan = $_POST['txtpekerjaan'];
	$anmperusahaan = $_POST['txtnmperusahaan'];
	$atlpperusahaan = $_POST['txttlpperusahaan'];




	$strsql = "select * from param_pekerjaan where code = '$apekerjaan'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$apekerjaan = $rows['attribute'];
		}
	}
	$strsqlscupdate2="";

	//echo "asdasd s".$status;
	$pasangangender="1";
	if($gender=="1")
	{
		$pasangangender="2";
	}



	$alamatkantor  =$_POST['_alamat'];

	if($status=="2")
	{

		 $strsqlscupdate2="

			INSERT INTO [dbo].[Tbl_CustomerSibling]
				   ([custnomid]
				   ,[entryseq]
				   ,[orderseq]
				   ,[ktp]
				   ,[name]
				   ,[bodplace]
				   ,[bod]
				   ,[address]
				   ,[gender]
				   ,[relation]
				   ,[phoneno]
				   ,[occupation]
				   ,[npwp]
				   ,[zipcode]
				   ,[workaddress]
				   ,[flagdelete]
				   ,[flagorder])
			 VALUES
				   ('$custnomid',
				   '02',
				   '00',
				   '$anoktp',
				   '$anmdepan',
				   '$atmptlahir',
				   '$atgllahir',
				   '$address',
				   '$pasangangender',
				   '01',
				   '',
				   '$apekerjaan',
				   '',
				   '$ktpzip',
				   '',
				   '0',
				   '0')
				   ";

	}


	$strsqlscupdate="

	delete from Tbl_CustomerSibling where custnomid = '$custnomid'
    INSERT INTO [dbo].[Tbl_CustomerSibling]
           ([custnomid]
           ,[entryseq]
           ,[orderseq]
           ,[ktp]
           ,[name]
           ,[bodplace]
           ,[bod]
           ,[address]
           ,[gender]
           ,[relation]
           ,[phoneno]
           ,[occupation]
           ,[npwp]
           ,[zipcode]
           ,[workaddress]
           ,[flagdelete]
           ,[flagorder])
     VALUES
           ('$custnomid',
		   '01',
		   '00',
		   '$ktp',
		   '$fullname',
		   '$bop',
		   '$bod',
		   '$address',
		   '$gender',
		   '00',
		   '$hp',
		   '$occupation',
		   '$custnpwpno',
		   '$ktpzip',
		   '$alamatkantor',
		   '0',
		   '0')



		   ".$strsqlscupdate2."";
		   //echo "<br/>".$strsqlscupdate."<br/><br/><br/>";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsqlscupdate, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);








	///scoring
	///scoring
	///scoring
    $control_value="";
    $strsql = "select control_value from ms_control where control_code='MASTERLOAN'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$control_value=$rows['control_value'];
		}
	}

    $custproccode="";
    $strsql = "select custproccode from Tbl_CustomerMasterPerson where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custproccode=$rows['custproccode'];
		}
	}

    /*
	$strsql2="select * from DW_SourceHead where sh_id='".$custnomid."'";
	echo $strsql2;
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_REQUEST['query']);
	$sqlcon2 = sqlsrv_query($conn, $strsql2, $params, $cursorType);
	if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
	$rowCounter2 = sqlsrv_num_rows($sqlcon2);
	if($rowCounter2==0)
	{
		$strsqlsc=
		"
		INSERT INTO [DW_SourceHead]
           ([sh_id]
           ,[sh_pm_group]
           ,[sh_entry_time]
           ,[sh_process_time]
           ,[sh_flag])
		VALUES
           ('".$custnomid."',
           'PLATINUM',
           GETDATE(),
           GETDATE(),
           'Y')

		INSERT INTO DW_SourceDetail([sd_id],[sd_field],[sd_pm_id],[sd_value],[sd_value_char],[sd_value_num],[sd_score],[sd_desc])
		SELECT '".$custnomid."',pmh_id,pmh_name,'C','','0','0',''
		FROM DW_ParamMatrixHead

		";

		echo $strsqlsc."</br>";
		$params = array(&$_POST['query']);
		$stmt = sqlsrv_prepare( $conn, $strsqlsc, $params);
		if( !$stmt )
		{
		echo "Error in preparing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}

		if( !sqlsrv_execute( $stmt))
		{
		echo "Error in executing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt);
	}

	*/

	$paramumur='A1';
	$strsqlaa = "SELECT * from param_age where range_start<=".$bod." and range_end>=".$bod."";


	$r_aa = sqlsrv_query($conn, $strsqlaa);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$paramumur = $row2['code'];
		}
	}

	echo "</br>".$gender."</br>";
	$DP_gender=0;
	if($gender==2)
	{
	$DP_gender=1;
	}

	$DP_marital_status='MS04';
	if($status==1){$DP_marital_status="MS02";}
	if($status==2){$DP_marital_status="MS01";}
	if($status==3){$DP_marital_status="MS03";}


	$DP_jml_tanggungan="";
	$strsqlaa = "select top 1 * from param_tanggungan where cast(attribute as int)<=".$duty." order by cast(attribute as int) desc";

	//echo $strsqlaa;
	$r_aa = sqlsrv_query($conn, $strsqlaa);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$DP_jml_tanggungan = $row2['code'];
		}
	}

	if($education=="1" || $education=="2"){$DP_educational_status="E0";}
	if($education=="3"){$DP_educational_status="E1";}
	if($education=="4" || $education=="5" || $education=="6"){$DP_educational_status="E3";}
	if($education=="0"){$DP_educational_status="E4";}




	$DP_statusRumah="H3";
	if($homestatus=="P001"){$DP_statusRumah='H0';}
	if($homestatus=="P002"){$DP_statusRumah='H1';}
	if($homestatus=="B003"){$DP_statusRumah='H2';}



	$lamatinggalcoi=$homeyearlong.'.'.$homemonthlong;
	$DP_lamastatusrumah='L4';
	$strsql="select * from param_lama_tinggal where range_start<$lamatinggalcoi and range_end>=$lamatinggalcoi";

	$r_aa = sqlsrv_query($conn, $strsql);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$DP_lamastatusrumah = $row2['code'];
		}
	}


	//echo $DP_lamastatusrumah."asdasdas";
	$emailcheck="E2";
	if($email!="")
	{
	$emailcheck="E1";
	}


	$checkexistingkartukredit='K2';
	$RDB_KK_anggota1='LK0';
	$DPP_job='PK03';
	$DPP_jabatan='JB01';
	$DPP_bidang_usaha='BU01';
	$DPP_status_karyawan='SP04';
	$DPP_jml_pegawai='JP01';
	$DPP_lamabekerja='LB01';
	$DPP_penghasilan='SL10';



    /*
	$strsqlscupdate="
	INSERT INTO DW_SourceHeadNew VALUES('".$control_value."','".$custproccode."','".$custnomid."',GETDATE(),'','0','N');
	UPDATE DW_SourceDetail set sd_value_char='$paramumur' where sd_pm_id='Age'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_gender' where sd_pm_id='Gender / Sex'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_marital_status' where sd_pm_id='Marital Status' and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_jml_tanggungan' where sd_pm_id='Tanggungan' and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_educational_status' where sd_pm_id='Education'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_statusRumah' where sd_pm_id='House'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_lamastatusrumah' where sd_pm_id='Lama menempati rumah'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='P01' where sd_pm_id='Phone'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$emailcheck' where sd_pm_id='Email addres'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$checkexistingkartukredit' where sd_pm_id='Memiliki Kartu Kredit Bank Lain'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$RDB_KK_anggota1' where sd_pm_id='Lama keanggotaan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_job' where sd_pm_id='Perkerjaan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_jabatan' where sd_pm_id='Jabatan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_bidang_usaha' where sd_pm_id='Bidang Usaha'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_status_karyawan' where sd_pm_id='Status Kepegawaian'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_jml_pegawai' where sd_pm_id='Jumlah Pegawai'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='$DPP_lamabekerja' where sd_pm_id='Lama Bekerja'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_penghasilan' where sd_pm_id='Salary'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='sid' where sd_pm_id='SID 'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='rating' where sd_pm_id='Rating'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='akki' where sd_pm_id='AKKI (NUMBER OF NEGATIVE AKKI RECORDS)'and sd_id='$custnomid'


	UPDATE DW_SourceDetail
	SET sd_score=b.pm_score
	from
	DW_SourceDetail  a
	join  DW_ParamMatrix b
	on b.pm_attribute = a.sd_value_char
	and b.pm_id = a.sd_field
	where b.pm_group ='PLATINUM'
	and a.sd_id='$custnomid'



	";
    */
	//echo "</br>".$strsqlscupdate;
    $strsqlscupdate="
    delete from DW_SourceHeadNew where sh_nom_id ='".$custnomid."'
    INSERT INTO DW_SourceHeadNew VALUES('".$control_value."','".$custproccode."','".$custnomid."',GETDATE(),'','0','N','');";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsqlscupdate, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);



	//////////end scoring
	//////////end scoring
	//////////end scoring
	//////////end scoring
	//////////end scoring
















		$strsql="
         delete from Tbl_Customerinsurance  where custnomid='$custnomid'
        INSERT INTO Tbl_Customerinsurance
				(custnomid,code_life,other_life,code_loss,other_loss,code_credit,other_credit)
				values
				('$custnomid','$lifeinsurance','$otherlifeinsurance','$lostinsurance','$otherlostinsurance','$kreditinsurance','$otherkreditinsurance')";

    echo "</br>";
    echo "</br>";
    echo "</br>";
    echo "</br>";
	//echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);

	require ("../../requirepage/do_saveflow.php");
	require ("insert_review.php");

	header("location:../flow.php?userwfid=$userwfid");
}

else if ($btn=="changekp")
{
	$seq=$_REQUEST['seq'];
	$creditneed =$_REQUEST['creditneed'];
	//echo $custkodepro;
	if ($creditneed=="1")
	{
		$tmp="where produk_type_description like '%$custkodepro%' and produk_type like '%1%'";
	}
	else
	{
		$tmp="where produk_type_description like '%$custkodepro%' and produk_type like '%0%'";
	}

	//echo $strsql = "select * from Tbl_KodeProduk $tmp ";
	?>
	<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
		<option value="">-- Pilih Jenis Fasilitas --</option>
		<?
			$strsql = "select * from Tbl_KodeProduk $tmp ";
			$sqlcon = sqlsrv_query($conn, $strsql);
			if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($sqlcon))
			{
				while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
				{
					echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
				}
			}
		?>
	</select>

	<?
}


?>
