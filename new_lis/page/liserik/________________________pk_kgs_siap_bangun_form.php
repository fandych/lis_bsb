<?php

require('./sqlsrv.php');

date_default_timezone_set('Asia/jakarta');

$arrayhari =array('Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
$tahun = date('Y');
$bulan = date('m');
$tanggal = date('d');
$hari = $arrayhari[date('w')];

$format='Y-m-d';



$custnomid= isset($_GET['custnomid']) ? $_GET['custnomid'] : "";


$custcreditplafond="";
$custcreditlong="";
$strsql="select custcreditplafond,custcreditlong  from tbl_CustomerFacility where custnomid='".$custnomid."' ";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custcreditplafond= $rs[$i]['custcreditplafond'];
  $custcreditlong= $rs[$i]['custcreditlong'];
}


$custktpno="";
$custaddr="";
$custfullname="";
$strsql="select custktpno,custaddr,custfullname  from Tbl_CustomerMasterPerson where custnomid='".$custnomid."' ";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{
  $custktpno= $rs[$i]['custktpno'];
  $custaddr= $rs[$i]['custaddr'];
  $custfullname= $rs[$i]['custfullname'];
}


$nomorpk_i= "";
$nomorpk_ii= "";
$tanggalpk= "";
$namapemimpincabang_i= "";
$tempatpemimpincabang_i= "";
$alamatpemimpincabang_i= "";
$nomorakta_i= "";
$tanggalakta_i= "";
$namadirektur_ii= "";
$direktur_ii= "";
$pt_ii= "";
$nomorktp_ii= "";
$nomorakta_ii= "";
$tanggalakta_ii= "";
$namanotaris_ii= "";
$tempatnotaris_ii= "";
$dstnotaris_ii= "";
$p2_perumahan= "";
$p2_lokasi= "";
$p11_tempattandatangan= "";
$p11_pihak_i= "";
$p11_pihak_ii= "";

$strsql = "select * from PK_KGS_SIAP_BANGUN where CUSTNOMID='".$custnomid."'";
$rs = $db->_RQ($strsql);
for($i=0;$i<count($rs);$i++)
{

$nomorpk_i= $rs[$i]['NOMORPK_I'];
$nomorpk_ii= $rs[$i]['NOMORPK_II'];
$namapemimpincabang_i= $rs[$i]['NAMAPEMIMPINCABANG_I'];
$tempatpemimpincabang_i= $rs[$i]['TEMPATPEMIMPINCABANG_I'];
$alamatpemimpincabang_i= $rs[$i]['ALAMATPEMIMPINCABANG_I'];
$nomorakta_i= $rs[$i]['NOMORAKTA_I'];
$tanggalakta_i= $rs[$i]['TANGGALAKTA_I']->format($format);
$namadirektur_ii= $rs[$i]['NAMADIREKTUR_II'];
$direktur_ii= $rs[$i]['DIREKTUR_II'];
$pt_ii= $rs[$i]['PT_II'];
$nomorktp_ii= $rs[$i]['NOMORKTP_II'];
$nomorakta_ii= $rs[$i]['NOMORAKTA_II'];
$tanggalakta_ii= $rs[$i]['TANGGALAKTA_II']->format($format);
$namanotaris_ii= $rs[$i]['NAMANOTARIS_II'];
$tempatnotaris_ii= $rs[$i]['TEMPATNOTARIS_II'];
$dstnotaris_ii= $rs[$i]['DSTNOTARIS_II'];
$p2_perumahan= $rs[$i]['P2_PERUMAHAN'];
$p2_lokasi= $rs[$i]['P2_LOKASI'];
$p11_tempattandatangan= $rs[$i]['P11_TEMPATTANDATANGAN'];
$p11_pihak_i= $rs[$i]['P11_PIHAK_I'];
$p11_pihak_ii= $rs[$i]['P11_PIHAK_II'];
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Perjanjian Kredit</title>

    <script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
    <script type="text/javascript" src="../../js/full_function.js" ></script>
    <script type="text/javascript" src="../../js/accounting.js" ></script>

    <script type="text/javascript">
    function namapt(thisid)
    {
      $('.namapt').html($('#'+thisid).val());
    }

    function lokasi(thisid)
    {
      $('.lokasi').html($('#'+thisid).val());
    }


    function submitform()
    {
      if($('#nomorpk_i').val()=="")
      {
        $('#nomorpk_i').focus();
        alert('Nomor Perjanjian Kredit 1Harus Diisi');
      }
      else if($('#nomorpk_ii').val()=="")
      {
        $('#nomorpk_ii').focus();
        alert('Nomor Perjanjian Kredit 1 Harus Diisi');
      }
      else if($('#namapemimpincabang_i').val()=="")
      {
        $('#namapemimpincabang_i').focus();
        alert('Nama Pemimpin Cabang Harus Diisi');
      }
      else if($('#tempatpemimpincabang_i').val()=="")
      {
        $('#tempatpemimpincabang_i').focus();
        alert('Tempat Cabang Harus Diisi');
      }
      else if($('#alamatpemimpincabang_i').val()=="")
      {
        $('#alamatpemimpincabang_i').focus();
        alert('Alamat Cabang Harus Diisi');
      }
      else if($('#nomorakta_i').val()=="")
      {
        $('#nomorakta_i').focus();
        alert('Nomor Akta Harus Diisi');
      }
      else if($('#tanggalakta_i').val()=="")
      {
        $('#tanggalakta_i').focus();
        alert('Tangal Akta Harus Diisi');
      }
      else if($('#namadirektur_ii').val()=="")
      {
        $('#namadirektur_ii').focus();
        alert('Nama Debitur Harus Diisi');
      }
      else if($('#direktur_ii').val()=="")
      {
        $('#direktur_ii').focus();
        alert('Direktur Harus Diisi');
      }
      else if($('#pt_ii').val()=="")
      {
        $('#pt_ii').focus();
        alert('PT Harus Diisi');
      }
      else if($('#nomorktp_ii').val()=="")
      {
        $('#nomorktp_ii').focus();
        alert('Nomor Ktp Harus Diisi');
      }
      else if($('#nomorakta_ii').val()=="")
      {
        $('#nomorakta_ii').focus();
        alert('Nomor Akta Harus Diisi');
      }
      else if($('#tanggalakta_ii').val()=="")
      {
        $('#tanggalakta_ii').focus();
        alert('Tanggal Akta Harus Diisi');
      }
      else if($('#namanotaris_ii').val()=="")
      {
        $('#namanotaris_ii').focus();
        alert('Nama Notaris Harus Diisi');
      }
      else if($('#tempatnotaris_ii').val()=="")
      {
        $('#tempatnotaris_ii').focus();
        alert('Tempat Notaris Harus Diisi');
      }
      else if($('#dstnotaris_ii').val()=="")
      {
        $('#dstnotaris_ii').focus();
        alert('DST Harus Diisi');
      }
      else if($('#p2_perumahan').val()=="")
      {
        $('#p2_perumahan').focus();
        alert('Perumahan Harus Diisi');
      }
      else if($('#p2_lokasi').val()=="")
      {
        $('#p2_lokasi').focus();
        alert('Lokasi Harus Diisi');
      }
      else if($('#p11_tempattandatangan').val()=="")
      {
        $('#p11_tempattandatangan').focus();
        alert('Tempat Tanda Tangan Harus Diisi');
      }
      else if($('#p11_pihak_i').val()=="")
      {
        $('#p11_pihak_i').focus();
        alert('Pihak 1 Harus Diisi');
      }
      else if($('#p11_pihak_ii').val()=="")
      {
        $('#p11_pihak_ii').focus();
        alert('Pihak 2 Harus Diisi');
      }
      else
      {
        $('#frm').submit();
      }
    }
    </script>

    <style media="screen">
      ol li {padding: 5px 0px;}
      .pihak {text-align:center;font-weight: bold;}
      #tbl_padding tr td{ padding:2px 5px;}
      #tableform{margin-top:50px; border:0px solid black;width: 900px;}
    </style>
  </head>
  <body>


    <form id="frm" action="pk_kgs_siap_bangun_exec.php" method="post">
      <table id="tableform" border="0" align="center">
        <tr>
          <td>
            <p style="text-align:center;">
              <b>
                PERJANJIAN KERJASAMA<br/><br/>
                ANTARA<br/>
                PT. BANK PEMBANGUNAN DAERAH SUMATERA SELATAN DAN BANGKA BELITUNG DENGAN<br/>
                <?php echo $custfullname; ?><br/>
                TENTANG<br/>
                FASILITAS KREDIT KGS SIAP BANGUN
              </b>
            </p>
            <hr/><hr/>
            <table align="center">
              <tr>
                <td>Nomor</td>
                <td>:</td>
                <td>
                  <input type="text" name="nomorpk_i" id="nomorpk_i" maxlegnth="100" value="<? echo $nomorpk_i; ?>" >
                </td>
              </tr>
              <tr>
                <td>Nomor</td>
                <td>:</td>
                <td>
                  <input type="text" name="nomorpk_ii" id="nomorpk_ii" maxlegnth="100" value="<? echo $nomorpk_ii; ?>" >
                </td>
              </tr>
            </table>

            <p>Pada hari ini <?php echo $hari; ?> tanggal <?php echo $tanggal; ?> bulan <?php echo $bulan; ?> tahun <?php echo $tahun; ?>, kami yang bertanda tangan di bawah ini : </p>

            <ol style="text-align: justify">
              <li type="I">
                  <input type="text" name="namapemimpincabang_i" id="namapemimpincabang_i" maxlegnth="100" value="<? echo $namapemimpincabang_i; ?>" >
                  , Wakil Pemimpin Cabang, PT.  Bank  Pembangunan  Daerah  Sumatera  Selatan  dan   Bangka   Belitung,  berkedudukan   di
                  <input type="text" name="tempatpemimpincabang_i" id="tempatpemimpincabang_i" maxlegnth="100" value="<? echo $tempatpemimpincabang_i; ?>" >
                  dan  beralamat di jalan
                  <input type="text" name="alamatpemimpincabang_i" id="alamatpemimpincabang_i" maxlegnth="200" value="<? echo $alamatpemimpincabang_i; ?>" >
                  dalam hal ini sah bertindak dalam  jabatannya  tersebut  berdasarkan  Akta  Kuasa Direksi Nomor.
                  <input type="text" name="nomorakta_i" id="nomorakta_i" maxlegnth="100" value="<? echo $nomorakta_i; ?>" >
                  tanggal
                  <input type="text" name="tanggalakta_i" id="tanggalakta_i" readonly="readonly" onFocus="NewCssCal(this.id,'YYYYMMDD');" value="<? echo $tanggalakta_i; ?>" >
                  dari  dan  selaku  demikian  sah  bertindak  untuk  dan  atas nama  PT. Bank Pembangunan Daerah Sumatera Selatan dan Bangka Belitung  yang berkedudukan dan berkantor pusat di Palembang Jalan Gubernur H. Ahmad Bastari No. 07 Jakabaring, yang didirikan berdasarkan Akta Pendirian No. 20 tanggal 25 November 2000 yang dibuat oleh Dr. Justin Aritonang, Sarjana Hukum, Notaris  di Palembang  dan diumumkan dalam Berita  Negara Republik Indonesia  Nomor 938  tahun 2001,  Tambahan  Berita  Negara RI  tanggal  9 Februari 2001 Nomor 12, berikut dengan perubahan-perubahannya terakhir berdasarkan Akta Nomor 75  tanggal  08 Desember 2010 yang dibuat dihadapan Ny. Elmadiantini, SH.SpN  Notaris  di Palembang  dan telah mendapatkan pengesahan dari  Menteri Hukum  dan  Hak Asasi Manusia  RI No. AHU-0004234.AH.01.09 tahun 2011 tanggal 18 Januari 2011, selanjutnya disebut :
                  <p class="pihak">PIHAK PERTAMA</p>
              </li>
              <li>
                  <input type="text" name="namadirektur_ii" id="namadirektur_ii" maxlegnth="100" value="<? echo $namadirektur_ii; ?>" >
                  selaku Direktur
                  <input type="text" name="direktur_ii" id="direktur_ii" maxlegnth="100" value="<? echo $direktur_ii; ?>" >
                  PT.
                  <input type="text" name="pt_ii" id="pt_ii" onblur="namapt(this.id)" maxlegnth="100" value="<? echo $pt_ii; ?>" >
                  , pemegang KTP Nomor <input type="text" name="nomorktp_ii" id="nomorktp_ii" maxlegnth="100" value="<? echo $nomorktp_ii; ?>" >
                  dalam hal ini bertindak dalam jabatannya tersebut berdasarkan Akta Pendirian PT.
                  <span class="namapt">namapt</span>
                  Nomor
                  <input type="text" name="nomorakta_ii" id="nomorakta_ii" maxlegnth="100" value="<? echo $nomorakta_ii; ?>" >
                  tanggal <input type="text" name="tanggalakta_ii" id="tanggalakta_ii" readonly="readonly" onFocus="NewCssCal(this.id,'YYYYMMDD');" value="<? echo $tanggalakta_ii; ?>" >
                  yang dibuat dihadapan Notaris
                  <input type="text" name="namanotaris_ii" id="namanotaris_ii" maxlegnth="100" value="<? echo $namanotaris_ii; ?>" >,
                  di <input type="text" name="tempatnotaris_ii" id="tempatnotaris_ii" maxlegnth="100" value="<? echo $tempatnotaris_ii; ?>" >
                  dst nya <input type="text" name="dstnotaris_ii" id="dstnotaris_ii" maxlegnth="2147483647" value="<? echo $dstnotaris_ii; ?>" > (disesuaikan dengan status PT Badan Hukum atau belum Badan Hukum Bukan, untuk selanjutnya disebut :
                <p class="pihak">PIHAK KEDUA</p>

              </li>
            </ol>

            <p>Kedua belah pihak dengan ini menerangkan terlebih dahulu sebagai berikut :</p>
            <ul style="text-align: justify">
              <li>Bahwa, PIHAK PERTAMA adalah suatu lembaga keuangan yang usahanya antara lain menyelenggarakan atau dapat memberikan fasilitas Kredit  untuk pemilikan rumah/ruko/rukan  siap huni.</li>
              <li>Bahwa, PIHAK KEDUA adalah perusahaan pengembang yang membangun komplek perumahan/ruko/rukan  lengkap dengan fasilitas umum dan fasilitas sosial sesuai persyaratan yang ditentukan pemerintah yang diperuntukan bagi masyarakat, dimana penjualannya melalui fasilitas KGS Siap Huni dengan pembayaran kembali secara angsuran.</li>
            </ul>

            <p>Berdasarkan hal-hal tersebut di atas, maka PARA PIHAK setuju dan sepakat untuk mengadakan Perjanjian Kerjasama yang selanjutnya disebut dengan ‘PERJANJIAN”, dengan ketentuan- ketentuan sebagai berikut :  </p>

            <p style="text-align:center;">
              <b>
                  Pasal 1<br/>
                  PENGERTIAN
              </b>
            </p>

            <p style="text-align: justify">Dalam Perjanjian Kerjasama ini yang dimaksud dengan KGS Siap Bangun adalah fasilitas kredit pemilikan rumah/ruko/rukan siap bangun yang dapat diberikan oleh PIHAK PERTAMA kepada masyarakat berpenghasilan tetap dan tidak tetap  yang sebelumnya sudah ada Perjanjian Jual Beli (Indent) dengan pembeli.</p>

            <p style="text-align:center;">
              <b>
                  Pasal 2<br/>
                  MAKSUD DAN TUJUAN
              </b>
            </p>

            <ol style="text-align: justify;margin-left:-25px;">
              <li type="1">
                Perjanjian Kerjasama ini dimaksudkan sebagai landasan untuk mengadakan Kerjasama dalam rangka pemberian fasilitas kredit pemilikan rumah/ruko/rukan kepada masyarakat/pembeli di Komplek Perumahan
                <input type="text" name="p2_perumahan" id="p2_perumahan" maxlegnth="100" value="<? echo $p2_perumahan; ?>" >
                di
                <input type="text" name="p2_lokasi" id="p2_lokasi" maxlegnth="100" onblur="lokasi(this.id)" value="<? echo $p2_lokasi; ?>" >
              </li>
              <li>Perjanjian Kerjasama ini bertujuan untuk suksesnya pemberian KGS Siap Bangun kepada sasaran masyarakat yang layak,  yang meliputi : Sukses Penyaluran dan Pengembalian serta terwujudnya prinsip saling menguntungkan. </li>
            </ol>

            <p style="text-align:center;">
              <b>
                  Pasal 3<br/>
                  KONDISI TANAH DAN BANGUNAN
              </b>
            </p>

            <ol style="text-align: justify;margin-left:-25px;">
              <li type="1">PIHAK KEDUA hanya menjual tanah dan bangunan yang dimiliki secara sah menurut hukum dan peraturan-peraturan yang ada, dengan kondisi minimal kepemilikan tanah berupa sertipikat induk dan pembangunan rumah/ruko/rukan sesuai dengan spesifikasi dan syarat-syarat yang telah ditentukan dan disetujui oleh pembeli/calon debitur dan PIHAK KEDUA dengan memperhatikan ketentuan dan perundang-undangan yang berlaku.</li>
              <li>PIHAK PERTAMA tidak bertanggung jawab dalam hal bangunan tidak selesai dan atau tidak dikerjakan oleh PIHAK KEDUA dan PIHAK PERTAMA dibebaskan dari segala tuntutan berupa apapun dari pembeli/debitur. </li>
            </ol>

            <p style="text-align:center;">
              <b>
                  Pasal 4<br/>
                  HAK DAN KEWAJIBAN
              </b>
            </p>
            <p>Hak dan Kewajiban masing-masing pihak dalam Perjanjian Kerjasama ini adalah sebagai berikut :</p>
            <ol style="text-align: justify;" type="1">
              <li>
                PIHAK PERTAMA mempunyai Hak dan Kewajiban :
                  <ol type='i'>
                    <li>Hak :
                        <ol type="a">
                          <li>Menerima dan meminta lembar asli Perjanjian Jual Beli yang ditandatangani Pihak Kedua di atas meterai secukupnya yang dibuat antara PIHAK KEDUA dengan Pembeli rumah/ruko/rukan  yang akan mengajukan permohonan kredit.</li>
                          <li>Menerima dan meminta laporan dari PIHAK KEDUA mengenai perkembangan hasil pelaksanaan pembangunan rumah/ruko/rukan  yang dibantu fasilitas kredit, berupa laporan proggress fisik bangunan setiap bulan.</li>
                          <li>Melakukan pemeriksaan langsung ke lapangan terhadap pelaksanaan pembangunan rumah/ruko/rukan  baik sebelum atau setelah dibantu fasilitas kredit.</li>
                          <li>Menerima sertipikat, IMB, Pengikatan Jual Beli/AJB, SKMHT/APHT dan lain-lain sehubungan dengan fasilitas kredit yang diberikan PIHAK PERTAMA.</li>
                          <li>Menagih dan atau mendebet/membebani rekening giro dan atau rekening pinjaman  PIHAK KEDUA pada PIHAK PERTAMA   atas  pembayaran  pelunasan  kredit atas nama pembeli rumah/ruko/rukan dari PIHAK KEDUA dalam hal terjadi pembatalan jual beli sebelum selesai dibangun atau belum dilaksanakan serah terima.</li>
                          <li>Menagih dan atau mendebet/membebani rekening giro dan atau rekening pinjaman PIHAK KEDUA pada Pihak Petama atas pembayaran pelunasan tunggakan angsuran kredit atas nama pembeli/debitur, apabila selama masa pembangunan (konstruksi) atau sebelum serah 	terima rumah ternyata pembeli/debitur menunggak angsuran.</li>
                          <li>Atas dasar pertimbangan Bank Teknis, berhak menyetujui atau menolak  permohonan kredit atas nama pembeli/calon debitur.</li>
                          <li>PIHAK PERTAMA berhak membatalkan atau menunda pencairan fasilitas kredit atas nama pembeli/debitur yang telah disetujui, dalam hal menurut pertimbangan Bank 	Teknis perlu dilakukan.</li>
                        </ol>
                    </li>
                    <li> Kewajiban :
                      <ol>
                        <li type="a">Memfasilitasi penyediaan dana melalui fasilitas kredit pemilikan rumah/ruko/rukan Siap Bangun bagi masyarakat pembeli.</li>
                        <li>Menerima dan memproses permohonan kredit atas nama pembeli rumah/ruko/rukan  yang dibangun oleh PIHAK KEDUA.</li>
                        <li>Menyetorkan dana fasilitas kredit yang telah disetujui dan ditarik/dicairkan oleh pembeli/debitur ke rekening PIHAK KEDUA pada PIHAK PERTAMA.</li>
                      </ol>
                    </li>
                  </ol>
              </li>
              <li>PIHAK KEDUA mempunyai Hak dan Kewajiban :
                <ol type="i">
                  <li>
                    Hak :
                    <ol type="a">
                      <li >Menerima pencairan dana fasilitas kredit yang telah disetujui PIHAK PERTAMA dan ditarik/dicairkan oleh  pembeli/debitur dan dalam hal ini pencairan dana fasilitas kredit tersebut berlaku ketentuan sebagaimana tersebut pada pasal 6 Perjanjian Kerjasama ini. </li>
                    </ol>
                  </li>
                  <li>Kewajiban :
                    <ol type="a">
                      <li >Membuka rekening giro pada PIHAK PERTAMA untuk menampung seluruh aktivitas keuangan yang berkaitan dengan transaksi jual beli rumah atau ruko  yang dibangun atau yang dibiayai dengan fasilitas kredit dari PIHAK PERTAMA.</li>
                      <li>Menyediakan dana dalam rekening giro  untuk pelunasan jumlah kewajiban kredit atas nama pembeli rumah/ruko/rukan, dalam hal terjadi pembatalan jual beli   dan atau untuk pelunasan tunggakan angsuran kredit, selama masa konstruksi   dan atau belum dilaksanakan serah terima bangunan  kepada pembeli/debitur .</li>
                      <li>Menyampaikan  kepada PIHAK PERTAMA laporan hasil perkembangan fisik bangunan yang dibiayai dengan fasilitas kredit, secara berkala setiap bulan yang untuk pertama kalinya 1 (satu) bulan sejak pencairan dana fasilitas kredit sampai dengan dilaksanakannya serah terima bangunan kepada pembeli/debitur.</li>
                      <li>Membantu PIHAK PERTAMA memasarkan kredit KGS  kepada calon pembeli rumah.</li>
                      <li>Membantu PIHAK PERTAMA dalam memuhi kelengkapan dokumen permohonan kredit yang diajukan oleh pembeli.</li>
                      <li>Atas beban dan biaya sendiri mengurus dan menyerahkan kepada PIHAK PERTAMA pemenuhan kelengkapan persyaratan dokumen pencairan kredit sebagaimana tersebut pada ayat 7 pasal 5 Perjanjian Kerjasama ini.</li>
                      <li>Menyerahkan pemenuhan penandatanganan Perjanjian Kerjasama kepada PIHAK PERTAMA berupa proposal rumah/ruko/rukan yang akan dibiayai dengan fasilitas kredit dengan dilengkapi ; identitas perusahaan (termasuk izin-izin usaha dan anggota REI), izin pemakaian lokasi tanah, bukti penguasaan tanah, gambar/site plan dari instansi yang berwenang dan Surat Keterangan Bebas Banjir.</li>
                    </ol>
                  </li>

                </ol>
              </li>
            </ol>


            <p style="text-align:center;">
              <b>
                  Pasal 5<br/>
                  PROSEDUR PERMOHONAN DAN PERSYARATAN KREDIT
              </b>
            </p>
            <p>Pembeli rumah/ruko/rukan dari PIHAK KEDUA dapat mengajukan permohonan untuk mendapatkan fasilitas kredit dari PIHAK PERTAMA melalui PIHAK KEDUA atau langsung kepada PIHAK PERTAMA.</p>
            <ol style="text-align: justify;"  type="1">
              <li>Penerima fasilitas kredit  adalah masyarakat berpenghasilan tetap (PNS atau Non PNS) dan masyarakat berpenghasilan tidak tetap (Pengusaha/ Wiraswasta,profesiDokter/Dosen/Pengacara/ Notaris dan akuntan yang usahanya sudah berjalan minimal tiga tahun) dan dinilai layak oleh PIHAK PERTAMA.</li>
              <li>Persyaratan penerima kredit mengikuti ketentuan pemberian fasilitas KGS Siap Bangun yang berlaku pada PIHAK PERTAMA.</li>
              <li>Fasilitas kredit diberikan dengan maksimum pembiayaan sebesar 70 % dari harga jual rumah/ ruko/rukan dengan ketentuan  maksimum  sebesar Rp. 500.000.000,- (lima ratus juta rupiah) dan jangka waktu maksimum 10 (sepuluh) tahun atau 120 (seratus dua puluh) bulan.</li>
              <li>Suku bunga kredit sesuai dengan ketentuan yang berlaku pada PIHAK PERTAMA. Jaminan kredit adalah bukti kepemilikan tanah dan bangunan yang dibeli oleh pembeli/debitur dari PIHAK KEDUA, yaitu berupa tanah dan bangunan yang terletak dalam Komplek Perumahan <span class="lokasi">ERIKAAA</span></li>
              <li>Penandatanganan akad kredit dapat dilaksanakan setelah penyerahan Perjanjian Jual Beli Indent antara PIHAK KEDUA dengan pembeli dan penyerahan bukti pelunasan pembayaran uang muka serta pemenuhan syarat kredit oleh pembeli/calon debitur.</li>
              <li>PIHAK KEDUA berkewajiban untuk menyerahkan dan menandatangani hal-hal tersebut dibawah ini sebagai dokumen persyaratan pencairan kredit :
                <ol type="a">
                  <li>Untuk sertipikat yang telah dipecah per kavling :
                    <ul>
                      <li>Sertipikat</li>
                      <li>Pengikatan sertipikat berupa SKMHT/APHT (disesuaikan Undang-Undang),</li>
                      <li>Akta Jual Beli (notariel),</li>
                      <li>Cover Note pengurusan balik nama sertipikat dari notaris,</li>
                      <li>IMB terpecah atau Cover Note pengurusan IMB dari PIHAK KEDUA,</li>
                      <li>Surat Pernyataan Pelepasan Hak</li>
                    </ul>
                  </li>
                  <li>Untuk Sertipikat masih induk :
                    <ul>
                      <li>Cover Note pengurusan pemecahan dan penyerahan sertipikat dari notaris, Pengikatan sertipikat berupa SKMHT/APHT (disesuaikan Undang-Undang),</li>
                      <li>Perjanjian Pengikatan Jual Beli (notariel) atau AJB (notariel),</li>
                      <li>Cover Note pengurusan dan penyerahan IMB dari PIHAK KEDUA,</li>
                      <li>Surat Pernyataan Pelepasan Hak.</li>
                    </ul>
                  </li>
                </ol>
              </li>
              <li>Notaris dan Perusahaan Asuransi yang akan digunakan untuk AJB, SKMHT/APHT dan Penutupan resiko kebakaran adalah Notaris dan Perusahaan Asuransi yang ditunjuk PIHAK PERTAMA.</li>
            </ol>


            <p style="text-align:center;">
              <b>
                  Pasal 6<br/>
                  PENCAIRAN DANA FASILITAS KREDIT
              </b>
            </p>

            <ol style="text-align: justify;margin-left:-25px;"  type="1">
              <li>Sebesar 60 % (enam puluh persen) dari maksimum krdit dicairkan ke rekning giro atau rekening pinjaman (apabila debitur) PIHAK KEDUA  pada PIHAK PERTAMA untuk selanjutnya digunakan sebagai biaya pembangunan (konstruksi), sedangkan sisa sebesar 40 % (empat puluh persen) dari maksimum kredit di blokir sebagai dana retensi dalam bentuk giro atas nama PIHAK KEDUA pada PIHAK PERTAMA. </li>
              <li>Pecairan deposito dengan kondisi sebagai berikut :
                <ol type="a">
                  <li>sebesar 80 % (delapan puluh persen) dari giro, setelah penyerahan Berita Acara Serah Terima bangunan dan disetor ke rekening giro atau rekening pinjaman (apabila debitur).</li>
                  <li>Sebesar 20 % (dua puluh persen) dari nominal deposito, disetor ke rekening titipan sebagai dana retensi dan dicairkan ke rekening giro atau rekening pinjaman (apabila debitur), dengan kondisi :
                    <ul>
                      <li>sebesar 15 % (lima belas persen), setelah penyerahan sertipikat a.n debitur, IMB, AJB, SKMHT/APHT dan Polis Asuransi Kebakaran (tahun pertama).</li>
                      <li>Sebesar 5 % (lima persen), setelah masa pemeliharaan (dua bulan setelah serah terima bangunan)</li>
                    </ul>
                  </li>

                </ol>
              </li>
            </ol>



            <p style="text-align:center;">
              <b>
                  Pasal 7<br/>
                  KUASA-KUASA
              </b>
            </p>
            <ol style="text-align: justify;margin-left:-25px;"  type="1">
              <li>Selama belum terjadi serah terima bangunan  antara PIHAK KEDUA dengan pembeli/debitur , PIHAK KEDUA memberi kuasa kepada PIHAK PERTAMA, kuasa mana merupakan bagian yang tidak terpisahkan dari Perjanjian Kerjasama ini dan oleh karenanya tidak akan berakhir karena sebab-sebab yang ditentukan oleh Pasal 1813 KUH Perdata, serta dengan telah ditandatangninya Perjanjian Kerjasama ini tidak diperlukan lagi adanya suatu Surat Kuasa Khusus untuk sewaktu-waktu tanpa persetujuan terlebih dahulu oleh PIHAK KEDUA untuk :
                <ol type="a">
                  <li>Mendebet/membebani rekening giro dan atau rekening pinjaman dan atau mencairkan deposito dana retensi dari pencairan dana kredit atas nama PIHAK KEDUA pada PIHAK PERTAMA, untuk pelunasan pembayaran jumlah kewajiban kredit atas nama pembeli/debitur dalam hal terjadi pembatalan jual beli bangunan.</li>
                  <li>Mendebet/membebani rekening giro dan atau rekening pinjaman PIHAK KEDUA 	pada Pihak Pertama untuk pelunasan tunggakan angsuran kreit dalan hal 	pembeli/debitur menunggak angsuran.</li>
                </ol>
              </li>
              <li>Kuasa yang diberikan oleh PIHAK KEDUA kepada PIHAK PERTAMA sebagaimana tersebut ayat 1 pasal ini diberikan dengan hak substitusi dan merupakan bagian yang tidak terpisahkan dari Perjanjian Kerjasama ini, sehingga tanpa adanya kuasa-kuasa tersebut Perjanjian Kerjasama ini tidak akan dibuat.</li>
            </ol>


            <p style="text-align:center;">
              <b>
                  Pasal 8<br/>
                  SANKSI
              </b>
            </p>
            <ol style="text-align: justify;margin-left:-25px;"  type="1">
              <li>Dalam hal PIHAK KEDUA tidak dapat memenuhi salah satu atau beberapa kewajiban sebagaimana tersebut pada huruf b ayat 2 pasal 3 Perjanjian Kerjasama ini, maka PIHAK PERTAMA akan memberikan peringatan tertulis sebanyak 3 (tiga) kali. </li>
              <li>Apabila setelah diberikan peringatan sebagaimana dimaksud ayat 1 pasal ini PIHAK KEDUA tidak dapat memenuhi kewajibannya, maka PIHAK PERTAMA akan melakukan pemutusan Perjanjian Kerjasama secara sepihak dan dengan adanya pemutusan Perjanjian Kerjasama secara sepihak tersebut tidak menghilangkan penyelesaian kewajiban PIHAK KEDUA kepada PIHAK PERTAMA sebagai akibat dari pelaksanaan sebelum pemutusan Perjanjian Kerjasama ini. </li>
            </ol>


            <p style="text-align:center;">
              <b>
                  Pasal 9<br/>
                  MASA BERLAKU
              </b>
            </p>
            <p style="text-align: justify;">Perjanjian Kerjasama ini berlaku sejak tanggal penandatanganan Perjanjian Kerjasama oleh masing-masing pihak dan berakhir sampai dengan dilaksanakannya serah terima seluruh unit rumah/ruko (rumah toko) antara PIHAK KEDUA dengan pembeli/debitur  yang dibiayai dengan fasilitas KGS Siap Bangun dari PIHAK PERTAMA dan atau berakhir karena pasal 7 Perjanjian Kerjasama ini.</p>




            <ol style="text-align: justify;margin-left:-25px;"  type="1">
              <li>Dalam hal terjadi perbedaan penafsiran atau perselisihan mengenai pelaksanaan isi Perjanjian Kerjasama ini, kedua belah pihak sepakat akan melakukan konsultasi, negosiasi dan musyawarah guna penyelesaiannya.</li>
              <li>Apabila penyelesian sebagaimana dimaksud pada ayat 1 pasal ini tidak dapat diselesaikan, maka akan diselesaikan melalui Pengadilan Negeri di tempat kedudukan PIHAK PERTAMA.</li>
            </ol>

            <p style="text-align:center;">
              <b>
                  Pasal 11<br/>
                  PENUTUP
              </b>
            </p>
            <p style="text-align: justify;">Perjanjian Kerjasama ini dibuat dan ditandatangani oleh kedua belah pihak di <input type="text" name="p11_tempattandatangan" id="p11_tempattandatangan" maxlegnth="100" value="<? echo $p11_tempattandatangan; ?>" > dalam rangkap 2 (dua) yang masing-masing  dibubuhi  meterai  cukup  dan mempunyai  kekuatan  hukum yang sama.</p>


            <table style="width:100%;">
              <tr>
                <td style="width:200px;text-align:center;">PIHAK PERTAMA</td>
                <td>&nbsp;</td>
                <td style="width:200px;text-align:center;">PIHAK KEDUA</td>
              </tr>
              <tr>
                <td colspan="3" style="height:100px"></td>
              </tr>
              <tr>
                <td style="text-align:center;"><input type="text" name="p11_pihak_i" id="p11_pihak_i" maxlegnth="100" value="<? echo $p11_pihak_i; ?>" ></td>
                <td>&nbsp;</td>
                <td style="text-align:center;"><input type="text" name="p11_pihak_ii" id="p11_pihak_ii" maxlegnth="100" value="<? echo $p11_pihak_ii; ?>" ></td>
              </tr>
            </table>

          </td>
        </tr>
        <tr>
          <td colspan="2" style="text-align:right;"><input type="button" onclick="submitform()" value="Submit"></td>
        </tr>
      </table>
      <input type="hidden" id="custnomid" name="custnomid" value="<?php echo $custnomid;?>">
    </form>
  </body>
</html>
