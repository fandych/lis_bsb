<?	
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;
	$view = $_GET['view'];
	$taginput = "";
	$tagselect = "";
	$warna = "class=\"input\"";
	$button = "type=\"button\"";
	$tampil = "1";
	
	if($view == "preview")
	{
		$taginput = "readonly=\"readonly\"";
		$tagselect = "disabled=\"disabled\"";
		$warna = "class=\"preview2\"";
		$button = "type=\"hidden\"";
		$tampil = "0";
	}
	
	require("../../requirepage/currency.php");
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD 1</title>
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>
<table width="100%" align="center" style="border:1px solid black;" <? echo $warna;?>>
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">MUTASI REKENING KORAN / TABUNGAN</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" id="yagami">
		
		<?
			$tsqls = "select * from Tbl_LKCDMutasiRekeningNamaBank where custnomid = '$custnomid' and flag = '1'";
			$as = sqlsrv_query($conn, $tsqls);

			if ( $as === false)
			die( FormatErrors( sqlsrv_errors() ) );

			if(sqlsrv_has_rows($as))
			{  
				while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
				{
					$idx = $rows["idx"];
					$nama_bank = $rows["nama_bank"];
					$saldo_terakhir = $rows["saldo_terakhir"];
					$nomor_rekening = $rows["nomor_rekening"];
					$jenis_rekening = $rows["jenis_rekening"];
					$atas_nama = $rows["atas_nama"];
					$keterangan = $rows["keterangan"];
					
					
					require("LKCD_VID6MUTASIREKENINGKORANTABUNGAN_n_detail_v.php");
				}
			}
		?>
		
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td align="center" colspan="2"><input <? echo $button; ?> value="SAVE" style="width:150px;background-color:blue;color:white;" onclick="nanako()" /></td>
</tr>

<input type="hidden" id="userid" name="userid"  value='<? echo $userid; ?>'>
<input type="hidden" id="userpwd" name="userpwd" value='<? echo $userpwd; ?>'>
<input type="hidden" id="userbranch" name="userbranch" value='<? echo $userbranch; ?>'>
<input type="hidden" id="userregion" name="userregion" value='<? echo $userregion; ?>'>
<input type="hidden" id="buttonaction" name="buttonaction" value='<? echo $buttonaction; ?>'>
<input type="hidden" id="userwfid" name="userwfid" value='<? echo $userwfid; ?>'>
<input type="hidden" id="custnomid" name="custnomid" value='<? echo $Custnomid; ?>'>
<input type="hidden" id="custfullname" name="custfullname" value='<? echo $custfullname; ?>'>
</table>


<script type="text/javascript">
	function savethis(idx) {
		var FormName="formsavethis"+idx;
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		/*
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{

				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false
					break;
				}
			}
		}
		*/
		if(StatusAllowSubmit == true)
		{
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax6_n.php",
				data: $('#'+FormName).serialize(),
				success: function(response)
				{	//alert(response);
					//$("#sugananako").html(response);
					alert('Data berhasil disimpan');
					window.location.reload();
				}
			});
		}

	}
	function save6(bank) {
		var nan="SALDOBANK6";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var MutasiRekening_saldoakhir = $("#MutasiRekening_saldoakhir"+bank).val();
		
		var FormName="formsaldo";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&MutasiRekening_namabank="+bank+"&MutasiRekening_saldoakhir="+MutasiRekening_saldoakhir+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	function nanako(){
		var nan="SAVE6"
		
		var custnomid=$("#custnomid").val();
		var StatusAllowSubmit=true;
		if(StatusAllowSubmit == true)
		{	
			submitform = window.confirm("<? echo $confmsg;?>")
			if (submitform == true)
			{
				$.ajax({
					type: "POST",
					url: "LKCD_VIDajax_n.php",
					data: "nan="+nan+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
					success: function(response)
					{	
						//alert('Data berhasil disimpan');	
					}
				});
			}
		}
	}
	
	function kumi() {
		var nan="SAVE6A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var MutasiRekening_namabulan = $("#MutasiRekening_namabulanAdd").val();
		var MutasiRekening_mutasidebet = $("#MutasiRekening_mutasidebetAdd").val();
		var MutasiRekening_mutasikredit = $("#MutasiRekening_mutasikreditAdd").val();
			
		var FormName="formentry1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&MutasiRekening_namabulan="+MutasiRekening_namabulan+"&MutasiRekening_mutasidebet="+MutasiRekening_mutasidebet+"&MutasiRekening_mutasikredit="+MutasiRekening_mutasikredit+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function kizaki(id) {
		var nan="EDIT6A";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var MutasiRekening_namabulan = $("#MutasiRekening_namabulanAdd"+id).val();
		var MutasiRekening_mutasidebet = $("#MutasiRekening_mutasidebetAdd"+id).val();
		var MutasiRekening_mutasikredit = $("#MutasiRekening_mutasikreditAdd"+id).val();
		var MutasiRekening_saldoakhir = $("#MutasiRekening_saldoakhirAdd"+id).val();
		var MutasiRekening_seq = id;	
		
		var FormName="formedit1";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&MutasiRekening_seq="+MutasiRekening_seq+"&MutasiRekening_namabulan="+MutasiRekening_namabulan+"&MutasiRekening_mutasidebet="+MutasiRekening_mutasidebet+"&MutasiRekening_mutasikredit="+MutasiRekening_mutasikredit+"&MutasiRekening_saldoakhir="+MutasiRekening_saldoakhir+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function rena(id){
		var nan="DEL6A"
		
		var custnomid=$("#custnomid").val();
		var seqMutasi = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&seqMutasi="+seqMutasi+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function yuria() {
		var nan="SAVE6B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var RekapPenjualan_namabulan = $("#RekapPenjualan_namabulanAdd").val();
		var RekapPenjualan_penjualan = $("#RekapPenjualan_penjualanAdd").val();
			
		var FormName="formentry2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&RekapPenjualan_namabulan="+RekapPenjualan_namabulan+"&RekapPenjualan_penjualan="+RekapPenjualan_penjualan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
	
	function akari(id){
		var nan="DEL6B"
		
		var custnomid=$("#custnomid").val();
		var seqRekap = id;
		
		$.ajax({
			type: "POST",
			url: "LKCD_VIDajax_n.php",
			data: "nan="+nan+"&seqRekap="+seqRekap+"&custnomid="+custnomid+"&random="+ <?php echo time(); ?> +"",
			success: function(response)
			{	
				window.location.reload();
			}
		});
	
	}
	
	function suga(id) {
		var nan="EDIT6B";
		var custnomid=$("#custnomid").val();
		var userid=$("#userid").val();
		var userpwd=$("#userpwd").val();
		var userbranch=$("#userbranch").val();
		var userregion=$("#userregion").val();
		var buttonaction=$("#buttonaction").val();
		var userwfid=$("#userwfid").val();
		
		var RekapPenjualan_namabulan = $("#RekapPenjualan_namabulanAdd"+id).val();
		var RekapPenjualan_penjualan = $("#RekapPenjualan_penjualanAdd"+id).val();
		var RekapPenjualan_seq = id;	
		
		var FormName="formedit2";	
		var StatusAllowSubmit=true;
		var elem = document.getElementById(FormName).elements;
		for(var i = 0; i < elem.length; i++)
		{
			if(elem[i].style.backgroundColor=="#ff0")
			{
				
				if(elem[i].value == "")
				{
					alert(elem[i].nai + "HARUS DIISI");
					elem[i].focus();
					StatusAllowSubmit=false				
					break;
				}
			}
		}
		
		if(StatusAllowSubmit == true)
		{	
			$.ajax({
				type: "POST",
				url: "LKCD_VIDajax_n.php",
				data: "nan="+nan+"&RekapPenjualan_seq="+RekapPenjualan_seq+"&RekapPenjualan_namabulan="+RekapPenjualan_namabulan+"&RekapPenjualan_penjualan="+RekapPenjualan_penjualan+"&custnomid="+custnomid+"&userid="+userid+"&userpwd="+userpwd+"&userbranch="+userbranch+"&userregion="+userregion+"&buttonaction="+buttonaction+"&userwfid="+userwfid+"&random="+ <?php echo time(); ?> +"",
				success: function(response)
				{	//alert(response);
					//$("#yagami").html(response);
					window.location.reload();
				}
			});
		}
		//alert('Data berhasil disimpan');		
	}
</script>
</body>
</html> 