<?php

class MYSSQL
{	
    //varibale global for get return query ( _RQ ) or execute query ( _EQ )
	var $_RS; // RETURN strsql
	var $_RR; // RETURN rows
	var $_REMOTE;
	var $_SHOWERROR;
	var $_RESULT;
    
    
    //data Remote
    var $_SERVER;
    var $_USERDB;
    
    //debug and language
    var $_DEBUGQUERY    = true;
    var $_LANGUAGE      = "0";//0=return null 1=LOCAL; 2=ENGLISH
    
    
	function _ON($server,$user,$password,$usedb)
	{
        //$multidb => for using more than 1 db in 1 server
		$this->_REMOTE = mysql_connect($server, $user, $password);
		if (!$this->_REMOTE) 
		{
			echo "
            <div style=\"font-weight:bold;color:red;font-size:12pt;border:0px solid black;padding:10px;display:inline-block;\">
            Wrong Parameter Connection To Database</div>";
		}
        else
        {
            $this->_EQ("USE ".$usedb);
            $this->_SERVER= $server;
            $this->_USEDB= $usedb;
        }
	}
	
	function _EQ($Strsql)
	{
		$Result = mysql_query($Strsql);
		
		if (!$Result)
		{
            if($this->_DEBUGQUERY)
            {
                $this->_RESULT = $this->_EM($Strsql);
            }
		}
        else
        {
            $this->_LANGUAGE();
        }
        return $this->_RESULT;
	}
	
	function _RQ($Strsql)
	{
		
		$Result = mysql_query($Strsql);
		
		
		if (!$Result)
		{
			if($this->_DEBUGQUERY)
            {
                $this->_RESULT = $this->_EM($Strsql);
            }
		}
		else
		{
			$this->_RS = array();
			$ResultRow = mysql_num_rows($Result);
			
			while ($rows = mysql_fetch_array($Result, MYSQL_ASSOC))
			{
				$this->_RS[] = $rows;
			}
			$this->_RR = $ResultRow;
            
            if($this->_RR > 0)
            {
                $this->_RESULT = $this->_RS;
            }
            else
            {
                $this->_LANGUAGE();
            }
			mysql_free_result($Result);
		}
        return $this->_RESULT;
	}
	
	function _EM($strsql){
		return "<div style=\"font-weight:bold;color:red;font-size:12pt;border:0px solid black;padding:10px;display:inline-block;\">
                Could not successfully run query on server ".$this->server." at database ".$this->usedb.": </br>"
                .$Strsql."</br>".mysql_error()."</div>";
	}
	
	function _LANGUAGE(){
		if($this->_LANGUAGE=="0")
		{
			$this->_RESULT = "";
		}
		else if($this->_LANGUAGE=="1")
		{
			$this->_RESULT = "Tidak Ada Data";
		}
		else if($this->_LANGUAGE=="2")
		{
			$this->_RESULT = "No Data";    
		}
	}
	
}



function get1data($condb,$strsql)
{
    $return ="";
	if($strsql!="")
	{
		$getlastsubstring = strpos($strsql, "from")-6;
		$getarrayname = trim(substr($strsql,6,$getlastsubstring));
		$rs = $condb->_RQ($strsql);
		if(!is_array($rs)){$return = "";}else{$return=$rs[0][$getarrayname];}
	}
    return $return;
}


$condb = new MYSSQL();
$condb->_ON("localhost","user","user","test");



?>