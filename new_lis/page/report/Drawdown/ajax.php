<?
require_once ("../../../lib/open_con.php");
require_once ("../../../lib/formatError.php");

$userid=$_REQUEST['userid'];
$userbranch = $_REQUEST['userbranch'];
$userregion = $_REQUEST['userregion'];
$daily1=$_REQUEST['daily1'];
$daily2=$_REQUEST['daily2'];

include("../../../requirepage/level.php");

$contentperpage=10;

$clickpage=1;
if(isset($_REQUEST['page']))
{
$clickpage=$_REQUEST['page'];
}

$totalpage=0;
$strsql="
select  cast((COUNT(*)/".$contentperpage.") as int)+ (case when (COUNT(*)%".$contentperpage.")<>0 then 1 else 0 end) as 'totalpage', count(*) as 'ttlapp',
convert(varchar,CONVERT(DATETIME,'".$daily1."',120),110) as 'daritanggal',convert(varchar,dateadd(DAY,1,CONVERT(datetime,'".$daily2."',120)),110) as 'sampaitanggal'
from Tbl_CustomerMasterPerson a
join tbl_format_drawdown b on a.custnomid = b.custnomid
where b.regioncode = '".$userregion."' AND
b.countertime between '".$daily1."' and dateadd(DAY,1,CONVERT(datetime,'".$daily2."',120))
";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$totalpage=$rows['totalpage'];
		$ttlapp=$rows['ttlapp'];
		$sampaitanggal=$rows['sampaitanggal'];
		$daritanggal=$rows['daritanggal'];
	}
}



$startrow=($clickpage-1)*10;
if ($clickpage!="1")
{
$startrow+=1;
}
$endrow=$clickpage*10;


?>
<div style="text-align:center;">
	<div style="width:600px;border:0px solid black">
		<div>
			<div style="border:0px solid black; text-align:left; font-weight:bold;">Total Aplikasi dari tanggal <?echo $daritanggal?> sampai tanggal <?echo $sampaitanggal?> adalah <?=$ttlapp?></div>
		</div>
	</div>
</div>
<div>&nbsp;</div>
<table align="center" border="1" style="width:600px; font-family:Tahoma, Arial, Verdana, sans-serif;" cellpadding="0">
	<tr  style="width:30%; background-color:#CCCCCC;">
		<td style="width:50px;">No.</td>
		<td style="width:100px;">Aplication ID</td>
		<td>Nama Debitur</td>
		<td style="width:20%;">Tanggal</td>
		<td style="width:5%;">view</td>
	</tr>
	<?	
		$strsql="select * from (
				select ROW_NUMBER() OVER(ORDER BY a.custnomid) 'idx',a.custnomid,
				'name'= case when custsex='0' then custbusname else custfullname end,
				b.countertime as 'tgl', a.custbranchcode as 'branch',
				 '<A style=\"text-decoration:none;\" target=\"baru\" HREF=\"../../PREVIEW/PREVIEWALL/PreviewALL_n.php?custnomid='+a.custnomid+'&userid=&userpwd=&userbranch=&userregion=&userwfid=&userpermission=&buttonaction=\">View</a>' as 'link'
				from Tbl_CustomerMasterPerson a
				join tbl_format_drawdown b on a.custnomid = b.custnomid
				where b.regioncode = '$userregion' AND
				b.countertime between '".$daily1."' and dateadd(DAY,1,CONVERT(datetime,'".$daily2."',120))
				)tblx WHERE idx >=".$startrow." AND idx <=".$endrow."";
				//echo $strsql;
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				echo "
				<tr>
					<td>".$rows['idx']."</td>
					<td>".$rows['custnomid']."</td>
					<td>".$rows['name']."</td>
					<td>".$rows['tgl']->format("d F Y")."</td>
					<td>".$rows['link']."</td>
				</tr>
				";
			}
		}
	?>
</table>
<div style="text-align:center;">
	<div style="width:600px;border:0px solid black">
		<div>&nbsp;</div>
		<div>
		<?
			for($i=0; $i<$totalpage; $i++)
			{
				$pagebutton =$i+1;
				echo ' <input class="button" id="'.$pagebutton.'" name="'.$pagebutton.'" type="button" value="'.$pagebutton.'" onclick="btnpage(this.id)">';
			}
		?>
		</div>
	</div>
</div>