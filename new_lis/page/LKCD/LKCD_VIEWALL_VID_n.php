<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");

	$userid=$_GET['userid'];
	$userpwd=$_GET['userpwd'];
	$userbranch=$_GET['userbranch'];
	$userregion=$_GET['userregion'];
	$userwfid=$_GET['userwfid'];
	$userpermission=$_GET['userpermission'];
	$buttonaction=$_GET['buttonaction'];
	$Custnomid=$_GET['custnomid'];
	$custnomid = $Custnomid;

	require ("../../requirepage/currency.php");

	$tanggalkunjungan = "";
	$cabang = "";

	$tsql = "select * from Tbl_customermasterperson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$tanggalkunjungan = $row["custapldate"];
			$cabang = $row["custbranchcode"];
		}
	}


	//Tahap 1

	$jenispermohonankredit = "";

	$tsql = "select custcreditstatus from Tbl_CustomerMasterPerson where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["custcreditstatus"];
		}
	}

	$tsql = "select status_name from TblCreditStatus where status_code = '$jenispermohonankredit'";
	$a = sqlsrv_query($conn, $tsql);

	  if ( $a === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$jenispermohonankredit = $row["status_name"];
		}
	}


	$bunga = 0;
	$bungatotal = 0;
	$bungapersen = 0;

	$tsql = "select custcreditplafond from tbl_CustomerFacility where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["custcreditplafond"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	$tsql = "SELECT * FROM TBL_LKCDFASILITASPINJAMANBANKMEGA WHERE FASILITASPINJAMANBANKMEGA_JENISFASILITAS IN (SELECT JENISFASILITASBANKMEGA_CODE FROM TBL_LKCDJENISFASILITASBANKMEGA WHERE JENISFASILITASBANKMEGA_FLAG = 'UKM') AND CUSTNOMID = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$bunga = $row["FasilitasPinjamanBankMega_outstanding"];
			$bungatotal = $bungatotal + $bunga;
		}
	}

	//$tsqlproduk = "select BUNGA_PERSEN from TBL_MASTER_BUNGA where BUNGA_MIN < '$bungatotal' AND BUNGA_MAX >= '$bungatotal'";
	$tsqlproduk = "SELECT sukubunga as BUNGA_PERSEN FROM tbl_customerfacility WHERE custnomid = '$custnomid'";
	$aproduk = sqlsrv_query($conn, $tsqlproduk);

	  if ( $aproduk === false)
	  die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($aproduk))
	{
		if($rowproduk = sqlsrv_fetch_array($aproduk, SQLSRV_FETCH_ASSOC))
		{
			$bungapersen = $rowproduk["BUNGA_PERSEN"];
		}
	}

	// Tahap 2
	//----------

	// Tahap 7

	$InfoFasilitasPinjamanBankLain_cb1 = "";
	$InfoFasilitasPinjamanBankLain_cb2 = "";
	$InfoFasilitasPinjamanBankLain_cb3 = "";

	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankLain where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankLain_cb1 = $row["InfoFasilitasPinjamanBankLain_cb1"];
			$InfoFasilitasPinjamanBankLain_cb2 = $row["InfoFasilitasPinjamanBankLain_cb2"];
			$InfoFasilitasPinjamanBankLain_cb3 = $row["InfoFasilitasPinjamanBankLain_cb3"];

		}
	}

	//Tahap 8

	$InfoFasilitasPinjamanBankMega_nocif = "";
	$InfoFasilitasPinjamanBankMega_nasabahsejak = "";
	$InfoFasilitasPinjamanBankMega_datapertanggal = "";
	$InfoFasilitasPinjamanBankMega_cb1 = "";

	$tsql = "select * from Tbl_LKCDInfoFasilitasPinjamanBankMega where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$InfoFasilitasPinjamanBankMega_nocif = $row["InfoFasilitasPinjamanBankMega_nocif"];
			$InfoFasilitasPinjamanBankMega_nasabahsejak = $row["InfoFasilitasPinjamanBankMega_nasabahsejak"];
			$InfoFasilitasPinjamanBankMega_datapertanggal = $row["InfoFasilitasPinjamanBankMega_datapertanggal"];
			$InfoFasilitasPinjamanBankMega_cb1 = $row["InfoFasilitasPinjamanBankMega_cb1"];

		}
	}

	//Tahap 9

	$TradeCheck_tanggalpemasok1 = "";
	$TradeCheck_namapemberiinfopemasok1 = "";
	$TradeCheck_namapemasok1 = "";
	$TradeCheck_noteleponpemasok1 = "";
	$TradeCheck_alamatpemasok1 = "";
	$TradeCheck_lamahubunganpemasok1 = "";
	$TradeCheck_jenisbarangpemasok1 = "";
	$TradeCheck_ratapenjualanpemasok1 = 0;
	$TradeCheck_termpembayaranpemasok1 = "";
	$TradeCheck_karaktercadebpemasok1 = "";
	$TradeCheck_infonegatifpemasok1 = "";
	$TradeCheck_ketepatanpemasok1 = "";
	$TradeCheck_tanggalpemasok2 = "";
	$TradeCheck_namapemberiinfopemasok2 = "";
	$TradeCheck_namapemasok2 = "";
	$TradeCheck_noteleponpemasok2 = "";
	$TradeCheck_alamatpemasok2 = "";
	$TradeCheck_lamahubunganpemasok2 = "";
	$TradeCheck_jenisbarangpemasok2 = "";
	$TradeCheck_ratapenjualanpemasok2 = 0;
	$TradeCheck_termpembayaranpemasok2 = "";
	$TradeCheck_karaktercadebpemasok2 = "";
	$TradeCheck_infonegatifpemasok2 = "";
	$TradeCheck_ketepatanpemasok2 = "";
	$TradeCheck_tanggalpembeli1 = "";
	$TradeCheck_namapemberiinfopembeli1 = "";
	$TradeCheck_namapembeli1 = "";
	$TradeCheck_noteleponpembeli1 = "";
	$TradeCheck_alamatpembeli1 = "";
	$TradeCheck_lamahubunganpembeli1 = "";
	$TradeCheck_jenisbarangpembeli1 = "";
	$TradeCheck_ratapenjualanpembeli1 = 0;
	$TradeCheck_termpembayaranpembeli1 = "";
	$TradeCheck_karaktercadebpembeli1 = "";
	$TradeCheck_infonegatifpembeli1 = "";
	$TradeCheck_tanggalpembeli2 = "";
	$TradeCheck_namapemberiinfopembeli2 = "";
	$TradeCheck_namapembeli2 = "";
	$TradeCheck_noteleponpembeli2 = "";
	$TradeCheck_alamatpembeli2 = "";
	$TradeCheck_lamahubunganpembeli2 = "";
	$TradeCheck_jenisbarangpembeli2 = "";
	$TradeCheck_ratapenjualanpembeli2 = 0;
	$TradeCheck_termpembayaranpembeli2 = "";
	$TradeCheck_karaktercadebpembeli2 = "";
	$TradeCheck_infonegatifpembeli2 = "";
	$TradeCheck_tanggalkomunitas1 = "";
	$TradeCheck_namapemberiinfokomunitas1 = "";
	$TradeCheck_namakomunitas1 = "";
	$TradeCheck_noteleponkomunitas1 = "";
	$TradeCheck_alamatkomunitas1 = "";
	$TradeCheck_lamahubungankomunitas1 = "";
	$TradeCheck_jenisbarangkomunitas1 = "";
	$TradeCheck_ratapenjualankomunitas1 = 0;
	$TradeCheck_termpembayarankomunitas1 = "";
	$TradeCheck_karaktercadebkomunitas1 = "";
	$TradeCheck_infonegatifkomunitas1 = "";
	$TradeCheck_ketepatankomunitas1 = "";
	$TradeCheck_tanggalkomunitas2 = "";
	$TradeCheck_namapemberiinfokomunitas2 = "";
	$TradeCheck_namakomunitas2 = "";
	$TradeCheck_noteleponkomunitas2 = "";
	$TradeCheck_alamatkomunitas2 = "";
	$TradeCheck_lamahubungankomunitas2 = "";
	$TradeCheck_jenisbarangkomunitas2 = "";
	$TradeCheck_ratapenjualankomunitas2 = 0;
	$TradeCheck_termpembayarankomunitas2 = "";
	$TradeCheck_karaktercadebkomunitas2 = "";
	$TradeCheck_infonegatifkomunitas2 = "";
	$TradeCheck_ketepatankomunitas2 = "";

	$tsql = "select * from Tbl_LKCDTradeCheck where custnomid = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);

	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$TradeCheck_tanggalpemasok1 = $row["TradeCheck_tanggalpemasok1"];
			$TradeCheck_namapemberiinfopemasok1 = $row["TradeCheck_namapemberiinfopemasok1"];
			$TradeCheck_namapemasok1 = $row["TradeCheck_namapemasok1"];
			$TradeCheck_noteleponpemasok1 = $row["TradeCheck_noteleponpemasok1"];
			$TradeCheck_alamatpemasok1 = $row["TradeCheck_alamatpemasok1"];
			$TradeCheck_lamahubunganpemasok1 = $row["TradeCheck_lamahubunganpemasok1"];
			$TradeCheck_jenisbarangpemasok1 = $row["TradeCheck_jenisbarangpemasok1"];
			$TradeCheck_ratapenjualanpemasok1 = $row["TradeCheck_ratapenjualanpemasok1"];
			$TradeCheck_termpembayaranpemasok1 = $row["TradeCheck_termpembayaranpemasok1"];
			$TradeCheck_karaktercadebpemasok1 = $row["TradeCheck_karaktercadebpemasok1"];
			$TradeCheck_infonegatifpemasok1 = $row["TradeCheck_infonegatifpemasok1"];
			$TradeCheck_ketepatanpemasok1 = $row["TradeCheck_ketepatanpemasok1"];
			$TradeCheck_tanggalpemasok2 = $row["TradeCheck_tanggalpemasok2"];
			$TradeCheck_namapemberiinfopemasok2 = $row["TradeCheck_namapemberiinfopemasok2"];
			$TradeCheck_namapemasok2 = $row["TradeCheck_namapemasok2"];
			$TradeCheck_noteleponpemasok2 = $row["TradeCheck_noteleponpemasok2"];
			$TradeCheck_alamatpemasok2 = $row["TradeCheck_alamatpemasok2"];
			$TradeCheck_lamahubunganpemasok2 = $row["TradeCheck_lamahubunganpemasok2"];
			$TradeCheck_jenisbarangpemasok2 = $row["TradeCheck_jenisbarangpemasok2"];
			$TradeCheck_ratapenjualanpemasok2 = $row["TradeCheck_ratapenjualanpemasok2"];
			$TradeCheck_termpembayaranpemasok2 = $row["TradeCheck_termpembayaranpemasok2"];
			$TradeCheck_karaktercadebpemasok2 = $row["TradeCheck_karaktercadebpemasok2"];
			$TradeCheck_infonegatifpemasok2 = $row["TradeCheck_infonegatifpemasok2"];
			$TradeCheck_ketepatanpemasok2 = $row["TradeCheck_ketepatanpemasok2"];
			$TradeCheck_tanggalpembeli1 = $row["TradeCheck_tanggalpembeli1"];
			$TradeCheck_namapemberiinfopembeli1 = $row["TradeCheck_namapemberiinfopembeli1"];
			$TradeCheck_namapembeli1 = $row["TradeCheck_namapembeli1"];
			$TradeCheck_noteleponpembeli1 = $row["TradeCheck_noteleponpembeli1"];
			$TradeCheck_alamatpembeli1 = $row["TradeCheck_alamatpembeli1"];
			$TradeCheck_lamahubunganpembeli1 = $row["TradeCheck_lamahubunganpembeli1"];
			$TradeCheck_jenisbarangpembeli1 = $row["TradeCheck_jenisbarangpembeli1"];
			$TradeCheck_ratapenjualanpembeli1 = $row["TradeCheck_ratapenjualanpembeli1"];
			$TradeCheck_termpembayaranpembeli1 = $row["TradeCheck_termpembayaranpembeli1"];
			$TradeCheck_karaktercadebpembeli1 = $row["TradeCheck_karaktercadebpembeli1"];
			$TradeCheck_infonegatifpembeli1 = $row["TradeCheck_infonegatifpembeli1"];
			$TradeCheck_tanggalpembeli2 = $row["TradeCheck_tanggalpembeli2"];
			$TradeCheck_namapemberiinfopembeli2 = $row["TradeCheck_namapemberiinfopembeli2"];
			$TradeCheck_namapembeli2 = $row["TradeCheck_namapembeli2"];
			$TradeCheck_noteleponpembeli2 = $row["TradeCheck_noteleponpembeli2"];
			$TradeCheck_alamatpembeli2 = $row["TradeCheck_alamatpembeli2"];
			$TradeCheck_lamahubunganpembeli2 = $row["TradeCheck_lamahubunganpembeli2"];
			$TradeCheck_jenisbarangpembeli2 = $row["TradeCheck_jenisbarangpembeli2"];
			$TradeCheck_ratapenjualanpembeli2 = $row["TradeCheck_ratapenjualanpembeli2"];
			$TradeCheck_termpembayaranpembeli2 = $row["TradeCheck_termpembayaranpembeli2"];
			$TradeCheck_karaktercadebpembeli2 = $row["TradeCheck_karaktercadebpembeli2"];
			$TradeCheck_infonegatifpembeli2 = $row["TradeCheck_infonegatifpembeli2"];
			$TradeCheck_tanggalkomunitas1 = $row["TradeCheck_tanggalkomunitas1"];
			$TradeCheck_namapemberiinfokomunitas1 = $row["TradeCheck_namapemberiinfokomunitas1"];
			$TradeCheck_namakomunitas1 = $row["TradeCheck_namakomunitas1"];
			$TradeCheck_noteleponkomunitas1 = $row["TradeCheck_noteleponkomunitas1"];
			$TradeCheck_alamatkomunitas1 = $row["TradeCheck_alamatkomunitas1"];
			$TradeCheck_lamahubungankomunitas1 = $row["TradeCheck_lamahubungankomunitas1"];
			$TradeCheck_jenisbarangkomunitas1 = $row["TradeCheck_jenisbarangkomunitas1"];
			$TradeCheck_ratapenjualankomunitas1 = $row["TradeCheck_ratapenjualankomunitas1"];
			$TradeCheck_termpembayarankomunitas1 = $row["TradeCheck_termpembayarankomunitas1"];
			$TradeCheck_karaktercadebkomunitas1 = $row["TradeCheck_karaktercadebkomunitas1"];
			$TradeCheck_infonegatifkomunitas1 = $row["TradeCheck_infonegatifkomunitas1"];
			$TradeCheck_ketepatankomunitas1 = $row["TradeCheck_ketepatankomunitas1"];
			$TradeCheck_tanggalkomunitas2 = $row["TradeCheck_tanggalkomunitas2"];
			$TradeCheck_namapemberiinfokomunitas2 = $row["TradeCheck_namapemberiinfokomunitas2"];
			$TradeCheck_namakomunitas2 = $row["TradeCheck_namakomunitas2"];
			$TradeCheck_noteleponkomunitas2 = $row["TradeCheck_noteleponkomunitas2"];
			$TradeCheck_alamatkomunitas2 = $row["TradeCheck_alamatkomunitas2"];
			$TradeCheck_lamahubungankomunitas2 = $row["TradeCheck_lamahubungankomunitas2"];
			$TradeCheck_jenisbarangkomunitas2 = $row["TradeCheck_jenisbarangkomunitas2"];
			$TradeCheck_ratapenjualankomunitas2 = $row["TradeCheck_ratapenjualankomunitas2"];
			$TradeCheck_termpembayarankomunitas2 = $row["TradeCheck_termpembayarankomunitas2"];
			$TradeCheck_karaktercadebkomunitas2 = $row["TradeCheck_karaktercadebkomunitas2"];
			$TradeCheck_infonegatifkomunitas2 = $row["TradeCheck_infonegatifkomunitas2"];
			$TradeCheck_ketepatankomunitas2 = $row["TradeCheck_ketepatankomunitas2"];
		}
	}

	//END TAHAP 9

	$counttab = "0";

	$tsql = "select count(*) as count from Tbl_InfoTab where custnomid = '$custnomid' AND InfoTab_Flow = 'LKCD'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{
		if($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$count = $row["count"];
		}
	}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LKCD VIEW</title>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<script type="text/javascript" src="../../js/full_function.js"></script>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function outputMoney(theid)
	{
		//alert(theid);
		var rep = replace(document.getElementById(theid).value, ',', '');
		document.getElementById(theid).value = rep;
		var number = document.getElementById(theid).value;
		var newOutput = outputDollars(Math.floor(number-0) +'');
		document.getElementById(theid).value = newOutput;
	}
</script>
</head>
<body>

<table width="950px" align="center" style="border:1px solid black;" class="preview">
<tr>
	<td colspan="2" style="text-align:center;font-weight:bold;">LAPORAN KUNJUNGAN SETEMPAT</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td >Tanggal Kunjungan</td>
	<td width="75%" ><div style="border:1px solid black;width:200px;"><? echo $tanggalkunjungan;?></div></td>
</tr>
<tr>
	<td >Cabang</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $cabang;?></div></td>
</tr>
<tr>
	<td >No. Aplikasi</td>
	<td width="75%"><div style="border:1px solid black;width:200px;"><? echo $custnomid;?></div></td>
</tr>
<!--
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 2">
	<td colspan="2" style="text-align:center;font-weight:bold;"><? include("LKCD_VID2IDENTIFIKASIKEBUTUHANKREDITINVESTASI_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
-->
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 3">
	<td colspan="2"><? include("./new_pasangan/pasanganvid_view.php"); ?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<tr id="TAHAP 4">
	<td colspan="2"><? include("./survey/surveyvid_v.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<?
	$custfullname = "";

	$tsqls = "select * from tbl_customermasterperson2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$custfullname = $rows["custfullname"];
		}
	}

	$tsqls = "select * from tbl_lkcdcallmemo where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$tanggal = $rows['tanggal'];
			$tanggal_call = $rows['tanggal_call'];
			$tanggal_call_sebelumnya = $rows['tanggal_call_sebelumnya'];
			$bentuk_call = $rows['bentuk_call'];
			$pejabat_melakukan_call = $rows['pejabat_melakukan_call'];
			$pejabat_di_call = $rows['pejabat_di_call'];
			$tujuan_call = $rows['tujuan_call'];
			$hasil = $rows['hasil'];
		}
	}
?>
<tr id="TAHAP 5">
	<td colspan="2">
		<table width="100%" align="center"  >
		<tr>
			<td colspan="2" style="text-align:center;font-weight:bold;">CALL MEMO</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="1" width="95%%" align="center" id="tblpreview" cellspacing="0" >
			<tr>
				<td colspan="4" width="66%">&nbsp;</td>
				<td colspan="2" width="34%">TANGGAL : <?php echo $tanggal;?></td>
			</tr>
			<tr>
				<td colspan="3" style="padding:5px;" width="50%">
					Nama Nasabah </br>
					<strong><?=$custfullname;?></strong>
				</td>
				<td colspan="3" style="padding:5px;" width="50%">
					<table border="0" width="90%">
					<tr>
						<td>Tanggal Call</td>
						<td>: <?php echo $tanggal_call;?></td>
					</tr>
					<tr>
						<td>Tanggal Call Sebelumnya</td>
						<td>: <?php echo $tanggal_call_sebelumnya;?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Bentuk Call :</strong> </br>
					<?=$bentuk_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang melakukan Call :</strong> </br>
					<?=$pejabat_melakukan_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang di Call :</strong> </br>
					<?=$pejabat_di_call;?>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Tujuan Call :</strong> </br>
					<?=$tujuan_call;?>
				</tr>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Hasil :</strong> </br>
					<?=$hasil;?>
				</tr>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<?
	$custfullname = "";

	$tsqls = "select * from tbl_customermasterperson2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$custfullname = $rows["custfullname"];
		}
	}

	$tsqls = "select * from tbl_lkcdcallmemo2 where custnomid = '$custnomid'";
	$as = sqlsrv_query($conn, $tsqls);
	if ( $as === false)
	die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($as))
	{
		while($rows = sqlsrv_fetch_array($as, SQLSRV_FETCH_ASSOC))
		{
			$tanggal = $rows['tanggal'];
			$tanggal_call = $rows['tanggal_call'];
			$tanggal_call_sebelumnya = $rows['tanggal_call_sebelumnya'];
			$bentuk_call = $rows['bentuk_call'];
			$pejabat_melakukan_call = $rows['pejabat_melakukan_call'];
			$pejabat_di_call = $rows['pejabat_di_call'];
			$tujuan_call = $rows['tujuan_call'];
			$hasil = $rows['hasil'];
		}
	}
?>
<tr id="TAHAP 5,2">
	<td colspan="2">
		<table width="100%" align="center"  >
		<tr>
			<td colspan="2" style="text-align:center;font-weight:bold;">CALL MEMO</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">
			<table border="1" width="95%%" align="center" id="tblpreview" cellspacing="0" >
			<tr>
				<td colspan="4" width="66%">&nbsp;</td>
				<td colspan="2" width="34%">TANGGAL : <?php echo $tanggal;?></td>
			</tr>
			<tr>
				<td colspan="3" style="padding:5px;" width="50%">
					Nama Nasabah </br>
					<strong><?=$custfullname;?></strong>
				</td>
				<td colspan="3" style="padding:5px;" width="50%">
					<table border="0" width="90%">
					<tr>
						<td>Tanggal Call</td>
						<td>: <?php echo $tanggal_call;?></td>
					</tr>
					<tr>
						<td>Tanggal Call Sebelumnya</td>
						<td>: <?php echo $tanggal_call_sebelumnya;?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Bentuk Call :</strong> </br>
					<?=$bentuk_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang melakukan Call :</strong> </br>
					<?=$pejabat_melakukan_call;?>
				</td>
				<td colspan="2" style="padding:5px;" width="33%">
					<strong>Pejabat yang di Call :</strong> </br>
					<?=$pejabat_di_call;?>
				</td>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Tujuan Call :</strong> </br>
					<?=$tujuan_call;?>
				</tr>
			</tr>
			<tr>
				<td colspan="6" style="padding:5px;" width="100%">
					<strong>Hasil :</strong> </br>
					<?=$hasil;?>
				</tr>
			</tr>
			</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 6">
	<td colspan="2" style="text-align:center;font-weight:bold;"><? include("LKCD_VID6MUTASIREKENINGKORANTABUNGAN_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<tr id="TAHAP 7">
	<td colspan="2" style="text-align:center;font-weight:bold;"><? include("LKCD_VID7FASILITASPINJAMANDIBANKLAIN_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<tr id="TAHAP 8">
	<td colspan="2" style="text-align:center;font-weight:bold;"><? include("LKCD_VID8FASILITASPINJAMANDIBANKMEGA_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr id="TAHAP 1">
	<td colspan="2" style="text-align:center;font-weight:bold;"><? include("LKCD_VID1IDENTIFIKASIFASILITASYANGDIAJUKAN_n.php");?></td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
<tr id="TAHAP 8.5">
	<!--<td colspan="2"><? include("../Appraisal/viewappraisalresponse_v3.php"); ?></td>-->
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2" class="pagebreak">&nbsp;</td>
</tr>
</table>
<div>&nbsp;</div>
<?
	$tsql = "select * from Tbl_FLKCD where TXN_ID = '$custnomid' AND TXN_ACTION = 'A'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($a))
	{

	}
	else
	{

		if($count == "6" && $userid != "" && $userwfid!="")
		{
	?>
		<form name="frm" id="frm">
		<div style="text-align:center; border:0px solid black;">
			<?require ("../../requirepage/btnview.php");
			require ("../../requirepage/hiddenfield.php");?>
		</div>
		</form>
	<?
		}
		else if($count != "6" && $userid != "")
		{
			echo '<div align="center" style="color:red;font-size:20px"><i>Harap semua flow diisi</i></div>';
		}
	}
?>

</body>
</html>
