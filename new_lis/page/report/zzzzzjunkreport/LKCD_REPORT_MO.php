<?
	$userid=$_REQUEST['userid'];
?>
<html>
	<head>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
		<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
		
			function statusflag() 
			{
				var flag = $("#flag").val();
				//alert (flag);
				
				document.getElementById("dtl_1").style.display="none";
				document.getElementById("dtl__1").style.display="none";
				document.getElementById("dtl_2").style.display="none";
				document.getElementById("dtl__2").style.display="none";
				
				if (flag=="1")
				{
					document.getElementById("dtl_1").style.display="block";
					document.getElementById("dtl__1").style.display="block";
				}
				else if (flag=="2")
				{
					document.getElementById("dtl_2").style.display="block";
					document.getElementById("dtl__2").style.display="block";
				}
			}
			
			function validation()
			{
				
				var flag = $("#flag").val();
				//alert (flag);
				var daily = $("#daily").val();
				var month = $("#month").val();
				var StatusAllowSubmit=true;
				
				if (flag=="")
				{
					alert("Jenis Report Harus di isi");
					$("#flag").focus();
					StatusAllowSubmit=false
				}
				else if (flag=="1")
				{
					if (daily=="")
					{
						alert("Daily Report Harus di isi");
						$("#daily").focus();
						StatusAllowSubmit=false
					}
					
				}
				else if (flag=="2")
				{
					if(month=="")
					{
						alert("Monthly Report Harus di isi");
						$("#month").focus();
						StatusAllowSubmit=false
					}
				}
				
				
				if(StatusAllowSubmit == true)
				{			
					document.getElementById("formentry").action = "LKCD_REPORT_MO_VIEW.php";			
					document.getElementById("formentry").submit();
				}
			}
		</script>
	</head>
	<body onLoad="statusflag();">
		<form id="formentry" name="formentry" method="post">
			<table align="center" border="1" style="width:300px; border:1px solid black;">
				<tr>
					<td colspan="2" style="font-size:16;font-weight:bold; text-align:center;">REPORT LKCD</td>
				</tr>
				<tr>
					<td colspan="2" style="font-size:16;font-weight:bold; text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:30%;">Jenis Report</td>
					<td>
						<select nai="Jenis Report " id="flag" name="flag" style="background:#ff0;" onChange="statusflag();">
							<option value=""> -- Pilih Jenis Report -- </option>
							<option value="1">Daily Report</option>
							<option value="2">Monthly Report</option>
						</select>
					</td>
				</tr>
				<tr>
					<td  id="dtl_1">Daily Report</td>
					<td  id="dtl__1"><input style="background:#ff0;" type="text" id="daily" name="daily" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');"/></td>
				</tr>
				<tr>
					<td id="dtl_2">Monthly Report</td>
					<td id="dtl__2">
						<select nai="Monthly Report" style="background:#ff0;" id="month" name="month">
							<option value=""> -- Pilih Bulan -- </option>
							<?
								for ($i=1; $i<=12; $i++)
								{
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;"><input type="hidden" name="userid" id="userid" value="<?echo $userid;?>" /></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:center;"><input type="button" id="btnsubmit" name="btnsubmit" class="button" value="submit"onclick="validation()"/></td>
				</tr>
			</table>
		</form>
	</body>
</html>