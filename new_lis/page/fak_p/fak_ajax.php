<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
$btn=$_REQUEST['btn'];

$custnomid=$_REQUEST['custnomid'];
$strsql="SELECT 'custcurcode'= case when ISNULL(custcurcode,0)='0' then 'IDR' else custcurcode end,custbusnpwp from tbl_customermasterperson where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$custcurcode=$rows['custcurcode'];
	}
}



if ($btn=="btnadd")
{
	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$period=$_REQUEST['period'];
	
	$strsql = "select cast(isnull(max(custfacseq),0)as int)+1 as seq 
				from tbl_CustomerFacility where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}
	
	$strsql = " INSERT INTO tbl_CustomerFacility
				(custnomid,custfacseq,custcreditneed,custcredittype,custcreditplafond,custcreditlong)
				VALUES
				('$custnomid','$seq','$creditneed','$codeproduct','$plafond','$period')
				";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");

}
else if ($btn=="btnD")
{
	$seq=$_REQUEST['seq'];
	$strsql = " delete from tbl_CustomerFacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");
	
}
else if($btn=="btnE")
{
	$seq=$_REQUEST['seq'];
	$strsql = " select custcreditneed,custcreditplafond,custcreditlong from tbl_customerfacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
		}
	}
			
	if ($custcreditneed=="1")
	{
		$tmp="where (produk_type_description like '%term%' or produk_type_description like 'FL%')";
	}
	else
	{
		$tmp="where (produk_type_description like '%Fixed%' or produk_type_description like 'TL%')";
	}
	
	
	$strsql = "select count(*) as countrow from tbl_customerfacility where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$chcekrow=$rows['countrow'];
		}
	}
	$condition="";
	if ($chcekrow!="0")
	{
		$tmpcondition="";
		$strsql = "select custcreditneed from tbl_customerfacility where custnomid='$custnomid'";
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				$tmpcondition.="'".$rows['custcreditneed']."',";
			}
		}
		$condition="where credit_need_code not in (".substr($tmpcondition,0,-1).")";
	}
	?>
	<table class="tbl100">
		<tr>
			<td>Tujuan Pengajuan Kredit</td>
			<td>
				<select id="creditneed" name="creditneed" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
					<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
					<?
						$strsql = "select * from tbl_Creditneed ".$condition;
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								
								echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Fasilitas</td>
			<td style="width:250px;" id="dtl_creditneed">
				<select id="codeproduct" name="codeproduct" style="width:100%;" class="nonmandatory">
					<option value="">-- Pilih Jenis Fasilitas --</option>
					<?
						$strsql = "select * from tbl_kodeproduk";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								if($custcredittype==$rows['produk_loan_type'])
								{
									echo '<option value="'.$rows['produk_loan_type'].'"selected="selected">'.$rows['produk_type_description'].'</option>';
								}
								else
								{
								echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
								}
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Plafond (<?echo $custcurcode?>)</td>
			<td>
				<input type="text" id="plafond" name="plafond" class="nonmandatory" onkeyup="currency(this.id);" maxlength="15"/>
			</td>
		</tr>
		<tr>
			<td>Jangka Waktu</td>
			<td>
				<input type="text" style="width:50px" id="period" name="period" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);"/> &nbsp;&nbsp; Bulan
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" id="btnadd" name="btnadd" value="save" class="button" onclick="adddtl();"/>
			</td>
		</tr>
	</table>
	<?
	
	$totalpalfond=0;
	$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,a.custcreditneed,a.custcredittype,
				c.produk_type_description,a.custcreditplafond,a.custcreditlong 
				from tbl_CustomerFacility a 
				left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
				left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
				where a.custnomid ='".$custnomid."'";
			
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		echo '
			<table class="tbl100" >
			';
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcredittype=$rows['custcredittype'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
			
			$totalpalfond+=$rows['custcreditplafond'];
			if($seq!=$rows['custfacseq'])
			{
				echo '
					<tr>
						<td style="width:180px;">Tujuan Pengajuan Kredit</td>
						<td style="width:190px;">'.$rows['credit_need_name'].'</td>
						<td rowspan="2" style="text-align:right;">&nbsp;
							<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jenis Fasilitas</td>
						<td>'.$rows['produk_type_description'].'</td>
					</tr>
					<tr>
						<td>Plafond ('.$custcurcode.')</td>
						<td>'.numberFormat($rows['custcreditplafond']).'</td>
						<td rowspan="2" style="text-align:right;">
							<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td>'.$rows['custcreditlong'].'</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				';
			}
			else 
			{
				?>				
				<tr>
					<td style="width:180px;">Tujuan Pengajuan Kredit</td>
					<td style="width:190px;">
						<select id="creditneed<?echo $seq?>" name="creditneed<?echo $seq?>" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
							<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
							<?
								$strsql2 = "select * from tbl_Creditneed where credit_need_code ='".$custcreditneed."'
											union select * from tbl_Creditneed ".$condition;
								$sqlcon2 = sqlsrv_query($conn, $strsql2);
								if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon2))
								{
									while($rows = sqlsrv_fetch_array($sqlcon2, SQLSRV_FETCH_ASSOC))
									{
										if($custcreditneed==$rows['credit_need_code'])
										{
											echo '<option value="'.$rows['credit_need_code'].'" selected="selected">'.$rows['credit_need_name'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
										}
									}
								}
							?>
						</select>	
					</td>
					<td rowspan="2" style="text-align:right;">&nbsp;
						<input type="button" id="btnY<?echo $seq?>" name="btnY<?echo $seq?>" class="button" value="YES" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jenis Fasilitas</td>
					<td id="dtl_creditneed<?echo $seq?>" style="width:190px;">
						<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
							<option value="">-- Pilih Jenis Fasilitas --</option>
							<?
								$strsql3 = "select * from tbl_kodeproduk $tmp";
								$sqlcon3 = sqlsrv_query($conn, $strsql3);
								if ( $sqlcon3 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon3))
								{
									while($rows = sqlsrv_fetch_array($sqlcon3, SQLSRV_FETCH_ASSOC))
									{
										if($custcredittype==$rows['produk_loan_type'])
										{
											echo '<option value="'.$rows['produk_loan_type'].'" selected="selected">'.$rows['produk_type_description'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
										}
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Plafond (<?echo $custcurcode?>)</td>
					<td>
						<input type="text" id="plafond<?echo $seq?>" name="plafond<?echo $seq?>" class="nonmandatory" onkeyup="currency(this.id);" maxlength="18" value="<? echo numberFormat($custcreditplafond)?>"/>
					</td>
					<td rowspan="2" style="text-align:right;">
						<input type="button" id="btnN<?echo $seq?>" name="btnN<?echo $seq?>" class="buttonneg" value="NO" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jangka Waktu</td>
					<td>
						<input type="text" style="width:50px" id="period<?echo $seq?>" name="period<?echo $seq?>" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);" value="<?echo $custcreditlong;?>"/> &nbsp;&nbsp; Bulan
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<?
			}
		}
		echo '
			</table>
			<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
		';
	}

}
else if($btn=="btnY")
{
	$seq=$_REQUEST['seq'];
	
	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$period=$_REQUEST['period'];
	
	$strsql = " update tbl_CustomerFacility set 
				custcreditneed='".$creditneed."',
				custcredittype='".$codeproduct."',
				custcreditplafond='".$plafond."',
				custcreditlong='".$period."'
				where custnomid ='".$custnomid."'
				and custfacseq ='".$seq."'
				";
				
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");

}
else if($btn=="btnN")
{
	require ("return_ajax.php");
}
else if($btn=="btnsave")
{
	require ("../../requirepage/parameter.php");
	$email=$_REQUEST['email'];
	$custnpwpno=$_REQUEST['custnpwpno'];
	$fullname=$_REQUEST['fullname'];
	$nickname=$_REQUEST['nickname'];
	$gender=$_REQUEST['gender'];
	$ktp=$_REQUEST['ktp'];
	$expiredktp=$_REQUEST['expiredktp'];
	$bop=$_REQUEST['bop'];
	$bod=$_REQUEST['bod'];
	$mothername=$_REQUEST['mothername'];
	$education=$_REQUEST['education'];
	$status=$_REQUEST['status'];
	$relation="";
	if ($status=="2")
	{
		$relation=$_REQUEST['relation'];
	}
	$duty=$_REQUEST['duty'];
	$address=str_replace("\n"," ",$_REQUEST['address']);
	$address=str_replace("\r"," ",$_REQUEST['address']);
	$address=str_replace("\r\n"," ",$_REQUEST['address']);
	$rt=$_REQUEST['rt'];
	$rw=$_REQUEST['rw'];
	$kec=$_REQUEST['kec'];
	$kel=$_REQUEST['kel'];
	$flag=$_REQUEST['flag'];
	$city=$_REQUEST['city'];
	$zip=$_REQUEST['zip'];
	$custbushp=$_REQUEST['custbushp'];
	$fixphone=$_REQUEST['fixphone'];
	$hp=$_REQUEST['hp'];
	$homestatus=$_REQUEST['homestatus'];
	$homeyearlong=$_REQUEST['homeyearlong'];
	$homemonthlong=$_REQUEST['homemonthlong'];
	if($flag=="1")
	{
		$ktpaddress=$address;
		$ktprt=$rt;
		$ktprw=$rw;
		$ktpkec=$kec;
		$ktpkel=$kel;
		$ktpcity=$city;
		$ktpzip=$zip;
	}
	else
	{
		$ktpaddress=$_REQUEST['ktpaddress'];
		$ktprt=$_REQUEST['ktprt'];
		$ktprw=$_REQUEST['ktprw'];
		$ktpkec=$_REQUEST['ktpkec'];
		$ktpkel=$_REQUEST['ktpkel'];
		$ktpcity=$_REQUEST['ktpcity'];
		$ktpzip=$_REQUEST['ktpzip'];
	}
	
	$bussname=$_REQUEST['bussname'];
	
	$bussaddress=str_replace("\n"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r\n"," ",$_REQUEST['bussaddress']);
	$busspfixphone=$_REQUEST['busspfixphone'];
	$bussnpwp=$_REQUEST['bussnpwp'];
	$busssiup=$_REQUEST['busssiup'];
	$busstdp=$_REQUEST['busstdp'];
	$custcreditstatus=$_REQUEST['custcreditstatus'];
	$lifeinsurance=$_REQUEST['lifeinsurance'];
	$otherlifeinsurance="";
	if($lifeinsurance=="JK99")
	{
		$otherlifeinsurance=$_REQUEST['otherlifeinsurance'];
	}
	$lostinsurance=$_REQUEST['lostinsurance'];
	$otherlostinsurance="";
	if($lostinsurance=="JK99")
	{
		$otherlostinsurance=$_REQUEST['otherlostinsurance'];
	}
	$introduce=$_REQUEST['introduce'];
	$lkcddate=$_REQUEST['lkcddate'];
	$recomen=$_REQUEST['recomen'];
	
	
	//lkcd
	$usaha_jenisusaha=$_REQUEST['usaha_jenisusaha'];
	$usaha_lamausahamonth=$_REQUEST['usaha_lamausahamonth'];
	$usaha_lamausahayear=$_REQUEST['usaha_lamausahayear'];
    
    
	
	$strsql = "
				UPDATE Tbl_CustomerMasterPerson
				SET custfullname = '$fullname'
				,custnpwpno = '$custnpwpno'
				,custshortname = '$nickname'
				,custsex = '$gender'
				,custktpno = '$ktp'
				,custktpexp = '$expiredktp'
				,email = '$email'
				,custboddate = '$bod'
				,custbodplace = '$bop'
				,custmothername = '$mothername'
				,custeducode = '$education'
				,custmarcode = '$status'
				,custmarname = '$relation'
				,custbushp = '$custbushp'
				,custjmltanggungan = '$duty'
				,custaddr = '$address'
				,custrt = '$rt'
				,custrw = '$rw'
				,custkel = '$kel'
				,custkec = '$kec'
				,custcity = '$city'
				,custzipcode = '$zip'
				,custtelp = '$fixphone'
				,custhp = '$hp'
				,custhomestatus = '$homestatus'
				,custhomeyearlong = '$homeyearlong'
				,custhomemonthlong = '$homemonthlong'
				,custaddrktp = '$ktpaddress'
				,custrtktp = '$ktprt'
				,custrwktp = '$ktprw'
				,custkelktp = '$ktpkel'
				,custkecktp = '$ktpkec'
				,custcityktp = '$ktpcity'
				,custzipcodektp = '$ktpzip'
				,custbusname = '$bussname'
				,custbusaddr = '$bussaddress'
				,custbustelp = '$busspfixphone'
				,custbusnpwp = '$bussnpwp'
				,custbussiup = '$busssiup'
				,custbustdp = '$busstdp'
				,custapldate = '$lkcddate'
				,custnomfrom = 'PC'
				,custnomperkenalan = '$introduce'
				,custpropendapatan = '$recomen'
				where custnomid='".$custnomid."'
				
				UPDATE Tbl_LKCDUsaha
				SET 
				usaha_jenisusaha='$usaha_jenisusaha',
				usaha_lamausahamonth='$usaha_lamausahamonth',
				usaha_lamausahayear='$usaha_lamausahayear'
				where custnomid='".$custnomid."'
				
				 ";
				 //echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	
	$strsql = "select count(*) as b
				from Tbl_Customerinsurance where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$checkrow=$rows['b'];
		}
	}
	
	
	
	
	
	
	
	
	///scoring
	///scoring
	///scoring
    $control_value="";
    $strsql = "select control_value from ms_control where control_code='MASTERLOAN'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$control_value=$rows['control_value'];
		}
	}
    
    $custproccode="";
    $strsql = "select custproccode from Tbl_CustomerMasterPerson where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custproccode=$rows['custproccode'];
		}
	}
    
    /*
	$strsql2="select * from DW_SourceHead where sh_id='".$custnomid."'";
	echo $strsql2;
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_REQUEST['query']);
	$sqlcon2 = sqlsrv_query($conn, $strsql2, $params, $cursorType);
	if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
	$rowCounter2 = sqlsrv_num_rows($sqlcon2);
	if($rowCounter2==0)
	{
		$strsqlsc=
		"
		INSERT INTO [DW_SourceHead]
           ([sh_id]
           ,[sh_pm_group]
           ,[sh_entry_time]
           ,[sh_process_time]
           ,[sh_flag])
		VALUES
           ('".$custnomid."',
           'PLATINUM',
           GETDATE(),
           GETDATE(),
           'Y')
		
		INSERT INTO DW_SourceDetail([sd_id],[sd_field],[sd_pm_id],[sd_value],[sd_value_char],[sd_value_num],[sd_score],[sd_desc])
		SELECT '".$custnomid."',pmh_id,pmh_name,'C','','0','0',''
		FROM DW_ParamMatrixHead
		
		";
		
		echo $strsqlsc."</br>";
		$params = array(&$_POST['query']);
		$stmt = sqlsrv_prepare( $conn, $strsqlsc, $params);
		if( !$stmt )
		{
		echo "Error in preparing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}

		if( !sqlsrv_execute( $stmt))
		{
		echo "Error in executing statement.\n";
		die( print_r( sqlsrv_errors(), true));
		}
		sqlsrv_free_stmt( $stmt);
	}
	
	*/
	
	$paramumur='A1';
	$strsqlaa = "SELECT * from param_age where range_start<=".$bod." and range_end>=".$bod."";
	
	echo $strsqlaa;
	$r_aa = sqlsrv_query($conn, $strsqlaa);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$paramumur = $row2['code'];
		}
	}
	
	echo "</br>".$gender."</br>";
	$DP_gender=0;
	if($gender==2)
	{
	$DP_gender=1;	
	}
	
	$DP_marital_status='MS04';
	if($status==1){$DP_marital_status="MS02";}
	if($status==2){$DP_marital_status="MS01";}
	if($status==3){$DP_marital_status="MS03";}
	
	
	$DP_jml_tanggungan="";
	$strsqlaa = "select top 1 * from param_tanggungan where cast(attribute as int)<=".$duty." order by cast(attribute as int) desc";
	
	//echo $strsqlaa;
	$r_aa = sqlsrv_query($conn, $strsqlaa);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$DP_jml_tanggungan = $row2['code'];
		}
	}
	
	if($education=="1" || $education=="2"){$DP_educational_status="E0";}
	if($education=="3"){$DP_educational_status="E1";}
	if($education=="4" || $education=="5" || $education=="6"){$DP_educational_status="E3";}
	if($education=="0"){$DP_educational_status="E4";}
	
	
	
	
	$DP_statusRumah="H3";
	if($homestatus=="P001"){$DP_statusRumah='H0';}
	if($homestatus=="P002"){$DP_statusRumah='H1';}
	if($homestatus=="B003"){$DP_statusRumah='H2';}
	
	
	
	$lamatinggalcoi=$homeyearlong.'.'.$homemonthlong;
	$DP_lamastatusrumah='L4';
	$strsql="select * from param_lama_tinggal where range_start<$lamatinggalcoi and range_end>=$lamatinggalcoi";
	echo $strsql;
	$r_aa = sqlsrv_query($conn, $strsql);
	if ( $r_aa === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($r_aa))
	{
		while($row2 = sqlsrv_fetch_array($r_aa, SQLSRV_FETCH_ASSOC))
		{
			$DP_lamastatusrumah = $row2['code'];
		}
	}
	
	echo $DP_lamastatusrumah;
	//echo $DP_lamastatusrumah."asdasdas";
	$emailcheck="E2";
	if($email!="")
	{
	$emailcheck="E1";
	}
	
	
	$checkexistingkartukredit='K2';
	$RDB_KK_anggota1='LK0';
	$DPP_job='PK03';
	$DPP_jabatan='JB01';
	$DPP_bidang_usaha='BU01';
	$DPP_status_karyawan='SP04';
	$DPP_jml_pegawai='JP01';
	$DPP_lamabekerja='LB01';
	$DPP_penghasilan='SL10';
	
	
    
    /*
	$strsqlscupdate="
	INSERT INTO DW_SourceHeadNew VALUES('".$control_value."','".$custproccode."','".$custnomid."',GETDATE(),'','0','N');
	UPDATE DW_SourceDetail set sd_value_char='$paramumur' where sd_pm_id='Age'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_gender' where sd_pm_id='Gender / Sex'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_marital_status' where sd_pm_id='Marital Status' and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_jml_tanggungan' where sd_pm_id='Tanggungan' and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_educational_status' where sd_pm_id='Education'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_statusRumah' where sd_pm_id='House'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DP_lamastatusrumah' where sd_pm_id='Lama menempati rumah'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='P01' where sd_pm_id='Phone'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$emailcheck' where sd_pm_id='Email addres'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$checkexistingkartukredit' where sd_pm_id='Memiliki Kartu Kredit Bank Lain'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$RDB_KK_anggota1' where sd_pm_id='Lama keanggotaan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_job' where sd_pm_id='Perkerjaan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_jabatan' where sd_pm_id='Jabatan'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_bidang_usaha' where sd_pm_id='Bidang Usaha'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_status_karyawan' where sd_pm_id='Status Kepegawaian'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_jml_pegawai' where sd_pm_id='Jumlah Pegawai'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='$DPP_lamabekerja' where sd_pm_id='Lama Bekerja'and sd_id='$custnomid'
	UPDATE DW_SourceDetail set sd_value_char='$DPP_penghasilan' where sd_pm_id='Salary'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='sid' where sd_pm_id='SID 'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='rating' where sd_pm_id='Rating'and sd_id='$custnomid'
	--UPDATE DW_SourceDetail set sd_value_char='akki' where sd_pm_id='AKKI (NUMBER OF NEGATIVE AKKI RECORDS)'and sd_id='$custnomid'
	
	
	UPDATE DW_SourceDetail 
	SET sd_score=b.pm_score
	from 
	DW_SourceDetail  a
	join  DW_ParamMatrix b
	on b.pm_attribute = a.sd_value_char
	and b.pm_id = a.sd_field
	where b.pm_group ='PLATINUM'
	and a.sd_id='$custnomid'

	
	
	";
    */
	//echo "</br>".$strsqlscupdate;
    $strsqlscupdate="
    delete from DW_SourceHeadNew where sh_nom_id ='".$custnomid."'
    INSERT INTO DW_SourceHeadNew VALUES('".$control_value."','".$custproccode."','".$custnomid."',GETDATE(),'','0','N','');";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsqlscupdate, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	
	
	//////////end scoring
	//////////end scoring
	//////////end scoring
	//////////end scoring
	//////////end scoring
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	if($checkrow=="0")
	{
		$strsql="INSERT INTO Tbl_Customerinsurance
				(custnomid,code_life,other_life,code_loss,other_loss)
				values
				('$custnomid','$lifeinsurance','$otherlifeinsurance','$lostinsurance','$otherlostinsurance')";
	}
	else
	{
		$strsql="UPdate Tbl_Customerinsurance set code_life='$lifeinsurance',other_life='$otherlifeinsurance',
				code_loss='$lostinsurance',other_loss='$otherlostinsurance' where custnomid='$custnomid'";
	}
	echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("../../requirepage/do_saveflow.php");	
	require ("insert_review.php");
	header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}

else if ($btn=="changekp")
{
	$seq=$_REQUEST['seq'];
	$creditneed =$_REQUEST['creditneed'];
	//echo $creditneed;
	if ($creditneed=="0")
	{
		$tmp="where (produk_type_description like '%term loan%' or produk_type_description like '%Term%' or produk_type_description like '%TL%')";
	}
	else
	{
		$tmp="where (produk_type_description like 'FL%' or produk_type_description like '%FIXED%' or produk_type_description like '%DL%' or produk_type_description like '%PRK%' or produk_type_description like '%demand%')";
	}
	?>
	<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
		<option value="">-- Pilih Jenis Fasilitas --</option>
		<?
			$strsql = "select * from Tbl_KodeProduk $tmp ";
			$sqlcon = sqlsrv_query($conn, $strsql);
			if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($sqlcon))
			{
				while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
				{
					echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
				}
			}
		?>
	</select>
	
	<?
}


?>