<?php

class SQL 
{

var $debug = false;
var $lastQuery;
var $lastResults;
var $link;

var $userID = "root";
var $userPWD = "";
var $server = "localhost";
var $dbName = "project_report";

	function connect()
	{
		$this->link = mysql_connect($this->server, $this->userID, $this->userPWD);
		if (!$this->link) 
		{
			die('Could not connect: ' . mysql_error());
		}
		//echo 'Connected successfully';
		//mysql_close($this->link);
		
		$this->executeNonQuery("USE ".$this->dbName);
		
	}
	
	function executeNonQuery($paramQuery)
	{
		$sql = $paramQuery;
		$result = mysql_query($sql);
		if (!$result) {
			die('Invalid query: ' . mysql_error());
		}		
		$this->lastQuery = $paramQuery;
	}
	
	function executeQuery($paramSelect)
	{
		$sql = $paramSelect;

		$result = mysql_query($sql);

		if (!$result) {
			echo "Could not successfully run query ($sql) from DB: " . mysql_error();
			exit;
		}

		if (mysql_num_rows($result) == 0) {
			//echo "No rows found, nothing to print so am exiting";
			//exit;
		}
		
		$rowCount = mysql_num_rows($result);

		// While a row of data exists, put that row in $row as an associative array
		// Note: If you're expecting just one row, no need to use a loop
		// Note: If you put extract($row); inside the following loop, you'll
		//       then create $userid, $fullname, and $userstatus
		$this->lastResults = array();
		while ($row = mysql_fetch_assoc($result)) 
		{
			$this->lastResults[] = $row;
		}

		mysql_free_result($result);
		
		$this->lastQuery = $paramSelect;
	}


}
?>