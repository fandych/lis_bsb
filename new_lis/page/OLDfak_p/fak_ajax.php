<?
require ("../../lib/open_con.php");
require ("../../lib/formatError.php");
$btn=$_REQUEST['btn'];

$custnomid=$_REQUEST['custnomid'];
$strsql="SELECT 'custcurcode'= case when ISNULL(custcurcode,0)='0' then 'IDR' else custcurcode end,custbusnpwp from tbl_customermasterperson where custnomid='$custnomid'";
$sqlcon = sqlsrv_query($conn, $strsql);
if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$custcurcode=$rows['custcurcode'];
	}
}



if ($btn=="btnadd")
{
	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$period=$_REQUEST['period'];
	
	$strsql = "select cast(isnull(max(custfacseq),0)as int)+1 as seq 
				from tbl_CustomerFacility where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$seq=$rows['seq'];
		}
	}
	
	$strsql = " INSERT INTO tbl_CustomerFacility
				(custnomid,custfacseq,custcreditneed,custcredittype,custcreditplafond,custcreditlong)
				VALUES
				('$custnomid','$seq','$creditneed','$codeproduct','$plafond','$period')
				";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");

}
else if ($btn=="btnD")
{
	$seq=$_REQUEST['seq'];
	$strsql = " delete from tbl_CustomerFacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");
	
}
else if($btn=="btnE")
{
	$seq=$_REQUEST['seq'];
	$strsql = " select custcreditneed,custcreditplafond,custcreditlong from tbl_customerfacility where custnomid='".$custnomid."' and custfacseq='".$seq."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
		}
	}
			
	if ($custcreditneed=="1")
	{
		$tmp="where (produk_type_description like '%term%' or produk_type_description like 'FL%')";
	}
	else
	{
		$tmp="where (produk_type_description like '%Fixed%' or produk_type_description like 'TL%')";
	}
	
	
	$strsql = "select count(*) as countrow from tbl_customerfacility where custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$chcekrow=$rows['countrow'];
		}
	}
	$condition="";
	if ($chcekrow!="0")
	{
		$tmpcondition="";
		$strsql = "select custcreditneed from tbl_customerfacility where custnomid='$custnomid'";
		$sqlcon = sqlsrv_query($conn, $strsql);
		if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlcon))
		{
			while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
			{
				$tmpcondition.="'".$rows['custcreditneed']."',";
			}
		}
		$condition="where credit_need_code not in (".substr($tmpcondition,0,-1).")";
	}
	?>
	<table class="tbl100">
		<tr>
			<td>Tujuan Pengajuan Kredit</td>
			<td>
				<select id="creditneed" name="creditneed" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
					<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
					<?
						$strsql = "select * from tbl_Creditneed ".$condition;
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								
								echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Jenis Fasilitas</td>
			<td style="width:250px;" id="dtl_creditneed">
				<select id="codeproduct" name="codeproduct" style="width:100%;" class="nonmandatory">
					<option value="">-- Pilih Jenis Fasilitas --</option>
					<?
						$strsql = "select * from tbl_kodeproduk";
						$sqlcon = sqlsrv_query($conn, $strsql);
						if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
						if(sqlsrv_has_rows($sqlcon))
						{
							while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
							{
								if($custcredittype==$rows['produk_loan_type'])
								{
									echo '<option value="'.$rows['produk_loan_type'].'"selected="selected">'.$rows['produk_type_description'].'</option>';
								}
								else
								{
								echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
								}
							}
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Plafond (<?echo $custcurcode?>)</td>
			<td>
				<input type="text" id="plafond" name="plafond" class="nonmandatory" onkeyup="currency(this.id);" maxlength="15"/>
			</td>
		</tr>
		<tr>
			<td>Jangka Waktu</td>
			<td>
				<input type="text" style="width:50px" id="period" name="period" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);"/> &nbsp;&nbsp; Bulan
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right">
				<input type="button" id="btnadd" name="btnadd" value="save" class="button" onclick="adddtl();"/>
			</td>
		</tr>
	</table>
	<?
	
	$totalpalfond=0;
	$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,a.custcreditneed,a.custcredittype,
				c.produk_type_description,a.custcreditplafond,a.custcreditlong 
				from tbl_CustomerFacility a 
				left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code 
				left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype 
				where a.custnomid ='".$custnomid."'";
			
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		echo '
			<table class="tbl100" >
			';
		while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcreditneed=$rows['custcreditneed'];
			$custcredittype=$rows['custcredittype'];
			$custcreditplafond=$rows['custcreditplafond'];
			$custcreditlong=$rows['custcreditlong'];
			
			$totalpalfond+=$rows['custcreditplafond'];
			if($seq!=$rows['custfacseq'])
			{
				echo '
					<tr>
						<td style="width:180px;">Tujuan Pengajuan Kredit</td>
						<td style="width:190px;">'.$rows['credit_need_name'].'</td>
						<td rowspan="2" style="text-align:right;">&nbsp;
							<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jenis Fasilitas</td>
						<td>'.$rows['produk_type_description'].'</td>
					</tr>
					<tr>
						<td>Plafond ('.$custcurcode.')</td>
						<td>'.numberFormat($rows['custcreditplafond']).'</td>
						<td rowspan="2" style="text-align:right;">
							<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />
						</td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td>'.$rows['custcreditlong'].'</td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
				';
			}
			else 
			{
				?>				
				<tr>
					<td style="width:180px;">Tujuan Pengajuan Kredit</td>
					<td style="width:190px;">
						<select id="creditneed<?echo $seq?>" name="creditneed<?echo $seq?>" style="width:100%;" class="nonmandatory" onchange="getkodeproduct(this.id);">
							<option value="">-- Pilih Tujuan Pengajuan Kredit --</option>
							<?
								$strsql2 = "select * from tbl_Creditneed where credit_need_code ='".$custcreditneed."'
											union select * from tbl_Creditneed ".$condition;
								$sqlcon2 = sqlsrv_query($conn, $strsql2);
								if ( $sqlcon2 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon2))
								{
									while($rows = sqlsrv_fetch_array($sqlcon2, SQLSRV_FETCH_ASSOC))
									{
										if($custcreditneed==$rows['credit_need_code'])
										{
											echo '<option value="'.$rows['credit_need_code'].'" selected="selected">'.$rows['credit_need_name'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['credit_need_code'].'">'.$rows['credit_need_name'].'</option>';
										}
									}
								}
							?>
						</select>	
					</td>
					<td rowspan="2" style="text-align:right;">&nbsp;
						<input type="button" id="btnY<?echo $seq?>" name="btnY<?echo $seq?>" class="button" value="YES" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jenis Fasilitas</td>
					<td id="dtl_creditneed<?echo $seq?>" style="width:190px;">
						<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
							<option value="">-- Pilih Jenis Fasilitas --</option>
							<?
								$strsql3 = "select * from tbl_kodeproduk $tmp";
								$sqlcon3 = sqlsrv_query($conn, $strsql3);
								if ( $sqlcon3 === false)die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($sqlcon3))
								{
									while($rows = sqlsrv_fetch_array($sqlcon3, SQLSRV_FETCH_ASSOC))
									{
										if($custcredittype==$rows['produk_loan_type'])
										{
											echo '<option value="'.$rows['produk_loan_type'].'" selected="selected">'.$rows['produk_type_description'].'</option>';
										}
										else
										{
											echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
										}
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Plafond (<?echo $custcurcode?>)</td>
					<td>
						<input type="text" id="plafond<?echo $seq?>" name="plafond<?echo $seq?>" class="nonmandatory" onkeyup="currency(this.id);" maxlength="18" value="<? echo numberFormat($custcreditplafond)?>"/>
					</td>
					<td rowspan="2" style="text-align:right;">
						<input type="button" id="btnN<?echo $seq?>" name="btnN<?echo $seq?>" class="buttonneg" value="NO" onclick="btnonclick(this.id)" />
					</td>
				</tr>
				<tr>
					<td>Jangka Waktu</td>
					<td>
						<input type="text" style="width:50px" id="period<?echo $seq?>" name="period<?echo $seq?>" class="nonmandatory" maxlength="3" onkeypress="return isNumberKey(event);" value="<?echo $custcreditlong;?>"/> &nbsp;&nbsp; Bulan
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<?
			}
		}
		echo '
			</table>
			<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
		';
	}

}
else if($btn=="btnY")
{
	$seq=$_REQUEST['seq'];
	
	$creditneed=$_REQUEST['creditneed'];
	$codeproduct=$_REQUEST['codeproduct'];
	$plafond=str_replace(",","",$_REQUEST['plafond']);
	$period=$_REQUEST['period'];
	
	$strsql = " update tbl_CustomerFacility set 
				custcreditneed='".$creditneed."',
				custcredittype='".$codeproduct."',
				custcreditplafond='".$plafond."',
				custcreditlong='".$period."'
				where custnomid ='".$custnomid."'
				and custfacseq ='".$seq."'
				";
				
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("return_ajax.php");

}
else if($btn=="btnN")
{
	require ("return_ajax.php");
}
else if($btn=="btnsave")
{
	require ("../../requirepage/parameter.php");
	$email=$_REQUEST['email'];
	$custnpwpno=$_REQUEST['custnpwpno'];
	$fullname=$_REQUEST['fullname'];
	$nickname=$_REQUEST['nickname'];
	$gender=$_REQUEST['gender'];
	$ktp=$_REQUEST['ktp'];
	$expiredktp=$_REQUEST['expiredktp'];
	$bop=$_REQUEST['bop'];
	$bod=$_REQUEST['bod'];
	$mothername=$_REQUEST['mothername'];
	$education=$_REQUEST['education'];
	$status=$_REQUEST['status'];
	$relation="";
	if ($status=="2")
	{
		$relation=$_REQUEST['relation'];
	}
	$duty=$_REQUEST['duty'];
	$address=str_replace("\n"," ",$_REQUEST['address']);
	$address=str_replace("\r"," ",$_REQUEST['address']);
	$address=str_replace("\r\n"," ",$_REQUEST['address']);
	$rt=$_REQUEST['rt'];
	$rw=$_REQUEST['rw'];
	$kec=$_REQUEST['kec'];
	$kel=$_REQUEST['kel'];
	$flag=$_REQUEST['flag'];
	$city=$_REQUEST['city'];
	$zip=$_REQUEST['zip'];
	$custbushp=$_REQUEST['custbushp'];
	$fixphone=$_REQUEST['fixphone'];
	$hp=$_REQUEST['hp'];
	$homestatus=$_REQUEST['homestatus'];
	$homeyearlong=$_REQUEST['homeyearlong'];
	$homemonthlong=$_REQUEST['homemonthlong'];
	if($flag=="1")
	{
		$ktpaddress=$address;
		$ktprt=$rt;
		$ktprw=$rw;
		$ktpkec=$kec;
		$ktpkel=$kel;
		$ktpcity=$city;
		$ktpzip=$zip;
	}
	else
	{
		$ktpaddress=$_REQUEST['ktpaddress'];
		$ktprt=$_REQUEST['ktprt'];
		$ktprw=$_REQUEST['ktprw'];
		$ktpkec=$_REQUEST['ktpkec'];
		$ktpkel=$_REQUEST['ktpkel'];
		$ktpcity=$_REQUEST['ktpcity'];
		$ktpzip=$_REQUEST['ktpzip'];
	}
	
	$bussname=$_REQUEST['bussname'];
	
	$bussaddress=str_replace("\n"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r"," ",$_REQUEST['bussaddress']);
	$bussaddress=str_replace("\r\n"," ",$_REQUEST['bussaddress']);
	$busspfixphone=$_REQUEST['busspfixphone'];
	$bussnpwp=$_REQUEST['bussnpwp'];
	$busssiup=$_REQUEST['busssiup'];
	$busstdp=$_REQUEST['busstdp'];
	$custcreditstatus=$_REQUEST['custcreditstatus'];
	$lifeinsurance=$_REQUEST['lifeinsurance'];
	$otherlifeinsurance="";
	if($lifeinsurance=="JK99")
	{
		$otherlifeinsurance=$_REQUEST['otherlifeinsurance'];
	}
	$lostinsurance=$_REQUEST['lostinsurance'];
	$otherlostinsurance="";
	if($lostinsurance=="JK99")
	{
		$otherlostinsurance=$_REQUEST['otherlostinsurance'];
	}
	$introduce=$_REQUEST['introduce'];
	$lkcddate=$_REQUEST['lkcddate'];
	$recomen=$_REQUEST['recomen'];
	
	
	//lkcd
	$usaha_jenisusaha=$_REQUEST['usaha_jenisusaha'];
	$usaha_lamausahamonth=$_REQUEST['usaha_lamausahamonth'];
	$usaha_lamausahayear=$_REQUEST['usaha_lamausahayear'];
	
	$strsql = "
				UPDATE Tbl_CustomerMasterPerson
				SET custfullname = '$fullname'
				,custnpwpno = '$custnpwpno'
				,custshortname = '$nickname'
				,custsex = '$gender'
				,custktpno = '$ktp'
				,custktpexp = '$expiredktp'
				,email = '$email'
				,custboddate = '$bod'
				,custbodplace = '$bop'
				,custmothername = '$mothername'
				,custeducode = '$education'
				,custmarcode = '$status'
				,custmarname = '$relation'
				,custbushp = '$custbushp'
				,custjmltanggungan = '$duty'
				,custaddr = '$address'
				,custrt = '$rt'
				,custrw = '$rw'
				,custkel = '$kel'
				,custkec = '$kec'
				,custcity = '$city'
				,custzipcode = '$zip'
				,custtelp = '$fixphone'
				,custhp = '$hp'
				,custhomestatus = '$homestatus'
				,custhomeyearlong = '$homeyearlong'
				,custhomemonthlong = '$homemonthlong'
				,custaddrktp = '$ktpaddress'
				,custrtktp = '$ktprt'
				,custrwktp = '$ktprw'
				,custkelktp = '$ktpkel'
				,custkecktp = '$ktpkec'
				,custcityktp = '$ktpcity'
				,custzipcodektp = '$ktpzip'
				,custbusname = '$bussname'
				,custbusaddr = '$bussaddress'
				,custbustelp = '$busspfixphone'
				,custbusnpwp = '$bussnpwp'
				,custbussiup = '$busssiup'
				,custbustdp = '$busstdp'
				,custapldate = '$lkcddate'
				,custnomfrom = 'PC'
				,custnomperkenalan = '$introduce'
				,custpropendapatan = '$recomen'
				where custnomid='".$custnomid."'
				
				UPDATE Tbl_LKCDUsaha
				SET 
				usaha_jenisusaha='$usaha_jenisusaha',
				usaha_lamausahamonth='$usaha_lamausahamonth',
				usaha_lamausahayear='$usaha_lamausahayear'
				where custnomid='".$custnomid."'
				
				 ";
				 echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	
	$strsql = "select count(*) as b
				from Tbl_Customerinsurance where custnomid='".$custnomid."'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$checkrow=$rows['b'];
		}
	}
	
	if($checkrow=="0")
	{
		$strsql="INSERT INTO Tbl_Customerinsurance
				(custnomid,code_life,other_life,code_loss,other_loss)
				values
				('$custnomid','$lifeinsurance','$otherlifeinsurance','$lostinsurance','$otherlostinsurance')";
	}
	else
	{
		$strsql="UPdate Tbl_Customerinsurance set code_life='$lifeinsurance',other_life='$otherlifeinsurance',
				code_loss='$lostinsurance',other_loss='$otherlostinsurance' where custnomid='$custnomid'";
	}
	echo $strsql;
	$params = array(&$_POST['query']);
	$stmt = sqlsrv_prepare( $conn, $strsql, $params);
	if( !$stmt )
	{
	echo "Error in preparing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}

	if( !sqlsrv_execute( $stmt))
	{
	echo "Error in executing statement.\n";
	die( print_r( sqlsrv_errors(), true));
	}
	sqlsrv_free_stmt( $stmt);
	
	require ("../../requirepage/do_saveflow.php");	
	require ("insert_review.php");
	header("location:../flow.php?userid=$userid&userpwd=$userpwd&userbranch=$userbranch&userregion=$userregion&userwfid=$userwfid");
}

else if ($btn=="changekp")
{
	$seq=$_REQUEST['seq'];
	$creditneed =$_REQUEST['creditneed'];
	//echo $creditneed;
	if ($creditneed=="0")
	{
		$tmp="where (produk_type_description like '%term loan%' or produk_type_description like '%Term%' or produk_type_description like '%TL%')";
	}
	else
	{
		$tmp="where (produk_type_description like 'FL%' or produk_type_description like '%FIXED%' or produk_type_description like '%DL%' or produk_type_description like '%PRK%' or produk_type_description like '%demand%')";
	}
	?>
	<select id="codeproduct<?echo $seq?>" name="codeproduct<?echo $seq?>" style="width:100%;" class="nonmandatory">
		<option value="">-- Pilih Jenis Fasilitas --</option>
		<?
			$strsql = "select * from Tbl_KodeProduk $tmp ";
			$sqlcon = sqlsrv_query($conn, $strsql);
			if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($sqlcon))
			{
				while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
				{
					echo '<option value="'.$rows['produk_loan_type'].'">'.$rows['produk_type_description'].'</option>';
				}
			}
		?>
	</select>
	
	<?
}


?>