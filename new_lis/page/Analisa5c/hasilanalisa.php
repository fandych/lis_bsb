<?
$a="MEMENUHI";
$b="TIDAK MEMENUHI";
//==================================================================//	
$idrkebutuhaninvestasi=0;
$strsql="SELECT isnull(SUM(cast(a as dec)),0) as 'totalkebutuhan',custnomid FROM( 
		select jenisbarang_total as 'a',custnomid from Tbl_LKCDJenisBarang2 
		union 
		select tempatusaha_total as 'a',custnomid from Tbl_LKCDTempatUsaha2 
		union 
		select rencanapembangunan_total as 'a',custnomid from Tbl_LKCDRencanaPembangunan2 
		union 
		select KebutuhanTakeover_totaloutstanding as 'a',custnomid from Tbl_LKCDKebutuhanTakeover2 
		)TBLZXX 
		where custnomid='$custnomid' 
		group by custnomid";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$idrkebutuhaninvestasi=$rows['totalkebutuhan'];
	}
}

//echo $idrkebutuaninvestasi;
//echo $idrkebutuhaninvestasi;
$hslkebutuhaninvestasi="TIDAK BUTUH";
if($idrkebutuhaninvestasi>0)
{
	$hslkebutuhaninvestasi="BUTUH";
}
//echo $hslkebutuhaninvestasi;

//==================================================================//	;

$idrwimodalkerja=$proyeksi_neraca_persediaan+$proyeksi_neraca_piutang-$proyeksi_neraca_hutang;
$hslwimodalkerja="TIDAK BUTUH";
if($idrwimodalkerja>0)
{
	$hslwimodalkerja="BUTUH";
}

$kesanalisakebutuhandebitur="TIDAK BUTUH";

if($hslwimodalkerja=="" && $hslkebutuhaninvestasi=="BUTUH")
{
	$kesanalisakebutuhandebitur="BUTUH";
}
else if($hslkebutuhaninvestasi=="BUTUH" && $hslwimodalkerja=="TIDAK BUTUH")
{
	$kesanalisakebutuhandebitur="BUTUH";
}
else if($hslkebutuhaninvestasi=="" && $hslwimodalkerja=="BUTUH")
{
	$kesanalisakebutuhandebitur="BUTUH";
}
else if($hslkebutuhaninvestasi=="TIDAK BUTUH" && $hslwimodalkerja=="BUTUH")
{
	$kesanalisakebutuhandebitur="BUTUH";
}

else if($hslkebutuhaninvestasi=="BUTUH" && $hslwimodalkerja=="BUTUH")
{
	$kesanalisakebutuhandebitur="BUTUH";
}

//==================================================================//	
$hsllamausaha=$b;
if($lkcdlamausahayear>=2)
{
		$hsllamausaha=$a;
}


$sektor_nama="";
$strsql="select * from Tbl_SektorIndustri where sektor_code ='$lkcdsektorindustri'";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$sektor_nama=$rows['sektor_nama'];
	}
}
$hslsektornama=$a;


$hslomset=$b;
if($proyeksi_proforma_omset>300000000)
{
	$hslomset=$a;
}

$hslprofit=$b;
if($proyeksi_proforma_pendapatanbersih>0)
{
	$hslprofit=$a;
}


$kescon=$b;
if($hsllamausaha==$a && $hslsektornama==$a && $hslomset==$a && $hslprofit==$a)
{
	$kescon=$a;
}
//==================================================================//	
//ANALISA CAPACITY CALON DEBITUR
$idrratio=($angsuranbaru/(($proyeksi_proforma_pendapatanbersih/12)+$angsuranbaru))*100;
$hslratio=$b;
if($idrratio<=70 && $idrratio>0)
{
	$hslratio=$a;
}


//Maksimum angsuran baru
$manmaximumangsuranbaru=(($proyeksi_proforma_pendapatanbersih/12)+$angsuranbaru)*0.7;
$idrmaximumangsuranbaru=$angsuranbaru;

$hslmaximumangsuranbaru=$b;
if($idrmaximumangsuranbaru<=$manmaximumangsuranbaru)
{
	$hslmaximumangsuranbaru=$a;
}

$idrcapratio=$manmaximumangsuranbaru/$idrmaximumangsuranbaru;

$hslcapratio=$b;

if($idrcapratio>=1)
{
$hslcapratio=$a;
}
$investasistatus=0;
$strsql="select * from tbl_CustomerFacility2 where custnomid like '$custnomid' and custcreditneed='0'";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$investasistatus=1;
}

$modalkerjastatus=0;
$strsql="select * from tbl_CustomerFacility2 where custnomid like '$custnomid' and custcreditneed='1'";
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$modalkerjastatus=1;
}

//echo $modalkerjastatus;
$totalmodalkerja=0;
if($modalkerjastatus=="1")
{
$totalmodalkerja=$idrwimodalkerja;
}

$totalinvestasi=0;
if($investasistatus=="1")
{
$totalinvestasi=$idrkebutuhaninvestasi;
}

$idrmaximumplafond=($totalmodalkerja*0.8)+($totalinvestasi*0.7);
$hslmaxplafond=$b;
if($idrmaximumplafond>=$totalplafondyangdiajukan)
{
$hslmaxplafond=$a;
}



//==================================================================//	
///analisa capital calon debitur
$totalinvestasi=0;
$strsql="select * from tbl_CustomerFacility2 where custcreditneed='0' and custnomid='$custnomid'";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totalinvestasi=$rows['custcreditplafond'];
	}
}
$totaltakeover=0;
$strsql="
select ISNULL(SUM(cast(FasilitasPinjamanBankLain_outstanding as dec)),0 ) as 'totaltakeover' from Tbl_LKCDFasilitasPinjamanBankLain2
where custnomid='$custnomid' and FasilitasPinjamanBankLain_type='TO'
";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totaltakeover=$rows['totaltakeover'];
	}
}
//$aidrkebutuhaninvestasi=$idrkebutuhaninvestasi*0.7;

if($totaltakeover>0)
{
	$prnkebutuhaninvestasi=$totalinvestasi/($totaltakeover/70/100);
	if($totalinvestasi==0)
	{
		$prnkebutuhaninvestasi=0;
	}
}
else
{
	$prnkebutuhaninvestasi=$totalinvestasi/$idrkebutuhaninvestasi*100;
	if($totalinvestasi==0)
	{
		$prnkebutuhaninvestasi=0;
	}
}


$hslprnkebutuhaninvestasi=$b;
if($prnkebutuhaninvestasi<=70 && $prnkebutuhaninvestasi>0)
{
	$hslprnkebutuhaninvestasi=$a;
}
else if($prnkebutuhaninvestasi>70)
{
	$hslprnkebutuhaninvestasi=$b;
}
else
{
	$hslprnkebutuhaninvestasi="&nbsp;";
}

$outstanding=0;
$strsql="
select isnull(sum(a),0) as 'outstanding',custnomid from (
select isnull(sum(cast(a.FasilitasPinjamanBankLain_outstanding as DEC)),0) as 'a', a.custnomid
from Tbl_LKCDFasilitasPinjamanBankLain2 a
join Tbl_LKCDJenisFasilitasBankLain b on 
a.FasilitasPinjamanBankLain_jenisfasilitas =JenisFasilitasBankLain_code
where JenisFasilitasBankLain_code='1'
and FasilitasPinjamanBankLain_jenisfasilitas<>'TO'
group by custnomid
union
select isnull(sum(cast(a.FasilitasPinjamanBankMega_outstanding as DEC)),0) as 'a', a.custnomid from Tbl_LKCDFasilitasPinjamanBankMega2 a
join Tbl_LKCDJenisFasilitasBankMega b on 
a.FasilitasPinjamanBankMega_jenisfasilitas = JenisFasilitasBankMega_code
where JenisFasilitasBankMega_code='1' or 
JenisFasilitasBankMega_code='2' or 
JenisFasilitasBankMega_code='3'
group by custnomid
union
select isnull(sum(cast(custcreditplafond  as DEC)),0) as 'a',a.custnomid from tbl_CustomerFacility2 a 
join Tbl_KodeProduk b on a.custcredittype= b.produk_loan_type
where (produk_type_description like 'FL%' or
produk_type_description like '%FIXED%' or
produk_type_description like '%DL%' or
produk_type_description like '%PRK%')
group by custnomid
) tblxx
where custnomid like '$custnomid'
group by custnomid
";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$outstanding=$rows['outstanding'];
	}
}
$idrkebutuhanmodalkerja=$outstanding;
//echo $idrwimodalkerja;
$prnkebutuhanmodalkerja=$idrkebutuhanmodalkerja/$idrwimodalkerja*100;
$prnkebutuhanmodalkerja=round($prnkebutuhanmodalkerja).' %';
$totalmodalkerja=0;
$strsql="select * from tbl_CustomerFacility2 where custcreditneed='1' and custnomid='$custnomid'";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totalmodalkerja=$rows['custcreditplafond'];
	}
}
if($totalmodalkerja==0)
{
	$prnkebutuhanmodalkerja=0;
}

if($prnkebutuhanmodalkerja<=80 && $prnkebutuhanmodalkerja>0)
{
	$hslkebutuhanmodalkerja=$a;
}
else if($prnkebutuhanmodalkerja>80)
{
	$hslkebutuhanmodalkerja=$b;
}
else
{
	$hslkebutuhanmodalkerja="&nbsp;";
}

$kescapital=$b;
if(($hslkebutuhanmodalkerja==$a && $hslprnkebutuhaninvestasi=="&nbsp;") || ($hslkebutuhanmodalkerja=="&nbsp;" && $hslprnkebutuhaninvestasi==$a))
{
$kescapital=$a;
}
else if($hslkebutuhanmodalkerja=="&nbsp;" && $hslprnkebutuhaninvestasi=="&nbsp;")
{
$kescapital="&nbsp;";
}


$kescapacity=$b;
if($kescapital==$a && $hslmaxplafond==$a && $hslcapratio==$a && $hslmaximumangsuranbaru==$a)
{
$kescapacity=$a;
}

//==================================================================//	
//analisa char
$tmpcheckbankmega="";
$strsql="select 'a' =
case when LKCDKolektibilitasTidakLancarBankMega_berapakali='' then 0
else LKCDKolektibilitasTidakLancarBankMega_berapakali end
from Tbl_LKCDKolektibilitasTidakLancarBankMega2 where custnomid ='$custnomid'";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		if($rows['a']>1)
		{
			//0 artinya lebih dari 1
			$tmpcheckbankmega=0;
			break;
		}
	}
}

$tmpcheckbanklain="";
$strsql="select 'a' =
case when LKCDKolektibilitasTidakLancarBankLain_berapakali='' then
0
else LKCDKolektibilitasTidakLancarBankLain_berapakali end
from Tbl_LKCDKolektibilitasTidakLancarBankLain2 where custnomid ='$custnomid'
and LKCDKolektibilitasTidakLancarBankLain_type<>'TO'";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		if($rows['a']>1)
		{
			//0 artinya lebih dari 1
			$tmpcheckbanklain=0;
			break;
		}
		else
		{
			
		}
	}
}

if($tmpcheckbankmega=="0" || $tmpcheckbanklain=="0")
{
	$hslbic="MEMENUHI DENGAN SYARAT";
}
else if($tmpcheckbankmega=="" && $tmpcheckbanklain=="")
{
	$hslbic=$a;
}


$TradeCheck_ketepatankomunitas1="";
$TradeCheck_ketepatankomunitas2="";
$TradeCheck_ketepatanpemasok1="";
$TradeCheck_ketepatanpemasok2="";
$TradeCheck_infonegatifkomunitas1="";
$TradeCheck_infonegatifkomunitas2="";
$TradeCheck_infonegatifpemasok1="";
$TradeCheck_infonegatifpemasok2="";
$strsql="select 
			TradeCheck_ketepatankomunitas1,TradeCheck_ketepatankomunitas2,
			TradeCheck_ketepatanpemasok1,TradeCheck_ketepatanpemasok2,
			TradeCheck_infonegatifkomunitas1,TradeCheck_infonegatifkomunitas2,
			TradeCheck_infonegatifpemasok1,TradeCheck_infonegatifpemasok2
			from Tbl_LKCDTradeCheck2
			where custnomid='$custnomid'";
		//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$TradeCheck_ketepatankomunitas1=$rows['TradeCheck_ketepatankomunitas1'];
		$TradeCheck_ketepatankomunitas2=$rows['TradeCheck_ketepatankomunitas2'];
		$TradeCheck_ketepatanpemasok1=$rows['TradeCheck_ketepatanpemasok1'];
		$TradeCheck_ketepatanpemasok2=$rows['TradeCheck_ketepatanpemasok2'];
		$TradeCheck_infonegatifkomunitas1=$rows['TradeCheck_infonegatifkomunitas1'];
		$TradeCheck_infonegatifkomunitas2=$rows['TradeCheck_infonegatifkomunitas2'];
		$TradeCheck_infonegatifpemasok1=$rows['TradeCheck_infonegatifpemasok1'];
		$TradeCheck_infonegatifpemasok2=$rows['TradeCheck_infonegatifpemasok2'];
	}

}


$hslkomunitas=$b;
if(($TradeCheck_ketepatankomunitas1=="SELALU TEPAT" && $TradeCheck_infonegatifkomunitas1=="TIDAK") || ($TradeCheck_ketepatankomunitas2=="SELALU TEPAT" && $TradeCheck_infonegatifkomunitas2=="TIDAK"))
{
	$hslkomunitas=$a;
}

$hslsupp=$b;
if(($TradeCheck_ketepatanpemasok1=="SELALU TEPAT" && $TradeCheck_infonegatifpemasok1=="TIDAK") || ($TradeCheck_ketepatanpemasok1=="SELALU TEPAT" && $TradeCheck_infonegatifpemasok1=="TIDAK"))
{
	$hslsupp=$a;
}


$keschar=$b;
if($hslbic=="MEMENUHI DENGAN SYARAT" && $hslsupp==$a && $hslkomunitas==$a)
{
	$keschar="MEMENUHI DENGAN SYARAT";
}
else if($hslbic==$a && $hslsupp==$a && $hslkomunitas==$a)
{
	$keschar=$a;
}

//=======================////

$col_id='';
$strsql="SELECt col_id FROM Tbl_Cust_MasterCol where flaginsert='1' and flagdelete='0'
and ap_lisregno like '$custnomid'";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($conn, $strsql, $postsql);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$col_id.="'".$rows['col_id']."',";
	}
}

//echo $col_id;
$totallikuidasi=0;
$strsql="select sum(likuidasi) as 'totallikuidasi' from(
select convert(int, _nilai_likuidasi) as 'likuidasi', _collateral_id as 'noapp' from appraisal_lnd_value union 
select convert(int, _nilai_likuidasi_imb) as 'likuidasi', _collateral_id as 'noapp' from appraisal_tnb_value union 
select convert(int, _nilai_likuidasi) as 'likuidasi', _collateral_id as 'noapp' from appraisal_vhc_value
) tblxx
where noapp in (".substr($col_id,0,-1).")";
//echo $strsql;
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($connapr, $strsql, $postsql);
if($connapr==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totallikuidasi=$rows['totallikuidasi'];
	}
}

$strsql = "
select sum(a) as a from (
SELECT isnull(sum(cast(cash_nilai as int)),0) as a  FROM tbl_col_cash where col_id in (".substr($col_id,0,-1).")
union 
SELECT isnull(sum(cast(col_nilaijaminan as int)),0) as a FROM tbl_col_lainnya where col_id in (".substr($col_id,0,-1).")
) tblxx ";
$postsql = array(&$_POST['query']);
$execsql = sqlsrv_query($connapr, $strsql, $postsql);
if($connapr==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($execsql))
{
	$rowcount = sqlsrv_num_rows($execsql);
	while($rows = sqlsrv_fetch_array( $execsql, SQLSRV_FETCH_ASSOC))
	{
		$totallikuidasi=$totallikuidasi+$rows['a'];
	}
}

//echo $totallikuidasi;
//likudisai
$nilailikuidasilkcd=$totallikuidasi;

$prnlikuidasi=$nilailikuidasilkcd/$totalplafondyangdiajukan*100;
$hsllikuidasi=$b;

if($prnlikuidasi>=100)
{
	$hsllikuidasi=$a;
}

////===============

?>