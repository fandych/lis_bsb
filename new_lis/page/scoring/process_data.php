<?php
# Copyright 2013
# Written by : PPC Team
# Leader     : Budi Hartoyo

	include ("../../lib/formatError.php");
  $programname = "txnscoringproses.php";

	require ("../../lib/open_con.php");

//$arralldb = array("db_new_lis");

$dbmustcheck = "db_lis_bsb";
$tempnomid=$_REQUEST['custnomid'];
$custcredittype="";
$custjenispembelian ="";
$custtarid ="";

             $tsql = "SELECT custcredittype FROM $dbmustcheck.dbo.tbl_CustomerFacility2
                    WHERE custnomid='$tempnomid'";
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $custcredittype = $row[0];
                }
             }
             sqlsrv_free_stmt( $sqlConn );

             $tsql = "SELECT jenis_pembelian FROM $dbmustcheck.dbo.Tbl_Scoring
                    WHERE custnomid='$tempnomid'";
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $custjenispembelian = $row[0];
                }
             }
             sqlsrv_free_stmt( $sqlConn );
			 
			 $tsql = "SELECT custtarid FROM $dbmustcheck.dbo.Tbl_CustomerMasterPerson2
                    WHERE custnomid='$tempnomid'";
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $custtarid = $row[0];
                }
             }
             sqlsrv_free_stmt( $sqlConn );
			 
			 
//TRANSLATOR JENIS DEBITUR TETAP ATAU TIDAK TETAP			 
if($custtarid == "1") {
	$custtarid = "PT";
}
else if($custtarid == "2") {
	$custtarid = "PTT";
}
else {
	//echo "CODE CUSTTARID TIDAK ADA DIDALAM PARAMETER HARAP DISETTING ULANG !!!";
}


// TRANSLATOR JENIS PEMBELIAN
switch($custjenispembelian) {

  case "RT" :
  $custjenispembelian = "T";
  break;

  case "AP" :
  $custjenispembelian = "A";
  break;

  default :
  $custjenispembelian = "";
  break;
}
			 
// TRANSLATOR KODE PRODUK
switch($custcredittype) {

  case "KF" :
  $custcredittype = "FLPP";
  break;

  case "KBA" :
  $custcredittype = "KBA";
  break;

  case "KKR" :
  $custcredittype = "KBA";
  break;

  case "KBE" :
  $custcredittype = "KBA";
  break;

  case "KKTO" :
  $custcredittype = "KBA";
  break;

  default :
  $custcredittype = "PNS";
  break;
}



//$vartemp = "PL" . "|" . $_REQUEST['custproccode']  . "|" . $_REQUEST['custnomid'] . "|";
if ($custcredittype == "FLPP")
{
   $scoringid = $_REQUEST['custproccode'] . "_" . $custcredittype . "_" . $custjenispembelian . "_" . $custtarid;
   $vartemp = $scoringid . "|" . $scoringid . "|" . $_REQUEST['custnomid'] . "|";
}
else
{
   $scoringid = $_REQUEST['custproccode'] . "_" . $custcredittype . "_" . $custtarid;	
   $scoringid2 = $_REQUEST['custproccode'] . "_" . $custcredittype . "_" . $custtarid;	
   $vartemp = $scoringid . "|" . $scoringid2 . "|" . $_REQUEST['custnomid'] . "|";
}
$arrsourcehead = array($vartemp);
//echo $vartemp;exit;
   for ($hitsource=0;$hitsource<count($arrsourcehead);$hitsource++)
   {
	 		$arrsplithead=explode("|",$arrsourcehead[$hitsource]);
   	  $shheadid = $arrsplithead[0];
   	  $shpmgroup = $arrsplithead[1];
   	  $shid = $arrsplithead[2];
   	  $sdscore = 0;
   	  $totalscore = 0;
   	  $sddesc = "";
      echo "$shid - $shheadid <BR>\n";

      $log = "DELETE From $dbmustcheck.dbo.DW_SourceDetailNew
                     WHERE sd_head_id='$shheadid'
                     AND sd_pm_group='$shpmgroup'
                     AND sd_nom_id='$shid'";

      $rslog = sqlsrv_query($conn,$log);

	   $dwheadname = "";
	   $dwdbname = "";
     $tsql = "SELECT head_name, head_db_name
		   									FROM DB_DATAWARE.dbo.DW_ParamHead
		   									WHERE head_id='$shheadid'";
                                            //echo $tsql;
     $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
     $params = array(&$_POST['query']);

     $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

     if ( $sqlConn === false)
        die( FormatErrors( sqlsrv_errors() ) );

     if(sqlsrv_has_rows($sqlConn))
     {
        $rowCount = sqlsrv_num_rows($sqlConn);
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
        {
	         $dwheadname = $row[0];
	         $dwdbname = $row[1];
        }
     }
     sqlsrv_free_stmt( $sqlConn );

      $tsqlawal = "SELECT * FROM DB_DATAWARE.dbo.DW_ParamMatrixHead
                   WHERE pmh_head_id='$shheadid'
                   AND SUBSTRING(pmh_flag,1,1)='Y'";
                   //echo $tsqlawal . "<BR>";
      $cursorTypeawal = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
      $paramsawal = array(&$_POST['query']);

      $sqlConnawal = sqlsrv_query($conn, $tsqlawal, $paramsawal, $cursorTypeawal);

      if ( $sqlConnawal === false)
         die( FormatErrors( sqlsrv_errors() ) );

      if(sqlsrv_has_rows($sqlConnawal))
      {
         $rowCountawal = sqlsrv_num_rows($sqlConnawal);
         while( $rowawal = sqlsrv_fetch_array( $sqlConnawal, SQLSRV_FETCH_NUMERIC))
         {
         	 $arrfield=explode(".",$rowawal[7]);
      	   $sdtable = $arrfield[0];
      	   $sdfield = $arrfield[1];
      	   $sdpmid = $rowawal[1];
      	   $sdpmhname = $rowawal[2];
      	   $sdpmhtype = $rowawal[4];
      	   $sdpmhtable = $rowawal[5];
      	   $sdvalue = "";

           $custnomidfield = "custnomid";
           $tsql = "SELECT COLUMN_NAME from $dwdbname.INFORMATION_SCHEMA.COLUMNS
                    where TABLE_NAME='$sdtable'
                    AND COLUMN_NAME like '%custnomid%'";
           $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
           $params = array(&$_POST['query']);
           $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

           if ( $sqlConn === false)
              die( FormatErrors( sqlsrv_errors() ) );

           if(sqlsrv_has_rows($sqlConn))
           {
              $rowCount = sqlsrv_num_rows($sqlConn);
              while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
              {
              	    $custnomidfield = $row[0];
              }
           }
           sqlsrv_free_stmt( $sqlConn );


           $tsql = "SELECT $sdfield FROM $dwdbname.dbo.$sdtable
                   WHERE $custnomidfield='$shid'";
                   echo $tsql . "<BR>";
           $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
           $params = array(&$_POST['query']);
           $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

           if ( $sqlConn === false)
              die( FormatErrors( sqlsrv_errors() ) );

           if(sqlsrv_has_rows($sqlConn))
           {
              $rowCount = sqlsrv_num_rows($sqlConn);
              while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
              {
              	    $sdvalue = $row[0];
              }
           }
		   else {
			   $sdvalue = "";
		   }
           sqlsrv_free_stmt( $sqlConn );
		   
		   

// FILL

           if ($sdpmhtype == "FILL")
           {
           	 $sdscore = 0;
           	 if ($sdvalue != "")
           	 {
           	 	 $varvalid = "Y";
           	 }
           	 else
           	 {
           	 	 $varvalid = "N";
           	 }
             $tsql = "SELECT pm_score FROM DB_DATAWARE.dbo.DW_ParamMatrix
                    WHERE pm_head_id='$shheadid'
                    AND pm_group='$shpmgroup'
                    AND pm_id='$sdpmid'
                    AND pm_attribute='$varvalid'";
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $sdscore = $row[0];
      	           $sddesc = $sdpmhname;
                }
             }
			 else {
				 
			 }
             sqlsrv_free_stmt( $sqlConn );

             $log = "INSERT INTO $dbmustcheck.dbo.DW_SourceDetailNew VALUES('$shheadid',
                     '$shpmgroup','$shid','$rowawal[7]','$sdpmid','$sdvalue','$sdscore','$sddesc')";
             $rslog = sqlsrv_query($conn,$log);
           }
// END FILL

// RANGE
           if ($sdpmhtype == "RANGE")
           {
			 if ($sdvalue == "")
           	 {
           	 	 $sdvalue = 0;
           	 }
			 
			 $sdvalue = str_replace(',','',$sdvalue);
           	 $sdscore = 0;
             $tsql = "SELECT pm_score FROM DB_DATAWARE.dbo.DW_ParamMatrix
                    WHERE pm_head_id='$shheadid'
                    AND pm_group='$shpmgroup'
                    AND pm_id='$sdpmid'
                    AND '$sdvalue'>=pm_range_one
                    AND '$sdvalue'<=pm_range_two";
					
					echo $tsql;
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $sdscore = $row[0];
      	           $sddesc = $sdpmhname;
                }
             }
             sqlsrv_free_stmt( $sqlConn );

             $log = "INSERT INTO $dbmustcheck.dbo.DW_SourceDetailNew VALUES('$shheadid',
                     '$shpmgroup','$shid','$rowawal[7]','$sdpmid','$sdvalue','$sdscore','$sddesc')";
             $rslog = sqlsrv_query($conn,$log);
           }
// END RANGE

// DATE RANGE
           if ($sdpmhtype == "DATER")
           {
           	 $oldsdvalue = $sdvalue;
        	   $sdvalue = Date('Y') - substr($sdvalue,0,4);

           	 $sdscore = 0;
             $tsql = "SELECT pm_score FROM DB_DATAWARE.dbo.DW_ParamMatrix
                    WHERE pm_head_id='$shheadid'
                    AND pm_group='$shpmgroup'
                    AND pm_id='$sdpmid'
                    AND '$sdvalue'>=pm_range_one
                    AND '$sdvalue'<=pm_range_two";
             $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params = array(&$_POST['query']);

             $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

             if ( $sqlConn === false)
                die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn))
             {
                $rowCount = sqlsrv_num_rows($sqlConn);
                while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
                {
      	           $sdscore = $row[0];
      	           $sddesc = $sdpmhname;
                }
             }
             sqlsrv_free_stmt( $sqlConn );

             $sdvalue = $oldsdvalue;

             $log = "INSERT INTO $dbmustcheck.dbo.DW_SourceDetailNew VALUES('$shheadid',
                     '$shpmgroup','$shid','$rowawal[7]','$sdpmid','$sdvalue','$sdscore','$sddesc')";
             $rslog = sqlsrv_query($conn,$log);
           }
// END DATE RANGE

// PARAM
           if ($sdpmhtype == "PARAM")
           {
           	 $sdscore = 0;
             $tsql2 = "SELECT *
		   									FROM $dwdbname.dbo.$sdpmhtable";
             $cursorType2 = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
             $params2 = array(&$_POST['query']);

             $sqlConn2 = sqlsrv_query($conn, $tsql2, $params2, $cursorType2);

             if ( $sqlConn2 === false)
               die( FormatErrors( sqlsrv_errors() ) );

             if(sqlsrv_has_rows($sqlConn2))
             {
               $rowCount2 = sqlsrv_num_rows($sqlConn2);
               while( $row2 = sqlsrv_fetch_array( $sqlConn2, SQLSRV_FETCH_NUMERIC))
               {
            	    if ($row2[0] == $sdvalue)
            	    {
                     $tsqltemp = "SELECT pm_score
		   									FROM DB_DATAWARE.dbo.DW_ParamMatrix
		   									WHERE pm_head_id='$shheadid'
		   									AND pm_group='$shpmgroup'
		   									AND pm_id='$sdpmid'
		   									AND pm_attribute='$sdvalue'";
                     $cursorTypetemp = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
                     $paramstemp = array(&$_POST['query']);

                     $sqlConntemp = sqlsrv_query($conn, $tsqltemp, $paramstemp, $cursorTypetemp);

                     if ( $sqlConntemp === false)
                       die( FormatErrors( sqlsrv_errors() ) );

                     if(sqlsrv_has_rows($sqlConntemp))
                     {
                       $rowCounttemp = sqlsrv_num_rows($sqlConntemp);
                       while( $rowtemp = sqlsrv_fetch_array( $sqlConntemp, SQLSRV_FETCH_NUMERIC))
                       {
            	            $sdscore = $rowtemp[0];
      	                  $sddesc = $sdpmhname;
                       }
                     }
                     sqlsrv_free_stmt( $sqlConntemp );
            	    }
               }
             }
            sqlsrv_free_stmt( $sqlConn2 );


             $log = "INSERT INTO $dbmustcheck.dbo.DW_SourceDetailNew VALUES('$shheadid',
                     '$shpmgroup','$shid','$rowawal[7]','$sdpmid','$sdvalue','$sdscore','$sddesc')";
             $rslog = sqlsrv_query($conn,$log);

           }
// END PARAM

          $totalscore += $sdscore;
         }
      }
      sqlsrv_free_stmt( $sqlConnawal );

// END PROCCESS

     $groupminvalue = "";
     $groupmaxvalue = "";
     $tsql = "SELECT group_min_value,group_max_value
		   									FROM DB_DATAWARE.dbo.DW_ParamGroup
		   									WHERE group_head_id='$shheadid'
		   									AND group_id='$shpmgroup'";
     $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
     $params = array(&$_POST['query']);

     $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

     if ( $sqlConn === false)
        die( FormatErrors( sqlsrv_errors() ) );

     if(sqlsrv_has_rows($sqlConn))
     {
        $rowCount = sqlsrv_num_rows($sqlConn);
        while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
        {
	         $groupminvalue = $row[0];
	         $groupmaxvalue = $row[1];
        }
     }
     sqlsrv_free_stmt( $sqlConn );

     $analisalayak = "";

     if ($totalscore < $groupminvalue)
     {
     	  $analisalayak = "Dinyatakan <b>Tidak Layak</b> Mendapatkan $dwheadname $shpmgroup";
        $rekomendasi = "";
        $tsql = "SELECT group_id, group_min_value, group_max_value
		   									FROM DB_DATAWARE.dbo.DW_ParamGroup
		   									WHERE group_head_id='$shheadid'
		   									AND group_min_value<='$totalscore'
		   									AND group_id<>'$shpmgroup'";
        $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
        $params = array(&$_POST['query']);

        $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

        if ( $sqlConn === false)
           die( FormatErrors( sqlsrv_errors() ) );

        if(sqlsrv_has_rows($sqlConn))
        {
           $rowCount = sqlsrv_num_rows($sqlConn);
           while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
           {
        	    if ($rekomendasi != "")
        	    {
	               $rekomendasi .= "<BR>";
        	    }
	            $rekomendasi .= "- <b>$row[0]</b> ($row[1] - $row[2])";
           }
        }
        sqlsrv_free_stmt( $sqlConn );
     }
     else
     {
     	  $analisalayak = "Dinyatakan <b>Layak</b> Mendapatkan $dwheadname $shpmgroup";
        $tsql = "SELECT reward_value
		   									FROM DB_DATAWARE.dbo.DW_ParamGroupReward
		   									WHERE reward_head_id='$shheadid'
		   									AND reward_group_id='$shpmgroup'
		   									AND reward_start<='$totalscore'
		   									AND reward_end>='$totalscore'";
        $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
        $params = array(&$_POST['query']);
        $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
        if ( $sqlConn === false)
           die( FormatErrors( sqlsrv_errors() ) );

        if(sqlsrv_has_rows($sqlConn))
        { 
           $rowCount = sqlsrv_num_rows($sqlConn);
           while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
           {
	               $analisalayak = " DINYATAKAN <u><b>" .($row[0]) . "</b></u>";
           }
        }
        sqlsrv_free_stmt( $sqlConn );

        $rekomendasi = "";
        $tsql = "SELECT group_id, group_min_value, group_max_value
		   									FROM DB_DATAWARE.dbo.DW_ParamGroup
		   									WHERE group_head_id='$shheadid'
		   									AND group_max_value<='$totalscore'
		   									AND group_id<>'$shpmgroup'";
        $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
        $params = array(&$_POST['query']);
        $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

        if ( $sqlConn === false)
           die( FormatErrors( sqlsrv_errors() ) );

        if(sqlsrv_has_rows($sqlConn))
        {
           $rowCount = sqlsrv_num_rows($sqlConn);
           while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
           {
        	    if ($rekomendasi != "")
        	    {
	               $rekomendasi .= "<BR>";
        	    }
	            $rekomendasi .= "- <b>$row[0]</b> ($row[1] - $row[2])";
           }
        }
        sqlsrv_free_stmt( $sqlConn );
     }
     $shdesc = $analisalayak;
//     if ($rekomendasi != "")
//     {
//        $shdesc .= "<BR>" . $rekomendasi;
//     }
//echo $shdesc;exit;
/*	 $log = "UPDATE $dbmustcheck.dbo.DW_SourceHeadNew set sh_process_time=GETDATE(),
                     sh_score='$totalscore', sh_flag='Y',
                     sh_desc='$shdesc'
                     WHERE sh_head_id='$shheadid'
                     AND sh_pm_group='$shpmgroup'
                     AND sh_nom_id='$shid'";
             $rslog = sqlsrv_query($conn,$log);*/

		  $log = "UPDATE $dbmustcheck.dbo.DW_SourceHeadNew set sh_process_time=GETDATE(),
                     sh_score='$totalscore', sh_flag='Y',
                     sh_desc='$shdesc'
                     WHERE sh_nom_id='$shid'";
             $rslog = sqlsrv_query($conn,$log);

			 // END PROCCESS
   }
   echo "Finisih ALL";

$custnomid = $_REQUEST['custnomid'];
$userwfid = $_REQUEST['userwfid'];
$userpermission = $_REQUEST['userpermission'];
$buttonaction = $_REQUEST['buttonaction'];

header("location:./flow_scoring.php?custnomid=$custnomid&userwfid=SCORE&userpermission=$userpermission&buttonaction=$buttonaction");

  require ("bin/lib/close_con.php");
  exit;

exit;
