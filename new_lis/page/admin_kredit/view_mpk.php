<?php

include ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require ("../../requirepage/parameter.php");
require("../../query/cmp_query.php");

//echo $userpermission;

$custproccode = "";
$currCode = "";

//GET CURRENCY
   $tsql = "SELECT * FROM Tbl_CustomerMasterPerson2 WHERE custnomid='$custnomid'";//echo $tsql;
   $cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
   $params = array(&$_POST['query']);

   $sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);

   if ( $sqlConn === false)
      die( FormatErrors( sqlsrv_errors() ) );

   if(sqlsrv_has_rows($sqlConn))
   {
      $rowCount = sqlsrv_num_rows($sqlConn);
      while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
      {		   
	  		$custproccode = $row['custproccode'];
			$currCode = $row['custcurcode'];
      }
   }
   sqlsrv_free_stmt( $sqlConn );
   
if($currCode == "")
{
	$currCode = "IDR";
} 



$pk_no="";
$select_pk="select pknumber from tbl_newpk where custnomid='$custnomid'";
$cursortype2_pk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_pk = array(&$_POST['query']);
$sqlConn_pk = sqlsrv_query($conn, $select_pk, $params_pk, $cursortype2_pk);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_pk))
{
	$rowCount_pk = sqlsrv_num_rows($sqlConn_pk);
	while($row_pk = sqlsrv_fetch_array( $sqlConn_pk, SQLSRV_FETCH_ASSOC))
		{
			$pk_no=$row_pk['pknumber'];
		}
}
sqlsrv_free_stmt( $sqlConn_pk );



$branch_name="";
$sql_branch="select * from Tbl_Branch where branch_code='$userbranch' ";
$cursortype_branch = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params_branch = array(&$_POST['query']);
$sqlConn_branch = sqlsrv_query($conn, $sql_branch, $params_branch, $cursortype_branch);
if($conn==false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn_branch))
	{
		$rowCount_branch = sqlsrv_num_rows($sqlConn_branch);
		while($row_branch = sqlsrv_fetch_array( $sqlConn_branch, SQLSRV_FETCH_ASSOC))
			{
			$branch_name=$row_branch['branch_name'];
			}						
	}
sqlsrv_free_stmt( $sqlConn_branch );


	$mpk_kepada="";
	$mpk_dari="";
	$mpk_tgl="";
	$mpk_tgl_lfk="";
	$mpk_komite_kredit="";
	$mpk_no_lkk="";
	$mpk_nominal_pencairan1=0;
	$mpk_tgl_valuta_pencairan1="";
	$mpk_keterangan1="";
	$mpk_nominal_pencairan2=0;
	$mpk_tgl_valuta_pencairan2="";
	$mpk_keterangan2="";
	$mpk_tgl_notaris="";
	$mpk_nama_notaris="";
	$mpk_provisi_rpk=0;
	$mpk_biaya_admin_kredit=0;
	$mpk_biaya_notaris=0;
	$mpk_total_biaya=0;
	$mpk_atas_nama_bm="";
	$mpk_no_rek_bm="";
	$mpk_tgl_valuta_bm="";
	$mpk_nominal_bm1=0;
	$mpk_ttun_no_bm="";
	$mpk_jangka_waktu1="";
	$mpk_jangka_waktu2="";
	$mpk_nominal_bm2=0;
	$mpk_dana="";
	$mpk_nama_bank="";
	$mpk_atas_nama="";
	$mpk_no_rek="";
	$mpk_tgl_valuta="";
	$mpk_nominal=0;
	$mpk_ppkspk="";
	$mpk_bagian_marketing="";
	$mpk_administrasi_kredit="";
	$mpk_loan_processing="";
	
	
	$mpk_x_angsuran="0";
	$mpk_asuransi_kredit="0";
	$mpk_asuransi_kebakaran="0";
	
	$mpk_debet_atasnama="";
	$mpk_debet_norek="";

	$mpk_last_branch="";
	$mpk_last_remarks="";

	$mpk_no_suratpermohonan="0";

	$sql_mpk="select * from tbl_master_mpk where custnomid='$custnomid' ";
	$cursortype_mpk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_mpk= array(&$_POST['query']);
	$sqlConn_mpk = sqlsrv_query($conn, $sql_mpk, $params_mpk, $cursortype_mpk);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_mpk))
	{
		$rowCount_mpk = sqlsrv_num_rows($sqlConn_mpk);
		while($row_mpk = sqlsrv_fetch_array( $sqlConn_mpk, SQLSRV_FETCH_ASSOC))
			{
			$mpk_kepada=$row_mpk['mpk_kepada'];				
			$mpk_dari=$row_mpk['mpk_dari'];				
			$mpk_tgl=$row_mpk['mpk_tgl'];				
			$mpk_tgl_lfk=$row_mpk['mpk_tgl_lfk'];				
			$mpk_komite_kredit=$row_mpk['mpk_komite_kredit'];				
			$mpk_no_lkk=$row_mpk['mpk_no_lkk'];				
			$mpk_nominal_pencairan1=$row_mpk['mpk_nominal_pencairan1'];			
			$mpk_tgl_valuta_pencairan1=$row_mpk['mpk_tgl_valuta_pencairan1'];			
			$mpk_keterangan1=$row_mpk['mpk_keterangan1'];				
			$mpk_nominal_pencairan2=$row_mpk['mpk_nominal_pencairan2'];				
			$mpk_tgl_valuta_pencairan2=$row_mpk['mpk_tgl_valuta_pencairan2'];				
			$mpk_keterangan2=$row_mpk['mpk_keterangan2'];				
			$mpk_tgl_notaris=$row_mpk['mpk_tgl_notaris'];				
			$mpk_nama_notaris=$row_mpk['mpk_nama_notaris'];				
			$mpk_provisi_rpk=$row_mpk['mpk_provisi_rpk'];				
			$mpk_biaya_admin_kredit=$row_mpk['mpk_biaya_admin_kredit'];			
			$mpk_biaya_notaris=$row_mpk['mpk_biaya_notaris'];				
			$mpk_total_biaya=$row_mpk['mpk_total_biaya'];				
			$mpk_atas_nama_bm=$row_mpk['mpk_atas_nama_bm'];				
			$mpk_no_rek_bm=$row_mpk['mpk_no_rek_bm'];				
			$mpk_tgl_valuta_bm=$row_mpk['mpk_tgl_valuta_bm'];				
			$mpk_nominal_bm1=$row_mpk['mpk_nominal_bm1'];				
			$mpk_ttun_no_bm=$row_mpk['mpk_ttun_no_bm'];				
			$mpk_jangka_waktu1=$row_mpk['mpk_jangka_waktu1'];				
			$mpk_jangka_waktu2=$row_mpk['mpk_jangka_waktu2'];				
			$mpk_nominal_bm2=$row_mpk['mpk_nominal_bm2'];				
			$mpk_dana=$row_mpk['mpk_dana'];				
			$mpk_nama_bank=$row_mpk['mpk_nama_bank'];				
			$mpk_atas_nama=$row_mpk['mpk_atas_nama'];				
			$mpk_no_rek=$row_mpk['mpk_no_rek'];				
			$mpk_tgl_valuta=$row_mpk['mpk_tgl_valuta'];				
			$mpk_nominal=$row_mpk['mpk_nominal'];				
			$mpk_ppkspk=$row_mpk['mpk_ppkspk'];				
			$mpk_bagian_marketing=$row_mpk['mpk_bagian_marketing'];			
			$mpk_administrasi_kredit=$row_mpk['mpk_administrasi_kredit'];			
			$mpk_loan_processing=$row_mpk['mpk_loan_processing'];		
			
			$mpk_x_angsuran=$row_mpk['mpk_x_angsuran'];
			$mpk_asuransi_kredit=$row_mpk['mpk_asuransi_kredit'];
			$mpk_asuransi_kebakaran=$row_mpk['mpk_asuransi_kebakaran'];	
			
			$mpk_debet_atasnama=$row_mpk['mpk_debet_atasnama'];
			$mpk_debet_norek=$row_mpk['mpk_debet_norek'];	
			
			$mpk_last_branch=$row_mpk['mpk_last_branch'];
			$mpk_last_remarks=$row_mpk['mpk_last_remarks'];
			
			$mpk_no_suratpermohonan=$row_mpk['mpk_no_suratpermohonan'];	
			}						
	}
	sqlsrv_free_stmt( $sqlConn_mpk );
	//echo $mpk_atas_nama_bm;
	
	$in_cif = "-";
	
	$sql_mpk="SELECT * FROM Tbl_MKKAnalisa5c WHERE custnomid = '$custnomid'";
	$cursortype_mpk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_mpk= array(&$_POST['query']);
	$sqlConn_mpk = sqlsrv_query($conn, $sql_mpk, $params_mpk, $cursortype_mpk);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_mpk))
	{
		$rowCount_mpk = sqlsrv_num_rows($sqlConn_mpk);
		if($row_mpk = sqlsrv_fetch_array( $sqlConn_mpk, SQLSRV_FETCH_ASSOC))
			{
				$in_cif = $row_mpk['analisa5c_cif'];
			}
	}
	
	$bankname="";
	$sql_bn="select * from MS_CONTROL WHERE CONTROL_CODE = 'BANKNAME'";
	$cursortype_bn = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params_bn= array(&$_POST['query']);
	$sqlConn_bn = sqlsrv_query($conn, $sql_bn, $params_bn, $cursortype_bn);
	if($conn==false){die(FormatErrors(sqlsrv_errors()));}
	if(sqlsrv_has_rows($sqlConn_bn))
	{
		$rowCount_bn = sqlsrv_num_rows($sqlConn_bn);
		while($row_bn = sqlsrv_fetch_array( $sqlConn_bn, SQLSRV_FETCH_ASSOC))
		{

			$bankname=$row_bn['CONTROL_VALUE'];

		}						
	}
	sqlsrv_free_stmt( $sqlConn_bn );
	
	
?>


<html>
<head>
</head>
<title>MPK</title>
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<body>
<script language="JavaScript">name-"utama";</script> 		
<link href="../../css/d.css" rel="stylesheet" type="text/css" />
<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../js/jquery-1.7.2.min.js" ></script>
<form id="frm" name="frm" action="" method="post">
	<table width="1200" align="center" class="preview">
		<tr> 
			<td>
				<input nai="" type="hidden" name=userid  value='<? echo $userid; ?>'>
				<input nai="" type="hidden" name=userpwd value='<? echo $userpwd; ?>'>
				<input nai="" type="hidden" name=userbranch value='<? echo $userbranch; ?>'>
				<input nai="" type="hidden" name=userregion value='<? echo $userregion; ?>'>
				<input nai="" type="hidden" name=buttonaction value='<? echo $buttonaction; ?>'>
				<input nai="" type="hidden" name=userwfid  value='<? echo $userwfid; ?>'>
				<input nai="" type="hidden" name=custnomid  value='<? echo $custnomid; ?>'>
				<table width="100%" align="center" style="margin-top: 30px;">
					<tr>
						<td align="center" style="font-size: 18px">MEMO PENCAIRAN KREDIT</td>
					</tr>
				</table>
				</br>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr> 
						<td valign="top" width="50%">
							<table width="100%">
								<tr>
									<td style="padding-left: 10px;">Kepada</td>
									<td><?php echo $mpk_kepada ?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Dari</td>
									<td><?php echo $mpk_dari?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Nomor</td>
									<td><?php echo $custnomid;?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Tanggal</td>
									<td><?php echo $mpk_tgl?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">CIF</td>
									<td>
										<? echo $in_cif;?>
									</td>
								</tr>
							</table>
						</td>
						<td width="50%">
							<table width="100%">
								<tr>
									<td style="padding-left: 10px;">Debitur</td>
									<td><? if($cmpsex!="0") {echo $cmpfullname;}else{echo $cmpnamausaha;} ?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;">Rekening</td>
									<td><?php echo $mpk_no_rek_bm?></td>
								</tr>
                                <?
                                if($custproccode == "KUK")
								{
								?>
									<tr>
									<td style="padding-left: 10px;">&nbsp;</td>
									<td>&nbsp;</td>
									</tr>
								<?
                                }
								else
								{
								?>
								<tr>
									<td style="padding-left: 10px;">Tgl./No.LFK</td>
									<td><?php echo $mpk_tgl_lfk?></td>
								</tr>
                                <?
								}
								?>
								<tr>
									<td style="padding-left: 10px;">Komite Kredit</td>
									<td><?php echo $mpk_komite_kredit?></td>
								</tr>
								<tr>
									<td style="padding-left: 10px;"><!--No. LKK!--></td>
									<td>&nbsp;<?php //echo $mpk_no_lkk?></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">Berdasarkan Surat Permohonan Debitur No. <?=$mpk_no_suratpermohonan;?></td>
					</tr>
				</table>
				<!--  -->
				<table width="100%" align="center" style="margin-top: 30px;">
					<tr>
						<td  style="font-size: 18px">FASILITAS YANG DI CAIRKAN</td>
					</tr>
				</table>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td width="9%" align="center">Fasilitas</td>
						<td width="4%" align="center">Mata Uang</td>
						<td width="13%" align="center">Plafond</td>
						<td width="13%" align="center">Outstanding</td>
						<td width="12%" align="center">Nominal Pencairan</td>
						<td width="10%" align="center">Tgl. Valuta Pencairan</td>
						<td width="7%" align="center">Bunga % pa</td>
						<td width="8%" align="center">Provisi / Komisi </td>
						<td width="5%" align="center">Jangka Waktu</td>
						<td width="20%" align="center">Keterangan</td>
					</tr>
					<?
					$total_plafond1=0;
					$plafond=0;
					$ttl_admin=0;
					$perkalian_plafond_provisi=0;
					$ttl_perkalian_plafond_provisi=0;
					$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
							cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.mkknewbungaflag,cf.custcreditlong, cf.sukubungayangdiberikan,
							cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran
							from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
							where cf.custnomid='$custnomid'
							and cf.custcredittype=kp.produk_loan_type";
					$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
					$params_mkk = array(&$_POST['query']);
					$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
					if($conn==false){die(FormatErrors(sqlsrv_errors()));}
					if(sqlsrv_has_rows($sqlConn_mkk))
					{
						$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
						while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
						{
						$sequence=$row_mkk['custfacseq'];
						$nama_produk=$row_mkk['produk_type_description'];
						$produk_currency_type=$row_mkk['produk_currency_type'];
						$plafond=$row_mkk['custcreditplafond'];
						$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
						$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
						$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
						$jangkawaktu=$row_mkk['custcreditlong'];
						$mkknewprovisi=$row_mkk['mkknewprovisi'];
						$mpk_keterangan=$row_mkk['mpk_keterangan'];
						$mkknewadm=$row_mkk['mkknewadm'];
						
						$mkknewangsuran = $row_mkk['mkknewangsuran'];
						
						$perkalian_plafond_provisi=$plafond*$mkknewprovisi/100;
						$total_plafond1+=$plafond;
						$ttl_perkalian_plafond_provisi+=$perkalian_plafond_provisi;
						$ttl_admin+=$mkknewadm;
					?>
					<tr>
						<td><?php echo $nama_produk ?></td>
						<td align="center"><?php echo $produk_currency_type ?></td>
						<td align="right"><?php  echo 'Rp. '.number_format($plafond); ?></td>
						<td align="right">&nbsp;<?php echo 'Rp. '.number_format($plafond);?></td>
						<td align="right"><?php echo 'Rp. '.number_format($plafond) ?></td>
						<td><? echo $mpk_tgl_valuta_pencairan; ?></td>
						<td align="center"><? echo $sukubungayangdiberikan." %" ?></td>
						<td align="center"><?echo $mkknewprovisi;?> %</td>
						<td align="center"><?php echo $jangkawaktu.' Bln'; ?></td>
						<td align="center"><?php if($mpk_keterangan==""){echo '&nbsp;';} else{echo $mpk_keterangan;}?></td>
					</tr>
					<?
						}
					}
					sqlsrv_free_stmt( $sqlConn_mkk );
					?>
					<tr>
						<td>Total</td>
						<td>&nbsp;</td>
						<td align="right">&nbsp;<?php echo 'Rp. '.number_format($total_plafond1); ?></td>
						<td align="right">&nbsp;<?php echo 'Rp. '.number_format($total_plafond1);?></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="10">Konfirmasi Teasury:</td>
					</tr>
					<tr>
						<td colspan="10">
							<table width="100%">
								<tr>
									<td width="15%"><b/>KETERANGAN</td>
									<td width="1%">:</td>
									<td width="84%">Perjanjian Kredit telah di bawah tanda tangan pada tanggal <?php echo  $mpk_tgl_notaris?> 
									oleh Notaris <?php echo $mpk_nama_notaris?> No, PK : <?php echo $pk_no;?></td>
								</tr>
								<tr>
									<td><b/>Pembebasan Biaya</td>
									<td colspan="2">:</td>
								</tr>
								<?
									$mkknewangsuran = 0;
									
									$total_plafond1=0;
									$plafond=0;
									$ttl_admin=0;
									$perkalian_plafond_provisi=0;
									$ttl_perkalian_plafond_provisi=0;
									$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
											cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.sukubungayangdiberikan, cf.custcreditlong,
											cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran
											from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
											where cf.custnomid='$custnomid'
											and cf.custcredittype=kp.produk_loan_type"; //echo $sql_mkk;
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];
											$nama_produk=$row_mkk['produk_type_description'];
											$produk_currency_type=$row_mkk['produk_currency_type'];
											$plafond=$row_mkk['custcreditplafond'];
											$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
											$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
											$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
											$jangkawaktu=$row_mkk['custcreditlong'];
											$mkknewprovisi=$row_mkk['mkknewprovisi'];
											$mpk_keterangan=$row_mkk['mpk_keterangan'];
											$mkknewadm=$row_mkk['mkknewadm'];
											
											$provisirpk = ($plafond * $mkknewprovisi)/100;
											
								?>
								<tr>
									<td>Provisi RPK</td>
									<td>:</td>
									<td><?echo 'Rp. '.number_format($provisirpk)?></td>
								</tr>
								<tr>
									<td>Biaya Admin Kredit</td>
									<td>:</td>
									<td><?echo 'Rp. '.number_format($mkknewadm)?></td>
								</tr>
								<?
										}
									}
								?>
								<tr>
									<td>Biaya Notaris</td>
									<td>:</td>
									<td><?php echo  'Rp. '.number_format($mpk_biaya_notaris)?></td>
								</tr>
								<?
									$mkknewangsuran = 0;
									
									$total_plafond1=0;
									$plafond=0;
									$ttl_admin=0;
									$perkalian_plafond_provisi=0;
									$ttl_perkalian_plafond_provisi=0;
									$sql_mkk="select cf.custfacseq,kp.produk_type_description,kp.produk_currency_type,cf.custcreditplafond,
											cf.mpk_nominal_pencairan,cf.mpk_tgl_valuta_pencairan, cf.sukubungayangdiberikan, cf.custcreditlong,
											cf.mkknewprovisi,cf.mpk_keterangan,cf.mkknewadm, cf.mkknewangsuran, cf.mpk_blokir
											from tbl_CustomerFacility2 cf,Tbl_KodeProduk kp
											where cf.custnomid='$custnomid'
											and cf.custcredittype=kp.produk_loan_type"; //echo $sql_mkk;
									$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
									$params_mkk = array(&$_POST['query']);
									$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
									if($conn==false){die(FormatErrors(sqlsrv_errors()));}
									if(sqlsrv_has_rows($sqlConn_mkk))
									{
										$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
										while($row_mkk = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
										{
											$sequence=$row_mkk['custfacseq'];
											$nama_produk=$row_mkk['produk_type_description'];
											$produk_currency_type=$row_mkk['produk_currency_type'];
											$plafond=$row_mkk['custcreditplafond'];
											$mpk_nominal_pencairan=$row_mkk['mpk_nominal_pencairan'];
											$mpk_tgl_valuta_pencairan=$row_mkk['mpk_tgl_valuta_pencairan'];
											$sukubungayangdiberikan=$row_mkk['sukubungayangdiberikan'];
											$jangkawaktu=$row_mkk['custcreditlong'];
											$mkknewprovisi=$row_mkk['mkknewprovisi'];
											$mpk_keterangan=$row_mkk['mpk_keterangan'];
											$mkknewadm=$row_mkk['mkknewadm'];
											$mkknewangsuran=$row_mkk['mkknewangsuran'];
											$mpk_x_angsuran = $row_mkk['mpk_blokir'];
											
								?>
								<tr>
								  <td>Blokir <?php echo $mpk_x_angsuran;?> X Angsuran &nbsp;</td>
								  <td>:</td>
								  <td><?php echo 'Rp. '.number_format($mpk_x_angsuran * $mkknewangsuran);?>&nbsp;</td>
							  </tr>
							  <?
										}
									}
							  ?>
								<tr>
								  <td>Asuransi Jiwa Kredit</td>
								  <td>:</td>
								  <td><?php echo 'Rp. '.number_format($mpk_asuransi_kredit);?>&nbsp;</td>
							  </tr>
								<tr>
								  <td>Asuransi Jiwa Kebakaran</td>
								  <td>:</td>
								  <td><?php echo 'Rp. '.number_format($mpk_asuransi_kebakaran);?>&nbsp;</td>
							  </tr>
								<tr>
									<td>Total Biaya</td>
									<td>:</td>
									<td><?php echo 'Rp. '.number_format($mpk_total_biaya)?></td>
								</tr>
								<tr>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
								  <td>&nbsp;</td>
							  </tr>
								<tr>
								  <td colspan="3">Seluruh biaya mohon di debet dari rekening Debitur</td>
							  </tr>
								<tr>
								  <td>Atas Nama</td>
								  <td>:</td>
								  <td><?=$mpk_debet_atasnama;?>&nbsp;</td>
							  </tr>
								<tr>
								  <td>No Rekening</td>
								  <td>:</td>
								  <td><?=$mpk_debet_norek;?>&nbsp;</td>
						      </tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3">Pencairan tersebut agar di- kreditkan kepada Rekening  <?php echo $bankname?> :</td>
								</tr>
								<tr>
									<td>Cabang</td>
									<td>:</td>
									<td><?php echo $branch_name;?></td>
								</tr>
								<tr>
									<td>Atas Nama</td>
									<td>:</td>
									<td><?php echo $mpk_atas_nama_bm; ?></td>
								</tr>
								<tr>
									<td>No. Rekening</td>
									<td>:</td>
									<td><?php echo $mpk_no_rek_bm?></td>
								</tr>
								<tr>
									<td>Tanggal Valuta</td>
									<td>:</td>
									<td><?php echo $mpk_tgl_valuta_bm?></td>
								</tr>
								<tr>
									<td>Nominal</td>
									<td>:</td>
									<td><?php echo 'Rp. '.number_format($mpk_nominal_bm1);?></td>
								</tr>
								<tr>
									<td>TTUN</td>
									<td>:</td>
									<td><?php echo $mpk_ttun_no_bm?> <?php echo $mpk_jangka_waktu1?> s/d <?php echo $mpk_jangka_waktu2?>
									, Nominal Rp <?php echo  number_format($mpk_nominal_bm2)?> </td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="3">
								<div id="DANA" style="border:1px solid black;">
								<table id="DANATABLE" name="DANATABLE">
									<tr>
										<td colspan="7"><strong>Untuk Selanjutnya Dana di - </strong></td>
									</tr>
									<tr>
										<td style="border:1px solid black;width:120px;">Tipe</td>
										<td style="border:1px solid black;width:120px;">Nama Bank</td>
										<td style="border:1px solid black;width:120px;">Cabang</td>
										<td style="border:1px solid black;width:120px;">Atas Nama</td>
										<td style="border:1px solid black;width:120px;">Nomor Rekening</td>
										<td style="border:1px solid black;width:120px;">Tanggal Valuta</td>
										<td style="border:1px solid black;width:120px;">Nominal</td>
										<td style="border:1px solid black;width:120px;">Remarks</td>
									</tr>
									
									<?
										
										$mpk_seq="";
										$mpk_dana="";
										$mpk_nama_bank="";
										$mpk_last_branch="";
										$mpk_atas_nama="";
										$mpk_no_rek="";
										$mpk_tgl_valuta="";
										$mpk_nominal=0;
										$mpk_last_remarks="";
										$mpk_flag="";
										
										$sql_mkk="select * from Tbl_MPKDANA where custnomid = '$custnomid' order by mpk_seq"; //echo $sql_mkk;
										$cursortype_mkk = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
										$params_mkk = array(&$_POST['query']);
										$sqlConn_mkk = sqlsrv_query($conn, $sql_mkk, $params_mkk, $cursortype_mkk);
										if($conn==false){die(FormatErrors(sqlsrv_errors()));}
										if(sqlsrv_has_rows($sqlConn_mkk))
										{
											$rowCount_mkk = sqlsrv_num_rows($sqlConn_mkk);
											while($rows = sqlsrv_fetch_array( $sqlConn_mkk, SQLSRV_FETCH_ASSOC))
											{
												$mpk_seq=$rows["mpk_seq"];
												$mpk_dana=$rows["mpk_dana"];
												$mpk_nama_bank=$rows["mpk_nama_bank"];
												$mpk_last_branch=$rows["mpk_last_branch"];
												$mpk_atas_nama=$rows["mpk_atas_nama"];
												$mpk_no_rek=$rows["mpk_no_rek"];
												$mpk_tgl_valuta=$rows["mpk_tgl_valuta"];
												$mpk_nominal=$rows["mpk_nominal"];
												$mpk_last_remarks=$rows["mpk_last_remarks"];
												$mpk_flag=$rows["mpk_flag"];
									?>
									<tr>
										<td style="border:1px solid black;width:120px;">&nbsp;<? echo $mpk_dana;?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?php echo $mpk_nama_bank?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?=$mpk_last_branch;?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?php echo $mpk_atas_nama?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?php echo $mpk_no_rek?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?php echo $mpk_tgl_valuta?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?php echo number_format($mpk_nominal)?></td>
										<td style="border:1px solid black;width:120px;">&nbsp;<?=$mpk_last_remarks;?></td>
										<td>&nbsp;</td>
									</tr>
									<?
											}
										}	
									?>
								</table>
								</div>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td> 
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td></b>Persyaratan Pencairan Kredit sesuai Perjanjian Kredit :</td>
					</tr>
					<tr>
						<td><?php echo nl2br($mpk_ppkspk)?></td>
					</tr>
				</table>
			</td>
		</tr> 
		<tr>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td>
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td align="center" width="33%">Diusulkan oleh : Bagian Marketing</td>
						<td align="center" width="33%">Diperiksa oleh : Administrasi Kredit</td>
						<td align="center" width="33%">Dilaksanakan oleh : Loan Processing</td>
					</tr>
					<tr>
                    
                    <?
					
					
                    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='MPK'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='CKPK'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOVE'
    //SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOOP'
    
    $getCheckerMPK="";
    $getApprovalMPK="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='MPK'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerMPK=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalMPK=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
    
	$getCheckerCKPK="";
    $getApprovalCKPK="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='CKPK'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerCKPK=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalCKPK=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
	
	$getCheckerLOVE="";
    $getApprovalLOVE="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOVE'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerLOVE=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalLOVE=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
	
	
	$getCheckerLOOP="";
    $getApprovalLOOP="";
    $sql="SELECT DISTINCT(txn_action),txn_id,user_name,txn_notes FROM Tbl_Txn_History LEFT JOIN Tbl_SE_User ON user_id = txn_user_id WHERE txn_id = '$custnomid' AND txn_flow ='LOOP'";
    $cursortype = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
    $params = array(&$_POST['query']);
    $sqlConn = sqlsrv_query($conn, $sql, $params, $cursortype);
    if($conn==false){die(FormatErrors(sqlsrv_errors()));}
    if(sqlsrv_has_rows($sqlConn))
    {
		$rowCount = sqlsrv_num_rows($sqlConn);
		while($row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_BOTH))
		{
			if($row['txn_action'] == "C")
			{
				$getCheckerLOOP=$row['user_name'];
			}
			else if($row['txn_action'] == "A")
			{
				$getApprovalLOOP=$row['user_name'];
			}		
		}
    }
    sqlsrv_free_stmt( $sqlConn );
    ?>
            
            
            
				<table width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff" >
					<tr>
						<td align="center" width="25%">Diusulkan oleh : Bagian Marketing</td>
						<td align="center" width="25%">Diperiksa oleh : Acceptance</td>
						<td align="center" width="25%">Diperiksa Oleh : Loan Verification</td>
                        <td align="center" width="25%">Dilaksanakan oleh : Loan Transaction</td>
					</tr>
					<tr>
                    <td colspan="4">
                    <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
                    <tr>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    <td align="center">Checker</td><td align="center">Approve</td>
                    </tr>
                    <tr height="100px">
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerMPK;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalMPK;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerCKPK;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalCKPK;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerLOVE;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalLOVE;?>&nbsp;</td>
                    <td width="12.5%" valign="bottom" align="center"><?=$getCheckerLOOP;?>&nbsp;</td><td width="12.5%" valign="bottom" align="center"><?=$getApprovalLOOP;?>&nbsp;</td>
                    </tr>
                    </table>
                    
                    <!--
						<td><?php echo $mpk_bagian_marketing?></td>
						<td><?php echo $mpk_administrasi_kredit?></td>
						<td><?php echo $mpk_loan_processing?></td>
                        
                    !-->
					
                    
                    </tr>
				</table>
			</td>
		</tr>
	</table>
    <center>
    <?php 
	
	if($userid!=""&&$userwfid!="")
	{
		require ("../../requirepage/btnview.php");
	}
	require ("../../requirepage/hiddenfield.php");
	require("../../requirepage/btnprint.php");

	?>
</center>
</form>
</body>
</html>