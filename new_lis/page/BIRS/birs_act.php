<?
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");
	
	if($custnomid != "")
	{
		
	}
?>
<html>
	<head>
		<title>BI Result</title>
		<link rel="stylesheet" href="../../bin/css/css-bj.css" type="text/css" />
		<link rel="shortcut icon" href="../../bin/img/favicon.png" type="image/x-icon">
		<link href="../../bin/css/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
		<script src="../../bin/css/SpryMenuBar.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/jquery-1.11.3.js" ></script>
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="../../js/datetimepicker_css.js"></script>		
		<script type="text/javascript" src="../../bin/js/datatable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstraptable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstrap-datepicker.min.js"></script>
		
		<link href="../../bin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
		
		<script language="javascript" type="text/javascript" src="../../bin/javascript/niceforms.js"></script>
		<link href="../../bin/css/table.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" media="screen" href="../../bin/css/styles_idr.css" >
		<script type="text/javascript">
			$(document).ready(function() {
					$('#example').DataTable();
					$('.tanggal').datepicker({format: 'yyyy-mm-dd',todayBtn: "linked"});
					$(".tanggal").keydown(function(e){
						e.preventDefault();
					});
				} );
			
		</script>
		
		<script type="text/javascript">
			function disablenama(){
				if (document.contact_form.flag.options[document.contact_form.flag.selectedIndex].value == "0"){
					document.contact_form.nama_bank.value = "Bank Sumselbabel";
					document.contact_form.nama_bank.readOnly = true;
				}else{
					document.contact_form.nama_bank.value = "";
					document.contact_form.nama_bank.readOnly = false;
				}
			}
		</script>
		
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<script type="text/javascript" src="../../js/my.js" ></script>
		<Script Language="JavaScript">
			function changeValue()
			{
				if (document.contact_form.input_sid.options[document.contact_form.input_sid.selectedIndex].value == "SID01")
				{
					document.contact_form.flag.options[0].selected = true;
					document.contact_form.nama_bank.value = "-";
					document.contact_form.sifat.value = "0";
					document.contact_form.norek.value = "-";
					document.contact_form.tanggal_permintaan.value = "-";
					document.contact_form.ket_kondisi.value = "-";
					document.contact_form.jenis_fasilitas.value = "-";
					document.contact_form.baki_debet.value = "0";
					document.contact_form.tanggal_mulai.value = "-";
					//document.contact_form.akad_awal.value = "-";
					document.contact_form.sektor_ekonomi.options[0].selected = true;
					document.contact_form.jatuh_tempo.value = "-";
					document.contact_form.kolektibilitas.value = "0";
					document.contact_form.kolektibilitas_terendah.value = "0";
					document.contact_form.jumlah_hari_tunggakan.value = "0";
				}
			}
		</Script>
		
	</head>
<body>
<div id="page"> 
<?php

	if(isset($_REQUEST['id'])){
		$id = $_REQUEST['id'];
	}else{
		$id = "";
	}
	
	if(isset($_REQUEST['act'])){
		$act = $_REQUEST['act'];
	}else{
		$act = "";
	}
	
	$nama_debitur = "";
	$sifat = "";
	$norek = "";
	$tanggal_permintaan = "";
	$nama_bank = "";
	$ket_kondisi = "";
	$jenis_fasilitas = "";
	$baki_debet = 0;
	$sektor_ekonomi = "";
	$tanggal_mulai = "";
	$akad_awal = "";
	$jatuh_tempo = "";
	$input_sid = "";
	$kolektibilitas = "";
	$kolektibilitas_terendah = "";
	$jumlah_hari_tunggakan = "";
	$strsqlv01="SELECT * FROM tbl_birs WHERE idx = '$id'";
	$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
	if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlconv01))
	{
		if($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
		{
			$custnomid = $rowsv01['custnomid'];
			$nama_debitur = $rowsv01['nama_debitur'];
			$sifat = $rowsv01['sifat'];
			$norek = $rowsv01['norek'];
			$tanggal_permintaan = $rowsv01['tanggal_permintaan'];
			$nama_bank = $rowsv01['nama_bank'];
			$ket_kondisi = $rowsv01['ket_kondisi'];
			$jenis_fasilitas = $rowsv01['jenis_fasilitas'];
			$baki_debet = $rowsv01['baki_debet'];
			$sektor_ekonomi = $rowsv01['sektor_ekonomi'];
			$tanggal_mulai = $rowsv01['tanggal_mulai'];
			$akad_awal = $rowsv01['akad_awal'];
			$jatuh_tempo = $rowsv01['jatuh_tempo'];
			$input_sid = $rowsv01['input_sid'];
			$kolektibilitas = $rowsv01['kolektibilitas'];
			$kolektibilitas_terendah = $rowsv01['kolektibilitas_terendah'];
			$jumlah_hari_tunggakan = $rowsv01['jumlah_hari_tunggakan'];
			$flag = $rowsv01['flag'];
			
		}
	}
		
?>
	
		<br>
		<div class="divcenter">
			<table border="1" style ="width:900px; border-color:black;" align="center">
				<tr>
					<td align="left" colspan="2" style="font-size:10pt;">
					<div align="left" style="padding:20px;">
					<form class="contact_form" action="birs_do.php" method="post" name="contact_form">
						<ul>
							<li>
								 <h2><?php echo "<a href=\"javascript:history.go(-1)\"><img src='../../bin/img/back.png' style='width:40px;'/></a>";?> Data BI Result</h2>
								 <span class="required_notification"><!--* Denotes Required Field--></span>
							</li>
							<li>
								<label for="name">Custnomid</label>
								<input type="text" id="custnomid" name="custnomid" maxlength="20" value="<?php echo $custnomid;?>" <?php echo "readonly"; ?> required/>
							</li>
							<li>
								<label for="name">Nama Debitur</label>
								<select id="nama_debitur" name="nama_debitur" <?php if($act=="v"){echo "";} ?> required>
									<option value="">-Select-</option>
									<?php
										$strsqlv01="select name
		                            from Tbl_CustomerSibling a
		                            where a.custnomid='".$custnomid."'  and a.flagdelete ='0'
		                            order by name";
										$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
										if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
										if(sqlsrv_has_rows($sqlconv01))
										{
											while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
											{
												$code = $rowsv01['name'];
												
											$varselected="";
											if($nama_debitur == $code){
												$varselected = "selected";}
											echo "<option value='".$code."'".$varselected.">".$code."</option>";			
											}
										}
									?>
								</select>
							</li>
							<li>
								<label for="name">Input SID</label>
								<select id="input_sid" name="input_sid" <?php if($act=="v"||$id!=""){echo "";} ?> required onChange="javascript:changeValue()">
									<option value="">-Select-</option>
									<?php
										$strsqlv01="SELECT * FROM param_sid";
										$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
										if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
										if(sqlsrv_has_rows($sqlconv01))
										{
											while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
											{
												$code = $rowsv01['code'];
												$attribute = $rowsv01['attribute'];
												
											$varselected="";
											if($input_sid == $code){
												$varselected = "selected";}
											echo "<option value='".$code."'".$varselected.">".$attribute."</option>";			
											}
										}
									?>
								</select>
							</li>
							<li>
								<label for="name">Pilih Bank</label>
								<select id="flag" name="flag" <?php if($act=="v"){echo "";}?>  onchange="disablenama();">
									<option value="" <?if($flag==""){echo "selected";}?>>-Select-</option>
									<option value="0" <?if($flag=="0"){echo "selected";}?>>Bank Sumselbabel</option>
									<option value="1" <?if($flag=="1"){echo "selected";}?>>Bank Lainnya</option>
								</select>
							</li>
							<li>
								<label for="name">Nama Bank</label>
								<input type="text" id="nama_bank" name="nama_bank" maxlength="50" value="<?php echo $nama_bank;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">No Rekening</label>
								<input type="text" id="norek" name="norek" maxlength="50" value="<?php echo $norek;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Tanggal Permintaan</label>
								<input type="text" id="tanggal_permintaan" name="tanggal_permintaan" maxlength="50" value="<?php echo $tanggal_permintaan;?>" onFocus="NewCssCal(this.id,'YYYYMMDD');" readonly <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Ket Kondisi</label>
								<input type="text" id="ket_kondisi" name="ket_kondisi" maxlength="250" value="<?php echo $ket_kondisi;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Jenis Fasilitas</label>
								<input type="text" id="jenis_fasilitas" name="jenis_fasilitas" maxlength="250" value="<?php echo $jenis_fasilitas;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Plafond</label>
								<input type="text" id="sifat" name="sifat" maxlength="50" value="<?php echo $sifat;?>" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Baki debet</label>
								<input type="text" id="baki_debet" name="baki_debet" maxlength="20" value="<?php echo number_format($baki_debet);?>" onkeypress="return isNumberKey(event)" onkeydown="return numbersonly(this, event);" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Sektor Ekonomi</label>
											<select id="sektor_ekonomi" name="sektor_ekonomi" <?php if($act=="v"){echo "disabled";} ?> required>
												<?
												$strsql = "select ekonomi_code,ekonomi_name from Tbl_SektorEkonomi order by ekonomi_name";
												$sqlcon = sqlsrv_query($conn, $strsql);
												if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
												if(sqlsrv_has_rows($sqlcon))
												{
													while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
													{
														if($sektor_ekonomi==$rows['ekonomi_code'])
														{
															echo '<option value="'.$rows['ekonomi_code'].'" selected="selected">'.$rows['ekonomi_code']. ' - ' .$rows['ekonomi_name'].'</option>';
														}
														else
														{
															echo '<option value="'.$rows['ekonomi_code'].'">'.$rows['ekonomi_code']. ' - ' .$rows['ekonomi_name'].'</option>';
														}
													}
												}
												
												?>
											</select>
							</li>
							<li>
								<label for="name">Tanggal Mulai/Akad Awal</label>
								<input type="text" id="tanggal_mulai" name="tanggal_mulai" maxlength="200" value="<?php echo $tanggal_mulai;?>" onFocus="NewCssCal(this.id,'YYYYMMDD');" readonly <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<!--<li>
								<label for="name">Akad Awal</label>
								<input type="text" id="akad_awal" name="akad_awal" maxlength="200" value="<?php echo $akad_awal;?>" onFocus="NewCssCal(this.id,'YYYYMMDD');" readonly <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>-->
							<li>
								<label for="name">Jatuh Tempo</label>
								<input type="text" id="jatuh_tempo" name="jatuh_tempo" maxlength="200" value="<?php echo $jatuh_tempo;?>" onFocus="NewCssCal(this.id,'YYYYMMDD');" readonly <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Kolektibilitas saat ini</label>
								<input type="text" id="kolektibilitas" name="kolektibilitas" maxlength="50" value="<?php echo $kolektibilitas;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Kolektibilitas terendah</label>
								<input type="text" id="kolektibilitas_terendah" name="kolektibilitas_terendah" maxlength="50" value="<?php echo $kolektibilitas_terendah;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<label for="name">Jumlah Hari Tunggakan</label>
								<input type="text" id="jumlah_hari_tunggakan" name="jumlah_hari_tunggakan" maxlength="50" value="<?php echo $jumlah_hari_tunggakan;?>" <?php if($act=="v"){echo "disabled";} ?> required/>
							</li>
							<li>
								<?php if($act!="v"){?>
								<button id="submit" name="submit" class="submit" type="submit">Submit Form</button>
								<?} ?>
							</li>
						</ul>
						<input type="hidden" id="id" name="id" value="<?php echo $id;?>" />		
						<? require ("../../requirepage/hiddenfield.php");?>
						</form>	
						</div>
					</td>
				</tr>
			</table>
						
						
						
						
		
</body>
</html>