<?
include("../../new_lis/lib/sqlsrv.lis.php");
$db = new DB_LIS();
$db->connect();

$sql = "select * from KEU_FORMULA";
$db->executeQuery($sql);
$result = $db->lastResults;


$sql = "select * from KEU_DETAIL where _type = 'VALUE' order by _sequence";
$db->executeQuery($sql);
$result_detail = $db->lastResults;

$db->lastResults = "";

$sql = "select * from KEU_DETAIL where _type = 'FORMULA' and _code_detail not in (select _code_detail from KEU_FORMULA) order by _sequence";
$db->executeQuery($sql);
$result_formula = $db->lastResults;
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<style>
div
{
}
</style>

<title>PARAMETER</title>
</head>


<body>
<form id="form_formula" method="post" action="KEU_FORMULA_process.php">
<table align="center" width="960" border="0" style="">
	<tr>
		<td align="center"><img src="../images/Header Mega (L).png" width="200px"></td>
	</tr>
	<tr>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><h3>FORMULA</h3></td>
	</tr>
</table>
<div id="all" style="width:100%">

	<div id="button" style="float:right;width:50%">
	<table border="0" align="center" width="640px" cellpadding="0" cellspacing="10">
		<tr>
			<td valign="top" colspan="4">
			<button type="reset" onClick="func_new()">New</button>
			</td>
		</tr>

		<tr>
			<td valign="top">Code</td>
			<td valign="top" width="1px">&nbsp;:&nbsp;</td>
			<td valign="top">
			<select id="pick_code" name="pick_code" onChange="fill_code()">
			<?
			for($x=0;$x<count($result_formula);$x++)
			{
				$cd = $result_formula[$x]['_code_detail'];
				$nm = $result_formula[$x]['_name'];
			?>
			<option value="<?=$cd;?>">[<?=$cd;?>] - <?=$nm;?></option>
			<?
			}
			?>
			</select>
			<input type="text" id="code" name="code" readonly value="">
			</td>
			<td valign="top" width="1px">&nbsp;</td>
		</tr>

		<tr>
			<td valign="top">Name</td>
			<td valign="top" width="1px">&nbsp;:&nbsp;</td>
			<td valign="top" width="100%">
			<div id="formula" style="padding:5px;border:#666666 solid 1px;height:250px;"></div>
			</td>
			<td valign="top" width="1px">&nbsp;</td>
		</tr>

		<tr>
			<td colspan="4">
			<div id="button_formula">
			<button type="button" onClick="add_button(this.id)" id="-" title="-" value="-">-</button>
			<button type="button" onClick="add_button(this.id)" id="+"  title="+" value="+">+</button>
			<button type="button" onClick="add_button(this.id)" id="*"  title="*" value="*">*</button>
			<button type="button" onClick="add_button(this.id)" id="/"  title="/" value="/">/</button>
			<button type="button" onClick="add_button(this.id)" id="("  title="(" value="(">(</button>
			<button type="button" onClick="add_button(this.id)" id=")"  title=")" value=")">)</button>
			<br>
			<?
			for($x=0;$x<count($result_detail);$x++)
			{
				$cd = $result_detail[$x]['_code_detail'];
				$nm = $result_detail[$x]['_name'];
			?>
			<button type="button" id="<?=$cd;?>" onClick="add_button(this.id)" title="<?=$nm;?>" value="<?=$cd;?>"><?=$cd;?></button>
			<?
			}
			?>
			<br><br>
			* hover for button information
			</div>
			</td>
		</tr>

		<tr>
			<td valign="top" colspan="4">
			<button id="button_form" name="button_form" onClick="to_do_submit()" value="add" type="button">Add</button>
			<input type="hidden" id="act" name="act" value="add">
			<input type="hidden" id="hidden_formula" name="hidden_formula" value="">
			</td>
		</tr>
	</table>
	</div>

	<div id="data" style="float:left;width:50%">
	<br><br>
	<table style="background-color:#FFF;" width="100%"  cellpadding="0" cellspacing="0" border="1" align="center" bordercolor="#4476b3" bordercolorlight="#000000" bordercolordark="#ffffff">
		<tr>
			<th width="10%">Code</th>
			<th width="75%">Formula</th>
			<th width="15%">Action</th>
		</tr>

		<?
		for($x=0;$x<count($result);$x++)
		{
			$code = $result[$x]['_code_detail'];
			$show = $result[$x]['_formula'];
		?>

		<tr>
		<td><?=$code;?></td>
		<td align="left"><?=$show;?></td>
		<td align="center">
		<button type="button" onClick="func_edit('<?=$code;?>','<?=$code."_data";?>')">Edit</button> | <button type="button" onClick="func_delete('<?=$code;?>','<?=$code."_data";?>')">Delete</button>
		<div id="<?=$code."_data";?>" style="visibility:hidden;height:0px;">
		<?
		echo convert($show);
		?>
		</div>
		</td>
		</tr>

		<?
		}
		?>

	</table>
	</div>

</div>
</form>

<div id="info" style="width:100%">
<table style="font-size:8px" border="0" cellspacing="5" cellpadding="0">
<?
$detail = "select * from KEU_DETAIL";
$db->executeQuery($detail);
$result_detail = $db->lastResults;

error_reporting("E_ERROR");
for($x=0;$x<count($result_detail);$x++)
{
	$cd = $result_detail[$x]['_code_detail'];
	$nm = $result_detail[$x]['_name'];

	?>
	<tr>
	<td><?=$cd;?></td>
	<td><?=$nm;?></td>
	<?

	$x++;
	$cd = $result_detail[$x]['_code_detail'];
	$nm = $result_detail[$x]['_name'];

	?>
	<td><?=$cd;?></td>
	<td><?=$nm;?></td>
	<?

	$x++;
	$cd = $result_detail[$x]['_code_detail'];
	$nm = $result_detail[$x]['_name'];

	?>
	<td><?=$cd;?></td>
	<td><?=$nm;?></td>
	<?

	$x++;
	$cd = $result_detail[$x]['_code_detail'];
	$nm = $result_detail[$x]['_name'];

	?>
	<td><?=$cd;?></td>
	<td><?=$nm;?></td>
	<?

	$x++;
	$cd = $result_detail[$x]['_code_detail'];
	$nm = $result_detail[$x]['_name'];

	?>
	<td><?=$cd;?></td>
	<td><?=$nm;?></td>
	</tr>
	<?
}
error_reporting("E_ALL");
?>
</table>
</div>

<script type="text/javascript">

function add_button(val)
{
   	var btn = document.createElement('button');
	var rnd = Math.floor((Math.random() * 999999999) + 1);
	btn.innerHTML = val;
	btn.setAttribute('id', rnd);
	btn.setAttribute('type', "button");
	btn.setAttribute('title',val);
	btn.setAttribute('name', rnd);
	btn.setAttribute('onClick',"remove_button(this.id)");
	btn.setAttribute('value',val);

	/*
	if(val == "+" || val == "-")
	{
		document.getElementById(val).disabled = false;
	}
	else
	{
		document.getElementById(val).disabled = true;
	}
	*/


	document.getElementById("formula").appendChild(btn);
}

function remove_button(val)
{
	var target = document.getElementById(val);
	document.getElementById("formula").removeChild(target);
	//var master_button = document.getElementById(target.value);
	//master_button.disabled = false;
}

function func_new()
{
	document.getElementById("pick_code").disabled = false;
	document.getElementById("button_form").innerHTML = "Add";
	document.getElementById("button_form").value = "add";
	document.getElementById("act").value = "add";
	document.getElementById("formula").innerHTML = "";
}

function func_delete(code,formula)
{
	document.getElementById("code").value = code;
	document.getElementById("pick_code").value = code;
	document.getElementById("pick_code").disabled = true;
	document.getElementById("formula").innerHTML = document.getElementById(formula).innerHTML;
	document.getElementById("button_form").innerHTML = "Delete";
	document.getElementById("button_form").value = "delete";
	document.getElementById("act").value = "delete";
}

function func_edit(code,formula)
{
	document.getElementById("code").value = code;
	document.getElementById("pick_code").value = code;
	document.getElementById("pick_code").disabled = true;
	document.getElementById("formula").innerHTML = document.getElementById(formula).innerHTML;
	document.getElementById("button_form").innerHTML = "Edit";
	document.getElementById("button_form").value = "edit";
	document.getElementById("act").value = "edit";
}

function fill_code()
{
	var get_code = document.getElementById("pick_code").value;
	document.getElementById("code").value = get_code;
}

function to_do_submit()
{
	var target = document.getElementById("formula");

	var children = target.childNodes;

	var output = "";
	for(child in children)
	{
		 var node = children[child];
		 if(node.value != undefined)
		 {
			 output += node.value;
		 }
	}

	document.getElementById("hidden_formula").value = output;


	//document.getElementById("hidden_formula").value = document.getHTML(target,true);
	document.getElementById("form_formula").submit();
}

fill_code();
</script>

</body>

</html>

<?
function convert($input)
{
	$save = "";
	$output = "";

	$chars = str_split($input);

	$x=0;
	$temp = "";

	foreach($chars as $char)
	{
    	$x++;
		//echo $char. "=" . $x ."<br>";

		if($char == "+" || $char == "-")
		{
			if($temp == "(" || $temp == ")")
			{
				$random = rand(1000000 ,9999999);
				$func_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$char\" value=\"$char\">$char</button>";

				$save = "";
				$output .= $func_node;
			}
			else
			{
				$random = rand(1000000 ,9999999);
				$char_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$save\" value=\"$save\">$save</button>";
				$random = rand(1000000 ,9999999);
				$func_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$char\" value=\"$char\">$char</button>";

				$save = "";
				$output .= $char_node . $func_node;
			}


			$char_node = "";
			$func_node = "";
		}
		else if($char == "(")
		{
			$random = rand(1000000 ,9999999);
			$func_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$char\" value=\"$char\">$char</button>";

			$save = "";
			$output .= $func_node;

			$func_node = "";

			$temp = "(";

		}
		else if($char == ")")
		{
			$random = rand(1000000 ,9999999);
			$char_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$save\" value=\"$save\">$save</button>";
			$random = rand(1000000 ,9999999);
			$func_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$char\" value=\"$char\">$char</button>";

			$save = "";
			$output .= $char_node . $func_node;

			$func_node = "";

			$temp = "(";
		}
		else
		{
			$save .= $char;

			if($x == count($chars))
			{
				$random = rand(1000000 ,9999999);
				$char_node = "<button type=\"button\" onClick=\"remove_button(this.id)\" id=\"$random\" title=\"$save\" value=\"$save\">$save</button>";
				$output .= $char_node;
			}

			$temp = $save;
		}
	}

	return $output;
}
?>
