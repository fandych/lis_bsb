<?
	require_once ("../lib/class.sqlserver.php");
	require_once ("../../sqlsrv.security.php");

  	$db_security = new DB_SECURITY();
  	$db_security->connect();
	
	$db_lis = new SQLSRV();
	$db_lis->connect();
	
	$a=$_REQUEST['a'];
	
	if($a=="CDL"){
		$s=$_REQUEST['s'];
		
		$query_info_user = "SELECT a.user_id, a.user_name, a.user_level_code, b.level_name, a.user_branch_code, c.branch_name, a.user_active
							FROM Tbl_SE_User a 
							JOIN Tbl_SE_Level b ON a.user_level_code = b.level_code
							JOIN Tbl_Branch c ON a.user_branch_code = c.branch_code
							WHERE a.user_id like '%".$s."%'";
		$db_lis->executeQuery($query_info_user);
		$info_user = $db_lis->lastResults;
		$count_info_user = count($info_user);
		
		if($count_info_user<=0)
		{
?>
			<hr size="2" color="blue">
			<p style="color: red;margin-top: 20px;">Pencarian userid <?php echo $s;?>, tidak ditemukan..</p>
<?
		}else{
?>
			<hr size="2" color="blue">
			<table width="100%" border="2" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" valign="top" style="padding: 5px 10px;">
	   	      			<b>USERID</b>
	   	      		</td>
	   	      		<td align="left" valign="top" style="padding: 5px 10px;">
	   	      			<b>USERNAME</b>
	   	      		</td>
	   	      		<td align="left" valign="top" style="padding: 5px 10px;">
	   	      			<b>CABANG</b>
	   	      		</td>
	   	      		<td align="left" valign="top" style="padding: 5px 10px;">
	   	      			<b>LEVEL</b>
	   	      		</td>
				</tr>
<?
			for($x=0;$x<$count_info_user;$x++)
			{
?>
				<tr>
					<td style="padding: 5px 10px;"><a href="javascript:viewDetail('<?php echo $info_user[$x]['user_id'];?>');"><?php echo $info_user[$x]['user_id'];?></a></td>
					<td style="padding: 5px 10px;"><?php echo $info_user[$x]['user_name'];?></td>
					<td style="padding: 5px 10px;"><?php echo $info_user[$x]['user_branch_code']." - ".$info_user[$x]['branch_name'];?></td>
					<td style="padding: 5px 10px;"><?php echo $info_user[$x]['user_level_code']." - ".$info_user[$x]['level_name'];?></td>
				</tr>
<?
			}
?>
			</table>
<?
		}
	}else if($a=="CDL1"){
		$s=$_REQUEST['s'];
		
		$query_info_user = "SELECT a.user_id, a.user_name, a.user_level_code, b.level_name, a.user_branch_code, c.branch_name, a.user_active
							FROM Tbl_SE_User a 
							JOIN Tbl_SE_Level b ON a.user_level_code = b.level_code
							JOIN Tbl_Branch c ON a.user_branch_code = c.branch_code
							WHERE a.user_id = '".$s."'";
		$db_lis->executeQuery($query_info_user);
		$info_user = $db_lis->lastResults;
		$count_info_user = count($info_user);
		
		if($count_info_user<=0)
		{
?>
			<hr size="2" color="blue">
			<p style="color: red;margin-top: 20px;">Pencarian userid <?php echo $s;?>, tidak ditemukan..</p>
<?
		}else{

			$userid = $info_user[0]['user_id'];
			$Username = $info_user[0]['user_name'];
			$userlevelcode = $info_user[0]['user_level_code'];
			$userlevelname = $info_user[0]['level_name'];
			$userbranchcode = $info_user[0]['user_branch_code'];
			$userbranchname = $info_user[0]['branch_name'];
			$userstatus = $info_user[0]['user_active'];

			$aktif_userstatus = "";
			$non_userstatus = "";

			if($userstatus=="9")
			{
				$non_userstatus = "selected";
			}else{
				$aktif_userstatus = "selected";
			}
?>
			<hr size="2" color="blue">
			<table width="100%">
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Userid :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<?php echo $userid;?>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Username :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<?php echo $Username;?>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Level :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<?php echo $userlevelcode." - ".$userlevelname;?>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Branch :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<?php echo $userbranchcode." - ".$userbranchname;?>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Status :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<select name="s_status" id="s_status">
	   	      				<option value="1" <?php echo $aktif_userstatus;?>>Aktif</option>
	   	      				<option value="9" <?php echo $non_userstatus;?>>Nonaktif / Cuti</option>
	   	      			</select>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Userid Temporary :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<select name="userid_temp" id="userid_temp">
	   	      				<option value="">--CHOOSE USERID TEMP--</option>
<?
						$query_user_temp = "SELECT user_id
											FROM se_user 
											WHERE user_active = '9'";
						$db_security->executeQuery($query_user_temp);
						$user_temp_info = $db_security->lastResults;

						for($xx=0;$xx<count($user_temp_info);$xx++)
						{
?>
							<option value="<?php echo $user_temp_info[$xx]['user_id'];?>"><?php echo $user_temp_info[$xx]['user_id'];?></option>
<?
						}
?>
	   	      			</select>
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Mulai Cuti :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<input type="text" name="start_date" id="start_date" value="" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');">
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			Selesai Cuti :
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<input type="text" name="end_date" id="end_date" value="" readonly onFocus="NewCssCal(this.id,'YYYYMMDD');">
	   	      		</td>
				</tr>
				<tr>
					<td width=50% align="right" valign="top">
	   	      			&nbsp;
	   	      		</td>
	   	      		<td width=50% align="left" valign="top">
	   	      			<input type="button" name="btn_process" id="btn_process" value="Process" onclick="javascript:execProcess('<?php echo $userid;?>');" />
	   	      		</td>
				</tr>
			</table>
<?
		}
	}
?>