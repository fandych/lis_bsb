<?php
include ("../lib/formatError.php");
require ("../lib/open_con.php");
require ("../requirepage/parameter.php");



if(isset($userid) && isset($userpwd) && isset($userbranch) && isset($userregion) && isset($userwfid) )
{
}
else
{
	header("location:restricted.php");
}


$tsql = "SELECT * FROM Tbl_customermasterperson WHERE custnomid='$custnomid'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
$rowCount = sqlsrv_num_rows($sqlConn);
if ( $sqlConn === false)
  die( FormatErrors( sqlsrv_errors() ) );

if(sqlsrv_has_rows($sqlConn))
{
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$custproccode = $row['custproccode'];
	}
}
sqlsrv_free_stmt( $sqlConn );

if($userpermission == "I")
{
	if($custproccode == "KGS")
	{
		header("location:./credit_kkuk/kkuk_kgs_input.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
	}
	else if($custproccode == "KKB")
	{
		header("location:./credit_kkuk/kkuk_kkb_input.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
	}
}
else
{
	if($custproccode == "KGS")
	{
		header("location:./credit_kkuk/kkuk_kgs_view.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
	}
	else if($custproccode == "KKB")
	{
		header("location:./credit_kkuk/kkuk_kkb_view.php?custnomid=$custnomid&userwfid=$userwfid&userpermission=$userpermission&buttonaction=$buttonaction");
	}
}

?>
