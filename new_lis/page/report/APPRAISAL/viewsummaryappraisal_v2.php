<?php
	
	include ("../../../lib/formatError.php");
	require ("../../../lib/open_con.php");
	require ("../../../lib/open_con_apr.php");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VIEW SUMMARY APPRAISAL</title>
<script type="text/javascript" src="../../../js/datetimepicker_css.js"></script>
<script type="text/javascript" src="../../../js/full_function.js"></script>
<link href="../../../css/crw.css" rel="stylesheet" type="text/css" />

	<style type="text/css" media="print">
		.NonPrintable
		{
		  display: none;
		}
	</style>
	
	<style type="text/css" media="print">
		.break{
		page-break-before: always;
		}
	</style> 
</head>

<BODY>
<script language="Javascript">
				name="utama";
</script>
<? 
	$tgl1 = $_REQUEST['tgl1'];
	$tgl2 = $_REQUEST['tgl2'];
	$userid = $_REQUEST['userid'];
	$userpwd = $_REQUEST['userpwd'];
	$userbranch = $_REQUEST['userbranch'];
	$userregion = $_REQUEST['userregion'];
	
	$querylevelcode = "";
	$user_level_code = 0;
		
	$tsql = "SELECT * FROM Tbl_SE_User WHERE user_id = '$userid'";
			
	$ase = sqlsrv_query($conn, $tsql);

	if ($ase === false)
		die( FormatErrors( sqlsrv_errors() ) );

	if(sqlsrv_has_rows($ase))
	{  
		while($rowse = sqlsrv_fetch_array($ase, SQLSRV_FETCH_ASSOC))
		{ 
			$user_level_code = $rowse['user_level_code'];
			$user_ao_code = $rowse['user_ao_code'];
			$user_child = $rowse['user_child'];
		}
	}
	
	if($user_level_code == 160)
	{
		$querylevelcode = "TXN_ID IN (SELECT CUSTNOMID FROM tbl_customerMasterPerson WHERE CUSTAOCODE = '$user_ao_code') AND ";
	}
	else if($user_level_code == 140)
	{
		$tempcustaocode = "";
		$array_user_child = explode("|", $user_child);
		for($i=0; $i < count($array_user_child)-1; $i++)
		{
			$tsql = "SELECT * FROM Tbl_SE_User WHERE user_id = '$array_user_child[$i]'";
			$ase = sqlsrv_query($conn, $tsql);
			if ($ase === false)
				die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($ase))
			{  
				while($rowmp = sqlsrv_fetch_array($ase, SQLSRV_FETCH_ASSOC))
				{ 
					$tempcustaocode = $rowmp['user_ao_code'];
				}
			}

			if($querylevelcode == "")
			{
				$querylevelcode = "TXN_ID IN (SELECT CUSTNOMID FROM tbl_customerMasterPerson WHERE CUSTAOCODE = '$tempcustaocode') AND ";
			}
			else
			{
				$querylevelcode = "TXN_ID IN (SELECT CUSTNOMID FROM tbl_customerMasterPerson WHERE CUSTAOCODE = '$tempcustaocode')"." OR "."$querylevelcode";
			}
		}
	}
	else if($user_level_code == 130 || $user_level_code == 120)
	{
		$querylevelcode = "TXN_BRANCH_CODE = '$userbranch' AND ";
	}
	else if($user_level_code == 100 || $user_level_code == 105 || $user_level_code == 106)
	{
		$querylevelcode = "TXN_REGION_CODE = '$userregion' AND ";
	}
	else
	{
	}
?>

<br>
	<div align=center valign=top> <strong>DAFTAR SUMMARY APPRAISAL</strong></div>
				
		</br></br>
	
		<table width=800px border=0 style="border:1px solid black;" cellpadding=0 cellspacing=0 align=center valign=top >
			
			<tr>
				<td style="border:1px solid black;" width=3% align=center><strong>NO.</strong></td>
				<td style="border:1px solid black;" width=18% align=center><strong>JUMLAH SEND TO SMA</strong></td>
				<td style="border:1px solid black;" width=18% align=center><strong>JUMLAH MARKETABLE</strong></td>
				<td style="border:1px solid black;" width=18% align=center><strong>JUMLAH NON MARKETABLE</strong></td>
				<td style="border:1px solid black;" width=18% align=center><strong>JUMLAH BACK TO AO</strong></td>
				<td style="border:1px solid black;" width=18% align=center><strong>BELUM ADA BALIKAN</strong></td>
			</tr>
			
			<?
				$jumlahsend = 0;
				$jumlahmarketable = 0;
				$jumlahnonmarketable = 0;
				$jumlahbacktoao = 0;
				$jumlahbelumbalik = 0;
				
				$i = 0;
				
				$tsql = "SELECT count(*) as jumlahsend FROM TBL_FCOL WHERE $querylevelcode TXN_ACTION = 'A' AND CONVERT(DATETIME, TXN_TIME, 105) BETWEEN CONVERT(DATETIME, '$tgl1', 105) AND DATEADD(DAY, 1, CONVERT(DATETIME, '$tgl2', 105))";
				
				$aside = sqlsrv_query($conn, $tsql);

				if ($aside === false)
					die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($aside))
				{  
					while($rowside = sqlsrv_fetch_array($aside, SQLSRV_FETCH_ASSOC))
					{ 
						$i++;
						$jumlahsend = $rowside['jumlahsend'];
					}
				}
				
				$tsql = "SELECT count(*) as jumlahmarketable FROM TBL_COL_APP WHERE AP_STATUS = 'A01' AND AP_LISREGNO IN (SELECT TXN_ID FROM TBL_FCOL WHERE TXN_ACTION = 'A' AND CONVERT(DATETIME, TXN_TIME, 105) BETWEEN CONVERT(DATETIME, '$tgl1', 105) AND CONVERT(DATETIME, '$tgl2', 105))";
				
				$aside = sqlsrv_query($conn, $tsql);

				if ($aside === false)
					die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($aside))
				{  
					while($rowside = sqlsrv_fetch_array($aside, SQLSRV_FETCH_ASSOC))
					{ 
						$jumlahmarketable = $rowside['jumlahmarketable'];
					}
				}
				
				$tsql = "SELECT count(*) as jumlahnonmarketable FROM TBL_COL_APP WHERE AP_STATUS = 'A02' AND AP_LISREGNO IN (SELECT TXN_ID FROM TBL_FCOL WHERE TXN_ACTION = 'A' AND CONVERT(DATETIME, TXN_TIME, 105) BETWEEN CONVERT(DATETIME, '$tgl1', 105) AND CONVERT(DATETIME, '$tgl2', 105))";
				
				$aside = sqlsrv_query($conn, $tsql);

				if ($aside === false)
					die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($aside))
				{  
					while($rowside = sqlsrv_fetch_array($aside, SQLSRV_FETCH_ASSOC))
					{ 
						$jumlahnonmarketable = $rowside['jumlahnonmarketable'];
					}
				}
				
				$tsql = "SELECT count(*) as jumlahbacktoao FROM TBL_COL_APP WHERE AP_STATUS = 'A03' AND AP_LISREGNO IN (SELECT TXN_ID FROM TBL_FCOL WHERE TXN_ACTION = 'A' AND CONVERT(DATETIME, TXN_TIME, 105) BETWEEN CONVERT(DATETIME, '$tgl1', 105) AND CONVERT(DATETIME, '$tgl2', 105))";
				
				$aside = sqlsrv_query($conn, $tsql);

				if ($aside === false)
					die( FormatErrors( sqlsrv_errors() ) );

				if(sqlsrv_has_rows($aside))
				{  
					while($rowside = sqlsrv_fetch_array($aside, SQLSRV_FETCH_ASSOC))
					{ 
						$jumlahbacktoao = $rowside['jumlahbacktoao'];
					}
				}
				
				$jumlahbelumbalik = $jumlahsend - $jumlahmarketable - $jumlahnonmarketable - $jumlahbacktoao;
			?>
				
			<tr>
				<td style="border:1px solid black;" width=3% align=center><? echo $i;?>.</td>
				<td style="border:1px solid black;" width=18% align=center><? echo $jumlahsend; ?></td>
				<td style="border:1px solid black;" width=18% align=center><? echo $jumlahmarketable; ?></td>
				<td style="border:1px solid black;" width=18% align=center><? echo $jumlahnonmarketable; ?></td>
				<td style="border:1px solid black;" width=18% align=center><? echo $jumlahbacktoao; ?></td>
				<td style="border:1px solid black;" width=18% align=center><? echo $jumlahbelumbalik; ?></td>
			</tr>
			
		</table>
		
</BODY>
</html>
<?
   	require("../../../lib/close_con.php");
?>