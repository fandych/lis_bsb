<?
	require ("../../lib/open_con.php");
	require ("../../lib/formatError.php");
	require ("../../requirepage/parameter.php");
  require ("../../lib/open_con_dm_mysql.php");

  $custcif = "";
	$strsql="SELECT * FROM Tbl_CustomerMasterPerson2
      				 WHERE custnomid='$custnomid'";
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
		{
			$custcif = $rows['custaplno'];
		}
	}

    $statusdoc="";
    $kondisistatusdoc = "";
		$strsqlv01="SELECT * FROM Tbl_DocPerson WHERE doc_segmen = 'FBIC'";
		$sqlconv01 = sqlsrv_query($conn, $strsqlv01);
		if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
		if(sqlsrv_has_rows($sqlconv01))
		{
			while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
			{
		    $statusdoc .= $rowsv01['doc_id'] . ",";
        $kondisistatusdoc .= "doc_type='" . $rowsv01['doc_id'] . "' or ";
			}
		}

    if ($kondisistatusdoc != "")
    {
	     $kondisistatusdoc = "AND (" . substr($kondisistatusdoc,0,strlen($kondisistatusdoc)-3) . ")";
    } 

  $ipdm = "";
  $userdm = "";
	$strsql="select control_value from ms_control where control_code='IPDM'";
	//echo $strsql;
	$sqlcon = sqlsrv_query($conn, $strsql);
	if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
	if(sqlsrv_has_rows($sqlcon))
	{
		if($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_NUMERIC))
		{
	 		$arrsplit=explode(",",$rows[0]);
	 		$ipdm = $arrsplit[0];
	 		$userdm = $arrsplit[1];
		}
	}
$key = $custcif . "_" . $custnomid;
$linkdmI = $ipdm. '/external_upload.php?dmuserid=' . $userdm . '&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=upload&key=' . $key . '&dmstatusdoc=' . $statusdoc;
$linkdmA = $ipdm. '/external_view.php?dmuserid=' . $userdm . '&username=user&userpwd=ee11cbb19052e40b07aac0ca060c23ee&dmuserorganization=PRIVATE&thecabinet=eILk6fO0&dmbranchcode=' . $userbranch . '&act=cek&key=' . $key . '&dmstatusdoc=' . $statusdoc;
	
?>
<html>
	<head>
		<title>Appraisal</title>
		<link rel="stylesheet" href="../../bin/css/css-bj.css" type="text/css" />
		<link rel="shortcut icon" href="../../bin/img/favicon.png" type="image/x-icon">
		<link href="../../bin/css/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
		<script src="../../bin/css/SpryMenuBar.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/jquery-1.11.3.js" ></script>
		<script type="text/javascript" src="../../bin/bootstrap/dist/js/bootstrap.min.js" ></script>
		
		<script type="text/javascript" src="../../bin/js/datatable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstraptable.js"></script>
		<script type="text/javascript" src="../../bin/js/bootstrap-datepicker.min.js"></script>
		
		<link href="../../bin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker.min.css"></link>
		<link rel="stylesheet" href="../../bin/bootstrap/dist/css/bootstrap-datepicker3.min.css"></link>
		
		<script language="javascript" type="text/javascript" src="../../bin/javascript/niceforms.js"></script>
		<link href="../../bin/css/table.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			$(document).ready(function() {
				$('#example').DataTable();
			} );
		</script>
		
		<script>
			function funcDelete(id) {
				var r = confirm("Are you sure want to delete?");
				if (r == true) {
					window.location = "birs_do.php?act=del&id=" +id + "&custnomid=<?=$custnomid?>&userwfid=<?=$userwfid?>&userpermission=<?=$userpermission?>&buttonaction=<?=$buttonaction?>&userbranch=<?=$userbranch?>&userregion=<?=$userregion?>&userid=<?=$userid?>&userpwd=<?=$userpwd?>";
				}
			}
			function save()
			{
				document.frm.action = "./birs_do.php?act=saveflow&custnomid=<?=$custnomid?>&userwfid=<?=$userwfid?>&userpermission=<?=$userpermission?>&buttonaction=<?=$buttonaction?>&userbranch=<?=$userbranch?>&userregion=<?=$userregion?>&userid=<?=$userid?>&userpwd=<?=$userpwd?>";
				document.frm.submit();
			}
			function uploadDocument(thelink)
			{
				varwidth = 980;
				varheight = 640;
				varx = 0;
				vary = 0;
				window.open(thelink,'lainnya','scrollbars=yes,width='+varwidth+',height='+varheight+',screenX='+varx+',screenY='+vary+',top='+varx+',left='+vary+',status=yes');
			}
		</script>
		<script type="text/javascript" src="../../js/full_function.js" ></script>
		<script type="text/javascript" src="../../js/accounting.js" ></script>
		<script type="text/javascript" src="../../js/my.js" ></script>
		<link href="../../css/d.css" rel="stylesheet" type="text/css" />
	</head>
	<body style="margin:0;">
	</br>
		<form id="frm" name="frm" method="post">
			<div style="float: right;margin-right: 207px;"><?require ("../../requirepage/btnbacktoflow.php"); ?></div>
		      <br />
		      <br />
			<div class="divcenter">
				<table border="1" style ="width:900px; border-color:black;" align="center" id="tblform">
					<tr>
						<td align="center" colspan="2" style="font-size:14pt;height:70px;">BI Result</td>
					</tr>
					<tr>
						<td align="center" colspan="2" style="font-size:12pt;">
							<div align="left" style="height:50px;padding-left:20px;padding-top:20px;">
								<a href="birs_act.php?custnomid=<?=$custnomid?>&userwfid=<?=$userwfid?>&userpermission=<?=$userpermission?>&buttonaction=<?=$buttonaction?>&userbranch=<?=$userbranch?>&userregion=<?=$userregion?>&userid=<?=$userid?>&userpwd=<?=$userpwd?>"><img src="../../bin/img/add_new.png" style="width:30px;"/> Tambah data BI Result</a>
								&nbsp; &nbsp;
								<A HREF="javascript:uploadDocument('<? echo $linkdmI ?>')"><img src="../../bin/img/upload.png" style="width:30px;"/> Upload Hasil BI Checking</A>
							</div>
						
							<div align="left" style="padding:20px;">
							<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>Nama Debitur</th>
										<th>Nama Bank</th>
										<th>Baki Debet</th>
										<th>Kolektibilitas saat ini</th>
										<th>Jenis Fasilitas</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Nama Debitur</th>
										<th>Nama Bank</th>
										<th>Baki Debet</th>
										<th>Kolektibilitas saat ini</th>
										<th>Jenis Fasilitas</th>
										<th>Action</th>
									</tr>
								</tfoot>
								<tbody>
									<?php
									$x = 0;
									$tsql = "select * from tbl_birs where custnomid = '$custnomid'";
									$sqlconv01 = sqlsrv_query($conn, $tsql);
									if ( $sqlconv01 === false)die( FormatErrors( sqlsrv_errors() ) );
									if(sqlsrv_has_rows($sqlconv01))
									{
										while($rowsv01 = sqlsrv_fetch_array($sqlconv01, SQLSRV_FETCH_ASSOC))
										{
											echo "<tr>";
											echo "<td>".$rowsv01['nama_debitur']."</td>";
											echo "<td>".$rowsv01['nama_bank']."</td>";
											echo "<td>".number_format($rowsv01['baki_debet'])."</td>";
											echo "<td>".$rowsv01['kolektibilitas']."</td>";
											echo "<td>".$rowsv01['jenis_fasilitas']."</td>";
											?>
											<td>
												<a href="birs_act.php?id=<?=$rowsv01['idx']?>&custnomid=<?=$custnomid?>&userwfid=<?=$userwfid?>&userpermission=<?=$userpermission?>&buttonaction=<?=$buttonaction?>&userbranch=<?=$userbranch?>&userregion=<?=$userregion?>&userid=<?=$userid?>&userpwd=<?=$userpwd?>"">EDIT</a> | 
												<a href="javascript:funcDelete('<?=$rowsv01['idx']?>')">DELETE</a>												
											</td>
											<?
											echo "</tr>";
											$x++;
										}
									}
									?>
								</tbody>
							</table>
							</div>
						</td>
					</tr>
<?
				if($x > 0)
				{
?>
					<tr>
						<td colspan="2" align="center" height="50px">
							<input type="button" id="btnadd" name="btnadd" value="Save" class="buttonsaveflow" onclick="save();"/>
						</td>
					</tr>
<?
				}
?>
				</table>
				<div>&nbsp;</div>
										<input type="hidden" id="txtcustlisid" name="txtcustlisid" alt="Customer LIS ID" maxlength="50" value="<?php echo $_custlistid?>" style="width:100%; background-color:#ffff00;"/>
										<input id="txtnorek" name="txtnorek" alt="No Rekening" type="hidden" maxlength="100" value="<?php echo $_norek?>" style="width:100%; background-color:#ffff00;" />
				<?
					require ("../../requirepage/hiddenfield.php");
				?>
		</form>
	</body>
</html>