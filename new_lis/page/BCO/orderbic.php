<?php

require ("../../lib/formatError.php");
require ("../../lib/open_con.php");
$status = 0;
$userid=$_REQUEST['userid'];
$userpwd=$_REQUEST['userpwd'];
$userbranch=$_REQUEST['userbranch'];
$userregion=$_REQUEST['userregion'];
$userwfid=$_REQUEST['userwfid'];

$strsql = "SELECT branch_flag_bi
				FROM Tbl_Branch
				WHERE branch_code='".$userbranch."'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursorType);
if ( $sqlConn === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$branchorderseq = $row['branch_flag_bi'];
	}
}
sqlsrv_free_stmt( $sqlConn );
   
  
//SELECT From Tbl_Workflow & PrevFlow
$strsql = "select * from Tbl_Workflow where wf_id='".$userwfid."'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursorType);
if($sqlConn === false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$wfflag = $row['wf_flag'];
	}
}
sqlsrv_free_stmt( $sqlConn );

$checkbichecking="";
$tsql = "select RIGHT(WF_FLAG,1) as 'a' from Tbl_Workflow where wf_id='$userwfid'";

$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
if($sqlConn === false){die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
	{
		$checkbichecking=$row['a'];
	}
}
sqlsrv_free_stmt( $sqlConn );

$kondisibranch="";
if ($checkbichecking=="0")
{
	 $kondisibranch = "AND (a.txn_branch_code='$userbranch' or a.txn_branch_code='$userregion' or a.txn_branch_code='888'";
}

$tsql = "select brancH_code from Tbl_Branch where branch_flag_bi='".$userbranch."'";
$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
$params = array(&$_POST['query']);
$sqlConn = sqlsrv_query($conn, $tsql, $params, $cursorType);
if($sqlConn === false){ die(FormatErrors(sqlsrv_errors()));}
if(sqlsrv_has_rows($sqlConn))
{
	$rowCount = sqlsrv_num_rows($sqlConn);
	while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_NUMERIC))
	{
		$kondisibranch .= " or a.txn_branch_code='$row[0]'";
	}
}
sqlsrv_free_stmt( $sqlConn );

$kondisibranch .=")";


?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Expires" CONTENT="0">
<meta http-equiv="Cache-Control" CONTENT="no-cache">
<meta http-equiv="Pragma" CONTENT="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Order Bi Checking</title>
	<link href="../../css/crw.css" rel="stylesheet" type="text/css" />
	<Script Language="JavaScript">
  	function cek()
  	{
  		varreturn = "";
<?
	$strsql = "	select a.txn_id,isnull(c.tua_userid,'') as 'namaorang',e.branch_name,e.branch_code,
				'name'= case when d.custsex='0' then custbusname else custfullname end,ttlsib,ttlorder,
				'status' = case 
				when cast(ttlorder as int)=0 then 'belum pernah di order' 
				when cast(ttlorder as int) < cast(ttlsib as int) then 'Sebagian sudah pernah di order oleh ' + isnull(c.tua_userid,'')
				when cast(ttlorder as int) = cast(ttlsib as int) then 'sudah pernah di order oleh ' + isnull(c.tua_userid,'') end ,
				j.txn_action
				from Tbl_FBIC a 
				join (select cast(COUNT(*) as int)  as ttlsib,custnomid from Tbl_CustomerSibling where flagdelete='0' group by custnomid
				) b on a.txn_id = b.custnomid 
				left join Tbl_FBIO j on j.txn_id = a.txn_id 
				left join Tbl_FBIR u on u.txn_id = a.txn_id 
				left join Tbl_TemporariUserAkses c on a.txn_id = c.tua_nomid 
				and j.txn_user_id = c.tua_userid
				join Tbl_CustomerMasterPerson d on d.custnomid = b.custnomid 
				join Tbl_Branch e on d.custbranchcode = e.branch_code 
				join (select SUM(case when cast(orderseq as int)>0 then 1 else 0 end) as ttlorder,custnomid 
				from Tbl_CustomerSibling group by custnomid) f on a.txn_id = f.custnomid  

				where a.txn_action='A' and isnull(u.txn_action,'')='' and (c.tua_userid='".$userid."' or  isnull(c.tua_userid,'a')='a') ".$kondisibranch." ";
				
	$cursorType = array("Scrollable" => SQLSRV_CURSOR_KEYSET);
	$params = array(&$_POST['query']);
	$sqlConn = sqlsrv_query($conn, $strsql, $params, $cursorType);
	if	($sqlConn === false)die(FormatErrors( sqlsrv_errors()));
	if(sqlsrv_has_rows($sqlConn))
	{
		$rowCount = sqlsrv_num_rows($sqlConn);
		while( $row = sqlsrv_fetch_array( $sqlConn, SQLSRV_FETCH_ASSOC))
		{
			$vartemp = $row['txn_id'];
			?>
			if (document.orderBIC.<? echo $vartemp; ?>.checked == true)
			{
			varreturn = varreturn + '<? echo $vartemp; ?>' + ",";
			} 
			<?
		}
	}

	sqlsrv_free_stmt( $sqlConn );
?>
		  if(varreturn == "")
		  {
		  	alert("Harap Pilih Minimal Satu");
		  	return false;
		  }
		  //document.orderBIC.target = "utama";
		  document.orderBIC.datachecked.value = varreturn;
		  document.orderBIC.action = "nextorderBIC.php";
		  submitform = window.confirm("<?=$confmsg;?>")
		  if (submitform == true)
		  {
			  document.orderBIC.submit();
			  return true;
		  }
   }
  </Script>
</head>

<body>
<?
if ($status == 0)
{
?>
<form id="orderBIC" name="orderBIC" method="post" action="do_orderBIC.php">
<table width="800" border="0" class="table" align="center">
	<tr>
		<td align="center" class="judul" ><b>ORDER BI CHECKING</b></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<table style="text-align:center;" width="100%" border="1">
				<tr>
					<td width="20%">BIC ID</td>
					<td width="20%">Nama Nasabah</td>
					<td width="20%">Kode cabang</td>
					<td width="20%">Status</td>
					<td width="5%">Action</td>
					<!--<td width="15%">Back To AO</td>-->
				</tr>
				<?
				
				$strsql = " 
							select a.txn_id,isnull(c.tua_userid,'') as 'namaorang',e.branch_name,e.branch_code,
							'name'= case when d.custsex='0' then custbusname else custfullname end,ttlsib,ttlorder,
							'status' = case 
							when cast(ttlorder as int)=0 then 'belum pernah di order' 
							when cast(ttlorder as int) < cast(ttlsib as int) then 'Sebagian sudah pernah di order oleh ' + isnull(c.tua_userid,'')
							when cast(ttlorder as int) = cast(ttlsib as int) then 'sudah pernah di order oleh ' + isnull(c.tua_userid,'') end ,
							j.txn_action
							from Tbl_FBIC a 
							join (select cast(COUNT(*) as int)  as ttlsib,custnomid from Tbl_CustomerSibling where flagdelete='0' group by custnomid
							) b on a.txn_id = b.custnomid 
							left join Tbl_FBIO j on j.txn_id = a.txn_id 
							left join Tbl_FBIR u on u.txn_id = a.txn_id 
							left join Tbl_TemporariUserAkses c on a.txn_id = c.tua_nomid 
							and j.txn_user_id = c.tua_userid
							join Tbl_CustomerMasterPerson d on d.custnomid = b.custnomid 
							join Tbl_Branch e on d.custbranchcode = e.branch_code 
							join (select SUM(case when cast(orderseq as int)>0 then 1 else 0 end) as ttlorder,custnomid 
							from Tbl_CustomerSibling group by custnomid) f on a.txn_id = f.custnomid  

							where a.txn_action='A' and isnull(u.txn_action,'')='' and (c.tua_userid='".$userid."' or  isnull(c.tua_userid,'a')='a') ".$kondisibranch." ";
							//echo $strsql;
				$sqlcon = sqlsrv_query($conn, $strsql);
				if ( $sqlcon === false)die(FormatErrors( sqlsrv_errors()));
					if(sqlsrv_has_rows($sqlcon))
					{
						while($row = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
						{
							$custnomid = $row['txn_id'];
							//$entryseq = $row['entryseq'];
							$name = $row['name'];
							$branchcode = $row['branch_code'];
							$branchname = $row['branch_name'];
							?>
							<tr>
								<td><a target="haha" href="../preview/previewall/previewall_n.php?custnomid=<?echo $custnomid?>&userwfid=&userpermission=&buttonaction=&userbranch=&userregion=&userid=&userpwd="><? echo $custnomid; ?></a></td>
								<td><? echo $name;?></td>
								<td><? echo $branchcode."\t-\t".$branchname;?></td>
								<td><? echo $row['status']; ?></td>
								<td><input type = "checkbox" name="<? echo $custnomid;?>" id="<? echo $custnomid;?>" value='Y'> </td>
							</tr>
							<?
						}
					}
				?>
				</table>
			</td>
		</tr>
		<tr>
			<td align = "center">&nbsp; </td>
		</tr>
		<tr>
			<td align = "center">
				<input id="orderBIC" name="orderBIC" class="blue" type="button" value="Submit" onClick="cek()" />
				<input type="hidden" name="datachecked"/>
				<input type="hidden" name="userpwd"  value="<? echo $userpwd ; ?>">
				<input type="hidden" name="userbranch"  value="<? echo $userbranch ; ?>">
				<input type="hidden" name="userregion"  value="<? echo $userregion  ; ?>">
				<input type="hidden" name="userid"  value="<? echo $userid; ?>">
				<input type="hidden" name="userwfid"  value="<? echo $userwfid; ?>">
			</td>
		</tr>
	</table>
</form>
<?
}
	require('../../lib/close_con.php'); 
?>