<?php
require ("../../lib/config.php");
require ("../../lib/formatError.php");
require ("../../requirepage/parameter.php");
//require ("../../requirepage/security.php");

$sh_desc="";
$sql2    = "SELECT sh_desc FROM DW_SourceheadNew WHERE sh_nom_id = '$custnomid'";
$result2 = sqlsrv_query($conn, $sql2);
if ( $result2 === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($result2))
{
    while($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC))
    {
        $sh_desc = $row2['sh_desc'];
    }
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>Phone Verification Information</title>
  <link rel="stylesheet" href="../../css/BJstyle.css" type="text/css">
</head>
<body>
<div id="content">
  <table width="100%" cellpadding="2" cellspacing="1" id="table-data" border="0px">
    <tr>
      <td colspan="9" align="center" style="background-color:#000080; color:#FFFFFF;">
        <b>SCORING</b>
      </td>
    </tr>
    <?
			$jmlscore = 0;
			$i = 1;
			$sql2    = "SELECT * FROM DW_SourceDetailNew WHERE sd_nom_id = '$custnomid'";
            //echo $sql2;
			$result2 = sqlsrv_query($conn, $sql2);
			if ( $result2 === false)die( FormatErrors( sqlsrv_errors() ) );
			if(sqlsrv_has_rows($result2))
			{
				while($row2 = sqlsrv_fetch_array($result2, SQLSRV_FETCH_ASSOC))
				{
					$jmlscore = $jmlscore + $row2['sd_score'];
					$sd_field = $row2['sd_field'];

          $sd_class = $sd_field + 1;
          /* ganjil atau genap */
          if ($sd_class % 2 == 0) {
            $class_name = 'even'; /* class_name = 'even' jika genap */
          } else {
            $class_name = 'odd'; /* class_name = 'odd' jika ganjil */
          }
		?>
        <tr class="<?php echo $class_name; ?>">
            <td width="5%" align="center"><?=$i;?></td>
			<td width="40%" align="left"><b><?=$row2['sd_pm_id']?></b></td>
			<td width="2%" class="titikDua">:</td>
			<td style="text-align:right;"><?=$row2['sd_score']?></td>
		</tr>
        <?	  $i++;
				}
			}
		?>
    <tr>
  		<td colspan="4" align="center" style="font-size:14px; background-color:rgb(233,237,236);">
        <b>TOTAL SCORE</b>
      </td>
  	</tr>
  	<tr>
  		<td colspan="4" align="center" style="font-size:14px; color:#058E19; background-color:#FAFDEC;">
        <b><?=$jmlscore;?></b>
      </td>
  	</tr>
  	<tr>
  		<td colspan="4" align="center" style="font-size:14px; color:#058E19; background-color:#FAFDEC;">
        <?=$sh_desc?>
      </td>
  	</tr>

  </table>

  <?

  $totalpalfond=0;
$strsql = " select a.custnomid,a.custfacseq,b.credit_need_name,
			c.produk_type_description,a.custcreditplafond,a.custcreditlong
			from tbl_CustomerFacility a
			left join Tbl_CreditNeed b on a.custcreditneed = b.credit_need_code
			left join Tbl_KodeProduk c on c.produk_loan_type = a.custcredittype
			where a.custnomid ='".$custnomid."'";

$sqlcon = sqlsrv_query($conn, $strsql);
  if ( $sqlcon === false)die( FormatErrors( sqlsrv_errors() ) );
if(sqlsrv_has_rows($sqlcon))
{
	echo '
		<table class="tbl100" align="center" border="0" style="margin-top:20px;">
		';
	while($rows = sqlsrv_fetch_array($sqlcon, SQLSRV_FETCH_ASSOC))
	{
		$totalpalfond+=$rows['custcreditplafond'];
		echo '
			<tr>
				<td style="width:180px;text-align:left;">Tujuan Pengajuan Kredit</td>
				<td style="width:180px;text-align:left;">'.$rows['credit_need_name'].'</td>
				<td rowspan="2" style="text-align:right;">&nbsp;
					<!--<input type="button" id="btnE'.$rows['custfacseq'].'" name="btnE'.$rows['custfacseq'].'" class="button" value="Edit" onclick="btnonclick(this.id)" />-->&nbsp;
				</td>
			</tr>
			<tr>
				<td style="text-align:left;">Jenis Fasilitas</td>
				<td style="text-align:left;">'.$rows['produk_type_description'].'</td>
			</tr>
			<tr>
				<td style="text-align:left;">Plafond </td>
				<td style="text-align:left;">'.numberFormat($rows['custcreditplafond']).'</td>
				<td rowspan="2" style="text-align:right;">
					<!--<input type="button" id="btnD'.$rows['custfacseq'].'" name="btnD'.$rows['custfacseq'].'" class="buttonneg" value="Del" onclick="btnonclick(this.id)" />-->&nbsp;
				</td>
			</tr>
			<tr>
				<td style="text-align:left;">Jangka Waktu</td>
				<td style="text-align:left;">'.$rows['custcreditlong'].'</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
		';
	}
	echo '
		</table>
		<input type="hidden" name="totalplafond" id="totalplafond" value="'.$totalpalfond.'" />
	';
}

  ?>
</div>
</body>
</html>
