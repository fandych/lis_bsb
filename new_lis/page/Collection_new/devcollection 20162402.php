<?php

require ("../../lib/class.sqlserverCOLL.php");

$SQL = new SQLSRV();
$SQL->connect();
date_default_timezone_set("Asia/Jakarta");

$_mode=$_REQUEST['_mode'];
if($_mode=="checkdata")
{

    
    $_custnomid = $_REQUEST['_custnomid'];
    $tsqlUpdate = " select a._cust_name,
                    b._custnomid,
                    b._txn_tagihanke,
                    (select COUNT(*)+1 from collect_custpayment where _payment_tagihanke = b._txn_tagihanke and _custnomid =b._custnomid) as '_pembayaranke',
                    b._txn_nominal-(select isnull(SUM(_payment_nominal),0) from collect_custpayment where _payment_tagihanke = b._txn_tagihanke and _custnomid =b._custnomid) as '1',
                    CONVERT(VARCHAR, b._txn_tgljatuhtempo, 105) as '_tempo',
                    substring(CONVERT(varchar, CAST(b._txn_nominal-(select isnull(SUM(_payment_nominal),0) from collect_custpayment where _payment_tagihanke = b._txn_tagihanke and _custnomid =b._custnomid) AS money) , 1),0,LEN(CONVERT(varchar, CAST(b._txn_nominal-(select isnull(SUM(_payment_nominal),0) from collect_custpayment where _payment_tagihanke = b._txn_tagihanke and _custnomid =b._custnomid) AS money) , 1))-2) as '_txn_nominal'
                    from collect_custmaster a 
                    join collect_custtxn b on a._custnomid =b._custnomid
                    where b._custnomid='".$_custnomid."'
                    and b._txn_statusbayar ='N'";
                    //echo $tsqlUpdate;
    $SQL->executeQuery($tsqlUpdate);
    $result = $SQL->lastResults;
    
    if($result)
    {
        $output = json_encode(array('status' => 'success',
									'messages' => 'success',
									'data' => $result
									));	
    }
    else
    {
        $output = json_encode(array('status' => 'No Data',
									'messages' => 'No Data'
									));	
    }    
}
elseif($_mode=="payment")
{
    
    $_payment_nominal=$_REQUEST['_payment_nominal'];
    $_payment_ket1=$_REQUEST['_payment_ket1'];
    $_custnomid=$_REQUEST['_custnomid'];
    $_payment_tagihanke=$_REQUEST['_payment_tagihanke'];
    $_txn_statusbayar=$_REQUEST['_txn_statusbayar'];
    $_payment_tglbayar=date("Y-m-d H:i:s");
    
    $strsql="
    update collect_custtxn set _txn_statusbayar='".$_txn_statusbayar."' where _custnomid='".$_custnomid."' and _txn_tagihanke='".$_payment_tagihanke."'
    insert into collect_custpayment(_custnomid,_payment_tagihanke,_payment_tglbayar,_payment_nominal,_payment_channel_id,_payment_ket1)
    values('".$_custnomid."','".$_payment_tagihanke."','".$_payment_tglbayar."','".$_payment_nominal."','DEV','".$_payment_ket1."')";
    
    $SQL->executeNonQuery($strsql);
    $output = json_encode(array('status' => 'success',
											'messages' => 'Pembayaran Berhasil',
                                            'dateserver' => date("d-m-Y"),
                                            'timeserver' => date("H:i:s")
											));	
}
else
{
	$output = json_encode(array('status' => 'failed',
											'messages' => 'unknown mode'
											));								
}

$length = mb_strlen($output, '8bit');
header("Content-Length: $length");
print($output);
?>