<?php
include ("../../lib/formatError.php");
require ("../../lib/open_con.php");
require("../../lib/sqlsrv.lis.php");
$db_lis = new DB_LIS();
$db_lis->connect();
$a=$_GET['a'];

if($a == "CABANG")
{
	$cabang = $_REQUEST['cabang'];

	$cust_dinas = "SELECT DISTINCT(L3COD2), L3DES2 FROM param_branch_dinas WHERE L3COD1 = '$cabang' AND L3COD1 <> ''";
	$db_lis->executeQuery($cust_dinas);
	$cust_dinas = $db_lis->lastResults;

	echo '
		<select id="dinas" name="dinas" onchange="changedinas()">
		<option value="" selected="selected" >-- Pilih Dinas --</option>';
		for($x=0;$x<count($cust_dinas);$x++){
		$code = $cust_dinas[$x]['L3COD2'];
		$name = $cust_dinas[$x]['L3DES2'];
		echo '<option value="'.$code.'">'.$code.' - '.$name.'</option>';
		}
	echo '</select>';

}
else if($a == "DINAS")
{
	$cabang = $_REQUEST['cabang'];
	$dinas = $_REQUEST['dinas'];

	$cust_dinas = "SELECT DISTINCT(L3COD3), L3DES3 FROM param_branch_dinas WHERE L3COD1 = '$cabang' AND L3COD2 = '$dinas' AND L3COD1 <> ''";
	$db_lis->executeQuery($cust_dinas);
	$cust_dinas = $db_lis->lastResults;

	echo '
		<select id="sub_dinas" name="sub_dinas" onchange="changesubdinas()">
		<option value="" selected="selected" >-- Pilih Sub Dinas --</option>';
		for($x=0;$x<count($cust_dinas);$x++){
		$code = $cust_dinas[$x]['L3COD3'];
		$name = $cust_dinas[$x]['L3DES3'];
		echo '<option value="'.$code.'">'.$code.' - '.$name.'</option>';
		}
	echo '</select>';
}
else if($a == "SUBDINAS")
{
	$cabang = $_REQUEST['cabang'];
	$dinas = $_REQUEST['dinas'];
	$subdinas = $_REQUEST['subdinas'];

	$cust_dinas = "SELECT DISTINCT(L3COD4), L3DES4 FROM param_branch_dinas WHERE L3COD1 = '$cabang' AND L3COD2 = '$dinas' AND L3COD3 = '$subdinas' AND L3COD1 <> ''";
	$db_lis->executeQuery($cust_dinas);
	$cust_dinas = $db_lis->lastResults;

	echo '
		<select id="sub_sub_dinas" name="sub_sub_dinas">
		<option value="" selected="selected" >-- Pilih Sub - Sub Dinas --</option>';
		for($x=0;$x<count($cust_dinas);$x++){
		$code = $cust_dinas[$x]['L3COD4'];
		$name = $cust_dinas[$x]['L3DES4'];
		echo '<option value="'.$code.'">'.$code.' - '.$name.'</option>';
		}
	echo '</select>';
}
else
{
	echo 'unknown dinas';
}

?>
