<?
	require_once ("../../lib/formatError.php");
	require_once ("../../lib/open_con.php");
	require_once ("../../lib/open_con_apr.php");
	
	$liscolid = $_GET["liscolid"];
	$custnomid = $_GET["custnomid"];
	
	// BAGIAN I
	
	$namacalondebitur = "";
	$alamat = "";
	$cabang = "";
	$namaao = "";
	$tanggalsurvey = "";
	$tanggalreport = "";
	$yangdihubungi = "";
	$bidangusaha = "";
	
	$tsql = "SELECT CUST_NAME, CUST_ADDR1, CUST_ADDR2, CUST_ADDR3, (SELECT BRANCH_NAME FROM TBL_BRANCH WHERE BRANCH_CODE IN (SELECT AP_BRANCH FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid')) AS CABANG, (SELECT USER_NAME FROM TBL_SE_USER WHERE USER_ID IN (SELECT TXN_USER_ID FROM TBL_FSTART  WHERE TXN_ID = '$custnomid')) AS NAMAAO, (SELECT AP_DATE FROM TBL_COL_APP WHERE AP_LISREGNO = '$custnomid') AS TANGGALSURVEY, CUST_YANGDIHUBUNGI, (SELECT BT_DESC FROM RFBUSINESSTYPE WHERE BT_CODE IN (SELECT CUST_USAHA FROM TBL_COL_CUSTOMER WHERE AP_LISREGNO = '$custnomid')) AS BIDANGUSAHA FROM TBL_COL_CUSTOMER WHERE AP_LISREGNO = '$custnomid'";
	$a = sqlsrv_query($conn, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$namacalondebitur = $row["CUST_NAME"];
			$alamat = $row["CUST_ADDR1"];
			$cabang = $row["CABANG"];
			$namaao = $row["NAMAAO"];
			$tanggalsurvey = $row["TANGGALSURVEY"]->format('d F Y');
			$tanggalreport = $row["TANGGALSURVEY"]->format('d F Y');
			$yangdihubungi = $row["CUST_YANGDIHUBUNGI"];
			$bidangusaha = $row["BIDANGUSAHA"];
			
			if($row["CUST_ADDR2"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR2"];
			}
			
			if($row["CUST_ADDR3"] != "")
			{
				$alamat = $alamat." ".$row["CUST_ADDR3"];
			}
		}
	}
	
	// BAGIAN II
	
	$LOKASI = ""; //
	$NOHAKTANGGUNGAN = "";
	$TGLHAKTANGGUNGAN = "";
	$HUBUNGANPEMEGANGHAK = "";
	$IDENTIFIKASI = "";
	$TOPOGRAFI = "";
	$BENTUK = "";
	$UKURANPANJANG = "";
	$UKURANLEBAR = "";
	$BATAS1 = "";
	$BATAS2 = "";
	$BATAS3 = "";
	$BATAS4 = "";
	$ARAH1 = "";
	$ARAH2 = "";
	$ARAH3 = "";
	$ARAH4 = "";
	$ISI1 = "";
	$ISI2 = "";
	$ISI3 = "";
	$ISI4 = "";
	$DAERAHTERMASUK = "";
	
	$tsql = "SELECT (SELECT RFCERTTYPE.CERTTYPE_DESC FROM RFCERTTYPE WHERE RFCERTTYPE.CERTTYPE_ID = APPRAISAL_LND.CERT_TYPE) AS JENISSERTIFIKAT,
			CERT_NO AS NOMORSERTIFIKAT, CERT_OWNER AS PEMEGANGHAK, CERT_LAND_DIM AS LUASTANAH, CERT_DUEDATE AS TGLBERAKHIRSERTIFIKAT, CERT_DATE AS TGLTERBITSERTIFIKAT,
			HYPOTIC_NO AS NOHAKTANGGUNGAN, HYPOTIC_DATE AS TGLHAKTANGGUNGAN, (SELECT RFRELATION.REL_DESC FROM RFRELATION WHERE RFRELATION.REL_CODE = APPRAISAL_LND.REL_CODE) AS HUBUNGANPEMEGANGHAK, 
			(SELECT RFIDENTIFICATION.IDTF_DESC FROM RFIDENTIFICATION WHERE RFIDENTIFICATION.IDTF_CODE = APPRAISAL_LND.IDTF_CODE) AS IDENTIFIKASI,
			(SELECT RFTOPOGRAPHY.TPGF_DESC FROM RFTOPOGRAPHY WHERE RFTOPOGRAPHY.TPGF_CODE = APPRAISAL_LND.TPGF_CODE) AS TOPOGRAFI,
			(SELECT RFBENTUKTANAH.BTKTNH_DESC FROM RFBENTUKTANAH WHERE RFBENTUKTANAH.BTKTNH_CODE = APPRAISAL_LND.BTKTNH_CODE) AS BENTUK,
			LND_LENM2 AS UKURANPANJANG, LND_WIDM2 AS UKURANLEBAR,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE1) AS BATAS1,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE2) AS BATAS2,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE3) AS BATAS3,
			(SELECT ENUMSIDE.SIDE_DESC FROM ENUMSIDE WHERE ENUMSIDE.SIDE_CODE = APPRAISAL_LND.BDRY_SIDE4) AS BATAS4,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR1) AS ARAH1,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR2) AS ARAH2,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR3) AS ARAH3,
			(SELECT ENUMCARDINALDIR.CDNDIR_DESC FROM ENUMCARDINALDIR WHERE ENUMCARDINALDIR.CDNDIR_CODE = APPRAISAL_LND.BDRY_CDNDIR4) AS ARAH4,
			BDRY_DATA1 AS ISI1, BDRY_DATA2 AS ISI2, BDRY_DATA3 AS ISI3, BDRY_DATA4 AS ISI4, 
			(SELECT RFKONDISIDAERAH.CNDDRH_DESC FROM RFKONDISIDAERAH WHERE RFKONDISIDAERAH.CNDDRH_CODE = APPRAISAL_LND.CNDDRH_CODE) AS DAERAHTERMASUK
			 FROM APPRAISAL_LND WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			//$LOKASI = $row["JENISSERTIFIKAT"];
			/*$JENISSERTIFIKAT = $row["JENISSERTIFIKAT"];
			$NOMORSERTIFIKAT = $row["NOMORSERTIFIKAT"];
			$PEMEGANGHAK = $row["PEMEGANGHAK"];
			$LUASTANAH = $row["LUASTANAH"];
			$TGLBERAKHIRSERTIFIKAT = $row["TGLBERAKHIRSERTIFIKAT"];
			$TGLTERBITSERTIFIKAT = $row["TGLTERBITSERTIFIKAT"];*/
			//$NOGSSU = $row["JENISSERTIFIKAT"]; //
			//$TGLGSSU = $row["JENISSERTIFIKAT"]; //
			$NOHAKTANGGUNGAN = $row["NOHAKTANGGUNGAN"];
			$TGLHAKTANGGUNGAN = $row["TGLHAKTANGGUNGAN"];
			$HUBUNGANPEMEGANGHAK = $row["HUBUNGANPEMEGANGHAK"];
			$IDENTIFIKASI = $row["IDENTIFIKASI"];
			$TOPOGRAFI = $row["TOPOGRAFI"];
			$BENTUK = $row["BENTUK"];
			$UKURANPANJANG = $row["UKURANPANJANG"];
			$UKURANLEBAR = $row["UKURANLEBAR"];
			$BATAS1 = $row["BATAS1"];
			$BATAS2 = $row["BATAS2"];
			$BATAS3 = $row["BATAS3"];
			$BATAS4 = $row["BATAS4"];
			$ARAH1 = $row["ARAH1"];
			$ARAH2 = $row["ARAH2"];
			$ARAH3 = $row["ARAH3"];
			$ARAH4 = $row["ARAH4"];
			$ISI1 = $row["ISI1"];
			$ISI2 = $row["ISI2"];
			$ISI3 = $row["ISI3"];
			$ISI4 = $row["ISI4"];
			$DAERAHTERMASUK = $row["DAERAHTERMASUK"];
			
			/*if($TGLBERAKHIRSERTIFIKAT != "")
			{
				$TGLBERAKHIRSERTIFIKAT = $row["TGLBERAKHIRSERTIFIKAT"]->format('d F Y');
			}
			if($TGLTERBITSERTIFIKAT != "")
			{
				$TGLTERBITSERTIFIKAT = $row["TGLTERBITSERTIFIKAT"]->format('d F Y');
			}*/
			if($TGLHAKTANGGUNGAN != "")
			{
				$TGLHAKTANGGUNGAN = $row["TGLHAKTANGGUNGAN"]->format('d F Y');
			}
		}
	}
	
	$alamatapr = "";
	
	$tsql = "SELECT * FROM COLLATERAL where COLMASTER_ID in (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$alamatapr = $row['COL_ADDR1'];
			
			if($row['COL_ADDR2'] != "")
			{
				$alamatapr = $alamatapr." ".$row['COL_ADDR2'];
			}
			
			if($row['COL_ADDR3'] != "")
			{
				$alamatapr = $alamatapr." ".$row['COL_ADDR3'];
			}
		}
	}
	
	// BAGIAN III
	
	$RANGKAUTAMA = "";
	$LANTAI = "";
	$DINDING = "";
	$LANGIT = "";
	$RANGKAATAP = "";
	$ATAP = "";
	$JUMLAHLANTAI = "";
	$LUASIMB = "";
	$LUASFISIK ="";
	$PEMBAGIANRUANG = "";
	$FASILITASBANGUNAN = "";
	$TAHUNDIBANGUN = "";
	$NOIMB = "";
	$TGLIMB = "";
	$DIHUNIOLEH = "";
	$KETERANGAN = "";
	
	$tsql = "SELECT RGKUT_CODE AS RANGKAUTAMA, LNTAI_CODE AS LANTAI, DDING_CODE AS DINDING, LNGIT_CODE AS LANGIT,  RGKAT_CODE AS RANGKAATAP,
			ATAP_CODE AS ATAP, JMLLT_CODE AS JUMLAHLANTAI, IMB_DIM AS LUASIMB, FIS_DIM AS LUASFISIK,
			PEMBAGIAN_RUANG AS PEMBAGIANRUANG, FASILITAS_BANGUNAN AS FASILITASBANGUNAN, BUILTYEAR AS TAHUNDIBANGUN, IMB_NO AS NOIMB, IMB_DATE AS TGLIMB,
			HUNI_CODE AS DIHUNIOLEH, NOTE AS KETERANGAN
			FROM APPRAISAL_BLD WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_BLD WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAUTAMA = $row["RANGKAUTAMA"];
			$LANTAI = $row["LANTAI"];
			$DINDING = $row["DINDING"];
			$LANGIT = $row["LANGIT"];
			$RANGKAATAP = $row["RANGKAATAP"];
			$ATAP = $row["ATAP"];
			$JUMLAHLANTAI = $row["JUMLAHLANTAI"];
			$LUASIMB = $row["LUASIMB"];
			$LUASFISIK =$row["LUASFISIK"];
			$PEMBAGIANRUANG = $row["PEMBAGIANRUANG"];
			$FASILITASBANGUNAN = $row["FASILITASBANGUNAN"];
			$TAHUNDIBANGUN = $row["TAHUNDIBANGUN"];
			$NOIMB = $row["NOIMB"];
			$TGLIMB = $row["TGLIMB"];
			$DIHUNIOLEH = $row["DIHUNIOLEH"];
			$KETERANGAN = $row["KETERANGAN"];
			
			if($TGLIMB != "")
			{
				$TGLIMB = $row["TGLIMB"]->format('d F Y');
			}
		}
	}
	
	$tsql = "SELECT * FROM RFRANGKAUTAMA WHERE RGKUT_CODE = '$RANGKAUTAMA'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAUTAMA = $row["RGKUT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFLANTAI WHERE LNTAI_CODE = '$LANTAI'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$LANTAI = $row["LNTAI_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFDINDING WHERE DDING_CODE = '$DINDING'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$DINDING = $row["DDING_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFRANGKAATAP WHERE RGKAT_CODE = '$RANGKAATAP'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$RANGKAATAP = $row["RGKAT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFATAP WHERE ATAP_CODE = '$ATAP'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$ATAP = $row["ATAP_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFJUMLAHLANTAI WHERE JMLLT_CODE = '$JUMLAHLANTAI'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$JUMLAHLANTAI = $row["JMLLT_DESC"];
		}
	}
	
	$tsql = "SELECT * FROM RFPENGHUNI WHERE HUNI_CODE = '$DIHUNIOLEH'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$DIHUNIOLEH = $row["HUNI_DESC"];
		}
	}
	
	// BAGIAN VII
	
	$LUASTANAH = "";
	$NILAITANAHPERM2 = "";
	$NILAITANAHTOTAL = "";
	$SAFETYMARGIN = "";
	$NILAILIKUIDASI = "";
	
	$tsql = "SELECT * FROM APPRAISAL_TYPE6_PENILAIAN WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$LUASTANAH = $row["LUAS_TANAH"];
			$NILAITANAHPERM2 = $row["NILAI_TANAH_PERM2"];
			$NILAITANAHTOTAL = $row["NILAI_TANAH_TOTAL"];
			$SAFETYMARGIN = $row["SAFETY_MARGIN"];
			$NILAILIKUIDASI = $row["NILAI_LIKUIDASI"];
		}
	}
	
	// BAGIAN VIII
	
	$OPINIPOSITIF = "";
	$OPININEGATIF = "";
	$KONDISIPASAR = "";
	$CATATAN = "";
	
	$tsql = "SELECT EVALPOS AS OPINIPOSITIF, EVALNEG AS OPININEGATIF, MK_CODE AS KONDISIPASAR, EVALNOTE AS CATATAN FROM APPRAISAL_OPINI WHERE COLMASTER_ID IN (SELECT COLMASTER_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$OPINIPOSITIF = $row["OPINIPOSITIF"];
			$OPININEGATIF = $row["OPININEGATIF"];
			$KONDISIPASAR = $row["KONDISIPASAR"];
			$CATATAN = $row["CATATAN"];
		}
	}
	
	$tsql = "SELECT * FROM RFMK WHERE MK_ID = '$KONDISIPASAR'";
	$a = sqlsrv_query($connapr, $tsql);
	if ( $a === false)
	die( FormatErrors( sqlsrv_errors() ) );
	
	if(sqlsrv_has_rows($a))
	{ 	
		while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
		{
			$KONDISIPASAR = $row["MK_DESC"];
		}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head><title>
	Letter Collateral Type 08 Page
</title><link href="../include/letterStyle.css" rel="stylesheet" />

    <style type="text/css">
        .style1
        {
            width: 1%;   
        }

        .tablebox1
        {
            background-color:white;
            border-collapse: collapse; 
            table-layout: fixed; 
            border: 1px solid #000000;         
        }
        .judul
        {
           background-color: #CCCCCC; 
        }
    
    </style>
    <script type="text/javascript">
        function onPrint()
        {
            var f = document.form1;
            f.btn_print.style.display = "none"; window.print();
            f.btn_print.style.display = "";
        
        }
    </script>
    
</head>
<body>
    <form name="form1" method="post" action="TPL_ColType08.aspx?tc=9.0&amp;regno=00012%2f805%2f2013&amp;curef=PE130206000001&amp;mcolid=PE13020600000101&amp;cetak=html&amp;rnq=1" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJODc1NDE0MDExD2QWAgIDD2QWmgECDQ8PFgIeBFRleHQFBEtJT1NkZAIPDw8WAh8ABSUwMDAwMDAwMDUvODA1L0xhcC1BcHAvRkFDMDQvICBJSS8yMDEzZGQCEQ8PFgIfAAULTkVXIFRBS1NBU0lkZAITDw8WAh8ABQpQVCBDQUxERVJBZGQCFQ8PFgIfAAUDMSAgZGQCFw8PFgIfAAUKS29udHJha3RvcmRkAhkPDxYCHwAFElJFR0lPTkFMIEpBS0FSVEEgSWRkAhsPDxYCHwBlZGQCHQ8PFgIfAAUPNiBGZWJydWFyeSAyMDEzZGQCHw8PFgIfAAUPNiBGZWJydWFyeSAyMDEzZGQCIQ9kFgICAw9kFgICAQ8PFgIfAGRkZAIjDw8WAh8ABQRvY2VwZGQCJQ8PFgIfAAWRAWFsYW1hdCBqYW1pbmFuICwgYXByIGFsYW1hdCBqYW1pbmFuICwgYXByIGFsYW1hdCBqYW1pbmFuIDEsIEtlbC4vIERlc2EgYXByIGtlbHVyYWhhbiwgS2VjLiBhcHIga2VjYW1hdGFuLCBLb3RhLyBLYWIuIGFwciBrb3RhLCBQcm9wLiBhcHIgcHJvcGluc2lkZAInDzwrAA0CAA8WBB4LXyFEYXRhQm91bmRnHgtfIUl0ZW1Db3VudAIBZAEQFgICAgIFFgI8KwAFAQAWAh4KRm9vdGVyVGV4dAUDMTIzPCsABQEAFgIeCkhlYWRlclRleHQFDE5vLiBHUyAmIFRnbBYCZmYWAmYPZBYCAgEPZBYMZg8PFgIfAAUIU0hNLyAxMjNkZAIBDw8WAh8ABRVhcHIgbmFtYSBwZW1lZ2FuZyBoYWtkZAICDw8WAh8ABQMxMjNkZAIDDw8WAh8ABQswNiBGZWIgMjAxM2RkAgQPDxYCHwAFCzA2IEZlYiAyMDEzZGQCBQ8PFgIfAAUPMTIvIDA2IEZlYiAyMDEzZGQCKQ8PFgIfAAUSYXByIGhhayB0YW5nZ3VuZ2FuZGQCKw8PFgIfAAUPNiBGZWJydWFyeSAyMDEzZGQCLQ8PFgIfAAUMUmVrYW4gQmlzbmlzZGQCLw8PFgIfAAUfVGFuYWggZGVuZ2FuIGJhbmd1bmFuIGRpYXRhc255YWRkAjEPDxYCHwAFF0xlYmloIHRpbmdnaSBkYXJpIGphbGFuZGQCMw8PFgIfAAUKU2VnaSBlbXBhdGRkAjUPDxYCHwAFAzEwMGRkAjcPDxYCHwAFAzEwMWRkAjkPDxYCHwAFCEJFTEFLQU5HZGQCOw8PFgIfAAUFVElNVVJkZAI9Dw8WAh8ABQdrdWJ1cmFuZGQCPw8PFgIfAAUFREVQQU5kZAJBDw8WAh8ABQpUSU1VUiBMQVVUZGQCQw8PFgIfAAUGbmVyYWthZGQCRQ8PFgIfAAUFREVQQU5kZAJHDw8WAh8ABQVVVEFSQWRkAkkPDxYCHwAFBnN1bmdhaWRkAksPDxYCHwAFCEJFTEFLQU5HZGQCTQ8PFgIfAAUFVElNVVJkZAJPDw8WAh8ABQVzdXJnYWRkAlEPDxYCHwAFDEJlYmFzIGJhbmppcmRkAlMPDxYCHwAFD0JldG9uIGJlcnR1bGFuZ2RkAlUPDxYCHwAFEFViaW4gUGFya2l0IEphdGlkZAJXDw8WAh8ABQZCYXRha29kZAJZDw8WAh8ABQdBa3VzdGlrZGQCWw8PFgIfAAUJRGFrIEJldG9uZGQCXQ8PFgIfAAULQWxhbmcgQWxhbmdkZAJfDw8WAh8ABQExZGQCYQ8PFgIfAAUDMjAwZGQCYw8PFgIfAAUDMjAwZGQCZQ8PFgIfAAUHcnVhbmdhbmRkAmcPDxYCHwAFFmFwciBmYXNpbGl0YXMgYmFuZ3VuYW5kZAJpDw8WAh8ABQQyMDExZGQCaw8PFgIfAAUBMmRkAm0PDxYCHwAFDzYgRmVicnVhcnkgMjAxM2RkAm8PDxYCHwAFB0RlYml0dXJkZAJxDw8WAh8ABQtHQU1CQVIgS0lPU2RkAnMPDxYCHwBkZGQCdQ8PFgIfAGRkZAJ3Dw8WAh8ABQEwZGQCeQ8PFgIfAGRkZAJ7Dw8WAh8AZGRkAn0PDxYCHwBkZGQCfw8PFgIfAGRkZAKBAQ8PFgIfAGRkZAKDAQ8PFgIfAGRkZAKFAQ8PFgIfAGRkZAKHAQ88KwANAQAPFgQfAWcfAmZkZAKJAQ88KwANAQAPFgQfAWcfAmZkZAKLAQ8PFgIfAAUEMjAxMWRkAo0BDw8WAh8ABQs4MzYsNzg5LDkwMGRkAo8BDw8WAh8ABQszNzgsNzM3LDQwMGRkApEBDw8WAh8ABQMxMjNkZAKTAQ8PFgIfAAUJOSwwMDAsMDAwZGQClQEPDxYCHwAFDTEsMTA3LDAwMCwwMDBkZAKXAQ8PFgIfAAUCMjBkZAKZAQ8PFgIfAAULODg1LDYwMCwwMDBkZAKbAQ8WAh4HVmlzaWJsZWcWAmYPZBYKAgEPDxYCHwAFAzEyM2RkAgMPDxYCHwAFATBkZAIFDw8WAh8ABQEwZGQCBw8PFgIfAAUCMjBkZAIJDw8WAh8ABQEwZGQCnQEPDxYCHwAFC2FwciBwb3NpdGlmZGQCnwEPDxYCHwAFC2FwciBuZWdhdGlmZGQCoQEPDxYCHwAFCk1hcmtldGFibGVkZAKjAQ8PFgIfAAULYXByIGNhdGF0YW5kZAKlAQ8PFgIfAAXqAjx0YWJsZSB3aWR0aD0iMTAwJSI+IDx0cj48dGQgd2lkdGg9IjMzJSI+UGVuaWxhaTwvdGQ+PHRkPiZuYnNwOzwvdGQ+PHRkIHdpZHRoPSIzMyUiPlBlbWVyaWtzYTwvdGQ+PC90cj48dHI+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PC90cj48dHI+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PC90cj48dHI+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PHRkPiZuYnNwOzwvdGQ+PC90cj48dHI+PHRkPihSLiBUb25ueSBIYXJ0b25vKTwvdGQ+PHRkPiZuYnNwOzwvdGQ+PHRkPihFcmZhbiBZb2hhbm5lcyAtIFJPIEpha2FydGEpPC90ZD48L3RyPjwvdGFibGU+ZGQYAwURR3JpZFZpZXdJbmZvSGFyZ2EPPCsACgEIZmQFDEdyaWRWaWV3SW5zcw88KwAKAQhmZAUPR3JpZFZpZXdDb2xfTE5EDzwrAAoBCAIBZHxQlOKnQZyZRqgUxj1o53WnP7X9" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/LOSAPPRAISAL-MEGA_DEV/WebResource.axd?d=E4Ieyflk7p3-YbMDJHLeVw2&amp;t=634354376266562500" type="text/javascript"></script>

<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
WebForm_ReEnableControls();
return true;
}
//]]>
</script>

<div>

	<input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
	<input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
	<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
	<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWBgK+nf/2CgK3zoG4DQLGnaHCDAKkh9WRCAKo6PHFAgKn3u2kBC1it74SEmiPC9aifPKCQtQh1GOP" />
</div>
    <div>
        <input name="h_mcolid" type="hidden" id="h_mcolid" value="PE13020600000101" />
        <input name="h_colid_lnd" type="hidden" id="h_colid_lnd" value="PE13020600000101LND01" />
        <input name="h_APP_OFFICER" type="hidden" id="h_APP_OFFICER" value="R. Tonny Hartono" />
        <input name="h_SPV_OFFICER" type="hidden" id="h_SPV_OFFICER" value="Erfan Yohannes - RO Jakarta" />
        <input name="h_PINCA" type="hidden" id="h_PINCA" />
        
        <table width="100%" class="tablebox1" border="1">
            <tr>
                <td>
                    <table width="100%" class="judul">
                        <tr>
                            <td rowspan="5" class="style1">
                                <!--<img src="../Image/megalogo.jpg" id="Img1" />--> </td>
                        </tr>
                        <tr>
                            <td align="center" ><strong>LAPORAN PENILAIAN</strong></td>
                        </tr>
                        <tr>
                            <td align="center"><strong>
                                <span id="COLTYPE_DESC">KIOS</span></strong></td>
                        </tr>
                        <!--<tr>
                            <td align="center">
                                NO.&nbsp<span id="NOREPORT">000000005/805/Lap-App/FAC04/  II/2013</span>
                            </td>
                        </tr>-->
                        <tr>
                            <td align="center"><strong>
                                <span id="TAXATYPE_DESC">NEW TAKSASI</span></strong>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td style="width: 100%">
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>I.</strong></td>
                            <td><strong>UMUM</strong> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td>1.</td>
                            <td width="30%">Nama Calon Debitur</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="CU_NAME"><? echo $namacalondebitur;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Alamat</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="CU_ADDR"><? echo $alamat;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Bidang Usaha</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="68%">
                            <span id="BT_DESC"><? echo $bidangusaha;?></span>
                            </td>
                        </tr>

                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Cabang / AO</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                                <span id="BRANCHNAME"><? echo $cabang;?></span>&nbsp/&nbsp<span id="AO_USERID"><? echo $namaao;?></span> 
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Tanggal Survey / Report</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                                <span id="EVALSURVEYDATE"><? echo $tanggalsurvey;?></span>&nbsp/&nbsp<span id="TGL_TRACK_APR"><? echo $tanggalreport;?></span>
                            </td>
                        </tr>
                        <tr id="Tr1" class="text1">
	<td>6.</td>
	<td width="30%">Penilaian Terdahulu</td>
	<td width="2%">:</td>
	<td colspan="2" width="15%">
                            <span id="LASTTAXA_DATE1"></span>
                            
                            </td>
</tr>

                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Yang dihubungi</td>
                            <td width="2%">:</td>
                            <td colspan="2" width="15%">
                            <span id="CU_CONTACT"><? echo $yangdihubungi;?></span>
                            </td>
                        </tr>
                    
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>II.</strong></td>
                            <td><strong>DESKRIPSI TANAH</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Lokasi Agunan</td>
                            <td width="2%">:</td>
                            <td><span id="COL_ADDR"><? echo $alamatapr;?></span>
                            
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Status</td>
                            <td width="2%">:</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr class="text1">
                            <td colspan="4">
							<?
								$JENISSERTIFIKAT = "";
								$NOMORSERTIFIKAT = "";
								$PEMEGANGHAK = "";
								$LUASTANAH = "";
								$TGLBERAKHIRSERTIFIKAT = "";
								$TGLTERBITSERTIFIKAT = "";
								$NOGSSU = ""; //
								$TGLGSSU = ""; //
								$JMLLUAS = 0;
								
								$tsql = "SELECT * FROM COL_CERT_LND WHERE COL_ID IN (SELECT COL_ID FROM COLLATERAL_LND WHERE LISCOL_ID = '$liscolid')";
								$a = sqlsrv_query($connapr, $tsql);
								if ( $a === false)
								die( FormatErrors( sqlsrv_errors() ) );
								if(sqlsrv_has_rows($a))
								{ 	
									
									
								
							?>
                                <div>
	<table cellspacing="0" cellpadding="2" rules="all" border="1" id="GridViewCol_LND" style="font-family:Arial;font-size:XX-Small;width:100%;border-collapse:collapse;">
		<tr>
			<th align="center" scope="col">Jenis Hak/ No.Sertifikat</th><th align="center" scope="col">Pemegang Hak</th><th align="center" scope="col">Luas (m2)</th><th align="center" scope="col">Berakhir Hak</th><th align="center" scope="col">Tgl Penerbitan Sertifikat</th><th align="center" scope="col">No. GS &amp; Tgl</th>
		</tr>
							<?
									while($row = sqlsrv_fetch_array($a, SQLSRV_FETCH_ASSOC))
									{	
										$JMLLUAS = $JMLLUAS + $row["CERT_LAND_DIM"];
									
										$JENISSERTIFIKAT = $row["CERT_TYPE"];
										$NOMORSERTIFIKAT = $row["CERT_NO"];
										$PEMEGANGHAK = $row["CERT_OWNER"];
										$LUASTANAH = $row["CERT_LAND_DIM"];
										$TGLBERAKHIRSERTIFIKAT = $row["CERT_DUEDATE"];
										$TGLTERBITSERTIFIKAT = $row["CERT_DATE"];
										$NOGSSU = $row["GB_NO"];
										$TGLGSSU = $row["GB_DATE"];

										if($TGLBERAKHIRSERTIFIKAT != "")
										{
											$TGLBERAKHIRSERTIFIKAT = $row["CERT_DUEDATE"]->format('d F Y');
										}
										if($TGLTERBITSERTIFIKAT != "")
										{
											$TGLTERBITSERTIFIKAT = $row["CERT_DATE"]->format('d F Y');
										}
										if($TGLGSSU != "")
										{
											$TGLGSSU = $row["GB_DATE"]->format('d F Y');
										}
										
							?>
		<tr>
			<td align="left"><? echo $JENISSERTIFIKAT;?>/ <? echo $NOMORSERTIFIKAT;?></td><td align="left"><? echo $PEMEGANGHAK;?></td><td align="right"><? echo $LUASTANAH;?></td><td align="right">&nbsp;<? echo $TGLBERAKHIRSERTIFIKAT;?></td><td align="right"><? echo $TGLTERBITSERTIFIKAT;?></td><td align="right"><? echo $NOGSSU;?>/ <? echo $TGLGSSU;?></td>
		</tr>
							<?
									}
							?>
		<tr>
			<td>&nbsp;</td><td>&nbsp;</td><td align="right"><? echo $JMLLUAS;?></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
		</tr>
	</table>
</div>
                            <?
								}
							?>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">No. Hak Tanggungan</td>
                            <td width="2%">:</td>
                            <td><span id="HYPOTIC_NO"><? echo $NOHAKTANGGUNGAN;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">Tgl. Hak Tanggungan</td>
                            <td width="2%">:</td>
                            <td><span id="HYPOTIC_DATE"><? echo $TGLHAKTANGGUNGAN;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Hubungan pemegang hak (Calon Debitur)</td>
                            <td width="2%">:</td>
                            <td><span id="REL_DESC"><? echo $HUBUNGANPEMEGANGHAK;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Identifikasi</td>
                            <td width="2%">:</td>
                            <td><span id="IDTF_DESC"><? echo $IDENTIFIKASI;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Topografi</td>
                            <td width="2%">:</td>
                            <td><span id="TPGF_DESC"><? echo $TOPOGRAFI;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>8.</td>
                            <td width="30%">Bentuk Type</td>
                            <td width="2%">:</td>
                            <td><span id="BTKTNH_DESC"><? echo $BENTUK;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>9.</td>
                            <td width="30%">Ukuran Type</td>
                            <td width="2%">:</td>
                            <td>panjang : <span id="LND_LENM2"><? echo $UKURANPANJANG;?></span>&nbsp
                                lebar : <span id="LND_WIDM2"><? echo $UKURANLEBAR;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>10.</td>
                            <td width="30%">Batas-batas Type</td>
                            <td width="2%">:</td>
                            <td>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td colspan="3">
                                <table width="100%">
                                    <tr>
                                        <td><span id="SIDE_DESC1"><? echo $BATAS1;?></span>/
                                        <span id="CDNDIR_DESC1"><? echo $ARAH1;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA1"><? echo $ISI1;?></span>
                                        </td>
                                        <td><span id="SIDE_DESC3"><? echo $BATAS3;?></span>/
                                        <span id="CDNDIR_DESC3"><? echo $ARAH3;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA3"><? echo $ISI3;?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span id="SIDE_DESC2"><? echo $BATAS2;?></span>/
                                        <span id="CDNDIR_DESC2"><? echo $ARAH2;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA2"><? echo $ISI2;?></span>
                                        </td>
                                        <td><span id="SIDE_DESC4"><? echo $BATAS4;?></span>/
                                        <span id="CDNDIR_DESC4"><? echo $ARAH4;?></span>
                                        </td>
                                        <td width="2%">:
                                        </td>
                                        <td><span id="BDRY_DATA4"><? echo $ISI4;?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>11.</td>
                            <td width="30%">Daerah termasuk</td>
                            <td width="2%">:</td>
                            <td><span id="CNDDRH_DESC"><? echo $DAERAHTERMASUK;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    
                    </table>
                </td>
            </tr>

            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>III.</strong> </td>
                            <td><strong>DESKRIPSI BANGUNAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Material Bangunan</td>
                            <td width="2%">&nbsp;</td>
                            <td>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Rangka Utama</td>
                            <td width="2%">:</td>
                            <td><span id="RGKUT_DESC"><? echo $RANGKAUTAMA;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Lantai</td>
                            <td width="2%">:</td>
                            <td><span id="LNTAI_DESC"><? echo $LANTAI;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Dinding</td>
                            <td width="2%">:</td>
                            <td><span id="DDING_DESC"><? echo $DINDING;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Langit-langit</td>
                            <td width="2%">:</td>
                            <td><span id="LNGIT_DESC"><? echo $LANGIT;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Rangkap atap</td>
                            <td width="2%">:</td>
                            <td><span id="RGKAT_DESC"><? echo $RANGKAATAP;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Atap</td>
                            <td width="2%">:</td>
                            <td><span id="ATAP_DESC"><? echo $ATAP;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Jumlah lantai</td>
                            <td width="2%">:</td>
                            <td><span id="JMLLT_DESC"><? echo $JUMLAHLANTAI;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Luas</td>
                            <td width="2%">:</td>
                            <td><span id="IMB_DIM"><? echo $LUASIMB;?></span>&nbsp m2 &nbsp(IMB)
                                <span id="FIS_DIM"><? echo $LUASFISIK;?></span>&nbsp m2 &nbsp(Fisik)
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">- Pembagian ruang</td>
                            <td width="2%">:</td>
                            <td><span id="PEMBAGIAN_RUANG"><? echo $PEMBAGIANRUANG;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Fasilitas Bangunan</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_BANGUNAN"><? echo $FASILITASBANGUNAN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Tahun di bangun</td>
                            <td width="2%">:</td>
                            <td><span id="BUILTYEAR"><? echo $TAHUNDIBANGUN;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>4.</td>
                            <td width="30%">IMB No.</td>
                            <td width="2%">:</td>
                            <td><span id="IMB_NO"><? echo $NOIMB;?></span>&nbsp Tgl :
                            &nbsp<span id="IMB_DATE"><? echo $TGLIMB;?></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Dihuni oleh</td>
                            <td width="2%">:</td>
                            <td><span id="HUNI_DESC"><? echo $DIHUNIOLEH;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Keterangan lain</td>
                            <td width="2%">:</td>
                            <td><span id="BLD_NOTE"><? echo $KETERANGAN;?></span></td>
                        </tr>
                    
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    
                    </table>
                </td>
            </tr>


            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>IV.</strong></td>
                            <td><strong>ANALISA LINGKUNGAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1">1.</td>
                            <td width="30%">Jalan Pencapaian</td>
                            <td width="2%">:</td>
                            <td><span id="JALAN_PENCAPAIAN"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td width="30%">Jalan didepan lokasi</td>
                            <td width="2%">:</td>
                            <td><span id="PMKJLN_DESC"></span>&nbsp;lebar&nbsp;
                                <span id="LEBAR_JLNDPNLOK">0</span>&nbsp;m&nbsp;&nbsp;kondisi&nbsp;
                                <span id="CNDJLN_DESC"></span>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>3.</td>
                            <td width="30%">Arus lalu lintas</td>
                            <td width="2%">:</td>
                            <td><span id="LLARAH_DESC"></span>&nbsp;arah&nbsp;dgn intensitas
                                <span id="LLINTN_DESC"></span>&nbsp;
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>4</td>
                            <td width="30%">Fasilitas Umum</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_UMUM"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>5.</td>
                            <td width="30%">Fasilitas Angkutan</td>
                            <td width="2%">:</td>
                            <td><span id="FASILITAS_ANGKUTAN"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>6.</td>
                            <td width="30%">Obyek Penting dekat lokasi</td>
                            <td width="2%">:</td>
                            <td><span id="OBJEK_PENTING_DEKAT_LOKASI"></span></td>
                        </tr>
                        <tr class="text1">
                            <td>7.</td>
                            <td width="30%">Peruntukan terbaik</td>
                            <td width="2%">:</td>
                            <td><span id="PRNTK_DESC"></span></td>
                        </tr>
                                            
                        <tr class="text1">
                            <td>&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                                            
                    </table>
                </td>
            </tr>
            
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td class="style1"><strong>V.</strong></td>
                            <td><strong>ASURANSI</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <div>

</div>
                </td>
            </tr>

            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VI.</strong></td>
                            <td><strong>KONSULTASI/ INFORMASI HARGA</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td align="left">
                    <table width="100%">
                        <tr class="text1">
                            <td width="3%">1.</td>
                            <td width="20%">Ditawarkan/ dijual</td>
                            <td width="2%">:</td>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div>

</div>
                            </td>
                        </tr>
                        <tr class="text1">
                            <td>2.</td>
                            <td>NJOP Tahun (<span id="NJOP_YYYY">2011</span>)</td>
                            <td>:</td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">Tanah</td>
                                        <td width="2%">:</td>
                                        <td><span id="NJOP_VAL_LND"></span>&nbsp /M2
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Bangunan</td>
                                        <td width="2%">:</td>
                                        <td><span id="NJOP_VAL_BLD"></span>&nbsp /M2</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VII.</strong></td>
                            <td><strong>PENILAIAN</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td width="70%" colspan="2" style="text-align: center">URAIAN</td>
                            <td style="text-align: center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">1.</td>
                            <td>Nilai Pasar</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">&nbsp;</td>
                            <td><table width="100%">
                                    <tr>
                                        <td width="15%">&nbsp;</td>
                                        <td width="2%">:</td>
                                        <td><span id="LUAS_TANAH"><? echo $LUASTANAH;?></span></td>
                                        <td width="2%">x</td>
                                        <td><span id="NILAI_TANAH_PERM2"><? echo numberFormat($NILAITANAHPERM2);?></span></td>
                                    </tr>
                                </table>
                                </td>
                            <td><span id="NILAI_TANAH_TOTAL"><? echo numberFormat($NILAITANAHTOTAL);?></span></td>
                        </tr>
                        <tr>
                            <td width="3%">2.</td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">Safety Margin</td>
                                        <td width="3%">:</td>
                                        <td><span id="SAFETY_MARGIN"><? echo $SAFETYMARGIN;?></span>&nbsp;%</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">3.</td>
                            <td>
                                Nilai Likuidasi</td>
                            <td><span id="NILAI_LIKUIDASI"><? echo numberFormat($NILAILIKUIDASI);?></span>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr id="TR_OVR" class="text1">
	<td>
                    <table width="100%">
                        <tr>
                            <td width="70%" colspan="2" style="text-align: center">URAIAN OVERRIDE</td>
                            <td style="text-align: center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">1.</td>
                            <td>Nilai Pasar</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">&nbsp;</td>
                            <td><table width="100%">
                                    <tr>
                                        <td width="15%">&nbsp;</td>
                                        <td width="2%">:</td>
                                        <td><span id="T_LUAS_TANAH"><? echo $LUASTANAH;?></span></td>
                                        <td width="2%">x</td>
                                        <td><span id="OVR_NILAI_TANAH">0</span></td>
                                    </tr>
                                </table>
                                </td>
                            <td><span id="OVR_TOTAL_NILAI_TANAH">0</span></td>
                        </tr>
                        <tr>
                            <td width="3%">2.</td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="30%">Safety Margin</td>
                                        <td width="3%">:</td>
                                        <td><span id="T_SAFETY_MARGIN"><? echo $SAFETYMARGIN;?></span>&nbsp;%</td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td width="3%">3.</td>
                            <td>
                                Nilai Likuidasi</td>
                            <td><span id="OVR_LIKUIDASI">0</span>
                                </td>
                        </tr>
                    </table>
                </td>
</tr>

            
            <tr class="text1">
                <td>
                    <table width="100%">
                        <tr>
                            <td class="style1"><strong>VIII.</strong></td>
                            <td><strong>OPINI PENILAI</strong></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr class="text1">
                            <td align="left" width="2%">-</td>
                            <td align="left" width="15%">Positif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALPOS"><? echo $OPINIPOSITIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Negatif</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNEG"><? echo $OPININEGATIF;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Kondisi Pasar</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="MK_DESC"><? echo $KONDISIPASAR;?></span></td>
                        </tr>
                        <tr class="text1">
                            <td align="left">-</td>
                            <td align="left">Catatan/ Alasan</td>
                            <td width="2%">:</td>
                            <td colspan="2"><span id="EVALNOTE"><? echo $CATATAN;?></span></td>
                        </tr>
                    
                    </table>
                </td>
            </tr>

            <!--<tr class="text1">
                <td align="left">
                    <span id="TTD"><table width="100%"> <tr><td width="33%">Penilai</td><td>&nbsp;</td><td width="33%">Pemeriksa</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>(R. Tonny Hartono)</td><td>&nbsp;</td><td>(Erfan Yohannes - RO Jakarta)</td></tr></table></span>
                </td>
            </tr>-->
            </table>    
    <input type="button" id="btn_print" value="Print" onclick="onPrint()" />
    </div>
    
<script type="text/javascript">
//<![CDATA[
var __enabledControlArray =  new Array('h_mcolid', 'h_colid_lnd', 'h_APP_OFFICER', 'h_SPV_OFFICER', 'h_PINCA');
//]]>
</script>



</script>
</form>
</body>
</html>
